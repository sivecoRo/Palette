package ro.siveco.ael;

import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.AssignableTypeFilter;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.testng.annotations.Test;
import ro.siveco.ram.controller.RestController;

/**
 * User: AlexandruVi
 * Date: 2017-08-01
 */
public class Ael7EdgeApplicationTest {


    @Test
    public void conformityChecks() throws Exception {
        ClassPathScanningCandidateComponentProvider provider = new ClassPathScanningCandidateComponentProvider(false);
        provider.addIncludeFilter(new AssignableTypeFilter(RestController.class));
        provider.findCandidateComponents("ro.siveco.ael").forEach(beanDefinition -> {
            try {
                Class<?> controllerClass = Class.forName(beanDefinition.getBeanClassName());
                if (controllerClass.getAnnotation(Controller.class) != null &&
                        controllerClass.getAnnotation(RequestMapping.class) == null) {
                    throw new RuntimeException("Gogule, [" + beanDefinition.getBeanClassName() +
                            "] tre să aibe adnotarea @RequestMapping.");

                }
            } catch (ClassNotFoundException e) {

            }
        });
    }

}