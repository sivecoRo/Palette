MetaRenderer.addPresentationRendererOverride('courseLearningMaterial', {
    name  : 'PIE_CHART',
    render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
        var courseLearningMaterialStatistics = model[propertyRenderer.propertyName], columnNames = [], columns = [], tooltips = [],
            names = [];

        if(courseLearningMaterialStatistics != null ) {
            columnNames.push(MetaRenderer.i18nMessage('model.plannedCourseStudent.completed')
                             .replace('{0}', courseLearningMaterialStatistics.finished + '%'));
            columnNames.push(MetaRenderer.i18nMessage('model.plannedCourseStudent.started')
                             .replace('{0}', courseLearningMaterialStatistics.learning + '%'));
            columnNames.push(MetaRenderer.i18nMessage('model.plannedCourseStudent.not.started')
                             .replace('{0}', courseLearningMaterialStatistics.inactive + '%'));

            columns.push([columnNames[0], courseLearningMaterialStatistics.finished]);
            columns.push([columnNames[1], courseLearningMaterialStatistics.learning]);
            columns.push([columnNames[2], courseLearningMaterialStatistics.inactive]);

            names.push(columnNames[0]);
            names.push(columnNames[1]);
            names.push(columnNames[2]);

            $propertyElement.data('chartSettings', {
                columns: columns,
                tooltips: tooltips,
                options: {data: {columns: columns, names: names}}
            });

            $propertyElement.on("app:elementTransposed", function (event) {
                console.debug('FLUSH CHART');
                var chart = MetaRenderer.chartRenderer($(event.target), 'pieChart');
                chart.flush();
            });
        }
    }
});

MetaRenderer.addPresentationRendererOverride('courseLearningMaterial', {
    name  : 'MODEL_COLLECTION',
    render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
        var defaultRenderer = MetaRenderer.getDefaultPresentationRenderer('MODEL_COLLECTION');
        if (propertyRenderer.propertyName === 'resourceUsers') {
            MetaRenderer.gridRenderer($propertyElement, propertyRenderer, $targetParent, model);
        }
        else {
            defaultRenderer.render(model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer);
        }
    }
});

MetaRenderer.addPresentationRendererOverride('courseLearningMaterial', {
    name  : 'MODEL',
    render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
        var defaultRenderer = MetaRenderer.getDefaultPresentationRenderer('MODEL');
        if (propertyRenderer.propertyName === 'resourceUserStatisticsDetails') {
            var value = model[propertyRenderer.propertyName];
            if( value != null ) {
                var activitiesNo = value.activitiesNo;
                var studentsNo = value.studentsNo;
                var averageTime = value.averageTime;
                var spentTime = value.spentTime;
                $propertyElement.append('<ul>');
                $propertyElement.append('<li style="list-style: none;">' + activitiesNo + '</li>');
                $propertyElement.append('<li style="list-style: none;">' + studentsNo + '</li>');
                $propertyElement.append('<li style="list-style: none;">' + averageTime + '</li>');
                $propertyElement.append('<li style="list-style: none;">' + spentTime + '</li>');
                $propertyElement.append('</ul>');
            }
        } else {
            defaultRenderer.render(model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer);
        }
    }
});
