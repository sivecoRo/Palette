EventsManager.getInstance().on(EventsManager.META_UPDATED, function (event) {

    // $('[data-model-property-name=interactionType]').find('select').material_select();

});


MetaRenderer.addInputRendererOverride('surveyItem', {
    name: 'SELECT',
    render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
        var defaultRenderer = MetaRenderer.getDefaultInputRenderer('SELECT');
        defaultRenderer.render(model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer);
        console.log($propertyElement);
        $propertyElement.on("app:dirtyChange",function(e,dirty,propertyName,currentValue,$sourceInput,sourceEvent){
            var $el=$(sourceEvent.target),
                $siv=$el.parent().parent().parent().nearest('[data-model-property-name=surveyItemValues]'),
                $sliderMin=$el.parent().parent().parent().nearest('[data-model-property-name=sliderMinVal]'),
                $sliderMax=$el.parent().parent().parent().nearest('[data-model-property-name=sliderMaxVal]');

            if('SLIDER' == currentValue)
            {
                $siv.css('display', 'none');
                $sliderMin.css('display', 'block');
                $sliderMin.find('input[type=number]').attr('min', 0);
                $sliderMax.css('display', 'block');
                $sliderMax.find('input[type=number]').attr('min', 0);
            }
            else if('ESSAY' == currentValue)
            {
                $siv.css('display', 'none');
                $sliderMin.css('display', 'none');
                $sliderMax.css('display', 'none');
            }
            else //if('SINGLE_CHOICE' == currentValue || 'MULTIPLE_CHOICE' == currentValue)
            {
                $siv.css('display', 'block');
                $sliderMin.css('display', 'none');
                $sliderMax.css('display', 'none');
            }

        });
    }

});


MetaRenderer.addInputRendererOverride('surveyItem', {
    name: 'COLLECTION_BUILDER',
    render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
        var defaultRenderer = MetaRenderer.getDefaultInputRenderer('COLLECTION_BUILDER');
        defaultRenderer.render(model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer);
        $propertyElement.hide();
    }

});

MetaRenderer.addInputRendererOverride('surveyItem', {
    name: 'NUMBER',
    render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
        var defaultRenderer = MetaRenderer.getDefaultInputRenderer('NUMBER');
        defaultRenderer.render(model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer);
        $propertyElement.hide();
    }
});