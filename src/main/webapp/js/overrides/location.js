/**
 * Created by albertb on 2/8/2018.
 */

//Cities autocomplete
var placeSearch, locationAutocomplete;
var componentForm = {
    street_number: 'short_name',
    route: 'long_name',
    locality: 'long_name',
    administrative_area_level_1: 'short_name',
    country: 'long_name',
    postal_code: 'short_name'
};

var options = {
    types: ['geocode'],
    componentRestrictions: {country: "si"}
};

function reinitAutocomplete()
{
    var selectedCountryLabel = $( "#countries" ).val();
    $('#cities').val(null);
    doOnTryPaletteEmptyCity();
    //reinitialize options with selected country
    options = {
        types: ['geocode'],
        componentRestrictions: {country: selectedCountryLabel}
    };
    Autocomplete(options);
}

function Autocomplete(options) {
// Create the autocomplete object, restricting the search to geographical
// location types.
    locationAutocomplete = new google.maps.places.Autocomplete(
        (document.getElementById('cities')),options);

    locationAutocomplete.addListener('place_changed', onLocationCityChange);
}

function doOnTryPaletteEmptyCity() {
    $("#usersResultCount").removeClass("visible-image");
    $("#usersResultCount").addClass("hidden-image");
    $("#textResult").removeClass("visible-image");
    $("#textResult").addClass("hidden-image");
}

function onLocationCityChange() {
    var place = locationAutocomplete != null ? locationAutocomplete.getPlace() : null,
        fData = null,
        tryPalette = JSON.parse(window.sessionStorage.getItem("tryPalette"));

    if (place != null && place.geometry) {
        var coordinates = place.geometry.location.lat() + ";" + place.geometry.location.lng();
        fData = new FormData()
        fData.append("coordinates", coordinates);
        tryPalette.coordinates = coordinates;
        window.sessionStorage.setItem("tryPalette", JSON.stringify(tryPalette));
    }
    else if (tryPalette != null && tryPalette.coordinates != null) {
        fData = new FormData();
        fData.append("coordinates", tryPalette.coordinates);
    }
    if (fData != null) {
        $.ajax({
            url: "/getUsersForCityCoordinates",
            type: 'POST',
            data: fData,
            dataType: 'json',
            processData: false, // Don't process the files
            contentType: false, // Set content type to false as jQuery will tell the server its a query string request
            success: function (data, textStatus, jqXHR) {
                if (data.success) {
                    if (data.userResponseCount != undefined) {
                        $("#usersResultCount").html(data.userResponseCount);
                        if ($("#usersResultCount").hasClass("hidden-image")) {
                            $("#usersResultCount").removeClass("hidden-image");
                            $("#usersResultCount").addClass("visible-image");

                            $("#textResult").removeClass("hidden-image");
                            $("#textResult").addClass("visible-image");
                        }
                        if ($("#cities").val() == "") {
                            doOnTryPaletteEmptyCity();
                        }
                    }
                }
                else {
                    alert("ERROR");
                }
            },
            error: function (data, textStatus, jqXHR) {
                alert("ERROR");
            }
        });
    }
}

function initTryPalette() {
    window.sessionStorage.setItem("tryPalette", JSON.stringify({country:null, city:null, coordinates:null, interests:null}));
    window.location = '/location';
}

function initTryPaletteLocation() {
    var tryPalette = JSON.parse(window.sessionStorage.getItem("tryPalette")),
        $countries = $( "#countries" );
    if (tryPalette != undefined) {
        if (tryPalette.country != null) {
            $countries.val(tryPalette.country);
            $countries.material_select();
            $countries.parent().removeClass('initialized');
        }
        if (tryPalette.city != null) {
            $( "#cities" ).val(tryPalette.city);
        }
        onLocationCityChange();
    }
}

function onTryPaletteLocationNext() {
    var selectedCountry = $( "#countries" ).val(),
        selectedCity = $( "#cities" ).val(),
        errorTitle,
        errorMessage;
    if (selectedCountry == null || $.trim(selectedCountry) == "") {
        errorMessage = MetaRenderer.i18nMessage("error.tryPalette.location.country.empty");
        errorTitle = MetaRenderer.i18nMessage("error.tryPalette.location.country.empty.title");
        app.errorNotify({title: errorTitle, message: errorMessage});
        hideLoader();
    } else if (selectedCity == null || $.trim(selectedCity) == "") {
        errorMessage = MetaRenderer.i18nMessage("error.tryPalette.location.city.empty");
        errorTitle = MetaRenderer.i18nMessage("error.tryPalette.location.city.empty.title");
        app.errorNotify({title: errorTitle, message: errorMessage});
    }
    else {
        var tryPalette = JSON.parse(window.sessionStorage.getItem("tryPalette"));
        tryPalette.country = $( "#countries" ).val();
        tryPalette.city = $( "#cities" ).val();
        window.sessionStorage.setItem("tryPalette",JSON.stringify(tryPalette));
        showLoader();
        window.location = '/interests';
    }
}


