function onClickResultsCourse() {
    var $modal = $('#tryPaletteResultsModal');
    $modal.modal();
    $modal.modal('open');
}

function closeTryPaletteResultsModal() {
    $('#tryPaletteResultsModal').modal('close');
}