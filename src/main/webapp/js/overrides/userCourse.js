MetaRenderer.addPresentationRendererOverride('userCourse', {
    name: 'PIE_CHART',
    render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
        var userCourseStatistics = model[propertyRenderer.propertyName], columnNames = [], columns = [], tooltips = [],
            names = [];

        if (userCourseStatistics != null) {
            columnNames.push(MetaRenderer.i18nMessage('model.plannedCourseStudent.completed')
                             .replace('{0}', userCourseStatistics.finished + '%'));
            columnNames.push(MetaRenderer.i18nMessage('model.plannedCourseStudent.started')
                             .replace('{0}', userCourseStatistics.learning + '%'));
            columnNames.push(MetaRenderer.i18nMessage('model.plannedCourseStudent.not.started')
                             .replace('{0}', userCourseStatistics.inactive + '%'));

            columns.push([columnNames[0], userCourseStatistics.finished]);
            columns.push([columnNames[1], userCourseStatistics.learning]);
            columns.push([columnNames[2], userCourseStatistics.inactive]);

            names.push(columnNames[0]);
            names.push(columnNames[1]);
            names.push(columnNames[2]);

            $propertyElement.data('chartSettings', {
                columns: columns,
                tooltips: tooltips,
                options: {data: {columns: columns, names: names}}
            });

            $propertyElement.on("app:elementTransposed", function (event) {
                console.debug('FLUSH CHART');
                var chart = MetaRenderer.chartRenderer($(event.target), 'pieChart');
                chart.flush();
            });
        }
    }
});

MetaRenderer.addPresentationRendererOverride('userCourse', {
    name: 'MODEL_COLLECTION',
    render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
        var defaultRenderer = MetaRenderer.getDefaultPresentationRenderer('MODEL_COLLECTION');
        if (propertyRenderer.propertyName === 'userResources') {
            MetaRenderer.gridRenderer($propertyElement, propertyRenderer, $targetParent, model);
            } else if (propertyRenderer.propertyName === 'plannedCourses') {
            renderOverviewSelectData(model, model.plannedCourses, $propertyElement);
            }
        else {
            defaultRenderer.render(model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer);
        }
    }
});

MetaRenderer.addPresentationRendererOverride('userCourse', {
    name: 'MODEL',
    render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
        var defaultRenderer = MetaRenderer.getDefaultPresentationRenderer('MODEL');
        if (propertyRenderer.propertyName === 'userResourceStatisticsDetails') {
            var value = model[propertyRenderer.propertyName];
            if (value != null) {
                var activitiesNo = value.activitiesNo,
                    score = value.score,
                    spentTime = value.spentTime,
                    promoted = value.promoted,
                    grade = value.grade;
                $propertyElement.append('<ul>');
                $propertyElement.append('<li style="list-style: none;">' + activitiesNo + '</li>');
                $propertyElement.append('<li style="list-style: none;">' + score + '</li>');
                $propertyElement.append('<li style="list-style: none;">' + spentTime + '</li>');
                $propertyElement.append('<li style="list-style: none;">' + promoted + '</li>');
                $propertyElement.append('<li style="list-style: none;">' + grade + '</li>');
                $propertyElement.append('</ul>');
            }
        } else {
            defaultRenderer.render(model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer);
        }
    }
});

function renderOverviewSelectData(model, items, $propertyElement) {
    var $ul = $('<ul class="element-transposed"></ul>');
    if (items) {
        $.each(items, function (index,item) {
            //var item = $(this)[0],
            var $li = $('<li></li>');
            $li.data('id', item.id);
            $li.data('label', item.reportLabel);
            $ul.append($li);
        });
        $propertyElement.append($ul);
    }
}

/*function renderOverviewSelect(model, items, $propertyElement) {
    //var $select = $('#userResourclass="element-transposed"cePlannedCoursesSelect'),
    var $select = $('<select ><option value="0" selected="selected">Choose planned course</option></select>'),
        $firstOption = $select.find('option').first();
/!*
    $select.empty()
    .append($firstOption);*!/

    if (items) {
        $.each(items, function (index) {
            var item = $(this)[0];
            $option = $('<option value="' + item.id + '">' + item.reportLabel + '</option>');
            $select.append($option);
        });
    }
    $select.material_select();

    $select.on('contentChanged', function () {
        $(this).material_select();
    });
    $select.on('change', function () {
        filterUserCourses(model);
    });
    $select.data("userOverview", model);
    $propertyElement.append($select);
}*/

/*function filterUserCourses(model) {
    var $selectedUser = $('#users').find('.collection-item.active'),
        selectedUserData = $selectedUser.data(),
        selectedUserId = selectedUserData == null ? null : selectedUserData.modelUid,
        plannedCourseId = $('#userResourcePlannedCoursesSelect').val(),
        $collectionContainer = $('.property-value.collection'),
        $usersSearch = $('#usersSearch');

    //if( selectedUserId != null ) {
    ModelStore.promiseOf('userCourse', true).done(function () {
        var modelStore = this, transposedProperties = [], extraPropertiesClasses = {},
            $wrapper = $('.model-property-userCourses'),
            propertyRenderer = {
                propertyName: 'userCourses',
                viewName: '_default',
                propertyHolderModelName: 'userOverview',
                modelName: 'userCourse'
            };

        this.remotePageRequest({
                                   id: selectedUserId, courseId: model.objectUID, plannedCourseId: plannedCourseId,
                                   courseName: $usersSearch.val()
                               }, null, false)
        .done(function (page) {
            if (page.content) {
                var collectionItems = $collectionContainer.find('.collection-item');
                $.each(collectionItems, function (index, collectionItem) {
                    collectionItem.remove();
                });
                simpleOverviewCollectionRenderer($wrapper, page.content, propertyRenderer, $collectionContainer, model);
                $('.collection').first().remove();
            }
        });
    });
    //}
}*/

