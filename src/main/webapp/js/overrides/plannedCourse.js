/**
 * User: AlexandruVi
 * Date: 2017-09-10
 */
MetaRenderer.addPresentationRendererOverride('plannedCourse', {
    name  : 'PIE_CHART',
    render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
        var plannedCourseDetails = model[propertyRenderer.propertyName], columnNames = [], columns = [], tooltips = [],
            names = [];

        columnNames.push(MetaRenderer.i18nMessage('model.plannedCourseStudent.not.started')
            .replace('{0}', plannedCourseDetails.notStartedStudentsNumber));
        columnNames.push(MetaRenderer.i18nMessage('model.plannedCourseStudent.started')
            .replace('{0}', plannedCourseDetails.startedStudentsNumber));
        columnNames.push(MetaRenderer.i18nMessage('model.plannedCourseStudent.completed')
            .replace('{0}', plannedCourseDetails.completedStudentsNumber));

        columns.push([columnNames[0], plannedCourseDetails.notStartedStudentsNumber]);
        columns.push([columnNames[1], plannedCourseDetails.startedStudentsNumber]);
        columns.push([columnNames[2], plannedCourseDetails.completedStudentsNumber]);

        names.push(columnNames[0]);
        names.push(columnNames[1]);
        names.push(columnNames[2]);

        $propertyElement.data('chartSettings', {
            columns : columns,
            tooltips: tooltips,
            options : {data: {columns: columns, names: names}}
        });

        $propertyElement.on("app:elementTransposed", function (event) {
            console.debug('FLUSH CHART');
            var chart = MetaRenderer.chartRenderer($(event.target), 'pieChart');
            chart.flush();
        });
    }
});
MetaRenderer.addPresentationRendererOverride('plannedCourse', {
    name  : 'MODEL_COLLECTION',
    render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
        var defaultRenderer = MetaRenderer.getDefaultPresentationRenderer('MODEL_COLLECTION');
        if (propertyRenderer.propertyName === 'attendingStudentsForDisplay' ) {
            $propertyElement.data('rendererType', 'MODEL_AVATAR_IMG');
        }
        if (propertyRenderer.propertyName === 'allAttendingStudents' ) {
            $propertyElement.data('rendererType', 'MODEL_AVATAR');
        }
        if (propertyRenderer.propertyName === 'attendingStudentsForDisplay' || propertyRenderer.propertyName === 'allAttendingStudents') {

            $propertyElement.on("app:elementTransposed", function (event) {
                $.each(model[propertyRenderer.propertyName], function (index, user) {
                    MetaRenderer.updateView('aelUser', user.id);
                });
            });
        }
        defaultRenderer.render(model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer);
        if (propertyRenderer.propertyName === 'attendingStudentsForDisplay' || propertyRenderer.propertyName === 'allAttendingStudents') {
            //addClickHandlerForUser($propertyElement);
            $propertyElement.find('li.model-view.collection-item').each(function(index,element) {
                $(element).click(function() {
                    window.location = "/profile/" + $(element).data('currentValue').id;
                });
            })
        }
        if (propertyRenderer.propertyName === 'allAttendingStudents' ) {
            $propertyElement.hide();
        }
    }
});

MetaRenderer.addInputRendererOverride('plannedCourse', {
    name  : 'IMAGE',
    render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
        var value = model[propertyRenderer.propertyName];
        $propertyElement.append('<img src="' + value + '" class="image-tag "/>');
    }
});

MetaRenderer.addPresentationRendererOverride('plannedCourse', {
    name  : 'IMAGE',
    render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
        var defaultRenderer = MetaRenderer.getDefaultPresentationRenderer('IMAGE');
        defaultRenderer.render(model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer);
        if (propertyRenderer.propertyName === 'ownerImageSrc') {
            $propertyElement.on('click', function() {
                window.location = '/profile/' + model.course.owner.id;
            });
        }
    }
});


MetaRenderer.addPresentationRendererOverride('plannedCourse', {
    name  : 'BUTTON',
    render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
        renderButtonsForAttendingSection(model, $propertyElement, propertyRenderer);
    }
});

function renderButtonsForAttendingSection(model, $propertyElement, propertyRenderer) {
    var value = model[propertyRenderer.propertyName],
        defaultRenderer = MetaRenderer.getDefaultPresentationRenderer('BUTTON'),
        addedToFavoriteViewClass = 'favorite-added-item-view',
        notAddedToFavoriteViewClass = 'favorite-not-added-item-view',
        notAddedButtonLabel = MetaRenderer.i18nMessage('model.' + model.modelName + '.property.addToFavoritesBtn.notAdded.label'),
        addedButtonLabel = MetaRenderer.i18nMessage('model.' + model.modelName + '.property.addToFavoritesBtn.label'),
        notAttendingClass = 'not-attending-item-view',
        attendingClass = 'attending-item-view';
    if (propertyRenderer.propertyName === "addToFavoritesBtn") {
        if (model.addedToFavorite) {
            if (!$propertyElement.hasClass(addedToFavoriteViewClass)) {
                $propertyElement.addClass(addedToFavoriteViewClass);
            }
            if ($propertyElement.hasClass(notAddedToFavoriteViewClass)) {
                $propertyElement.removeClass(notAddedToFavoriteViewClass);
            }
            $propertyElement.find('.property-label').find('span').html(addedButtonLabel);
        }
        else {
            if (!$propertyElement.hasClass(notAddedToFavoriteViewClass)) {
                $propertyElement.addClass(notAddedToFavoriteViewClass);
            }
            if ($propertyElement.hasClass(addedToFavoriteViewClass)) {
                $propertyElement.removeClass(addedToFavoriteViewClass);
            }
            $propertyElement.find('.property-label').find('span').html(notAddedButtonLabel);
        }
    }
    else {
        if (propertyRenderer.propertyName === "attendingBtn") {
            if (model.attending) {
                if (!$propertyElement.hasClass(attendingClass)) {
                    $propertyElement.addClass(attendingClass);
                }
                if ($propertyElement.hasClass(notAttendingClass)) {
                    $propertyElement.removeClass(notAttendingClass);
                }
            }
            else {
                if (!$propertyElement.hasClass(notAttendingClass)) {
                    $propertyElement.addClass(notAttendingClass);
                }
                if ($propertyElement.hasClass(attendingClass)) {
                    $propertyElement.removeClass(attendingClass);
                }
            }
        }
        else {
            if (propertyRenderer.propertyName === "allAttendingBtn") {
                $propertyElement.on("click", function () {
                    var $modal = $("#attendingStudentsModal"),
                        $attendingUsersDiv = null, $newDiv = null,
                        $modalContent = $modal.find('.modal-content');
                    $modalContent.html('');
                    $attendingUsersDiv = $('.model-property-allAttendingStudents.simple-collection-container.property-input-wrapper')
                    $newDiv = $attendingUsersDiv.clone(true);
                    //addClickHandlerForUser($newDiv);
                    $newDiv.removeAttr("style");
                    $newDiv.appendTo($modalContent);
                    $modal.modal();
                    $modal.modal('open');
                });

            }
        }
    }
}

function addClickHandlerForUser($usersContainer) {
    $usersContainer.find('li.model-view.collection-item').each(function(index,element) {
        $(element).click(function() {
            window.location = "/profile/" + $(element).data('currentValue').id;
        });
    })
}

MetaRenderer.addPresentationRendererOverride('plannedCourse', {
    name: 'ENUM',
    render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
        var defaultRenderer = MetaRenderer.getDefaultPresentationRenderer('ENUM'),
            className = 'course-type-view-' + model.courseType;
        defaultRenderer.render(model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer);
        if (propertyRenderer.propertyName === 'courseType') {
            $propertyElement.addClass(className);
        }
    }
});

MetaRenderer.addPresentationRendererOverride('plannedCourse', {
    name: 'HTML',
    render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
        var defaultRenderer = MetaRenderer.getDefaultPresentationRenderer('HTML'),
            $name = null;
        defaultRenderer.render(model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer);
        if (propertyRenderer.propertyName === 'name') {
            $name = $propertyElement.find('span');
            $name.attr('data-position', 'top');
            $name.attr('data-tooltip', model.name);
            if (!$name.hasClass('tooltipped')) {
                $name.addClass('tooltipped');
            }
        }
    }
});

MetaRenderer.addPresentationRendererOverride('plannedCourse', {
    name: 'STRING',
    render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
        var defaultRenderer = MetaRenderer.getDefaultPresentationRenderer('STRING');
        if (propertyRenderer.propertyName === 'ownerEmail') {
            if (model.showOwnerEmail !== null && model.showOwnerEmail === true) {
                defaultRenderer.render(model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer);
            }
        } else if (propertyRenderer.propertyName === 'ownerPhone') {
            if (model.showOwnerPhone !== null && model.showOwnerPhone === true) {
                defaultRenderer.render(model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer);
            }
        }
        else {
            defaultRenderer.render(model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer);
        }

        if (propertyRenderer.propertyName === 'ownerName') {
            $propertyElement.on('click', function() {
                window.location = '/profile/' + model.course.owner.id;
            });
        }
    }
});

EventsManager.getInstance().on(EventsManager.META_UPDATED, function (event) {

    $("#plannedCourse-plannedCourse-create").find('select').material_select();
    $("#plannedCourse-plannedCourse-update").find('select').material_select();

    registerDefaultBehaviour('#plannedCourse-plannedCourse-create', 'create');
    registerDefaultBehaviour('#plannedCourse-plannedCourse-update', 'update');


    initRecommendedPlannedCoursesCarousel();
    initPastPlannedCoursesCarousel();
    initTooltips();
    managePagination();

    $( "#addComment" ).on("click", function(event) {
        event.preventDefault();
        var fData = new FormData();
        fData.append("comment", app.htmlDecode($("#commentInput").val()));
        var pcId = $("#pcId").val(),
            errorMessage = MetaRenderer.i18nMessage("model.course.error.comment.empty"),
            errorTitle = MetaRenderer.i18nMessage("model.course.error.comment.empty.title");
        fData.append("pcId", pcId);

        $.ajax({
            url        : "/sessions/getComment",
            type       : 'POST',
            data       : fData,
            dataType   : 'json',
            processData: false, // Don't process the files
            contentType: false, // Set content type to false as jQuery will tell the server its a query string request
            success    : function (data, textStatus, jqXHR) {
                if (data.success) {
                    location.reload();
                }
                else {
                    app.errorNotify({title: errorTitle, message: errorMessage});
                }
            },
            error      : function (data, textStatus, jqXHR) {
                console.log("Error adding comment.");
            }
        });
    });

});

function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
}

function isRequestForFirstPage() {
    var pageRequestParam = getUrlParameter('page');
    return pageRequestParam === undefined || pageRequestParam === null || pageRequestParam === '1';
}

function managePagination() {
    var modelName = '',
        viewType = '',
        updateDescriptor = window.__updateDescriptor,
        $recordsStart = $('.records-start'),
        $recordsEnd = $('.records-end'),
        $totalRecords = $('.total-records'),
        offset = $('#relevantItemsNo').val();
    if (updateDescriptor) {
        modelName = window.__updateDescriptor.modelName;
        if (window.__updateDescriptor.viewType) {
            viewType = window.__updateDescriptor.viewType.$name;
        }
        if (modelName === 'plannedCourse' && viewType === 'INDEX_FRAGMENT') {
            //first page
            if (!isRequestForFirstPage()) {
                $recordsStart.text(Number($recordsStart.text()) + Number(offset));
                $recordsEnd.text(Number($recordsEnd.text()) + Number(offset));
            }
            $totalRecords.text(Number($totalRecords.text()) + Number(offset));
        }
    }
}

function initTooltips(){
    $('.tooltipped').tooltip();
}

/*function myFunction() {
    var placeHolder = document.getElementById("placeHolder").value;
    document.getElementById("commentInput").placeholder = placeHolder.value;
}*/

function initRecommendedPlannedCoursesCarousel() {
    var settings = {
            slidesToShow: 3,
            slidesToScroll: 3,
            adaptiveHeight: true,
            prevArrow       : '<button type="button" data-role="none" class="slick-prev" aria-label="Previous" tabindex="0" role="button">' + MetaRenderer.i18nMessage('carousel.previous.label') + '</button>',
            nextArrow       : '<button type="button" data-role="none" class="slick-next" aria-label="Next" tabindex="0" role="button">' + MetaRenderer.i18nMessage('carousel.next.label') + '</button>'
        },
        carousel = $('#recommendedPlannedCoursesCarousel');
    if (carousel) {
        $('#recommendedPlannedCoursesCarousel').slick(settings);
    }

}

function initPastPlannedCoursesCarousel() {
    var settings = {
            slidesToShow: 3,
            slidesToScroll: 3,
            adaptiveHeight: true,
            prevArrow       : '<button type="button" data-role="none" class="slick-prev" aria-label="Previous" tabindex="0" role="button">' + MetaRenderer.i18nMessage('carousel.previous.label') + '</button>',
            nextArrow       : '<button type="button" data-role="none" class="slick-next" aria-label="Next" tabindex="0" role="button">' + MetaRenderer.i18nMessage('carousel.next.label') + '</button>'
        },
        carousel = $('#pastPlannedCoursesCarousel');
    if (carousel) {
        $('#pastPlannedCoursesCarousel').slick(settings);
    }

}

function prevRecommendedItems(event) {
    event.stopPropagation();
    event.preventDefault();
}

function nextRecommendedItems(event) {
    event.stopPropagation();
    event.preventDefault();
}

MetaRenderer.addInputRendererOverride('plannedCourse', {
    name: 'SELECT',
    render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
        var defaultRenderer = MetaRenderer.getDefaultInputRenderer('SELECT');
        defaultRenderer.render(model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer);
        console.log($propertyElement);
        $propertyElement.on("app:dirtyChange",function(e,dirty,propertyName,currentValue,$sourceInput,sourceEvent){

            if(propertyName == "selfRegistration" )
            {
                if(currentValue == true)
                {
                    $('[data-model-property-name=registrationUser]').hide();
                    $('[data-model-property-name=registrationUser]').nearest('input').val('');
                    $('[data-model-property-name=registrationApprovalUser]').hide();
                    $('[data-model-property-name=registrationApprovalUser]').nearest('input').val('');

                    $('[data-model-property-name=signupStartDate]').show();
                    $('[data-model-property-name=signupEndDate]').show();
                }
                else
                {
                    $('[data-model-property-name=signupStartDate]').hide();
                    $('[data-model-property-name=signupStartDate]').nearest('input').val('');
                    $('[data-model-property-name=signupEndDate]').hide();
                    $('[data-model-property-name=signupEndDate]').nearest('input').val('');

                    $('[data-model-property-name=registrationUser]').show();
                    $('[data-model-property-name=registrationApprovalUser]').show();
                }
            }

        });
    }

});

function registerDefaultBehaviour(formId, operation) {
    var propertiesToHideOrDisplay = ['objective', 'teachingMode', 'selfRegistration', 'registrationUser', 'registrationApprovalUser', 'signupStartDate',
        'signupEndDate', 'generateParticipationCertificate', 'generatePromovationCertificate', 'addBackgroundToCertificate',
        'maxNrOfStudents', 'showTestResults', 'showTestCorrectAnswares', 'showFinalTestsResults',
        'sendTestResultNotification'];

    $('[data-model-property-name=maxNrOfStudents]')
        .find('#plannedCourse-plannedCourse-' + operation + '-maxNrOfStudents').prop('min', 0);
    hideFields(propertiesToHideOrDisplay, formId);

    $(formId + ' [data-model-property-name=advancedOptions]').on('click', function () {
        var advancedOptionsChecked = $(formId + ' [data-model-property-name=advancedOptions]')
            .find('#plannedCourse-plannedCourse-' + operation + '-advancedOptions').prop('checked');
        if (!advancedOptionsChecked) {
            hideFields(propertiesToHideOrDisplay, formId);
        }
        else {
            showFields(propertiesToHideOrDisplay, formId);
            displayOrHideRegistrationFields(formId, operation);
        }
    });
}

function displayOrHideRegistrationFields(formId, operation) {
    var selfRegistrationProperties = ['signupStartDate', 'signupEndDate'];
    var managedRegistrationProperties = ['registrationUser', 'registrationApprovalUser'];

    var selfRegisterValue = $(formId + ' [data-model-property-name=selfRegistration]')
        .find('#plannedCourse-plannedCourse-' + operation + '-selfRegistration').val();
    if (selfRegisterValue == 'true') {
        hideFields(managedRegistrationProperties, formId);
        $(formId + ' [data-model-property-name=registrationUser]')
            .find('#plannedCourse-plannedCourse-' + operation + '-registrationUser').val('');
        $(formId + ' [data-model-property-name=registrationApprovalUser]')
            .find('#plannedCourse-plannedCourse-' + operation + '-registrationApprovalUser').val('');
        showFields(selfRegistrationProperties, formId);
    }
    else {
        showFields(managedRegistrationProperties, formId);
        hideFields(selfRegistrationProperties, formId);
        $(formId + ' [data-model-property-name=signupStartDate]')
            .find('#plannedCourse-plannedCourse-' + operation + '-signupStartDate').val('');
        $(formId + ' [data-model-property-name=signupEndDate]')
            .find('#plannedCourse-plannedCourse-' + operation + '-signupEndDate').val('');
    }

}

function hideFields(arr, formId) {
    for (var i = 0; i < arr.length; i++) {
        $(formId + ' [data-model-property-name=' + arr[i] + ']').css('display', 'none');
    }

}

function showFields(arr, formId) {
    for (var i = 0; i < arr.length; i++) {
        $(formId + ' [data-model-property-name=' + arr[i] + ']').css('display', 'block');
    }
}

plannedCourseOnCUSTOMSuccess = function (event, $controller, action, responseData, jqXHR, pushState, dataType){
    if(action.uri == "/sessions/addItemToFavorites/{id}")     //add item to favorites button
    {
        var $addToFavoritesBtn = $controller.parent().parent().parent().nearest(".model-property-displayUsersAddedToFavorites"),
            propertyValues = null,
            usersAddedToFavoritesVal = responseData.usersAddedToFavorites + ' ' + MetaRenderer.i18nMessage("displayUsersAddedToFavorites.label"),
            $element = $controller.parent().parent().parent(),
            elementData = $element.data(),
            model = elementData.currentValue;

        if ($addToFavoritesBtn.length == 0) {
            $addToFavoritesBtn = $controller.parent().parent().parent().nearest(".model-property-usersAddedToFavorites");
        }
        propertyValues = $addToFavoritesBtn.nearest(".property-value")
        propertyValues.html(usersAddedToFavoritesVal);
        updateFavorites($element, responseData.addedToFavorite);
    }
    else if(action.uri == "/sessions/likeItem/{id}" || action.uri == "/sessions/dislikeItem/{id}" )
    {
        var currentItemLikesNumber = $(".model-property-itemLikeNumber").nearest(".property-value");
        currentItemLikesNumber.html(responseData.itemLikeNumber);

        var currentItemDislikeNumber = $(".model-property-itemDislikeNumber").nearest(".property-value");
        currentItemDislikeNumber.html(responseData.itemDislikeNumber);
    }
    else if(action.uri == "/sessions/attendItem/{id}")     //attending item
    {
        updateAttendingOnSuccess(responseData);
    }

}

function updateAttendingOnSuccess(responseData) {
    var usersAttendingVal = responseData.usersAttending + ' ' + MetaRenderer.i18nMessage("displayUsersAttending.label"),
        $element = $('.model-property-displayUsersAttending'),
        propertyValues = null;

    propertyValues = $element.find('span.property-value');
    propertyValues.html(usersAttendingVal);
    updateAttending($element, responseData.attending);
    showLoader();
    window.location.reload();
}

function buildFilter()
{
    var urlFilter = "?";

    //get button filters
    var arr= $(".active-filter");
    arr.each(function (index, elem) {
        if(urlFilter.indexOf($(elem).attr("filter")) == -1) {
            urlFilter += $(elem).attr("category") + "=" + $(elem).attr("filter") + "&";
        }
    });

    $('option[selected=selected]').each(function (index, elem) {
        if($(elem).attr("filter") != 'NONE') {
            urlFilter += $(elem).attr("category") + "=" + $(elem).attr("filter") + "&";
        }
    });

    $("[category=calendarFilter]").each(function (index, elem) {
        if($(elem).attr("filter") != 'NONE') {
            urlFilter += $(elem).attr("category") + "=" + $(elem).attr("filter") + "&" + 'filterDisplayVal=' + $(elem).val() + "&";
        }
    });

    if(urlFilter.endsWith("&") || urlFilter.endsWith("?"))
    {
        urlFilter = urlFilter.substr(0, urlFilter.length-1);
    }

    window.location = window.location.origin + window.location.pathname + urlFilter;
}

function filterByOption(elem)
{
    showLoader();
    $(elem).find('option[selected=selected]').removeAttr("selected");
    $(elem).find('option[value='+$(elem).val() + ']').attr("selected","selected");

    buildFilter();
}

/* search enter key binding */
$(document).ready(function() {
    $('#filterSearch').bind("enterKey",function(e){
        filterByElement("search");
    });
    $('#filterSearch').keyup(function(e){
        if(e.keyCode == 13)
        {
            $(this).trigger("enterKey");
        }
    });
});

/* my search enter key binding */
$(document).ready(function() {
    $('#filterMySearch').bind("enterKey",function(e){
        filterByElement("mysearch");
    });
    $('#filterMySearch').keyup(function(e){
        if(e.keyCode == 13)
        {
            $(this).trigger("enterKey");
        }
    });
});

/* clear search parameters */
function resetSearch()
{
    window.location = window.location.origin + window.location.pathname;
}

function filterByElement(elem)
{

    if(elem == "search")
    {
        var str = $("#filterSearch").val();
        if(str != undefined && str != "") {
            window.location = window.location.origin + window.location.pathname + "?search=" + str;
        }
    }
    else if (elem == "mysearch")
    {
        var str = $("#filterMySearch").val();
        if(str != undefined && str != "") {
            window.location = window.location.origin + window.location.pathname + "?mysearch=" + str;
        }
    }
    else
    {
        var obj = $(elem);

        if (!obj.hasClass("active-filter")) {
            obj.addClass("active-filter");
        }
        else {
            obj.removeClass("active-filter");
        }

        buildFilter();
    }
}

function parseQuery(search) {

    var args = search.substring(1).split('&');
    var arrParams = [];
    var i, arg, kvp, key, value;

    for (i=0; i < args.length; i++) {

        var argsParsed = {};
        arg = args[i];
        if (-1 === arg.indexOf('=')) {

            argsParsed[decodeURIComponent(arg).trim()] = true;
        }
        else {

            kvp = arg.split('=');
            key = decodeURIComponent(kvp[0]).trim();
            value = decodeURIComponent(kvp[1]).trim();
            argsParsed["key"]=key;
            argsParsed["value"]=value;
            arrParams.push(argsParsed);
        }
    }

    return arrParams;
}

function updateFilterLayout()
{
    var params=parseQuery(window.location.search);

    var calendarDate = [],
        calendarDisplayDate = [];
    for(var i = 0; i<params.length; i++)
    {
        var itemElem;
        if(params[i].key != 'calendarFilter' && params[i].key != 'filterDisplayVal')
        {
            itemElem = $("[category="+params[i].key+"][filter='"+params[i].value+"']");
            if(itemElem.hasClass("hidden-image"))
            {
                interestChangeImage(itemElem, itemElem.id , itemElem.attr('filter'), $('#pcFilterTagList'));
            }
            if(itemElem.prop("tagName") == "OPTION")
            {
                itemElem.attr("selected", "selected");
            }
            else if(itemElem.prop("tagName") == "A") {
                itemElem.addClass("active-filter");
            }
        }
        else
        {
            if (params[i].key == 'calendarFilter') {
                itemElem = $("[category=" + params[i].key + "][filter=NONE]");
                calendarDate.push(params[i].value);
            }
            else {
                calendarDisplayDate.push(params[i].value);
            }
        }
    }

    if(calendarDate.length == 2)
    {
        $("#calendarStart").val(calendarDisplayDate[0]);
        $("#calendarStart").attr("filter",calendarDate[0]);
        $("#calendarEnd").val(calendarDisplayDate[1]);
        $("#calendarEnd").attr("filter",calendarDate[1]);

        $("#startDateFilterId").val(calendarDisplayDate[0]);
        $("#endDateFilterId").val(calendarDisplayDate[1]);

    }
}

EventsManager.getInstance().on(EventsManager.HTML_REQUEST, function (event) {
    $('#dateOptions').material_select();
    $('#distanceOptions').material_select();
    $('#organizerOptions').material_select();

    updateFilterLayout();
});

pcDatePickerI18nObject = function () {
    var _datePickerI18nObject = {
        today: MetaRenderer.i18nMessage('datePicker.today'),
        clear: MetaRenderer.i18nMessage('datePicker.clear'),
        close: MetaRenderer.i18nMessage('datePicker.ok'),
        labelMonthNext: MetaRenderer.i18nMessage('datePicker.labelMonthNext'),
        labelMonthPrev: MetaRenderer.i18nMessage('datePicker.labelMonthPrev'),
        labelMonthSelect: MetaRenderer.i18nMessage('datePicker.labelMonthSelect'),
        labelYearSelect: MetaRenderer.i18nMessage('datePicker.today'),
        monthsFull: [
            MetaRenderer.i18nMessage('datePicker.monthsFull.january'),
            MetaRenderer.i18nMessage('datePicker.monthsFull.february'),
            MetaRenderer.i18nMessage('datePicker.monthsFull.march'),
            MetaRenderer.i18nMessage('datePicker.monthsFull.April'),
            MetaRenderer.i18nMessage('datePicker.monthsFull.may'),
            MetaRenderer.i18nMessage('datePicker.monthsFull.june'),
            MetaRenderer.i18nMessage('datePicker.monthsFull.july'),
            MetaRenderer.i18nMessage('datePicker.monthsFull.august'),
            MetaRenderer.i18nMessage('datePicker.monthsFull.september'),
            MetaRenderer.i18nMessage('datePicker.monthsFull.october'),
            MetaRenderer.i18nMessage('datePicker.monthsFull.november'),
            MetaRenderer.i18nMessage('datePicker.monthsFull.december')
        ],
        monthsShort: [
            MetaRenderer.i18nMessage('datePicker.monthsShort.january'),
            MetaRenderer.i18nMessage('datePicker.monthsShort.february'),
            MetaRenderer.i18nMessage('datePicker.monthsShort.march'),
            MetaRenderer.i18nMessage('datePicker.monthsShort.April'),
            MetaRenderer.i18nMessage('datePicker.monthsShort.may'),
            MetaRenderer.i18nMessage('datePicker.monthsShort.june'),
            MetaRenderer.i18nMessage('datePicker.monthsShort.july'),
            MetaRenderer.i18nMessage('datePicker.monthsShort.august'),
            MetaRenderer.i18nMessage('datePicker.monthsShort.september'),
            MetaRenderer.i18nMessage('datePicker.monthsShort.october'),
            MetaRenderer.i18nMessage('datePicker.monthsShort.november'),
            MetaRenderer.i18nMessage('datePicker.monthsShort.december')
        ],
        weekdaysFull: [
            MetaRenderer.i18nMessage('datePicker.weekdaysFull.sunday'),
            MetaRenderer.i18nMessage('datePicker.weekdaysFull.monday'),
            MetaRenderer.i18nMessage('datePicker.weekdaysFull.tuesday'),
            MetaRenderer.i18nMessage('datePicker.weekdaysFull.wednesday'),
            MetaRenderer.i18nMessage('datePicker.weekdaysFull.thursday'),
            MetaRenderer.i18nMessage('datePicker.weekdaysFull.friday'),
            MetaRenderer.i18nMessage('datePicker.weekdaysFull.saturday')
        ],
        weekdaysShort: [
            MetaRenderer.i18nMessage('datePicker.weekdaysShort.sunday'),
            MetaRenderer.i18nMessage('datePicker.weekdaysShort.monday'),
            MetaRenderer.i18nMessage('datePicker.weekdaysShort.tuesday'),
            MetaRenderer.i18nMessage('datePicker.weekdaysShort.wednesday'),
            MetaRenderer.i18nMessage('datePicker.weekdaysShort.thursday'),
            MetaRenderer.i18nMessage('datePicker.weekdaysShort.friday'),
            MetaRenderer.i18nMessage('datePicker.weekdaysShort.saturday')
        ],
        weekdaysLetter: [
            MetaRenderer.i18nMessage('datePicker.weekdaysLetter.sunday'),
            MetaRenderer.i18nMessage('datePicker.weekdaysLetter.monday'),
            MetaRenderer.i18nMessage('datePicker.weekdaysLetter.tuesday'),
            MetaRenderer.i18nMessage('datePicker.weekdaysLetter.wednesday'),
            MetaRenderer.i18nMessage('datePicker.weekdaysLetter.thursday'),
            MetaRenderer.i18nMessage('datePicker.weekdaysLetter.friday'),
            MetaRenderer.i18nMessage('datePicker.weekdaysLetter.saturday')
        ],
        timeAgo: {
            prefixAgo: MetaRenderer.i18nMessage('timeAgo.prefixAgo'),//"acum",
            prefixFromNow: MetaRenderer.i18nMessage('timeAgo.prefixFromNow'),//"peste",
            suffixAgo: MetaRenderer.i18nMessage('timeAgo.suffixAgo'),//"",
            suffixFromNow: MetaRenderer.i18nMessage('timeAgo.suffixFromNow'),//"",
            seconds: MetaRenderer.i18nMessage('timeAgo.seconds'),//"mai putin de un minut",
            minute: MetaRenderer.i18nMessage('timeAgo.minute'),//"un minut",
            minutes: MetaRenderer.i18nMessage('timeAgo.minutes'),//"%d minute",
            hour: MetaRenderer.i18nMessage('timeAgo.hour'),//"o ora",
            hours: MetaRenderer.i18nMessage('timeAgo.hours'),//"%d ore",
            day: MetaRenderer.i18nMessage('timeAgo.day'),//"o zi",
            days: MetaRenderer.i18nMessage('timeAgo.days'),//"%d zile",
            month: MetaRenderer.i18nMessage('timeAgo.month'),//"o lună",
            months: MetaRenderer.i18nMessage('timeAgo.months'),//"%d luni",
            year: MetaRenderer.i18nMessage('timeAgo.year'),//"un an",
            years: MetaRenderer.i18nMessage('timeAgo.years'),//"%d ani",
            more: MetaRenderer.i18nMessage('timeAgo.more'),//"o vesnicie",
            wordSeparator: MetaRenderer.i18nMessage('timeAgo.wordSeparator')//" "
        }
    };
    return _datePickerI18nObject;
}

function openDatesFilterModal(elem) {

    var obj = $(elem),
        $startDateFilter = $("#startDateFilterId"),
        $endDateFilter = $("#endDateFilterId"),
        $nextButton = $('#course-dates-modal-next'),
        options = $.extend(pcDatePickerI18nObject(), {
            hiddenName: true,
            format: app.g11n.lang.app.dateFormats.LONG_DATE.replace("MMMM", "mmmm")
                .replace("EEE", "ddd"),
            selectYears: false,
            selectMonths: false,
            firstDay: 1,
            closeOnSelect: true,
            onSet: function (context) {
                var currentValue = context.select || null;
                $(this)[0].$node.data('currentValue', currentValue);
            }
        });

    if (!obj.hasClass("active-filter")) {
        obj.addClass("active-filter");
    }
    else {
        obj.removeClass("active-filter");
    }

    $('#modalSelectFilterDates').modal({ dismissible: false}).modal('open');


    $startDateFilter.pickadate(options).on('change', function (e) {
        var startDate = this.value;
        var endDate = $endDateFilter.val();
        /* <![CDATA[ */
        if (startDate && endDate) {
            //  doOnOverviewDateChange(startDate, endDate);
        }
        /* ]]> */
    });

    $endDateFilter.pickadate(options).on('change', function (e) {
        var startDate = $startDateFilter.val();
        var endDate = this.value;
        /* <![CDATA[ */
        if (startDate && endDate) {
            //   doOnOverviewDateChange(startDate, endDate);
        }
        /* ]]> */
    });

    updateFilterLayout();
}

function ModalSelectFilterDates() {
    resetFields();
    buildFilter();
    showLoader();
}

function closeModal()
{
    $('#modalSelectFilterDates').modal('close');
    if($("#calendarBtn").hasClass("active-filter"))
    {
        $("#calendarBtn").removeClass("active-filter");
    }
}

function resetFields()
{
    $("#startDateFilterId").val("");
    $("#endDateFilterId").val("");

    $("#calendarStart").val("");
    $("#calendarStart").attr("filter","NONE");
    $("#calendarEnd").val("");
    $("#calendarEnd").attr("filter","NONE");

    if($("#calendarBtn").hasClass("active-filter"))
    {
        $("#calendarBtn").removeClass("active-filter");
    }
}

function setCalendarFilters(filterStartDate, filterEndDate, filterStartDateVal, filterEndDateVal)
{
    $("#calendarStart").val(filterStartDateVal);
    $("#calendarStart").attr("filter",filterStartDate);
    $("#calendarEnd").val(filterEndDateVal);
    $("#calendarEnd").attr("filter",filterEndDate);

}

function filterByPeriod() {
    var $filterStartDateInput = $("#startDateFilterId"),
        filterStartDate = $filterStartDateInput.data('currentValue'),
        $filterEndDateInput = $("#endDateFilterId"),
        filterEndDate = $filterEndDateInput.data('currentValue'),
        filterStartDateVal = $filterStartDateInput.val(),
        filterEndDateVal = $filterEndDateInput.val();
    if(filterStartDateVal != null && filterEndDateVal != null &&
        filterStartDateVal != "" && filterEndDateVal != "")
    {
        setCalendarFilters(filterStartDate, filterEndDate, filterStartDateVal, filterEndDateVal);
        $('#modalSelectFilterDates').modal('close');
        showLoader();
        buildFilter();
    }
    else
    {
        //app.notify(MetaRenderer.i18nMessage("calendar.enter.both.dates"), 2000, "red-text text-accent-1");
        app.errorNotify({
            title: app.g11n.lang.app.messages.error,
            message: MetaRenderer.i18nMessage("calendar.enter.both.dates")
        });
    }

}

function selectButtonToDisplay()
{
    $('#modalSelectOtherInterests').modal({ dismissible: false}).modal('open');
    $("#tagList").val(JSON.stringify([]));
    addLoader();

}

function closeModalSelectOtherInterests() {
    $('#modalSelectOtherInterests').modal('close');
}

function addInterestFilters() {
    var selectedTags = JSON.parse($("#tagList").val()),
        $container = $("#pcFilterOtherInterestsContainer");
    for(var i=0; i < selectedTags.length; i++)
    {
        var interest = $container.find(".img-selected[category=tagFilter][filter=" + selectedTags[i].id + "]");
        var interestParent = interest.parent();
        if(interestParent.hasClass("other-interest"))
        {
            interestParent.removeClass("other-interest");
        }
        if(!interest.hasClass("active-filter"))
        {
            interest.addClass("active-filter");
            if ($container.hasClass("hidden-container")) {
                $container.removeClass("hidden-container");
            }
        }
    }

    buildFilter();
}

function openPresentationForRecommendedItem(sessionId) {
    if (sessionId) {
        window.location = '/sessions/' + sessionId;
    }
}

function openPresentationForAttendedItem(sessionId) {
    if (sessionId) {
        window.location = '/sessions/' + sessionId;
    }
}

function addToFavoritesRecommendedItem(sessionId) {
    if (sessionId) {
        $.ajax({
            type : 'POST',
            url : "/sessions/addItemToFavorites/" + sessionId,
            data : {id: sessionId},
            cache : false,
            processData : false,
            contentType : "application/json",
            success: function(data){
                window.location.reload();
            },
            error : function(XMLHttpRequest, textStatus, errorThrown) {
                alert(MetaRenderer.i18nMessage('error.item.not.added.to.favorites'));
            }
        });
    }

}

function addToFavoritesAttendedItem(sessionId) {
    if (sessionId) {
        $.ajax({
            type : 'POST',
            url : "/sessions/addItemToFavorites/" + sessionId,
            data : {id: sessionId},
            cache : false,
            processData : false,
            contentType : "application/json",
            success: function(data){
                window.location.reload();
            },
            error : function(XMLHttpRequest, textStatus, errorThrown) {
                alert(MetaRenderer.i18nMessage('error.item.not.added.to.favorites'));
            }
        });
    }

}

function openPresentationForPastItem(sessionId) {
    if (sessionId) {
        window.location = '/sessions/' + sessionId;
    }
}


function addToFavoritesPastItem(sessionId) {
    if (sessionId) {
        $.ajax({
            type : 'POST',
            url : "/sessions/addItemToFavorites/" + sessionId,
            data : {id: sessionId},
            cache : false,
            processData : false,
            contentType : "application/json",
            success: function(data){
                window.location.reload();
            },
            error : function(XMLHttpRequest, textStatus, errorThrown) {
                alert(MetaRenderer.i18nMessage('error.item.not.added.to.favorites'));
            }
        });
    }

}
$("#emailBtn").on("click", function(){
    var pcId = $(this).data("modelUid");
    var $sendButton = $(".btn.send-email", $modal);
    var $modal = $("#sendMailModal");
    var $textField = $("textarea", $modal);

    $textField.val("");
    $modal.modal();
    $modal.modal('open');

    $sendButton.click(function () {
        var params = {};
        params.plannedCourseId = pcId;
        params.message = $textField.val().replace('\n','<br/>');
        if(params.message===null || params.message.match(/^ *$/) !== null) {
            app.notify(MetaRenderer.i18nMessage("error.no.text.in.email"), 2000, "red-text text-accent-1");
        }
        else{
            Request.send("/sessions/mail", params, function () {
                $modal.modal('close');
                hideLoader();
            }, 'text', 'POST');
            app.notify(MetaRenderer.i18nMessage("message.email.sent"), 2000, "green-text text-accent-1");
        }
        window.refreshContentMetadata();
    });
});

function plannedCourseOnUiUpdated(event, $element) {
    var elementData = $element.data(),
        model = elementData.currentValue,
        elementViewType = elementData.modelViewType,
        itemType=model.courseType;

    if (elementViewType === 'INDEX_FRAGMENT') {
        var typeClass = 'course-type-' + itemType,
            $typeProperty = $element.find('.model-property-courseType');

        if (!$typeProperty.hasClass(typeClass)) {
            $typeProperty.addClass(typeClass);
        }
        updateFavorites($element, model.addedToFavorite);
        updateAttending($element, model.attending);
    } else if(elementViewType === 'VIEW_FRAGMENT') {
        processItemTextAreaOnView('plannedCourse','courseDescription');
        processItemTextAreaOnView('plannedCourse','courseObjective');
        if(itemType !== "COURSE") {
            $('.row.item-objective').hide();
        }
    }
}

function updateFavorites($element, addedToFavorite) {
    var $addedToFavorite = $element.find('.model-property-addedToFavorite'),
        addedToFavoriteClass = 'favorite-added-item',
        notAddedToFavoriteClass = 'favorite-not-added-item',
        addedToFavoriteViewClass = 'favorite-added-item-view',
        notAddedToFavoriteViewClass = 'favorite-not-added-item-view',
        $favoriteButton,
        $favoriteButtonView,
        notAddedButtonLabel = MetaRenderer.i18nMessage('model.plannedCourse.property.addToFavoritesBtn.notAdded.label'),
        addedButtonLabel = MetaRenderer.i18nMessage('model.plannedCourse.property.addToFavoritesBtn.label');

    $favoriteButton = $element.find('.main-actions').find('.action-plannedCourse-custom');
    $favoriteButtonView =  $('.model-property-addToFavoritesBtn');
    if (addedToFavorite) {
        if (!$favoriteButton.hasClass(addedToFavoriteClass)) {
            $favoriteButton.addClass(addedToFavoriteClass);
        }
        if ($favoriteButton.hasClass(notAddedToFavoriteClass)) {
            $favoriteButton.removeClass(notAddedToFavoriteClass);
        }
        if (!$favoriteButtonView.hasClass(addedToFavoriteViewClass)) {
            $favoriteButtonView.addClass(addedToFavoriteViewClass);
        }
        if ($favoriteButtonView.hasClass(notAddedToFavoriteViewClass)) {
            $favoriteButtonView.removeClass(notAddedToFavoriteViewClass);
        }
        $favoriteButtonView.find('.property-label').find('span').html(addedButtonLabel);
    }
    else {
        if (!$favoriteButton.hasClass(notAddedToFavoriteClass)) {
            $favoriteButton.addClass(notAddedToFavoriteClass);
        }
        if ($favoriteButton.hasClass(addedToFavoriteClass)) {
            $favoriteButton.removeClass(addedToFavoriteClass);
        }
        if (!$favoriteButtonView.hasClass(notAddedToFavoriteViewClass)) {
            $favoriteButtonView.addClass(notAddedToFavoriteViewClass);
        }
        if ($favoriteButtonView.hasClass(addedToFavoriteViewClass)) {
            $favoriteButtonView.removeClass(addedToFavoriteViewClass);
        }
        $favoriteButtonView.find('.property-label').find('span').html(notAddedButtonLabel);
    }

}

function updateAttending($element, attending) {
    var $attending = $element.find('.model-property-attending'),
        attendingClass = 'attending-item-view ',
        notAttendingClass = 'not-attending-item-view',
        $attendingButton;

    $attendingButton =  $('.model-property-attendingBtn');
    if (attending) {
        if (!$attendingButton.hasClass(attendingClass)) {
            $attendingButton.addClass(attendingClass);
        }
        if ($attendingButton.hasClass(notAttendingClass)) {
            $attendingButton.removeClass(notAttendingClass);
        }
    }
    else {
        if (!$attendingButton.hasClass(notAttendingClass)) {
            $attendingButton.addClass(notAttendingClass);
        }
        if ($attendingButton.hasClass(attendingClass)) {
            $attendingButton.removeClass(attendingClass);
        }
    }

}

function interestChangeImage(obj, id, tagId, $includedTagsInput) {
    var bb = $(obj),
        tagsInputValue = $includedTagsInput.val(),
        includedTags = null;


    if( tagsInputValue == undefined || tagsInputValue == null || tagsInputValue == "") {
        includedTags = [];
    }
    else {
        includedTags = JSON.parse(tagsInputValue);
    }

    if (bb.hasClass("img-default")) {
        bb.removeClass("visible-image");
        bb.addClass("hidden-image");
        if (bb.parent().nearest(".img-selected").hasClass("hidden-image")) {
            bb.parent().nearest(".img-selected").removeClass("hidden-image");
            bb.parent().nearest(".img-selected").addClass("visible-image");
            bb.parent().nearest(".img-selected").addClass("active-filter");
        }
        includedTags.push({id:tagId})
    }
    else if (bb.hasClass("img-selected")) {
        bb.removeClass("visible-image");
        bb.addClass("hidden-image");
        if (bb.hasClass("active-filter")) {
            bb.removeClass("active-filter")
        }
        if (bb.parent().nearest(".img-default").hasClass("hidden-image")) {
            bb.parent().nearest(".img-default").removeClass("hidden-image");
            bb.parent().nearest(".img-default").addClass("visible-image");
        }
        //remove interest id from selected interests array
        includedTags = $.grep(includedTags, function(n) {
            return n.id != tagId;
        })
    }
    $includedTagsInput.val(JSON.stringify(includedTags));
}

function modalFilterInterestChangeImage(obj, id, tagId) {
    interestChangeImage(obj, id, tagId, $('#tagList'));
}

function plannedCourseFilterChangeImage(obj, id, tagId) {
    filterByElement(obj);
    interestChangeImage(obj, id, tagId, $('#pcFilterTagList'));
}

function courseFilterChangeImage(obj, id, tagId) {
    function courseFilterInterestChangeImage(obj, id, tagId) {
        var bb = $(obj),
            $includedTagsInput = $('#tagList'),
            includedTags = JSON.parse($includedTagsInput.val());
        if (bb.hasClass("img-default")) {
            bb.removeClass("visible-image");
            bb.addClass("hidden-image");
            if (bb.parent().nearest(".img-selected").hasClass("hidden-image")) {
                bb.parent().nearest(".img-selected").removeClass("hidden-image");
                bb.parent().nearest(".img-selected").addClass("visible-image")
            }
            includedTags.push({id:tagId})
        }
        else if (bb.hasClass("img-selected")) {
            bb.removeClass("visible-image");
            bb.addClass("hidden-image");
            if (bb.parent().nearest(".img-default").hasClass("hidden-image")) {
                bb.parent().nearest(".img-default").removeClass("hidden-image");
                bb.parent().nearest(".img-default").addClass("visible-image");
            }
            //remove interest id from selected interests array
            includedTags = $.grep(includedTags, function(n) {
                return n.id != tagId;
            })
        }
        $includedTagsInput.val(JSON.stringify(includedTags));
    }
    filterByElement(obj);
}

plannedCourseOnOPEN_PRESENTATIONSuccess = function (event, $controller, action, responseData, jqXHR, pushState, dataType){
    hideLoader();
    addLoader();
}

plannedCourseOnOPEN_EDIT_FORMSuccess = function (event, $controller, action, responseData, jqXHR, pushState, dataType){
    hideLoader();
    addLoader();
}

plannedCourseOnOPEN_CREATE_FORMSuccess = function (event, $controller, action, responseData, jqXHR, pushState, dataType){
    hideLoader();
    addLoader();
}

plannedCourseOnCREATESuccess = function (event, $controller, action, responseData, jqXHR, pushState, dataType){
    showLoader();
}

plannedCourseOnUPDATESuccess = function (event, $controller, action, responseData, jqXHR, pushState, dataType){
    showLoader();
}

function closeAttendingStudentsModal() {
    $('#attendingStudentsModal').modal('close');
}

function goToUserProfile(userId) {
    window.location = '/profile/' + userId;
}

function deleteItemComment(commentId) {
    debugger;
    var params = {};
    params.commentId = commentId;
    Request.send("/sessions/deleteComment", params, function () {
        hideLoader();
        app.notify(MetaRenderer.i18nMessage("model.plannedCourse.comment.delete.success"), 2000, "green-text text-accent-1");
        showLoader();
        window.location.reload();
    }, 'text', 'POST');
}