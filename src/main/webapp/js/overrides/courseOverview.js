MetaRenderer.addPresentationRendererOverride('courseOverview', {
    name: 'INTEGER',
    render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
        var defaultRenderer = MetaRenderer.getDefaultPresentationRenderer('INTEGER'),
            value = model[propertyRenderer.propertyName];
        if (propertyRenderer.propertyName === 'progress') {
            $propertyElement.append('<meter value="' + value + '" min="0" max="100"></meter>');
            $propertyElement.append('<span class="property-value"> ' + value + '%</span>');
        }
        else {
            defaultRenderer.render(model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer);
        }
    }
});

EventsManager.getInstance().on(EventsManager.HTML_REQUEST, function (event) {
    $(document).ready(function () {
        initDatesForOverview();
    });
});

function initDatesForOverview() {
    var startDateParam = getUrlParameter("sDate"),
        endDateParam = getUrlParameter("eDate"),
        $startDateOverview = $("#startDateOverview"),
        $endDateOverview = $("#endDateOverview");

    if (startDateParam != null) {
        $startDateOverview.val(startDateParam);
    }
    else {
        $startDateOverview.val(moment().subtract(1, 'months').format('D MMM, YYYY'));
    }

    if (endDateParam != null) {
        $endDateOverview.val(endDateParam);
    }
    else {
        $endDateOverview.val(moment().format('D MMM, YYYY'));
    }
    $startDateOverview.pickadate({
                                     hiddenName: true
                                 }).on('change', function (e) {
        var startDate = this.value;
        var endDate = $endDateOverview.val();
        /* <![CDATA[ */
        if (startDate && endDate) {
            doOnOverviewDateChange(startDate, endDate);
        }
        /* ]]> */
    });

    $endDateOverview.pickadate({
                                   hiddenName: true
                               }).on('change', function (e) {
        var startDate = $startDateOverview.val();
        var endDate = this.value;
        /* <![CDATA[ */
        if (startDate && endDate) {
            doOnOverviewDateChange(startDate, endDate);
        }
        /* ]]> */
    });
}

function doOnOverviewDateChange(startDate, endDate) {
    var $currentStartDate = $('#hiddenStartDateOverview'),
        $currentEndDate = $('#hiddenEndDateOverview');
    if( $currentStartDate.val() != startDate || $currentEndDate.val() != endDate ) {
        $currentStartDate.val(startDate);
        $currentEndDate.val(endDate);
        ModelStore.promiseOf('courseDetails', true).done(function () {
            this.remotePageRequest({courseId: 1, sDate: startDate, eDate: endDate}, null, false)
            .done(function (page) {
                if (page.content) {
                    var $details = $('[data-group-name="courseOverviewStatisticsDetails"]').find('.model-property-courseOverviewStatisticsDetails'),
                        $chart = $('[data-group-name="courseOverviewStatistics"]').find('.model-property-courseOverviewStatistics'),
                        $grid = $('[data-group-name="courseOverviews"]').find('.model-property-courseOverviews'),
                        model = page.content[0];

                    renderOverviewChartDetails(model, $details);
                    renderOverviewChart(model, $chart);
                    renderOverviewGrid(model, $grid);
                }
            });
        });
    }
}

function renderOverviewChartDetails(model, $propertyElement) {
    var value = model['courseOverviewStatisticsDetails'],
        coursesNo = value.coursesNo,
        studentsNo = value.studentsNo,
        averageTime = value.averageTime,
        spentTime = value.spentTime;
    $propertyElement.children().each(function (index, el) {
        el.remove();
    });
    $propertyElement.append('<li style="list-style: none;">' + coursesNo + '</li>');
    $propertyElement.append('<li style="list-style: none;">' + studentsNo + '</li>');
    $propertyElement.append('<li style="list-style: none;">' + averageTime + '</li>');
    $propertyElement.append('<li style="list-style: none;">' + spentTime + '</li>');
}

function renderOverviewChart(model, $propertyElement) {
    var courseOverviewStatistics = model['courseOverviewStatistics'], columnNames = [], columns = [],
        tooltips = [],
        names = [];

    $propertyElement.children().each( function(index,el) {
        el.remove();
    });

    columnNames.push(MetaRenderer.i18nMessage('model.plannedCourseStudent.completed')
                     .replace('{0}', courseOverviewStatistics.finished + '%'));
    columnNames.push(MetaRenderer.i18nMessage('model.plannedCourseStudent.started')
                     .replace('{0}', courseOverviewStatistics.learning + '%'));
    columnNames.push(MetaRenderer.i18nMessage('model.plannedCourseStudent.not.started')
                     .replace('{0}', courseOverviewStatistics.inactive + '%'));

    columns.push([columnNames[0], courseOverviewStatistics.finished]);
    columns.push([columnNames[1], courseOverviewStatistics.learning]);
    columns.push([columnNames[2], courseOverviewStatistics.inactive]);

    names.push(columnNames[0]);
    names.push(columnNames[1]);
    names.push(columnNames[2]);

    $propertyElement.data('chartSettings', {
        columns: columns,
        tooltips: tooltips,
        options: {data: {columns: columns, names: names}}
    });

    var chart = MetaRenderer.chartRenderer($propertyElement, 'pieChart');
    chart.flush();
}

function renderOverviewGrid(model,$propertyElement) {
    var propertyRenderer = {
            modelName: 'courseOverview',
            groupName: 'courseOverviews',
            propertyName: 'courseOverviews',
            propertyHolderModelName: 'courseDetails',
            viewName: '_default'},
        $targetParent = $('[data-group-name="courseOverviews"]').find('.properties-wrapper-content');
    $propertyElement.children().each(function (index, el) {
        el.remove();
    });
    MetaRenderer.gridRenderer($propertyElement, propertyRenderer, $targetParent, model);
}
