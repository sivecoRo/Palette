function genericNodeOnUiUpdated(event, $element) {
    var elementData = $element.data(),
        model = elementData.currentValue,
        modelName = elementData.modelName,
        elementViewName = elementData.modelViewName,
        elementViewType = elementData.modelViewType,
        elementIndexViewType = elementData.modelIndexViewType;
    if (elementViewType === 'INDEX_FRAGMENT' && !model.folder) {
        var $moreActions = $element.nearest('.more-actions'),
            $moreActionsController = $element.nearest('.more-actions-controller');
        $moreActions.find('.action-genericNode-delete').parent('li').remove();
        $moreActions.find('.action-importTest-open-create-form').parent('li').remove();
        if ($moreActions.children().length === 0) {
            $moreActionsController.hide();
        }
    }
}
