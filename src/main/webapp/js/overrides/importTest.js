MetaRenderer.addInputRendererOverride('importTest', {
    name: 'FILE',
    render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
        var defaultRenderer = MetaRenderer.getDefaultInputRenderer('FILE');
        defaultRenderer.render(model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer);
        $propertyElement.nearest('input[type=file]').attr('accept', '.xls');
    }
});

EventsManager.getInstance().on(EventsManager.META_UPDATED, function (event,element) {
    var $formWrapper = $('.form-wrapper');
    if( $formWrapper && $formWrapper.data('model-name') === 'importTest') {
        var $actionSelector = $('.other-actions').find('.action-importTest-update'),
            action = $actionSelector.data('action'),
        newAction = {
            modelName: 'genericNode',
            type:{
                associatedManagementAction:'LIST',
                name: 'LIST',
                requestMethod:'GET'
            },
            uri:''
        };
        action.forward = newAction;
    }
});

