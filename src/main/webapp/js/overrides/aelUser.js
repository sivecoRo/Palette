MetaRenderer.addInputRendererOverride('aelUser', {
    name: 'FILE',
    render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
        var defaultRenderer = MetaRenderer.getDefaultInputRenderer('FILE');
        defaultRenderer.render(model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer);
        $propertyElement.nearest('input[type=file]').attr('accept', '.xls,.csv');
    }
});

EventsManager.getInstance().on(EventsManager.META_UPDATED, function (event) {
    var formId = "#aelUser-aelUser-update";
    $(formId + ' [data-model-property-name=password ]').css('display', 'none');
    $(formId + ' [data-model-property-name=confirmPassword ]').css('display', 'none');

});


