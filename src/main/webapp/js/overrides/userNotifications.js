function onClickUserNotificationMenu() {
    showLoader();
}

function deleteNotif(elem) {
    $('#modalDeleteNotification #sourceId').val($(elem).parent().parent().attr("id"));
    $('#modalDeleteNotification').modal('open');
}

function noDeleteNotification() {
    $('#modalDeleteNotification #sourceId').modal('close');
    hideLoader();
}

function yesDeleteNotification() {
    var ids = [];
    ids.push($('#modalDeleteNotification #sourceId').val());
    showLoader();
    $.ajax({
        url: '/user_notifications/deleteMessages',
        type: "POST",
        data: JSON.stringify(ids),
        contentType: "application/json",
        success: function (data, textStatus, jqXHR) {
            window.location = '/user_notifications';
        },
        error: function (jqXHR, textStatus, errorThrown) {
            hideLoader();
            alert($(".messages [message-attr='user-notifications.delete.error']").text());
        }
    });
}

function addUserAvatar(avatarWrapper, avatarHolder) {
    $('.text.authentication-name').text('');
    MetaRenderer.renderAvatar(avatarWrapper,avatarHolder);
}