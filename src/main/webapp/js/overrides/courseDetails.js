MetaRenderer.addPresentationRendererOverride('courseDetails', {
    name: 'PIE_CHART',
    render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
        var courseDetailsStatistics = model[propertyRenderer.propertyName], columnNames = [], columns = [],
            tooltips = [],
            names = [];

        columnNames.push(MetaRenderer.i18nMessage('model.plannedCourseStudent.completed')
                         .replace('{0}', courseDetailsStatistics.finished + '%'));
        columnNames.push(MetaRenderer.i18nMessage('model.plannedCourseStudent.started')
                         .replace('{0}', courseDetailsStatistics.learning + '%'));
        columnNames.push(MetaRenderer.i18nMessage('model.plannedCourseStudent.not.started')
                         .replace('{0}', courseDetailsStatistics.inactive + '%'));

        columns.push([columnNames[0], courseDetailsStatistics.finished]);
        columns.push([columnNames[1], courseDetailsStatistics.learning]);
        columns.push([columnNames[2], courseDetailsStatistics.inactive]);

        names.push(columnNames[0]);
        names.push(columnNames[1]);
        names.push(columnNames[2]);

        $propertyElement.data('chartSettings', {
            columns: columns,
            tooltips: tooltips,
            options: {data: {columns: columns, names: names}}
        });

        var chart = MetaRenderer.chartRenderer($propertyElement, 'pieChart');
        chart.flush();

        $propertyElement.on("app:elementTransposed", function (event) {
            console.debug('FLUSH CHART');
            var chart = MetaRenderer.chartRenderer($(event.target), 'pieChart');
            chart.flush();
        });
    }
});

MetaRenderer.addPresentationRendererOverride('courseDetails', {
    name: 'MODEL_COLLECTION',
    render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
        var defaultRenderer = MetaRenderer.getDefaultPresentationRenderer('MODEL_COLLECTION'),
            value = model[propertyRenderer.propertyName],
            isNull = !value,
            isArray = !isNull && $.isArray(value),
            items = isNull ? [] : (isArray ? value : value.content),
            $defaultTemplate = MetaRenderer.defaultModelViewTemplate(propertyRenderer.modelName);
        if (propertyRenderer.propertyName === 'courseOverviews') {
            MetaRenderer.gridRenderer($propertyElement, propertyRenderer, $targetParent, model);
        } else if (propertyRenderer.propertyName === 'courseUsers') {
            simpleCourseUserCollectionRenderer($propertyElement, items, propertyRenderer, $targetParent, model);
        } else if (propertyRenderer.propertyName === 'courseLearningMaterials') {
            simpleCourseUserCollectionRenderer($propertyElement, items, propertyRenderer, $targetParent, model);
        } else if (propertyRenderer.propertyName === 'plannedCourses') {
            renderPlannedCoursesSelectInTabs( model, model.plannedCourses );
        }
        else {
            defaultRenderer.render(model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer);
        }
    }
});

MetaRenderer.addPresentationRendererOverride('courseDetails', {
    name: 'MODEL',
    render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
        var defaultRenderer = MetaRenderer.getDefaultPresentationRenderer('MODEL');
        if (propertyRenderer.propertyName === 'courseOverviewStatisticsDetails') {
            var value = model[propertyRenderer.propertyName];
            var coursesNo = value.coursesNo;
            var studentsNo = value.studentsNo;
            var averageTime = value.averageTime;
            var spentTime = value.spentTime;
            $propertyElement.append('<ul');
            $propertyElement.append('<li style="list-style: none;">' + coursesNo + '</li>');
            $propertyElement.append('<li style="list-style: none;">' + studentsNo + '</li>');
            $propertyElement.append('<li style="list-style: none;">' + averageTime + '</li>');
            $propertyElement.append('<li style="list-style: none;">' + spentTime + '</li>');
            $propertyElement.append('</ul>');
        } else {
            defaultRenderer.render(model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer);
        }
    }
});

EventsManager.getInstance().on(EventsManager.HTML_REQUEST, function (event) {
    $(document).ready(function () {
        initDatesForDetails();
        // $('select').material_select();
    });
});

function initDatesForDetails() {
    var startDateParam = getUrlParameter("sDate");
    var endDateParam = getUrlParameter("eDate");
    if (startDateParam != null) {
        $("#startDate").val(startDateParam);
    }
    else {
        $("#startDate").val(moment().subtract(1, 'months').format('D MMM, YYYY'));
    }

    if (endDateParam != null) {
        $("#endDate").val(endDateParam);
    }
    else {
        $("#endDate").val(moment().format('D MMM, YYYY'));
    }
    $('#startDate').pickadate({
                                  hiddenName: true
                              }).on('change', function (e) {
        var startDate = this.value;
        var endDate = $('#endDate').val();
        /* <![CDATA[ */
        if (startDate && endDate) {
            doOnDetailsDateChange(startDate, endDate);
        }
        /* ]]> */
    });

    $('#endDate').pickadate({
                                hiddenName: true
                            }).on('change', function (e) {
        var startDate = $('#startDate').val();
        var endDate = this.value;
        /* <![CDATA[ */
        if (startDate && endDate) {
            doOnDetailsDateChange(startDate, endDate);
        }
        /* ]]> */
    });
}

function doOnDetailsDateChange(startDate, endDate) {
    /* <![CDATA[ */
    window.location = '/courseDetails/?sDate=' + startDate + '&eDate=' + endDate;
    /* ]]> */
}

function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
}

function evaluateExpressionForCDModel(target, modelName, objectUID) {
    return target.replace("{modelName}", modelName)
    .replace("{objectUID}", objectUID);
}

function evaluateExpressionForCDModelPropertyAndView(target, modelName, propertyName, viewName,
                                                     defaultViewName) {
    return target.replace("{modelName}", modelName)
    .replace("{propertyName}", propertyName)
    .replace("{view}", viewName || '__default')
    .replace('.' + (defaultViewName || '__default'), "");
}

function evaluateCDRendererPlaceholderSelector(propertyName) {
    return evaluateExpressionForCDModelPropertyAndView('[data-renderer-placeholder-for={propertyName}]', null, propertyName, null);
}

function doRenderAllElementsFromOneModelStoreItemCD(modelName, modelUid, modelStore, $context, currentValue, extraPropertiesClasses, refRenderer) {
    if ($context && $context.length) {
        var model = currentValue || modelStore.get(modelUid),
            modelViewSelector = '.model-view[data-model-name={modelName}][data-meta-renderer=true][data-model-uid={objectUID}]',
            $elements = $context.find(evaluateExpressionForCDModel(modelViewSelector, modelName, modelUid));
        console.log("Render view", modelName, modelUid ? ('#' + modelUid) : null, "Html elements to update: ",
                    $elements.length);
        $elements.each(function (index, el) {
            try {
                var $element = $(el);
                console.debug('---> [htmlIndex:', index, ']');
                MetaRenderer.renderModelInstance($element, model, modelStore, refRenderer, extraPropertiesClasses);
            } catch (e) {
                console.error(e);
            }
        });

    }
}

function simpleCourseUserCollectionRenderer($wrapper, items, propertyRenderer, $targetParent, model) {
    var $collectionContainer = $('<ul class="property-value collection"></ul>'),
        modelViewSelector = '.model-view[data-model-name={modelName}][data-meta-renderer=true][data-model-uid={objectUID}]',
        propertyEmptyValue = 'model.{modelName}.property.{propertyName}.{view}.emptyValueOrCollection.label',
        $defaultTemplate = MetaRenderer.defaultModelViewTemplate(propertyRenderer.modelName, null, 'li'),
        $viewWrapper = $targetParent.closest(evaluateExpressionForCDModel(modelViewSelector, propertyRenderer.propertyHolderModelName, model.objectUID)),
        $template = MetaRenderer.findModelViewRendererTemplate($viewWrapper, propertyRenderer.modelName, null, $defaultTemplate),
        $placeholders = $viewWrapper.find(evaluateCDRendererPlaceholderSelector(propertyRenderer.propertyName)),
        $emptyCollection = $('<li class="collection-item empty-collection-wrapper"></li>'),
        emptyMessageCode = evaluateExpressionForCDModelPropertyAndView(propertyEmptyValue, propertyRenderer.propertyHolderModelName, propertyRenderer.propertyName, propertyRenderer.viewName),
        rendererType = $wrapper.data('rendererType') || MetaRenderer.defaultSimpleCollectionSimpleRendererType();
    if (items) {
        if (items.length == 0) {
            var defaultItem = {
                id: 0,
                modelName: 'courseUser',
                new: false,
                objectLabel: emptyMessageCode,
                objectUID: 0,
                userResourceStatistics: null,
                userResourceStatisticsDetails: null,
                userResources: null
            };
            items.push(defaultItem);
        }
        $.each(items, function (index) {
            var item = $(this)[0],
                $item = $template.clone().removeClass('renderer-template');
            $item.attr("data-model-uid", item.objectUID);
            $item.addClass('collection-item');
            if (index == 0) {
                $item.addClass('active');
            }
            $item.data({
                           currentValue: item//,
                           //rendererType: rendererType
                       });
            //$item.attr("data-renderer-type", rendererType);
            $item.click(function (e) {
                $item.parent().find('.collection-item.active').removeClass('active');
                $item.addClass('active');
                MetaRenderer.transposeToPlaceholders($placeholders, $(this));
            });
            $collectionContainer.append($item);
        });
    }
    ModelStore.promiseOf(propertyRenderer.modelName, true).done(function () {
        var modelStore = this, transposedProperties = [], extraPropertiesClasses = {};
        $placeholders.each(function () {
            var transposedProperty = $(this).data('rendererPlaceholderProperty');
            transposedProperties.push(transposedProperty);
        });
        $.each(transposedProperties, function (index, transposedProperty) {
            extraPropertiesClasses[transposedProperty] = ['element-transposed'];
        });
        if (items) {
            $.each(items, function (index) {
                var item = $(this)[0];
                doRenderAllElementsFromOneModelStoreItemCD(propertyRenderer.modelName, item.objectUID, modelStore, $collectionContainer, item, extraPropertiesClasses);
            });
        }
        $collectionContainer.children('.active').first().click();
    });
    $emptyCollection.append('<div class="empty-collection"><div>' + MetaRenderer.i18nMessage(emptyMessageCode) + '</div></div>');
    $collectionContainer.prepend($emptyCollection);
    if (items.length === 0) {
        $emptyCollection.show();
    }
    else {
        $emptyCollection.hide();
    }
    $wrapper.append($collectionContainer);
    $wrapper.addClass('simple-collection-container');
}

function renderPlannedCoursesSelectInTabs(model, items) {

    renderSelect(model, items, '#resourceUserPlannedCoursesSelect');
    renderSelect(model, items, '#userResourcePlannedCoursesSelect');
}
function renderSelect(model, items, elementId) {
    var $select = $(elementId),
        $firstOption = $select.find('option').first();

    $select.empty()
    .append($firstOption);

    if (items) {
        $.each(items, function (index) {
            var item = $(this)[0];
            $option = $('<option value="' + item.id + '">' + item.reportLabel + '</option>');
            $select.append($option);
        });
    }
    // $select.material_select();

    $select.on('contentChanged', function () {
        // $(this).material_select();
    });
    $select.on('change', function () {
        filterCourseUsers(model);
    });
    $select.data("courseDetails", model);
}

function doOnUsersSearchClick() {
    var $select = $('#userResourcePlannedCoursesSelect');
    if ($select) {
        filterCourseUsers($select.data("courseDetails"));
    }
}

function filterCourseUsers(model) {
    var $selectedUser = $('#users').find('.collection-item.active'),
        selectedUserData = $selectedUser.data(),
        selectedUserId = selectedUserData == null ? null : selectedUserData.modelUid,
        plannedCourseId = $('#userResourcePlannedCoursesSelect').val(),
        $collectionContainer = $('.property-value.collection'),
        $usersSearch = $('#usersSearch');

    ModelStore.promiseOf('courseUser', true).done(function () {
        var modelStore = this, transposedProperties = [], extraPropertiesClasses = {},
            $wrapper = $('.model-property-courseUsers'),
            propertyRenderer = {
                propertyName: 'courseUsers',
                viewName: '_default',
                propertyHolderModelName: 'courseDetails',
                modelName: 'courseUser'
            };

        this.remotePageRequest({
                                   id: selectedUserId, courseId: model.objectUID, plannedCourseId: plannedCourseId,
                                   courseUserName: $usersSearch.val()
                               }, null, false)
        .done(function (page) {
            if (page.content) {
                var collectionItems = $collectionContainer.find('.collection-item');
                $.each(collectionItems, function (index, collectionItem) {
                    collectionItem.remove();
                });
                simpleCourseUserCollectionRenderer($wrapper, page.content, propertyRenderer, $collectionContainer, model);
                $('.collection').first().remove();
            }
        });
    });
}

function doOnActivitiesSearchClick() {
    var $select = $('#resourceUserPlannedCoursesSelect');
    if( $select ) {
        filterCourseLearningMaterials( $select.data("courseDetails") );
    }
}

function filterCourseLearningMaterials(model) {
    var $selectedLearningMaterial = $('#activities').find('.collection-item.active'),
        selectedData = $selectedLearningMaterial.data(),
        selectedUserId = selectedData == null ? null : selectedData.modelUid,
        plannedCourseId = $('#resourceUserPlannedCoursesSelect').val(),
        $collectionContainer = $('.property-value.collection'),
        $activitiesSearch = $('#activitiesSearch');

    ModelStore.promiseOf('courseUser', true).done(function () {
        var modelStore = this, transposedProperties = [], extraPropertiesClasses = {},
            $wrapper = $('.model-property-courseUsers'),
            propertyRenderer = {
                propertyName: 'courseLearningMaterials',
                viewName:'_default',
                propertyHolderModelName: 'courseDetails',
                modelName:'courseLearningMaterials'
            };

        this.remotePageRequest({id: selectedUserId, courseId: model.objectUID, plannedCourseId: plannedCourseId, learningMaterialName: $activitiesSearch.val()}, null, false)
        .done(function (page) {
            if (page.content) {
                var collectionItems = $collectionContainer.find('.collection-item');
                $.each( collectionItems, function(index, collectionItem) {
                    collectionItem.remove();
                });
                simpleCourseUserCollectionRenderer($wrapper,page.content,propertyRenderer,$collectionContainer,model);
                $('.collection').first().remove();
            }
        });
    });
}

function courseDetailsOnUiUpdated(event, $element) {
    var elementData = $element.data(),
        model = elementData.currentValue,
        modelName = elementData.modelName,
        elementViewName = elementData.modelViewName,
        elementViewType = elementData.modelViewType,
        elementIndexViewType = elementData.modelIndexViewType;
    if (elementViewType === 'INDEX_FRAGMENT') {
        var $mainActions = $element.nearest('.main-actions');
        $mainActions.find('.generic-action').removeClass('btn').addClass('btn-flat');
    }
}
