MetaRenderer.addInputRendererOverride('plannedNotification', {
    name: 'SELECT',
    render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
        var defaultRenderer = MetaRenderer.getDefaultInputRenderer('SELECT');
        defaultRenderer.render(model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer);
        var $select = $propertyElement.nearest('select.property-input');
        if (propertyRenderer.propertyName === 'userMessageFormat') {
            $select.children('option').each(function (option) {
                var $option = $(this), optionData = $option.data('selectValue');
                if (!( $option.hasClass('hint-option') || ( optionData != null && !optionData.application ) )) {
                    $option.remove();
                }
            });
        }
    }

});

EventsManager.getInstance().on(EventsManager.META_UPDATED, function (event) {
    $(document).ready(function () {
        customizeForm();
    });
});

function customizeForm() {
    var $notificationType = $('[data-model-property-name=notificationEntityType]').find('select');
    var $session = $('[data-model-property-name=plannedCourse]');
    var $sendByEvent = $('[data-model-property-name=sendByEvent]').find('input');

    manageSession($notificationType.val());
    $notificationType.on('change', function () {
        manageSession($notificationType.val());
    });

    manageEventAndDate($sendByEvent.prop('checked'));
    $sendByEvent.on('change', function () {
        manageEventAndDate($sendByEvent.prop('checked'));
    });
}

function manageSession(notificationType) {
    var $session = $('[data-model-property-name=plannedCourse]');
    if (notificationType != null && notificationType == 'PLANNED_COURSE') {
        $session.show();
    }
    else {
        $session.hide();
        $session.val(null);
    }
}

function manageEventAndDate(sendByEvent) {
    var $event = $('[data-model-property-name=notificationEventType]');
    var $date = $('[data-model-property-name=sendDate]');
    var $daysNo = $('[data-model-property-name=daysNo]');
    var $hoursNo = $('[data-model-property-name=hoursNo]');
    var $minutesNo = $('[data-model-property-name=minutesNo]');
    if (sendByEvent != null && sendByEvent == true) {
        $event.show();
        $daysNo.show();
        $hoursNo.show();
        $minutesNo.show();
        $date.hide();
        $date.find('input').val(null);
    }
    else {
        $event.find('input').val(null);
        $event.hide();
        $daysNo.find('input').val(null);
        $daysNo.hide();
        $hoursNo.find('input').val(null);
        $hoursNo.hide();
        $minutesNo.find('input').val(null);
        $minutesNo.hide();
        $date.show();
    }
}
