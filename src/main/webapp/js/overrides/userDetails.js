var userDetailsCitiesAutocomplete;

MetaRenderer.addInputRendererOverride('userDetails', {
    name: 'IMAGE_UPLOAD',
    render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
        var defaultRenderer = MetaRenderer.getDefaultInputRenderer('IMAGE_UPLOAD');
        defaultRenderer.render(model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer);
        var $button = $propertyElement.nearest('.btn'),
            $buttonLabel = $button.children('span'),
            $filePathWrapper = $propertyElement.nearest('.file-path-wrapper'),
            $fileInput = $button.children('input[type=file]'),
            imageSrc = model.imageSrc,
            $deleteProfilePhoto = $('#userDetailsDeleteProfilePhoto');
        $fileInput.attr('accept', '.jpg,.jpeg,.png');
        $button.removeClass('btn').addClass('hidden-upload-button');
        $buttonLabel.addClass('center-icon');
        $filePathWrapper.hide();
        if (imageSrc) {
            $propertyElement.css('background-image', 'url(' + imageSrc + ')');
            if (imageSrc !== model.defaultImage) {
                $deleteProfilePhoto.show();
            }
        }
        $propertyElement.on('app:dirtyChange', function (e, dirty, propertyName, currentValue, $sourceInput, sourceEvent) {
            var imageSrc = URL.createObjectURL(currentValue[0]);
            if (imageSrc) {
                $propertyElement.css('background-image', 'url(' + imageSrc + ')');
                if (imageSrc !== model.defaultImage) {
                    $deleteProfilePhoto.show();
                }
            }
        });
    }
});

MetaRenderer.addInputRendererOverride('userDetails', {
    name: 'TEXT',
    render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
        var defaultRenderer = MetaRenderer.getDefaultInputRenderer('TEXT');
        defaultRenderer.render(model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer);
        $propertyElement.on('app:dirtyChange', function (e, dirty, propertyName, currentValue, $sourceInput, sourceEvent) {
            if (propertyName != 'city_name') {
                updateSwitchInputValue(propertyName, currentValue);
            }
            else {
                if (currentValue == null) {
                    initializeAutocomplete();
                    updateCity(null,null);
                }
            }
        });
        if (propertyRenderer.propertyName == 'city_name') {
            $propertyElement.find('input').off('mouseout');
        }
    }
});

MetaRenderer.addInputRendererOverride('userDetails', {
    name: 'SELECT',
    render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
        var defaultRenderer = MetaRenderer.getDefaultInputRenderer('SELECT');
        defaultRenderer.render(model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer);
        if (propertyRenderer.propertyName == 'gender') {
            $propertyElement.on('app:dirtyChange', function (e, dirty, propertyName, currentValue, $sourceInput, sourceEvent) {
                updateSwitchInputValue(propertyName, $sourceInput.children('option:selected').text());
            });
        }
    }
});

MetaRenderer.addInputRendererOverride('userDetails', {
    name: 'DATE',
    render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
        var defaultRenderer = MetaRenderer.getDefaultInputRenderer('DATE'),
            options = {selectYears: 60, selectMonths: true, min: new Date(1920,0,0,0,0,0,0), max: new Date(1980,11,31,0,0,0,0)};
        defaultRenderer.render(model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer, options);
        if (propertyRenderer.propertyName == 'birthDate') {
            $propertyElement.on('app:dirtyChange', function (e, dirty, propertyName, currentValue, $sourceInput, sourceEvent) {
                updateSwitchInputValue(propertyName, $('#userDetails-userDetails-update-birthDate').val());
            });
            $propertyElement.on("app:propertyAppended", function () {
                $propertyElement.find('input').val(model['displayBirthDate']);
            });
        }
    }
});

MetaRenderer.addInputRendererOverride('userDetails', {
    name: 'LIST_OF_VALUES',
    render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
        var defaultRenderer = MetaRenderer.getDefaultInputRenderer('LIST_OF_VALUES');
        defaultRenderer.render(model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer);
        if (propertyRenderer.propertyName == 'country') {
            $propertyElement.on('app:dirtyChange', function (e, dirty, propertyName, currentValue, $sourceInput, sourceEvent) {
                if (currentValue) {
                    updateSwitchInputValue(propertyName, currentValue.countryName);
                }
                else {
                    updateSwitchInputValue(propertyName, currentValue);
                }
                initializeAutocomplete();
            });
        }
    }
});

MetaRenderer.addInputRendererOverride('userDetails', {
    name: 'LIST_OF_VALUES_MULTI_SELECT',
    render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
        var defaultRenderer = MetaRenderer.getDefaultInputRenderer('LIST_OF_VALUES_MULTI_SELECT');
        defaultRenderer.render(model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer);
        $propertyElement.hide();
    }
});


MetaRenderer.addInputRendererOverride('userDetails', {
    name: 'CHECKBOX',
    render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
        var defaultRenderer = MetaRenderer.getDefaultInputRenderer('CHECKBOX'),
            $switchContainer = $('<div class="switch-container col s12 m2 l2"></div>'),
            $switchLabel = $('<label class="switch switch-green middle"></label>'),
            $switchInput = $('<input type="checkbox" class="switch-input"/>'),
            $valueInput = $('<input class="property-input validate valid col s12 m8 l8" type="TEXT" name="' + propertyRenderer.propertyName + '" disabled="true" id="summaryProfile' + propertyRenderer.propertyName + '">'),
            $switchLabelSpan = $('<span class="switch-label" data-on="' + MetaRenderer.i18nMessage("create.profile.show") + '" data-off="' + MetaRenderer.i18nMessage("create.profile.hide") + '"></span>'),
            $switchHandle = $('<span class="switch-handle"></span>'),
            $interestsDiv = $('<div id="selectedInterestsDiv"></div>');

        if (propertyRenderer.propertyName == 'sendEmail' || propertyRenderer.propertyName == 'showAttendingItems') {
            defaultRenderer.render(model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer);
        }
        else {

            $switchInput.attr('property-name', propertyRenderer.propertyName);
            if (model[propertyRenderer.propertyName] == true) {
                $switchInput.attr('checked', "checked");
            }
            $switchInput.change(function (e) {
                var $target = $(e.currentTarget),
                    value = false;
                if ($(this).is(":checked")) {
                    value = true;
                }
                $propertyElement.trigger('app:dirtyChange', [true, $propertyElement.data('modelPropertyName'), value, $target, e]);
            });
            if (propertyRenderer.orderInForm % 2 == 0 || propertyRenderer.propertyName == '') {
                $switchContainer.addClass('float-left');
            }
            else {
                $switchContainer.addClass('float-right');
            }
            $switchLabel.append($switchInput);
            $switchLabel.append($switchLabelSpan);
            $switchLabel.append($switchHandle);
            $switchContainer.append($switchLabel);
            $propertyElement.append($switchContainer);
            if (propertyRenderer.propertyName == 'show_interests') {
                $propertyElement.append($interestsDiv);
            }
            else {
                $propertyElement.append($valueInput);
            }
            $propertyElement.removeClass();
        }
    }
});

function getSwitchInputValue(model, propertyName) {
    var value = null;
    if (propertyName == 'show_city') {
        if (model.city_name == undefined || model.city_name == null || model.city_name.length == 0) {
            value = MetaRenderer.i18nMessage("property.city_name.placeholder");
        }
        else {
            value = model.city_name;
        }
    } else if (propertyName == 'show_country') {
        if (model.country == undefined || model.country == null ) {
            value = MetaRenderer.i18nMessage("property.country.placeholder");
        }
        else {
            value = model.country.countryName;
        }
    } else if (propertyName == 'show_phone') {
        if (model.phoneNo == undefined || model.phoneNo == null || model.phoneNo.length == 0) {
            value = MetaRenderer.i18nMessage("property.phoneNo.placeholder");
        }
        else {
            value = model.phoneNo;
        }
    } else if (propertyName == 'show_gender') {
        if (model.gender == undefined || model.gender == null || model.gender.length == 0) {
            value = MetaRenderer.i18nMessage("property.gender.placeholder");
        }
        else {
            value = $('#userDetails-userDetails-update-gender option:selected').text();
        }
    } else if (propertyName == 'show_email') {
        if (model.email == undefined || model.email == null || model.email.length == 0) {
            value = MetaRenderer.i18nMessage("property.email.placeholder");
        }
        else {
            value = model.email;
        }
    } else if (propertyName == 'show_birth_date') {
        if (model.birthDate == undefined || model.birthDate == null || model.birthDate.length == 0) {
            value = MetaRenderer.i18nMessage("property.birthDate.placeholder");
        }
        else {
            value = $('#userDetails-userDetails-update-birthDate').val();
        }
    }
    return value;
}

function setSwitchInputValue(propertyName, val) {
    var $input = $('#summaryProfile' + propertyName);
    if (val == undefined || val == null || val.length == 0) {
        val = MetaRenderer.i18nMessage("property." + propertyName + ".placeholder");
    }
    $input.val(val);
}

function updateSwitchInputValue(propertyName, val) {
    if (propertyName == 'city_name') {
        setSwitchInputValue('show_city',val);
    } else if (propertyName == 'country') {
        setSwitchInputValue('show_country',val);
    } else if (propertyName == 'phoneNo') {
        setSwitchInputValue('show_phone',val);
    } else if (propertyName == 'gender') {
        setSwitchInputValue('show_gender',val);
    } else if (propertyName == 'email') {
        setSwitchInputValue('show_email',val);
    } else if (propertyName == 'birthDate') {
        setSwitchInputValue('show_birth_date',val);
    }
}

function userDetailsChangeImage(obj, id, tagId) {
    var bb = $(obj),
        $includedTagsInput = $('#userDetails-userDetails-update-includedTags'),
        includedTags = $includedTagsInput.data().currentValue;
    if (bb.hasClass("img-default")) {
        bb.removeClass("visible-image");
        bb.addClass("hidden-image");
        if (bb.parent().nearest(".img-selected").hasClass("hidden-image")) {
            bb.parent().nearest(".img-selected").removeClass("hidden-image");
            bb.parent().nearest(".img-selected").addClass("visible-image")
        }
        includedTags.push({id:tagId})
    }
    else if (bb.hasClass("img-selected")) {
        bb.removeClass("visible-image");
        bb.addClass("hidden-image");
        if (bb.parent().nearest(".img-default").hasClass("hidden-image")) {
            bb.parent().nearest(".img-default").removeClass("hidden-image");
            bb.parent().nearest(".img-default").addClass("visible-image");
        }
        //remove interest id from selected interests array
        includedTags = $.grep(includedTags, function(n) {
            return n.id != tagId;
        })
    }
    $includedTagsInput.data('currentValue',includedTags);
    $includedTagsInput.val(includedTags).change();
    updateSummaryInterests();
}

function userDetailsOnUiUpdated(event, $element) {
    var model = $element.data('currentValue'),
        $switchInputs = $('.switch-input'),
        propertyName;
    $.each($switchInputs, function(index,input) {
        propertyName = $(input).attr('property-name');
        if (propertyName != 'show_interests') {
            setSwitchInputValue(propertyName, getSwitchInputValue(model, propertyName));
        }
    });
    updateSummaryInterests();
    showHideFields(model);
    updateForDeleteUserPicture();
}

function updateForDeleteUserPicture() {
    var deleteUserPicture = window.sessionStorage.getItem("deleteUserPicture"),
        tabs = $('.tabs');
    if (deleteUserPicture === "true") {
        tabs.tabs('select_tab', 'personalProfile');
        window.sessionStorage.setItem("deleteUserPicture",false);
    }
}

function showHideFields(model) {
    if (model.show_city == false) {
        $('.user-details-city-row').hide();
    }

    if (model.show_country == false) {
        $('.user-details-country-row').hide();
    }

    if (model.show_email == false) {
        $('.user-details-email-row').hide();
    }

    if (model.show_phone == false) {
        $('.user-details-phone-row').hide();
    }

    if (model.show_interests == false) {
        $('.user-details-interests-row').hide();
    }
}

EventsManager.getInstance().on(EventsManager.META_UPDATED, function (event) {
    initUserPlannedCoursesCarousel();
});

function updateSummaryInterests(){

    var selectedInterestsElements = $( ".img-selected.visible-image" );
    $("#selectedInterestsDiv").empty();

    for(i = 0; i < selectedInterestsElements.length; i++) {
        $("#selectedInterestsDiv").append("<div class='s12 summary-interests'><div class='selectedInterestsDivElement'><img id='profile-summary-interests' src='"+selectedInterestsElements[i].src+"'/><h6 id='profile-summary-interests-label'>"+selectedInterestsElements[i].getAttribute("tagValue")+"</h6></div></div>");
        $("#selectedInterestsDiv").css('text-align', 'center');
    }

    if(selectedInterestsElements.length == 0)
    {
        $("#selectedInterestsDiv").append("<div id='noInterestsSelected'>" + MetaRenderer.i18nMessage("userDetails.no.interests") + "</div>");
    }
}

function initializeAutocomplete()
{
    var $country = $('#userDetails-userDetails-update-country'),
        $city = $('#userDetails-userDetails-update-city_name'),
        selectedCountryCode,
        options;
    if ($country && $country.data('currentValue')) {
        selectedCountryCode = $country.data('currentValue').code;
        options = {
            types: ['geocode'],
            componentRestrictions: {country: selectedCountryCode}
        };
    }
    else {
        options = {
            types: ['geocode']
        };
        $city.val(null);
        updateCity(null,null);
    }
    initGeneralAutocomplete(options);
}

function initGeneralAutocomplete(options) {
    var cityElement = document.getElementById('userDetails-userDetails-update-city_name');
    var courseCreateLocationElement = document.getElementById('course-course-create-itemLocation'),
        courseUpdateLocationElement = document.getElementById('course-course-update-itemLocation'),
        courseElement = null;

    if (cityElement !== null) {
        $(cityElement).on('keypress', function ()  {
            updateCity(null,null);
        });
        userDetailsCitiesAutocomplete = new google.maps.places.Autocomplete(
            (cityElement), options);
        userDetailsCitiesAutocomplete.addListener('place_changed', onUDCityChange);
    }
    else {
        if (courseCreateLocationElement !== null || courseUpdateLocationElement !== null) {
            if (courseCreateLocationElement !== null) {
                courseElement = courseCreateLocationElement;
            }
            else {
                courseElement = courseUpdateLocationElement;
            }
            autocomplete = new google.maps.places.Autocomplete(
                courseElement, options);
            autocomplete.addListener('place_changed', fillInCountry);
        }
    }
}

function updateCity(name,coordinates) {
    var $form = $('.form-wrapper');
    if ($form && $form.length > 0) {
        var formData = $form.data('currentValue');
        if (formData) {
            formData.city_name = name;
            formData.city = coordinates;
            updateSwitchInputValue('city_name', name);
        }
    }
}

function onUDCityChange(){
    var $form = $('.form-wrapper');
    if (userDetailsCitiesAutocomplete) {
        var place = userDetailsCitiesAutocomplete.getPlace(),
            coordinates;
        if (place && place.geometry) {
            coordinates = place.geometry.location.lat() + ";" + place.geometry.location.lng();
            //updateSwitchInputValue('city_name', userDetailsCitiesAutocomplete.getPlace().formatted_address);
            updateCity(userDetailsCitiesAutocomplete.getPlace().formatted_address, coordinates);
        }
        else {
            var formData = $form.data('currentValue');
            if (formData) {
                formData.city_name = null;
                formData.city = null;
                //updateSwitchInputValue('city_name', null);
                updateCity(null, null);
            }
        }
    }
}

// Show `Delete Profile` button after the rest of the form with user details is loaded.
$(document).ready(function(){
    $("#userDetailsDeleteProfileButton").css("display", "inline-block");
});


function initUserPlannedCoursesCarousel() {
    var settings = {
            slidesToShow: 3,
            slidesToScroll: 3,
            adaptiveHeight: true,
            prevArrow       : '<button type="button" data-role="none" class="slick-prev" aria-label="Previous" tabindex="0" role="button">' + MetaRenderer.i18nMessage('carousel.previous.label') + '</button>',
            nextArrow       : '<button type="button" data-role="none" class="slick-next" aria-label="Next" tabindex="0" role="button">' + MetaRenderer.i18nMessage('carousel.next.label') + '</button>'
        },
        carousels = $('.userPlannedCoursesCarousel');
    if (carousels && carousels.length > 0) {
        $.each(carousels, function (index, carousel) {
            if ($(carousel).children().filter('.carousel-item').length > 0) {
                $(carousel).slick(settings);
            }
        });
    }

}

function addToFavoritesUserItem(sessionId,userId) {
    var url = "/sessions/addItemToFavorites/" + sessionId;
    if (userId) {
        url = url + "/" + userId;
    }
    if (sessionId) {
        $.ajax({
            type : 'POST',
            url : url,
            data : {id: sessionId},
            cache : false,
            processData : false,
            contentType : "application/json",
            success: function(data){
                window.location.reload();
            },
            error : function(XMLHttpRequest, textStatus, errorThrown) {
                alert(MetaRenderer.i18nMessage('error.item.not.added.to.favorites'));
            }
        });
    }

}

function openPresentationForUserItem(sessionId) {
    if (sessionId) {
        window.location = '/sessions/' + sessionId;
    }
}

/*function userDetailsDeleteProfilePhoto(defaultImage) {
    var $form = $('.model-form').data();
    $form.currentValue.image=null;
    $form.currentValue.imageSrc=defaultImage;
}*/

function userDetailsDeleteProfilePhoto(userId) {
    var $modal = $("#deleteProfilePhotoModal"),
        $okButton = $(".btn.ok-delete-picture", $modal);

    $modal.modal();
    $modal.modal('open');

    $okButton.click(function () {
        var params = {};
        params.userId = userId;
            Request.send("/profile/delete-picture", params, function () {
                $modal.modal('close');
                hideLoader();
                app.notify(MetaRenderer.i18nMessage("profile.deletePhoto.success"), 2000, "green-text text-accent-1");
                window.location.reload();
                window.sessionStorage.setItem("deleteUserPicture",true);
            }, 'text', 'POST');
    });
}