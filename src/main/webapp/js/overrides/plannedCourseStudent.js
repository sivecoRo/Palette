/**
 * User: AlexandruVi
 * Date: 2017-09-10
 */
MetaRenderer.addPresentationRendererOverride('plannedCourseStudent', {
    name: 'IMAGE',
    render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType) {
        var value = model[propertyRenderer.propertyName];
        if (model.id === 125 || model.id === 117 || model.id === 118 || model.id === 119) {
            //pentru brain local id = 125
            //pentru brain dmz id in (117,118,119)
            $propertyElement.append('<img src="/img/generic/brain-apartment.png" class="image-tag "/>');
        }
        else if (model.id === 126 || model.id === 120 || model.id === 121 || model.id === 122) {
            //pentru brain local id = 126
            //pentru brain dmz id in (120,121,122)
            $propertyElement.append('<img src="/img/generic/brain-nature.png" class="image-tag "/>');
        }
        else if (model.id === 123 || model.id === 124 || model.id === 125 || model.id === 126) {
            //pentru coefr pe dmz id in (116)
            $propertyElement.append('<img src="/img/generic/coefr_curs_default.png" class="image-tag "/>');
        }
        else if (model.id === 127 || model.id === 128 || model.id === 129 || model.id === 130) {
            $propertyElement.append('<img src="/img/generic/coefr_principiul14.png" class="image-tag "/>');
        }
        else if (model.id === 131 || model.id === 132 || model.id === 133 || model.id === 134) {
            $propertyElement.append('<img src="/img/generic/coefr_principiul3.png" class="image-tag "/>');
        }
        else if (model.id === 135 || model.id === 136 || model.id === 137 || model.id === 138) {
            $propertyElement.append('<img src="/img/generic/coefr_principiul2.png" class="image-tag "/>');
        }
        else {
            $propertyElement.append('<img src="' + value + '" class="image-tag "/>');
        }
    }
});
MetaRenderer.addPresentationRendererOverride('plannedCourseStudent', {
    name: 'RATING_STARS',
    render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType) {
        var value = model[propertyRenderer.propertyName],
            defaultRenderer = MetaRenderer.getDefaultPresentationRenderer('RATING_STARS');
        defaultRenderer.render(model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType);
        $propertyElement.data({
                                  requestData: {
                                      plannedCourseStudentId: model.id,
                                      ratingValue: value
                                  }
                              });
        $propertyElement.find('.rating-star-element').hover(function (event) {
            var $ratingStart = $(event.target),
                newRating = $ratingStart.data('ratingValue');
            $propertyElement.data({
                                      requestData: {
                                          plannedCourseStudentId: model.id,
                                          ratingValue: newRating
                                      }
                                  });
            MetaRenderer.ratingViewUpdate($propertyElement, newRating);
        });
        $propertyElement.hover(null, function (event) {
            var $me = $(event.currentTarget), currentRating = $me.data('currentValue');
            MetaRenderer.ratingViewUpdate($me, currentRating);
        });
    }
});

MetaRenderer.addPresentationRendererOverride('plannedCourseStudent', {
    name: 'HTML',
    render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
        var value = model[propertyRenderer.propertyName],
            defaultRenderer = MetaRenderer.getDefaultPresentationRenderer('HTML');
        defaultRenderer.render(model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer);
        $propertyElement.find('ul.collapsible').collapsible();
    }
});

MetaRenderer.addPresentationRendererOverride('plannedCourseStudent', {
    name: 'BUTTON',
    render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
        var value = model[propertyRenderer.propertyName],
            defaultRenderer = MetaRenderer.getDefaultPresentationRenderer('BUTTON'),
            controllerData = $propertyElement.data(),
            onSuccess = function (data, textStatus, jqXHR) {
                console.log("OPEN PLAYER MODAL");
                var $responseTarget = $('body').find(controllerData.responseTarget),
                    $resp = $(data);
                $responseTarget.html($resp);
                $resp.closest('.modal-ajax-content').trigger('app:modalUpdated');
            };
        if (propertyRenderer.clickAction) {
            $propertyElement.addClass('btn');
            $propertyElement.attr('data-play', $propertyElement.data('target'));
            $propertyElement.data('modalTarget', '#player-modal');
            $propertyElement.data('actionOnSuccess', onSuccess);
            $propertyElement.data('xEmbedded', true);
            $propertyElement.html(MetaRenderer.i18nMessage(propertyRenderer.clickAction.label));
        } else {
            defaultRenderer.render(model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer);
        }
    }
});
