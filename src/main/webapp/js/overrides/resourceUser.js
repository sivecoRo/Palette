MetaRenderer.addPresentationRendererOverride('resourceUser', {
    name: 'INTEGER',
    render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
        var defaultRenderer = MetaRenderer.getDefaultPresentationRenderer('INTEGER'),
            value = model[propertyRenderer.propertyName];
        if (propertyRenderer.propertyName === 'progress') {
            $propertyElement.append('<meter value="' + value + '" min="0" max="100"></meter>');
            $propertyElement.append('<span class="property-value"> ' + value + '%</span>');
        }
        else {
            defaultRenderer.render(model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer);
        }
    }
});