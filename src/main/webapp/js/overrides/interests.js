/**
 * Created by albertb on 2/5/2018.
 */

function changeImage(obj) {
    var bb = $(obj);
    if (bb.hasClass("img-default")) {
        bb.removeClass("visible-image");
        bb.addClass("hidden-image");
        if (bb.parent().nearest(".img-selected").hasClass("hidden-image")) {
            bb.parent().nearest(".img-selected").removeClass("hidden-image");
            bb.parent().nearest(".img-selected").addClass("visible-image")
        }
    }
    else if (bb.hasClass("img-selected")) {
        bb.removeClass("visible-image");
        bb.addClass("hidden-image");
        if (bb.parent().nearest(".img-default").hasClass("hidden-image")) {
            bb.parent().nearest(".img-default").removeClass("hidden-image");
            bb.parent().nearest(".img-default").addClass("visible-image");
        }
    }
}
function initTryPaletteInterests() {
    var tryPalette = JSON.parse(window.sessionStorage.getItem("tryPalette")),
        interestsElements = $('.interest'),
        selected = false;
    if (tryPalette != undefined) {
        if (interestsElements) {
            if (tryPalette.interests != null) {
                for (i = 0; i < interestsElements.length; i++) {
                    var $defaultImage = $(interestsElements[i]).children().filter('.img-default'),
                        $selectedImage = $(interestsElements[i]).children().filter('.img-selected');
                    selected = false;
                    for (j = 0; j < tryPalette.interests.length; j++) {
                        if ($(interestsElements[i]).attr('id') ==  tryPalette.interests[j]) {
                            selected = true;
                            $defaultImage.removeClass('visible-image');
                            $defaultImage.addClass('hidden-image');
                            $selectedImage.removeClass('hidden-image');
                            $selectedImage.addClass('visible-image');
                        }
                    }
                    if (!selected) {
                        $defaultImage.removeClass('hidden-image');
                        $defaultImage.addClass('visible-image');
                        $selectedImage.removeClass('visible-image');
                        $selectedImage.addClass('hidden-image');
                    }
                }
            }
        }
    }
}

function onTryPaletteInterestsNext() {
    var selectedItemsNo = $('.img-selected.visible-image').length;
    if (selectedItemsNo > 0) {
        var tryPalette = JSON.parse(window.sessionStorage.getItem("tryPalette")),
            selectedInterestsElements = $( ".img-selected.visible-image" ),
            selectedInterests = [],
            selectedInterestsString = "";
        for(i = 0; i < selectedInterestsElements.length; i++) {
            selectedInterests.push($(selectedInterestsElements[i]).parent().attr('id'));
            selectedInterestsString = selectedInterestsString + $(selectedInterestsElements[i]).parent().attr('id') + ",";
        }
        tryPalette.interests = selectedInterests;
        window.sessionStorage.setItem("tryPalette",JSON.stringify(tryPalette));
        showLoader();
        window.location = '/results?country=' + tryPalette.country + '&coordinates=' + tryPalette.coordinates + '&interests=' + selectedInterestsString ;
    }
    else {
        errorMessage = MetaRenderer.i18nMessage("error.tryPalette.interests.no.interests.selected");
        errorTitle = MetaRenderer.i18nMessage("error.tryPalette.interests.no.interests.selected.title");
        app.errorNotify({title: errorTitle, message: errorMessage});
    }
}

function sendTryPaletteInterestsRequest(tryPalette) {
    var fData = new FormData();
    //fData.append("tryPalette", tryPalette);
    fData.append("test", "1234");
    $.ajax({
        url: "/results",
        type: 'POST',
        data: fData,
        dataType: 'json',
        processData: false, // Don't process the files
        contentType: false, // Set content type to false as jQuery will tell the server its a query string request
        success: function (data, textStatus, jqXHR) {
            window.location = '/results';
        },
        error: function (data, textStatus, jqXHR) {
            alert("ERROR");
        }
    });
}