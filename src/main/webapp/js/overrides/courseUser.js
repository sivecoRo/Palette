MetaRenderer.addPresentationRendererOverride('courseUser', {
    name: 'PIE_CHART',
    render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
        var courseUserStatistics = model[propertyRenderer.propertyName], columnNames = [], columns = [], tooltips = [],
            names = [];

        if (courseUserStatistics != null) {
            columnNames.push(MetaRenderer.i18nMessage('model.plannedCourseStudent.completed')
                             .replace('{0}', courseUserStatistics.finished + '%'));
            columnNames.push(MetaRenderer.i18nMessage('model.plannedCourseStudent.started')
                             .replace('{0}', courseUserStatistics.learning + '%'));
            columnNames.push(MetaRenderer.i18nMessage('model.plannedCourseStudent.not.started')
                             .replace('{0}', courseUserStatistics.inactive + '%'));

            columns.push([columnNames[0], courseUserStatistics.finished]);
            columns.push([columnNames[1], courseUserStatistics.learning]);
            columns.push([columnNames[2], courseUserStatistics.inactive]);

            names.push(columnNames[0]);
            names.push(columnNames[1]);
            names.push(columnNames[2]);

            $propertyElement.data('chartSettings', {
                columns: columns,
                tooltips: tooltips,
                options: {data: {columns: columns, names: names}}
            });

            $propertyElement.on("app:elementTransposed", function (event) {
                console.debug('FLUSH CHART');
                var chart = MetaRenderer.chartRenderer($(event.target), 'pieChart');
                chart.flush();
            });
        }
    }
});

MetaRenderer.addPresentationRendererOverride('courseUser', {
    name: 'MODEL_COLLECTION',
    render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
        var defaultRenderer = MetaRenderer.getDefaultPresentationRenderer('MODEL_COLLECTION');
        if (propertyRenderer.propertyName === 'userResources') {
            MetaRenderer.gridRenderer($propertyElement, propertyRenderer, $targetParent, model);
        }
        else {
            defaultRenderer.render(model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer);
        }
    }
});

MetaRenderer.addPresentationRendererOverride('courseUser', {
    name: 'MODEL',
    render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
        var defaultRenderer = MetaRenderer.getDefaultPresentationRenderer('MODEL');
        if (propertyRenderer.propertyName === 'userResourceStatisticsDetails') {
            var value = model[propertyRenderer.propertyName];
            if (value != null) {
                var activitiesNo = value.activitiesNo;
                var score = value.score;
                var spentTime = value.spentTime;
                var promoted = value.promoted;
                var grade = value.grade;
                $propertyElement.append('<ul>');
                $propertyElement.append('<li style="list-style: none;">' + activitiesNo + '</li>');
                $propertyElement.append('<li style="list-style: none;">' + score + '</li>');
                $propertyElement.append('<li style="list-style: none;">' + spentTime + '</li>');
                $propertyElement.append('<li style="list-style: none;">' + promoted + '</li>');
                $propertyElement.append('<li style="list-style: none;">' + grade + '</li>');
                $propertyElement.append('</ul>');
            }
        } else {
            defaultRenderer.render(model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer);
        }
    }
});
