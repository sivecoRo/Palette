function showReportInappropriate(message)
{
    debugger;

    if(!!message) { /* inappropriate comment data */
        $("#reportCommentData").val(message);
    }
    else {  /* inappropriate item data (empty) */
        $("#reportCommentData").val("");
    }

    /* show controls */
    $("#overlay-box-send").show();
    $("#rblReportContainer").show();
    $("#lblReportSent").hide();

    /* reset controls */
    $("#txtReportMessage").val("");
    $("input:radio[name='radio-report-inappropriate']").each(function(i) {
       this.checked = false;
    });

    /* show dialog */
    $('#overlay-box-review').modal();
    $('#overlay-box-review').modal('open');

    /* bind close dialog event */
    $("#overlay-box-close").bind( "click", function() {
        $("#overlay-box-review").modal('close');
    });
}

function sendReportInappropriate()
{
    debugger;

    var reportType = $('input[name=radio-report-inappropriate]:checked').val();
    var reportMessage = $("#txtReportMessage").val();
    var reportComment = $("#reportCommentData").val();

    var courseUrl = window.location.href.split("/").pop();
    var arr = courseUrl.split("?");

    if(arr.length > 0)
    {
        if(!!reportType || !!reportMessage)
        {
            /* Structure: URL | Course ID | Report Type | Report Message | Comment Data (User|Comment|Date|CommentId) */
            reportParams = window.location.href + "|" + arr[0] + "|" + reportType + "|" + reportMessage;
            if(!!reportComment) {
                reportParams = reportParams + "|" + reportComment;
            }

            $.ajax({
                type : 'POST',
                url : '/reportInappropriate',
                data : reportParams,
                dataType : 'text',
                contentType : "application/json",
                cache : false,
                processData : false,
                success: function(data){
                    var returnedData = JSON.parse(data);
                    if( returnedData.success === "true") {
                        $("#overlay-box-send").hide();
                        $("#rblReportContainer").hide();
                        $("#lblReportSent").show();
                    }
                    else {
                        alert('Report not sent!');
                    }
                },
                error : function(XMLHttpRequest, textStatus, errorThrown) {
                    alert('Report not sent.');
                }
            });
        }
    }
}
