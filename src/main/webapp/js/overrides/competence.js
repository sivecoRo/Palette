EventsManager.getInstance().on(EventsManager.META_UPDATED, function (event,element) {
    if( element && element.data('model-view-type') === 'FORM_FRAGMENT' && element.data('model-name') === 'competence' ) {
        $('#app-modal').find('.collapsible-header').each(function (index, element) {
            $(element).removeClass('collapsible-header');
        });
    }
    if( element && element.data('model-view-type') === 'INDEX_FRAGMENT' && $('.index-content').data('model-index') === 'competence') {
        var $addButton = $('button.action-competence-custom');
        $addButton.removeClass('action-custom');
        $addButton.addClass('action-open-create-form');
    }
});

EventsManager.getInstance().on(EventsManager.HTML_REQUEST, function (event,element) {
    var $form = $('.form-wrapper');
    if( $form && $form.data('model-name') === 'competence' ) {
        $('.collapsible-header').each(function (index, element) {
            $(element).removeClass('collapsible-header');
        });
    }
});