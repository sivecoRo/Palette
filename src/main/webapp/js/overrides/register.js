$('#modalRegisterSuccessful').modal();

$("#register-form").validate({
    rules: {
        email: {
            required: true,
            email:true
        },
        password: {
            required: true,
            minlength: 8
        },
        confirmPassword: {
            required: true,
            minlength: 8,
            equalTo: "#password"
        }
    },
    messages: {
        email: {
            required: $(".messages [message-attr='register.email.required']").text(),
            email: $(".messages [message-attr='register.email.required.valid']").text()
        },
        password: {
            required: $(".messages [message-attr='register.password.required']").text(),
            minlength: $(".messages [message-attr='register.password.minlength']").text()
        },
        confirmPassword: {
            required: $(".messages [message-attr='register.confirm.password.required']").text(),
            minlength: $(".messages [message-attr='register.confirm.password.minlength']").text(),
            equalTo: $(".messages [message-attr='registration.failed.passwords']").text()
        }
    },
    errorElement : 'div',
    errorClass: 'invalid red-text',
    errorPlacement: function(error, element) {
        $('#' + element.attr("id") + '-error').remove();
        var placement = $(element).data('error');
        if (placement) {
            $(placement).append(error)
        } else {
            error.insertAfter(element);
        }
    }
});


$("#email").on('blur', function(event) {
    var url = '/checkEmail?email=' + $(this).val();

    $.ajax({
        type : 'GET',
        url : url,
        cache : false,
        processData : false,
        contentType : "application/json",
        success : function(response) {
            var returnedData = $.parseJSON(response);
            if( returnedData.success === "false") {
                $('#email-error').remove();
                var placement = $("#emailLabel").data('error');
                var error = $('<div id="email-error" class="invalid red-text" th:utext="#{registration.failed.email}">Email already in use</div>');

                if (placement) {
                    $(placement).append(error)
                } else {
                    error.insertAfter($("#emailLabel"));
                }

                $('#email').removeClass("valid");
                $("#email").addClass("invalid red-text");
            }
        },
        error : function(XMLHttpRequest, textStatus, errorThrown) {
            alert($(".messages [message-attr='registration.check.email.error']").text());
        }
    });
});


$("#register-form").on('submit', function(event){
    event.preventDefault();
    var user= {};
    $.each($(this).serializeArray(), function( i, element){
        user[element.name]=element.value;
    });

    if ( $("#register-form").valid() ) {
        $.ajax({
            type : 'POST',
            url : "/register",
            data : JSON.stringify(user),
            cache : false,
            processData : false,
            contentType : "application/json",
            success: function(data){
                var returnedData = JSON.parse(data);
                if( returnedData.success === "true") {
                    $('#modalRegisterSuccessful').modal().modal('open');
                } else {
                    if( returnedData.msg === "email") {
                        $('#email-error').remove();
                        var placement = $("#emailLabel").data('error');
                        var error = $('<div id="email-error" class="invalid red-text" th:utext="#{registration.failed.email}">Email already in use</div>');

                        if (placement) {
                            $(placement).append(error)
                        } else {
                            error.insertAfter($("#emailLabel"));
                        }

                        $('#email').removeClass("valid");
                        $("#email").addClass("invalid red-text");
                    } else {
                        $('#confirmPassword-error').remove();
                        var placement = $("#confirmPasswordLabel").data('error');
                        var error = $('<div id="confirmPassword-error" class="invalid red-text" th:utext="#{registration.failed.passwords}">Passwords do not match</div>');

                        if (placement) {
                            $(placement).append(error)
                        } else {
                            error.insertAfter($("#confirmPasswordLabel"));
                        }

                        $('#confirmPassword').removeClass("valid");
                        $("#confirmPassword").addClass("invalid red-text");
                    }
                }
            },
            error : function(XMLHttpRequest, textStatus, errorThrown) {
                alert($(".messages [message-attr='register.user.error']").text());
            }
        });
    }
});


function closeModalRegisterSuccessful() {
    $('#modalRegisterSuccessful').modal('close');
}

function goToLoginRegisterSuccessful() {
    window.location = '/login';
}
