/**
 * User: AlexandruVi
 * Date: 2017-09-10
 */
MetaRenderer.addPresentationRendererOverride('course', {
    name: 'MODEL_AVATAR',
    render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
        var avatarModel = model[propertyRenderer.propertyName],
            defaultRenderer = MetaRenderer.getDefaultPresentationRenderer('MODEL_AVATAR');

        defaultRenderer.render(model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer);
        if (propertyRenderer.propertyName === 'creator') {
            //$propertyElement.prepend('<span class="created-by" data-x-label="createdBy"></span>');
        }
    }
});

MetaRenderer.addPresentationRendererOverride('course', {
    name: 'ENUM',
    render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
        var defaultRenderer = MetaRenderer.getDefaultPresentationRenderer('ENUM'),
            className = 'course-type-view-' + model.type;
        defaultRenderer.render(model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer);
        if (propertyRenderer.propertyName === 'type') {
            $propertyElement.addClass(className);
        }
    }
});

MetaRenderer.addPresentationRendererOverride('aelUser', {
    name: 'MODEL_AVATAR',
    render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
        var avatarModel = model[propertyRenderer.propertyName],
            defaultRenderer = MetaRenderer.getDefaultPresentationRenderer('MODEL_AVATAR');

        defaultRenderer.render(model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer);
        console.log('render trainees');
        console.log($propertyElement);

    }
});

MetaRenderer.addPresentationRendererOverride('course', {
    name: 'MODEL_COLLECTION_CAROUSEL',
    render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
        var defaultRenderer = MetaRenderer.getDefaultPresentationRenderer('MODEL_COLLECTION_CAROUSEL');
        defaultRenderer.render(model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer);
        $propertyElement.on('sliderAfterChangeElement', function (event, $currentSlideElement, model, slick, currentSlide) {
            // console.log(arguments);
        });
    }
});

MetaRenderer.addPresentationRendererOverride('course', {
    name  : 'MODEL_COLLECTION',
    render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
        var defaultRenderer = MetaRenderer.getDefaultPresentationRenderer('MODEL_COLLECTION');
        if (propertyRenderer.propertyName === 'attendingStudentsForDisplay' ) {
            $propertyElement.data('rendererType', 'MODEL_AVATAR_IMG');
        }
        if (propertyRenderer.propertyName === 'allAttendingStudents' ) {
            $propertyElement.data('rendererType', 'MODEL_AVATAR');
        }
        if (propertyRenderer.propertyName === 'attendingStudentsForDisplay' || propertyRenderer.propertyName === 'allAttendingStudents') {

            $propertyElement.on("app:elementTransposed", function (event) {
                $.each(model[propertyRenderer.propertyName], function (index, user) {
                    MetaRenderer.updateView('aelUser', user.id);
                });
            });
        }
        defaultRenderer.render(model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer);
        if (propertyRenderer.propertyName === 'attendingStudentsForDisplay' || propertyRenderer.propertyName === 'allAttendingStudents') {
            $propertyElement.find('li.model-view.collection-item').each(function(index,element) {
                $(element).click(function() {
                    window.location = "/profile/" + $(element).data('currentValue').id;
                });
            })
        }
        if (propertyRenderer.propertyName === 'allAttendingStudents' ) {
            $propertyElement.hide();
        }
    }
});

MetaRenderer.addPresentationRendererOverride('course', {
    name  : 'BUTTON',
    render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
        renderButtonsForAttendingSection(model, $propertyElement, propertyRenderer);
    }
});

MetaRenderer.addInputRendererOverride('course', {
    name: 'IMAGE_UPLOAD',
    render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
        var defaultRenderer = MetaRenderer.getDefaultInputRenderer('IMAGE_UPLOAD');
        defaultRenderer.render(model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer);
        var $button = $propertyElement.nearest('.btn'),
            $buttonLabel = $button.children('span'),
            $filePathWrapper = $propertyElement.nearest('.file-path-wrapper'),
            $fileInput = $button.children('input[type=file]'),
            imageSrc = model.addImageSrc,
            viewName = getUrlParameter("viewName");
        $fileInput.attr('accept', '.jpg,.jpeg,.png');
        $button.removeClass('btn').addClass('hidden-upload-button');
        $buttonLabel.addClass('center-icon');
        $buttonLabel.attr("data-image",MetaRenderer.i18nMessage('upload.picture'));
        $filePathWrapper.hide();
        if (model.id === null) {
            if (viewName !== undefined && viewName !== null) {
                imageSrc = '/img/generic/generic-' + viewName.substring(0, viewName.length - 4).toLowerCase() + '-light.png'
            }
            else {
                imageSrc = '/img/generic/generic-course-light.png';
            }
        }
        if (imageSrc) {
            $propertyElement.css('background-image', 'url(' + imageSrc + ')');
        }
        $propertyElement.on('app:dirtyChange', function (e, dirty, propertyName, currentValue, $sourceInput, sourceEvent) {
            var imageSrc = URL.createObjectURL(currentValue[0]);
            if (imageSrc) {
                $propertyElement.css('background-image', 'url(' + imageSrc + ')');
            }
        });
    }
});

MetaRenderer.addPresentationRendererOverride('course', {
    name: 'IMAGE',
    render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
        var value = model[propertyRenderer.propertyName];
        //brain local id=25
        //brain pe dmz id =126
        if (value == undefined) {
            $propertyElement.append('<img src="/img/generic/generic-course-light.png" class="image-tag "/>');
        }
        else {
            $propertyElement.append('<img src="' + value + '" class="image-tag "/>');
        }
        if (propertyRenderer.propertyName === 'ownerImageSrc') {
            $propertyElement.on('click', function() {
                window.location = '/profile/' + model.owner.id;
            });
        }
    }
});

MetaRenderer.addPresentationRendererOverride('course', {
    name: 'HTML',
    render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
        var defaultRenderer = MetaRenderer.getDefaultPresentationRenderer('HTML'),
            $name = null;
        defaultRenderer.render(model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer);
        if (propertyRenderer.propertyName === 'name') {
            $name = $propertyElement.find('span');
            $name.attr('data-position', 'top');
            $name.attr('data-tooltip', model.name);
            if (!$name.hasClass('tooltipped')) {
                $name.addClass('tooltipped');
            }
        }
    }
});

MetaRenderer.addPresentationRendererOverride('course', {
    name: 'STRING',
    render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
        var defaultRenderer = MetaRenderer.getDefaultPresentationRenderer('STRING');
        if (propertyRenderer.propertyName === 'ownerEmail') {
            if (model.showOwnerEmail !== null && model.showOwnerEmail === true) {
                defaultRenderer.render(model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer);
            }
        } else if (propertyRenderer.propertyName === 'ownerPhone') {
            if (model.showOwnerPhone !== null && model.showOwnerPhone === true) {
                defaultRenderer.render(model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer);
            }
        }
        else {
            defaultRenderer.render(model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer);
        }
        if (propertyRenderer.propertyName === 'ownerName') {
            $propertyElement.on('click', function() {
                window.location = '/profile/' + model.owner.id;
            });
        }
    }
});

MetaRenderer.addInputRendererOverride('course', {
    name: 'LIST_OF_VALUES_MULTI_SELECT',
    render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
        var defaultRenderer = MetaRenderer.getDefaultInputRenderer('LIST_OF_VALUES_MULTI_SELECT');
        if (propertyRenderer.propertyName == 'includedTags') {
            //defaultRenderer.render(model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer);
            var $containerDiv = $('<div id="courseInterestsFormContainer"><div class="row center-align"><div class="col s12"></div></div></div>');
            $.each(model.allInterests, function(index, interest) {
                var $selectedHiddenImage = null,
                    $selectedVisibleImage = null,
                    $interestParentContainer = $('<div id="courseInterestsList"></div>');
                $interestContainer = $('<div class="course-interest" id="' + interest.id + '"></div>');
                $interestName = $('<h6>' + interest.displayName + '</h6>');
                if (interest.selected == true) {
                    $selectedHiddenImage = $('<img class="interest-img img-default hidden-image" id="image" src="/img/home/icons/' + interest.image + '" onClick="courseChangeImage(this,' + interest.id + ',' + interest.tag.id+ ')"/>');
                    $selectedVisibleImage = $('<img class="interest-img img-selected visible-image  z-depth-5" id="image"  src="/img/home/icons/' + interest.imageSelected + '" onClick="courseChangeImage(this,' + interest.id + ',' + interest.tag.id+ ')"/>');
                    $selectedHiddenImage.attr('tagValue',interest.displayName);
                    $selectedVisibleImage.attr('tagValue',interest.displayName);
                }
                else {
                    $selectedHiddenImage = $('<img class="interest-img img-default visible-image  z-depth-5" id="image" src="/img/home/icons/' + interest.image + '" onClick="courseChangeImage(this,' + interest.id + ',' + interest.tag.id+ ')"/>');
                    $selectedVisibleImage = $('<img class="interest-img img-selected hidden-image" id="image"  src="/img/home/icons/' + interest.imageSelected + '" onClick="courseChangeImage(this,' + interest.id + ',' + interest.tag.id+ ')"/>');
                    $selectedHiddenImage.attr('tagValue',interest.displayName);
                    $selectedVisibleImage.attr('tagValue',interest.displayName);
                }
                $interestContainer.append($selectedHiddenImage);
                $interestContainer.append($selectedVisibleImage);
                $interestContainer.append($interestName);
                $interestParentContainer.append($interestContainer);
                $containerDiv.append($interestParentContainer);
            });
            $propertyElement.append($containerDiv);
            defaultRenderer.render(model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer);
            $propertyElement.children().filter('input').hide();
            $propertyElement.children().filter('.chips').hide();
        }
        else {
            defaultRenderer.render(model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer);
        }
    }
});

MetaRenderer.addInputRendererOverride('course', {
    name: 'DATE',
    render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
        var defaultRenderer = MetaRenderer.getDefaultInputRenderer('DATE');
        defaultRenderer.render(model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer);
        if (propertyRenderer.propertyName === 'plannedCourseStartDate' || propertyRenderer.propertyName === 'plannedCourseEndDate') {
            $propertyElement.on("app:propertyAppended", function () {
                if (propertyRenderer.propertyName === 'plannedCourseStartDate') {
                    $propertyElement.find('input').val(model['displayPlannedCourseStartDate']);
                }
                else if (propertyRenderer.propertyName === 'plannedCourseEndDate') {
                    $propertyElement.find('input').val(model['displayPlannedCourseEndDate']);
                }
            });
            $propertyElement.find('input').change(function(event) {
                $formData = $('.form-wrapper').data('currentValue');
                if (propertyRenderer.propertyName === 'plannedCourseStartDate') {
                    $formData.startDateRaw = $(event.currentTarget).val();
                }
                else if (propertyRenderer.propertyName === 'plannedCourseEndDate') {
                    $formData.endDateRaw = $(event.currentTarget).val();
                }
            });
        }
    }
});

function confirmDeleteUserModal(userId)
{
    $('#deleteTraineeModal').modal();
    $('#deleteTraineeModal').find('input[name=traineeId]').val(userId);
    $('#deleteTraineeModal').modal('open');
}

function deleteTrainee()
{
    var userId = $('#deleteTraineeModal').find('input[name=traineeId]').val();
    var deleteUserUrl = '/courses/removeUserFromSession/'+$('.slick-active').data('model-uid') + '/' + userId;

    $.ajax({
        url: deleteUserUrl,
        type: "GET",
        contentType: "application/json",
        success: function (data, textStatus, jqXHR) {
            var returnedData = JSON.parse(data);
            if (returnedData.error === "1") {
                alert($(".messages [message-attr='modal.plannedCourse.error.remove.trainee']").text());
            }
            else {
                MetaRenderer.updateView('plannedCourse', $('.slick-active').data('model-uid'));
                $('#deleteTraineeModal').modal('close');
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
        }
    });
}

function closeModalDeleteTrainee()
{
    $('#deleteTraineeModal').modal('close');
}

function initModalAddUser() {
    $('#modalAddUser').modal();
    // $('select').material_select();
}

function openModalAddUser() {
    $('#modalAddUser').modal('open');
}

function closeModalAddUser() {
    $('#modalAddUser').modal('close');
}

function addUserToCourseSession() {
    var assocUrl = '/courses/addUserToCourseSession/' + $('.slick-active').data('model-uid') + '/' + $('#chosenUserID')
        .val();

    $.ajax({
        url: assocUrl,
        type: "GET",
        contentType: "application/json",
        success: function (data, textStatus, jqXHR) {
            var returnedData = JSON.parse(data);
            if (returnedData.error === "1") {
                alert($(".messages [message-attr='courses.student.add.error']").text());
            }
            else {
                /*$('#sessionDetailsId').hide().load( "/courses/session/getDetails/" + $('input#sessionId').val() , function(){
                 $("#sessionDetailsId").fadeIn();
                 ChartInitializer.init();
                 });

                 $('#sessionStudentsId').hide().load( "/courses/session/getStudents/" + $('input#sessionId').val() , function(){
                 $("#sessionStudentsId").fadeIn();
                 });*/
                MetaRenderer.updateView('plannedCourse', $('.slick-active').data('model-uid'));
                $('#modalAddUser').modal('close');
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
        }
    });
};

function courseOnUiUpdated(event, $element) {
    var elementData = $element.data(),
        model = elementData.currentValue,
        modelName = elementData.modelName,
        elementViewName = elementData.modelViewName,
        elementViewType = elementData.modelViewType,
        elementIndexViewType = elementData.modelIndexViewType,
        itemType=model.type;
    if (elementViewType === 'INDEX_FRAGMENT') {
        var typeClass = 'course-type-' + itemType,
            $typeProperty = $element.find('.model-property-type'),
            $createItemButton = $('.create-item-btn-wrapper').find('a.btn-floating.btn-large').not('.generic-action.action-open-create-form.action-course-open-create-form');

        if ($createItemButton && !$createItemButton.hasClass('btn-create-item')) {
            $createItemButton.addClass('btn-create-item');
        }
        if (!$typeProperty.hasClass(typeClass)) {
            $typeProperty.addClass(typeClass);
        }

        updateAttending($element, model.attending);

        if(itemType == "ACTIVITY")
        {
            $element.find('.edit-service-class').remove();
            $element.find('.edit-meeting-class').remove();
            $element.find('.edit-course-class').remove();
        }
        else if(itemType == "SERVICE")
        {
            $element.find('.edit-activity-class').remove();
            $element.find('.edit-meeting-class').remove();
            $element.find('.edit-course-class').remove();
        }
        else if(itemType == "MEETING")
        {
            $element.find('.edit-service-class').remove();
            $element.find('.edit-activity-class').remove();
            $element.find('.edit-course-class').remove();
        }
        else if(itemType == "COURSE")
        {
            $element.find('.edit-service-class').remove();
            $element.find('.edit-meeting-class').remove();
            $element.find('.edit-activity-class').remove();
        }
    }
    else if(elementViewType === 'FORM_FRAGMENT')
    {
        initTimeInputs();
        initDateInputs();
        focusFirstInput();
    } else if(elementViewType === 'VIEW_FRAGMENT') {
        processItemTextAreaOnView('course','description');
        processItemTextAreaOnView('course','objective');
        if(itemType !== "COURSE") {
            $('.row.item-objective').parent().hide();
        }
    }
}

function focusFirstInput() {
    var $firstInputWrapper = $('.input-field.model-course-property.model-property-value.model-property-name.property-input-wrapper'),
        $firstInput;
    if ($firstInputWrapper) {
        $firstInput = $firstInputWrapper.children().filter('input');
        $firstInput.focus();
        $firstInput.blur();

    }
}

function processItemTextAreaOnView(model,property){
    var $descriptionWrapper = $('.property-presentation.model-' + model + '-property.model-property-value.model-property-' + property + '.property-input-wrapper'),
        $description;
    if ($descriptionWrapper) {
        $description = $descriptionWrapper.children().filter('span.property-value');
        if ($description.length > 0) {
            $description[0].innerHTML = $description[0].innerHTML.replace(/\n/g, '<br/>');
        }
    }
}

MetaRenderer.addInputRendererOverride('course', {
    name: 'TEXT',
    render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
        var defaultRenderer = MetaRenderer.getDefaultInputRenderer('TEXT');
        var value = model[propertyRenderer.propertyName];

        if(propertyRenderer.propertyName === "itemLocation") {
            $propertyElement.on("app:propertyAppended",function () {
                var $input=$(this).nearest('input');
                options = {
                    types: ['(cities)']
                };
                autocompleteLocation($input, options);
                $input.keyup(function() {
                    if (!this.value) {
                        updateCourseProperty(null);
                    }

                });
                $input.on('keypress', function ()  {
                    updateCourseProperty('country',null);
                    updateCourseProperty('countryCode',null);
                    updateCourseProperty('itemLocationCoordinates',null);
                    setCountryInForm(null);
                    setCountryCodeInForm(null);
                    setItemLocationCoordinatesInForm(null);
                });
            });
        }

        defaultRenderer.render(model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer);
        if(propertyRenderer.propertyName === "itemLocation") {
            $propertyElement.find('input').off('mouseout');
        }
    }
});

function initTimeInputs() {
    var $startTimeInput = $('.model-property-plannedCourseStartTime.property-input-wrapper').find('input'),
        $endTimeInput = $('.model-property-plannedCourseEndTime.property-input-wrapper').find('input'),
        $formData = $('.form-wrapper').data('currentValue');
    $startTimeInput.change(function(event) {
        timeChangeHandler(event,'plannedCourseStartTime');
    });
    $endTimeInput.change(function(event) {
        timeChangeHandler(event,'plannedCourseEndTime');
    });
    $formData.startTimeRaw = $startTimeInput.val();
    $formData.endTimeRaw = $endTimeInput.val();
}

function timeChangeHandler(event, propertyName) {
    var $formData = $('.form-wrapper').data('currentValue');
    if (propertyName === 'plannedCourseStartTime') {
        $formData.startTimeRaw = $(event.currentTarget).val();
    }
    else if (propertyName === 'plannedCourseEndTime') {
        $formData.endTimeRaw = $(event.currentTarget).val();
    }
}

function initDateInputs() {
    var $startDateInput = $('.model-property-plannedCourseStartDate.property-input-wrapper').find('input'),
        $endDateInput = $('.model-property-plannedCourseEndDate.property-input-wrapper').find('input'),
        $formData = $('.form-wrapper').data('currentValue'),
        clientTimeZoneId = Intl.DateTimeFormat().resolvedOptions().timeZone;

    $formData.startDateRaw = $startDateInput.val();
    $formData.endDateRaw = $endDateInput.val();
    $formData.clientTimeZoneId = clientTimeZoneId;
}

function autocompleteLocation($propertyElement, options)
{
    autocomplete = new google.maps.places.Autocomplete(
        /** @type {!HTMLInputElement} */$propertyElement[0], options);
    autocomplete.addListener('place_changed', fillInCountry);
}

function fillInCountry() {
    // Get the place details from the autocomplete object.
    var place = autocomplete.getPlace(),
        value;

    $('.form-wrapper').data('currentValue').itemLocation = place.formatted_address;
    updateCourseProperty('country',null);
    updateCourseProperty('countryCode',null);
    updateCourseProperty('itemLocationCoordinates',null);

    // Get each component of the address from the place details
    // and fill the corresponding field on the form.
    for (var i = 0; i < place.address_components.length; i++) {
        var addressType = place.address_components[i].types[0];
        if (addressType == 'country') {
            value = place.address_components[i];
            updateCourseProperty('country',value.long_name);
            updateCourseProperty('countryCode',value.short_name);
            setCountryInForm(value.long_name);
            setCountryCodeInForm(value.short_name);
        }
    }
    if (place.geometry) {
        var coordinates = place.geometry.location.lat() + ';' + place.geometry.location.lng();
        updateCourseProperty('itemLocationCoordinates',coordinates);
        setItemLocationCoordinatesInForm(coordinates);
    }
}

function updateCourseProperty(propertyName, value) {
    var $property = $('.model-property-' + propertyName),
        $input = $property.find('input');

    $input.val(value);
    $property.data('currentValue',value);
}

function setCountryInForm(value) {
    $('.form-wrapper').data('currentValue').country = value;
}

function setCountryCodeInForm(value) {
    $('.form-wrapper').data('currentValue').countryCode = value;
}

function setItemLocationCoordinatesInForm(value) {
    $('.form-wrapper').data('currentValue').itemLocationCoordinates = value;
}

function courseChangeImage(obj, id, tagId) {
    var bb = $(obj),
        $includedTagsInput = null,
        $createIncludedTags = $('#course-course-create-includedTags'),
        $updateIncludedTags = $('#course-course-update-includedTags'),
        includedTags;
    if ( $createIncludedTags.length > 0) {
        $includedTagsInput = $createIncludedTags;
    }
    else {
        $includedTagsInput = $updateIncludedTags;
    }
    includedTags = $includedTagsInput.data().currentValue;
    if (includedTags == null) {
        includedTags = [];
    }
    if (bb.hasClass("img-default")) {
        bb.removeClass("visible-image");
        bb.addClass("hidden-image");
        if (bb.parent().nearest(".img-selected").hasClass("hidden-image")) {
            bb.parent().nearest(".img-selected").removeClass("hidden-image");
            bb.parent().nearest(".img-selected").addClass("visible-image")
        }
        includedTags.push({id:tagId})
    }
    else if (bb.hasClass("img-selected")) {
        bb.removeClass("visible-image");
        bb.addClass("hidden-image");
        if (bb.parent().nearest(".img-default").hasClass("hidden-image")) {
            bb.parent().nearest(".img-default").removeClass("hidden-image");
            bb.parent().nearest(".img-default").addClass("visible-image");
        }
        //remove interest id from selected interests array
        includedTags = $.grep(includedTags, function(n) {
            return n.id != tagId;
        })
    }
    $includedTagsInput.data('currentValue',includedTags);
    $includedTagsInput.val(includedTags).change();
}

function addLoader() {
    $('a').not('#calendarBtn').not('#course-filter-by-period').not('#planned-course-filter-by-period').not('#course-close-modal')
        .not('#planned-course-close-modal').not('#pc-filter-add-more-button').not('#course-close-interests').not('#planned-course-close-interests')
        .not('#course-close-modal')
        .not('#course-filter-add-more-button')
        .not('.generic-action.action-custom-get.action-plannedCourse-custom-get.btn.meta-controller')
        .not('.send-mail-action')
        .not('.modal-action.modal-close.close-action.waves-effect.waves-green.btn.left')
        .not('.dropdown-button')
        .not('.btn-create-item')
        .not('#user-details-personal-profile')
        .not('#user-details-complete-profile')
        .not('#user-details-interests-profile')
        .not('#user-details-summary-profile')
        .not('#try-palette-interests-next')
        .not('#try-palette-location-next')
        .not('#close-modal-reset-password')
        .not('.modal-action.waves-effect.waves-light.btn.send-email')
        .not('#pc-filter-interests-modal-close')
        .not('#course-filter-interests-modal-close')
        .not('#course-dates-modal-next')
        .not('#send-mail-modal-back')
        .not('.no-preloader')
        .click(function() {
            showLoader();
        });
    $('button').not('.modal-action.modal-close.cancel-action.waves-effect.waves-green.btn.left')
        .not('.generic-action.action-custom.action-plannedCourse-custom.btn.meta-controller.favorite-not-added-item')
        .not('.generic-action.action-custom.action-plannedCourse-custom.btn.meta-controller')
        .not('.property-presentation.model-plannedCourse-property.model-property-value.model-property-addToFavoritesBtn.meta-controller.property-input-wrapper.btn.favorite-not-added-item-view')
        .not('.property-presentation.model-plannedCourse-property.model-property-value.model-property-addToFavoritesBtn.meta-controller.property-input-wrapper.btn.favorite-added-item-view')
        .not('.property-presentation.model-plannedCourse-property.model-property-value.model-property-itemLikeBtn.meta-controller.property-input-wrapper.btn')
        .not('.property-presentation.model-plannedCourse-property.model-property-value.model-property-itemDislikeBtn.meta-controller.property-input-wrapper.btn')
        .not('.generic-action.action-delete.action-course-delete.btn.meta-controller')
        .not('#btnCreateProfileFirstStep')
        .not('#btnCreateProfileSecondStep')
        .not('#btnCreateProfileBackSecondStep')
        .not('#btnCreateProfileThirdStep')
        .not('#btnCreateProfileBackThirdStep')
        .not('#btnCreateProfileBackFourthStep')
        .not('.slick-prev.slick-arrow')
        .not('.slick-next.slick-arrow')
        .not('#forgot-password-reset')
        .not('.btn-flat.picker__clear.waves-effect')
        .not('.btn-flat.picker__today.waves-effect')
        .not('.btn-flat.picker__close.waves-effect')
        .not('.modal-action.modal-close.cancel-action.waves-effect.waves-light.btn.secondary.left.app')
        .not('.modal-action.modal-close.close-action.waves-effect.waves-light.btn.secondary.left.app')
        .not('.no-preloader')
        .click(function() {
            showLoader();
        });
}

function hideLoaderTimedOut() {
    if (typeof app !== 'undefined') {
        setTimeout( function(){
            console.log('Hiding preloader .....');
            app.hideLoader();
        }, 5000 );
    }
}

function showLoader() {
    if (typeof app !== 'undefined') {
        app.showLoader();
        hideLoaderTimedOut();
    }
}

function hideLoader() {
    if (typeof app !== 'undefined') {
        app.hideLoader();
    }
}

courseOnOPEN_PRESENTATIONSuccess = function (event, $controller, action, responseData, jqXHR, pushState, dataType){
    hideLoader();
    addLoader();
};

courseOnOPEN_EDIT_FORMSuccess = function (event, $controller, action, responseData, jqXHR, pushState, dataType){
    hideLoader();
    addLoader();
};

courseOnOPEN_CREATE_FORMSuccess = function (event, $controller, action, responseData, jqXHR, pushState, dataType){
    hideLoader();
    addLoader();
};

courseOnCREATESuccess = function (event, $controller, action, responseData, jqXHR, pushState, dataType){
    showLoader();
};

courseOnUPDATESuccess = function (event, $controller, action, responseData, jqXHR, pushState, dataType){
    showLoader();
};

EventsManager.getInstance().on(EventsManager.META_UPDATED, function (event) {
    initAttendedCoursesCarousel();
    initMyPastCoursesCarousel();
    initCourseComments();
});

function initCourseComments() {
    $( "#addCourseComment" ).off("click");
    $( "#addCourseComment" ).on("click", function(event) {
        event.preventDefault();
        var fData = new FormData(),
            courseId = $("#courseId").val(),
            errorMessage = MetaRenderer.i18nMessage("model.course.error.comment.empty");
            errorTitle = MetaRenderer.i18nMessage("model.course.error.comment.empty.title");
        fData.append("comment", app.htmlDecode($("#commentInput").val()));
        fData.append("courseId", courseId);

        $.ajax({
            url        : "/courses/getComment",
            type       : 'POST',
            data       : fData,
            dataType   : 'json',
            processData: false, // Don't process the files
            contentType: false, // Set content type to false as jQuery will tell the server its a query string request
            success    : function (data, textStatus, jqXHR) {
                if (data.success) {
                    location.reload();
                }
                else {
                    app.errorNotify({title: errorTitle, message: errorMessage});
                }
            },
            error   : function (data, textStatus, jqXHR) {
                console.log("Error adding comment.");
            }
        });
    });
}
function initAttendedCoursesCarousel() {
    var settings = {
            slidesToShow: 3,
            slidesToScroll: 3,
            adaptiveHeight: true,
            prevArrow       : '<button type="button" data-role="none" class="slick-prev" aria-label="Previous" tabindex="0" role="button">' + MetaRenderer.i18nMessage('carousel.previous.label') + '</button>',
            nextArrow       : '<button type="button" data-role="none" class="slick-next" aria-label="Next" tabindex="0" role="button">' + MetaRenderer.i18nMessage('carousel.next.label') + '</button>'
        },
        carousels = $('.attendedCoursesCarousel');
    if (carousels && carousels.length > 0) {
        $.each(carousels, function (index, carousel) {
            if ($(carousel).children().filter('.carousel-item').length > 0) {
                $(carousel).slick(settings);
            }
        });
    }
}

function initMyPastCoursesCarousel() {
    var settings = {
            slidesToShow: 3,
            slidesToScroll: 3,
            adaptiveHeight: true,
            prevArrow       : '<button type="button" data-role="none" class="slick-prev" aria-label="Previous" tabindex="0" role="button">' + MetaRenderer.i18nMessage('carousel.previous.label') + '</button>',
            nextArrow       : '<button type="button" data-role="none" class="slick-next" aria-label="Next" tabindex="0" role="button">' + MetaRenderer.i18nMessage('carousel.next.label') + '</button>'
        },
        carousel = $('#myPastCoursesCarousel');
    if (carousel && carousel.length > 0 && carousel.children().filter('.carousel-item').length > 0) {
        $('#myPastCoursesCarousel').slick(settings);
    }

}

function openPresentationForMyPastItem(courseId) {
    if (courseId) {
        window.location = '/courses/' + courseId;
    }
}


$(document).ready(function() {
    /* get url parameter helper */
    var getUrlParameter = function getUrlParameter(sParam) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)), sURLVariables = sPageURL.split('&'), sParameterName, i;
        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    };

    /* review from the organizer or participant */
    if(getUrlParameter("userId") || getUrlParameter("courseId"))
    {
        /* Parameter: userId (participant) */
        /* Parameter: courseId (organizer) */
        var feedbackTitle = "";
        if(getUrlParameter("userId")) {
            feedbackTitle = MetaRenderer.i18nMessage('review.participant');
        }
        else if (getUrlParameter("courseId")) {
            feedbackTitle = MetaRenderer.i18nMessage('review.organizer');
        }

        var courseUrl = window.location.href.split("/").pop();
        var arr = courseUrl.split("?");

        if(arr.length > 0)
        {
            var courseId = arr[0];
            /* dialog element */
            var elem = document.createElement('div');
            elem.setAttribute("id", "idReview");
            elem.style.cssText = 'position:absolute;background-color:#f9f8cc;border: 2px solid #000;width: 350px;height: 200px;margin: auto;top: 20px;right: 0;left: 0;padding:20px;z-index:9999;font-size:24px;border-radius:20px 20px';
            /* review title */
            var elemContainer = document.createElement('div');
            var txtFeedback = document.createElement("txtFeedback");
            var t = document.createTextNode(feedbackTitle);
            txtFeedback.appendChild(t);
            txtFeedback.setAttribute("class","property-label");
            txtFeedback.setAttribute("style","font-size:30px;color:#283593");
            elemContainer.appendChild(txtFeedback);
            elem.appendChild(elemContainer);
            /* no button */
            var btnNo = document.createElement("btnFeedbackNo");
            var btnNoText = document.createTextNode(MetaRenderer.i18nMessage('modal.no'));
            btnNo.setAttribute("class","no-preloader no-ajax waves-effect btn secondary");
            btnNo.onclick = function() {
                if(getUrlParameter("userId")) {
                    var strParams = getUrlParameter("userId") + "|false";
                    reviewParticipant(strParams);
                }
                else if(getUrlParameter("courseId")) {
                    var strParams = getUrlParameter("courseId") + "|false";
                    reviewOrganizer(strParams);
                }
            };
            btnNo.appendChild(btnNoText);
            elem.appendChild(btnNo);
            /* yes button */
            var btnYes = document.createElement("btnFeedbackYes");
            var btnYesText = document.createTextNode(MetaRenderer.i18nMessage('modal.yes'));
            btnYes.setAttribute("class","no-preloader no-ajax waves-effect btn");
            btnYes.onclick = function() {
                if(getUrlParameter("userId")) {
                    var strParams = getUrlParameter("userId") + "|true";
                    reviewParticipant(strParams);
                }
                else if(getUrlParameter("courseId")) {
                    var strParams = getUrlParameter("courseId") + "|true";
                    reviewOrganizer(strParams);
                }
            };
            btnYes.appendChild(btnYesText);
            elem.appendChild(btnYes);
            /* append dialog to body */
            document.body.appendChild(elem);
        }
    }
    else if(getUrlParameter("deleteCommentId"))
    {
        var deleteCommentTitle = MetaRenderer.i18nMessage('comments.inappropriate.title');
        var courseUrl = window.location.href.split("/").pop();
        var arr = courseUrl.split("?");

        if(arr.length > 0)
        {
            var args = arr[1].split("=");
            var commentId = args[1];
            /* dialog element */
            var elem = document.createElement('div');
            elem.setAttribute("id", "idDeleteComment");
            elem.style.cssText = 'position:absolute;background-color:#f9f8cc;border: 2px solid #000;width: 400px;height: 250px;margin: auto;top: 20px;right: 0;left: 0;padding:20px;z-index:9999;font-size:24px;border-radius:20px 20px';
            /* review title */
            var elemContainer = document.createElement('div');
            var txtDeleteComment = document.createElement("txtDeleteComment");
            var t = document.createTextNode(deleteCommentTitle);
            txtDeleteComment.appendChild(t);
            txtDeleteComment.setAttribute("class","property-label");
            txtDeleteComment.setAttribute("style","font-size:30px;color:#283593");
            elemContainer.appendChild(txtDeleteComment);
            elem.appendChild(elemContainer);
            /* no button */
            var btnNo = document.createElement("btnDeleteCommentNo");
            var btnNoText = document.createTextNode(MetaRenderer.i18nMessage('modal.no'));
            btnNo.setAttribute("class","no-preloader no-ajax waves-effect btn secondary");
            btnNo.onclick = function() {
                /* redirect to item's page */
                var arrUrl = window.location.href.split("?");
                window.location.replace(arrUrl[0]);
            };
            btnNo.appendChild(btnNoText);
            elem.appendChild(btnNo);
            /* yes button */
            var btnYes = document.createElement("btnDeleteCommentYes");
            var btnYesText = document.createTextNode(MetaRenderer.i18nMessage('modal.yes'));
            btnYes.setAttribute("class","no-preloader no-ajax waves-effect btn");
            btnYes.onclick = function() {
                deleteInappropriateComment(commentId);
            };
            btnYes.appendChild(btnYesText);
            elem.appendChild(btnYes);
            /* append dialog to body */
            document.body.appendChild(elem);
        }
    }
});

function deleteInappropriateComment(val)
{
    $.ajax({
        type : 'POST',
        url : '/deleteInappropriateComment',
        data : val,
        dataType : 'text',
        contentType : "application/json",
        cache : false,
        processData : false,
        success: function(data){
            var returnedData = JSON.parse(data);
            if( returnedData.success === "true") {
                /* redirect to item's page */
                var arrUrl = window.location.href.split("?");
                window.location.replace(arrUrl[0]);
                console.log("Inappropriate comment deleted!");
            }
            else {
                $("#idDeleteComment").hide();
                console.log("Inappropriate comment not deleted!");
            }
        },
        error : function(XMLHttpRequest, textStatus, errorThrown) {
            console.log("Inappropriate comment not deleted!");
        }
    });
}

function reviewOrganizer(val)
{
    $.ajax({
        type : 'POST',
        url : '/reviewOrganizer',
        data : val,
        dataType : 'text',
        contentType : "application/json",
        cache : false,
        processData : false,
        success: function(data){
            var returnedData = JSON.parse(data);
            if( returnedData.success === "true") {
                console.log("Review organizer sent!");
            }
            else {
                console.log("Review organizer not sent!");
            }
            /* hide review dialog */
            $("#idReview").hide();
        },
        error : function(XMLHttpRequest, textStatus, errorThrown) {
            console.log("Review organizer not sent!");
        }
    });
}

function reviewParticipant(val)
{
    $.ajax({
        type : 'POST',
        url : '/reviewParticipant',
        data : val,
        dataType : 'text',
        contentType : "application/json",
        cache : false,
        processData : false,
        success: function(data){
            var returnedData = JSON.parse(data);
            if( returnedData.success === "true") {
                console.log("Review participant sent!");
            }
            else {
                console.log("Review participant not sent!");
            }
            /* hide review dialog */
            $("#idReview").hide();
        },
        error : function(XMLHttpRequest, textStatus, errorThrown) {
            console.log("Review participant not sent!");
        }
    });
}

function courseOnBeforeCREATE() {
    validateForm();
}

function courseOnBeforeUPDATE() {
    validateForm();
}

function validateForm() {
    var inputs = $('form').children().find('input');
    $.each(inputs, function(index, input) {
        window.validate_field($(input));
    });
}

courseOnCUSTOMSuccess = function (event, $controller, action, responseData, jqXHR, pushState, dataType){
    if(action.uri == "/courses/attendItem/{id}")     //attending item
    {
        updateAttendingOnSuccess(responseData);
    }
}