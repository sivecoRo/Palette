function showContextualHelp(name)
{
    var str = "";

    if(name == "contextualhelp.createprofile.firstname") { str = $("#ch_firstname").val(); }
    else if(name == "contextualhelp.createprofile.lastname") { str = $("#ch_lastname").val(); }
    else if(name == "contextualhelp.createprofile.email") { str = $("#ch_email").val(); }
    else if(name == "contextualhelp.createprofile.password") { str = $("#ch_password").val(); }
    else if(name == "contextualhelp.createprofile.phone") { str = $("#ch_phone").val(); }
    else if(name == "contextualhelp.createprofile.country") { str = $("#ch_country").val(); }
    else if(name == "contextualhelp.createprofile.city") { str = $("#ch_city").val(); }
    else if(name == "contextualhelp.createprofile.dateofbirth") { str = $("#ch_dateofbirth").val(); }
    else if(name == "contextualhelp.createprofile.picture") { str = $("#ch_picture").val(); }
    else if(name == "contextualhelp.createprofile.interests") { str = $("#ch_interests").val(); }
    else { str = "Contextual Help"; }

    /* set text */
    $("#overlay-box-text").text(str);

    /* show dialog */
    $('#overlay-box').modal();
    $('#overlay-box').modal('open');
    /* bind close event */
    $("#overlay-box-close").bind( "click", function() {
        $("#overlay-box").modal('close');
    });
}
