MetaRenderer.addPresentationRendererOverride('userOverview', {
    name: 'MODEL_COLLECTION',
    render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
        var defaultRenderer = MetaRenderer.getDefaultPresentationRenderer('MODEL_COLLECTION'),
            value = model[propertyRenderer.propertyName],
            isNull = !value,
            isArray = !isNull && $.isArray(value),
            items = isNull ? [] : (isArray ? value : value.content),
            $defaultTemplate = MetaRenderer.defaultModelViewTemplate(propertyRenderer.modelName);

        if (propertyRenderer.propertyName === 'userCourses') {
            simpleOverviewCollectionRenderer($propertyElement, items, propertyRenderer, $targetParent, model);
        }
        else {
            defaultRenderer.render(model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer);
        }
    }
});

EventsManager.getInstance().on(EventsManager.HTML_REQUEST, function (event) {
    $(document).ready(function () {
        // $('select').material_select();
    });
});

function evaluateExpressionForUOModel(target, modelName, objectUID) {
    return target.replace("{modelName}", modelName)
    .replace("{objectUID}", objectUID);
}

function evaluateExpressionForUOModelPropertyAndView(target, modelName, propertyName, viewName,
                                                     defaultViewName) {
    return target.replace("{modelName}", modelName)
    .replace("{propertyName}", propertyName)
    .replace("{view}", viewName || '__default')
    .replace('.' + (defaultViewName || '__default'), "");
}

function evaluateUORendererPlaceholderSelector(propertyName) {
    return evaluateExpressionForUOModelPropertyAndView('[data-renderer-placeholder-for={propertyName}]', null, propertyName, null);
}

function doRenderAllElementsFromOneModelStoreItemUO(modelName, modelUid, modelStore, $context, currentValue, extraPropertiesClasses, refRenderer) {
    if ($context && $context.length) {
        var model = currentValue || modelStore.get(modelUid),
            modelViewSelector = '.model-view[data-model-name={modelName}][data-meta-renderer=true][data-model-uid={objectUID}]',
            $elements = $context.find(evaluateExpressionForUOModel(modelViewSelector, modelName, modelUid));
        console.log("Render view", modelName, modelUid ? ('#' + modelUid) : null, "Html elements to update: ",
                    $elements.length);
        $elements.each(function (index, el) {
            try {
                var $element = $(el);
                console.debug('---> [htmlIndex:', index, ']');
                MetaRenderer.renderModelInstance($element, model, modelStore, refRenderer, extraPropertiesClasses);
            } catch (e) {
                console.error(e);
            }
        });

    }
}

function simpleOverviewCollectionRenderer($wrapper, items, propertyRenderer, $targetParent, model) {
    var $collectionContainer = $('<ul class="property-value collection"></ul>'),
        modelViewSelector = '.model-view[data-model-name={modelName}][data-meta-renderer=true][data-model-uid={objectUID}]',
        propertyEmptyValue = 'model.{modelName}.property.{propertyName}.{view}.emptyValueOrCollection.label',
        $defaultTemplate = MetaRenderer.defaultModelViewTemplate(propertyRenderer.modelName, null, 'li'),
        $viewWrapper = $targetParent.closest(evaluateExpressionForUOModel(modelViewSelector, propertyRenderer.propertyHolderModelName, model.objectUID)),
        $template = MetaRenderer.findModelViewRendererTemplate($viewWrapper, propertyRenderer.modelName, null, $defaultTemplate),
        $placeholders = $viewWrapper.find(evaluateUORendererPlaceholderSelector(propertyRenderer.propertyName)),
        $emptyCollection = $('<li class="collection-item empty-collection-wrapper"></li>'),
        emptyMessageCode = evaluateExpressionForUOModelPropertyAndView(propertyEmptyValue, propertyRenderer.propertyHolderModelName, propertyRenderer.propertyName, propertyRenderer.viewName),
        rendererType = $wrapper.data('rendererType') || MetaRenderer.defaultSimpleCollectionSimpleRendererType();

    $('#usersSearch').data("userOverview", model);
    if (items) {
        if (items.length == 0) {
            var defaultItem = {
                id: 0,
                modelName: 'userCourse',
                new: false,
                objectLabel: emptyMessageCode,
                objectUID: 0,
                userResourceStatistics: null,
                userResourceStatisticsDetails: null,
                userResources: null
            };
            items.push(defaultItem);
        }
        $.each(items, function (index) {
            var item = $(this)[0],
                $item = $template.clone().removeClass('renderer-template');
            $item.attr("data-model-uid", item.objectUID);
            $item.addClass('collection-item');
            if (index == 0) {
                $item.addClass('active');
            }
            $item.data({
                           currentValue: item//,
                           //rendererType: rendererType
                       });
            //$item.attr("data-renderer-type", rendererType);
            $item.click(function (e) {
                $item.parent().find('.collection-item.active').removeClass('active');
                $item.addClass('active');
                MetaRenderer.transposeToPlaceholders($placeholders, $(this));
                transposeOverviewSelect(model,$(this));
            });
            $collectionContainer.append($item);
        });
    }
    ModelStore.promiseOf(propertyRenderer.modelName, true).done(function () {
        var modelStore = this, transposedProperties = [], extraPropertiesClasses = {};
        $placeholders.each(function () {
            var transposedProperty = $(this).data('rendererPlaceholderProperty');
            transposedProperties.push(transposedProperty);
        });
        $.each(transposedProperties, function (index, transposedProperty) {
            extraPropertiesClasses[transposedProperty] = ['element-transposed'];
        });
        if (items) {
            $.each(items, function (index) {
                var item = $(this)[0];
                doRenderAllElementsFromOneModelStoreItemUO(propertyRenderer.modelName, item.objectUID, modelStore, $collectionContainer, item, extraPropertiesClasses);
            });
        }
        $collectionContainer.children('.active').first().click();
    });
    $emptyCollection.append('<div class="empty-collection"><div>' + MetaRenderer.i18nMessage(emptyMessageCode) + '</div></div>');
    $collectionContainer.prepend($emptyCollection);
    if (items.length === 0) {
        $emptyCollection.show();
    }
    else {
        $emptyCollection.hide();
    }
    $wrapper.append($collectionContainer);
    $wrapper.addClass('simple-collection-container');
}

function doOnSearchClick() {
    var $search = $('#usersSearch');
    if ($search) {
        filterUserCourses($search.data("userOverview"));
    }
}

function filterUserCourses(model) {
    var $selectedUser = $('#users').find('.collection-item.active'),
        selectedUserData = $selectedUser.data(),
        selectedUserId = selectedUserData == null ? null : selectedUserData.modelUid,
        plannedCourseId = $('#userResourcePlannedCoursesSelect').val(),
        $collectionContainer = $('.property-value.collection'),
        $usersSearch = $('#usersSearch');

    //if( selectedUserId != null ) {
    ModelStore.promiseOf('userCourse', true).done(function () {
        var modelStore = this, transposedProperties = [], extraPropertiesClasses = {},
            $wrapper = $('.model-property-userCourses'),
            propertyRenderer = {
                propertyName: 'userCourses',
                viewName: '_default',
                propertyHolderModelName: 'userOverview',
                modelName: 'userCourse'
            };

        this.remotePageRequest({
                                   id: selectedUserId, plannedCourseId: plannedCourseId,
                                   courseName: $usersSearch.val()
                               }, null, false)
        .done(function (page) {
            if (page.content) {
                var collectionItems = $collectionContainer.find('.collection-item');
                $.each(collectionItems, function (index, collectionItem) {
                    collectionItem.remove();
                });
                simpleOverviewCollectionRenderer($wrapper, page.content, propertyRenderer, $collectionContainer, model);
                $('.collection').first().remove();
            }
        });
    });
    //}
}

function transposeOverviewSelect(model, $el) {
    var $select = $('#userResourcePlannedCoursesSelect'),
        $firstOption = $select.find('option').first();

    $select.empty().append($firstOption);

    $el.find('.model-property-plannedCourses').find('li').each( function(index,item) {
        var id = $(item).data('id'),
            label = $(item).data('label'),
            $option = $('<option value="' + id + '">' + label + '</option>');
        $select.append($option);
    });

    // $select.material_select();

    $select.on('contentChanged', function () {
        // $(this).material_select();
    });
    $select.on('change', function () {
        doOnUserOverviewPlannedCourseChange(model);
    });
    $select.data("userOverview", model);
}

function doOnUserOverviewPlannedCourseChange(model) {

        ModelStore.promiseOf('userCourse', true).done(function () {
            var selectedPlannedCourseId = $('#userResourcePlannedCoursesSelect').val(),
                selectedCourseId = $('.model-property-userCourses').find('li.active').data('modelUid'),
                $hiddenPlannedCourseId = $('#hiddenPlannedCourseId');
            if( $hiddenPlannedCourseId.val() != selectedPlannedCourseId ) {
                $hiddenPlannedCourseId.val(selectedPlannedCourseId);
                this.remotePageRequest({
                                           courseId: selectedCourseId, plannedCourseId: selectedPlannedCourseId
                                       }, null, false)
                .done(function (page) {
                    if (page.content) {
                        var $details = $('[data-group-name="userResourceStatisticsDetails"]')
                        .find('.model-property-userResourceStatisticsDetails'),
                            $chart = $('[data-group-name="userResourceStatistics"]')
                            .find('.model-property-userResourceStatistics'),
                            $grid = $('[data-group-name="userResources"]').find('.model-property-userResources'),
                            model = page.content[0];

                        renderUserOverviewChartDetails(model, $details);
                        renderUserOverviewChart(model, $chart);
                        renderUserOverviewGrid(model, $grid);
                    }
                });
            }
        });
}

function renderUserOverviewChartDetails(model, $propertyElement) {
    var value = model['userResourceStatisticsDetails'],
        activitiesNo = value.activitiesNo,
        score = value.score,
        spentTime = value.spentTime,
        promoted = value.promoted,
        grade = value.grade;
    $propertyElement.children().each(function (index, el) {
        el.remove();
    });
    $propertyElement.append('<li style="list-style: none;">' + activitiesNo + '</li>');
    $propertyElement.append('<li style="list-style: none;">' + score + '</li>');
    $propertyElement.append('<li style="list-style: none;">' + spentTime + '</li>');
    $propertyElement.append('<li style="list-style: none;">' + promoted + '</li>');
    $propertyElement.append('<li style="list-style: none;">' + grade + '</li>');
}

function renderUserOverviewChart(model, $propertyElement) {
    var userResourceStatistics = model['userResourceStatistics'], columnNames = [], columns = [],
        tooltips = [],
        names = [];

    $propertyElement.children().each( function(index,el) {
        el.remove();
    });

    columnNames.push(MetaRenderer.i18nMessage('model.plannedCourseStudent.completed')
                     .replace('{0}', userResourceStatistics.finished + '%'));
    columnNames.push(MetaRenderer.i18nMessage('model.plannedCourseStudent.started')
                     .replace('{0}', userResourceStatistics.learning + '%'));
    columnNames.push(MetaRenderer.i18nMessage('model.plannedCourseStudent.not.started')
                     .replace('{0}', userResourceStatistics.inactive + '%'));

    columns.push([columnNames[0], userResourceStatistics.finished]);
    columns.push([columnNames[1], userResourceStatistics.learning]);
    columns.push([columnNames[2], userResourceStatistics.inactive]);

    names.push(columnNames[0]);
    names.push(columnNames[1]);
    names.push(columnNames[2]);

    $propertyElement.data('chartSettings', {
        columns: columns,
        tooltips: tooltips,
        options: {data: {columns: columns, names: names}}
    });

    var chart = MetaRenderer.chartRenderer($propertyElement, 'pieChart');
    chart.flush();
}

function renderUserOverviewGrid(model,$propertyElement) {
    var propertyRenderer = {
            modelName: 'userResource',
            groupName: 'userResources',
            propertyName: 'userResources',
            propertyHolderModelName: 'userCourse',
            viewName: '_default'},
        $targetParent = $('[data-group-name="userResources"]').find('.properties-wrapper-content');
    $propertyElement.children().each(function (index, el) {
        el.remove();
    });
    MetaRenderer.gridRenderer($propertyElement, propertyRenderer, $targetParent, model);
}

