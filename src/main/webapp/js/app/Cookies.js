/**
 * User: AlexandruVi
 * Date: 2017-12-10
 */

/**
 * Cookies Singleton
 * @constructor
 */
var Cookies = (function () {
    var instance;

    function createInstance() {
        return {
            create: function (name, value, seconds, path) {
                var expires = "";
                if (seconds) {
                    var date = new Date();
                    date.setTime(date.getTime() + (seconds * 1000));
                    expires = "; expires=" + date.toUTCString();
                }
                document.cookie = name + "=" + value + expires + "; path=" + path;
            },

            read: function (name) {
                var nameEQ = name + "=";
                var ca = document.cookie.split(';');
                for (var i = 0; i < ca.length; i++) {
                    var c = ca[i];
                    while (c.charAt(0) == ' ') {
                        c = c.substring(1, c.length);
                    }
                    if (c.indexOf(nameEQ) === 0) {
                        return c.substring(nameEQ.length, c.length);
                    }
                }
                return null;
            },

            delete: function (name) {
                this.create(name, "", -1);
            }
        };
    }

    return {

        /**
         *
         * @returns Object
         */
        getInstance: function () {
            if (!instance) {
                instance = createInstance();
            }
            return instance;
        }
    };
})();
var cookies = Cookies.getInstance();