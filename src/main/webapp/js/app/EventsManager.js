/**
 * User: AlexandruVi
 * Date: 2017-04-27
 */
/**
 * EventsManager Singleton
 * @constructor
 */
var EventsManager = (function () {
    var instance;

    function createInstance() {
        return {
            trigger: function (selector, eventName, params) {
                $(selector).trigger(eventName, params);
                return this;
            },
            fire: function (event, params) {
                var selector = window.document, eventName = event,
                    args = (arguments.length === 1 ? [] : Array.apply(null, arguments)).slice(1);
                if (typeof event === "object") {
                    eventName = event.name;
                    if (event.selector) {
                        selector = event.selector;
                    }
                }
                if (eventName.indexOf('app:ajaxRequest') !== -1) {
                    console.debug('[fire]', eventName, args[0]);
                } else {
                    console.debug('[fire]', eventName, selector);
                }
                this.trigger(selector, eventName, args);
                return this;
            },
            on: function (event, selectorToFilterOrHandler, data, handler) {
                var selector = window.document, eventName = event;
                if (typeof event === "object") {
                    eventName = event.name;
                    if (event.selector) {
                        selector = event.selector;
                    }
                }
                if (typeof selectorToFilterOrHandler === "function") {
                    $(selector).on(eventName, selectorToFilterOrHandler);
                } else {
                    $(selector).on(eventName, selectorToFilterOrHandler, data, handler);
                }
                return this;
            },
            off: function (events, selectorToFilter, handler) {
                var selector = window.document, eventName = event;
                if (typeof event === "object") {
                    eventName = event.name;
                    if (event.selector) {
                        selector = event.selector;
                    }
                }
                $(selector).off(eventName, selectorToFilter, handler);
                return this;
            }
        };
    }

    return {
        HTML_REQUEST: 'app:htmlRequest',
        META_UPDATED: 'app:metaUpdated',
        AJAX_REQUEST_STARTED: 'app:ajaxRequestStarted',
        AJAX_REQUEST_COMPLETED: 'app:ajaxRequestCompleted',
        AJAX_REQUEST_SUCCESS: 'app:ajaxRequestSuccess',
        EXCEPTION: 'app:exception',
        AJAX_EXCEPTION: 'app:ajaxException',
        MODEL_UPDATE: 'app:modelUpdate',
        VIEW_UPDATE: 'app:viewUpdate',
        ACTION_COMPLETED: 'app:actionCompleted',
        ACTION_SUCCESS: 'app:actionSuccess',
        PLAYER_OPEN: 'app:playerOpen',
        PLAYER_CLOSE: 'app:playerClose',
        /**
         * @return {string}
         */
        VIEW_MODEL_UPDATE: function (modelName) {
            return 'app:viewUpdate:' + modelName;
        },

        /**
         * @return {string}
         */
        BEFORE_HTML: function (requestDiscriminator) {
            return 'app:beforeHtml' + (requestDiscriminator ? ':' + requestDiscriminator : '');
        },

        /**
         *
         * @returns Object
         */
        getInstance: function () {
            if (!instance) {
                instance = createInstance();
            }
            return instance;
        }
    };
})();

