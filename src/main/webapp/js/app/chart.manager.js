/**
 * innerHTML property for SVGElement
 * Copyright(c) 2010, Jeff Schiller
 *
 * Licensed under the Apache License, Version 2
 *
 * Works in a SVG document in Chrome 6+, Safari 5+, Firefox 4+ and IE9+.
 * Works in a HTML5 document in Chrome 7+, Firefox 4+ and IE9+.
 * Does not work in Opera since it doesn't support the SVGElement interface yet.
 *
 * I haven't decided on the best name for this property - thus the duplication.
 */
var initialised = false;
var x1 = 0;
var x2 = 0;
var y1 = 0;
var y2 = 0;
(function () {
    var serializeXML = function (node, output) {
        var nodeType = node.nodeType;
        if (nodeType == 3) { // TEXT nodes.
            // Replace special XML characters with their entities.
            output.push(node.textContent.replace(/&/, '&amp;').replace(/</, '&lt;').replace('>', '&gt;'));
        } else if (nodeType == 1) { // ELEMENT nodes.
            // Serialize Element nodes.
            output.push('<', node.tagName);
            if (node.hasAttributes()) {
                var attrMap = node.attributes;
                for (var i = 0, len = attrMap.length; i < len; ++i) {
                    var attrNode = attrMap.item(i);
                    output.push(' ', attrNode.name, '=\'', attrNode.value, '\'');
                }
            }
            if (node.hasChildNodes()) {
                output.push('>');
                var childNodes = node.childNodes;
                for (var i = 0, len = childNodes.length; i < len; ++i) {
                    serializeXML(childNodes.item(i), output);
                }
                output.push('</', node.tagName, '>');
            } else {
                output.push('/>');
            }
        } else if (nodeType == 8) {
            // TODO(codedread): Replace special characters with XML entities?
            output.push('<!--', node.nodeValue, '-->');
        } else {
            // TODO: Handle CDATA nodes.
            // TODO: Handle ENTITY nodes.
            // TODO: Handle DOCUMENT nodes.
            throw 'Error serializing XML. Unhandled node of type: ' + nodeType;
        }
    };
    // The innerHTML DOM property for SVGElement.
    Object.defineProperty(SVGElement.prototype, 'innerHTML', {
        get: function () {
            var output = [];
            var childNode = this.firstChild;
            while (childNode) {
                serializeXML(childNode, output);
                childNode = childNode.nextSibling;
            }
            return output.join('');
        },
        set: function (markupText) {
            // Wipe out the current contents of the element.
            while (this.firstChild) {
                this.removeChild(this.firstChild);
            }

            try {
                // Parse the markup into valid nodes.
                var dXML = new DOMParser();
                dXML.async = false;
                // Wrap the markup into a SVG node to ensure parsing works.
                sXML = '<svg xmlns=\'http://www.w3.org/2000/svg\'>' + markupText + '</svg>';
                var svgDocElement = dXML.parseFromString(sXML, 'text/xml').documentElement;

                // Now take each node, import it and append to this element.
                var childNode = svgDocElement.firstChild;
                while (childNode) {
                    this.appendChild(this.ownerDocument.importNode(childNode, true));
                    childNode = childNode.nextSibling;
                }
            } catch (e) {
                throw new Error('Error parsing XML string');
            }
        }
    });

// The innerSVG DOM property for SVGElement.
    Object.defineProperty(SVGElement.prototype, 'innerSVG', {
        get: function () {
            return this.innerHTML;
        },
        set: function (markupText) {
            this.innerHTML = markupText;
        }
    });

})();

ChartFactory = {
    _c3Generate   : function (opts) {
        return c3.generate($.extend({
            /* color: {
             pattern: ['#7b62a6', '#7b62a6', '#7b62a6', '#7b62a6', '#7b62a6', '#7b62a6', '#7b62a6', '#7b62a6', '#7b62a6', '#7b62a6']
             }*/
        }, opts));
    },
    renderers     : {
        'millis-to-h-m-s': function (millis) {
            return wandReports.renderers['millis-to-h-m-s'](millis);
        },
        'percent'        : function (percent) {
            return percent + "%";
        }
    },
    pieChart      : function ($target, columns, tooltips, options) {

        var me = this, opts = $.extend({
            data : {
                columns: columns
            },
            color: {
                pattern: ['#77AA33', '#F44336', '#d0d0d0']
            },
            size : {
                width : 300,
                height: 175
            },

            tooltip: {
                show   : false,
                grouped: true,
                format : {
                    name : function (name, ratio, id, index) {
                        return "";
                    },
                    value: function (value, ratio, id, index) {
                        return tooltips[index];
                    }
                }
            },
            legend : {
                show    : true,
                position: 'right'
            },
            pie    : {
                label: {
                    show: false
                }
            }
        }, options);
        var chart = me._c3Generate($.extend(true, opts, {
            bindto: $target.get(0),
            data  : {
                type: 'pie'
            }
        }));
        return chart;

    },
    donut         : function ($target, columns, options) {
        var me = this, opts = $.extend({
            data   : {
                columns: columns
            },
            tooltip: {
                show  : true,
                format: {
                    name: function (name, ratio, id, index) {
                        return columns[index][3];
                    }
                }
            },
            legend : {
                show: false
            }
        }, options);
        var chart = me._c3Generate($.extend(true, opts, {
            bindto: $target.get(0),
            data  : {
                type: 'donut'
            }
        }));
    },
    horizontalBars: function ($target, columns, options) {

        $.fn.textWidth = function (text, font) {
            if (!$.fn.textWidth.fakeEl) {
                $.fn.textWidth.fakeEl = $('<span>').hide().appendTo(document.body);
            }
            $.fn.textWidth.fakeEl.text(text || this.val() || this.text()).css('font', font || this.css('font'));
            return $.fn.textWidth.fakeEl.width();
        };

        var longest_text_width = 0;
        var paddingLeft = 0;
        columns[0].forEach(function (data, index) {
            var w = $.fn.textWidth(data, '14px "Source Sans Pro", "Helvetica Neue", "Helvetica", "Arial", "sans-serif"');
            if (w > longest_text_width) {
                longest_text_width = w;
            }

            /*if(longest_text_width > 250) {
             data = data.substring(0, 25) + '[...]';
             console.log(data);
             columns[0][index] = data;
             longest_text_width = $.fn.textWidth(data, '14px "Source Sans Pro", "Helvetica Neue", "Helvetica", "Arial", "sans-serif"');
             }*/

            if (longest_text_width > 250) {
                paddingLeft = 400;
            } else {
                paddingLeft = longest_text_width + 20;
            }
        });
        var me = this, opts = $.extend(true, {
            size   : {
                height: (columns && columns.length > 0 && columns[0] && columns[0].length >= 5) ? columns[0].length * 65 : 350
            },
            padding: {
                left: paddingLeft
            },
            data   : {
                columns: [columns[0], columns[1]]
            },
            colors : {
                'Answers': '#ff0000'
            },
            color  : function (color, d) {
                // d will be 'id' when called for legends
                return d.id && d.id === 'data3' ? d3.rgb(color).darker(d.value / 150) : color;
            },
            bar    : {
                width: {
                    ratio: 0.5// this makes bar width 50% of length between ticks
                }
            },
            tooltip: {
                show    : true,
                contents: function (d, defaultTitleFormat, defaultValueFormat, color) {
                    var toolTipContainer = columns[2];

                    //return this.getTooltipContent(d, defaultTitleFormat, defaultValueFormat, color);
                    if (toolTipContainer && toolTipContainer[d[0].index + 1]) {
                        return '<div class="c3-tooltip">' +
                            '<div class="popover bottom" style="display: block"> ' +
                            '<div class="arrow"></div> ' +
                            '<h3 class="popover-title">' + toolTipContainer[0] + '</h3> ' +
                            '<div class="popover-content"> ' +
                            '<span>' + toolTipContainer[d[0].index + 1] + '</span> ' +
                            '</div> ' +
                            '</div> ' +
                            '</div>';
                    } else {
                        return '<div class="c3-tooltip"></div>';
                    }
                }
            },

            legend: {
                show: false
            }

        }, options);
        var chart = me._c3Generate($.extend(true, opts, {
            bindto: $target.get(0),
            data  : {
                type: 'bar'
            },
            zoom  : {
                enabled: false
            },
            axis  : {
                rotated: true,
                x      : {
                    type: 'category',
                    tick: {
                        fit  : true,
                        width: (longest_text_width > 250) ? paddingLeft - 200 : longest_text_width + 10
                    }
                }

            }

        }));

        var labels = $(".c3-axis-x .tick tspan");
        $.each(labels, function (label) {
            var self = this;
            var currentDy = parseInt($(this).attr('dy'));
            if (currentDy > 0) {
                $(this).attr('dy', currentDy + 4);
            }
        });
        if (lang == 'ar') {
            $('.c3-legend-item-tile').attr('x1', (parseInt($('.c3-legend-item-tile').attr('x1')) + 22));
            $('.c3-legend-item-tile').attr('x2', (parseInt($('.c3-legend-item-tile').attr('x2')) + 22));
            $('.c3-legend-item-tile').attr('y1', (parseInt($('.c3-legend-item-tile').attr('y1')) + 1));
            $('.c3-legend-item-tile').attr('y2', (parseInt($('.c3-legend-item-tile').attr('y2')) + 1));
        }

    },
    verticalBars  : function ($target, columns, options) {
        var studentsIndex = [], arrayIntValues = [], avgV = 0, lang = '';
        var labelText = {
            'ar': 'Avg. Time Spent',
            'en': 'Avg. Time Spent',
            'ro': 'Timpul mediu petrecut'
        };

        $.each(columns[1], function (x, y) {
            studentsIndex.push(x);
        });
        var avgTime = 0;
        $.each(columns[0], function (index, item) {
            if ($.isNumeric(item)) {
                arrayIntValues.push(item);
                avgTime = avgTime + item;

            }
        });
        avgV = avgTime / arrayIntValues.length;

        var lang = $('html').attr('lang').substring(0, 2);
        var me = this, opts = $.extend(true, {
            size: {
                height: 350
            },
            data: {
                columns: [columns[0]]
            },

            tooltip: {
                show: true
            },
            legend : {
                show : true,
                inset: {
                    anchor: 'top-right',
                    x     : 20,
                    y     : 10,
                    step  : 2
                }
            },
            grid   : {
                y: {
                    lines: [

                        {value: avgV, text: labelText[lang], axis: 'y', class: "red"}

                    ]
                }
            },
            bar    : {
                width: {
                    ratio: 0.5
                }
            },

            tooltip: {
                contents: function (d, defaultTitleFormat, defaultValueFormat, color) {
                    var toolTipContainer = columns[1];
                    //return this.getTooltipContent(d, defaultTitleFormat, defaultValueFormat, color);
                    if (toolTipContainer && toolTipContainer[d[0].index + 1]) {
                        return '<div class="c3-tooltip">' +
                            '<div class="popover" style="display: block"> ' +
                            '<div class="arrow"></div> ' +
                            '<h3 class="popover-title">' + toolTipContainer[0] + '</h3> ' +
                            '<div class="popover-content"> ' +
                            '<span>' + toolTipContainer[d[0].index + 1] + '</span> ' +
                            '</div> ' +
                            '</div> ' +
                            '</div>';
                    } else {
                        return '<div class="c3-tooltip"></div>';
                    }
                }
            }
        }, options);

        var chart = me._c3Generate($.extend(true, opts, {
            bindto: $target.get(0),
            data  : {
                type: 'bar'
            },
            zoom  : {
                enabled: false
            },
            axis  : {
                rotated: false
                /*x: {
                 type: 'category',
                 tick: {
                 fit: true
                 },
                 }*/
            }
        }));
        if (!initialised) {
            x1 = parseInt($('.c3-legend-item-tile').attr('x1'));
            x2 = parseInt($('.c3-legend-item-tile').attr('x2'));
            y1 = parseInt($('.c3-legend-item-tile').attr('y1'));
            y2 = parseInt($('.c3-legend-item-tile').attr('y2'));
            initialised = true;
        }

        if (lang == 'ar') {
            $('.c3-legend-item-tile').attr('x1', (x1 + 22));
            $('.c3-legend-item-tile').attr('x2', (x2 + 22));
            $('.c3-legend-item-tile').attr('y1', (y1 + 1));
            $('.c3-legend-item-tile').attr('y2', (y2 + 1));
        }

    }
};

ChartFactory = $.extend(true, ChartFactory, App.charts, {/* add here options that can/must override wandApp.services.chart */});

ChartInitializer = {
    i18n          : {},
    lang          : $('html').attr('lang').substring(0, 2),//get lang from cookie
    decodeEntities: function (encodedString) {
        var textArea = document.createElement('textarea');
        textArea.innerHTML = encodedString;
        return textArea.value;
    },
    tooltipsList  : function () {
        $.each($('.tabs-left.report-activities-list > li'), function () {
            if ($(this).attr('data-toggle') === 'tooltip') {
                if (ChartInitializer.lang === 'ar') {
                    $(this).attr('data-placement', 'left');
                }

            }
        });
    },
    renderers     : {
        'pie-chart'  : function (columnsSelector, $j) {
            var columnsContainer = $j.parent().parent().find(columnsSelector).find('.chart-line'), columns = [],
                names = {}, tooltips = [];
            columnsContainer.each(function () {

                var $item = $(this), property = $item.find('.data').data('property'),
                    value = $item.find('.data').data('value');
                var tooltipValue = $item.find('.chart-tooltip').data('tooltip-value');
                tooltips.push(tooltipValue);
                columns.push([property, value]);
                names[property] = $item.find('.data').data('value') + '% ' + $item.find('.data').data('property');
            });
            ChartFactory.pieChart($j, columns, tooltips, {data: {columns: columns, names: names}});
            return false;
        },
        'donut-chart': function (columnsSelector, $j, categoriesName, valuesName, tooltipName) {
            var $columnsContainer = $j.parent().parent().find(columnsSelector).find('.chart-line'), columns = [],
                properties = [], values = [], tooltips = [], colors = [];

            $columnsContainer.each(function () {
                var $item = $(this);
                properties.push($item.find('.property').data('renderValue'));
                values.push($item.find('.value').data('renderValue'));
                tooltips.push($item.find('.chart-tooltip').data('renderValue') || "");
                columns.push([$item.find('.property').data('renderValue'), $item.find('.property')
                    .text(), $item.find('.value').data('renderValue'), $item.find('.chart-tooltip')
                    .data('tooltip-value') || ""]);
                var $color = $item.find('.color');
                if ($color.data('color')) {
                    $color.css("background-color", $color.data('color'));
                    colors.push($color.data('color'));
                }

            });
            var chartsOpts = {
                tooltip: {show: false},
                donut  : {
                    label: {show: false},
                    title: tooltipName
                },
                data   : {
                    columns    : columns,
                    onmouseover: function (d, i) {
                        var x = function (d, i) {
                            var $target = $(this.bindto),
                                number = tooltips[d.index];

                            //to be changed to ChartInitializer.lang
                            if (ChartInitializer.lang !== 'ar' || ChartInitializer.lang == undefined) {
                                $target.find('.c3-chart-arcs-title')[0].innerHTML = '<tspan x="0" dy="0" class="donut-number">' + number + ' </tspan><tspan x="0" dy="1em" class="donut-label">' + columns[d.index][1] + '</tspan>';
                            } else {
                                $target.find('.c3-chart-arcs-title')[0].innerHTML = '<tspan x="0" dy="0" class="donut-number">' + number + ' </tspan><tspan x="0" dy="0" class="donut-label">' + columns[d.index][1] + '</tspan>';
                            }

                        };

                        x.call(this, d, i);
                    },
                    onmouseout : function (d, i) {
                        var $target = $(this.bindto);
                        $target.find('.c3-chart-arcs-title').html(this.donut_title);

                    }
                }

            };
            if (colors.length > 0) {
                chartsOpts.color = {pattern: colors};
            }

            ChartFactory.donut($j, columns, chartsOpts);
            return false;
        },

        'horizontal-bars-chart': function (chartDataSelector, $j, categoriesName, valuesName, tooltipName) {

            var $chartData = $j.parent().parent().find(chartDataSelector),
                $columnsContainer = $chartData.find('.chart-line'), columns = [],
                properties = [categoriesName], values = [valuesName], tooltips = [tooltipName],
                $axisXFormat = $chartData.find('.axis-format-x'), $axisYFormat = $chartData.find('.axis-format-y');
            $columnsContainer.each(function () {
                var $item = $(this);
                properties.push(ChartInitializer.decodeEntities($item.find('.property').data('renderValue')));
                values.push($item.find('.value').data('renderValue'));
                tooltips.push($item.find('.chart-tooltip').data('renderValue') || "");
            });

            if (categoriesName && categoriesName != "null") {

                columns.push(properties);
            }
            if (valuesName && valuesName != "null") {
                columns.push(values);
            }
            if (tooltipName && tooltipName != "null") {
                columns.push(tooltips);
            }
            var chartsOpts = {
                data   : {
                    x: properties[0]
                },
                axis   : {},
                tooltip: {}
            };
            if ($axisXFormat.length) {
                chartsOpts.axis.x = {
                    tick: {
                        format: ChartFactory.renderers[$axisXFormat.data('renderType')]
                    }
                };
                if ($axisXFormat.data('renderType') == 'percent') {
                    chartsOpts.axis.x.max = 95;
                }
            }
            if ($axisYFormat.length) {
                chartsOpts.axis.y = {
                    tick: {
                        format: ChartFactory.renderers[$axisYFormat.data('renderType')]
                    }
                };
                if ($axisYFormat.data('renderType') == 'percent') {
                    chartsOpts.axis.y.max = 95;
                }
            }

            ChartFactory.horizontalBars($j, columns, chartsOpts);
            return false;
        },
        'vertical-bars-chart'  : function (chartDataSelector, $j, categoriesName, valuesName, tooltipName, categoryData) {
            var $chartData = $j.parent().parent().find(chartDataSelector),
                $columnsContainer = $chartData.find('.chart-line'), columns = [],
                properties = [categoriesName], values = [valuesName], tooltips = [tooltipName],
                $axisXFormat = $chartData.find('.axis-format-x'), $axisYFormat = $chartData.find('.axis-format-y');
            $columnsContainer.each(function () {
                var $item = $(this);
                properties.push($item.find('.property').data('renderValue'));
                values.push($item.find('.value').data('renderValue'));
                tooltips.push($item.find('.chart-tooltip').data('renderValue') || "");
            });

            if (categoriesName && categoriesName != "null") {
                columns.push(properties);
            }
            if (valuesName && valuesName != "null") {
                columns.push(values);
            }
            if (tooltipName && tooltipName != "null") {
                columns.push(tooltips);
            }
            var chartsOpts = {data: {}, axis: {}, tooltip: {}};

            if ($axisXFormat.length) {
                chartsOpts.axis.x = {
                    tick: {
                        format: ChartFactory.renderers[$axisXFormat.data('renderType')]
                    }
                };
            }
            if ($axisYFormat.length) {
                chartsOpts.axis.y = {
                    tick: {
                        format: ChartFactory.renderers[$axisYFormat.data('renderType')]
                    }
                };
            }

            ChartFactory.verticalBars($j, columns, chartsOpts);
            return false;
        },
        'views'                : function () {
            var elementViews = $('.item .value.views'),
                elementCorrect = $('.item .value.correct'),
                elementWrong = $('.item .value.wrong'),
                array = [];

            if (ChartInitializer.lang == 'ar') {
                var doSomething = function () {
                    array = $(this).text().split('/');
                    if (array !== undefined) {
                        $(this).html(array[1] + '\\' + array[0]);
                    }
                };
                $.each(elementViews, doSomething);
                $.each(elementCorrect, doSomething);
                $.each(elementWrong, doSomething);
            }
        },
        'stars-rating'         : function (floatVal, $j) {
            var ratingValue = parseFloat(floatVal), htmlEmptyStar = '<i class="fa fa-star-o"/>',
                htmlHalfStar = '<i class="fa fa-star-half-o"/>', htmlFullStar = '<i class="fa fa-star"/>',
                rating = '';
            rating += ratingValue > 0 ? (ratingValue < 1 ? htmlHalfStar : htmlFullStar) : htmlEmptyStar;
            rating += ratingValue > 1 ? (ratingValue < 2 ? htmlHalfStar : htmlFullStar) : htmlEmptyStar;
            rating += ratingValue > 2 ? (ratingValue < 3 ? htmlHalfStar : htmlFullStar) : htmlEmptyStar;
            rating += ratingValue > 3 ? (ratingValue < 4 ? htmlHalfStar : htmlFullStar) : htmlEmptyStar;
            rating += ratingValue > 4 ? (ratingValue < 5 ? htmlHalfStar : htmlFullStar) : htmlEmptyStar;
            return rating;
        },
        'percent'              : function (floatVal, $j, divide, decimals) {
            if (divide == null || divide == 0) {
                divide = 1;
            }
            if (decimals == null) {
                decimals = 0;
            }
            if (floatVal) {
                return ((floatVal / divide) * 100).toFixed(decimals) + " %";
            }
            else {
                return "0 %";
            }
        },

        'normal': function (floatVal, $j) {
            if (floatVal != null) {
                return floatVal;
            } else {
                return 0;
            }
        },

        'correct-incorrect': function (isCorrect, $j) {
            isCorrect ? $j.css("color", "green") : $j.css("color", "red");
            return $j.html();
        },

        'complete-incomplete': function (longValue, $j) {
            return longValue == 0 ? ChartInitializer.i18n['renderer.incomplete'] : ChartInitializer.i18n['renderer.complete'];
        },

        'millis-to-h-m-s': function (millis, $j) {
            if (millis == null) {
                return ChartInitializer.i18n['renderer.date.hms.undefined'];
            }
            var h = Math.floor((millis / (1000 * 60 * 60)) % 60), m = Math.floor(((millis / (1000 * 60)) % 60)),
                s = Math.floor(((millis / 1000) % 60));
            if (ChartInitializer.lang == 'ar') {
                return ChartInitializer.i18n['renderer.date.format.sec'] + ' ' + s + ' ' + ChartInitializer.i18n['renderer.date.format.min'] + ' ' + m + ' ' + (h > 0 ? (ChartInitializer.i18n['renderer.date.format.hour']) + ' ' + h : '');
            } else {
                return (h > 0 ? (h + ' ' + ChartInitializer.i18n['renderer.date.format.hour']) : '') + ' ' + m + ' ' + ChartInitializer.i18n['renderer.date.format.min'] + ' ' + s + ' ' + ChartInitializer.i18n['renderer.date.format.sec'];
            }
        }
    },
    render        : function ($target, promise) {
        var me = this, _renderNow = function ($targetToRender, data) {
            var argsToPassToRenderer = [data.renderValue, $targetToRender];
            if (data.renderExtraArgs) {
                var extraArgs = typeof data.renderExtraArgs == "string" ? data.renderExtraArgs.split(",") : [data.renderExtraArgs];
                for (var i = 0; i < extraArgs.length; i++) {
                    argsToPassToRenderer.push(extraArgs[i]);
                }
            }

            var renderer = me.renderers[data.renderType], html = renderer.apply(null, argsToPassToRenderer);
            if (html !== false) {
                $targetToRender.html(html);
            }
        };

        $target.each(function () {
            try {
                var $targetToRender = $(this), data = $targetToRender.data();
                if (data.renderDeferred && promise) {
                    promise.then(function (resolvedData) {
                        setTimeout(function () {
                            _renderNow($targetToRender, data);
                        }, 100);
                    });
                } else {
                    _renderNow($targetToRender, data);
                }
            } catch (e) {
                console.error(e);
            } finally {
            }
        });
    },
    load          : function (href, $targetContentPane) {
        var me = this, request, urlPart = href, fragment;
        if (href.indexOf(" ") > 0) {
            var hrefComponents = href.split(/\s/);
            if (hrefComponents.length == 2) {
                urlPart = hrefComponents[0];
                fragment = hrefComponents[1];
            }
        }
        $targetContentPane.addClass('overlay');
        request = $.get(urlPart);
        var htmlApplyer = $.Deferred();
        var onSuccess = function (response, status, xhr) {
            htmlApplyer.promise($targetContentPane.html(fragment ? $(response).find(fragment) : response));
            htmlApplyer.resolve($targetContentPane);
            me.render($targetContentPane.find('[data-render=true]'), $targetContentPane.promise());

        }, onFail = function (xhr, status, error) {
            if (status == "error") {
                var notifArray = [{
                    type    : 'error',
                    message : 'reports.generic.error',
                    title   : 'notification.error.title',
                    msgParam: []
                }];
                addNotification(notifArray);
            }
        };
        request.always(function () {
            $targetContentPane.removeClass('overlay');
        }).then(onSuccess, onFail);
    },
    navigate      : function (e) {
        var me = ChartInitializer, $currentPill = $(this), currentPillId = $currentPill.data('questionLink'),
            currentPillHref = e.target.href, $currentPillLi = $currentPill.parent(),
            targetSelector = '#' + currentPillId + '-pill', $targetContentPane = $(targetSelector);
        e.preventDefault();
        if ($currentPillLi.hasClass('active')) {
            //just reload the content
            me.load(currentPillHref, $targetContentPane);
        } else {
            //remove active pill class
            $currentPillLi.siblings().removeClass('active');
            $currentPillLi.addClass('active');

            //hide all
            $targetContentPane.siblings().removeClass('active');
            $targetContentPane.siblings().hide();

            //show current pill target
            $targetContentPane.show();
            $targetContentPane.addClass('active');
            //load the content
            me.load(currentPillHref, $targetContentPane);
        }

    },
    init          : function () {
        var me = this;

        //me.tooltipsList();
        //force focus on aggregated on activity show
        //$('.report-activities-list a.report-activities-menu-item.report-activity').on('show.bs.tab', function (e) {
        //    $('#' + e.target.id + '-aggregated').click();
        //});
        //
        ////handle show/hide/loading aggregated/individual pagination (dynamic dom binding here)
        //$(document).on('click', '.report-activity-wrapper a.report-activity-second-level, .report-activity-container
        // a.report-activity-question-level', me.navigate);

        //apply renderers for #overview
        me.render($('[data-render=true]'));
        ChartInitializer.tooltipsList();
    }
};
//ChartInitializer = $.extend(true, ChartInitializer, wandApp.reports, {/* add here options that can/must override
// wandApp.services.reports */});

/* start */
(function () {
    $(document).ready(function () {
        ChartInitializer.init();
    });
})();

//
//
//wandApp = {
//        reports:{
//            i18n:{
//                'assignments.status.SCHEDULED':[[#{assignments.status.scheduled}]],
//                'assignments.status.CLOSED':[[#{assignments.status.closed}]],
//                'assignments.status.ACTIVE':[[#{assignments.status.active}]],
//                'renderer.date.format.hour': [[#{renderer.date.format.hour}]],
//                'renderer.date.format.min': [[#{renderer.date.format.min}]],
//                'renderer.date.format.sec': [[#{renderer.date.format.sec}]],
//                'renderer.date.hms.undefined': [[#{renderer.date.hms.undefined}]] | '0 h 0 min',
//                'renderer.incomplete': [[#{renderer.incomplete}]],
//                'renderer.complete': [[#{renderer.complete}]]
//            }
//        }
//    }
