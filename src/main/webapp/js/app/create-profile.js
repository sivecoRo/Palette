var user= {};
var selectedInterest = []; //contains id of selected user interests
var uniqueEmail = false;
var validEmail = false;
var validConfirmEmail = false;
var validPassword = false;
var validConfirmPassword = false;
var validCity = false;
var validCountry = false;
var validFirstName = false;
var validPhoneNumber = false;

var placeSearch, autocomplete;
var componentForm = {
    street_number: 'short_name',
    route: 'long_name',
    locality: 'long_name',
    administrative_area_level_1: 'short_name',
    country: 'long_name',
    postal_code: 'short_name'
};

function completeUserFirstStep()
{
    validateFirstName();
    validateEmail();
    validateConfirmEmail();
    validatePassword();
    validateConfirmPassword();
    validatePhoneNumber();
    validateCountry();

    if(uniqueEmail == true &&
       validEmail == true &&
       validConfirmEmail == true &&
       validPassword == true &&
       validConfirmPassword &&
       validFirstName == true &&
       validCity == true &&
       validCountry == true &&
       validPhoneNumber == true)
    {
        changeTab(3);
    }
}

// obsolete
//function completeUser()
//{
//    var year = $("#year_of_birth").val();
//    var month = $("#month_of_birth").val();
//    var day = $("#day_of_birth").val();
//
//    if(year == "" && month == "" && day == "") /* valid */
//    {
//        $("#birth_date_validation_err_span").css("display", "none");
//    }
//    else /* validate */
//    {
//        if(year != "" && month != "" && day != "")
//        {
//            /* valid */
//            user["birth_date"] = new Date(year + '-' + month + '-' + day + ' 12:00');
//            $("#birth_date_validation_err_span").css("display", "none");
//        }
//        else /* invalid */
//        {
//            $("#birth_date_validation_err_span").css("display", "block");
//            return;
//        }
//    }
//
//    /* save data to user object */
//    user["gender"] = $("#gender").val();
//    user["first_name"] = $("#first_name").val();
//    user["last_name"] = $("#last_name").val();
//    user["mail"] = $("#mail").val();
//    user["password"] = $("#password").val();
//    user["phone"] = $("#phone").val();
//    user["country"] = $("#countries").val();
//    user["acceptTerms"] = $("#termsCheckbox").prop("checked");
//
//    /* next tab */
//    changeTab(3);
//}

function saveProfile()
{
    /* interests */
    user["preferences_interests"] = $('#displayUserInterests').is(':checked');

    /* visibility */
    user["show_city"] = $('#displayUserCity').is(':checked');
    user["show_country"] = $('#displayUserCountry').is(':checked');
    user["show_gender"] = $('#displayUserGender').is(':checked');
    user["show_birth_date"] = $('#displayUserBirthDate').is(':checked');
    user["show_phone"] = $('#displayUserPhone').is(':checked');
    user["show_email"] = $('#displayUserMail').is(':checked');
    user["show_interests"] = $('#displayUserInterests').is(':checked');

    $.ajax({
        type : 'POST',
        url : "/completeProfile",
        data : JSON.stringify(user),
        cache : false,
        processData : false,
        contentType : "application/json",
        success: function(data){
            var returnedData = JSON.parse(data);
            if( returnedData.success === "true") {
                document.location.href = returnedData.redirect;
            }
            else { alert("Error: cannot create the profile"); }
        },
        error : function(XMLHttpRequest, textStatus, errorThrown) {
            alert($(".messages [message-attr='register.user.error']").text());
        }
    });
}

function validatePhoneNumber()
{
    var phoneNumberValue = $("#phone").val();

    if(phoneNumberValue == "")
    {
        validPhoneNumber = true;
    }
    else {
        /* allow numbers only */
        if(phoneNumberValue.match(/^[0-9]+$/) != null) {
            validPhoneNumber = true;
        }
        else {
            validPhoneNumber = false;
        }
    }
    displayErrorsPhoneNumber();
}

function validateFirstName()
{

    var firstNameValue = $("#first_name").val();

    if(firstNameValue != "") {
        validFirstName = true;
    }
    else {
        validFirstName = false;
    }
    displayErrorsFirstName();
}

// obsolete
/*
function validateUsername()
{
    var userNameValue = $("#username").val();
    $.ajax({
            type : 'POST',
            url : "/validateUsername",
            data : JSON.stringify(userNameValue),
            cache : false,
            processData : false,
            contentType : "application/json",
            success: function(data){
                var returnedData = JSON.parse(data);
                if( returnedData.exists === "false") {
                    validUsername = true;
                }
                else {
                    validUsername = false;
                }
            },
            error : function(XMLHttpRequest, textStatus, errorThrown) {
                alert($(".messages [message-attr='register.user.error']").text());
            }
        });
}
*/

function validateEmail()
{
    showLoader();
    validEmail = document.getElementById("mail").checkValidity();
    if(validEmail == true)
    {
        var mailValue = $("#mail").val();
        $.ajax({
                type : 'POST',
                url : "/validateEmail",
                data : JSON.stringify(mailValue),
                async: false,
                cache : false,
                processData : false,
                contentType : "application/json",
                success: function(data){
                    var returnedData = JSON.parse(data);
                    if( returnedData.exists === "false") {
                        uniqueEmail = true;
                    }
                    else {
                        uniqueEmail = false; /* user already exists (in our case email) */
                    }
                    hideLoader();
                },
                error : function(XMLHttpRequest, textStatus, errorThrown) {
                    alert($(".messages [message-attr='register.user.error']").text());
                }
            });
    }
    else {
        hideLoader();
    }

    displayErrorsEmail();
}

function validateConfirmEmail()
{
    var mailValue = $("#mail").val();
    var confirmMailValue = $("#confirmMail").val();

    if(mailValue != "" && confirmMailValue != mailValue) {
        validConfirmEmail = false;
    }
    else {
        validConfirmEmail = true;
    }
    displayErrorsConfirmEmail();
}

function validatePassword()
{
    var password = $("#password").val();
    var containsDigits = /[0-9]/.test(password);
    var containsUpper = /[A-Z]/.test(password);
    var containsLower = /[a-z]/.test(password);

    if(password.length >= 6 && containsDigits && containsUpper && containsLower) {
        validPassword = true;
    }
    else {
        validPassword = false;
    }
    displayErrorsPassword();
}

function validateConfirmPassword()
{
    if($("#password").val() == $("#confirmPassword").val()) {
        validConfirmPassword = true;
    }
    else {
        validConfirmPassword = false;
    }
    displayErrorsConfirmPassword();
}

function validateCountry()
{
    if($("#countries").val() == "") {
        validCountry = false;
    }
    else {
        validCountry = true;
    }
    displayErrorsCountry();
}

function displayErrorsPhoneNumber()
{
    if(validPhoneNumber) {
        $("#phone_number_validation_err_span").css("display", "none");
    }
    else {
        $("#phone_number_validation_err_span").css("display", "block");
    }
}

function displayErrorsFirstName()
{
    if(validFirstName) {
        $("#first_name_validation_err_span").css("display", "none");
    }
    else {
        $("#first_name_validation_err_span").css("display", "block");
    }
}

function displayErrorsPassword()
{
    if(validPassword) {
        $("#password_validation_err_span").css("display", "none");
    }
    else {
        $("#password_validation_err_span").css("display", "block");
    }
}

function displayErrorsConfirmPassword()
{
    if(validConfirmPassword) {
        $("#confirmPassword_validation_err_span").css("display", "none");
    }
    else {
        $("#confirmPassword_validation_err_span").css("display", "block");
    }
}

function displayErrorsEmail()
{
debugger;
    if($("#mail").val().length == 0) {
        $("#email_empty_validation_err_span").css("display", "block");
        $("#email_validation_err_span").css("display", "none");
        $("#email_unique_validation_err_span").css("display", "none");
    }
    else if(uniqueEmail == true && validEmail == true){
        $("#email_validation_err_span").css("display", "none");
        $("#email_unique_validation_err_span").css("display", "none");
        $("#email_empty_validation_err_span").css("display", "none");
    }
    else{
        $("#email_empty_validation_err_span").css("display", "none");
        /* email already exists */
        if(uniqueEmail == false && validEmail == true) {
            $("#email_unique_validation_err_span").css("display", "block");
            $("#email_validation_err_span").css("display", "none");
            $("#confirmEmail_validation_err_span").css("display", "none");
        }
        else { /* validEmail == false */
            $("#email_unique_validation_err_span").css("display", "none");
            $("#email_validation_err_span").css("display", "block");
            $("#confirmEmail_validation_err_span").css("display", "block");
        }
    }
}

function displayErrorsConfirmEmail()
{
    if(validConfirmEmail) {
        $("#confirmEmail_validation_err_span").css("display", "none");
    }
    else {
        if($("#mail").val().length > 0) {
            $("#confirmEmail_validation_err_span").css("display", "block");
        }
    }
}

function displayErrorsCountry()
{
    if(validCountry) {
        $("#country_validation_err_span").css("display", "none");
    }
    else {
        $("#country_validation_err_span").css("display", "block");
    }
}

function displayErrorsCity()
{
    if(validCountry)
    {
        if(validCity) {
            $("#city_validation_err_span").css("display", "none");
        }
        else {
            $("#city_validation_err_span").css("display", "block");
        }
    }
    else {
        $("#city_validation_err_span").css("display", "none");
    }
}

function displayValidationErrors()
{
    displayErrorsFirstName();
    displayErrorsPassword();
    displayErrorsEmail();
    displayErrorsConfirmEmail();
    displayErrorsCountry();
    displayErrorsCity();
}

function changeTab(tab_id)
{
    $("#createProfileDiv").css("display", "none");
    //$("#completeProfileDiv").css("display", "none");
    $("#interestsDiv").css("display", "none");
    $("#summaryDiv").css("display", "none");

    $("#tabButton1").removeClass("tabButtonSelected");
    //$("#tabButton2").removeClass("tabButtonSelected");
    $("#tabButton3").removeClass("tabButtonSelected");
    $("#tabButton4").removeClass("tabButtonSelected");

    $("#tabButton1").removeClass("tabButtonCompleted");
    //$("#tabButton2").removeClass("tabButtonCompleted");
    $("#tabButton3").removeClass("tabButtonCompleted");
    $("#tabButton4").removeClass("tabButtonCompleted");

    if(tab_id == '1')
    {
        $("#createProfileDiv").css("display", "block");
        $("#tabButton1").addClass("tabButtonSelected");
    }
    //else if(tab_id == '2')
    //{
    //    $("#completeProfileDiv").css("display", "block");
    //    $("#tabButton2").addClass("tabButtonSelected");
    //    $("#tabButton1").addClass("tabButtonCompleted");
    //}
    else if(tab_id == '3')
    {
        $("#interestsDiv").css("display", "block");
        $("#tabButton3").addClass("tabButtonSelected");
        $("#tabButton1").addClass("tabButtonCompleted");
        //$("#tabButton2").addClass("tabButtonCompleted");
    }
    else if(tab_id == '4')
    {
        $("#summaryDiv").css("display", "block");
        $("#tabButton4").addClass("tabButtonSelected");
        $("#tabButton1").addClass("tabButtonCompleted");
        //$("#tabButton2").addClass("tabButtonCompleted");
        $("#tabButton3").addClass("tabButtonCompleted");

        //load data
        $('#summaryCity').val(user["city_name"]);
        $('#summaryCountry').val($("#countries option:selected").text());  //$('#summaryCountry').val(user["country"]);

//        /* birth date */
//        if(user["birth_date"] != null && user["birth_date"] != "") {
//            $('#summaryBirthDate').val(user["birth_date"].toLocaleDateString());
//            $('#displayUserBirthDate').prop('checked', true);
//        }
//        else {
//            $('#displayUserBirthDate').prop('checked', false);
//        }

        /* phone number */
        if(user["phone"] == "") {
            $('#displayUserPhone').prop('checked', false);
        }
        else {
            $('#displayUserPhone').prop('checked', true);
            $('#summaryPhone').val(user["phone"]);
        }

        if(user["gender"] == "-1") {
            $('#displayUserGender').prop('checked', false);
        }
        else {
            $("#summaryGender").val($("#gender option:selected").text());
            $('#displayUserGender').prop('checked', true);
        }

        /* email */
        $('#summaryMail').val(user["mail"]);
    }
}

//Profile image functions
function triggerFileUpload()
{
    $('#profileImgInput').trigger('click');
}

function getBase64(file) {
   var reader = new FileReader();
   reader.readAsDataURL(file);
   reader.onload = function () {
     console.log(reader.result);
     //save image to profile data
     /* we support images in .png and .jpg format only */
     user["image"] = reader.result.replace("data:image/png;base64,","").replace("data:image/jpeg;base64,","");
   };
   reader.onerror = function (error) {
     console.log('Error: ', error);
   };
}

function showImage(src, target) {
    var fr = new FileReader();
    fr.onload = function(){
        target.src = fr.result;
    }
    fr.readAsDataURL(src);
}

function getFiles(){
    var file = document.getElementById("profileImgInput").files[0];
    console.log(file)
    getBase64(file); // prints the base64 string

    //show selected profile image
    var profileImgElement = document.getElementById("profileImg");
    showImage(file,profileImgElement);
    //summary img
    var summaryProfileImgElement = document.getElementById("profileImgSummary");
    showImage(file,summaryProfileImgElement);
}

//Cities autocomplete
var placeSearch, autocomplete;
var componentForm = {
street_number: 'short_name',
route: 'long_name',
locality: 'long_name',
administrative_area_level_1: 'short_name',
country: 'long_name',
postal_code: 'short_name'
};

var options = {
types: ['geocode'],
componentRestrictions: {country: "si"}
};

function reinitializeAutocomplete()
{
    $("#cities").val("");
    var selectedCountryCode = $("#countries").val();
    validCountry = true;
    validCity = false;

    /* setting the selected country code */
    if(selectedCountryCode == "Slovenia") selectedCountryCode = "si";
    else if (selectedCountryCode == "Poland") selectedCountryCode = "pl";
    else if (selectedCountryCode == "Romania") selectedCountryCode = "ro";
    else if (selectedCountryCode == "Suisse") selectedCountryCode = "ch";
    else if (selectedCountryCode == "Netherlands") selectedCountryCode = "nl";
    else {
        selectedCountryCode = "";
        validCountry = false;
    }

    //reinitialize options with selected country
    options = {
        types: ['geocode'],
        componentRestrictions: {country: selectedCountryCode}
    };

    initAutocomplete(options);

    displayErrorsCountry();
    displayErrorsCity();
}

function initAutocomplete(options) {
    // Create the autocomplete object, restricting the search to geographical
    // location types.
    autocomplete = new google.maps.places.Autocomplete(
    /** @type {!HTMLInputElement} */
        (document.getElementById('cities')),options);
    //{types: ['geocode']});
    // When the user selects an address from the dropdown, populate the address
        autocomplete.addListener('place_changed', fillInAddress
    );
}

function fillInAddress() {
    // Get the place details from the autocomplete object.
    var place = autocomplete.getPlace();
    if(typeof place != 'undefined') {
        user["city"] = place.geometry.location.lat() + ";" + place.geometry.location.lng(); /* coordinates */
        user["city_name"] = place.name; /* city name */
        validCity = true;
        validCountry = true;
        displayErrorsCity();
    }
}

function createProfileChangeImage(obj, id) {
    var bb = $(obj);
    if (bb.hasClass("img-default")) {
        bb.removeClass("visible-image");
        bb.addClass("hidden-image");
        if (bb.parent().nearest(".img-selected").hasClass("hidden-image")) {
            bb.parent().nearest(".img-selected").removeClass("hidden-image");
            bb.parent().nearest(".img-selected").addClass("visible-image")
        }
        selectedInterest.push(id);
    }
    else if (bb.hasClass("img-selected")) {
        bb.removeClass("visible-image");
        bb.addClass("hidden-image");
        if (bb.parent().nearest(".img-default").hasClass("hidden-image")) {
            bb.parent().nearest(".img-default").removeClass("hidden-image");
            bb.parent().nearest(".img-default").addClass("visible-image");
        }
        //remove interest id from selected interests array
        var index = $.inArray(id, selectedInterest)
        selectedInterest.splice(index,1);
    }
}

function completeInterests(){

    var selectedInterestsElements = $( ".img-selected.visible-image" );
    $("#selectedInterestsDiv").empty();

    for(i = 0; i < selectedInterestsElements.length; i++) {
        $("#selectedInterestsDiv").append("<div class='selectedInterestsDivElement'><img src='"+selectedInterestsElements[i].src+"'/><h6>"+selectedInterestsElements[i].getAttribute("tagValue")+"</h6></div>");
    }

    if(selectedInterestsElements.length == 0)
    {
        /* TODO */
        // show localized property if no interests were selected */
        //$("#selectedInterestsDiv").append("<div style='padding:20px'>No Interests Selected</div>");
    }

    /* save data to user object */
    user["selectedInterests"] = selectedInterest;
    user["gender"] = $("#gender").val();
    user["first_name"] = $("#first_name").val();
    user["last_name"] = $("#last_name").val();
    user["mail"] = $("#mail").val();
    user["password"] = $("#password").val();
    user["phone"] = $("#phone").val();
    user["country"] = $("#countries").val();
    user["acceptTerms"] = $("#termsCheckbox").prop("checked");

    //summary tab
    changeTab(4);
}

//terms conditions and informed consent check
function termsConditionsAndInformedConsentCheck()
{
debugger;
    var agreedToTerms = $("#termsCheckbox").prop("checked");
    var agreedToInformedConsent = $("#informedConsentCheckbox").prop("checked");

    if(agreedToTerms == true && agreedToInformedConsent == true)
    {
        $('#btnCreateProfileFirstStep').prop('disabled', false);
        $('#btnSaveProfile').prop('disabled', false);
    }
    else
    {
        $('#btnCreateProfileFirstStep').prop('disabled', true);
        $('#btnSaveProfile').prop('disabled', true);
    }
}
function openTermsModal() {
    $("#terms-and-conditions-modal").modal({
        dismissible: false
    }).modal('open');
}

function closeTermsAndConditionsModalAccept() {
    $('#terms-and-conditions-modal').modal('close');
    $('#termsCheckbox').prop('checked', true);
    termsConditionsAndInformedConsentCheck();
}

function closeTermsAndConditionsModalDecline() {
    $('#terms-and-conditions-modal').modal('close');
    $('#termsCheckbox').prop('checked', false);
    termsConditionsAndInformedConsentCheck();
}

function openInformedConsentModal() {
    $("#informed-consent-modal").modal({
        dismissible: false
    }).modal('open');
}

function closeInformedConsentModalAccept() {
    $('#informed-consent-modal').modal('close');
    $('#informedConsentCheckbox').prop('checked', true);
    termsConditionsAndInformedConsentCheck();
}

function closeInformedConsentModalDecline() {
    $('#informed-consent-modal').modal('close');
    $('#informedConsentCheckbox').prop('checked', false);
    termsConditionsAndInformedConsentCheck();
}

function handleVisibilityPassword() {
    var pass = document.getElementById("password");
    var conf = document.getElementById("confirmPassword");
    if(pass.type === "password" && conf.type === "password") {
        pass.type = "text";
        conf.type = "text";
    }
    else {
        pass.type = "password";
        conf.type = "password";
    }
}

/* on init load first tab */
$(document).ready(function(){
    changeTab('1');
});
