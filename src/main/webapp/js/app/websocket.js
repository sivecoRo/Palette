var stompClient = null;
var socket = null;
var username = null;
var userArray = [];
var newUserArray = [];

function connect() {
    socket = new SockJS('/navbarEndPoint');
    stompClient = Stomp.over(socket);
    stompClient.connect('', '', function (frame) {
        username = frame.headers['user-name'];

        stompClient.subscribe('/user/queue/navbarNumbers', function (resp) {
            setNavbarNumbers(JSON.parse(resp.body));
        });

        stompClient.send('/aelChat/getNavbarNumbers', {}, '');
    });

}

function disconnect() {
    stompClient.disconnect();
}

function hideMarkAllReadLink() {
    $("#markAllReadDropdown").hide();
    $("#notificationNrId").remove();
}

function setNavbarNumbers(resp) {
    $("#mailIconId").remove("#mailNrId");
    $("#notificationIconId").remove("#notificationNrId");
    $("#mail-dropdown").html("");
    $("#notification-dropdown").empty()
        .append($('<li class="info"><span class="left">' + app.g11n.lang.app.model.notification.header + '</span> <a id="markAllReadDropdown" href="' + app.g11n.lang.app.model.notification.action.markAllAsRead.href + '" class="right" data-rest-method="POST" data-rest-on-success="hideMarkAllReadLink">' + app.g11n.lang.app.model.notification.action.markAllAsRead.label + '</a></li>'));

    if (resp.unreadMsgNr != "0") {
        var unreadMessagesNr = parseInt(resp.unreadMsgNr),
            elem = $('<span id="mailNrId" class="new badge red" data-badge-caption="" style="position: absolute; top: 20%; margin-left: 20px; ' +
                'padding:0; min-width: 16px; max-width: inherit; height: 16px; line-height: 1.4; text-align: center;">'),
            $refreshButton = $('button#refreshMessages');
        elem.html(resp.unreadMsgNr);
        $("#mailIconId").append(elem);
        if ($refreshButton.length) {
            if ($refreshButton.data('currentValue') !== unreadMessagesNr) {
                $refreshButton.data('currentValue', unreadMessagesNr);
                $refreshButton.click();
            }
        }

        var list = resp.lastMsg;
        $.each(list, function (index, value) {
            $("#mail-dropdown").append($('<li><span class="title">' + value.from + '</span><br/><p>' + value.subject + '</p></li>'));
        });

    }
    if (resp.unreadNotifNr != "0") {
        var elem = $('<span id="notificationNrId" class="new badge red" data-badge-caption="" style="position: absolute; top: 20%; margin-left: 20px; ' +
            'padding:0; min-width: 16px; max-width: inherit; height: 16px; line-height: 1.4; text-align: center;">');
        elem.html(resp.unreadNotifNr);
        $("#notificationIconId").append(elem);
    }
    else {
        $("#notificationNrId").remove();
        $("#markAllReadDropdown").hide();
    }
    var list = resp.lastNotif;
    $.each(list, function (index, value) {
        if (index >= 5) {
            return false;
        }
        var $notification = $('<li><span class="sender-wrapper" data-app-plugin="avatar">' + value.from + '</span><time datetime="' + value.sendDate + '" data-app-plugin="timeAgo" data-has-tool-tip="false" data-allow-past="true" data-allow-future="false">' + value.sendDate + '</time><p>' + MetaRenderer.i18nMessage(value.subject) + '</p></li>');
        if (value.readDate !== null) {
            $notification.addClass("is-read");
        }

        $notification.find(".sender-wrapper").data("user", value.sender);

        $("#notification-dropdown").append($notification);
    });
    $("#notification-dropdown")
        .append($('<li class="info center"><a href="' + app.g11n.lang.app.model.notification.action.viewAll.href + '" onClick="onClickUserNotificationMenu()" class="center no-ajax">' + MetaRenderer.i18nMessage('model.notification.action.viewAll.label') + '</a></li>'));
    EventsManager.getInstance().fire({
        name: EventsManager.HTML_REQUEST,
        selector: $("#notification-dropdown")
    });
}

$(document).ready(function () {
    connect();
});
