/**
 * User: AlexandruVi
 * Date: 2017-09-08
 */
/**
 * MetaController Utility
 * @constructor
 */
var MetaController = function () {
    /* Private static variables and constants */
    var commonConstants = {
        X_FRAGMENT_HEADER: "x-fragment",
        X_EMBEDDED_FRAGMENT_HEADER: "x-embedded-fragment",
        CONTROLLER_CLASS: 'meta-controller',
        DATA_TARGET: 'data-target',
        NO_AJAX_CLASS: 'no-ajax'
    };
    /* Private static methods */
    var updateOrCreateController = function (action, $controller, basePath, uid, model) {
            if (!$controller || $controller.length === 0) {
                $controller = $('<' + action.inputType.htmlTagName + '></' + action.inputType.htmlTagName + '>');
                MetaRenderer.prepareForI18n($controller, action.label);
                $controller.addClass(action.cssClass + ' btn');
            } else if ($controller.data('actionProcessController')) {
                MetaRenderer.prepareForI18n($controller, action.label);
                $controller.addClass(action.cssClass + ' btn');
            }
            var onActionComplete = window[action.modelName + 'On' + action.type.name + 'Complete'];
            var uri = evaluateUri(action, basePath, uid, model);
            if ($controller[0].tagName === 'A') {
                $controller.attr({
                                     href: uri
                                 });
            }
            $controller.attr('data-target', uri);
            $controller.attr('data-x-controller-status', true);
            $controller.addClass(commonConstants.CONTROLLER_CLASS);
            if (typeof onActionComplete === 'function') {
                $controller.data('onActionComplete', onActionComplete);
            }
            return $controller;
        },
        evaluateUri = function (action, basePath, uid, model) {
            var uri = action.uri
                .replace('&&', '&')
                .replace(/\{endpoint\}\//, basePath || '{__INVALID-BASE-PATH__}')
                .replace(/\{basePath:[\w]+\}\//, basePath || '{__INVALID-BASE-PATH__}')
                .replace('{contentName}', model ? model.name : '{__INVALID-CONTENT-NAME__}')
                .replace('{id}', uid || '{__INVALID-ID__}')
                .replace('{refId}', '{__INVALID-ID__}')
                .replace('{guid}', uid || '{__INVALID-ID__}')
                .replace('{learnerCourseAssociationId}', uid || '{__INVALID-ID__}');
            if (action.viewName && action.viewName !== RestMetaModel.DEFAULT_VIEW_NAME && uri.indexOf('viewName=')==-1) {
                if (uri.indexOf('?') >= 0) {
                    uri += '&viewName=' + action.viewName;
                } else {
                    uri += '?viewName=' + action.viewName;
                }
            }
            if (uri.indexOf('ref=')==-1 && action.propertyName!="__undefined"){
                uri+='&ref='+action.propertyName;
            }
            if (uri.indexOf('refId={__INVALID-ID__}') > 0) {
                var m = uri.match(/ref=(\w+)/);
                if (m && m.length > 0 && model && model[m[1]] && model[m[1]].objectUID) {
                    uri = uri.replace('refId={__INVALID-ID__}', 'refId=' + model[m[1]].objectUID);
                } else if (uid){
                    uri = uri.replace('refId={__INVALID-ID__}', 'refId=' + uid);
                }
            }
            return uri;
        },
        beforeAction = function ($controller) {
            $controller.addClass("meta-disabled");
            $controller.prop('disabled', true);
        },
        afterAction = function ($controller) {
            $controller.removeClass("meta-disabled");
            $controller.prop('disabled', false);
        },
        uriOfSection = function (url) {
            var x = url.indexOf("?");
            return url.substr(0, x <= 0 ? url.length : x);
        },
        updateDom = function ($responseTarget, responseData, controllerData, updateSectionClass) {
            var $response = $(responseData),
                $xFragment = $response.nearest('.x-fragment', true),
                $modelView = $xFragment.nearest('.model-view');
            $responseTarget.html($xFragment);
            if (updateSectionClass) {
                $responseTarget.addClass(window.__updateDescriptor.sectionClass);
            }
            if (controllerData.action.openInModal) {
                $xFragment.closest('.modal-ajax-content').trigger('app:modalUpdated');
            }
            App.getInstance().update(null, $modelView);
        },
        onHtmlRequest = function ($controller, responseData, jqXHR, pushState) {
            var controllerData = $controller.data(),
                forwardAction = controllerData.action.forward || controllerData.action.backTo;
            if (History && pushState) {
                var absoluteAction = controllerData.target.startsWith("http") ? controllerData.target : window.location.origin + controllerData.target,
                    hashIndex = absoluteAction.indexOf("#"),
                    actionWithoutHash = hashIndex > 0 ? absoluteAction.substr(0, hashIndex) : absoluteAction,
                    hash = hashIndex > 0 ? absoluteAction.substr(hashIndex + 1) : null,
                    sectionUri = uriOfSection(actionWithoutHash),
                    allStateSectionsSelector = "body>section.main-section",
                    $mainSection = $(allStateSectionsSelector + '[data-state-url="' + sectionUri + '"]');
                if ($mainSection.length === 1) {
                    console.debug("[state change] Section exists, replace html", sectionUri);
                    $(allStateSectionsSelector).hide().addClass('hide');
                    $mainSection.attr('data-state-url', sectionUri);
                    updateDom($mainSection, responseData, controllerData);
                    $mainSection.removeClass('hide');
                } else {
                    console.debug("[state change] Section DOESN'T exists, create it");
                    $(allStateSectionsSelector).hide().addClass('hide');
                    $mainSection = $('<section class="main-section hide"></section>');
                    $('.main-section').after($mainSection);
                    $mainSection.attr('data-state-url', sectionUri);
                    updateDom($mainSection, responseData, controllerData, true);
                    $mainSection.removeClass('hide');
                }
                EventsManager.getInstance().fire({
                                                     name: EventsManager.HTML_REQUEST,
                                                     selector: $mainSection
                                                 }, $mainSection);
                var title = MetaRenderer.i18nMessage(window.__updateDescriptor.pageTitleCode);
                History.pushState({
                                      initial: false,
                                      responseText: jqXHR.responseText,
                                      responseJSON: jqXHR.responseJSON,
                                      hash: hash
                                  }, title, actionWithoutHash);
            } else {
                var $responseTarget = $('body').find(controllerData.responseTarget);
                updateDom($responseTarget, responseData, controllerData);
            }
        },
        onJsonRequest = function ($controller, responseData, jqXHR) {
            var controllerData = $controller.data(),
                forwardAction = controllerData.action.forward || controllerData.action.backTo;
            if(responseData && responseData.responseMsg != undefined && responseData.responseMsg != "")
            {
                app.notify(MetaRenderer.i18nMessage(responseData.responseMsg), 2000, "newton-text text-accent-1", function () {
                    window.location.reload();
                });
            }
            else {
                app.notify(MetaRenderer.i18nMessage(controllerData.action.successMessage), 2000, "green-text text-accent-1", function () {
                    if (forwardAction && forwardAction.type.requestMethod === 'GET' && forwardAction.modelName) {
                        ModelStore.promiseOf(forwardAction.modelName, true, {viewName: forwardAction.viewName})
                            .done(function () {
                                var store = this,
                                    modelDescriptor = store.getRestMetaModel().getModelDescriptor(),
                                    uri = evaluateUri(forwardAction, modelDescriptor.basePath, responseData ? responseData.id : null);
                                //var forwardRequest = new Request(uri, null, null, 'text', 'GET');
                                // forwardRequest.sendRequest();
                                window.location = uri;
                            });

                    }
                });
            }

        },
        onAjaxActionSuccess = function ($controller, responseData, jqXHR, pushState, dataType) {
            var controllerData = $controller.data(),
                forwardAction = controllerData.action.forward || controllerData.action.backTo,
                action = controllerData.action,
                isHtml = dataType === "text",
                onActionSuccess = window[action.modelName + 'On' + action.type.name + 'Success'];
            if (isHtml) {
                onHtmlRequest($controller, responseData, jqXHR, pushState);
            } else {
                onJsonRequest($controller, responseData, jqXHR);
            }
            if (typeof onActionSuccess === 'function') {
                onActionSuccess.call(window, null, $controller, action, responseData, jqXHR, pushState, dataType);
            }
        },
        onAjaxActionComplete = function ($controller, responseData, jqXHR) {
            var onActionComplete = $controller.data('onActionComplete');
            if (typeof onActionComplete === 'function') {
                onActionComplete.call(window, null, $controller,$controller.data('action'), responseData, jqXHR);
            }
        },
        onControllerClick = function (event, $controller, $form) {
            var controllerData = $controller.data(),
                action = controllerData.action,
                requestData = controllerData.requestData,
                controllerExecute = function () {
                    var deferred = $.Deferred(),
                        method = (controllerData.action.type ? controllerData.action.type.requestMethod : (controllerData.requestMethod || 'GET')).toUpperCase(),
                        dataType = (controllerData.requestDataType === "json" || controllerData.requestDataType === "text") ? controllerData.requestDataType : method === "GET" ? "text" : "json",
                        ajax = controllerData.action.ajax && !(method === 'GET' && controllerData.action.backTo == null && controllerData.action.forward == null);
                    if (ajax || controllerData.action.openInModal) {
                        var onSuccess = controllerData.actionOnSuccess || function (data, textStatus, jqXHR) {
                                var pushState = method === "GET" && dataType === "text" && !controllerData.action.openInModal;
                                onAjaxActionSuccess($controller, data, jqXHR, pushState, dataType);
                            },
                            requestOptions = $.extend({
                                                          beforeSend: function (jqXHR) {
                                                              if (controllerData.xEmbedded) {
                                                                  jqXHR.setRequestHeader(commonConstants.X_EMBEDDED_FRAGMENT_HEADER, 'yes');
                                                              } else if (method === "GET" && dataType === "text") {
                                                                  jqXHR.setRequestHeader(commonConstants.X_FRAGMENT_HEADER, 'yes');
                                                              }
                                                              var action = controllerData.action,
                                                                  onBeforeAction = window[action.modelName + 'OnBefore' + action.type.name];
                                                              if (typeof onBeforeAction === 'function') {
                                                                  var rez = onBeforeAction.call(window, null, $controller, action, jqXHR);
                                                                  if (rez !== undefined) {
                                                                      return !!rez;
                                                                  }
                                                              }
                                                              return true;
                                                          },
                                                          error: function (jqXHR, textStatus, errorThrown) {
                                                              var action = controllerData.action,
                                                                  onActionFail = window[action.modelName + 'On' + action.type.name + 'Fail'];
                                                              if (typeof onActionFail === 'function') {
                                                                  onActionFail.call(window, null, $controller, controllerData.action, jqXHR, textStatus, errorThrown);
                                                              }
                                                          },
                                                          complete: function (jqXHR, textStatus) {
                                                              onAjaxActionComplete($controller, jqXHR.responseJSON ? jqXHR.responseJSON : jqXHR.responseText, jqXHR);
                                                              afterAction($controller);
                                                          }
                                                      }, controllerData.requestOptions),
                            request = new Request(controllerData.target, requestData, onSuccess, dataType, method, requestOptions);
                        if ($form) {
                            $form.off('submit');
                            $form.submit(function (e) {
                                e.preventDefault();
                                e.stopPropagation();
                                var formData = MetaRenderer.formData($form);
                                if ($form.data('hasFormData')) {
                                    requestOptions.contentType = false;
                                } else {
                                    // requestOptions.contentType = 'application/json';
                                    requestOptions.contentType = false;
                                }
                                request.sendRequest($.extend(requestOptions, {data: formData}));
                            });
                            $form.submit();
                        } else {
                            if (action.openInModal) {
                                var $html = $('<div class="modal-ajax-content"></div>'),
                                    modalTarget = $controller.data('modalTarget') || '#app-modal';
                                MetaRenderer.openModal({
                                                           modalDialog: false,
                                                           modalCloseAction: true,
                                                           modalCancelAction: false,
                                                           modalOkAction: false
                                                       }, $html, modalTarget);
                                $controller.data('responseTarget', $html);
                            }

                            request.sendRequest(requestOptions);
                        }
                        request.always(function () {
                            deferred.resolveWith(request);
                        });
                    } else {
                        /* No ajax request */
                        window.location = controllerData.target;
                        afterAction($controller);
                        return deferred.resolve().promise();
                    }
                    return deferred.promise();

                };
            if (event) {
                event.preventDefault();
                event.stopPropagation();
            }
            beforeAction($controller);
            if (action.requiresConfirmation) {
                MetaRenderer.confirmationDialog(MetaRenderer.i18nMessage(action.confirmationMessage),
                                                function (e) {
                                                    e.preventDefault();
                                                    e.stopPropagation();
                                                    var $okButton = $(this), $dialog = $okButton.closest('.modal');
                                                    $dialog.modal('close');
                                                    controllerExecute();
                                                },
                                                function () {
                                                    afterAction($controller);
                                                });
            } else {
                controllerExecute();
            }
        },
        doCreateAction = function (action, $controller, modelDescriptor, model, viewType) {
            var basePath = modelDescriptor ? modelDescriptor.basePath : null,
                uid = model ? model.objectUID : null,
                $form = $controller && $controller[0].tagName === 'FORM' ? $controller : null;
            $controller = updateOrCreateController(action, !$form ? $controller : null, basePath, uid, model);
            var serviceName = $controller.data('service-name');
            if (serviceName) {
                $controller.attr('data-disabled-message', MetaRenderer.i18nMessage('service.' + serviceName + '.disabled'));
            }
            action.successMessage = MetaController.evaluateExpressionForAction(MetaController.ACTION_SUCCESS_MESSAGE, action);
            $controller.data('action', action);
            var formSubmitClause = $form && $form.closest('.form-wrapper').data('formType') === action.type.name;
            if (formSubmitClause) {
                $controller.data({
                                     requestOptions: {
                                         processData: false,
                                         contentType: 'application/json'
                                     },
                                     requestDataType: 'json'
                                 });
            }
            $controller.click(function (event) {
                var controllerData = $controller.data();
                if (controllerData.serviceDisabled) {
                    app.errorNotify({
                                        title: app.g11n.lang.app.messages.error,
                                        message: $controller.data('disabledMessage')
                                    });
                } else {
                    onControllerClick(event, $controller, formSubmitClause ? $form : null);
                }
            });
            return $controller;
        };
    return {
        ACTION_SUCCESS_MESSAGE: 'model.{modelName}.action.{action.type}.success',
        X_FRAGMENT_HEADER: "x-fragment",
        getUriOfAction: function (action, basePath, uid, model) {
            return evaluateUri(action, basePath, uid, model);
        },
        evaluateExpressionForAction: function (target, action) {
            return target.replace("{modelName}", action.modelName)
                .replace("{action.type}", action.type.name.toLowerCase().replace('_', '.'));
        },
        isActionEnabled: function (action, viewType) {
            switch (viewType) {
                case 'VIEW_FRAGMENT':
                    return action.availableOnlyInPresentation || action.availableInPresentation;
                case 'FORM_FRAGMENT':
                    return action.availableOnlyInForm || action.availableInForm;
                case 'INDEX_FRAGMENT':
                    return !action.availableOnlyInPresentation && !action.availableOnlyInForm;
            }
            return false;
        },
        createAction: function (action, $controller, triggerModelDescriptor, model, viewType) {
            if (action.type && (action.type.name === 'CUSTOM' || action.type.name === 'CUSTOM_GET')) {
                action.label = action.label.replace('action.custom.get', 'action.' + action.actionId).replace('action.custom', 'action.' + action.actionId)
            }
            var deferred = $.Deferred();
            if (!MetaController.isActionEnabled(action, viewType)) {
                return deferred.resolveWith($controller, [$controller]).promise($controller);
            } else {
                if ($controller && $controller.length){
                    $controller.addClass("future-controller");
                }
                if (triggerModelDescriptor.modelName === action.modelName) {
                    $controller = doCreateAction(action, $controller, triggerModelDescriptor, model, viewType);
                    if ($controller.attr('data-x-label')) {
                        MetaRenderer.i18nLabels($controller);
                    }
                    deferred.resolveWith($controller, [$controller]);
                    $controller.removeClass("future-controller");
                    $controller.triggerHandler('app:controllerReady');
                } else {
                    ModelStore.promiseOf(action.modelName, true, {viewName: action.viewName}).done(function () {
                        var store = this,
                            modelDescriptor = store.getRestMetaModel().getModelDescriptor();
                        $controller = doCreateAction(action, $controller, modelDescriptor, model, viewType);
                        if ($controller.attr('data-x-label')) {
                            MetaRenderer.i18nLabels($controller);
                        }
                        deferred.resolveWith($controller, [$controller]);
                        $controller.removeClass("future-controller");
                        $controller.triggerHandler('app:controllerReady');
                    });

                }
                return deferred.promise($controller);
            }

        },
        asMetaController: function ($controllers) {
            $controllers.each(function () {
                var $controller = $(this),
                    action = {uri: $controller.attr('href') || $controller.attr(commonConstants.DATA_TARGET)};
                MetaController.createAction(action, $controller);
            });
        },
        controllersTooltips: function ($controllers, tooltipOptions) {
            $controllers.each(function () {
                var $controller = $(this);
                $controller.on('app:i18n', function (event) {
                    var $controller = $(event.target),
                        $controllerText = $controller.find('.text').first(),
                        actionToolTip = $controllerText.length ? $controllerText.text() : $controller.html(),
                        options = $.extend({
                                               tooltip: actionToolTip,
                                               position: 'left',
                                               html: true
                                           }, tooltipOptions);
                    $controller.tooltip(options);
                });

            });
        },
        onStateChange: function (state) {
            console.log('[onStateChange]', state);
            var $mainSections = $('body>section.main-section'),
                $stateSection = $mainSections.filter('[data-state-url="' + state.url + '"]');
            if ($stateSection.length == 1) {
                if ($stateSection.hasClass('hide')) {
                    $mainSections.hide().addClass('hide');
                    $stateSection.removeClass('hide').fadeIn('fast');
                } else {
                    //todo highlight
                    $stateSection.removeClass('hide').fadeIn('fast');
                }
            } else {
                window.location = state.url;
            }
        }
    };
}();

