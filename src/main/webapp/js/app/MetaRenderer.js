/**
 * User: AlexandruVi
 * Date: 2017-08-23
 */
/**
 * MetaRenderer Utility
 * @constructor
 */
var MetaRenderer = function () {
    /* Private static variables and constants */
    var _labelsToTranslate = {},
        _i18nCache = {},
        _datePickerI18nObjectCached,
        _overrides = {},
        viewRendererType = {
            ID: {
                name: 'ID',
                render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
                    var value = model[propertyRenderer.propertyName];
                    simpleRenderer($propertyElement, value);
                }
            },
            OBJECT_LABEL: {
                name: 'OBJECT_LABEL',
                render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
                    var $target = $('<span class="property-value"></span>');
                    $propertyElement.append($target);
                    setObjectLabel($propertyElement, $target, model, propertyRenderer.propertyHolderModelName);
                }
            },
            INTEGER: {
                name: 'INTEGER',
                render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
                    var value = model[propertyRenderer.propertyName];
                    simpleRenderer($propertyElement, value);
                }
            },
            DOUBLE: {
                name: 'DOUBLE',
                render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
                    var value = model[propertyRenderer.propertyName];
                    simpleRenderer($propertyElement, value);
                }
            },
            STRING: {
                name: 'STRING',
                render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
                    var value = model[propertyRenderer.propertyName];
                    simpleRenderer($propertyElement, value);
                }
            },
            BUTTON: {
                name: 'BUTTON',
                render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
                    var value = model[propertyRenderer.propertyName];
                    simpleRenderer($propertyElement, value);
                }
            },
            STRING_MESSAGE: {
                name: 'STRING_MESSAGE',
                render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
                    var value = model[propertyRenderer.propertyName];
                    messageSimpleRenderer($propertyElement, value);
                }
            },
            WEB_LINK: {
                name: 'WEB_LINK',
                render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
                    var value = model[propertyRenderer.propertyName];
                    simpleRenderer($propertyElement, value);
                }
            },
            WEB_STORE_LINK: {
                name: 'WEB_STORE_LINK',
                render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
                    var value = model[propertyRenderer.propertyName];
                    simpleRenderer($propertyElement, value);
                }
            },
            BOOLEAN: {
                name: 'BOOLEAN',
                render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
                    var value = model[propertyRenderer.propertyName],
                        modelName = propertyRenderer.propertyHolderModelName,
                        propertyName = propertyRenderer.propertyName,
                        viewName = propertyRenderer.viewName,
                        messageTemplate = commonConstants.PROPERTY_VALUE_BOOLEAN(value),
                        messageCode = evaluateExpressionForModelPropertyAndView(messageTemplate, modelName,
                                                                                propertyName, viewName);
                    messageSimpleRenderer($propertyElement, messageCode, modelName, propertyName);
                }
            },
            CHIP: {
                name: 'CHIP',
                render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
                    var $child = $('<div class="inner-value"></div>'),
                        value = model[propertyRenderer.propertyName];
                    if (value) {
                        MetaRenderer.getPresentationRenderer(propertyRenderer,
                                                             propertyRenderer.defaultRendererType.name).render(model,
                                                                                                               $child,
                                                                                                               propertyRenderer,
                                                                                                               elementViewType,
                                                                                                               elementIndexViewType);

                        chip($propertyElement, $child);
                        $propertyElement.addClass('chip-wrapper');
                    }

                }
            },
            TIME: {
                name: 'TIME',
                render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
                    var value = model[propertyRenderer.propertyName];
                    simpleRenderer($propertyElement, value);
                }
            },
            TIME_AGO: {
                name: 'TIME_AGO',
                render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
                    var value = model[propertyRenderer.propertyName];
                    timeAgoRenderer($propertyElement, value, app.g11n.lang.app.dateFormats.LONG_DATE_TIME, app.g11n.lang.app.dateFormats.MACHINE_READY);
                }
            },
            DATE: {
                name: 'DATE',
                render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
                    var value = model[propertyRenderer.propertyName];
                    dateRenderer($propertyElement, value, app.g11n.lang.app.dateFormats.LONG_DATE);
                }
            },
            DATETIME: {
                name: 'DATETIME',
                render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
                    var value = model[propertyRenderer.propertyName];
                    dateRenderer($propertyElement, value, app.g11n.lang.app.dateFormats.LONG_DATE_TIME);
                }
            },
            IMAGE: {
                name: 'IMAGE',
                render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
                    var value = model[propertyRenderer.propertyName];
                    $propertyElement.append('<img src="' + value + '" class="image-tag "/>');
                }
            },
            BACKGROUND_IMAGE: {
                name: 'BACKGROUND_IMAGE',
                render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
                    var value = model[propertyRenderer.propertyName];
                    $propertyElement.append('<div style="background-image:url(' + value + ');" class="image-tag background-image"></div>');
                }
            },
            ICON: {
                name: 'ICON',
                render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
                    var value = model[propertyRenderer.propertyName];
                    simpleRenderer($propertyElement, value);
                    $propertyElement.addClass('material-icons');
                }
            },
            ENUM: {
                name: 'ENUM',
                render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
                    var value = model[propertyRenderer.propertyName],
                        modelName = propertyRenderer.propertyHolderModelName,
                        propertyName = propertyRenderer.propertyName,
                        viewName = propertyRenderer.viewName,
                        messageTemplate = commonConstants.PROPERTY_VALUE_ENUM(value),
                        messageCode = evaluateExpressionForModelPropertyAndView(messageTemplate, modelName,
                                                                                propertyName, viewName);
                    messageSimpleRenderer($propertyElement, messageCode, modelName, propertyName);
                }
            },
            HTML: {
                name: 'HTML',
                render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
                    var value = model[propertyRenderer.propertyName];
                    simpleRenderer($propertyElement, value);
                    $propertyElement.addClass('long-text');
                }
            },
            PIE_CHART: {
                name: 'PIE_CHART',
                render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
                    var value = model[propertyRenderer.propertyName];
                    $propertyElement.data('chartSettings', value);
                    MetaRenderer.chartRenderer($propertyElement, 'pieChart');
                }
            },
            DONUT_CHART: {
                name: 'DONUT_CHART',
                render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
                    var value = model[propertyRenderer.propertyName];
                    simpleRenderer($propertyElement, value);
                }
            },
            HORIZONTAL_BARS_CHART: {
                name: 'HORIZONTAL_BARS_CHART',
                render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
                    var value = model[propertyRenderer.propertyName];
                    simpleRenderer($propertyElement, value);
                }
            },
            VERTICAL_BARS_CHART: {
                name: 'VERTICAL_BARS_CHART',
                render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
                    var value = model[propertyRenderer.propertyName];
                    simpleRenderer($propertyElement, value);
                }
            },
            RATING_STARS: {
                name: 'RATING_STARS',
                render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
                    var value = model[propertyRenderer.propertyName];
                    ratingViewRenderer($propertyElement, value);
                }
            },
            MODEL: {
                name: 'MODEL',
                render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
                    var value = model[propertyRenderer.propertyName];
                    simpleRenderer($propertyElement, value ? value.objectLabel : null);
                }
            },
            MODEL_AVATAR: {
                name: 'MODEL_AVATAR',
                render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
                    var avatarModel = model[propertyRenderer.propertyName];
                    avatarModelRenderer($propertyElement, avatarModel);
                }
            },
            MODEL_AVATAR_IMG: {
                name: 'MODEL_AVATAR',
                render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
                    var avatarModel = model[propertyRenderer.propertyName];
                    avatarImgModelRenderer($propertyElement, avatarModel);
                }
            },
            MODEL_COLLECTION: {
                name: 'MODEL_COLLECTION',
                render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
                    var value = model[propertyRenderer.propertyName],
                        isNull = !value,
                        isArray = !isNull && $.isArray(value),
                        items = isNull ? [] : (isArray ? value : value.content),
                        length = isNull ? 0 : (value.totalElements !== undefined ? value.totalElements : items.length),
                        evaluatedLengthMessage = evaluateLengthMessage(propertyRenderer, length),
                        lengthMessage = MetaRenderer.i18nMessage(evaluatedLengthMessage, {
                            modelName: propertyRenderer.propertyHolderModelName, fallbackToNoModel: true
                        }, propertyRenderer.propertyName);
                    if (elementViewType === 'INDEX_FRAGMENT') {
                        lengthRenderer($propertyElement, length, lengthMessage);
                    } else {
                        MetaRenderer.simpleCollectionRenderer($propertyElement, items, propertyRenderer, $targetParent, model);
                    }
                }
            },
            MODEL_COLLECTION_CAROUSEL: {
                name: 'MODEL_COLLECTION_CAROUSEL',
                render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
                    var value = model[propertyRenderer.propertyName],
                        isNull = !value,
                        isArray = !isNull && $.isArray(value),
                        items = isNull ? [] : (isArray ? value : value.content),
                        length = isNull ? 0 : (value.totalElements !== undefined ? value.totalElements : items.length),
                        evaluatedLengthMessage = evaluateLengthMessage(propertyRenderer, length),
                        lengthMessage = MetaRenderer.i18nMessage(evaluatedLengthMessage, {
                            modelName: propertyRenderer.propertyHolderModelName, fallbackToNoModel: true
                        }, propertyRenderer.propertyName);
                    if (elementViewType === 'INDEX_FRAGMENT') {
                        lengthRenderer($propertyElement, length, lengthMessage);
                    } else {
                        MetaRenderer.slickSliderRenderer($propertyElement, items, propertyRenderer, $targetParent, model);
                    }
                }
            },
            VALUE_OBJECT_MODEL: {
                name: 'VALUE_OBJECT_MODEL',
                render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
                    var value = model[propertyRenderer.propertyName];
                    simpleRenderer($propertyElement, value);
                }
            },
            VALUE_OBJECT_MODEL_COLLECTION: {
                name: 'VALUE_OBJECT_MODEL_COLLECTION',
                render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
                    var value = model[propertyRenderer.propertyName],
                        isNull = !value,
                        isArray = !isNull && $.isArray(value),
                        items = isNull ? [] : (isArray ? value : value.content),
                        length = isNull ? 0 : (value.totalElements !== undefined ? value.totalElements : items.length),
                        evaluatedLengthMessage = evaluateLengthMessage(propertyRenderer, length),
                        lengthMessage = MetaRenderer.i18nMessage(evaluatedLengthMessage, {
                            modelName: propertyRenderer.propertyHolderModelName, fallbackToNoModel: true
                        }, propertyRenderer.propertyName);
                    if (elementViewType === 'INDEX_FRAGMENT') {
                        lengthRenderer($propertyElement, length, lengthMessage);
                    } else {
                        MetaRenderer.simpleCollectionRenderer($propertyElement, items, propertyRenderer, $targetParent, model);
                    }
                }
            },
            VALUE_OBJECT_MODEL_COLLECTION_CAROUSEL: {
                name: 'VALUE_OBJECT_MODEL_COLLECTION_CAROUSEL',
                render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
                    var value = model[propertyRenderer.propertyName],
                        isNull = !value,
                        isArray = !isNull && $.isArray(value),
                        items = isNull ? [] : (isArray ? value : value.content),
                        length = isNull ? 0 : (value.totalElements !== undefined ? value.totalElements : items.length),
                        evaluatedLengthMessage = evaluateLengthMessage(propertyRenderer, length),
                        lengthMessage = MetaRenderer.i18nMessage(evaluatedLengthMessage, {
                            modelName: propertyRenderer.propertyHolderModelName, fallbackToNoModel: true
                        }, propertyRenderer.propertyName);
                    if (elementViewType === 'INDEX_FRAGMENT') {
                        lengthRenderer($propertyElement, length, lengthMessage);
                    } else {
                        MetaRenderer.slickSliderRenderer($propertyElement, items, propertyRenderer, $targetParent, model);
                    }
                }
            }
        },
        formInputType = {
            ANCHOR: {
                name: 'ANCHOR',
                render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
                    var value = model[propertyRenderer.propertyName];
                    simpleInputRenderer($propertyElement, propertyRenderer, value, $superContainer);
                }
            },
            BUTTON: {
                name: 'BUTTON',
                render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
                    var value = model[propertyRenderer.propertyName];
                    simpleInputRenderer($propertyElement, propertyRenderer, value, $superContainer);
                }
            },
            BUTTON_INPUT: {
                name: 'BUTTON_INPUT',
                render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
                    var value = model[propertyRenderer.propertyName];
                    simpleInputRenderer($propertyElement, propertyRenderer, value, $superContainer);
                }
            },
            CHECKBOX: {
                name: 'CHECKBOX',
                render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
                    var value = model[propertyRenderer.propertyName];
                    simpleInputRenderer($propertyElement, propertyRenderer, value, $superContainer);
                    $propertyElement.on("app:propertyAppended", function () {
                        var $input = $(this).nearest('input.property-input');
                        if (value) {
                            $input.prop('checked', true);
                        }
                    });
                }
            },
            SWITCH: {
                name: 'SWITCH',
                render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
                    var value = model[propertyRenderer.propertyName];
                    simpleInputRenderer($propertyElement, propertyRenderer, value, $superContainer);
                    $propertyElement.on("app:propertyAppended", function () {
                        var $input = $propertyElement.nearest('input.property-input');
                        if (value) {
                            $input.prop('checked', true);
                        }
                    });
                }
            },
            CHECKBOX_SET: {
                name: 'CHECKBOX_SET',
                render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
                    var value = model[propertyRenderer.propertyName];
                    simpleInputRenderer($propertyElement, propertyRenderer, value, $superContainer);
                }
            },
            COLOR: {
                name: 'COLOR',
                render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
                    var value = model[propertyRenderer.propertyName];
                    simpleInputRenderer($propertyElement, propertyRenderer, value, $superContainer);
                }
            },
            DATE: {
                name: 'DATE',
                render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer, options) {
                    var value = model[propertyRenderer.propertyName];
                    simpleInputRenderer($propertyElement, propertyRenderer, value, $superContainer);
                    datePicker($propertyElement, propertyRenderer, value, $superContainer, null, options);
                }
            },
            DATETIME_LOCAL: {
                name: 'DATETIME_LOCAL',
                render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
                    var value = model[propertyRenderer.propertyName];
                    simpleInputRenderer($propertyElement, propertyRenderer, value, $superContainer);
                    datePicker($propertyElement, propertyRenderer, value, $superContainer, true);
                }
            },
            EMAIL: {
                name: 'EMAIL',
                render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
                    var value = model[propertyRenderer.propertyName];
                    simpleInputRenderer($propertyElement, propertyRenderer, value, $superContainer);
                }
            },
            FILE: {
                name: 'FILE',
                render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
                    var value = model[propertyRenderer.propertyName];
                    simpleInputRenderer($propertyElement, propertyRenderer, value, $superContainer);
                    fileInput($propertyElement, propertyRenderer, value, $superContainer);
                }
            },
            IMAGE_UPLOAD: {
                name: 'IMAGE_UPLOAD',
                render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
                    var value = model[propertyRenderer.propertyName];
                    simpleInputRenderer($propertyElement, propertyRenderer, value, $superContainer);
                    fileInput($propertyElement, propertyRenderer, value, $superContainer);
                }
            },
            HIDDEN: {
                name: 'HIDDEN',
                render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
                    var value = model[propertyRenderer.propertyName];
                    simpleInputRenderer($propertyElement, propertyRenderer, value, $superContainer);
                }
            },
            IMAGE: {
                name: 'IMAGE',
                render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
                    var value = model[propertyRenderer.propertyName];
                    simpleInputRenderer($propertyElement, propertyRenderer, value, $superContainer);
                }
            },
            MONTH: {
                name: 'MONTH',
                render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
                    var value = model[propertyRenderer.propertyName];
                    simpleInputRenderer($propertyElement, propertyRenderer, value, $superContainer);
                }
            },
            NUMBER: {
                name: 'NUMBER',
                render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
                    var value = model[propertyRenderer.propertyName];
                    simpleInputRenderer($propertyElement, propertyRenderer, value, $superContainer);
                }
            },
            PASSWORD: {
                name: 'PASSWORD',
                render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
                    var value = model[propertyRenderer.propertyName];
                    simpleInputRenderer($propertyElement, propertyRenderer, value, $superContainer);
                }
            },
            RADIO: {
                name: 'RADIO',
                render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
                    var value = model[propertyRenderer.propertyName];
                    simpleInputRenderer($propertyElement, propertyRenderer, value, $superContainer);
                }
            },
            RADIO_SET: {
                name: 'RADIO_SET',
                render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
                    var value = model[propertyRenderer.propertyName];
                    simpleInputRenderer($propertyElement, propertyRenderer, value, $superContainer);
                }
            },
            RANGE: {
                name: 'RANGE',
                render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
                    var value = model[propertyRenderer.propertyName];
                    simpleInputRenderer($propertyElement, propertyRenderer, value, $superContainer);
                }
            },
            SLIDER: {
                name: 'SLIDER',
                render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
                    var value = model[propertyRenderer.propertyName];
                    simpleInputRenderer($propertyElement, propertyRenderer, value, $superContainer);
                }
            },
            RESET: {
                name: 'RESET',
                render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
                    var value = model[propertyRenderer.propertyName];
                    simpleInputRenderer($propertyElement, propertyRenderer, value, $superContainer);
                }
            },
            SEARCH: {
                name: 'SEARCH',
                render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
                    var value = model[propertyRenderer.propertyName];
                    simpleInputRenderer($propertyElement, propertyRenderer, value, $superContainer);
                }
            },
            SUBMIT: {
                name: 'SUBMIT',
                render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
                    var value = model[propertyRenderer.propertyName];
                    simpleInputRenderer($propertyElement, propertyRenderer, value, $superContainer);
                }
            },
            TEL: {
                name: 'TEL',
                render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
                    var value = model[propertyRenderer.propertyName];
                    simpleInputRenderer($propertyElement, propertyRenderer, value, $superContainer);
                }
            },
            TEXT: {
                name: 'TEXT',
                render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
                    var value = model[propertyRenderer.propertyName];
                    simpleInputRenderer($propertyElement, propertyRenderer, value, $superContainer);
                }
            },
            TIME: {
                name: 'TIME',
                render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
                    var value = model[propertyRenderer.propertyName];
                    simpleInputRenderer($propertyElement, propertyRenderer, value, $superContainer);
                    timePicker($propertyElement, propertyRenderer, value, $superContainer);
                }
            },
            URL: {
                name: 'URL',
                render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
                    var value = model[propertyRenderer.propertyName];
                    simpleInputRenderer($propertyElement, propertyRenderer, value, $superContainer);
                }
            },
            WEEK: {
                name: 'WEEK',
                render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
                    var value = model[propertyRenderer.propertyName];
                    simpleInputRenderer($propertyElement, propertyRenderer, value, $superContainer);
                }
            },
            TEXTAREA: {
                name: 'TEXTAREA',
                render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
                    var value = model[propertyRenderer.propertyName];
                    simpleInputRenderer($propertyElement, propertyRenderer, value, $superContainer);
                }
            },
            WYSIWYG: {
                name: 'WYSIWYG',
                render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
                    var value = model[propertyRenderer.propertyName];
                    simpleInputRenderer($propertyElement, propertyRenderer, value, $superContainer);
                }
            },
            SELECT: {
                name: 'SELECT',
                render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
                    var value = model[propertyRenderer.propertyName];
                    simpleInputRenderer($propertyElement, propertyRenderer, value, $superContainer);
                    MetaRenderer.selectProcessor($propertyElement, propertyRenderer, value, $superContainer);
                }
            },
            MULTI_SELECT: {
                name: 'MULTI_SELECT',
                render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
                    var value = model[propertyRenderer.propertyName];
                    simpleInputRenderer($propertyElement, propertyRenderer, value, $superContainer);
                    MetaRenderer.selectProcessor($propertyElement, propertyRenderer, value, $superContainer, true);
                }
            },
            LIST_OF_VALUES: {
                name: 'LIST_OF_VALUES',
                render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
                    var value = model[propertyRenderer.propertyName],
                        options = $.extend({
                                               openOnEmptyInput: true,
                                               minLength: 0,
                                               limit: 6
                                           }, $propertyElement.data('autocompleteOptions'), {
                                               modelName: propertyRenderer.modelName,
                                               viewName: propertyRenderer.valueObjectTargetViewName
                                           });
                    simpleInputRenderer($propertyElement, propertyRenderer, value, $superContainer);
                    var $input = $propertyElement.nearest('input.property-input');
                    $input.attr('type', 'TEXT');
                    $input.attr('autocomplete', 'off');
                    $input.addClass('autocomplete');
                    if (propertyRenderer.modelName && value) {
                        $input.val(value.objectLabel);
                    }
                    $propertyElement.prepend('<i class="material-icons prefix">arrow_drop_down_circle</i>');
                    $input.autocomplete(options);
                }
            },
            LIST_OF_VALUES_MULTI_SELECT: {
                name: 'LIST_OF_VALUES_MULTI_SELECT',
                render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
                    MetaRenderer.multiSelect(model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer);
                }
            },
            OBJECT_BUILDER: {
                name: 'OBJECT_BUILDER',
                render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
                    var value = model[propertyRenderer.propertyName];
                    simpleInputRenderer($propertyElement, propertyRenderer, value, $superContainer);
                    MetaRenderer.objectBuilder(model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer);
                }
            },
            COLLECTION_BUILDER: {
                name: 'COLLECTION_BUILDER',
                render: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer) {
                    var value = model[propertyRenderer.propertyName];
                    simpleInputRenderer($propertyElement, propertyRenderer, value, $superContainer);
                    MetaRenderer.collectionBuilder(model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $superContainer);
                }
            }
        },
        commonConstants = {
            VIEW_TYPE: {
                INDEX_FRAGMENT: 'INDEX_FRAGMENT',
                VIEW_FRAGMENT: 'VIEW_FRAGMENT',
                FORM_FRAGMENT: 'FORM_FRAGMENT'
            },
            INDEX_VIEW_TYPE: {
                LIST: 'LIST',
                GRID: 'GRID',
                CARD: 'CARD',
                TREE: 'TREE'
            },
            NO_AJAX_CLASS: 'no-ajax',
            PROPERTY_CLASS: 'property-presentation model-{modelName}-property model-property-value',
            PROPERTY_SELECTOR: '.model-{modelName}-property[data-model-property-name={propertyName}]',
            PROPERTY_SELECTOR_FOR_INPUT: '.model-{modelName}-property[data-model-property-name]',
            PROPERTY_LABEL: 'model.{modelName}.property.{propertyName}.{view}.label',
            ACTIONS_LABEL: 'model.{modelName}.actions.{view}.label',
            PROPERTY_VALUE_LENGTH: 'model.{modelName}.property.{propertyName}.{view}.length.label',
            PROPERTY_VALUE_LENGTH_NONE: 'model.{modelName}.property.{propertyName}.{view}.length.none.label',
            PROPERTY_VALUE_LENGTH_ONCE: 'model.{modelName}.property.{propertyName}.{view}.length.one.label',
            PROPERTY_NULL_VALUE: 'model.{modelName}.property.{propertyName}.{view}.nullValue.label',
            PROPERTY_EMPTY_VALUE: 'model.{modelName}.property.{propertyName}.{view}.emptyValueOrCollection.label',
            PROPERTY_PLACEHOLDER: 'model.{modelName}.property.{propertyName}.{view}.placeholder',
            PROPERTY_VALIDATION_FAILED_MESSAGE: 'model.{modelName}.property.{propertyName}.{view}.validation.failed',
            FILE_SELECTOR_BUTTON: 'model.{modelName}.file.field.button',
            MODEL_VIEW_SELECTOR: '.model-view[data-model-name={modelName}][data-meta-renderer=true][data-model-uid={objectUID}]',
            MODEL_VIEW_SELECTOR_ITEM: '.model-view[data-model-name={modelName}][data-meta-renderer=true][data-collection-item-index={objectUID}]',
            MODEL_VIEW_SELECTOR_WITHOUT_ID: '.model-view[data-model-name={modelName}][data-meta-renderer=true]',
            RENDERER_PLACEHOLDER_SELECTOR: '[data-renderer-placeholder-for={propertyName}]',
            /**
             * @return {string}
             */
            PROPERTY_VALUE_BOOLEAN: function (booleanValue) {
                return 'model.{modelName}.property.{propertyName}.boolean.{view}.' + (booleanValue ? 'TRUE' : 'FALSE');
            },
            /**
             * @return {string}
             */
            PROPERTY_VALUE_ENUM: function (enumInstance) {
                var enumInstanceName = typeof  enumInstance === 'object' ? enumInstance.name : enumInstance;
                return 'model.{modelName}.property.{propertyName}.enum.{view}.' + enumInstanceName;
            },
            DEFAULT_VIEW_NAME: '__default'
        };
    /* Private static methods */
    var propertyTag = function (propertyRenderer, viewType, indexViewType) {
            if (indexViewType === commonConstants.INDEX_VIEW_TYPE.GRID) {
                return "td";
            }
            var tagName = 'div';
            switch (propertyRenderer.rendererType.name) {
                case 'BUTTON':
                    tagName = 'button';
            }
            return propertyRenderer.clickAction && MetaController.isActionEnabled(propertyRenderer.clickAction, viewType) && tagName !== 'button' ? 'a' : tagName;
        },
        isHidden = function (model, propertyRenderer, elementViewType, elementIndexViewType) {
            return propertyRenderer.hidden ||
                'INDEX_FRAGMENT' === elementViewType && (('LIST' === elementIndexViewType || 'TREE' === elementIndexViewType) && propertyRenderer.hiddenOnList || 'CARD' === elementIndexViewType && propertyRenderer.hiddenOnCard || 'GRID' === elementIndexViewType && propertyRenderer.hiddenOnGrid) ||
                'FORM_FRAGMENT' === elementViewType && (propertyRenderer.hiddenOnForm || model.new && propertyRenderer.hiddenOnCreate || !model.new && propertyRenderer.hiddenOnUpdate) ||
                'VIEW_FRAGMENT' === elementViewType && propertyRenderer.hiddenOnPresentation;
        },
        isLabelDisplayed = function (model, propertyRenderer, elementViewType, elementIndexViewType) {
            return propertyRenderer.displayLabelInForm && 'FORM_FRAGMENT' === elementViewType || 'FORM_FRAGMENT' !== elementViewType && propertyRenderer.displayLabelInPresentation && 'GRID' !== elementIndexViewType;
        },
        evaluateLengthMessage = function (propertyRenderer, length) {
            return evaluateExpressionForModelPropertyAndView(length ? (length == 1 ? commonConstants.PROPERTY_VALUE_LENGTH_ONCE : commonConstants.PROPERTY_VALUE_LENGTH) : commonConstants.PROPERTY_VALUE_LENGTH_NONE, propertyRenderer.propertyHolderModelName,
                                                             propertyRenderer.propertyName)
        },
        evaluateExpressionForModel = function (target, modelName, objectUID) {
            return target.replace("{modelName}", modelName)
                .replace("{objectUID}", objectUID);
        },
        evaluateModelViewSelector = function (modelName, objectUID) {
            if (objectUID) {
                return evaluateExpressionForModel(commonConstants.MODEL_VIEW_SELECTOR, modelName, objectUID);
            } else {
                return evaluateExpressionForModel(commonConstants.MODEL_VIEW_SELECTOR_WITHOUT_ID, modelName);
            }
        },
        evaluateModelViewSelectorForCollectionItem = function (modelName, itemIndex) {
            return evaluateExpressionForModel(commonConstants.MODEL_VIEW_SELECTOR_ITEM, modelName, itemIndex);
        },
        evaluateModelViewSelectorWithoutId = function (modelName) {
            return evaluateExpressionForModel(commonConstants.MODEL_VIEW_SELECTOR_WITHOUT_ID, modelName);
        },
        evaluateRendererPlaceholderSelector = function (propertyName) {
            return evaluateExpressionForModelPropertyAndView(commonConstants.RENDERER_PLACEHOLDER_SELECTOR, null, propertyName, null);
        },
        evaluateExpressionForPropertyRenderer = function (target, propertyRenderer, defaultViewName) {
            return evaluateExpressionForModelPropertyAndView(target, propertyRenderer.propertyHolderModelName,
                                                             propertyRenderer.propertyName, propertyRenderer.viewName,
                defaultViewName || commonConstants.DEFAULT_VIEW_NAME);
        },
        evaluateExpressionForModelPropertyAndView = function (target, modelName, propertyName, viewName,
                                                              defaultViewName) {
            return target.replace("{modelName}", modelName)
                .replace("{propertyName}", propertyName)
                .replace("{view}", viewName || commonConstants.DEFAULT_VIEW_NAME)
                .replace('.' + (defaultViewName || commonConstants.DEFAULT_VIEW_NAME), "");
        },
        processPropertyClickAction = function (modelDescriptor, model, $propertyElement, propertyRenderer, elementViewType,
                                               elementIndexViewType) {
            if (model && typeof model[propertyRenderer.propertyName] === 'object') {
                $propertyElement.data('actionProcessController', true);
            }
            return MetaController.createAction(propertyRenderer.clickAction, $propertyElement, modelDescriptor, model, elementViewType);
        },
        avatarModelRenderer = function ($wrapper, avatarModel) {
            var $avatar = $('<span class="user-avatar"><span class="user-icon target-for-with-action"></span><span class="user-name target-for-with-action"></span></span>');
            if (avatarModel) {
                $avatar.find('.user-name').html(avatarModel.displayName);
                if (avatarModel.avatarSrc) {
                    $avatar.find('.user-icon').addClass('with-image')
                        .append('<img src="' + avatarModel.avatarSrc + '">');
                } else {
                    $avatar.find('.user-icon').addClass('no-image').attr('data-initials', avatarModel.initials);
                }
            }
            $wrapper.append($('<span class="property-value"></span>').html($avatar));
        },
        avatarImgModelRenderer = function ($wrapper, avatarModel) {
            var $avatar = $('<span class="user-avatar"><span class="user-icon target-for-with-action"></span><span class="user-name target-for-with-action"></span></span>');
            if (avatarModel) {
                if (avatarModel.avatarSrc) {
                    $avatar.find('.user-icon').addClass('with-image')
                        .append('<img src="' + avatarModel.avatarSrc + '">');
                } else {
                    $avatar.find('.user-icon').addClass('no-image').attr('data-initials', avatarModel.initials);
                }
            }
            $wrapper.append($('<span class="property-value"></span>').html($avatar));
        },
        simpleRenderer = function ($wrapper, value) {
            $wrapper.append('<span class="property-value">' + app.htmlDecode(value) + '</span>');
        },
        setXLabel = function ($target, code, propertyRenderer, attributeName) {
            var evaluatedCode = propertyRenderer ? evaluateExpressionForPropertyRenderer(code, propertyRenderer) : code;
            $target.attr('data-x-label', evaluatedCode);
            $target.attr('data-x-label-fallback', true);
            if (propertyRenderer) {
                $target.attr('data-x-label-model-name', propertyRenderer.propertyHolderModelName);
            }
            if (attributeName) {
                $target.attr('data-x-label-attribute-name', attributeName);
            }
        },
        setPlaceholder = function ($input, propertyRenderer) {
            setXLabel($input, commonConstants.PROPERTY_PLACEHOLDER, propertyRenderer, 'placeholder');
        },
        simpleInputRenderer = function ($wrapper, propertyRenderer, value, $superContainer) {
            var tagName = propertyRenderer.inputType.htmlTagName,
                rootInputType = propertyRenderer.inputType.rootInputType,
                $input = $('<' + tagName + ' class="property-input" />'),
                inputType = 'TEXT',
                $modelView = $superContainer,
                formId = $superContainer.nearest('.model-form[id]', true)
                    .attr('id') || $superContainer.closest('.model-form[id]').attr('id'),
                valueNormalizer = function (value, inputType, clone) {
                    if (inputType === 'CHECKBOX') {
                        return !!value;
                    } else {
                        if (inputType === 'TEXT' || inputType === 'TEXTAREA') {
                            return value === null || value === undefined || value === '' ? null : app.htmlDecode(value);
                        }
                        else {
                            return value === null || value === undefined || value === '' ? null : (clone ? ($.isArray(value) ? value.slice() : (typeof value === 'object' ? $.extend(true, {}, value) : value)) : value);
                        }
                    }
                };
            if (rootInputType == null) {
                inputType = propertyRenderer.inputType.name;
            } else if (rootInputType.htmlTagName === 'INPUT') {
                inputType = rootInputType.name;
            }
            if (tagName === 'INPUT') {
                $input.attr('type', inputType);
                $input.attr('name', propertyRenderer.propertyName);
                if (inputType === 'TEXT' || inputType === 'NUMBER' || inputType === 'LIST_OF_VALUES' || inputType === 'FILE' || inputType === 'PASSWORD') {
                    setPlaceholder($input, propertyRenderer);
                }
                if (propertyRenderer.disabled) {
                    $input.attr('disabled', propertyRenderer.disabled);
                }
                if (propertyRenderer.readonly) {
                    $input.attr('readonly', propertyRenderer.readonly);
                }
                if (propertyRenderer.required) {
                    $input.attr('required', propertyRenderer.required);
                }
                if (!!propertyRenderer.inputFieldValidationPattern) {
                    $input.attr('pattern', propertyRenderer.inputFieldValidationPattern);
                }
                if (inputType !== 'FILE' && inputType !== 'CHECKBOX' && inputType !== 'TEXT' && inputType !== 'TEXTAREA') {
                    $input.val(value);
                }
                if (inputType === 'TEXT' || inputType === 'TEXTAREA') {
                    $input.val(valueNormalizer(value, inputType));
                }
                if (inputType === 'CHECKBOX') {
                    $input.change(function (e) {
                        var $currentInput = $(e.currentTarget),
                            $currentWrapper = $currentInput.closest('.property-input-wrapper');
                        if ($currentInput.is(':checked')) {
                            $currentInput.data('currentValue', true);
                        }
                        else {
                            $currentInput.data('currentValue', false);
                        }
                    });
                } else if (inputType === 'TEXT' || inputType === 'NUMBER' || inputType === 'PASSWORD' || inputType ===  'TIME') {
                    $input.change(function (e) {
                        var $currentInput = $(e.currentTarget);
                        $currentInput.data('currentValue', valueNormalizer($currentInput.val(), inputType));
                    });
                }
            }
            if (inputType === 'SELECT') {
                $input.change(function (e) {
                    var $currentInput = $(e.currentTarget),
                        $selectedOptions = $currentInput.find('option:selected');
                    $currentInput.data('currentValue', $selectedOptions.data('currentValue'));
                });
            }
            if (inputType === 'TEXTAREA') {
                $input.val(value);
                $input.change(function (e) {
                    var $currentInput = $(e.currentTarget),
                        newValue = $currentInput.val();
                    $currentInput.data('currentValue', newValue !== "" ? newValue : null);
                });
            }
            var currentValue = valueNormalizer(value, inputType),
                initialValue = valueNormalizer(value, inputType, true);
            $input.data('currentValue', currentValue);
            $input.data('initialValue', initialValue);
            $wrapper.data('currentValue', currentValue);
            $wrapper.data('initialValue', initialValue);
            $input.attr('id', formId + '-' + propertyRenderer.propertyName);
            $wrapper.append($input);
            $wrapper.data('dirty', false);
            if (propertyRenderer.inputFieldValidation) {
                $input.addClass('validate');
            }
            if (inputType != 'LIST_OF_VALUES') {
                $input.on('mouseout', function (e) {
                    $(this).blur();
                });
            }
            $input.on('change', function (e) {
                var $currentInput = $(e.currentTarget);
                e.stopPropagation();
                onInputChange(e, $currentInput, propertyRenderer.propertyName, $currentInput.val());
            });
        },
        onInputChange = function (e, $currentInput, propertyName, inputVal) {
            var $currentWrapper = $currentInput.closest('.property-input-wrapper'),
                currentValue = $currentInput.data('currentValue'),
                initialValue = $currentInput.data('initialValue'),
                newVal = inputVal,
                dirty = currentValue !== initialValue;
            $currentWrapper.data('currentValue', currentValue);
            $currentWrapper.data('dirty', dirty);
            console.debug('[', propertyName, '] [input value: ', newVal, '] [current value:', currentValue, '] [initial value:', initialValue, '] [dirty:', dirty, ']');
            $currentWrapper.trigger('app:dirtyChange', [dirty, propertyName, currentValue, $currentInput, e]);
        },
        fileInput = function ($propertyElement, propertyRenderer, value, $superContainer) {
            var $input = $propertyElement.nearest('input.property-input'),
                $realFileInput = $('<input type="file"/>'),
                $button = $('<div class="btn"></div>'),
                $buttonContent = $('<span></span>'),
                $filePathWrapper = $('<div class="file-path-wrapper"></div>');
            $input.addClass('file-path');
            $input.attr('type', 'TEXT');
            setXLabel($buttonContent, commonConstants.FILE_SELECTOR_BUTTON, propertyRenderer);
            $button.append($buttonContent);
            $button.append($realFileInput);
            $realFileInput.change(function () {
                $input.data('currentValue', $realFileInput[0].files);
            });
            $propertyElement.prepend($button);
            $propertyElement.append($filePathWrapper);
            $input.appendTo($filePathWrapper);
            $propertyElement.addClass('file-field');
        },
        lengthRenderer = function ($wrapper, length, lengthMessage) {
            var message = (lengthMessage ? lengthMessage.replace('{length}', length) : length);
            $wrapper.html('<span class="property-value">' + message + '</span>');
        },
        timeAgoRenderer = function ($wrapper, unixTimestamp, format, inputFormat) {
            var $time = $('<time class="property-value time-ago" datetime="' + $.format.date(unixTimestamp, inputFormat)
                    .replace(/\.[\d]+ZZZ/, 'Z') + '" title="' + $.format.date(unixTimestamp, format) + '"></time>'),
                isInPast = unixTimestamp <= Date.now(),
                options = {
                    autoDispose: false,
                    allowPast: isInPast,
                    allowFuture: !isInPast,
                    strings: datePickerI18nObject().timeAgo
                };
            $time.timeago('init', options);
            $wrapper.append($time);
        },
        ratingViewRenderer = function ($wrapper, value) {
            var $rating = $('<span class="property-value rating-stars"><i class="material-icons rating-star-element">star</i><i class="material-icons rating-star-element">star</i><i class="material-icons rating-star-element">star</i><i class="material-icons rating-star-element">star</i><i class="material-icons rating-star-element">star</i></span>');
            MetaRenderer.ratingViewUpdate($rating, value);
            $wrapper.html($rating);
            $wrapper.removeClass('btn generic-action');
        },
        dateRenderer = function ($wrapper, unixTimestamp, format) {
            $wrapper.append('<span class="property-value">' + $.format.date(unixTimestamp, format) + '</span>');
        },
        messageSimpleRenderer = function ($messageWrapper, messageCode, modelName, propertyName) {
            var message = MetaRenderer.i18nMessage(messageCode, modelName, propertyName);
            $messageWrapper.append('<span class="property-value">' + message + '</span>');
        },
        chip = function ($chipWrapper, content) {
            $chipWrapper.append($('<div class="chip property-value"></div>').append(content));
        },
        doRenderAllElementsFromOneModelStoreItem = function (modelName, modelUid, modelStore, $context, currentValue, extraPropertiesClasses, refRenderer, itemIndex) {
            if ($context && $context.length) {
                var model = currentValue || modelStore.get(modelUid),
                    $elements = !isNaN(itemIndex) ? $context.find(evaluateModelViewSelectorForCollectionItem(modelName, itemIndex)) : $context.find(evaluateModelViewSelector(modelName, modelUid));
                console.log("Render view", modelName, modelUid ? ('#' + modelUid) : null, "Html elements to update: ",
                            $elements.length);
                $elements.each(function (index, el) {
                    try {
                        var $element = $(el);
                        console.debug('---> [htmlIndex:', index, ']', $element);
                        MetaRenderer.renderModelInstance($element, model, modelStore, refRenderer, extraPropertiesClasses);
                    } catch (e) {
                        console.error(e);
                    }
                });

            }
        },
        gridTHeadUpdate = function ($table, modelDescriptor, withActionsHeader) {
            var $tHead = $table.nearest('thead'), $actionsWrappers = $table.nearest('td.actions-wrapper'),
                $actionsWrapper = $actionsWrappers.first(),
                hasActions = $actionsWrapper.find('.more-actions').children().length > 0 || $actionsWrapper.find('.main-actions').children().length > 0;
            if ($tHead.length === 1) {
                var $tr = $('<tr></tr>');
                $.each(modelDescriptor.modelProperties, function (index, propertyRenderer) {
                    if (!propertyRenderer.hidden && !propertyRenderer.hiddenOnGrid) {
                        $tr.append(createTableHeader(propertyRenderer, modelDescriptor));
                    }
                });
                $tHead.append($tr);
                if (withActionsHeader && hasActions) {
                    var $actionsTableHeaderCell = $('<th></th>');
                    MetaRenderer.prepareForI18n($actionsTableHeaderCell, evaluateExpressionForModelPropertyAndView(commonConstants.ACTIONS_LABEL, modelDescriptor.modelName, null, modelDescriptor.viewName));
                    $tr.append($actionsTableHeaderCell);
                } else {
                    $actionsWrappers.remove();
                }
                MetaRenderer.i18nLabels($tr.find('[data-x-label][data-x-label-status!=true]'));
            }
        },
        renderPropertyPresentation = function (modelDescriptor, model, $propertyElement, propertyRenderer, elementViewType,
                                               elementIndexViewType, $targetParent, $superContainer) {
            var propertyValue = model ? model[propertyRenderer.propertyName] : null;
            console.debug("      ---> Render [", propertyRenderer.propertyHolderModelName, "->", propertyRenderer.propertyName, "]", "[rendererType:", propertyRenderer.rendererType.name, "]", propertyValue);
            if (isLabelDisplayed(model, propertyRenderer, elementViewType, elementIndexViewType)) {
                $propertyElement.append('<span class="property-label">' + MetaRenderer.i18nMessage(
                    propertyRenderer.label, {
                        modelName: propertyRenderer.propertyHolderModelName, fallbackToNoModel: true
                    }) + '</span>');
            }
            if (propertyValue != null || !propertyRenderer.messageOnNull || propertyRenderer.clickAction && (propertyRenderer.rendererType.name === 'BUTTON' || propertyRenderer.rendererType.name === 'ANCHOR')) {
                if (propertyRenderer.clickAction) {
                    console.debug("            ---> Process click action:", propertyRenderer.clickAction);
                    processPropertyClickAction(modelDescriptor, model, $propertyElement, propertyRenderer,
                                               elementViewType, elementIndexViewType).done(function () {
                        if (propertyValue != null || propertyRenderer.rendererType.name !== 'BUTTON' && propertyRenderer.rendererType.name !== 'ANCHOR') {
                            $propertyElement = MetaRenderer.getPresentationRenderer(propertyRenderer)
                                .render(model, $propertyElement, propertyRenderer,
                                        elementViewType, elementIndexViewType, $targetParent, $superContainer);
                        }
                    });
                } else {
                    if (propertyRenderer.messageOnNull && propertyValue == null) {
                        MetaRenderer.showMessageOnNull($propertyElement, propertyRenderer);
                    } else {
                        MetaRenderer.getPresentationRenderer(propertyRenderer)
                            .render(model, $propertyElement, propertyRenderer,
                                    elementViewType, elementIndexViewType, $targetParent, $superContainer);
                    }
                }

            } else {
                if (propertyRenderer.messageOnNull) {
                    MetaRenderer.showMessageOnNull($propertyElement, propertyRenderer);
                }
            }

        },
        renderPropertyInput = function (modelDescriptor, model, $propertyElement, propertyRenderer, elementViewType,
                                        elementIndexViewType, $targetParent, $superContainer, filter) {
            var propertyValue = model ? model[propertyRenderer.propertyName] : null;
            console.debug("      ---> Render (input) [", propertyRenderer.propertyHolderModelName, "->", propertyRenderer.propertyName, "]", "[inputType:", propertyRenderer.inputType.name, "]", propertyValue);
            MetaRenderer.getInputRenderer(propertyRenderer).render(model, $propertyElement, propertyRenderer,
                                                                   elementViewType, elementIndexViewType, $targetParent, $superContainer, filter);
            var $input = $propertyElement.nearest('.property-input');
            if (filter || isLabelDisplayed(model, propertyRenderer, elementViewType, elementIndexViewType)) {
                var $label = $('<label class="property-label active"></label>'),
                    inputId = $input.attr('id');
                $label.append('<span class="text">' + MetaRenderer.i18nMessage(propertyRenderer.label, {
                    modelName: propertyRenderer.propertyHolderModelName, fallbackToNoModel: true
                }) + '</span>');
                if (!filter && !propertyRenderer.required && propertyRenderer.defaultRendererType.name !== 'BOOLEAN') {
                    $label.append('<span class="optional-field-text">(' + MetaRenderer.i18nMessage(RestMetaModel.OPTIONAL_INPUT_LABEL, modelDescriptor, propertyRenderer.propertyName) + ')</span>');
                }
                $label.attr('for', inputId);
                setXLabel($label, commonConstants.PROPERTY_VALIDATION_FAILED_MESSAGE, propertyRenderer, 'data-error');
                if ($input.attr('type') === 'CHECKBOX') {
                    $propertyElement.removeClass('input-field');
                    $propertyElement.addClass('input-field-checkbox');
                    $label.removeClass('active');

                }
                if (propertyRenderer.inputType.name === 'IMAGE_UPLOAD' || propertyRenderer.inputType.name === 'FILE') {
                    $propertyElement.append($label);
                } else if (propertyRenderer.inputType.name === 'DATE') {
                    $input.before($label);
                } else {
                    $input.after($label);
                }
            }
            $propertyElement.addClass('property-input-wrapper col s12 l' + propertyRenderer.inputFieldSize.size);
            $propertyElement.addClass('field-display-mode-' + propertyRenderer.fieldDisplayMode.toLowerCase());
            $propertyElement.attr('data-input-type', propertyRenderer.inputType.name);
            if (!filter) {
                $propertyElement.on('app:dirtyChange', function (e, dirty, propertyName, currentValue, $sourceInput, sourceEvent) {
                    e.stopPropagation();
                    var $pel = $(e.target),
                        $modelView = $pel.closest('.model-view'),
                        wrapperModel = $modelView.data('currentValue');
                    console.debug("[form] [dirtyChange] => [", propertyName, "] Current property value:", currentValue, 'Wrapper model:', wrapperModel, 'Property element:', $pel);
                    wrapperModel[propertyName] = currentValue;
                    $modelView.data('currentValue', wrapperModel);
                    if (dirty) {
                        $pel.addClass("dirty-property");
                    } else {
                        $pel.removeClass("dirty-property");
                    }
                });
            } else {
                $propertyElement.on('app:dirtyChange', function (e, dirty, propertyName, currentValue, $sourceInput, sourceEvent) {
                    e.stopPropagation();
                    var $pel = $(e.target),
                        wrapperModel = $targetParent.data('currentValue') || {};
                    if (typeof currentValue === 'object') {
                        currentValue = currentValue.objectUID;
                        if (propertyName.indexOf('.') == -1) {
                            propertyName += '.id';
                        }
                    }
                    if (filter.paramName != filter.propertyName) {
                        propertyName = filter.paramName;
                    }
                    wrapperModel[propertyName] = currentValue;
                    $targetParent.data('currentValue', $.extend({}, wrapperModel));
                    $targetParent.attr('data-query-to-perform', $.param(wrapperModel));
                    console.debug("[filter]", filter, " [dirtyChange] => [", propertyName, "] Current property value:", currentValue, "All filters:", wrapperModel, "Filter element:", $pel);
                });
            }
        },
        renderProperty = function (modelDescriptor, modelName, $propertiesWrapper, model, propertyRenderer, elementViewType,
                                   elementIndexViewType, extraPropertiesClasses, $superContainer) {
            if (isHidden(model, propertyRenderer, elementViewType, elementIndexViewType)) {
                return;
            }
            var propertyElementTag = propertyTag(propertyRenderer, elementViewType, elementIndexViewType),
                propertyCssClassFromConfig = elementViewType === 'FORM_FRAGMENT' ? propertyRenderer.cssClass.replace('property-presentation', 'input-field') : propertyRenderer.cssClass,
                propertyCssClass = evaluateExpressionForPropertyRenderer(propertyCssClassFromConfig, propertyRenderer),
                $propertyElement = $('<' + propertyElementTag + ' class="' + propertyCssClass + '"></' + propertyElementTag + '>'),
                propertyName = propertyRenderer.propertyName,
                propertyValue = model ? model[propertyName] : null;
            $propertyElement.attr({
                                      'data-model-property-name': propertyName,
                                      'data-model-name': modelName
                                  });
            $propertyElement.data('currentValue', model[propertyName]);
            var $group = $propertiesWrapper.filter('[data-group-name=' + propertyRenderer.groupName + ']'), $pwc,
                $targetParent;
            if ($group.length > 0) {
                $pwc = $group.children('.properties-wrapper-content');
                if ($pwc.length > 0) {
                    $targetParent = $pwc;
                } else {
                    $targetParent = $group.first();
                }
            } else {
                $pwc = $propertiesWrapper.children('.properties-wrapper-content');
                if ($pwc.length > 0) {
                    $targetParent = $pwc.first();
                } else {
                    $targetParent = $propertiesWrapper.first();
                }
            }
            if (elementViewType === 'FORM_FRAGMENT' && !(propertyRenderer.readonly)) {
                renderPropertyInput(modelDescriptor, model, $propertyElement, propertyRenderer, elementViewType,
                                    elementIndexViewType, $targetParent, $superContainer);
            } else {
                renderPropertyPresentation(modelDescriptor, model, $propertyElement, propertyRenderer, elementViewType,
                                           elementIndexViewType, $targetParent, $superContainer);
                if (elementViewType === 'FORM_FRAGMENT') {
                    $propertyElement.addClass('col s12 l' + propertyRenderer.inputFieldSize.size);
                }
                $propertyElement.addClass('property-presentation property-input-wrapper').removeClass('input-field');
            }
            if (extraPropertiesClasses && extraPropertiesClasses[propertyName] && extraPropertiesClasses[propertyName].length) {
                $propertyElement.addClass(extraPropertiesClasses[propertyName].join(' '));
            }
            $propertyElement.data('propertyRenderer', propertyRenderer);
            $propertyElement.attr('data-order-in-form', propertyRenderer.orderInForm);
            $propertyElement.attr('data-order-in-view', propertyRenderer.order);
            $propertyElement.attr('data-renderer-type-name', propertyRenderer.rendererType.name);
            $propertyElement.attr('data-input-type-name', propertyRenderer.inputType.name);
            $targetParent.append($propertyElement);
            if (!$superContainer.hasClass('renderer-template') && $superContainer[0].tagName === 'LI') {
                updateIds($targetParent, '__collection-item-index__', $superContainer.data('collectionItemIndex'));
            }
            $propertyElement.triggerHandler("app:propertyAppended");
        },
        getPropertyRenderer = function (modelDescriptor, propertyName) {
            return propertyName && modelDescriptor.propertyNameToIndex[propertyName] !== undefined ? modelDescriptor.modelProperties[modelDescriptor.propertyNameToIndex[propertyName]] : null;
        },
        findPropertyElement = function ($context, modelDescriptor, propertyName) {
            var propertyRenderer = getPropertyRenderer(modelDescriptor, propertyName),
                propertyElementSelector = propertyRenderer ? evaluateExpressionForPropertyRenderer(
                    commonConstants.PROPERTY_SELECTOR,
                    propertyRenderer) : null;
            return propertyElementSelector ? $context.nearest(propertyElementSelector) : $();
        },
        findPropertyElementByArg = function ($context, modelName, propertyName) {
            var propertyElementSelector = evaluateExpressionForModelPropertyAndView(commonConstants.PROPERTY_SELECTOR, modelName, propertyName);
            return $context.nearest(propertyElementSelector);
        },
        findRootProperty = function (propertyName, value) {
            if (typeof value === 'object') {
                return findRootProperty(propertyName, value[propertyName]);
            }
            return value;
        },
        createTableHeader = function (propertyRenderer, modelDescriptor) {
            var $tag = $('<th></th>');
            MetaRenderer.prepareForI18n($tag, propertyRenderer.label);
            return $tag;
        },
        refineListView = function (modelDescriptor, $element, $labelProperty, $imageProperty) {
            if ($imageProperty.length/* === 1*/) {
                $element.addClass('avatar');
                $element.nearest('.image-wrapper').html($imageProperty.detach());
            } else {
                $element.removeClass('avatar');
                $element.nearest('.image-wrapper').hide();
            }
            if ($labelProperty.length/* === 1*/) {
                $element.nearest('.title').html($labelProperty.detach());
            } else {
                $element.nearest('.title').hide();
            }
        },
        refineGridView = function (modelDescriptor, $element, $labelProperty, $imageProperty) {
            var $propertiesWrapperContent = $element.nearest('.properties-wrapper-content');
            $element.prepend($propertiesWrapperContent.children());
            $propertiesWrapperContent.remove();
        },
        refineTreeView = function (modelDescriptor, $element, $labelProperty, $imageProperty) {
            var options = {
                onOpen: function ($modelView) {
                    var modelViewData = $modelView.data(),
                        modelDescriptor = modelViewData.modelDescriptor,
                        $collapsibleBody = $modelView.nearest('.collapsible-body'),
                        getChildrenUri = '/' + modelDescriptor.basePath,
                        onSuccess = function (responseData, textStatus, jqXHR) {
                            var $html = $(responseData).nearest('.index-fragment', true).children();
                            $collapsibleBody.html($html);
                            App.getInstance().update(event, $html);
                            EventsManager.getInstance().fire({
                                                                 name: EventsManager.HTML_REQUEST,
                                                                 selector: $html
                                                             }, $html);
                        },
                        ajaxOptions = {
                            beforeSend: function (jqXHR) {
                                jqXHR.setRequestHeader(MetaController.X_FRAGMENT_HEADER, 'yes');
                                return true;
                            }
                        };
                    console.log(modelViewData);
                    Request.send(getChildrenUri, {parentId: modelViewData.modelUid}, onSuccess, 'text', 'GET', ajaxOptions);
                }
            };
            $element.collapsible(options);
        },
        refineCardView = function (modelDescriptor, $element, $labelProperty, $imageProperty) {
            var $cardImage = $element.nearest('.card-image'),
                $labelWrapper = $element.nearest('.label-wrapper'),
                $secondaryProperties = $element.nearest('.card-reveal .secondary-properties-wrapper');
            if ($labelProperty.length >= 1) {
                $labelWrapper.html($labelProperty.first().detach());
            }
            if ($cardImage.length === 1) {
                if ($imageProperty.length/* === 1*/) {
                    var $imageTag = $imageProperty.detach().find('.image-tag'),
                        isBackground = $imageTag.hasClass('background-image'),
                        $imageInsteadOfBackground = isBackground ? $('<img class="image-tag"/>') : $imageTag;

                    if (isBackground) {
                        var src = /url\(\"(.*?)\"\)/g.exec($imageTag.css('backgroundImage'))[1];
                        $imageInsteadOfBackground.attr('src', src);
                    }

                    $cardImage.nearest('.image-wrapper').html($imageInsteadOfBackground);
                } else {
                    $cardImage.hide();
                }
            }
            $.each(modelDescriptor.modelProperties, function (index, propertyRenderer) {
                if (propertyRenderer.secondaryContent) {
                    var $secondaryProperty = findPropertyElement($element, modelDescriptor,
                                                                 propertyRenderer.propertyName);
                    $secondaryProperty.detach().appendTo($secondaryProperties);
                }
            });
            if ($secondaryProperties.children().length === 0) {
                $element.nearest('.activator').hide();
            } else {
                $element.nearest('.activator').show();
            }
        },
        processActionsForIndex = function ($modelView, modelDescriptor, model, indexViewType) {
            var $actionsWrapper = $modelView.find('.actions-wrapper'),
                $moreActionsController = $actionsWrapper.nearest('.more-actions-controller'),
                $mainActionsContainer = $actionsWrapper.nearest('.main-actions'),
                $dropDown = $('#' + $moreActionsController.data('activates'));

            if (!$actionsWrapper.data('actionsProcessed')) {
                $actionsWrapper.data('actionsProcessed', true);
                var actionsArray = [];
                $.each(modelDescriptor.actions, function (actionId, action) {
                   actionsArray.push(action);
                });
                actionsArray.sort(function(a1, a2) {
                    return a1.order - a2.order;
                });
                $.each(actionsArray, function (actionId, action) {
                    if (!action.flowAction && !action.fixedAction) {
                        console.debug("      ---> Process action:", action);
                        MetaController.createAction(action, null, modelDescriptor, model, commonConstants.VIEW_TYPE.INDEX_FRAGMENT)
                            .done(function ($controller) {
                                if ($controller != null) {
                                    if (indexViewType === commonConstants.INDEX_VIEW_TYPE.TREE) {

                                    }
                                    if (action.mainAction) {
                                        $mainActionsContainer.append($controller);
                                    } else {
                                        var $li = $('<li>/</li>');
                                        $li.html($controller);
                                        $dropDown.append($li);
                                    }
                                    if ($dropDown.children().length === 0) {
                                        $moreActionsController.hide();
                                    } else {
                                        $moreActionsController.show();
                                    }
                                }
                            });
                    }
                });
                if ($dropDown.children().length === 0) {
                    $moreActionsController.hide();
                } else {
                    $moreActionsController.show();
                }
            }
        },
        processActionsForForm = function ($modelView, modelDescriptor, model) {
            var $formActionsWrapper = $modelView.find('.actions-wrapper'),
                $pageActionsWrapper = $modelView.closest('.form-section').nearest('.actions-wrapper'),
                $form = $modelView.nearest('form'),
                formType = $modelView.data('formType'),
                currentViewAction = $modelView.data('action');
            if (currentViewAction && $formActionsWrapper.length === 1 && $form.length === 1 && !$formActionsWrapper.data('actionsProcessed')) {
                console.debug("Process actions for form:", $form);
                $formActionsWrapper.data('actionsProcessed', true);
                $form.data('action', currentViewAction);

                $.each(modelDescriptor.actions, function (actionId, action) {
                    if (action.availableInForm || action.availableOnlyInForm) {
                        console.debug("      ---> Process action:", action);
                        MetaController.createAction(action, $form, modelDescriptor, model, commonConstants.VIEW_TYPE.FORM_FRAGMENT)
                            .done(function ($controller) {
                                if ($controller != null) {
                                    if (action.type.name === formType) {
                                        $controller.addClass('right');
                                        $form.attr('action', $controller.data('target'));
                                        $formActionsWrapper.append($controller);
                                    } else if ($.inArray(currentViewAction.actionId, action.backToFor) !== -1) {
                                        $controller.addClass('left back-action');
                                        $pageActionsWrapper.append($controller);
                                    } else if ($.inArray(currentViewAction.actionId, action.forwardFor) !== -1) {
                                        $controller.addClass('right forward-action');
                                        $pageActionsWrapper.append($controller);
                                    } else {
                                        console.debug("       ----> Invalid");
                                    }
                                } else {
                                    console.debug("       ----> Invalid");
                                }
                            });
                    }
                });
            }
        },
        processActionsForView = function ($modelView, modelDescriptor, model) {
            var $pageActionsWrapper = $modelView.closest('.view-section').nearest('.actions-wrapper'),
                currentViewAction = $modelView.data('action');
            if (currentViewAction && $pageActionsWrapper.length === 1 && !$pageActionsWrapper.data('actionsProcessed')) {
                console.debug("Process actions for presentation:", $modelView);
                $pageActionsWrapper.data('actionsProcessed', true);

                $.each(modelDescriptor.actions, function (actionId, action) {
                    if (action.availableInPresentation || action.availableOnlyInPresentation) {
                        console.debug("      ---> Process action:", action);
                        MetaController.createAction(action, null, modelDescriptor, model, commonConstants.VIEW_TYPE.VIEW_FRAGMENT)
                            .done(function ($controller) {
                                if ($controller != null) {
                                    if ($.inArray(currentViewAction.actionId, action.backToFor) !== -1) {
                                        $controller.addClass('left back-action');
                                        $pageActionsWrapper.append($controller);
                                    } else if ($.inArray(currentViewAction.actionId, action.forwardFor) !== -1) {
                                        $controller.addClass('right forward-action');
                                        $pageActionsWrapper.append($controller);
                                    } else {
                                        console.debug("       ----> Invalid");
                                    }
                                } else {
                                    console.debug("       ----> Invalid");
                                }
                            });
                    }
                });
            }
        },
        processItemActionsPerModelView = function (modelDescriptor, $modelView, model, viewType, indexViewType) {
            // console.debug("Process actions for item:", $modelView);
            switch (viewType) {
                case commonConstants.VIEW_TYPE.INDEX_FRAGMENT:
                    processActionsForIndex($modelView, modelDescriptor, model, indexViewType);
                    break;
                case commonConstants.VIEW_TYPE.FORM_FRAGMENT:
                    processActionsForForm($modelView, modelDescriptor, model);
                    break;
                case commonConstants.VIEW_TYPE.VIEW_FRAGMENT:
                    processActionsForView($modelView, modelDescriptor, model);
                    break;
            }
        },
        processFixedActions = function (modelDescriptor, $element) {
            var $fixedActionsHolder = $element.find('.index-bulk-actions-wrapper'),
                actionsList = [];
            if ($fixedActionsHolder.length === 1 && !$fixedActionsHolder.data('isUiUpdated')) {
                $.each(modelDescriptor.actions, function (actionId, action) {
                    if (!action.flowAction && action.fixedAction) {
                        console.debug("Process bulk action:", action);
                        MetaController.createAction(action, null, modelDescriptor, null, commonConstants.VIEW_TYPE.INDEX_FRAGMENT)
                            .done(function ($controller) {
                                if ($controller != null) {
                                    actionsList.push($controller);
                                }
                            });
                    }
                });
                if (actionsList.length > 0) {
                    $fixedActionsHolder.addClass('fixed-action-btn top-down');
                    if (actionsList.length > 1) {
                        var $actionsListItems = $('<ul></ul>'),
                            toggleMenu = $fixedActionsHolder.data('toggle-menu-label') || "add";
                        $.each(actionsList, function (index, $controller) {
                            $controller.addClass('btn-floating btn-large');
                            $actionsListItems.append($controller);
                        });
                        var $toggleController = $('<a class="btn-floating btn-large no-preloader">' + toggleMenu + '</a>');
                        //var $toggleController = $('<a class="btn-floating btn-large"><i class="material-icons">add</i></a>');
                        $fixedActionsHolder.append($toggleController);
                        $fixedActionsHolder.append($actionsListItems);
                    } else {
                        var $controller = actionsList[actionsList.length - 1];
                        $controller.addClass('btn-floating btn-large waves-effect waves-light');
                        $fixedActionsHolder.append($controller);
                    }
                }
                $fixedActionsHolder.data('isUiUpdated', true);
                MetaController.controllersTooltips($fixedActionsHolder.find('.meta-controller'));
            }
        },
        timePicker = function ($propertyElement, propertyRenderer, value, $superContainer) {
            $propertyElement.on("app:propertyAppended", function () {

                $(this).nearest('input.property-input').remove();
                
                var $timeInput = $('<input type="text" class="timepicker-input-part">');
                $(this).append($timeInput);
                if (value) {
                    $timeInput.val($.format.date(value, app.g11n.lang.app.dateFormats.SHORT_TIME))
                }

                $timeInput.pickatime({
                    default: 'now', // Set default time: 'now', '1:30AM', '16:30'
                    fromnow: 0,       // set default time to * milliseconds from now (using
                                      // with default = 'now')
                    twelvehour: false, // Use AM/PM or 24-hour format
                    donetext: MetaRenderer.i18nMessage('datePicker.ok'), // text for
                                                                         // done-button
                    cleartext: MetaRenderer.i18nMessage('datePicker.clear'), // text for
                                                                             // clear-button
                    canceltext: MetaRenderer.i18nMessage('action.cancel'), // Text for
                                                                           // cancel-button
                    autoclose: true, // automatic close timepicker
                    ampmclickable: true
                });
                $timeInput.change(function (e) {
                    var $timeInput = $(e.currentTarget),
                        timeString = $timeInput.val(),
                        time = hmsToSecondsOnly(timeString + ":00"),
                        $propertyElement = $timeInput.closest('.property-input-wrapper'),
                        datetime = null;
                    $propertyElement.find("label").addClass("active");
                    datetime = (new Date(datetime)).setHours(0, 0, 0, 0) + time * 1000;
                    $propertyElement.data('currentValue', datetime);
                    $propertyElement.data('currentValueString', timeString);
                    $propertyElement.trigger('app:dirtyChange', [true, $propertyElement.data('modelPropertyName'), datetime, $timeInput, e]);
                });

            });
        },
        datePicker = function ($propertyElement, propertyRenderer, value, $superContainer, withTimePicker, extraOptions) {
            var options = $.extend(datePickerI18nObject(), {
                format: app.g11n.lang.app.dateFormats.LONG_DATE.replace("MMMM", "mmmm")
                    .replace("EEE", "ddd"),
                selectYears: extraOptions === undefined ? false : extraOptions.selectYears,
                selectMonths: extraOptions === undefined ? false : extraOptions.selectMonths,
                min: extraOptions === undefined ? undefined : extraOptions.min,
                max: extraOptions === undefined ? undefined : extraOptions.max,
                firstDay: 1,
                closeOnSelect: true,
                onSet: function (context) {
                    var $input = $(this)[0].$node,
                        $label = $input.siblings('label'),
                        currentValue = context.select || null;
                    $input.data('currentValue', currentValue);
                    $input.data('currentValueString', $input.val());
                    if (currentValue || $input.val()) {
                        $label.addClass('active');
                    } else {
                        $label.removeClass('active');
                    }
                    if (propertyRenderer.inputFieldValidation) {
                        $input.change().validate();
                    }
                }
            }, $propertyElement.data('datePickerOptions'));
            $propertyElement.on("app:propertyAppended", function () {
                var $input = $(this).nearest('input.property-input'),
                    $timeInput = $('<input type="text" class="timepicker-input-part">');
                $input.attr('type', 'TEXT');
                $input.removeClass('validate');
                if (withTimePicker) {
                    $input.addClass('datepicker-input-part');
                    $(this).append($timeInput);
                    if (value) {
                        $input.val($.format.date(value, app.g11n.lang.app.dateFormats.LONG_DATE.replace("EEE", "ddd")));
                        $timeInput.val($.format.date(value, app.g11n.lang.app.dateFormats.SHORT_TIME))
                    }
                    $input.pickadate(options);
                    $timeInput.pickatime({
                                             default: 'now', // Set default time: 'now', '1:30AM', '16:30'
                                             fromnow: 0,       // set default time to * milliseconds from now (using
                                                               // with default = 'now')
                                             twelvehour: false, // Use AM/PM or 24-hour format
                                             donetext: MetaRenderer.i18nMessage('datePicker.ok'), // text for
                                                                                                  // done-button
                                             cleartext: MetaRenderer.i18nMessage('datePicker.clear'), // text for
                                                                                                      // clear-button
                                             canceltext: MetaRenderer.i18nMessage('action.cancel'), // Text for
                                                                                                    // cancel-button
                                             autoclose: true, // automatic close timepicker
                                             ampmclickable: true
                                         });
                    $timeInput.change(function (e) {
                        var $timeInput = $(e.currentTarget),
                            timeString = $timeInput.val(),
                            time = hmsToSecondsOnly(timeString + ":00"),
                            $propertyElement = $timeInput.closest('.property-input-wrapper'),
                            datetime = $propertyElement.data('currentValue');
                        datetime = (new Date(datetime)).setHours(0, 0, 0, 0) + time * 1000;
                        $propertyElement.data('currentValue', datetime);
                        $propertyElement.trigger('app:dirtyChange', [true, $propertyElement.data('modelPropertyName'), datetime, $timeInput, e]);
                    });
                } else {
                    if (value) {
                        $input.val($.format.date(value, app.g11n.lang.app.dateFormats.LONG_DATE.replace("EEE", "ddd")));
                    }
                    $input.pickadate(options);
                }
            });
        },
        hmsToSecondsOnly = function (str) {
            var p = str.split(':'),
                s = 0, m = 1;

            while (p.length > 0) {
                s += m * parseInt(p.pop(), 10);
                m *= 60;
            }

            return s;
        },
        datePickerI18nObject = function () {
            if (!_datePickerI18nObjectCached) {
                _datePickerI18nObjectCached = {
                    today: MetaRenderer.i18nMessage('datePicker.today'),
                    clear: MetaRenderer.i18nMessage('datePicker.clear'),
                    close: MetaRenderer.i18nMessage('datePicker.ok'),
                    labelMonthNext: MetaRenderer.i18nMessage('datePicker.labelMonthNext'),
                    labelMonthPrev: MetaRenderer.i18nMessage('datePicker.labelMonthPrev'),
                    labelMonthSelect: MetaRenderer.i18nMessage('datePicker.labelMonthSelect'),
                    labelYearSelect: MetaRenderer.i18nMessage('datePicker.today'),
                    monthsFull: [
                        MetaRenderer.i18nMessage('datePicker.monthsFull.january'),
                        MetaRenderer.i18nMessage('datePicker.monthsFull.february'),
                        MetaRenderer.i18nMessage('datePicker.monthsFull.march'),
                        MetaRenderer.i18nMessage('datePicker.monthsFull.April'),
                        MetaRenderer.i18nMessage('datePicker.monthsFull.may'),
                        MetaRenderer.i18nMessage('datePicker.monthsFull.june'),
                        MetaRenderer.i18nMessage('datePicker.monthsFull.july'),
                        MetaRenderer.i18nMessage('datePicker.monthsFull.august'),
                        MetaRenderer.i18nMessage('datePicker.monthsFull.september'),
                        MetaRenderer.i18nMessage('datePicker.monthsFull.october'),
                        MetaRenderer.i18nMessage('datePicker.monthsFull.november'),
                        MetaRenderer.i18nMessage('datePicker.monthsFull.december')
                    ],
                    monthsShort: [
                        MetaRenderer.i18nMessage('datePicker.monthsShort.january'),
                        MetaRenderer.i18nMessage('datePicker.monthsShort.february'),
                        MetaRenderer.i18nMessage('datePicker.monthsShort.march'),
                        MetaRenderer.i18nMessage('datePicker.monthsShort.April'),
                        MetaRenderer.i18nMessage('datePicker.monthsShort.may'),
                        MetaRenderer.i18nMessage('datePicker.monthsShort.june'),
                        MetaRenderer.i18nMessage('datePicker.monthsShort.july'),
                        MetaRenderer.i18nMessage('datePicker.monthsShort.august'),
                        MetaRenderer.i18nMessage('datePicker.monthsShort.september'),
                        MetaRenderer.i18nMessage('datePicker.monthsShort.october'),
                        MetaRenderer.i18nMessage('datePicker.monthsShort.november'),
                        MetaRenderer.i18nMessage('datePicker.monthsShort.december')
                    ],
                    weekdaysFull: [
                        MetaRenderer.i18nMessage('datePicker.weekdaysFull.sunday'),
                        MetaRenderer.i18nMessage('datePicker.weekdaysFull.monday'),
                        MetaRenderer.i18nMessage('datePicker.weekdaysFull.tuesday'),
                        MetaRenderer.i18nMessage('datePicker.weekdaysFull.wednesday'),
                        MetaRenderer.i18nMessage('datePicker.weekdaysFull.thursday'),
                        MetaRenderer.i18nMessage('datePicker.weekdaysFull.friday'),
                        MetaRenderer.i18nMessage('datePicker.weekdaysFull.saturday')
                    ],
                    weekdaysShort: [
                        MetaRenderer.i18nMessage('datePicker.weekdaysShort.sunday'),
                        MetaRenderer.i18nMessage('datePicker.weekdaysShort.monday'),
                        MetaRenderer.i18nMessage('datePicker.weekdaysShort.tuesday'),
                        MetaRenderer.i18nMessage('datePicker.weekdaysShort.wednesday'),
                        MetaRenderer.i18nMessage('datePicker.weekdaysShort.thursday'),
                        MetaRenderer.i18nMessage('datePicker.weekdaysShort.friday'),
                        MetaRenderer.i18nMessage('datePicker.weekdaysShort.saturday')
                    ],
                    weekdaysLetter: [
                        MetaRenderer.i18nMessage('datePicker.weekdaysLetter.sunday'),
                        MetaRenderer.i18nMessage('datePicker.weekdaysLetter.monday'),
                        MetaRenderer.i18nMessage('datePicker.weekdaysLetter.tuesday'),
                        MetaRenderer.i18nMessage('datePicker.weekdaysLetter.wednesday'),
                        MetaRenderer.i18nMessage('datePicker.weekdaysLetter.thursday'),
                        MetaRenderer.i18nMessage('datePicker.weekdaysLetter.friday'),
                        MetaRenderer.i18nMessage('datePicker.weekdaysLetter.saturday')
                    ],
                    timeAgo: {
                        prefixAgo: MetaRenderer.i18nMessage('timeAgo.prefixAgo'),//"acum",
                        prefixFromNow: MetaRenderer.i18nMessage('timeAgo.prefixFromNow'),//"peste",
                        suffixAgo: MetaRenderer.i18nMessage('timeAgo.suffixAgo'),//"",
                        suffixFromNow: MetaRenderer.i18nMessage('timeAgo.suffixFromNow'),//"",
                        seconds: MetaRenderer.i18nMessage('timeAgo.seconds'),//"mai putin de un minut",
                        minute: MetaRenderer.i18nMessage('timeAgo.minute'),//"un minut",
                        minutes: MetaRenderer.i18nMessage('timeAgo.minutes'),//"%d minute",
                        hour: MetaRenderer.i18nMessage('timeAgo.hour'),//"o ora",
                        hours: MetaRenderer.i18nMessage('timeAgo.hours'),//"%d ore",
                        day: MetaRenderer.i18nMessage('timeAgo.day'),//"o zi",
                        days: MetaRenderer.i18nMessage('timeAgo.days'),//"%d zile",
                        month: MetaRenderer.i18nMessage('timeAgo.month'),//"o lună",
                        months: MetaRenderer.i18nMessage('timeAgo.months'),//"%d luni",
                        year: MetaRenderer.i18nMessage('timeAgo.year'),//"un an",
                        years: MetaRenderer.i18nMessage('timeAgo.years'),//"%d ani",
                        more: MetaRenderer.i18nMessage('timeAgo.more'),//"o vesnicie",
                        wordSeparator: MetaRenderer.i18nMessage('timeAgo.wordSeparator')//" "
                    }
                };
            }
            return _datePickerI18nObjectCached;
        },
        indexFragmentViewTypeExtraProcessor = function ($element, $propertiesWrapper, modelDescriptor, elementIndexViewType) {
            var $imageProperty = findPropertyElement($element, modelDescriptor, modelDescriptor.imageProperty),
                $labelProperty = findPropertyElement($element, modelDescriptor, modelDescriptor.labelProperty);
            switch (elementIndexViewType) {
                case commonConstants.INDEX_VIEW_TYPE.LIST:
                    refineListView(modelDescriptor, $element, $labelProperty, $imageProperty);
                    break;
                case commonConstants.INDEX_VIEW_TYPE.CARD:
                    refineCardView(modelDescriptor, $element, $labelProperty, $imageProperty);
                    break;
                case commonConstants.INDEX_VIEW_TYPE.GRID:
                    refineGridView(modelDescriptor, $element, $labelProperty, $imageProperty);
                    break;
                case commonConstants.INDEX_VIEW_TYPE.TREE:
                    refineTreeView(modelDescriptor, $element, $labelProperty, $imageProperty);
                    break;
            }
        },
        viewFragmentViewTypeExtraProcessor = function ($element, $propertiesWrapper, modelDescriptor, elementIndexViewType, model) {
        },
        formFragmentViewTypeExtraProcessor = function ($element, $propertiesWrapper, modelDescriptor, elementIndexViewType, model) {
            var $imageProperty = findPropertyElement($element, modelDescriptor, modelDescriptor.imageProperty),
                $labelProperty = findPropertyElement($element, modelDescriptor, modelDescriptor.labelProperty);
            /* Sort by orderInForm */
            $propertiesWrapper.each(function () {
                var $pw = $(this), $pwc = $pw.children('.properties-wrapper-content'),
                    $targetPw = $pw;
                if ($pwc.length > 0) {
                    $targetPw = $pwc;
                }
                $targetPw.children('.model-property-value')
                    .sort(function (a, b) {
                        var $a = $(a), $b = $(b), left = $a.data('orderInForm') || 0,
                            right = $b.data('orderInForm') || 0;
                        return left - right;
                    }).appendTo($targetPw);
            });
            var $form = $element.nearest('.model-form'),
                actionUrl;
            if ($form.length && $form[0].tagName === 'FORM') {
                if (!model || model.new) {
                    actionUrl = '/' + modelDescriptor.basePath;
                    $form.attr('method', 'POST');
                } else {
                    actionUrl = '/' + modelDescriptor.basePath + model.objectUID;
                    $form.attr('method', 'PUT');
                }
            }
            $form.data({
                           currentValue: model
                       });
            $form.attr('action', actionUrl);
            console.log("Form", $form);
        },
        referenceRenderer = function (modelName, $element, modelDescriptor, model, rendererType, elementViewType, elementIndexViewType) {
            try {
                var modelSimulator = {_self: model},
                    propertyRenderer = {
                        propertyName: '_self',
                        rendererType: {name: rendererType},
                        propertyHolderModelName: modelName
                    };
                $element.empty();
                renderPropertyPresentation(modelDescriptor, modelSimulator, $element, propertyRenderer, elementViewType,
                                           elementIndexViewType);
            } catch (e) {
                console.error(e);
            }
        },
        propertiesWrapperRenderer = function (modelName, $element, $propertiesWrapper, modelDescriptor, model, elementViewType, elementIndexViewType, extraPropertiesClasses) {
            $propertiesWrapper.each(function () {
                var $pw = $(this), $pwc = $pw.children('.properties-wrapper-content');
                if ($pwc.length > 0) {
                    $pwc.empty();
                } else {
                    $pw.empty();
                }

            });
            $.each(modelDescriptor.modelProperties, function (index, propertyRenderer) {
                try {
                    renderProperty(modelDescriptor, modelName, $propertiesWrapper, model, propertyRenderer, elementViewType,
                                   elementIndexViewType, extraPropertiesClasses, $element);
                } catch (e) {
                    console.error(e);
                }
            });
            switch (elementViewType) {
                case commonConstants.VIEW_TYPE.INDEX_FRAGMENT:
                    indexFragmentViewTypeExtraProcessor($element, $propertiesWrapper, modelDescriptor, elementIndexViewType);
                    break;
                case commonConstants.VIEW_TYPE.VIEW_FRAGMENT:
                    viewFragmentViewTypeExtraProcessor($element, $propertiesWrapper, modelDescriptor, elementIndexViewType, model);
                    break;
                case commonConstants.VIEW_TYPE.FORM_FRAGMENT:
                    formFragmentViewTypeExtraProcessor($element, $propertiesWrapper, modelDescriptor, elementIndexViewType, model);
                    break;
            }
        },
        updateUiElementAttributes = function ($element, model, modelName, modelDescriptor) {
            MetaRenderer.i18nLabels($element.find('[data-x-label][data-x-label-status!=true]'));
            $element.addClass("model-" + modelName);
            $element.attr('data-is-ui-updated', true);
            $element.data('isUiUpdated', true);
            $element.data('currentValue', model);
            $element.trigger("app:uiUpdated");
        },
        setObjectLabel = function ($propertyElement, $target, model, modelName) {
            var cb = $propertyElement.data('setObjectLabel') || MetaRenderer.objectLabel;
            cb.call($propertyElement, $target, model, modelName);
        },
        updateIds = function ($target, variablePart, variable) {
            $target.find('[id],[for],[data-activates],[data-select-id]').each(function () {
                var $el = $(this),
                    id = $el.attr("id"),
                    forAttr = $el.attr("for"),
                    dataActivates = $el.attr("data-activates"),
                    dataSelectId = $el.attr("data-select-id");
                if (id) {
                    $el.attr("id", id.replace(variablePart, variable));
                }
                if (forAttr) {
                    $el.attr("for", forAttr.replace(variablePart, variable));
                }
                if (dataActivates) {
                    $el.attr("data-activates", dataActivates.replace(variablePart, variable));
                }
                if (dataSelectId) {
                    $el.attr("data-select-id", dataSelectId.replace(variablePart, variable));
                }
            });
        };
    return {
        renderAvatar:function ($wrapper,avatarHolder) {
           avatarModelRenderer($wrapper,avatarHolder);
        },
        showMessageOnNull: function ($propertyElement, propertyRenderer) {
            var nullPropertyMessageCode = evaluateExpressionForModelPropertyAndView(commonConstants.PROPERTY_NULL_VALUE, propertyRenderer.propertyHolderModelName, propertyRenderer.propertyName, propertyRenderer.viewName);
            $propertyElement.append('<span class="property-null-value">' + MetaRenderer.i18nMessage(
                nullPropertyMessageCode) + '</span>');
        },
        findProperty: function ($context, modelName, propertyName) {
            var $p = findPropertyElementByArg($context, modelName, propertyName);
            return $p;
        },
        dateI18nObject: function () {
            return datePickerI18nObject();
        },
        objectLabel: function ($target, model, modelName) {
            if (model && typeof model === 'string') {
                $target.html(model);
            } else if (model && typeof model === 'object' && model._self) {
                MetaRenderer.objectLabel($target, model._self, modelName);
            } else if (model && typeof model === 'object' && modelName) {
                ModelStore.promiseOf(modelName, true).done(function () {
                    var md = this.getRestMetaModel().getModelDescriptor(),
                        labelPropertyRenderer = getPropertyRenderer(md, md.labelProperty);
                    if (labelPropertyRenderer) {
                        MetaRenderer.objectLabel($target, model[md.labelProperty], labelPropertyRenderer.modelName);
                    }
                });
            }
        },
        scrollTo: function ($target, offset, duration) {
            $('html, body').animate({scrollTop: $target.offset().top + (offset || 0)}, duration || 400);
        },
        setDisabled: function ($target, disabledStatus, doNotSetProperty) {
            if (disabledStatus) {
                if (!doNotSetProperty) {
                    $target.prop('disabled', true);
                }
                $target.addClass('disabled');
            } else {
                if (!doNotSetProperty) {
                    $target.prop('disabled', false);
                }
                $target.removeClass('disabled');
            }
        },
        multiSelect: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $viewWrapper) {
            var value = model[propertyRenderer.propertyName],
                objectLabelProperty = 'objectLabel',
                options = $.extend(true, {
                    autocompleteOptions: {
                        limit: 5,
                        minLength: 1,
                        modelName: propertyRenderer.modelName,
                        viewName: propertyRenderer.valueObjectTargetViewName
                    },
                    autocompleteLimit: 20
                }, $propertyElement.data('multiSelectOptions'));
            simpleInputRenderer($propertyElement, propertyRenderer, value, $viewWrapper);
            var $input = $propertyElement.nearest('input.property-input'),
                $chips = $('<div class="chips chips-autocomplete input-field"></div>'),
                initialData = [];
            $input.attr('type', 'HIDDEN');
            // $input.off('change');
            $propertyElement.append($chips);
            if (value && value.length) {
                $.each(value, function (index, object) {
                    initialData.push({
                                         tag: object[objectLabelProperty],
                                         currentValue: object,
                                         image: object.imageSrc || object.avatarSrc || object.image,
                                         id: object.objectUID
                                     })
                });
            }
            options['data'] = initialData;
            $propertyElement.on("app:propertyAppended", function () {
                var $pel = $(this),
                    $chips = $pel.nearest('.chips.chips-autocomplete');
                $chips.material_chip(options);
            });

            $chips.on('chip.add chip.delete', function (e, chip) {
                var $c = $(e.target), currentValue = [], chips = $c.data('chips'),
                    $currentInput = $c.siblings('.property-input[type=HIDDEN]');
                if (chips) {
                    $.each(chips, function (index, chip) {
                        currentValue.push(chip.currentValue);
                    })
                }
                $currentInput.data('currentValue', currentValue);
                $currentInput.val(chips).change();
            })
        },
        objectBuilder: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $viewWrapper) {
            var embeddedViewName = propertyRenderer.valueObjectTargetViewName,
                embeddedModelDescriptor = propertyRenderer.embeddedModelDescriptor,
                currentValue = $propertyElement.data('currentValue') || {},
                setObjectLabel = function ($target, model, modelName) {
                    var cb = $propertyElement.data('setObjectLabel') || MetaRenderer.objectLabel;
                    cb.call($propertyElement, $target, model, modelName);
                },
                $input = $propertyElement.nearest('.property-input'),
                prepareButton = function ($button, clickHandler, tooltip) {
                    $button.off('click').on('click', clickHandler);
                    if (tooltip) {
                        $button.tooltip({
                                            tooltip: tooltip,
                                            position: 'top',
                                            delay: 50
                                        });
                    }
                    return $button;
                },
                collectionItemLabel = function ($item, model, modelName) {
                    setObjectLabel($item.nearest('.collapsible-header .object-label'), model, modelName);
                },
                prepareTemplate = function ($template, model) {
                    var $contentWrapper = $('<div class="collapsible-body"></div>'),
                        $header = $('<div class="collapsible-header header-template"><i class="material-icons">drag_handle</i><div class="object-label"></div></div>'),
                        $actionsWrapper = $('<div class="collection-item-actions-wrapper"></div>'),
                        $deleteButton = $('<a href="#remove-item" class="secondary-content red-text remove-item left no-ajax"><i class="material-icons small">delete</i></a>'),
                        $saveButton = $('<a href="#save-item" class="secondary-content green-text save-item right no-ajax"><i class="material-icons small">save</i></a>');

                    $actionsWrapper.append(prepareButton($saveButton, saveObject, MetaRenderer.i18nMessage(RestMetaModel.COLLECTION_BUILDER_SAVE_BUTTON_TOOLTIP, $template.data('modelName'))));
                    $actionsWrapper.append(prepareButton($deleteButton, removeObject, MetaRenderer.i18nMessage(RestMetaModel.COLLECTION_BUILDER_DELETE_BUTTON_TOOLTIP, $template.data('modelName'))));

                    $template.addClass('collection-item-template tree-item collection-item');
                    $template.data({
                                       modelViewType: commonConstants.VIEW_TYPE.FORM_FRAGMENT
                                   });
                    $template.find("[id]").each(function () {
                        var $idEl = $(this), id = $idEl.attr("id");
                        if (id.indexOf("__collection-item-index__") === -1) {
                            $idEl.attr("id", id + "-__collection-item-index__");
                        }
                    });
                    $template.append($header);
                    $template.append($contentWrapper);
                    $template.nearest('.properties-wrapper').appendTo($contentWrapper);
                    if (model) {
                        MetaRenderer.prepareForRenderer($template, model);
                        $template.removeClass('renderer-template collection-item-template');
                        $header.removeClass('header-template');
                        collectionItemLabel($template, model, model.modelName);
                    }
                    $contentWrapper.append($actionsWrapper);
                },
                doStartOnWrapperReady = function () {
                    ModelStore.promiseOf(propertyRenderer.modelName, !embeddedModelDescriptor, embeddedModelDescriptor)
                        .done(function () {
                            var modelStore = this,
                                embeddedModelDescriptor = modelStore.getRestMetaModel()
                                    .getModelDescriptor(embeddedViewName);
                            $propertyElement.data('embeddedModelDescriptor', embeddedModelDescriptor);
                            $propertyElement.trigger('app:propertyModelDescriptorReady', [embeddedModelDescriptor, currentValue]);
                        });
                },
                editMode = function ($collection, $item, editMode) {
                    var index = $item.data('collectionItemIndex');
                    if (editMode) {
                        $item.addClass('edit-mode-on');
                        $collection.collapsible('open', index);
                    } else {
                        $item.removeClass('edit-mode-on');
                        $collection.collapsible('close', index);
                    }
                },
                openObjectForm = function (e) {
                    var $button = $(e.currentTarget);
                    e.preventDefault();
                    e.stopPropagation();
                    if (!$button.prop('disabled')) {
                        var $collection = $button.closest('.collection'),
                            collectionSize = $collection.data('collectionSize'),
                            $template = $collection.nearest('.collection-item-template'),
                            $item = getReadyToInsertTemplate($template, collectionSize);
                        $collection.append($item);
                        editMode($collection, $item, true);
                        MetaRenderer.setDisabled($button, true);
                    }
                },
                getReadyToInsertTemplate = function ($template, collectionSize) {
                    var $t = $template.clone(true/*with data and events*/, true/*deepClone*/)
                        .removeClass('renderer-template collection-item-template')
                        .addClass('new-collection-item');
                    $t.data('collectionItemIndex', collectionSize);
                    $t.nearest('.collapsible-header').removeClass('header-template');
                    updateIds($t, '__collection-item-index__', collectionSize);
                    return $t;
                },
                removeObject = function (e) {
                    e.preventDefault();
                    e.stopPropagation();
                    var $deleteButton = $(e.currentTarget),
                        $item = $deleteButton.closest('.model-view'),
                        $collection = $deleteButton.closest('.collection'),
                        collectionSize = $collection.data('collectionSize'),
                        $currentInput = $item.closest('.property-input'),
                        itemData = $item.data(),
                        itemCurrentValue = $.extend({}, itemData.currentValue),
                        itemIndex = itemData.collectionItemIndex,
                        $propertyElement = $item.closest('.property-input-wrapper'),
                        propertyName = $propertyElement.data('modelPropertyName'),
                        propertyElementCurrentValue = $propertyElement.data('currentValue'),
                        $addButton = $propertyElement.nearest('.add-list-item'),
                        isRevert = $item.hasClass('new-collection-item');
                    e.preventDefault();
                    e.stopPropagation();
                    MetaRenderer.setDisabled($addButton, false);
                    MetaRenderer.scrollTo($addButton, -100);
                    editMode($collection, $item, false);
                    $currentInput.data('currentValue', null);
                    $propertyElement.data('currentValue', null);
                    $item.remove();
                    onInputChange(e, $currentInput, propertyName, null);
                },
                saveObject = function (e) {
                    e.preventDefault();
                    e.stopPropagation();
                    var $saveButton = $(e.currentTarget),
                        $item = $saveButton.closest('.model-view'),
                        itemData = $item.data(),
                        $collection = $saveButton.closest('.collection'),
                        collectionSize = $collection.data('collectionSize'),
                        $currentInput = $item.closest('.property-input'),
                        itemCurrentValue = $.extend({}, itemData.currentValue),
                        itemIndex = itemData.collectionItemIndex,
                        $propertyElement = $currentInput.closest('.property-input-wrapper'),
                        propertyName = $propertyElement.data('modelPropertyName'),
                        propertyElementCurrentValue = $propertyElement.data('currentValue'),
                        $addButton = $propertyElement.nearest('.add-list-item'),
                        isInsert = $item.hasClass('new-collection-item');
                    console.log("[saveObject]", $item, " [value] =>", itemCurrentValue);
                    $propertyElement.data('currentValue', itemCurrentValue);
                    collectionItemLabel($item, itemCurrentValue, itemData.modelName);
                    $item.removeClass('new-collection-item');
                    editMode($collection, $item, false);
                    MetaRenderer.scrollTo($addButton, -100);
                    onInputChange(e, $currentInput, propertyName, propertyElementCurrentValue);
                },
                init = function (e, embeddedModelDescriptor, items) {
                    var $pEl = $(this),
                        $label = $pEl.nearest('label.property-label'),
                        $collection = $('<ul class="property-value collection with-header collapsible popout"></ul>'),
                        $defaultTemplate = MetaRenderer.defaultModelViewTemplate(propertyRenderer.modelName, null, 'li'),
                        $template = MetaRenderer.findModelViewRendererTemplate($viewWrapper, propertyRenderer.modelName, null, $defaultTemplate),
                        $placeholders = $viewWrapper.find(evaluateRendererPlaceholderSelector(propertyRenderer.propertyName)),
                        $emptyCollection = $('<li class="collection-item empty-collection-wrapper tree-item"></li>'),
                        emptyMessageCode = evaluateExpressionForModelPropertyAndView(commonConstants.PROPERTY_EMPTY_VALUE, propertyRenderer.propertyHolderModelName, propertyRenderer.propertyName, propertyRenderer.viewName),
                        $collectionHeader = $('<li class="collection-header tree-item"><button class="btn-floating right secondary-content primary-bg add-list-item" type="button" name="add-list-item"><i class="large material-icons">keyboard_arrow_down</i></button></li>');
                    $collection.append($collectionHeader);
                    var $insertTemplate = $template.clone(true, true);
                    prepareTemplate($insertTemplate);
                    $collection.append($insertTemplate);
                    $insertTemplate.data('modelDescriptor', embeddedModelDescriptor);
                    $collection.appendTo($input);
                    MetaRenderer.renderModelInstance($insertTemplate, currentValue);
                    $collection.data('collectionSize', $collection.nearest('.collapsible-header:not(.header-template)').length);
                    var $expandObjectButton = $collectionHeader.find('button.add-list-item').first();
                    prepareButton($expandObjectButton, openObjectForm, MetaRenderer.i18nMessage(RestMetaModel.OBJECT_BUILDER_EXPAND_BUTTON_TOOLTIP, embeddedModelDescriptor.modelName));
                    $collectionHeader.prepend($label.addClass("left"));
                    $collection.collapsible({
                                                accordion: true,
                                                onOpen: function ($item) {
                                                    var $itemHeader = $item.nearest('.collapsible-header'),
                                                        $collection = $item.closest('.collection'),
                                                        $propertyElement = $item.closest('.property-input-wrapper'),
                                                        propertyElementCurrentValue = $propertyElement.data('currentValue'),
                                                        $addButton = $propertyElement.nearest('.add-list-item');

                                                    if ($item.hasClass('new-collection-item')) {
                                                        $itemHeader.hide();
                                                    }
                                                },
                                                onClose: function ($item) {
                                                    var $itemHeader = $item.nearest('.collapsible-header');
                                                    if (!$item.hasClass('new-collection-item')) {
                                                        $itemHeader.show();
                                                    }
                                                }
                                            });
                };
            $input.addClass('object-builder model-form');
            $propertyElement.on('app:propertyModelDescriptorReady', init);
            $propertyElement.addClass('object-builder-wrapper').removeClass("input-field");
            $propertyElement.on("app:propertyAppended", doStartOnWrapperReady);
        },
        collectionBuilder: function (model, $propertyElement, propertyRenderer, elementViewType, elementIndexViewType, $targetParent, $viewWrapper) {
            var prepareButton = function ($button, clickHandler, tooltip) {
                    $button.off('click').on('click', clickHandler);
                    if (tooltip) {
                        $button.tooltip({
                                            tooltip: tooltip,
                                            position: 'top',
                                            delay: 50
                                        });
                    }
                    return $button;
                },
                setObjectLabel = function ($pEl, $target, model, modelName) {
                    var cb = $pEl.data('setObjectLabel') || MetaRenderer.objectLabel;
                    cb.call($pEl, $target, model, modelName);
                },
                collectionItemLabel = function ($pEl, $item, model, modelName) {
                    setObjectLabel($pEl, $item.nearest('.collapsible-header .object-label'), model, modelName);
                },
                prepareTemplate = function ($pEl, $template, model) {
                    var $contentWrapper = $('<div class="collapsible-body"></div>'),
                        $header = $('<div class="collapsible-header header-template"><i class="material-icons drag-controller">drag_handle</i><div class="object-label"></div></div>'),
                        $actionsWrapper = $('<div class="collection-item-actions-wrapper"></div>'),
                        $deleteButton = $('<a href="#remove-item" class="secondary-content red-text remove-item left no-ajax"><i class="material-icons small">delete</i></a>'),
                        $saveButton = $('<a href="#save-item" class="secondary-content green-text save-item right no-ajax"><i class="material-icons small">save</i></a>');

                    $actionsWrapper.append(prepareButton($saveButton, saveHandler, MetaRenderer.i18nMessage(RestMetaModel.COLLECTION_BUILDER_SAVE_BUTTON_TOOLTIP, $template.data('modelName'))));
                    $actionsWrapper.append(prepareButton($deleteButton, removeHandler, MetaRenderer.i18nMessage(RestMetaModel.COLLECTION_BUILDER_DELETE_BUTTON_TOOLTIP, $template.data('modelName'))));

                    $template.addClass('collection-item-template tree-item collection-item');
                    $template.data({
                                       modelViewType: commonConstants.VIEW_TYPE.FORM_FRAGMENT
                                   });
                    $template.find("[id]").each(function () {
                        var $idEl = $(this), id = $idEl.attr("id");
                        if (id.indexOf("__collection-item-index__") === -1) {
                            $idEl.attr("id", id + "-__collection-item-index__");
                        }
                    });
                    $template.append($header);
                    $template.append($contentWrapper);
                    $template.nearest('.properties-wrapper').appendTo($contentWrapper);
                    if (model) {
                        MetaRenderer.prepareForRenderer($template, model);
                        $template.removeClass('renderer-template collection-item-template');
                        $header.removeClass('header-template');
                        collectionItemLabel($pEl, $template, model, model.modelName);
                    }
                    $contentWrapper.append($actionsWrapper);
                },
                doStartOnWrapperReady = function (e) {
                    var $pEl = $(this),
                        propertyRenderer = $pEl.data('propertyRenderer'),
                        embeddedViewName = propertyRenderer.valueObjectTargetViewName,
                        embeddedModelDescriptor = propertyRenderer.embeddedModelDescriptor,
                        itemNewObject = $pEl.data('itemNewObject') || {},
                        items = $pEl.data('currentValue') || [],
                        $input = $pEl.nearest('.property-input');
                    ModelStore.promiseOf(propertyRenderer.modelName, !embeddedModelDescriptor, embeddedModelDescriptor)
                        .done(function () {
                            var modelStore = this,
                                embeddedModelDescriptor = modelStore.getRestMetaModel()
                                    .getModelDescriptor(embeddedViewName);
                            $pEl.data('embeddedModelDescriptor', embeddedModelDescriptor);
                            $pEl.triggerHandler('app:propertyModelDescriptorReady', [embeddedModelDescriptor, items]);
                        });
                },
                editMode = function ($collection, $item, editMode) {
                    var index = $item.data('collectionItemIndex');
                    if (editMode) {
                        $item.addClass('edit-mode-on');
                        $collection.collapsible('open', index);
                    } else {
                        $item.removeClass('edit-mode-on');
                        $collection.collapsible('close', index);
                    }
                },
                openAddItemForm = function (e) {
                    var $button = $(e.currentTarget);
                    e.preventDefault();
                    e.stopPropagation();
                    if (!$button.prop('disabled')) {
                        var $collection = $button.closest('.collection'),
                            collectionSize = $collection.data('collectionSize'),
                            $template = $collection.nearest('.collection-item-template'),
                            $item = getReadyToInsertTemplate($template, collectionSize);
                        $collection.append($item);
                        editMode($collection, $item, true);
                        MetaRenderer.setDisabled($button, true);
                        $item.data('save-item-handler', saveItem);
                        $button.triggerHandler("app:openAddItemForm", $item);
                    }
                },
                getReadyToInsertTemplate = function ($template, collectionSize) {
                    var $t = $template.clone(true/*with data and events*/, true/*deepClone*/)
                        .removeClass('renderer-template collection-item-template')
                        .addClass('new-collection-item');
                    $t.data('collectionItemIndex', collectionSize);
                    $t.data('currentValue', $.extend(true, {}, $t.data('currentValue')));
                    $t.nearest('.collapsible-header').removeClass('header-template');
                    updateIds($t, '__collection-item-index__', collectionSize);
                    return $t;
                },
                removeHandler = function (e) {
                    e.preventDefault();
                    e.stopPropagation();
                    var $removeButton = $(e.currentTarget),
                        $item = $removeButton.closest('.model-view'),
                        $propertyElement = $item.closest('.property-input-wrapper');
                    if ($propertyElement.data('requiresConfirmation')) {
                        MetaRenderer.confirmationDialog(MetaRenderer.i18nMessage($propertyElement.data('confirmationMessage') || 'model.collectionBuilder.removeItem'),
                                                        function (e) {
                                                            e.preventDefault();
                                                            e.stopPropagation();
                                                            var $okButton = $(this),
                                                                $dialog = $okButton.closest('.modal');
                                                            $dialog.modal('close');
                                                            removeItem($item, e);
                                                        },
                                                        function () {
                                                            //on close
                                                        });
                    } else {
                        removeItem($item, e);
                    }
                },
                removeItem = function ($item, e) {
                    var $collection = $item.closest('.collection'),
                        collectionSize = $collection.data('collectionSize'),
                        $currentInput = $item.closest('.property-input'),
                        itemData = $item.data(),
                        itemCurrentValue = $.extend({}, itemData.currentValue),
                        itemIndex = itemData.collectionItemIndex,
                        $propertyElement = $item.closest('.property-input-wrapper'),
                        propertyName = $propertyElement.data('modelPropertyName'),
                        propertyElementCurrentValue = $propertyElement.data('currentValue') || [],
                        $addButton = $propertyElement.nearest('.add-list-item'),
                        isRevert = $item.hasClass('new-collection-item');
                    MetaRenderer.setDisabled($addButton, false);
                    MetaRenderer.scrollTo($addButton, -100);
                    editMode($collection, $item, false);
                    if (!isRevert) {
                        propertyElementCurrentValue.splice(itemIndex, 1);
                    }
                    $propertyElement.data('currentValue', propertyElementCurrentValue);
                    $currentInput.data('currentValue', propertyElementCurrentValue);
                    $item.nextAll().each(function () {
                        var $nextItem = $(this),
                            nextItemCurrentIndex = $nextItem.data('collectionItemIndex'),
                            nextItemNewIndex = nextItemCurrentIndex - 1;
                        $nextItem.data('collectionItemIndex', nextItemNewIndex);
                    });
                    $item.remove();
                    if (!isRevert) {
                        $collection.data('collectionSize', collectionSize - 1);
                    }
                    onInputChange(e, $currentInput, propertyName, propertyElementCurrentValue);
                },
                saveHandler = function (e) {
                    e.preventDefault();
                    e.stopPropagation();
                    var $saveButton = $(e.currentTarget),
                        $item = $saveButton.closest('.model-view');
                    saveItem($item, e);
                },
                saveItem = function ($item, e) {
                    var itemData = $item.data(),
                        $collection = $item.closest('.collection'),
                        collectionSize = $collection.data('collectionSize'),
                        $currentInput = $item.closest('.property-input'),
                        itemCurrentValue = $.extend({}, itemData.currentValue),
                        itemIndex = itemData.collectionItemIndex,
                        $pEl = $currentInput.closest('.property-input-wrapper'),
                        propertyName = $pEl.data('modelPropertyName'),
                        propertyElementCurrentValue = $pEl.data('currentValue') ? $pEl.data('currentValue').slice() : [],
                        $addButton = $pEl.nearest('.add-list-item'),
                        isInsert = $item.hasClass('new-collection-item'),
                        indexProperty = $item.data('modelDescriptor').indexProperty;
                    console.log("[saveItem]", $item, " [value] =>", itemCurrentValue);
                    if (itemCurrentValue && indexProperty) {
                        itemCurrentValue[indexProperty] = itemIndex;
                    }
                    if (isInsert) {
                        $item.removeClass('new-collection-item');
                        propertyElementCurrentValue.push(itemCurrentValue);
                        $item.data('collectionItemIndex', itemIndex);
                        $collection.data('collectionSize', collectionSize + 1);
                    } else if (propertyElementCurrentValue[itemIndex]) {
                        propertyElementCurrentValue[itemIndex] = itemCurrentValue;
                    }
                    collectionItemLabel($pEl, $item, itemCurrentValue, itemData.modelName);
                    editMode($collection, $item, false);
                    MetaRenderer.setDisabled($addButton, false);
                    MetaRenderer.scrollTo($addButton, -100);
                    $currentInput.data('currentValue', propertyElementCurrentValue);
                    onInputChange(e, $currentInput, propertyName, propertyElementCurrentValue);
                },
                init = function (e, embeddedModelDescriptor, items) {
                    var $pEl = $(this),
                        itemNewObject = $pEl.data('itemNewObject') || {},
                        $input = $pEl.nearest('.property-input'),
                        propertyRenderer = $pEl.data('propertyRenderer'),
                        $label = $pEl.nearest('label.property-label'),
                        $collection = $('<ul class="property-value collection with-header collapsible popout"></ul>'),
                        $defaultTemplate = MetaRenderer.defaultModelViewTemplate(propertyRenderer.modelName, null, 'li'),
                        $template = MetaRenderer.findModelViewRendererTemplate($viewWrapper, propertyRenderer.modelName, null, $defaultTemplate),
                        $placeholders = $viewWrapper.find(evaluateRendererPlaceholderSelector(propertyRenderer.propertyName)),
                        $emptyCollection = $('<li class="collection-item empty-collection-wrapper tree-item"></li>'),
                        emptyMessageCode = evaluateExpressionForModelPropertyAndView(commonConstants.PROPERTY_EMPTY_VALUE, propertyRenderer.propertyHolderModelName, propertyRenderer.propertyName, propertyRenderer.viewName),
                        $collectionHeader = $('<li class="collection-header tree-item"><button class="btn-floating right secondary-content primary-bg add-list-item" type="button" name="add-list-item"><i class="large material-icons">add</i></button></li>');
                    $collection.append($collectionHeader);
                    $collection.appendTo($input);
                    $input.addClass('collection-builder model-form');
                    $input.attr("id", $input.attr("id") + "-__collection-item-index__");
                    var $insertTemplate = $template.clone(true, true);
                    prepareTemplate($pEl, $insertTemplate);
                    $collection.append($insertTemplate);
                    $insertTemplate.data('modelDescriptor', embeddedModelDescriptor);
                    MetaRenderer.renderModelInstance($insertTemplate, itemNewObject);
                    if (items) {
                        $.each(items, function (index, item) {
                            var $updateTemplate = $template.clone(true, true);
                            prepareTemplate($pEl, $updateTemplate, item);
                            $updateTemplate.attr('data-collection-item-index', index);
                            $updateTemplate.data('collectionItemIndex', index);
                            $collection.append($updateTemplate);
                        });
                    }
                    $collection.data('collectionSize', $collection.nearest('.collapsible-header:not(.header-template)').length);
                    var $addItemButton = $collectionHeader.find('button.add-list-item').first();
                    prepareButton($addItemButton, openAddItemForm, MetaRenderer.i18nMessage(RestMetaModel.COLLECTION_BUILDER_ADD_BUTTON_TOOLTIP, embeddedModelDescriptor.modelName));
                    $collectionHeader.prepend($label.addClass("left"));
                    $collection.sortable({
                                             items: "> li.collection-item:not(.renderer-template)",
                                             // start: function (e, ui) {
                                             //     var $item = ui.item;
                                             //     console.log($item.data(), $item);
                                             // },
                                             update: function (e, ui) {
                                                 var $item = ui.item,
                                                     $items = $item.parent().children('.collection-item:not(.renderer-template)');

                                                 $items.each(function (index, el) {
                                                     var $el = $(this);
                                                     $el.data('collectionItemIndex', index);
                                                     console.debug(index, $el, $el.data());
                                                     saveItem($el, e);
                                                 })

                                             }
                                         });
                    $collection.collapsible({
                                                accordion: true,
                                                onOpen: function ($item) {
                                                    var $itemHeader = $item.nearest('.collapsible-header'),
                                                        $collection = $item.closest('.collection'),
                                                        $propertyElement = $item.closest('.property-input-wrapper'),
                                                        propertyElementCurrentValue = $propertyElement.data('currentValue'),
                                                        $addButton = $propertyElement.nearest('.add-list-item');

                                                    if ($item.hasClass('new-collection-item')) {
                                                        $itemHeader.hide();
                                                    } else {
                                                        $collection.children('.new-collection-item').each(function () {
                                                            var $newCollectionItem = $(this),
                                                                $deleteButton = $newCollectionItem.find('> .collapsible-body .collection-item-actions-wrapper a.remove-item').first();
                                                            //Force delete on opened and not saved elements
                                                            $deleteButton.click();
                                                        });
                                                        $collection.sortable('disable');
                                                    }
                                                },
                                                onClose: function ($item) {
                                                    var $itemHeader = $item.nearest('.collapsible-header');
                                                    if (!$item.hasClass('new-collection-item')) {
                                                        $itemHeader.show();
                                                    }
                                                    $collection.sortable('enable');
                                                }
                                            });
                    if (items) {
                        MetaRenderer.renderModels(embeddedModelDescriptor.modelName, items, $pEl);
                    }
                    $pEl.data('currentValue', items);
                };
            $propertyElement.on('app:propertyModelDescriptorReady', init);
            $propertyElement.addClass('collection-builder-wrapper').removeClass("input-field");
            $propertyElement.on("app:propertyAppended", doStartOnWrapperReady);

        },
        selectProcessor: function ($propertyElement, propertyRenderer, value, $superContainer, multiple) {
            var $select = $propertyElement.nearest('select.property-input'),
                selectOptions = propertyRenderer.selectOptions,
                $placeholderOption = $('<option value="" disabled class="hint-option"></option>'),
                isEnum = propertyRenderer.defaultRendererType.name === 'ENUM',
                selected = false;
            $select.append($placeholderOption);
            $select.attr('multiple', !!multiple);
            $.each(selectOptions, function (index, selectOption) {
                var $opt = $('<option value=""></option>'),
                    currentValue = value && typeof  value === 'object' ? value.name : value,
                    optionValue = selectOption && typeof  selectOption === 'object' ? selectOption.name : selectOption;
                $opt.val(optionValue);
                if (isEnum) {
                    setXLabel($opt, commonConstants.PROPERTY_VALUE_ENUM(selectOption), propertyRenderer);
                } else {
                    setXLabel($opt, commonConstants.PROPERTY_VALUE_BOOLEAN(selectOption), propertyRenderer);
                }
                $opt.data({
                              currentValue: optionValue,
                              selectValue: selectOption
                          });
                if (currentValue === optionValue) {
                    selected = true;
                    $opt.attr("selected", true);
                }

                $select.append($opt);
            });
            setXLabel($placeholderOption, commonConstants.PROPERTY_PLACEHOLDER, propertyRenderer);
            if (!selected) {
                $placeholderOption.attr("selected", true);
            }
            $propertyElement.on("app:propertyAppended", function () {
                var $pel = $(this),
                    $select = $pel.nearest('select.property-input');
                MetaRenderer.i18nLabels($pel.find('[data-x-label][data-x-label-status!=true]'));
                $select.material_select();
            });
        },
        prepareForI18n: function ($target, code, propertyRenderer, attributeName) {
            setXLabel($target, code, propertyRenderer, attributeName);
        },
        prepareForRenderer: function ($target, model, viewName, viewType, indexViewType, rendererName) {
            $target.attr({
                             'data-model-uid': model.objectUID,
                             'data-model-name': model.modelName,
                             'data-meta-renderer': true
                         });
            if (viewType) {
                $target.attr({
                                 'data-model-view-type': viewType
                             });
            }
            if (viewName) {
                $target.attr({
                                 'data-model-view-name': viewName
                             });
            }
            if (indexViewType) {
                $target.attr({
                                 'data-model-index-view-type': indexViewType
                             });
            }
            if (rendererName) {
                $target.attr({
                                 'data-renderer-type': rendererName
                             });
            }

        },
        chartRenderer: function ($wrapper, type) {
            var chartSettings = $.extend({}, $wrapper.data('chartSettings')),
                $chartContainer = $('<div class="property-value"></div>'),
                chart;
            switch (type) {
                case 'pieChart':
                    chart = ChartFactory.pieChart($chartContainer, chartSettings.columns, chartSettings.tooltips, chartSettings.options);
            }
            setTimeout(function () {
                $wrapper.append($chartContainer).fadeIn();
            }, 1000);
            return chart;
        },
        findPropertiesWrapper: function ($context, modelName, modelUid) {
            var contextTagName = $context[0].tagName,
                $propertiesWrapper = contextTagName === 'TR' ? $context : $context.find('.properties-wrapper');
            return $propertiesWrapper.filter(function (index, element) {
                var $instance = $(this);
                return $instance.length === 1 && $instance.closest(evaluateModelViewSelector(modelName, modelUid)).length;
            });
        },
        addPresentationRendererOverride: function (modelName, rendererWrapper) {
            var renderersStoreForModel = _overrides[modelName];
            if (!renderersStoreForModel) {
                _overrides[modelName] = {viewRendererType: {}, formInputType: {}};
                renderersStoreForModel = _overrides[modelName];
            }
            renderersStoreForModel.viewRendererType[rendererWrapper.name] = rendererWrapper;
        },
        addInputRendererOverride: function (modelName, rendererWrapper) {
            var renderersStoreForModel = _overrides[modelName];
            if (!renderersStoreForModel) {
                _overrides[modelName] = {viewRendererType: {}, formInputType: {}};
                renderersStoreForModel = _overrides[modelName];
            }
            renderersStoreForModel.formInputType[rendererWrapper.name] = rendererWrapper;
        },
        getDefaultPresentationRenderer: function (rendererName) {
            var r = viewRendererType[rendererName];
            if (!r) {
                throw new AppException("Renderer [" + rendererName + "] is not defined!");
            }
            return r;
        },
        getDefaultInputRenderer: function (rendererName) {
            var r = formInputType[rendererName];
            if (!r) {
                throw new AppException("Renderer [" + rendererName + "] is not defined!");
            }
            return r;
        },
        getPresentationRenderer: function (propertyRenderer, typeCandidate) {
            var type = typeCandidate || propertyRenderer.rendererType.name,
                modelName = propertyRenderer.propertyHolderModelName,
                renderersStoreForModel = _overrides[modelName],
                r = renderersStoreForModel && renderersStoreForModel.viewRendererType[type] ? renderersStoreForModel.viewRendererType[type] : viewRendererType[type];
            if (!r) {
                throw new AppException("Renderer [" + type + "] is not defined!");
            }
            return r;
        },
        getInputRenderer: function (propertyRenderer, typeCandidate) {
            var type = typeCandidate || propertyRenderer.inputType.name,
                modelName = propertyRenderer.propertyHolderModelName,
                renderersStoreForModel = _overrides[modelName],
                r = renderersStoreForModel && renderersStoreForModel.formInputType[type] ? renderersStoreForModel.formInputType[type] : formInputType[type];
            if (!r) {
                throw new AppException("Renderer [" + type + "] is not defined!");
            }
            return r;
        },
        renderModelInstance: function ($element, model, modelStore, refRenderer, extraPropertiesClasses) {
            if (!$element || $element.length > 1) {
                throw new AppException("Invalid element!");
            }
            var elementData = $element.data(),
                modelName = elementData.modelName,
                elementViewName = elementData.modelViewName,
                elementViewType = elementData.modelViewType,
                elementIndexViewType = elementData.modelIndexViewType,
                modelDescriptor = modelStore ? modelStore.getRestMetaModel()
                    .getModelDescriptor(elementViewName) : elementData.modelDescriptor,
                $propertiesWrapper = MetaRenderer.findPropertiesWrapper($element, modelName, modelDescriptor.dto ? null : model.objectUID),
                rendererType = elementData.rendererType;
            $element.data('modelDescriptor', modelDescriptor);
            if (!refRenderer && rendererType) {
                refRenderer = true;
            }
            if (!elementData.isUiUpdated) {
                if ($propertiesWrapper.length > 0 || refRenderer && rendererType) {
                    if (refRenderer) {
                        referenceRenderer(modelName, $element, modelDescriptor, model, rendererType, elementViewType, elementIndexViewType);
                    } else {
                        propertiesWrapperRenderer(modelName, $element, $propertiesWrapper, modelDescriptor, model, elementViewType, elementIndexViewType, extraPropertiesClasses);
                        processItemActionsPerModelView(modelDescriptor, $element, model, elementViewType, elementIndexViewType);
                    }
                    updateUiElementAttributes($element, model, modelName, modelDescriptor);
                } else {
                    throw new AppException("Invalid template to update!");
                }
            }
        },
        getModelViewElement: function ($component, propertyRenderer, objectUID) {
            return $component.closest(evaluateModelViewSelector(propertyRenderer.propertyHolderModelName, objectUID));
        },
        renderModel: function (modelName, modelUidOrModel, $context, modelDescriptor) {
            var hasId = !(modelUidOrModel && typeof modelUidOrModel === "object"),
                modelUid = hasId ? modelUidOrModel : null,
                model = !hasId ? modelUidOrModel : null;
            if ($context && $context.length) {
                ModelStore.promiseOf(modelName, !modelDescriptor, modelDescriptor).done(function () {
                    var modelStore = this;
                    doRenderAllElementsFromOneModelStoreItem(modelName, modelUid, modelStore, $context, model);
                });
            }
        },
        renderModels: function (modelName, items, $context, viewName) {
            if ($context && $context.length) {
                $.each(items, function (index, item) {
                    ModelStore.promiseOf(modelName, true, viewName?{viewName:viewName}:null).done(function () {
                        var modelStore = this;
                        doRenderAllElementsFromOneModelStoreItem(modelName, item.objectUID, modelStore, $context, item, null, null, index);
                    });
                });
            }
        },
        renderModelReference: function ($elements) {
            console.debug('---> Render model references (elements to update:', $elements.length, ')');
            $elements.each(function (index, el) {
                var $element = $(el),
                    elementData = $element.data(),
                    modelName = elementData.modelName,
                    modelUid = elementData.modelUid,
                    model = elementData.currentValue;
                if (modelName) {
                    console.debug('------> [htmlIndex:', index, '] elementData: ', elementData, 'el: ', $element);
                    ModelStore.promiseOf(modelName, true).done(function () {
                        var modelStore = this;
                        if (model) {
                            MetaRenderer.renderModelInstance($element, model, modelStore, true);
                        } else {
                            modelStore.remoteGet(modelUid).done(function (model) {
                                if (model) {
                                    MetaRenderer.renderModelInstance($element, model, modelStore, true);
                                }
                            });
                        }

                    });
                }
            });
        },
        onMetaUpdated: function (event) {
            var $el = $(event.target);
            /* Model references */
            MetaRenderer.renderModelReference($el.find('[data-renderer-type][data-meta-renderer=true][data-is-ui-updated!=true]'));
            /* Services */
            MetaController.asMetaController($el.find('.meta-service[data-service-name][data-x-controller-status!=true]'));
            /* At the end because some labels are created dynamically */
            MetaRenderer.i18nLabels($el.find('[data-x-label][data-x-label-status!=true]'));
        },
        i18nCode: function (value, type) {
            switch (type) {
                case "enum":
                    return commonConstants.PROPERTY_VALUE_ENUM(value);
                case "boolean":
                    return commonConstants.PROPERTY_VALUE_BOOLEAN(value);
            }
            return value;
        },
        i18nMessage: function (code, modelDescriptor, propertyName) {
            if (_i18nCache[code] !== undefined) {
                return _i18nCache[code];
            }
            var modelName = (modelDescriptor && typeof modelDescriptor === 'object') ? modelDescriptor.modelName : (typeof modelDescriptor === 'string' ? modelDescriptor : null),
                fallbackToNoModel = (typeof modelDescriptor === 'object') ? modelDescriptor.fallbackToNoModel : false,
                superCode = modelName ? code.replace('{modelName}', modelName) : code;
            if (propertyName) {
                superCode = superCode.replace('{propertyName}', propertyName);
            }
            var message = app.g11n.lang.app.messages[superCode] || superCode;
            if (modelName && message === superCode && superCode.indexOf('model.' + modelName) === 0) {
                superCode = superCode.replace('model.' + modelName + (fallbackToNoModel ? '.' : ''), fallbackToNoModel ? "" : "model");
                message = app.g11n.lang.app.messages[superCode] || superCode;
            }
            /*todo i18n generic property same as for model*/

            if (message === superCode) {
                console.warn('[i18n] No translation found for', code === superCode ? [code] : [code, superCode]);
                _labelsToTranslate[superCode] = code;
            } else {
                console.debug('[i18n]', code, '=', message);
                _i18nCache[code] = message;
            }
            return message;
        },
        i18nLabels: function ($elements) {
            $elements.each(function (index) {
                var $element = $(this),
                    elementData = $element.data(),
                    code = elementData.xLabel,
                    targetAttribute = elementData.xLabelAttributeName,
                    modelName = elementData.xLabelModelName,
                    fallbackToNoModel = elementData.xLabelFallback,
                    translatedValue,
                    translated = elementData.xLabelStatus;
                if (!translated) {
                    if (modelName) {
                        translatedValue = MetaRenderer.i18nMessage(code, {
                            modelName: modelName,
                            fallbackToNoModel: fallbackToNoModel
                        });
                    } else {
                        translatedValue = MetaRenderer.i18nMessage(code);
                    }
                    if (targetAttribute) {
                        $element.attr(targetAttribute, translatedValue);
                    } else {
                        $element.html(translatedValue);
                    }
                    $element.attr({
                                      'data-x-label-status': true
                                  });
                    $element.trigger('app:i18n');
                }
            });
        },
        processIndexFragment: function ($context, modelDescriptor, page) {
            var event = null,
                beforeIndexCallback = window[modelDescriptor.modelName + 'OnBeforeIndex'],
                afterIndexCallback = window[modelDescriptor.modelName + 'OnAfterIndex'];
            if (typeof beforeIndexCallback == 'function') {
                beforeIndexCallback.call(window, event, $context, modelDescriptor, page);
            }
            processFixedActions(modelDescriptor, $context);
            MetaRenderer.renderPagination($context, modelDescriptor, page);
            $context.find('.grid table.grid-table').each(function () {
                var $table = $(this);
                gridTHeadUpdate($table, modelDescriptor, true);
            });
            MetaRenderer.processFilters($context, modelDescriptor, page);
            if (typeof afterIndexCallback == 'function') {
                afterIndexCallback.call(window, event, $context, modelDescriptor, page);
            }
            $context.show();
        },
        processFilters: function ($element, modelDescriptor, page) {
            var $filters = $element.nearest('.filters');
            console.log("Process filters:", $filters, modelDescriptor.indexOptions.filters);
            if ($filters.length === 1 && modelDescriptor.indexOptions.filters.length > 0) {
                $filters.empty();
                var $propertiesWrapper = $('<div class="properties-wrapper"></div>');
                $filters.append($propertiesWrapper);
                $filters.addClass('model-form filters-form-mock');
                $filters.attr('id', modelDescriptor.modelName + '-filters');
                $.each(modelDescriptor.indexOptions.filters, function (index, filter) {
                    var $filter = $('<div class="filter-property input-field property-input-wrapper"></div>'),
                        propertyName = filter.propertyName,
                        filterInitialValue = filter.value,
                        paramName = filter.paramName,
                        propertyRenderer = $.extend(true, {}, getPropertyRenderer(modelDescriptor, propertyName));
                    if (propertyRenderer) {
                        var filterModelWrapper = {};
                        filterModelWrapper[propertyName] = filterInitialValue;
                        propertyRenderer.inputFieldValidation = false;
                        renderPropertyInput(modelDescriptor, filterModelWrapper, $filter, propertyRenderer, commonConstants.VIEW_TYPE.INDEX_FRAGMENT, null, $filters, $element, filter);
                        $filter.data('propertyRenderer', propertyRenderer);
                    }
                    $filter.addClass('filter-property-' + propertyName);
                    $filter.attr('data-filter-param', paramName);
                    $filter.data('propertyFilter', filter);
                    $propertiesWrapper.append($filter);
                    $filter.triggerHandler("app:propertyAppended");
                });
            }
        },
        renderPagination: function ($context, modelDescriptor, page) {
            var elDta = $context.data(),
                viewType = elDta.modelViewType,
                $paginationWrapper = $context.nearest('.pagination-wrapper');
            if (modelDescriptor && modelDescriptor.indexOptions && viewType === 'INDEX_FRAGMENT' && $paginationWrapper.length) {
                var threshold = modelDescriptor.indexOptions.navigationLinksForMoreThen,
                    pageParam = modelDescriptor.serviceProperties.pageParamName,
                    path = "/" + modelDescriptor.basePath,
                    processHref = function ($pageLink, pageNumber, textAlso) {
                        $pageLink.attr("href", path + "?" + pageParam + "=" + pageNumber);
                        if (textAlso || textAlso === undefined) {
                            $pageLink.text(pageNumber);
                        }
                        return $pageLink;
                    };
                /* Data binding */
                $paginationWrapper.find('.current-page').text(page.number + 1);
                $paginationWrapper.find('.total-pages').text(page.totalPages);
                $paginationWrapper.find('.records-start').text(page.number * page.size + 1);
                $paginationWrapper.find('.total-records').text(page.totalElements);
                $paginationWrapper.find('.records-end').text(page.number * page.size + page.numberOfElements);
                processHref($paginationWrapper.find('.go-to-first-page a'), 1, false);
                processHref($paginationWrapper.find('.go-to-previous-page a'), page.number - 1, false);
                processHref($paginationWrapper.find('.go-to-next-page a'), page.number + 2, false);
                processHref($paginationWrapper.find('.go-to-last-page a'), page.totalPages, false);

                /* Individual pages sequence */
                var halfThreshold = 3,
                    firstPageNumberShown = page.number - halfThreshold < 0 ? 0 : page.number - halfThreshold,
                    lastPageNumberShown = page.number + halfThreshold > page.totalPages ? page.totalPages : page.number + halfThreshold,
                    $goToPage = $paginationWrapper.find('.go-to-page').first(),
                    $previous = $paginationWrapper.find('.go-to-previous-page');
                $paginationWrapper.find('.go-to-page').remove();
                for (var i = firstPageNumberShown; i < lastPageNumberShown; i++) {
                    var $p = $goToPage.clone(true, true);
                    $p.removeClass("active disabled no-select");
                    processHref($p.find('a'), i + 1);
                    if (i === page.number) {
                        $p.addClass("active disabled no-select");
                    }
                    $previous.after($p);
                    $previous = $p;
                }

                /* Show/hide business */
                if (modelDescriptor.indexOptions.paginationEnabled && page.totalPages !== 1) {
                    $paginationWrapper.show();
                    if (page.numberOfElements > 0) {
                        $paginationWrapper.find('.pagination-info').show();
                        $paginationWrapper.find('.no-records-found').hide();

                    } else {
                        $paginationWrapper.find('.pagination-info').hide();
                        $paginationWrapper.find('.no-records-found').show();
                    }
                    if (page.totalPages > 0) {
                        $paginationWrapper.find('ul.pagination').show();
                        if (page.totalPages > threshold && page.number > 0) {
                            $paginationWrapper.find('.go-to-first-page').show();
                            if (page.number !== 1) {
                                $paginationWrapper.find('.go-to-previous-page').show();
                            } else {
                                $paginationWrapper.find('.go-to-previous-page').hide();
                            }
                        } else {
                            $paginationWrapper.find('.go-to-first-page').hide();
                            $paginationWrapper.find('.go-to-previous-page').hide();
                        }
                        if (page.totalPages > threshold && page.number < page.totalPages - 1) {
                            $paginationWrapper.find('.go-to-last-page').show();
                            if (page.number + 1 !== page.totalPages - 1) {
                                $paginationWrapper.find('.go-to-next-page').show();
                            } else {
                                $paginationWrapper.find('.go-to-next-page').hide();
                            }
                        } else {
                            $paginationWrapper.find('.go-to-last-page').hide();
                            $paginationWrapper.find('.go-to-next-page').hide();
                        }
                    } else {
                        $paginationWrapper.find('ul.pagination').hide();
                    }

                    if (modelDescriptor.indexOptions.paginationInfoDisplayed) {
                        $paginationWrapper.find('.page-info-wrapper').show();
                    } else {
                        $paginationWrapper.find('.page-info-wrapper').hide();
                    }
                } else {
                    $paginationWrapper.hide();
                }

            }
        },
        defaultModelViewTemplate: function (modelName, modelUid, tagName) {
            var propertyElementTag = tagName || 'div',
                $defaultTemplate = $('<' + propertyElementTag + ' class="model-view renderer-template"></' + propertyElementTag + '>');
            $defaultTemplate.attr('data-model-name', modelName);
            $defaultTemplate.attr('data-meta-renderer', true);
            if (modelUid) {
                $defaultTemplate.attr('data-model-uid', modelUid);
            }
            $defaultTemplate.append('<div class="properties-wrapper"></div>');
            return $defaultTemplate;
        },
        defaultSimpleCollectionSimpleRendererType: function () {
            return 'OBJECT_LABEL';
        },
        findModelViewRendererTemplate: function ($context, modelName, modelUid, $default) {
            var $template = $context.find(evaluateModelViewSelector(modelName, modelUid));
            if ($template.length) {
                return $template.first();
            }
            return $default;
        },
        slickSliderRenderer: function ($wrapper, items, propertyRenderer, $targetParent, model) {
            var $carouselContainer = $('<div class="property-value carousel"></div>'),
                $defaultTemplate = MetaRenderer.defaultModelViewTemplate(propertyRenderer.modelName),
                settings = $.extend({
                                        slidesToShow: 1,
                                        slidesToScroll: 1
                                    }, $wrapper.data('carouselSettings')),
                $viewWrapper = $targetParent.closest(evaluateModelViewSelector(propertyRenderer.propertyHolderModelName, model.objectUID)),
                $template = MetaRenderer.findModelViewRendererTemplate($viewWrapper, propertyRenderer.modelName, null, $defaultTemplate),
                $placeholders = $viewWrapper.find(evaluateRendererPlaceholderSelector(propertyRenderer.propertyName));
            if (items) {
                $.each(items, function (index, collectionItem) {
                    var isRef = typeof collectionItem === 'number',
                        item = isRef ? {objectUID: collectionItem} : collectionItem,
                        $itemNavDescriptor = $template.clone(true, true).removeClass('renderer-template'),
                        $item = $('<div class="carousel-item"></div>');

                    $itemNavDescriptor.attr("data-model-uid", item.objectUID);
                    $item.html($itemNavDescriptor);
                    $itemNavDescriptor.on("app:uiUpdated", function (event) {
                        var $modelView = $(event.target),
                            $slide = $modelView.parent();
                        if ($slide.hasClass('slick-active')) {
                            MetaRenderer.transposeToPlaceholders($placeholders, $modelView);
                            console.log('on app:uiUpdated', $modelView);
                        }

                    });
                    $item.attr("data-model-uid", item.objectUID);
                    $carouselContainer.append($item);
                    ModelStore.promiseOf(propertyRenderer.modelName, true).done(function () {
                        var modelStore = this, transposedProperties = [], extraPropertiesClasses = {};
                        $placeholders.each(function () {
                            var transposedProperty = $(this).data('rendererPlaceholderProperty');
                            transposedProperties.push(transposedProperty);
                        });
                        $.each(transposedProperties, function (index, transposedProperty) {
                            extraPropertiesClasses[transposedProperty] = ['element-transposed'];
                        });
                        modelStore.remoteGet(item.objectUID, false).done(function (object) {
                            item = object;
                            items[index] = item;
                            $item.data({
                                           currentValue: item
                                       });
                            doRenderAllElementsFromOneModelStoreItem(propertyRenderer.modelName, item.objectUID, modelStore, $carouselContainer, item, extraPropertiesClasses);
                        });
                    });
                });
            }
            $carouselContainer.slick(settings);
            $carouselContainer.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
                var $currentSlideElement = $(slick.$slides[currentSlide]),
                    model = $currentSlideElement.data('currentValue');
                $wrapper.trigger('sliderBeforeChangeElement', [$currentSlideElement, model, slick, currentSlide, nextSlide]);
            });
            $carouselContainer.on('afterChange', function (event, slick, currentSlide) {
                var $currentSlideElement = $(slick.$slides[currentSlide]),
                    model = $currentSlideElement.data('currentValue'),
                    $currentSlideModelViewElement = $currentSlideElement.children('.model-view');
                MetaRenderer.transposeToPlaceholders($placeholders, $currentSlideModelViewElement, true);
                $wrapper.trigger('sliderAfterChangeElement', [$currentSlideElement, model, slick, currentSlide]);
            });
            $wrapper.append($carouselContainer);
            // ModelStore.promiseOf(propertyRenderer.modelName, true).done(function () {
            //     var modelStore = this, transposedProperties = [], extraPropertiesClasses = {};
            //     $placeholders.each(function () {
            //         var transposedProperty = $(this).data('rendererPlaceholderProperty');
            //         transposedProperties.push(transposedProperty);
            //     });
            //     $.each(transposedProperties, function (index, transposedProperty) {
            //         extraPropertiesClasses[transposedProperty] = ['element-transposed'];
            //     });
            //     if (items) {
            //         $.each(items, function (index, item) {
            //             doRenderAllElementsFromOneModelStoreItem(propertyRenderer.modelName, item.objectUID,
            // modelStore, $carouselContainer, item, extraPropertiesClasses); }); } });
            $wrapper.addClass('carousel-container');
        },
        simpleCollectionRenderer: function ($wrapper, items, propertyRenderer, $targetParent, model, renderAsReference) {
            var $collectionContainer = $('<ul class="property-value collection"></ul>'),
                $defaultTemplate = MetaRenderer.defaultModelViewTemplate(propertyRenderer.modelName, null, 'li'),
                $viewWrapper = $targetParent.closest(evaluateModelViewSelector(propertyRenderer.propertyHolderModelName, model.objectUID)),
                $template = MetaRenderer.findModelViewRendererTemplate($viewWrapper, propertyRenderer.modelName, null, $defaultTemplate),
                viewName=$template.data('modelViewName'),
                $placeholders = $viewWrapper.find(evaluateRendererPlaceholderSelector(propertyRenderer.propertyName)),
                $emptyCollection = $('<li class="collection-item empty-collection-wrapper"></li>'),
                emptyMessageCode = evaluateExpressionForModelPropertyAndView(commonConstants.PROPERTY_EMPTY_VALUE, propertyRenderer.propertyHolderModelName, propertyRenderer.propertyName, propertyRenderer.viewName),
                rendererType = $wrapper.data('rendererType') || MetaRenderer.defaultSimpleCollectionSimpleRendererType(),
                simpleRender = renderAsReference == undefined || renderAsReference;
            $template.find("[id]").each(function () {
                var $idEl = $(this), id = $idEl.attr("id");
                if (id.indexOf("__collection-item-index__") === -1) {
                    $idEl.attr("id", id + "-__collection-item-index__");
                }
            });
            $template.find("[data-activates]").each(function () {
                var $idEl = $(this), id = $idEl.attr("data-activates");
                if (id.indexOf("__collection-item-index__") === -1) {
                    $idEl.attr("data-activates", id + "-__collection-item-index__");
                }
            });
            if (items) {
                $.each(items, function (index) {
                    var item = $(this)[0],
                        $item = $template.clone(true, true).removeClass('renderer-template');
                    updateIds($item, '__collection-item-index__', index);
                    $item.attr("data-model-uid", item.objectUID);
                    $item.attr("data-collection-item-index", index);
                    $item.addClass('collection-item');
                    $item.data({
                                   currentValue: item,
                                   setObjectLabel:$wrapper.data('setObjectLabel')
                               });
                    if (simpleRender) {
                        $item.attr("data-renderer-type", rendererType);
                    }
                    var onUiUpdated=window[$item.data('app-handler')];
                    $item.on("app:uiUpdated", function (e) {
                        var $el=$(this);
                        if (typeof onUiUpdated === 'function') {
                            onUiUpdated.call($el, e, $el);
                        }
                    });
                    $collectionContainer.append($item);
                });
            }
            ModelStore.promiseOf(propertyRenderer.modelName, true, viewName?{viewName:viewName}:null).done(function () {
                var modelStore = this, transposedProperties = [], extraPropertiesClasses = {};
                $placeholders.each(function () {
                    var transposedProperty = $(this).data('rendererPlaceholderProperty');
                    transposedProperties.push(transposedProperty);
                });
                $.each(transposedProperties, function (index, transposedProperty) {
                    extraPropertiesClasses[transposedProperty] = ['element-transposed'];
                });
                if (items) {
                    $.each(items, function (index) {
                        var item = $(this)[0];
                        doRenderAllElementsFromOneModelStoreItem(propertyRenderer.modelName, item.objectUID, modelStore, $collectionContainer, item, extraPropertiesClasses, simpleRender, index);
                    });
                }
            });
            $emptyCollection.append('<div class="empty-collection"><div>' + MetaRenderer.i18nMessage(emptyMessageCode) + '</div></div>');
            $collectionContainer.prepend($emptyCollection);
            if (items.length === 0) {
                $emptyCollection.show();
            }
            else {
                $emptyCollection.hide();
            }
            $wrapper.append($collectionContainer);
            $wrapper.addClass('simple-collection-container');
        },
        transposeToPlaceholders: function ($placeholders, $fromContext, fadeIn) {
            if ($fromContext.length) {
                $placeholders.each(function () {
                    var $placeholder = $(this),
                        modelName = $fromContext.data('modelName'),
                        propertyName = $placeholder.data('rendererPlaceholderProperty'),
                        model = $fromContext.data('currentValue');
                    if (model.hasOwnProperty(propertyName)) {
                        var $propertyElement = findPropertyElementByArg($fromContext, modelName, propertyName),
                            doTranspose = function ($propertyElement, $placeholder) {
                                var $propertyElementClone = $propertyElement.clone(true, true)
                                        .removeClass('element-transposed'),
                                    onComplete = function () {
                                        $propertyElementClone.trigger("app:elementTransposed");
                                    };
                                $placeholder.data('currentValue', model[propertyName]);
                                if (fadeIn) {
                                    $placeholder.fadeOut('fast', 'swing', function () {
                                        $placeholder.html($propertyElementClone).fadeIn('fast', onComplete);
                                    });
                                } else {
                                    $placeholder.html($propertyElementClone);
                                    onComplete();
                                }
                                $propertyElement.addClass('element-transposed');
                            };
                        if ($propertyElement.hasClass('future-controller')) {
                            $propertyElement.on("app:controllerReady", function () {
                                var $controller = $(this);
                                doTranspose($controller, $placeholder);
                            });
                        } else {
                            doTranspose($propertyElement, $placeholder);
                        }
                    }
                });
            }
        },
        ratingViewUpdate: function ($ratingElement, newValue) {
            $ratingElement.find('.rating-star-element').each(function (index) {
                var $ratingStarEl = $(this);
                if (newValue < index + 0.5) {
                    $ratingStarEl.html('star_border');
                } else if (newValue < index + 1) {
                    $ratingStarEl.html('star_half');
                } else if (newValue >= index + 1) {
                    $ratingStarEl.html('star');
                }
                $ratingStarEl.data({ratingValue: index + 1});
            });
            $ratingElement.data('toCommitValue', newValue);
        },
        gridRenderer: function ($wrapper, propertyRenderer, $targetParent, model) {
            var $grid = $('<div class="grid"><table class="grid-table bordered bordered-all striped highlight responsive-table"><thead></thead><tbody></tbody></table></div>'),
                $table = $grid.find('table'),
                $tBody = $grid.find('tbody'),
                value = model[propertyRenderer.propertyName],
                isNull = !value,
                isArray = !isNull && $.isArray(value),
                items = isNull ? [] : (isArray ? value : value.content),
                length = isNull ? 0 : (value.totalElements !== undefined ? value.totalElements : items.length),
                evaluatedLengthMessage = evaluateLengthMessage(propertyRenderer, length),
                lengthMessage = MetaRenderer.i18nMessage(evaluatedLengthMessage, {
                    modelName: propertyRenderer.propertyHolderModelName, fallbackToNoModel: true
                }, propertyRenderer.propertyName);

            if (propertyRenderer.modelName) {
                $.each(items, function (index, item) {
                    var $tr = $('<tr class="table-row model-view"><td class="properties-wrapper-content"></td></tr>');
                    MetaRenderer.prepareForRenderer($tr, item, propertyRenderer.valueObjectTargetViewName, commonConstants.VIEW_TYPE.INDEX_FRAGMENT, commonConstants.INDEX_VIEW_TYPE.GRID);
                    $tBody.append($tr);
                });
                $wrapper.append($grid);
                ModelStore.promiseOf(propertyRenderer.modelName, true, {viewName: propertyRenderer.valueObjectTargetViewName})
                    .done(function () {
                        var store = this,
                            modelDescriptor = store.getRestMetaModel().getModelDescriptor();
                        gridTHeadUpdate($table, modelDescriptor);
                        $.each(items, function (index, item) {
                            if (item.id) {
                                store.set(item.id, item, true);
                            }
                        });
                    });
            }

        },
        updateView: function (modelName, modelUid, $context) {
            if (!$context) {
                $context = $('body');
            }
            ModelStore.promiseOf(modelName, true).done(function () {
                var modelStore = this, isModel = modelUid && typeof modelUid === "object",
                    uid = isModel ? modelUid['id'] : modelUid;
                $context.find(evaluateModelViewSelector(modelName, uid)).data('isUiUpdated', false);
                if (isModel) {
                    modelStore.set(uid, modelUid, true);
                } else {
                    modelStore.remoteGet(modelUid, true, true).done(function (model) {

                    });
                }
            });

        },
        formToObject: function ($form) {
            var event = null,
                modelName = $form.data('modelName'),
                formData = $form.closest('.form-wrapper').data('currentValue'),
                object = $.extend({}, formData),
                formToObjectCallback = window[modelName + 'FormToObject'];
            if (typeof formToObjectCallback === 'function') {
                formToObjectCallback.call(window, event, $form, object);
            }
            return object;
        },
        formData: function ($form) {
            var object = MetaRenderer.formToObject($form),
                $fileInput = $form.find(':file:enabled').first(),
                hasFileInputs = $fileInput.length > 0,
                formData = new FormData();

            if (!hasFileInputs) {
                $form.data('hasFormData', false);
                formData.append('object', new Blob([JSON.stringify(object)], {
                    type: "application/json"
                }));
                console.debug("[formData] Model ===> ", object);
                return formData;
            }
            var fileField = $fileInput.closest('.input-field[data-model-property-name]').data('modelPropertyName'),
                fileFieldFiles = object[fileField];
            object[fileField] = null;
            formData.append('object', new Blob([JSON.stringify(object)], {
                type: "application/json"
            }));
            if (fileFieldFiles && fileFieldFiles.length) {
                $.each(fileFieldFiles, function (index, file) {
                    formData.append('files', file);
                });
            }
            formData.append('fileField', fileField);
            $form.data('hasFormData', true);
            console.debug("[formData] Model ===> ", object);
            console.debug("[formData] Form data ===> ", formData);
            return formData;
        },
        inputContainerToObject: function ($inputContainer, object) {
            var modelName = $inputContainer.data('modelName');
            $inputContainer.nearest(evaluateExpressionForModel(commonConstants.PROPERTY_SELECTOR_FOR_INPUT, modelName))
                .each(function (index, item) {
                    var $item = $(item), propertyName = $item.data('modelPropertyName'),
                        propertyValue = $item.data('currentValue'),
                        dirty = $item.data('dirty');
                    console.debug('---->', propertyName, ' [New Value:', propertyValue, '] [Previous Value:', object[propertyName], '] ', dirty ? '[CHANGED]' : '[unchanged]');
                    object[propertyName] = propertyValue;
                });
            return object;
        },
        openModal: function (opts, html, modal$orSelector) {
            var $modal = modal$orSelector ? $(modal$orSelector) : $('#app-modal'),
                $html = $(html),
                isDynamic = $html.hasClass('.modal-ajax-content'),
                options = $.extend({
                                       modalFixedFooter: true,
                                       closeOnActionSuccess: true,
                                       actionsSelector: '.actions-wrapper .generic-action',
                                       modalCloseAction: true,
                                       modalCancelAction: false,
                                       modalOkAction: false,
                                       actionsRight: true
                                   }, opts);
            if ($html.length > 0) {
                $modal.find('.modal-content').html($html);
                $html.on('app:modalUpdated', function () {
                    var $modelView = $html.nearest('.model-view'),
                        actionsProcessor = function () {
                            if (options.actionsSelector) {
                                $modal.find('.modal-footer .other-actions')
                                    .html($html.find(options.actionsSelector).detach());
                            }
                        };
                    if ($modelView.length) {
                        $modelView.on('app:uiUpdated', function () {
                            actionsProcessor();
                        });
                    } else {
                        actionsProcessor();
                    }
                });
                if (!isDynamic) {
                    $html.trigger('app:modalUpdated');
                }
            }
            if (options.modalCloseAction) {
                $modal.find('.modal-footer .modal-close.close-action').show();
                $modal.find('.modal-footer .modal-close.cancel-action').hide();
            } else if (options.modalCancelAction) {
                $modal.find('.modal-footer .modal-close.cancel-action').show();
                $modal.find('.modal-footer .modal-close.close-action').hide();
            }
            if (options.modalOkAction) {
                var $ok = $modal.find('.modal-footer .modal-action.modal-ok');
                if ($ok.length) {
                    $ok.show();
                    if (options.onOk) {
                        $ok.off('click').click(options.onOk);
                    } else {
                        $ok.off('click');
                    }
                }
            } else {
                $modal.find('.modal-footer .modal-action.modal-ok').hide();
            }
            if (options.modalDialog) {
                $modal.addClass('modal-dialog');
            } else {
                $modal.removeClass('modal-dialog');
            }
            if (options.modalFixedFooter) {
                $modal.addClass('modal-fixed-footer');
            } else {
                $modal.removeClass('modal-fixed-footer');
            }
            if (options.actionsRight) {
                $modal.find('.modal-footer .other-actions').addClass('right');
            } else {
                $modal.find('.modal-footer .other-actions').removeClass('right');
            }
            if (options.closeOnActionSuccess) {
                var $mainActionElement = $modal.find('.main-action');
                $mainActionElement.data('appOn', EventsManager.ACTION_SUCCESS);
            }
            if (/*has no actions */ !options.modalCloseAction && !options.modalCancelAction && !options.actionsSelector && !options.modalOkAction) {
                $modal.find('.modal-footer').hide();
            } else {
                $modal.find('.modal-footer').show();
            }
            $modal.modal(options).modal('open');
            if (typeof app !== 'undefined') {
                app.hideLoader();
            }
            addLoader()
        },
        confirmationDialog: function (confirmationMessage, onConfirm, onClose) {
            var $html = $('<h1>' + MetaRenderer.i18nMessage('dialog.confirmation.header') + '</h1><p class="confirmation-message">' + confirmationMessage + '</p>');
            MetaRenderer.openModal({
                                       modalDialog: true,
                                       modalCloseAction: false,
                                       modalCancelAction: true,
                                       modalOkAction: true,
                                       complete: onClose,
                                       onOk: onConfirm
                                   }, $html);
        },
        devToDo: function () {
            var messages = [];
            for (var code in _labelsToTranslate) {
                if (_labelsToTranslate.hasOwnProperty(code)) {
                    messages.push(code + '=TRANSLATED_VALUE (originalKey:' + _labelsToTranslate[code] + ')');
                }
            }
            if (messages.length > 0) {
                console.debug("Hy, dev, you need to translate the following messages: ", "\n" + messages.join('\n'));
            }
        }
    };
}();