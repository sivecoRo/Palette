/**
 * User: AlexandruVi
 * Date: 2017-04-27
 */
/* Some overloading and overriding */
RegExp.quote = function (str) {
    return (str + '').replace(/[.?*+^$[\]\\(){}|-]/g, "\\$&");
};
String.prototype.ucFirst = function () {
    return this.charAt(0).toUpperCase() + this.slice(1);
};
jQuery.fn.extend({

                     serialize: function () {
                         return jQuery.param(this.serializeArray());
                     },
                     serializeArray: function () {
                         var rCRLF = /\r?\n/g,
                             rsubmitterTypes = /^(?:submit|button|image|reset|file)$/i,
                             rsubmittable = /^(?:input|select|textarea|keygen)/i,
                             rcheckableType = ( /^(?:checkbox|radio)$/i ),
                             arr;
                         arr = this.map(function () {

                             // Can add propHook for "elements" to filter or add form elements
                             var elements = jQuery.prop(this, "elements");
                             return elements ? jQuery.makeArray(elements) : this;
                         })
                             .filter(function () {
                                 var type = this.type;

                                 // Use .is( ":disabled" ) so that fieldset[disabled] works
                                 return this.name && !jQuery(this).is(":disabled") &&
                                     (rsubmittable.test(this.nodeName) || rcheckableType.test(type)) && !rsubmitterTypes.test(type);
                             });

                         return arr.map(function (i, elem) {
                             var val = jQuery(this).val();

                             return val === null ?
                                 {name: elem.name, value: null} :
                                 jQuery.isArray(val) ?
                                     jQuery.map(val, function (val) {
                                         return {name: elem.name, value: val.replace(rCRLF, "\r\n")};
                                     }) :
                                     {name: elem.name, value: val.replace(rCRLF, "\r\n")};
                         }).get();
                     },
                     nearest: function (filter, selfIncluded) {
                         var $found = $(),
                             $currentSet = selfIncluded ? this : this.children(); // starting point
                         while ($currentSet.length) {
                             $found = $currentSet.filter(filter);
                             if ($found.length) {
                                 break;
                             }  // At least one match: break loop
                             // Get all children of the current set
                             $currentSet = $currentSet.children();
                         }
                         return $found;
                     }
                 });

/**
 * Application Singleton
 * @constructor
 */
var App = (function () {
    var initialized = false,
        instance,
        stores = {},
        referrer = "";

    function getRestMethod($el) {
        return ($el.data('restMethod') || "GET").toUpperCase();
    }

    function defaultBeforeSend(jqXHR, $el) {
        var restMethod = getRestMethod($el);
        if (restMethod) {
            jqXHR.setRequestHeader(App.X_HTTP_METHOD_OVERRIDE_HEADER, restMethod);
        }
        referrer = window.location.href;
        return true;
    }

    function executeFunctionByName(functionName, context /*, args */) {
        var args = [].slice.call(arguments).splice(2);
        var namespaces = functionName.split(".");
        var func = namespaces.pop();
        for (var i = 0; i < namespaces.length; i++) {
            context = context[namespaces[i]];
        }
        var functionToExecute = context[func];
        if (typeof functionToExecute === "function") {
            return functionToExecute.apply(context, args);
        }
        if (functionName && functionName.indexOf('OnUiUpdated') === -1) {
            console.error("Function [", functionName, "] does not exist!");
        }
        return null;
    }

    function doRequest($restActionElement, action, data, $controller, beforeSend, firstSuccessHandler,
                       requestDataType, method, $successContainerP, $errorContainerP, opts) {
        if (!$restActionElement) {
            $restActionElement = $();
        }
        var modelName = $restActionElement.data('modelName'),
            restMethod = $restActionElement.data('restMethod'),
            restActionElementIsController = !$controller && $restActionElement.length && !$restActionElement.data("restIsNotController");
        if (restActionElementIsController) {
            $controller = $restActionElement;
        }
        if (!method) {
            method = getRestMethod($restActionElement);
        }
        method = method.toUpperCase();
        var request,
            ajaxTimeout = $controller ? $controller.data("restAjaxTimeout") :
                (method === 'POST' ? 300000 : 30000),
            controllerInitialHtml = $controller ? $controller.html() : '',
            controllerInitialWidth = $controller ? $controller.width() : 80,
            $successContainer = $successContainerP ? $successContainerP : $(),
            $errorContainer = $errorContainerP ? $errorContainerP : $(),
            pushState = !($controller && ($controller.data('xEmbedded') || $controller.data('xNoState') || $controller.data('openInModal'))),
            successWrapper = function (data, textStatus, jqXHR) {
                $successContainer.removeClass('hide');
                $successContainer.fadeIn();
                var dismissTimeout = $successContainer.data('dismissTimeout') || instance.preferences.webFeedback.defaultDismissTimeout,
                    allStateSectionsSelector = "body>section.main-section";
                if (typeof firstSuccessHandler === "function") {
                    firstSuccessHandler(data, textStatus, jqXHR);
                }
                if (method === "GET") {
                    if (History && pushState) {
                        var title = window.document.title,
                            absoluteAction = action.startsWith("http") ? action : window.location.origin + action,
                            sectionUri = uriOfSection(absoluteAction),
                            hashIndex = absoluteAction.indexOf("#"),
                            actionWithoutHash = hashIndex > 0 ? absoluteAction.substr(0, hashIndex) : absoluteAction,
                            hash = hashIndex > 0 ? absoluteAction.substr(hashIndex + 1) : null,
                            $mainSection;
                        console.log("[push state]", action);
                        $mainSection = $(allStateSectionsSelector + '[data-state-url="' + sectionUri + '"]');

                        if (false/*$mainSection.length*/) {
                            History.replaceState({
                                                     initial: false,
                                                     responseText: jqXHR.responseText,
                                                     responseJSON: jqXHR.responseJSON,
                                                     hash: hash
                                                 }, title, actionWithoutHash);
                        } else {
                            History.pushState({
                                                  initial: false,
                                                  responseText: jqXHR.responseText,
                                                  responseJSON: jqXHR.responseJSON,
                                                  hash: hash
                                              }, title, actionWithoutHash);
                        }
                    } else if ($controller && $controller.length && ($controller.data("xEmbedded") || $controller.data('xNoState') || $controller.data('openInModal'))) {
                        var controllerReEnableDelay = $controller.data("controllerReEnableDelay") || instance.preferences.webFeedback.controllerReEnableDelay,
                            onReEnable = function () {
                                var $controllerIcon = $controller.find(".material-icons").first();
                                if ($controllerIcon.length > 0) {
                                    $controllerIcon.html($controllerIcon.data("initialHtml"));
                                }
                                var uriOfHtml = uriOfSection(action),
                                    $html = beforeHtml($(data), uriOfHtml);
                                if ($controller.data('openInModal')) {
                                    $html = openInModal($html, $controller);
                                } else {
                                    var $responseTarget;
                                    if ($controller.data("xEmbedded")) {
                                        $responseTarget = $controller.find($controller.data("responseTargetSelector"));
                                        if (!$responseTarget.length) {
                                            $responseTarget = $($controller.data("responseTargetSelector"));
                                        }
                                    } else {
                                        $responseTarget = $($controller.data("responseTargetSelector"));
                                    }
                                    $responseTarget.html($html).focus();
                                    if ($controller.data("scrollToResponseTarget")) {
                                        scrollTo($responseTarget, $controller.data("scrollToResponseTargetTopOffsetCorrection"));
                                    }
                                }
                                htmlRequest(null, $html, uriOfHtml);
                            };
                        if (controllerReEnableDelay > 0) {
                            setTimeout(onReEnable, controllerReEnableDelay);
                        } else {
                            onReEnable();
                        }
                        $controller.blur();
                    }
                }
                if (dismissTimeout) {
                    setTimeout(function () {
                        $successContainer.fadeOut('slow');
                    }, dismissTimeout);
                }
                if (modelName) {
                    var modelData = restMethod === "DELETE" ? {id: action.substring(action.lastIndexOf("/") + 1)} : data;
                    EventsManager.getInstance().fire(EventsManager.MODEL_UPDATE, modelName, restMethod, modelData);
                    // setTimeout(function () {
                    //     if ($controller && $controller.length) {
                    //         var backTo = $controller.data("backTo"),
                    //             destination = backTo ? (window.location.origin + backTo) : window.document.referrer;
                    //         instance.go(destination, $controller, true);
                    //     }
                    // }, 300)
                }
            },
            updateControllerBeforeRequest = function () {
                if ($controller && $controller.length) {
                    $controller.data("restActive", true);
                    $controller.prop("disabled", true);
                    var customControllerWaitingHtml = $controller.data("waitingHtml");
                    if (customControllerWaitingHtml !== false) {
                        var waitingHtml = typeof customControllerWaitingHtml === "string" && customControllerWaitingHtml.startsWith("#") ? $(customControllerWaitingHtml) : instance.preferences.webFeedback.waitingHtml;
                        var $controllerIcon = $controller.find(".material-icons").first();
                        if ($controllerIcon.length > 0) {
                            $controllerIcon.data("initialHtml", $controllerIcon.html());
                            $controllerIcon.html(waitingHtml);
                        } else if ($controller[0].tagName === "A") {
                            if ($controller.hasClass("btn-floating")) {
                                $controller.html('<i class="material-icons">' + waitingHtml + '</i>');
                            } else {
                                $controller.html('<i class="material-icons">' + waitingHtml + '</i>' + ' ' + controllerInitialHtml);
                            }
                        }
                    }
                    $controller.addClass("pending-request");
                }
            },
            updateControllerAfterRequest = function (requestCompleted, responseData) {
                var controllerReEnableDelay = $controller.data("controllerReEnableDelay") || instance.preferences.webFeedback.controllerReEnableDelay,
                    onReEnable = function () {
                        if ($controller.data("restActive")) {
                            if ($controller.data("waitingHtml") !== false) {
                                var $controllerIcon = $controller.find(".material-icons").first();
                                if ($controllerIcon.length === 0) {
                                    $controllerIcon.html($controllerIcon.data("initialHtml"));
                                } else if ($controller[0].tagName === "A" || $controller[0].tagName === "BUTTON") {
                                    $controller.html(controllerInitialHtml);
                                }
                            }
                        }
                        $controller.prop("disabled", false);
                        $controller.data("restActive", false);
                        $controller.removeClass("pending-request");
                        $controller.blur();
                    };
                if (controllerReEnableDelay > 0) {
                    setTimeout(onReEnable, controllerReEnableDelay);
                } else {
                    onReEnable();
                }
            },
            onComplete = function (requestCompleted, responseData) {
                updateControllerAfterRequest(requestCompleted, responseData);
                // if (!restActionElementIsController) {
                //     $restActionElement.data("restActive", false);
                // }
            },
            beforeSendWrapper = function (jqXHR) {
                updateControllerBeforeRequest();
                if (!restActionElementIsController) {
                    $restActionElement.data("restActive", true);
                }
                $errorContainer.hide();
                var continueWithRequest = defaultBeforeSend(jqXHR, $restActionElement) && (typeof beforeSend === "function") ?
                    !!beforeSend(jqXHR) : true;
                if (!continueWithRequest) {
                    onComplete(continueWithRequest);
                }
                return continueWithRequest;

            }, settings = {
                beforeSend: beforeSendWrapper,
                error: function (jqXHR, textStatus, errorThrown) {
                    $errorContainer.removeClass('hide').show();
                    $successContainer.hide();
                },
                complete: function (jqXHR, textStatus) {
                    onComplete(true, jqXHR.responseJSON ? jqXHR.responseJSON : jqXHR.responseText);
                }
            };
        if (ajaxTimeout) {
            settings.timeout = ajaxTimeout;
        }
        if (opts) {
            if ($controller && $controller.data("skipp-redirect")) {
                settings.skippRedirect = true;
            }
            settings = $.extend(settings, opts);
        }
        if ($controller && $controller.length) {
            if ($controller.data("preserveHashOnReturn")) {
                $controller.data("hashOnReturn", window.location.hash);
            }
        }
        request = Request.send(action, data, successWrapper, requestDataType, method, settings);
        var restOnSuccess = $restActionElement.data('restOnSuccess');
        if (restOnSuccess) {
            request.done(function (data, textStatus, jqXHR) {
                executeFunctionByName(restOnSuccess, window, data, $restActionElement);
            });
        }
        return request;
    }

    function scrollTo($target, topOffsetCorrection) {
        var offsetCorrection = topOffsetCorrection ? topOffsetCorrection : ($target.data("scrollToResponseTargetTopOffsetCorrection") || 0);
        $('html,body').animate({
                                   scrollTop: $target.offset().top + offsetCorrection
                               });
    }

    function ramInputCollectionProcessor($inputWrapper, $collectionItem, collectionToUpdate) {
        var object = {}, modelName = $inputWrapper.data('targetModelName');
        $collectionItem.nearest('[data-model-name=' + modelName + ']').each(function (index, item) {
            var $item = $(item), propertyName = $item.data('modelPropertyName');
            object[propertyName] = ramInputValueExtractor($item);
        });
        collectionToUpdate.push(object);
    }

    function ramInputValueExtractor($inputWrapper) {
        var wrapperData = $inputWrapper.data(),
            $input = $inputWrapper.nearest('[name=' + wrapperData.modelPropertyName + ']');
        switch (wrapperData.propertyRenderer.rendererType.name) {
            case 'MODEL':
                return {id: parseInt($input.data('modelPropertyValue'))};
            case 'VALUE_OBJECT_MODEL':
                return ramInputContainerToObject($input);
            case 'MODEL_COLLECTION':
                return [];
            case 'VALUE_OBJECT_MODEL_COLLECTION':
                var collection = [];
                $input.nearest('.collection-item').each(function () {
                    ramInputCollectionProcessor($inputWrapper, $(this), collection);
                });
                return collection;
            case 'DOUBLE':
                return parseFloat($input.val());
            case 'ID':
                /* todo handler for String id */
                return parseInt($input.val());
            case 'INTEGER':
                return parseInt($input.val());
            case 'BOOLEAN':
                return $input.is(':checked');
            default:
                return $input.val();
        }
    }

    function formToObject($form) {
        var object = $form.data('currentValue') || {}, $f = $form, mapper = function (nameAndValue) {
            var $input = $f.find('[name=' + nameAndValue.name + ']'),
                data = $input.data(),
                wrapperData = $input.closest('[data-model-property-name=' + nameAndValue.name + ']').data();
            // console.log('[name=' + nameAndValue.name + ']', data, wrapperData);
            if (!wrapperData) {
                /* Is not a model property */
                return;
            }
            switch (wrapperData.modelPropertyRenderer) {
                case 'MODEL':
                    object[nameAndValue.name] = {id: parseInt($input.data('modelPropertyValue'))};
                    break;
                case 'DOUBLE':
                    object[nameAndValue.name] = parseFloat(nameAndValue.value);
                    break;
                case 'ID':
                    /* todo handler for String id */
                    object[nameAndValue.name] = parseInt(nameAndValue.value);
                    break;
                case 'INTEGER':
                    object[nameAndValue.name] = parseInt(nameAndValue.value);
                    break;
                case 'BOOLEAN':
                    object[nameAndValue.name] = $input.is(':checked');
                    break;
                default:
                    object[nameAndValue.name] = nameAndValue.value;
            }
        };
        /* backward compatibility to custom forms */
        if ($form.data('ramForm')) {
            ramInputContainerToObject($form, object);
        } else {
            $form.serializeArray().map(mapper);
        }
        return object;
    }

    function ramInputContainerToObject($inputsContainer, object) {
        var modelName = $inputsContainer.data('modelName');
        $inputsContainer.nearest('[data-model-name=' + modelName + ']').each(function (index, item) {
            var $item = $(item), propertyName = $item.data('modelPropertyName');
            object[propertyName] = ramInputValueExtractor($item);
        });
        return object;
    }

    function formToFormData($form) {
        return new FormData(document.querySelector("#" + $form.prop('id')));
    }

    function submitForm($form, action, data, requestDataType) {
        var formAction = action ? action : $form.prop("action"),
            isHtml5FormData = $form.find(':file:enabled').length > 0,
            formData = data ? data : isHtml5FormData ? formToFormData($form) : JSON.stringify(formToObject($form)),
            $submitButton = $form.find("button[type=submit]"),
            elementId = $form.attr('id'),
            $errorContainer = $form.closest("#" + elementId + "Error"),
            $successContainer = $form.closest("#" + elementId + "Success"),
            opts = {
                processData: false,
                contentType: 'application/json'
            },
            request = doRequest($form, formAction, formData, $submitButton, null, null, requestDataType, $form.attr('method'),
                                $successContainer, $errorContainer, opts);

        request.fail(function (jqXHR, textStatus, errorThrown) {
            var dataApiOptions = $form.data();
            if (dataApiOptions.deferredCheck && request.processedError && request.processedError.meta) {
                $.each(request.processedError.meta, function (fieldName) {
                    var $fields = $form.find('[name=' + fieldName + ']');
                    $fields.each(function () {
                        var $input = $(this);
                        $input.data('deferredCheckError', request.processedError.message);
                        $input.one("change", function () {
                            $input.data('deferredCheckError', null);
                        });

                    });
                    $form.validator('validate');
                });
            }
            return jqXHR;
        });
        return request;
    }

    function buttonAction($button, action, data, requestDataType) {
        var buttonAction = action ? action : $button.data("target"),
            params = data ? data : JSON.stringify($button.data('modelData')),
            method = getRestMethod($button), opts;
        if (method === 'POST' || method === 'PUT') {
            opts = {
                processData: false,
                contentType: 'application/json'
            };
        }
        return doRequest($button, buttonAction, params, $button, asFragmentRequestBeforeSendCallback($button, method, requestDataType), null, requestDataType, method, null, null, opts);
    }

    function asFragmentRequestBeforeSendCallback($restActionElement, method, requestDataType) {
        method = method ? method : getRestMethod($restActionElement);
        return function (jqXhr) {
            if (method === "GET") {
                var elementData = $restActionElement.data(), requestFragmentType = App.X_FRAGMENT_HEADER;
                if (elementData.xEmbedded) {
                    requestFragmentType = App.X_EMBEDDED_FRAGMENT_HEADER;
                }
                jqXhr.setRequestHeader(requestFragmentType, 'yes');
            }
            return true;
        };
    }

    function linkAction($link, action, data, requestDataType) {
        var linkAction = action ? action : $link.data("target") || $link.attr("href"),
            preserveHash = $link.data('preserveHash'),
            method = getRestMethod($link);
        if (preserveHash) {
            linkAction += window.location.hash;
        }
        return doRequest($link, linkAction, data, null, asFragmentRequestBeforeSendCallback($link, method, requestDataType), null, requestDataType, method);
    }

    function uriOfSection(url) {
        var x = url.indexOf("?");
        return url.substr(0, x <= 0 ? url.length : x);
    }

    function htmlRequest(state, $html, requestDiscriminator) {
        if (state && state.data && state.data.hash) {
            window.location.hash = state.data.hash;
        }
        EventsManager.getInstance().fire({
                                             name: EventsManager.HTML_REQUEST,
                                             selector: $($html || 'body')
                                         }, $html);
    }

    function htmlOfState($html) {
        if ($html.hasClass('wrapper-container')) {
            return $('<div class="container"></div>').append($html);
        }
        return $html;
    }

    function onStateChange(forceState) {
        var state = forceState || History.getState(), $mainSection, sectionUri = uriOfSection(state.url),
            allStateSectionsSelector = "body>section.main-section";
        console.debug("[state change]", state.url);

        $mainSection = $(allStateSectionsSelector + '[data-state-url="' + sectionUri + '"]');

        if (state.data.responseText) {
            console.debug("[state change] Update dom");
            if ($mainSection.length === 1) {
                console.debug("[state change] Section exists, replace html", sectionUri);
                $(allStateSectionsSelector).hide();
                $mainSection.attr('data-state-url', sectionUri);
                $mainSection.html(htmlOfState(beforeHtml($(state.data.responseText, sectionUri))));
                $mainSection.fadeIn('fast');
                htmlRequest(state, $mainSection, sectionUri);
            } else {
                console.debug("[state change] Section DOESN'T exists, create it");
                $(allStateSectionsSelector).hide();
                $mainSection = $('<section class="main-section hide"></section>')
                    .append(htmlOfState(beforeHtml($(state.data.responseText, sectionUri))));
                $('body').append($mainSection);
                $mainSection.attr('data-state-url', sectionUri);
                $mainSection.addClass(window.__updateDescriptor.sectionClass);
                $mainSection.removeClass('hide');
                htmlRequest(state, $mainSection, sectionUri);
            }
        } else if (state.data.initial && $mainSection.length === 1) {
            console.debug("[state change] Show initial section");
            $(allStateSectionsSelector).hide();
            $mainSection.fadeIn('fast');
            htmlRequest(state, $mainSection, sectionUri);
        } else if (window.location.href !== state.url) {
            console.debug("[state change] Go to", state.url);
            window.location = state.url;
        }
    }

    function onAnchorChange() {
        //todo TASK: Find workaround. STR: When press back button of browser and previous state is a hash, no state
        // change fires
        var state = History.extractState(location.href);
        if (state) {
            // onStateChange(state);
        }
    }

    function historyInit() {
        var History = window.History, allStateSectionsSelector = "body>section.main-section";

        if (History.enabled) {
            var state = History.getState();
            // set initial state to first page that was loaded
            History.pushState({initial: true}, document.title, state.url);
            $(allStateSectionsSelector).attr('data-state-url', uriOfSection(state.url));
            History.Adapter.bind(window, 'statechange', function () {
                MetaController.onStateChange(History.getState());
            });
            // History.Adapter.bind(window, 'anchorchange', function () {
            //     onAnchorChange();
            // });
        }
    }

    function ajaxInit() {
        EventsManager.getInstance()
            .on("submit", "form[action]:not(.no-ajax):not(.meta-form):not(.meta-service)", null, handleAjaxEvent);
        EventsManager.getInstance()
            .on("click", "a[data-target]:not(.no-ajax):not(.meta-controller):not(.meta-service),a[href]:not(.no-ajax):not(.meta-controller):not(.meta-service),button[data-target]:not(.no-ajax):not(.meta-controller):not(.meta-service)", null, handleAjaxEvent);
    }

    function handleAjaxEvent(event) {
        return ajaxAction(event);
    }

    function ajaxAction(event, action, data, requestDataType) {
        var $el = event.type === 'click' ? $(event.currentTarget) : $(event.target),
            tagName = $el[0].tagName,
            preventDefault = $el.data('xPreventDefault') === undefined ? true : !!$el.data('xPreventDefault'),
            stopPropagation = $el.data('xStopPropagation') === undefined ? true : !!$el.data('xStopPropagation');
        if ($el.hasClass('disabled') || $el.is(':disabled')) {
            return false;
        }
        if (!$el.data('restMethod') && !requestDataType) {
            requestDataType = "text";
        }
        console.debug("Do ajax action for ", $el);
        if (tagName === "FORM") {
            if (preventDefault) {
                event.preventDefault();
            }
            if (stopPropagation) {
                event.stopPropagation();
            }
            return submitForm($el, action, data, requestDataType || "json");
        } else if (tagName === "BUTTON") {
            if (preventDefault) {
                event.preventDefault();
            }
            if (stopPropagation) {
                event.stopPropagation();
            }
            return buttonAction($el, action, data, requestDataType || "json");
        } else if (tagName === "A") {
            if (!action) {
                action = $el.data("target") || $el.attr("href");
                if (action && action.indexOf("#") !== 0) {
                    if (preventDefault) {
                        event.preventDefault();
                    }
                    if (stopPropagation) {
                        event.stopPropagation();
                    }
                    return linkAction($el, action, data, requestDataType || "text");
                }
            }

        } else {
            console.warn("Unhandled tagName [" + tagName + "]");
        }
    }

    function sortInit() {
        EventsManager.getInstance().on({
                                           selector: "body",
                                           name: "click"
                                       }, ".sort-button", null, onSortClick);

        var $sortContainers = $('.sort-container');
        $sortContainers.each(function () {
            var $sortContainer = $(this), sortContainerData = $sortContainer.data(),
                modelName = sortContainerData.modelTarget,
                $sortButtons = $sortContainer.find('.sort-button[data-sort-model=' + modelName + ']');
            $sortButtons.each(function () {
                processSortButton($(this), $sortContainer);
            });
        });
    }

    function processSortButton($sortButton, $sortContainer, change) {
        var state = $sortButton.data('sortState'),
            propertyName = $sortButton.closest('[data-model-property-name]').data('modelPropertyName'),
            sortParams = $sortContainer.data('sortParams') || '',
            sortHandler = $sortContainer.data('sortHandler');

        if (change) {
            if (state === "off") {
                state = "asc";
            } else if (state === "asc") {
                state = "desc";
            } else if (state === "desc") {
                state = "off";
            }
            $sortButton.data('sortState', state);
            $sortButton.find('i.sort-icon').html(instance.g11n.lang.app.sort.state[state]);
            $sortButton.blur();
        }

        switch (state) {
            case "asc":
                sortParams = sortParams.replace(new RegExp('&sort=' + propertyName + ',(asc|desc)'), '') + '&sort=' + propertyName + ',asc';
                break;
            case "desc":
                sortParams = sortParams.replace(new RegExp('&sort=' + propertyName + ',(asc|desc)'), '') + '&sort=' + propertyName + ',desc';
                break;
            default:
                sortParams = sortParams.replace(new RegExp('&sort=' + propertyName + ',(asc|desc)'), '');
                break;
        }
        $sortContainer.data('sortParams', sortParams);
        if (change) {
            var modelName = $sortContainer.data('modelTarget');
            console.debug('[modelName = ', modelName, ']', sortParams);
            if (sortHandler) {
                executeFunctionByName(sortHandler, window, [modelName, sortParams]);
            } else {
                var currentHash = window.location.hash,
                    href = window.location.href.replace(currentHash, "").replace(/&sort=\w+,(asc|desc)/g, '');
                if (href.indexOf("?") === -1) {
                    sortParams = sortParams.replace("&", "?");
                }
                instance.go(href + sortParams + currentHash, $sortButton, false);
            }
        }
    }

    function onSortClick(event) {
        var $target = $(event.currentTarget);
        processSortButton($target, $target.closest('.sort-container'), true);
    }

    function applySettings(settings) {
        $.extend(true, instance.g11n, settings.g11n);
        var updateDescriptor = settings,
            modelName = updateDescriptor.modelName,
            remoteInit = !!updateDescriptor.remoteInit,
            modelDescriptor = updateDescriptor.modelDescriptor,
            triggerEvents = !!updateDescriptor.triggerEvents || updateDescriptor.triggerEvents === undefined,
            page = updateDescriptor.page;
        if (modelName) {
            ModelStore.promiseOf(modelName, remoteInit, modelDescriptor).done(function () {
                var store = this;
                if (page) {
                    store.updateStoreData(page, triggerEvents);
                }
            });
        }
    }

    function actionInit() {
        EventsManager.getInstance()
            .on("click", ".with-action:not(.no-ajax) .target-for-with-action", null, onActionClick);
    }

    function readController($target) {
        var href = $target.data('target'),
            rdt = $target.data('requestDataType') || "text",
            resolveFromCache = !!$target.data('resolveFromCache');
        event.stopPropagation();
        event.preventDefault();
        if ($target.data("xEmbedded")) {
            if ($target.data("requestOnActive") && !$target.hasClass("active")) {
                return;
            }
            instance.read(href, null, rdt, resolveFromCache, $target);
        } else {
            instance.read(href, null, rdt, resolveFromCache, $target);
        }
    }

    function onActionClick(event) {
        var $target = $(event.currentTarget);
        if ($target.hasClass('disabled') || $target.is(':disabled')) {
            return false;
        }
        if (!$target.data('target')) {
            $target = $target.closest(".with-action");
        }
        if ($target.data('target') === "parent") {
            $target = $target.parent();
        }
        readController($target);
    }

    function getModelViewComponent(modelName, id) {
        return $('[data-model-name="' + modelName + '"][data-model-id="' + id + '"]');
    }

    function getPaginationInfoComponent(modelName) {
        return $('[data-model-index="' + modelName + '"] .pagination-info');
    }

    function restEventsInit() {
        EventsManager.getInstance().on(EventsManager.MODEL_UPDATE, function (event, modelName, restMethod, data) {
            //console.debug(arguments);
            var modelStore = instance.getStore(modelName);
            if (modelStore instanceof ModelStore) {
                var doUpdateStore = function (object) {
                    switch (restMethod) {
                        case "POST":
                            modelStore.create(object);
                            break;
                        case "PUT":
                            modelStore.update(object);
                            break;
                        case "DELETE":
                            modelStore.remove(object.id);
                            break;
                        default:
                            break;
                    }
                };
                if ($.isArray(data)) {
                    $.each(data, function (index, item) {
                        doUpdateStore(item);
                    });
                } else {
                    doUpdateStore(data);
                }
            }

        });
        EventsManager.getInstance().on(EventsManager.VIEW_UPDATE, function (event, modelName, updateType, uid, object) {
            var $context = $(event.currentTarget);
            switch (updateType) {
                case "create":
                    console.log("Update view [eventType: create] [", modelName, "] uid:", uid);
                    EventsManager.getInstance()
                        .fire(EventsManager.VIEW_MODEL_UPDATE(modelName), modelName, updateType, uid, object);
                    break;
                case "update":
                    console.log("Update view [eventType: update] [", modelName, "] uid:", uid);
                    MetaRenderer.renderModel(modelName, uid, $context);
                    EventsManager.getInstance()
                        .fire(EventsManager.VIEW_MODEL_UPDATE(modelName), modelName, updateType, uid, object);
                    break;
                case "remove":
                    console.log("Update view [eventType: remove] [", modelName, "] uid:", uid);
                    getModelViewComponent(modelName, uid).each(function () {
                        var $component = $(this);
                        $component.fadeOut().remove();
                    });
                    getPaginationInfoComponent(modelName).each(function () {
                        var $component = $(this), $recordsEnd = $component.find('.records-end'),
                            $totalRecords = $component.find('.total-records'), recordsEnd, totalRecords;
                        recordsEnd = parseInt($recordsEnd.html());
                        totalRecords = parseInt($totalRecords.html());
                        $recordsEnd.html(recordsEnd - 1);
                        $totalRecords.html(totalRecords - 1);
                    });
                    EventsManager.getInstance()
                        .fire(EventsManager.VIEW_MODEL_UPDATE(modelName), modelName, updateType, uid);
                    break;
            }
        });
    }

    function openInModal($html, $controller) {
        var modalSelector = $controller.data('openInModalTarget') ? $controller.data('openInModalTarget') : "#app-modal",
            $modal = $(modalSelector);
        $modal.data("appPlugin", "modal");
        return initializeAppPlugin($modal, $html, $controller);
    }

    function resetAppPlugin($el, html, $controller) {
        var data = $el.data(), controllerData = $controller && $controller.length ? $controller.data() : {};
        if (true/*data.appPluginInitialized [commented because the intent is to call resetAppPlugin on an element with no .data but modified dom by the plugin initializer]*/) {
            var $selector = data.appSelector === "this" || !data.appSelector ? $el : data.appSelectorRelative ? $el.find(data.appSelector) : $(data.appSelector);
            console.debug("resetAppPlugin", data.appPlugin, "for element:", ($selector.selector || $selector.context));
            switch (data.appPlugin) {
                case "selector":
                    $selector.material_select('destroy');
                    break;
                default:
                    console.warn("No plugin found with name:", data.appPlugin);
                    break;
            }
            return $selector;
        }
        return $el;
    }

    function initializeAppPlugin($el, html, $controller) {
        var data = $el.data(), controllerData = $controller && $controller.length ? $controller.data() : {};
        if (data.appPlugin && (!data.appPluginInitialized || data.appPluginAlwaysInitialize)) {
            var $selector = data.appSelector === "this" || !data.appSelector ? $el : data.appSelectorRelative ? $el.find(data.appSelector) : $(data.appSelector);
            // console.debug("initializeAppPlugin", data.appPlugin, "for element:", ($selector.selector ||
            // $selector.context));
            switch (data.appPlugin) {
                case "modal":
                    var options = $.extend({
                                               modalFixedFooter: true,
                                               closeOnActionSuccess: true,
                                               actionsSelector: '.actions',
                                               modalCloseAction: true,
                                               modalCancelAction: false,
                                               actionsRight: true
                                           }, controllerData);
                    if (html) {
                        var $html = $(html), $h1 = $html.find('h1');
                        $h1.replaceWith('<h4>' + $h1.html() + '</h4>');
                        if (options.actionsSelector) {
                            $html.find(options.actionsSelector).find('button[type=submit]').each(function () {
                                var $submitButton = $(this), $form = $submitButton.closest('form');
                                if ($form.length) {
                                    $submitButton.click(function () {
                                        $form.trigger('submit');
                                    });
                                }
                            });
                            $selector.find('.modal-footer .other-actions')
                                .html($html.find(options.actionsSelector).detach());
                        }
                        if ($html.find('.activates-cancel').length > 0) {
                            options.modalCancelAction = true;
                        }
                        if ($html.find('.activates-close').length > 0) {
                            options.modalCloseAction = true;
                        }
                        $selector.find('.modal-content').html($html);
                    }
                    if (options.modalCloseAction) {
                        $selector.find('.modal-footer .modal-close.close-action').show();
                    } else if (options.modalCancelAction) {
                        $selector.find('.modal-footer .modal-close.cancel-action').show();
                    }
                    if (options.modalDialog) {
                        $selector.addClass('modal-dialog');
                    } else {
                        $selector.removeClass('modal-dialog');
                    }
                    if (options.modalFixedFooter) {
                        $selector.addClass('modal-fixed-footer');
                    } else {
                        $selector.removeClass('modal-fixed-footer');
                    }
                    if (options.actionsRight) {
                        $selector.find('.modal-footer .other-actions').addClass('right');
                    } else {
                        $selector.find('.modal-footer .other-actions').removeClass('right');
                    }
                    if (options.closeOnActionSuccess) {
                        var $mainActionElement = $selector.find('.main-action');
                        $mainActionElement.data('appOn', EventsManager.ACTION_SUCCESS);
                    }
                    if (/*has no actions */ !options.modalCloseAction && !options.modalCancelAction && !options.actionsSelector) {
                        $selector.find('.modal-footer').hide();
                    } else {
                        $selector.find('.modal-footer').show();
                    }
                    $selector.modal(options).modal('open');
                    $el.data('appPluginInitialized', true);
                    break;
                case "lov":
                    var $autocompleteInput = $selector.find("input.autocomplete"),
                        options = $.extend({
                                               modelName: $selector.data("targetModelName"),
                                               openOnEmptyInput: true,
                                               minLength: 0,
                                               limit: 20
                                           }, $autocompleteInput.data());
                    $autocompleteInput.autocomplete(options);
                    $el.data('appPluginInitialized', true);
                    break;
                case "rating":
                    var $ratingWrapper = $selector.parent();
                    if ($selector.val() !== undefined) {
                        var rate = $selector.val();
                        $ratingWrapper.find('input[name="over_rating"][value="' + rate + '"]').prop("checked", true);
                    }
                    break;
                case "timeago":
                case "timeAgo":
                    var timeagoOptions = $.extend(data, {strings: instance.g11n.lang.app.timeago});
                    $.each(data, function (prop, value) {
                        if (prop.indexOf('timeago') === 0) {
                            timeagoOptions.strings[prop.replace('timeago', '').toLowerCase()] = value;
                        }
                    });
                    $selector.timeago('init', timeagoOptions);
                    if (data.hasToolTip || data.hasToolTip === undefined) {
                        $selector.tooltip({tooltip: $selector.attr('title')});
                    }
                    break;
                case "sidenav":
                case "sideNav":
                    var sideNavOptions = $.extend({
                                                      menuWidth: 300,
                                                      edge: 'left',
                                                      closeOnClick: true,
                                                      draggable: true
                                                  }, data);
                    $selector.sideNav(sideNavOptions);
                    break;
                case "datepicker":
                case "datePicker":
                    break;
                case "avatar":
                    instance.viewFunction.avatar(null, $selector);
                    break;
                case "selector":
                    $selector.material_select();
                    break;
                case "collectionBuilder":
                    var $collection = $selector.find('ul.collection').first(),
                        collectionSize = $collection.nearest('.collection-item').length,
                        getPreparedTemplate = function ($template) {
                            //Destroy all plugins
                            $template.find('[data-app-plugin]').each(function () {
                                resetAppPlugin($(this));
                            });
                            //Rename all ids and id references of the template
                            $template.find('[id]').each(function () {
                                var $elementWithId = $(this), currentId = $elementWithId.attr('id'),
                                    newId = currentId.replace('{#}', collectionSize);
                                $elementWithId.attr('id', newId);
                                $template.find('label[for="' + currentId + '"]').each(function () {
                                    var $label = $(this);
                                    $label.attr('for', newId);
                                });
                            });
                            var $closeButton = $('<a href="#remove-item" class="secondary-content grey-text remove-item right no-ajax"><i class="material-icons">delete</i></a>');
                            $closeButton.click(removeItem).appendTo($template);
                            return $template;
                        },
                        addItem = function (e) {
                            e.preventDefault();
                            var $template = $collection.nearest('.collection-item-template').clone()
                                .toggleClass('collection-item-template collection-item');
                            getPreparedTemplate($template).appendTo($collection);
                            EventsManager.getInstance().fire({
                                                                 name: EventsManager.HTML_REQUEST,
                                                                 selector: $template
                                                             });
                        },
                        removeItem = function (e) {
                            e.preventDefault();
                            // $(e.currentTarget).closest('.collection-item-template, .collection-item').remove();
                            $(e.currentTarget).closest('.collection-item-template, .collection-item')
                                .toggleClass('removed-element').hide();
                        };
                    $collection.find('[name=add-list-item]').first().off('click').click(addItem);

                    break;
                default:
                    console.warn("No plugin found with name:", data.appPlugin);
                    break;

            }
            return $selector;
        }
        return $el;
    }

    function processBackButton() {
        var $target = $("[data-history-back]");
        if (referrer !== "") {
            if (!$target.hasClass('hide-on-modal') || $target.parents('.modal').length === 0) {
                $target.show();
            }
            $target.on('click', function (event) {
                event.preventDefault();
                History.back();
            });
        } else {
            $target.hide();
        }
    }

    function processDom(selector,event) {
        var $context = $(selector);
        $context.find('[data-app-plugin]').each(function () {
            initializeAppPlugin($(this));
        });
        $context.find('[data-app-plugin-destroy]').each(function () {
            resetAppPlugin($(this));
        });
        $context.find('[data-app-on]').each(function () {
            var $el = $(this), data = $el.data(),
                eventName = data.appOn;
            if (eventName) {
                $el.attr('data-app-on', null);
                $el.data('appOn', null);
                if (eventName === EventsManager.HTML_REQUEST) {
                    executeFunctionByName(data.appHandler, window, event, $el, data.funcArgs ? data.funcArgs.split(",") : []);
                } else if (eventName === "app:uiUpdated") {
                    if ($el.data('isUiUpdated')) {
                        executeFunctionByName(data.appHandler, window, null, $el);
                    } else {
                        $el.on("app:uiUpdated", function (e) {
                            executeFunctionByName(data.appHandler, window, e, $el);
                        });
                    }

                } else {
                    var ev = {name: eventName};
                    if (data.appOnSelf === "this") {
                        ev.selector = $el;
                    }
                    EventsManager.getInstance().on(ev, function (event) {
                        executeFunctionByName(data.appHandler, window, event, $el, data.funcArgs ? data.funcArgs.split(",") : []);
                    });
                }
            }
        });
        processBackButton();
        $context.find("[data-desintegrate=true]").remove();
    }

    function appEventsInit() {

        EventsManager.getInstance().on(EventsManager.HTML_REQUEST, function (event) {
            var $context = $(event.target);
            console.debug("[on] -> START", EventsManager.HTML_REQUEST, "context:", $context.context, "event:", event);
            processDom($context,event);
            console.debug("[on] <- END", EventsManager.HTML_REQUEST);
        });

        /* Move everything of $.on('ready')|$(document).ready(fn) to $(document).on('app:htmlRequest',fn) because this
         event is triggered also on ajax request that can produce dom changes */
        htmlRequest(null, $('body'));

    }

    function beforeHtml($html, requestDiscriminator) {
        EventsManager.getInstance().fire({
                                             name: EventsManager.BEFORE_HTML(requestDiscriminator),
                                             selector: $($html || window.document)
                                         }, $html);
        return $html;
    }

    function responsiveReady() {
        var responsive = function () {
            $(".x-truncate").each(function () {
                var $item = $(this), width = $item.width(), childrenWidth = 0, $children = $item.children();
                $children.each(function () {
                    childrenWidth += $(this).width();
                });
                var childrenOverlap = childrenWidth / width > 0.85;
                if (childrenOverlap) {
                    $item.find(".x-truncate-hide").addClass("hide");
                } else {
                    $item.find(".x-truncate-hide").removeClass("hide");
                }
                // console.log(width, childrenWidth, childrenOverlap ? $item : "[ok]");

            });
        };
        EventsManager.getInstance().on({
                                           selector: "body",
                                           name: "resize"
                                       }, responsive);//.on(EventsManager.HTML_REQUEST, responsive);
        responsive();
    }

    function init(settings) {
        if (!initialized) {
            window.app = instance;
            restEventsInit();
            applySettings(settings);
            historyInit();
            appEventsInit();
            ajaxInit();
            sortInit();
            actionInit();
            responsiveReady();
            Player.ready();
            initialized = true;
        }
    }

    function createInstance() {
        return {
            g11n: {
                lang: {
                    app: {
                        errors: {
                            generic: {
                                consumerFriendly: true,
                                code: 500,
                                title: "Error",
                                message: "Request failed.",
                                icon: "error",
                                iconClass: "red",
                                fatal: true
                            },
                            timeout: {
                                consumerFriendly: true,
                                code: 500,
                                title: "Request timeout",
                                message: "It takes to long ({{timeout}} seconds already elapsed).",
                                messageArguments: {
                                    timeout: 30000
                                },
                                option: {
                                    text: "Try again",
                                    href: "#"
                                },
                                icon: "warning",
                                iconClass: "yellow",
                                fatal: true
                            },
                            net: {
                                consumerFriendly: true,
                                code: 500,
                                title: "Network problem",
                                message: "Something went wrong.",
                                icon: "warning",
                                iconClass: "yellow",
                                fatal: true
                            },
                            notFound: {
                                consumerFriendly: true,
                                code: 404,
                                title: "Not Found",
                                message: "The request is not handled.",
                                icon: "warning",
                                iconClass: "yellow",
                                fatal: true
                            }
                        }
                    }
                }
            },
            preferences: {
                request: {
                    ajaxTimeout: 30000
                },
                webFeedback: {
                    defaultDismissTimeout: 15000,
                    controllerReEnableDelay: 400
                    //waitingHtml: '<div class="preloader-wrapper small active"> <div class="spinner-layer"> <div class="circle-clipper left"> <div class="circle"></div> </div><div class="gap-patch"> <div class="circle"></div> </div><div class="circle-clipper right"> <div class="circle"></div> </div> </div> </div>'
                }
            },
            viewFunction: {
                fadeIn: function (event, $target, args) {
                    $target.fadeIn.apply($target, args);
                },
                selfHide: function (event, $target, args) {
                    $target.hide();
                },
                fullPage: function (event, $target) {
                    var data = $target.data(), $el = $(data.target);
                    if ($el.hasClass('full-page')) {
                        $el.removeClass('full-page');
                    } else {
                        $el.addClass('full-page');
                    }
                },
                avatar: function (event, $target, user) {
                    user = user || $target.data('user');
                    // <span class="user-avatar" th:with="imageSrc=${user.avatarSrc},dataTarget=${dataTarget?:null}"
                    //     th:classappend="${dataTarget!=null?'with-action':''}"
                    // th:attr="data-target=${dataTarget}">
                    //     <span class="user-icon target-for-with-action"
                    // th:classappend="${imageSrc==null?'no-image':'with-image'}"
                    // th:attr="data-initials=${user.initials}"> <img th:if="${imageSrc}" src=""
                    // th:src="@{${imageSrc}}" alt="${user.getAlwaysNonEmptyDisplayName()}"/> </span> <span
                    // class="user-name target-for-with-action"
                    // th:utext="${user.getAlwaysNonEmptyDisplayName()}"></span> </span>

                    var $avatar = $('<span class="user-avatar"><span class="user-icon target-for-with-action"></span><span class="user-name target-for-with-action">' + user.alwaysNonEmptyDisplayName + '</span></span>');
                    if (user) {
                        if (user.avatarSrc) {
                            $avatar.find('.user-icon').addClass('with-image')
                                .append('<img src="' + user.avatarSrc + '">');
                        } else {
                            $avatar.find('.user-icon').addClass('no-image').attr('data-initials', user.initials);
                        }
                    }
                    $target.html($avatar);
                }
            },
            testButtonInForm: function (event, $target, args) {
                $target.data('modelData', formToObject($target.closest('form')));
            },
            plugin: function ($el, html, $controller) {
                return initializeAppPlugin($el, html, $controller);
            },
            pluginDestroy: function ($el, html, $controller) {
                return resetAppPlugin($el, html, $controller);
            },
            read: function (action, data, requestDataType, resolveFromCache, $controller, $responseContainer) {
                var requestFragmentType = App.X_FRAGMENT_HEADER, controllerData;
                console.debug("[read]", action);
                if ($controller && $controller.length) {
                    controllerData = $controller.data();
                    if (controllerData.xDownload) {
                        return this.downloadFile(action);
                    }
                    if (controllerData.xEmbedded) {
                        requestFragmentType = App.X_EMBEDDED_FRAGMENT_HEADER;
                    }
                    console.log("[controller data]", controllerData);
                }
                if (History && resolveFromCache && requestFragmentType === App.X_FRAGMENT_HEADER) {
                    var state = History.extractState(action);
                    if (state) {
                        //todo return promise
                        if (window.location.href !== state.url) {
                            return History.pushState(state.data, state.title, state.url);
                        }
                    }
                }
                return this.ajaxRequest($responseContainer, action, data, $controller, function (jqXhr) {
                    jqXhr.setRequestHeader(requestFragmentType, 'yes');
                    return true;
                }, function (data, textStatus, jqXHR) {
                }, requestDataType || "text", "GET", null, null, {
                                            // contentType: requestDataType || "json"
                                        });
            },
            getStore: function (modelName) {
                return ModelStore.promiseOf(modelName, true);
            },
            ajaxRequest: function ($restActionElement, action, data, $controller, beforeSend, firstSuccessHandler,
                                   requestDataType, method, $successContainerP, $errorContainerP, opts) {
                return doRequest($restActionElement, action, data, $controller, beforeSend, firstSuccessHandler,
                                 requestDataType, method, $successContainerP, $errorContainerP, opts);
            },
            updateModelStoreData: function (modelName, page, triggerEvents) {
                if (modelName) {
                    var modelStore = this.getStore(modelName);
                    if (modelStore && page) {
                        modelStore.updateStoreData(page, triggerEvents);
                    }
                }
            },
            defaultErrorHandler: function (error) {
                this.errorNotify(error);
            },
            notify: function (message, displayLength, className, completeCallback) {
                Materialize.toast(message, displayLength || 5000, className || "", completeCallback);
            },
            errorNotify: function (error) {
                var errorTitle = error.title, errorMessage = error.message, $body;
                console.error(error);
                $body = $('<div class="yellow-text"><span>' + errorMessage + '</span></div>');
                this.notify($body, 5000, "error");
                hideLoader();
            },
            htmlDecode: function(value)
            {
                var doc = null;
                if( value === null || value === undefined || value === '') {
                    return null;
                }
                doc = new DOMParser().parseFromString(value, "text/html");
                return doc.documentElement.textContent;
            },
           showLoader: function() {
                var $loader = $('#portrait-overlay');
                if ($loader) {
                    $loader.removeClass('portrait-overlay-hidden');
                    $loader.addClass('portrait-overlay-visible');
                }
            },
            hideLoader: function() {
                var $loader = $('#portrait-overlay');
                if ($loader) {
                    $loader.removeClass('portrait-overlay-visible');
                    $loader.addClass('portrait-overlay-hidden');
                }
            },
            go: function (destination, $controller, resolveFromCache) {
                console.log("Go to: ", destination);
                if (History) {
                    this.read(destination, null, "text", resolveFromCache, $controller);
                }
                else {
                    window.location = destination;
                }
            },
            downloadFile: function (href, onSuccess, onFailure) {
                $.fileDownload(href, {
                    successCallback: onSuccess,
                    failCallback: onFailure
                });
            },
            onTreeOpen: function (event) {
                readController($(event.target));
            },
            update: function (event, $element, args) {
                var inputUpdateDescriptor = null;
                if (args && args.length > 0) {
                    inputUpdateDescriptor = args[0];
                }
                console.debug('[app.update] $element.data()', $element.data());
                if (window.__updateDescriptor && !$element.data('isMetaUpdated')) {
                    console.debug('[app.update] window.__updateDescriptor', window.__updateDescriptor);

                    var updateDescriptor = inputUpdateDescriptor || window.__updateDescriptor,
                        modelName = updateDescriptor.modelName,
                        remoteInit = !!updateDescriptor.remoteInit,
                        modelDescriptor = updateDescriptor.modelDescriptor,
                        triggerEvents = !!updateDescriptor.triggerEvents || updateDescriptor.triggerEvents === undefined,
                        page = updateDescriptor.page,
                        actionId = updateDescriptor.actionId;
                    if (modelName) {
                        ModelStore.promiseOf(modelName, remoteInit, modelDescriptor).done(function () {
                            var store = this,
                                $modelView = $element.nearest('.model-view', true);
                            $modelView.data('action', modelDescriptor.actions[actionId]);
                            if (page) {
                                store.updateStoreData(page, triggerEvents);
                                if (updateDescriptor.viewType && updateDescriptor.viewType.$name === 'INDEX_FRAGMENT') {
                                    MetaRenderer.processIndexFragment($element, modelDescriptor, page);
                                }
                            } else if (updateDescriptor.model) {
                                MetaRenderer.renderModel(modelName, updateDescriptor.model, $element.parent());
                            }
                            $element.data('isMetaUpdated', true);
                            EventsManager.getInstance().fire(EventsManager.META_UPDATED, $element);
                        });
                    }
                }

                return this;
            }
        };
    }

    return {
        X_FRAGMENT_HEADER: "x-fragment",
        X_EMBEDDED_FRAGMENT_HEADER: "x-embedded-fragment",
        X_HTTP_METHOD_OVERRIDE_HEADER: "x-http-method-override",
        DEFAULT_VIEW_NAME: '__default',

        run: function (settings) {
            this.getInstance(settings);
        },
        /**
         *
         * @returns Object
         */
        getInstance: function (settings) {
            if (!instance) {
                instance = createInstance();
                init(settings || {});
            }
            return instance;
        }
    };
})();