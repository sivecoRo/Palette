/**
 * User: AlexandruVi
 * Date: 2017-04-27
 */
/**
 *
 * @param message
 * @param code
 * @constructor
 */
function AppException(message, code) {
    this.name = "Application Error";
    this.message = message;
    this.getCode = function () {
        var c = parseInt(code);
        if (isNaN(c)) {
            return 500;
        }
        return c;
    };
    this.toString = function () {
        return this.name + " [code=" + this.getCode() + "] [message=" + this.message + "]";
    };
}
