var SurveyPlayer = function () {
    var _this = this, $submitButton;

    function init(config) {
        _this.config = config;
        _this.reportManager = (config.reportManager || ReportManager).find(config.materialNodeId);
        $submitButton = $(".survey-submit");
        $submitButton.off("click").click(submitSurvey);
    }

    function submitSurvey() {
        var registerAnswer = function (x, item) {
            var $data = $(item).data();
            var $identifier = $data.identifier;
            var $value = $data.valueId;
            var $rawValue = $(item).val();
            var $surveyItem = $data.itemId;
            var $surveyId = $data.surveyId;
            if (!$value) {
                if (isNaN($rawValue)) {
                    _this.reportManager.registerItem($identifier, {
                        survey: {id: $surveyId},
                        textValue: $rawValue,
                        surveyItem: {id: $surveyItem}
                    });
                } else {
                    _this.reportManager.registerItem($identifier, {
                        survey: {id: $surveyId},
                        longValue: parseInt($rawValue, 10),
                        surveyItem: {id: $surveyItem}
                    });
                }
            } else {
                _this.reportManager.registerItem($identifier, {survey: {id: $surveyId}, surveyItemValue: {id: $value}});
            }

        };

        $("input:radio:checked, input:checkbox:checked, input[type=range],  textarea", ".survey").each(registerAnswer);

        _this.reportManager.persist(function () {
            app.notify("Your answers were saved. Please continue!");
        });
        return false;
    }

    return {
        init: init
    }
};

var TestPlayer = function () {
    var $progress, $questionSelector, $questionSelectorValues, $numericalProgress, $currentProgress,
        $nextButton, $previousButton, absoluteProgress = 0, $finishButton, $summaryCard, $summaryLoader, $summaryBody,
        $retakeButton, reportManager, _this = this;

    var questionStore = (function () {
        var currentQuestion = null, positions = [], map = {};

        return {
            init: function ($questions) {
                $questions.each(function (index, element) {
                    var $q = $(element);
                    var $data = $q.data();

                    var record = {
                        index: index + 1,
                        identifier: $data.identifier,
                        element: element
                    };
                    positions.push(record);
                    map[$data.identifier] = record;
                })
            },
            size: function () {
                return positions.length;
            },
            get: function (identifier) {
                return map[identifier];
            },

            itemOn: function (position) {
                if (position < 0 || position > positions.length) {
                    return null;
                } else {
                    return positions[position - 1];
                }
            },

            current: function (currentItem) {
                if (currentItem) {
                    currentQuestion = currentItem;
                }
                return currentQuestion;
            }
        }
    })();

    function init(config) {
        var $container = config.container;
        _this.config = config;
        _this.reportManager = (config.reportManager || ReportManager).find(config.materialNodeId);

        //init elements:
        var $questions = $(".test .question", $container);
        $progress = $("#test-progress .determinate", $container);
        $questionSelector = $("#question-selector", $container);
        $questionSelectorValues = $("#question-selector-values", $container);
        $numericalProgress = $("#numerical-progress", $container);
        $currentProgress = $("#current", $container);
        $nextButton = $("button.next-controller");
        $previousButton = $("button.previous-controller");
        $finishButton = $("button.test-submit-controller");
        $summaryCard = $(".card.summary");
        $summaryLoader = $(".loading", $summaryCard);
        $summaryBody = $(".card-content", $summaryCard);
        $retakeButton = $("button.test-retake", $summaryBody);

        //init progress:
        $("#total", $container).text($questions.length);
        $finishButton.off("click");
        $finishButton.click(function () {
            $(this).hide();
            $(".test-container", $container).hide();
            $summaryCard.fadeIn(300);
            _this.reportManager.persist(function () {
                showReport(config);
            });
            EventsManager.getInstance().off("click", "button.next-controller", null, nextButtonHandler);
            EventsManager.getInstance().off("click", "button.previous-controller", null, previousButtonHandler);
            $("#player-modal").trigger("ENABLE_PLAYER_NAVIGATION");
            return false;
        });

        $retakeButton.click(function () {
            $("[data-material-id='" + config.materialId + "']").data("completed", false).click();
        });

        //init questions:
        questionStore.init($questions);
        if (config.completed) {
            EventsManager.getInstance().off("click", "button.next-controller", null, nextButtonHandler);
            EventsManager.getInstance().off("click", "button.previous-controller", null, previousButtonHandler);
            $("#player-modal").trigger("ENABLE_PLAYER_NAVIGATION");
            $summaryCard.show();
            showReport(config);
        } else {
            initListeners($questions);
            EventsManager.getInstance().on("click", "button.next-controller", null, nextButtonHandler);
            EventsManager.getInstance().on("click", "button.previous-controller", null, previousButtonHandler);
            showQuestion(questionStore.itemOn(1).identifier);
        }
    }

    var nextButtonHandler = function () {
        var next = questionStore.itemOn(questionStore.current().index + 1);
        if (next) {
            showQuestion(next.identifier);
        }
    };

    var previousButtonHandler = function () {
        var previousIndex = questionStore.current() !== null ? questionStore.current().index - 1 : 0;
        if (previousIndex === 0) {
            $("#player-modal").trigger("ENABLE_PLAYER_NAVIGATION");
            $("button.previous-controller").click();
        } else {
            var previous = questionStore.itemOn(previousIndex);
            if (previous) {
                showQuestion(previous.identifier);
            }
        }
    };

    function showQuestion(identifier) {
        Timer.record(_this.config.materialNodeId + "_" + identifier);
        var current = questionStore.current();
        if (current) {
            $(current.element).hide();
            var timeSpentOnCurrent = player.stopTimeOnItem();
            console.log(current, timeSpentOnCurrent);
            _this.reportManager.updateItem(current.identifier, {
                timeSpent: Timer.get(_this.config.materialNodeId + "_" + current.identifier),
                endDate: new Date(),
                commit: true
            });
            _this.reportManager.persist();
        }
        questionStore.current(questionStore.get(identifier));
        $(questionStore.current().element).fadeIn(300);
        player.startTimeOnItem();
        progress(questionStore.current().index);

        if (questionStore.current().index >= 1 && questionStore.current().index < questionStore.size()) {
            $nextButton.show();
            $finishButton.hide();
        } else {
            $nextButton.hide();
            $finishButton.show();
        }
    }

    function progress(value) {
        if (value > absoluteProgress) {
            absoluteProgress = value;
        }
        $currentProgress.text(value);
        $progress.css("width", 100 * absoluteProgress / questionStore.size() + '%');
    }

    function initListeners(questions) {
        questions.each(function (index, question) {
            var $question = $(question);
            $("input", $question).change($question, onAnswerChange);
            $("textarea", $question).on("blur", $question, onAnswerChange);
            $("#player-modal").on("MATCHING_CHANGED", onAnswerChange);
            _this.reportManager.registerItem($question.data("identifier"), $question.data())
        });
        _this.reportManager.persist();
    }

    function onAnswerChange(event, data) {
        var $identifier = event.data ? $(event.data).data("identifier") : data.identifier;
        var answer = null;

        //single choice
        var singleChoice = "input:radio[name ='" + $identifier + "']";
        var multiChoice = "input:checkbox[name ='" + $identifier + "']";
        var textarea = "textarea[name ='" + $identifier + "']";
        var matching = "input:hidden[name ='" + $identifier + "']";

        if ($(singleChoice).length) {
            answer = answer || "";
            answer = $(singleChoice + ":checked").val();
        } else if ($(multiChoice).length) {
            answer = answer || "";
            $(multiChoice + ":checked").each(function (i, item) {
                answer += item.value + " ";
            });
            answer = answer.substring(0, answer.length - 1);
        } else if ($(textarea).length) {
            answer = answer || "";
            answer = $(textarea).val();
        } else if ($(matching).length) {
            answer = answer || "";
            $(matching).each(function (i, item) {
                if (item.value) {
                    answer = item.value.substring(0, item.value.length - 1);
                }
            });
        }
        _this.reportManager.answer($identifier, answer);
    }

    function showReport(config) {
        var url = "/play/report/{plannedCourseStudentId}/{materialId}"
            .replace("{plannedCourseStudentId}", config.plannedCourseStudentId)
            .replace("{materialId}", config.materialId);

        var requestOptions = {
                beforeSend: function (jqXHR) {
                    jqXHR.setRequestHeader("x-embedded-fragment", 'yes');
                    return true;
                }
            },
            onSuccess = function (response, textStatus) {
                $summaryLoader.hide();
                $summaryBody.append(response);
                $("[data-material-id='" + config.materialId + "']").data("completed", true);
                $("a", $summaryBody).click(function () {
                    $('#player-modal').trigger("SHOW_ACTIVITY", {identifier: $(this).data("target")});
                    return false;
                });
            };
        Request.send(url, {}, onSuccess, "text", "GET", requestOptions);
    }

    return {
        init: init
    }
};

var LessonPlayer = (function (window) {
    var scormApi = {
        dataRepositoryMock: {
            "cmi.objectives._count": 6,
            "cmi.objectives.0.id": "obj_module_0",
            "cmi.objectives.1.id": "obj_module_1",
            "cmi.objectives.2.id": "obj_module_2",
            "cmi.objectives.3.id": "obj_module_3",
            "cmi.objectives.4.id": "obj_module_4",
            "cmi.objectives.5.id": "obj_module_5"
        },

        errors: {},

        init: function () {
            window.API_1484_11 = {
                version: function () {
                    return "1";
                },

                Initialize: function () {
                    return true;
                },

                Terminate: function () {
                    return true;
                },

                GetValue: function (parameter) {
                    return scormApi.dataRepositoryMock[parameter] || " ";
                },

                SetValue: function (parameter, value) {
                    scormApi.dataRepositoryMock[parameter] = value;
                    return true;
                },

                Commit: function () {
                    return true;
                },
                GetLastError: function () {
                    return "0";
                },
                GetErrorString: function (errorCode) {
                    return errors[errorCode] || "Not yet implemented";
                },

                GetDiagnostic: function (paramter) {
                    return "0";
                }
            };
        }
    };

    scormApi.init();

    return {};
})(window);

function BaseReport(plannedCourseStudentId, materialId, materialNodeId) {
    var _this = this;

    _this.records = {};
    _this.recordsArray = [];
    _this.plannedCourseStudentId = plannedCourseStudentId;
    _this.materialId = materialId;
    _this.materialType = "resource";

    _this.fields = {
        identifier: null,
        plannedCourse: null,
        user: null,
        sessionId: null,
        rloNodeId: materialNodeId,
        lessonName: null,
        startDate: new Date(),
        endDate: null,
        score: 0.0,
        completed: false,
        minTimeSpent: "0:0:0" // cu formatul h:m:s - ar trebui sa fie long in baza
    };

    _this.answer = function (itemRef, answerData) {
        _this.records[itemRef].answer(answerData);
    };

    _this.registerItem = function (itemRef, data) {
        if (!_this.records[itemRef]) {
            _this.records[itemRef] = _this.getItemInstance(data);
            _this.recordsArray.push(_this.records[itemRef]);
        } else {
            _this.records[itemRef].fields = $.extend(_this.records[itemRef].fields, data);
        }
    };

    _this.formatMinTimeSpent = function () {
        var start = _this.fields.startDate, end = _this.fields.endDate;
        var secs = Math.floor((end.getTime() - start.getTime()) / 1000);
        var h = Math.floor(secs / 3600);
        var m = Math.floor((secs - h * 3600) / 60);
        var s = secs - h * 3600 - m * 60;
        return "" + (h < 10 ? "0" + h : h) + ":" + (m < 10 ? "0" + m : m) + ":" + (s < 10 ? "0" + s : s);
    };


    _this.persistData = function (reportData, callback) {
        Request.send('/play/report/{materialType}/{materialId}/{plannedCourseStudentId}/'
                .replace('{materialType}', _this.materialType || 'resource')
                .replace('{materialId}', _this.materialId || 1)
                .replace('{plannedCourseStudentId}', _this.plannedCourseStudentId || 1),
            JSON.stringify(reportData), callback, "json", "POST", {
                processData: false,
                contentType: 'application/json'
            }).done(function (data, textStatus, jqXHR) {
            callback && callback.call();
        }).fail(function (jqXHR, textStatus, errorThrown) {
            return false;
        });
    };

    _this.persist = function (callback) {
        if (!this.fields.startDate) {
            _this.fields.startDate = new Date();
        }
        if (!this.fields.endDate) {
            _this.fields.endDate = new Date();
        }
        _this.fields.minTimeSpent = _this.formatMinTimeSpent();
        _this.fields.completed = true;

        var reportData = $.extend({}, _this.fields);
        _this.persistData(reportData, callback);
    };

    _this.updateItem = function (itemRef, data) {
        if (_this.records[itemRef]) {
            _this.records[itemRef].update(data);
        }
    };

    _this.getItemInstance = function (data) {
        return new BaseItemReport(data);
    }
}


var BaseItemReport = function (data) {
    var _this = this;

    this.fields = $.extend({
        identifier: null,
        title: null,
        description: null,
        completed: false,
        startDate: new Date(),
        endDate: null,
        commit: false,
        learningActivityLOType: TYPES.INTERACTIVE,
        learningActivityType: TYPES.QTI,
        score: 0.0,
        timeSpent: 0
    }, data);

    _this.answer = function (answerData) {
        var data;
        if (typeof answerData === 'string') {
            data = {response: answerData, completed: true, commit: true}
        } else {
            if (answerData.response) {
                answerData.completed = true;
            }
            data = answerData;
        }
        _this.fields = $.extend(_this.fields, data);
    };

    _this.update = function (data) {
        _this.fields = $.extend(_this.fields, data);
    }
};

var AssessmentItemReport = function (data) {
    BaseItemReport.call(this, data);
    this.fields = $.extend({
        learningInteractionType: null,
        learningActivityLOType: TYPES.INTERACTIVE,
        learningActivityType: TYPES.QTI,
        weight: 1,
        response: null,
        result: null,
        xhtmlRepresentation: null, // ItemBody->presentation
        lessonActivityLink: null
    }, this.fields);
};

var ActivityItemReport = function (data) {
    BaseItemReport.call(this, {
        identifier: data.activityIdentifier,
        title: data.activityName,
        description: data.activityName,
        learningActivityLOType: TYPES.EXPOSITIVE,
        learningActivityType: TYPES.SCORM
    });
};

var TestReport = function (plannedCourseStudentId, materialId, materialNodeId) {
    BaseReport.call(this, plannedCourseStudentId, materialId, materialNodeId);
    var _this = this;
    _this.fields.assessmentItemReports = [];
    _this.materialType = "test";

    _this.getItemInstance = function (data) {
        return new AssessmentItemReport(data);
    };

    _this.persist = function (callback) {
        var reportData = $.extend({}, _this.fields);
        reportData.assessmentItemReports = [];
        $.each(_this.recordsArray, function (i, item) {
            reportData.assessmentItemReports.push($.extend({}, item.fields));
        });
        return _this.persistData(reportData, callback);
    }
};

var LessonReport = function (plannedCourse, rloNodeId, materialNodeId) {
    BaseReport.call(this, plannedCourse, rloNodeId, materialNodeId);
    var _this = this;
    _this.fields.activityItemReports = [];
    _this.materialType = "lesson";

    _this.getItemInstance = function (data) {
        return new ActivityItemReport(data);
    };

    _this.persist = function () {
        var reportData = $.extend({}, _this.fields);
        reportData.activityItemReports = [];
        $.each(_this.recordsArray, function (i, item) {
            reportData.activityItemReports.push($.extend({}, item.fields));
        });
        _this.persistData(reportData);
    }
};

var UserSurveyAnswer = function (data) {
    var _this = this;
    _this.fields = $.extend({
        survey: null,
        surveyItemValue: null,
        surveyItem: null,
        user: null,
        plannedCourse: null,
        booleanValue: null,
        textValue: null,
        longValue: null,
        submissionDate: null
    }, data);
};

var SurveyReport = function (plannedCourse, rloNodeId, materialNodeId) {
    BaseReport.call(this, plannedCourse, rloNodeId, materialNodeId);
    var _this = this;
    _this.materialType = "survey";
    _this.fields.userSurveyAnswers = [];
    _this.getItemInstance = function (data) {
        return new UserSurveyAnswer(data);
    };

    _this.persist = function (callback) {
        var reportData = $.extend({}, _this.fields);
        reportData.userSurveyAnswers = [];
        var date = new Date();
        $.each(_this.recordsArray, function (i, item) {
            item.fields.submissionDate = date;
            item.fields.completed = true;
            reportData.userSurveyAnswers.push($.extend({}, item.fields));
        });
        reportData.completed = true;
        _this.persistData(reportData, callback);
    };
};

var TYPES = {
    QTI: 0,
    SCORM: 1,
    TINCAN_ASSESMENT: 2,
    TINCAN_ACTIVITY: 3,

    EXPOSITIVE: 0,
    INTERACTIVE: 1,

    CHOICE_INTERACTION: 0,
    CHOICE_MULTIPLE_INTERACTION: 1,
    ORDER_INTERACTION: 2,
    ASSOCIATE_INTERACTION: 3,
    MATCH_INTERACTION: 4,
    GAP_MATCH_INTERACTION: 5,
    INLINE_INTERACTION: 6,
    TEXT_ENTRY_INTERACTION: 7,
    HOTTEXT_INTERACTION: 8,
    HOTSPOT_INTERACTION: 9,
    SELECT_POINT_INTERACTION: 10,
    GRAPHIC_ORDER_INTERACTION: 11,
    GRAPHIC_ASSOCIATE_INTERACTION: 12,
    GRAPHIC_GAP_MATCH_INTERACTION: 13,
    POSITION_OBJECT_INTERACTION: 14,
    SLIDER_INTERACTION: 15,
    UPLOAD_INTERACTION: 16,
    EXTENDED_TEXT_INTERACTION: 17
};

var Timer = (function () {
    var timeSpentOnCurrentRlo = 0, timeSpent = 0, timeSpentJob, intervalDelay = 1000, lastItemTime = 0, lastItem;

    var timerMap = {};

    function stopCounter() {
        lastItemTime = timeSpentOnCurrentRlo;
        if (timeSpentJob !== undefined) {
            clearInterval(timeSpentJob);
        }
        timeSpentOnCurrentRlo = 0;
        return lastItemTime;
    }

    function startCounter() {
        var doHandle = function () {
            timeSpent += 1;
            timeSpentOnCurrentRlo += 1;
        };
        stopCounter();
        timeSpentJob = setInterval(doHandle, intervalDelay);
    }

    function recordTime(data) {
        if (data && data.itemRef) {
            stopCounter();
            if (lastItem) {
                timerMap[lastItem] += lastItemTime;
            }
            if (!timerMap[data.itemRef]) {
                timerMap[data.itemRef] = 0;
            }
            lastItem = data.itemRef;
            startCounter();
        }
    }

    return {
        start: startCounter,
        stop: stopCounter,
        get: function (itemRef) {
            return (timerMap[itemRef] || 0) * 1000;
        },
        record: function (identifier) {
            recordTime(identifier);
        }
    }
})();

var ReportManager = (function (config) {

    var reportStore = {};

    var init = function (config) {
        reportStore = {};
    };

    var createReporter = function (config) {
        var reporter = null;
        switch (config.type) {
            case "TEST":
                if (!reportStore[config.materialNodeId]) {
                    reportStore[config.materialNodeId] = new TestReport(config.plannedCourseStudentId, config.materialId, config.materialNodeId);
                }
                reporter = reportStore[config.materialNodeId];
                break;
            case "LESSON":
                if (!reportStore[config.materialNodeId]) {
                    reportStore[config.materialNodeId] = new LessonReport(config.plannedCourseStudentId, config.materialId, config.materialNodeId);
                }
                reporter = reportStore[config.materialNodeId];
                break;
            case "SURVEY":
                if (!reportStore[config.materialNodeId]) {
                    reportStore[config.materialNodeId] = new SurveyReport(config.plannedCourseStudentId, config.materialId, config.materialNodeId);
                }
                reporter = reportStore[config.materialNodeId];
                break;
            default:
                if (!reportStore[config.materialNodeId]) {
                    reportStore[config.materialNodeId] = new BaseReport(config.plannedCourseStudentId, config.materialId, config.materialNodeId);
                }
                reporter = reportStore[config.materialNodeId];
        }

        return reporter;
    };

    return {
        init: init,
        register: createReporter,
        send: function (rloIdentifier) {
            //do some magic here
        },
        find: function (materialRef) {
            return reportStore[materialRef];
        }
    }

})();


var playerDefaults = {
    navigation: {show: false},
    controls: {show: false},
    reportManager: ReportManager
};

var Player = (function (playerDefaults) {
    var instance, timeSpentJob, intervalDelay = 1000/*1 sec*/,
        playerSideMenuControllerSelector = '.player-side-menu-controller';

    function navigation() {
        var currentItem = {index: null, item: null};
        var store = [];

        var goTo = function (item, backwards) {
            if (currentItem) {
                toggleActive(currentItem.item, false);
                toggleActive(item, true);
            }

            currentItem.index = store.indexOf(item);
            currentItem.item = item;

            if (store.indexOf(item) === 0) {
                $(".previous-controller").hide();
            } else {
                $(".previous-controller").show();
            }

            if (store.indexOf(item) === store.length - 1) {
                $(".next-controller").hide();
                $(".course-finish-controller").show();
            } else {
                $(".next-controller").show();
                $(".course-finish-controller").hide();
            }

            openItem($(item), backwards);
        };
        return {
            init: function () {
                var currentReportManager = null;
                $(".rlo, .rlo .activity").each(function (position, item) {
                    store[position] = item;
                    var $data = $(item).data();
                    var $type = $data.materialType;
                    if ($type) {
                        currentReportManager = instance.reportManager.register({
                            type: $type,
                            plannedCourseStudentId: instance.playerSession.learnerCourseAssociation.id,
                            materialNodeId: $data.materialNodeId,
                            materialId: $data.materialId
                        });
                    } else {
                        if (!$data.activityIsgroupactivity && currentReportManager) {
                            currentReportManager.registerItem($data.activityCrnodeid, $data)
                        }
                    }
                });
            },
            next: function () {
                console.log("go next");
                if (currentItem.index === null) {
                    var initialIndex = 0, item = null;
                    if (instance.playerSession.currentItemNodeId) {
                        item = $("[data-material-node-id='" + instance.playerSession.currentItemNodeId + "']");
                        if (item && item.length === 0) {
                            item = $("[data-activity-identifier='" + instance.playerSession.currentItemNodeId + "']");
                        }
                        if (item && item.length) {
                            initialIndex = store.indexOf(item[0]);
                        }
                    }
                    goTo(store[initialIndex < 0 ? 0 : initialIndex]);
                } else if (currentItem.index < store.length) {
                    goTo(store[currentItem.index + 1]);
                }
            },
            previous: function () {
                if (currentItem.index > 0) {
                    goTo(store[currentItem.index - 1], true);
                }
            },
            to: goTo,

            store: store,

            current: currentItem
        }
    }

    function toggleActive(element, active) {
        if (active) {
            $(element).addClass("selected");
        } else {
            $(element).removeClass("selected");
        }

    }

    function disableNavigationEvents() {
        EventsManager.getInstance()
            .off("click", "button.next-controller", null, nextButtonHandler)
            .off("click", "button.previous-controller", null, previousButtonHandler);
        $("#player-modal").on("ENABLE_PLAYER_NAVIGATION", enableNavigationEvents);
    }

    function enableNavigationEvents() {
        EventsManager.getInstance()
            .off("click", "button.next-controller", null, nextButtonHandler)
            .on("click", "button.next-controller", null, nextButtonHandler)
            .off("click", "button.previous-controller", null, previousButtonHandler)
            .on("click", "button.previous-controller", null, previousButtonHandler);
        // $("#player-modal").off("ENABLE_PLAYER_NAVIGATION", enableNavigationEvents);
        if (instance.navigation.store.indexOf(instance.navigation.current.item) === 0) {
            $(".previous-controller").hide();
        } else {
            $(".previous-controller").show();
        }

        if (instance.navigation.store.indexOf(instance.navigation.current.item) === instance.navigation.store.length - 1) {
            $(".next-controller").hide();
            $(".course-finish-controller").show();
        } else {
            $(".next-controller").show();
            $(".course-finish-controller").hide();
        }
    }

    function stopCounter() {
        var t = instance.timeSpentOnCurrentRlo;
        if (timeSpentJob !== undefined) {
            clearInterval(timeSpentJob);
        }
        instance.timeSpentOnCurrentRlo = 0;
        return t;
    }

    function startCounter() {
        var doHandle = function () {
            instance.timeSpent += 1;
            instance.timeSpentOnCurrentRlo += 1;
        };
        stopCounter();
        timeSpentJob = setInterval(doHandle, intervalDelay);
    }

    function updateReport(parentItemRef, childItemRef, callback) {
        Timer.record(childItemRef || parentItemRef);
        if (instance.current && instance.current.parentRef) {
            var itemReport = instance.reportManager.find(instance.current.parentRef);
            itemReport.updateItem(instance.current.itemRef || instance.current.parentRef, {
                timeSpent: Timer.get(instance.current.itemRef || instance.current.parentRef),
                endDate: new Date(),
                completed: !!(instance.current.itemRef && instance.current.parentRef),
                commit: true
            });
            itemReport.persist(callback);
        }
        instance.current = {
            itemRef: childItemRef,
            parentRef: parentItemRef
        };
    }

    function openItem(itemRef, backwards) {
        var $controller = $(itemRef || this);
        if ($controller.hasClass("activity")) {
            if ($controller.data("activityIsgroupactivity") === true) {
                if (!backwards) {
                    instance.navigation.next();
                } else {
                    instance.navigation.previous();
                }

            } else {
                if ($("iframe").length === 0) {
                    instance.contentArea.prepend("<iframe class='item'\>");
                }
                $("iframe", instance.contentArea)[0].src = "/cms/stream/" + $controller.data().activityCrnodeid + "/";
                updateReport($controller.data().activityMaterialNodeId, $controller.data().activityCrnodeid);
            }
            $controller.addClass("active");
            $(".collapsible-header", $controller).first().addClass("active");

        } else if ($controller.hasClass("rlo")) {
            if ($controller.data("materialType") === "LESSON") {
                if (!backwards) {
                    instance.navigation.next();
                } else {
                    instance.navigation.previous();
                }
            } else {
                var requestOptions = {
                        beforeSend: function (jqXHR) {
                            jqXHR.setRequestHeader("x-embedded-fragment", 'yes');
                            return true;
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log("error");
                        }
                    },
                    onSuccess = function (response, textStatus) {
                        instance.contentArea.empty();
                        instance.contentArea.append($(response));
                        $controller.addClass("active");
                        $(".collapsible-header", $controller).first().addClass("active");
                        $(".collapsible-body", $controller).show();
                        if ($controller.data("materialType") === "TEST") {
                            $controller.data("plannedCourseStudentId", instance.playerSession.learnerCourseAssociation.id);
                            disableNavigationEvents();
                            TestPlayer().init($.extend({
                                container: instance.contentArea,
                                reportManager: instance.reportManager
                            }, $controller.data()));
                            // instance.contentArea.prepend("<iframe src='/play/report/" + reportURl +"'/>");
                        } else if ($controller.data("materialType") === "SURVEY") {
                            SurveyPlayer().init($.extend({
                                container: instance.contentArea,
                                reportManager: instance.reportManager
                            }, $controller.data()));
                        }
                        updateReport($controller.data().materialNodeId);
                    };
                Request.send($controller.data().target, {}, onSuccess, "text", "GET", requestOptions);
            }
        }
    }

    function prepareSideMenuController($sideMenuController) {
        $sideMenuController.data({
            onOpen: function (el) {
                $sideMenuController.addClass('side-menu-open');
            },
            onClose: function (el) {
                $sideMenuController.removeClass('side-menu-open');
            }
        });
    }

    function openSideMenu() {
        $(playerSideMenuControllerSelector).sideNav('show');

    }

    function closeSideMenu() {
        $(playerSideMenuControllerSelector).sideNav('hide');
    }

    function makePlayButtonStandOut() {
        //todo feature discovery
    }

    function unmakePlayButtonStandOut() {
        //todo cancel feature discovery
    }

    function onPlayerOpen(modal, trigger) {
        console.log("The player is opened!");
        EventsManager.getInstance().fire(EventsManager.PLAYER_OPEN, player);
        if (instance.playerSession.progress == 0) {
            makePlayButtonStandOut();
            openSideMenu();
        }
    }

    function persistSession() {
        // Request.send(instance.uri, instance.playerSession, null, "json", "POST", {
        //     //other ajax options
        // }).done(function (data, textStatus, jqXHR) {
        //
        // }).fail(function (jqXHR, textStatus, errorThrown) {
        //
        // });
    }

    function onPlayerClose() {
        updateReport(instance.current.parentRef, instance.current.itemRef, function () {
            instance.stop();
            instance.contentArea.empty();
            closeSideMenu();
            $('body').removeClass('player-open-body');
            exitFullScreen($('button.request-fullscreen-controller'));
            console.log("The player is closed!");
            EventsManager.getInstance().fire(EventsManager.PLAYER_CLOSE, player);
        });
    }

    function onBeforePlayerHtml(event, $playerHtml) {
        prepareSideMenuController($playerHtml.find(playerSideMenuControllerSelector));
    }

    function showMenu() {
        $("#player-side-menu > .navigation-tree").removeClass("collapsed");
        $("#player-side-menu .player-side-menu-controller > .off-state").show();
        $("#player-side-menu .player-side-menu-controller > .on-state").hide();
    }

    function hideMenu() {
        $("#player-side-menu > .navigation-tree").addClass("collapsed");
        $("#player-side-menu .player-side-menu-controller > .on-state").show();
        $("#player-side-menu .player-side-menu-controller > .off-state").hide();
    }
    function openPlayer(event) {
        var me = this, $controller = $(event.currentTarget), action = $controller.data('play'), params = null,
            requestDataType = 'text', method = 'GET',
            beforeSend = function (jqXhr) {
                jqXhr.setRequestHeader(App.X_EMBEDDED_FRAGMENT_HEADER, 'yes');
                return true;
            },
            onSuccess = function (playerHtml, textStatus, jqXHR) {
                console.log("Open player:", action);

            }, otherAjaxOptions = null;
        $controller.data({
            openInModal: true,
            openInModalTarget: '#player-modal',
            dismissible: false,
            actionsSelector: '.player-actions',
            dataActionsRight: false,
            modalCloseAction: true,
            modalCancelAction: false,
            ready: function (modal, trigger) {
                onPlayerOpen.call(me, modal, trigger);
            },
            complete: function () {
                onPlayerClose.call(me);
            }
        });
        instance.uri = action;

        EventsManager.getInstance().off(EventsManager.BEFORE_HTML(instance.uri), null, onBeforePlayerHtml).on(EventsManager.BEFORE_HTML(instance.uri), onBeforePlayerHtml);
        app.ajaxRequest($controller, action, params, $controller, beforeSend, onSuccess, requestDataType, method, null, null, otherAjaxOptions);
        return false; // event.preventDefault() and event.stopPropagation()
    }

    function playButtonHandler(event) {
        instance.play();
        return false; // event.preventDefault() and event.stopPropagation()
    }

    function nextButtonHandler(event) {
        console.log("nextButtonHandler");
        instance.navigation.next();
        return false; // event.preventDefault() and event.stopPropagation()
    }

    function previousButtonHandler(event) {
        instance.navigation.previous();
        return false; // event.preventDefault() and event.stopPropagation()
    }

    function playerControllers() {
        EventsManager.getInstance()
            .off("click", "button[data-play]", null, openPlayer)
            .on("click", "button[data-play]", null, openPlayer)
            .on("click", "button.play-controller", null, playButtonHandler)
            .on("click", "button.next-controller", null, nextButtonHandler)
            .on("click", "button.previous-controller", null, previousButtonHandler)
            .on("click", "button.close-action", null, onPlayerClose)
            .off("click", "button.course-finish-controller", null, onPlayerClose)
            .on("click", "button.course-finish-controller", null, onPlayerClose)
            .on("click", "button.request-fullscreen-controller", $('#player-modal').find('.player-content'), requestFullScreen)
            .on("click", "a.player-side-menu-controller .off-state", null, hideMenu)
            .on("click", "a.player-side-menu-controller .on-state", null, showMenu)
            .on("NAVIGATION_EVENT", instance.navigation.to);
        $('#player-modal').on("SHOW_ACTIVITY", function (e, data) {
            instance.navigation.to($("[data-activity-crnodeid='" + data.identifier + "']"));
        });

    }

    function initCourseNavigation() {
        instance.navigation = navigation();
        instance.navigation.init();

        var itemClickHandler = function (e) {
            $("#player-modal").trigger("ENABLE_PLAYER_NAVIGATION");
            instance.navigation.to(this);
        };

        $(".tree-item")
            .off("click", itemClickHandler)
            .on("click", itemClickHandler);
        //first init:
        instance.contentArea = $('#player-content-area');
        instance.navigation.next();
        setTimeout(function () {
            $(".player-side-menu-controller").removeClass("pulse");
            hideMenu();
        }, 6000)
    }

    function requestFullScreen(event, $el) {
        // Supports most browsers and their versions.
        var $button = $(event.currentTarget),
            element = $el && $el.length ? $el[0] : document.body,
            startFullscreen = element.requestFullScreen || element.webkitRequestFullScreen || element.mozRequestFullScreen || element.msRequestFullScreen,
            exitFullscreen = document.exitFullscreen || document.webkitExitFullscreen || document.mozCancelFullScreen || document.msExitFullscreen;
        if (!$button.data('hasFullscreen')) {
            if (startFullscreen) { // Native full screen.
                startFullscreen.call(element);
                $button.data('hasFullscreen', true)
            }
        } else {
            if (exitFullscreen) { // Native exit full screen.
                exitFullScreen($button)
            }
        }
    }

    function exitFullScreen($controller) {
        if (document.exitFullscreen) {
            document.exitFullscreen();
        } else if (document.msExitFullscreen) {
            document.msExitFullscreen();
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        } else if (document.webkitExitFullscreen) {
            document.webkitExitFullscreen();
        }
        $controller.data('hasFullscreen', false)
    }

    function createInstance() {
        return {
            uri: null, /*player url*/
            playerSession: null, /* player session */
            currentRlo: null, /* zero based course material (rlo) to start/resume from */
            timeSpent: 0, /* seconds since the current player session started */
            timeSpentOnCurrentRlo: 0, /* seconds since the current rlo session started */
            init: function (playerSession) {
                instance.playerSession = playerSession;
                instance.reportManager = playerDefaults.reportManager || ReportManager;
                instance.reportManager.init(playerSession);
                initCourseNavigation();
                playerControllers();
                $(".close-action-mobile").off("click");
                $(".close-action-mobile").click(function () {
                    $("#player-modal").modal('close');
                    onPlayerClose();
                });
            },
            play: function () {
                unmakePlayButtonStandOut();
                startCounter();
            },
            nextMaterial: function () {
                instance.navigation.next();
            },
            previousMaterial: function () {
                instance.navigation.previous();
            },
            pause: function () {
                stopCounter();
                persistSession();
            },
            stop: function () {
                stopCounter();
                persistSession();
            },
            stopTimeOnItem: function () {
                return stopCounter();
            },
            startTimeOnItem: function () {
                startCounter();
            }
        }
    }

    return {
        ready: function (config) {
            player = this.getInstance(config);
            return player;
        },
        /**
         *
         * @returns Object
         */
        getInstance: function (config) {
            if (!instance) {
                instance = createInstance(config);
            }
            return instance;
        }
    }
})(playerDefaults);