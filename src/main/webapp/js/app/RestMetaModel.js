/**
 * User: AlexandruVi
 * Date: 2017-04-27
 */
/**
 * RestMetaModel
 * @param modelName
 * @constructor
 */
function RestMetaModel(modelName) {
    var name = modelName,
        cacheExpiryInSeconds = 3600 * 1000,
        searchParamName = "search",
        modelDescriptors = {};
    this.setModelDescriptor = function (md) {
        modelDescriptors[md.viewName] = md;
    };

    this.hasModelDescriptorForView = function (viewName) {
        var vn = viewName || RestMetaModel.DEFAULT_VIEW_NAME;
        return !!modelDescriptors[vn];
    };

    this.getModelDescriptor = function (viewName) {

        var vn = viewName || RestMetaModel.DEFAULT_VIEW_NAME;
        if (!modelDescriptors[vn]) {
            throw new AppException("[" + name + "] There is no model descriptor for the given view name (" + vn + ")!");
        }
        return modelDescriptors[vn];
    };

    this.getCacheExpiry = function () {
        return cacheExpiryInSeconds;
    };
    this.setCacheExpiry = function (expiry) {
        cacheExpiryInSeconds = expiry;
    };
    this.getModelName = function () {
        return name;
    };
    this.getSearchParamName = function (viewName) {
        return this.getModelDescriptor(viewName).serviceProperties.searchParamName;
    };
    this.getSearchCriteriaForSearchTerm = function (searchTerm, filtersBeforeSearch, viewName) {
        var modelDescriptor = this.getModelDescriptor(viewName), searchCriteria = {};
        if (filtersBeforeSearch) {
            searchCriteria = $.extend(searchCriteria, filtersBeforeSearch);
        }
        searchCriteria[searchParamName] = null;
        if (searchTerm) {
            if (modelDescriptor.searchOptions.criteria && modelDescriptor.searchOptions.criteria.length > 0) {
                var propertySearchCriteriaList = [];
                $.each(modelDescriptor.searchOptions.criteria, function (index, propertySearchCriteria) {
                    propertySearchCriteriaList.push(propertySearchCriteria.propertyName + propertySearchCriteria.operation.urlOperator + searchTerm);
                });
                if (propertySearchCriteriaList.length > 0) {
                    searchCriteria[searchParamName] = propertySearchCriteriaList.join(",");
                }
            }
        }
        if (viewName && viewName!==RestMetaModel.DEFAULT_VIEW_NAME){
            searchCriteria['viewName']=viewName;
        }
        return searchCriteria;
    };
}

RestMetaModel.DEFAULT_VIEW_NAME = '__default';
RestMetaModel.OPTIONAL_INPUT_LABEL = 'model.{modelName}.property.{propertyName}.optional';
RestMetaModel.OBJECT_BUILDER_EXPAND_BUTTON_TOOLTIP = 'model.{modelName}.action.expand.object.tooltip';
RestMetaModel.COLLECTION_BUILDER_ADD_BUTTON_TOOLTIP = 'model.{modelName}.action.add.item.tooltip';
RestMetaModel.COLLECTION_BUILDER_DELETE_BUTTON_TOOLTIP = 'model.{modelName}.action.delete.item.tooltip';
RestMetaModel.COLLECTION_BUILDER_SAVE_BUTTON_TOOLTIP = 'model.{modelName}.action.save.item.tooltip';