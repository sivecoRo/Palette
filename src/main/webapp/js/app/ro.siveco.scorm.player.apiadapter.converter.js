/**
 * SCORM 1.2 to 1.3 Converter/Stub
 * defineste obiectul API
 * functii de conversie intre tipuri de date CMI
 * stub-uri pentru functiile neschimbate
 * stub-uri pentru metode
 * @autor: Bogdan Costea <bogdan.costea@siveco.ro>
 */

// obiectul definit ca API adapter
API = new Object();
// obiect pentru stocarea de date temporare
API.kFG = new Object();
// obiect pentru stocarea unei simulari de RTE
API.kFG.rte = new Object();

// marker pentru tracking al apelarii metodelor
API.kFG.initialize = false;
API.kFG.terminate = false;

// obiect ce simuleaza un scor in RTE
function kFGScore() {
    this.scaled = '';
    this.raw = '';
    this.min = API.kFG.scoreMin + "";
    this.max = API.kFG.scoreMax + "";
};

// marker pentru tracking al erorilor
API.kFG._err = 0;

/**
 * Valori utilizate in normalizarea score-ului
 * vor fi utilizate pentru a calcula score.scaled
 */
API.kFG.scoreMin = 0;
API.kFG.scoreMax = 100;

// score-ul SCO-ului
API.kFG.score = new kFGScore();

/**
 * runtime-ul original 1.3
 */
API.kFG.originalAPI = null;

/* Starea sesiunii
 * 0=not initialized yet;
 * 1=initializing;
 * 2=initialized;
 * 3=terminating;
 * 4=terminated;
 * -1=wtf (session cannot be established?)
 */
API.kFG.sessionState = 0;

/**
 * verificam daca a fost definit un obiect de internationalizare
 * daca nu, completam unul cu mesaje in engleza
 */
if (!API.kFG.staticmessages) {
    API.kFG.staticmessages = new Object();
    API.kFG.staticmessages.initialize = new Object();
    API.kFG.staticmessages.errorTitle = "APIAdapter error";
}

function scorm12DurationToISODuration(sOld) {
    return centisecsToISODuration(SCORM12DurationToCs(sOld), true);
}

function FixTimeStamp(sOld) {
    var sD = MakeISOtimeStamp(null, true);
    return sD.substr(0, 11) + sOld;
}

API.kFG.getOriginal13API = function () {
    if (!API.kFG.originalAPI) {
        API.kFG.originalAPI = API_1484_11;
    }

    return API.kFG.originalAPI;
}

/**
 * Initializeaza comunicatia
 * LMSInitialize("") changed to Initialize("")
 * In 1.3 ai voie sa apelezi metoda de cate ori vrei, in 1.2 nu.
 */
API.LMSInitialize = function (/*String*/ s) {
    // am mai fost apelat?
    if (API.kFG.initialize && API.kFG.sessionState != 0) {
        return "false"; //returnam false cf standardului
    }
    else {
        API.kFG.sessionState = 1; //adica initializing
        API.kFG.initialize = true;
        var res = API.kFG.getOriginal13API().Initialize("");
        if (res == true || res == "true") {
            API.kFG.sessionState = 2; //initialized
            return "true";
        }
    }
    API.kFG.sessionState = -1; //daca am ajuns aici avem o mare problema deci eroare
    return "false";
}

/**
 * Inchide comunicatia
 * LMSFinish("") changed to Terminate("")
 */
API.LMSFinish = function (/*String*/ s) {
    if (API.kFG.sessionState == 2) {
        API.kFG.sessionState = 3; //terminating
        var res = API.kFG.getOriginal13API().Terminate("");
        if (res == true || res == "true") {
            API.kFG.initialize = false;
            API.kFG.terminate = true;
            API.kFG.sessionState = 4; // you have been terminated
            return "true";
        }
    }
    return "false";
}

/**
 * Persista
 * LMSCommit("") changed to Commit("")
 * commit-ul este un clean passthrough, fara modificari
 */
API.LMSCommit = function (/*String*/ s) {
    // suntem intr-o sesiune activa?
    if (API.kFG.scormSessionActive()) {
        // clean passthrough
        return API.kFG.getOriginal13API().Commit("");
    }
    return "false";
}

/**
 * Seteaza o valoare
 * LMSGetValue("") changed to GetValue("")
 *
 */
API.LMSGetValue = function (/*String*/ parameter) {
    if (parameter == null) {
        API.kFG._err = 201;
        return "false";
    }

    if (!API.kFG.scormSessionActive()) {
        return "false";
    }

    API.kFG._err = 0;
    var r = "";
    var a = parameter.split(".");

    switch (a[0]) {
        case "cmi":
            switch (a[1]) {
                case "comments":
                    r = API.kFG.getOriginal13API().GetValue("cmi.comments_from_learner.0.comment");
                    break;
                case "comments_from_lms":
                    r = API.kFG.getOriginal13API().GetValue("cmi.comments_from_lms.0.comment");
                    break;
                case "core":
                    switch (a[2]) {
                        case "_children":
                            r = "credit,entry,exit,lesson_location,lesson_mode,lesson_status,"
                                + "score,session_time,student_id,student_name,total_time";
                            break;
                        case "credit":
                            r = API.kFG.getOriginal13API().GetValue("cmi.credit");
                            break;
                        case "entry":
                            r = API.kFG.getOriginal13API().GetValue("cmi.entry");
                            if (r == "ab_initio") {
                                r = "ab-initio";
                            }
                            break;
                        case "lesson_location":
                            r = API.kFG.getOriginal13API().GetValue("cmi.location");
                            break;
                        case "lesson_mode":
                            r = API.kFG.getOriginal13API().GetValue("cmi.mode");
                            break;
                        case "lesson_status":
                            var s_s = API.kFG.getOriginal13API().GetValue("cmi.success_status");
                            if ((s_s == "passed") || (s_s == "failed")) {
                                r = s_s;
                            } else {
                                r = API.kFG.getOriginal13API().GetValue("cmi.completion_status");
                            }
                            if ((r == "") || (r == "unknown")) {
                                r = "not attempted";
                            }
                            break;
                        case "score":
                            if (IsInList(a[3], "min,max,raw")) {
                                r = API.kFG.getScore(API.kFG.score, a[3]);
                            } else {
                                API.kFG._err = 201;
                            }
                            break;
                        case "student_id":
                            r = API.kFG.getOriginal13API().GetValue("cmi.learner_id");
                            break;
                        case "student_name":
                            r = API.kFG.getOriginal13API().GetValue("cmi.learner_name");
                            break;
                        case "total_time":
                            //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                            r = centisecsToSCORM12Duration(ISODurationToCentisec(API.kFG.getOriginal13API().GetValue("cmi.total_time")));
                            break;
                        default:
                            API.kFG._err = 201;
                            break;
                    }
                    break;
                case "interactions":
                    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    r = GetValueInteractions(parameter)
                    break;
                case "objectives":
                    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    r = GetValueObjectives(parameter)
                    break;
                case "student_data":
                    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    r = GetValueStudentDataItem(a[2]);
                    break;
                case "student_preference":
                    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    r = GetValueLearnerPrefItem(n);
                    break;
                default:
                    r = API.kFG.getOriginal13API().GetValue(parameter);
            }
    }

    //console.log(parameter + " = " + r + "");

    return r + "";
}

API.LMSSetValue = function (/*String*/ parameter, /*String*/ value) {
    //console.info(parameter + ": " + value);
    var setValue = function (param, val) {
        return API.kFG.getOriginal13API().SetValue(param, val);
    }

    if (parameter == null) {
        API.kFG._err = 201;
        return "false";
    }

    if (!API.kFG.scormSessionActive()) {
        return "false";
    }

    API.kFG._err = 0;
    var r = "";
    var a = parameter.split(".");

    switch (a[0]) {
        case "cmi":
            switch (a[1]) {
                case "comments":
                    r = setValue("cmi.comments_from_learner.0.comment", value);
                    break;
                case "comments_from_lms":
                    r = setValue("cmi.comments_from_lms.0.comment");
                    break;
                case "core":
                    switch (a[2]) {
                        case "_children":
                            r = "true"; // Ignoram ( nu mai exista in SCORM 2004 )
                            break;
                        case "credit":
                        case "total_time":
                        case "lesson_mode":
                        case "student_id":
                        case "student_name":
                        case "entry":
                            API.kFG._err = 403;
                            break;
                        case "lesson_location":
                            r = setValue("cmi.location", value);
                            break;
                        case "exit":
                            /*r = setValue("cmi.exit", value);*/
                            r = "true";
                            break;
                        case "lesson_status":
                            if ((value == "completed") || (value == "incomplete") || (value == "not attempted")) {
                                r = setValue("cmi.completion_status", value);
                            }
                            else if ((value == "passed") || (value == "failed")) {
                                // setValue("cmi.completion_status", "completed")
                                r = setValue("cmi.success_status", value);
                            }
                            else if (value == "browsed") {
                                r = "true"; // Ignoram
                            }
                            else {
                                API.kFG._err = 201;
                            }
                            break;
                        case "score":
                            fixScore(API.kFG.score, a[3], value)
                            if ((a[3] == "raw") && (API.kFG.score.scaled != "")) {
                                setValue("cmi.score.scaled", API.kFG.score.scaled);
                            }
                            r = setValue("cmi.score." + a[3], value);
                            break;
                        case "session_time":
                            r = setValue('cmi.session_time', centisecsToISODuration(SCORM12DurationToCs(value)));
                            break;
                        default:
                            API.kFG._err = 201;
                            break;
                    }
                    break;
                case "interactions":
                    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    r = SetValueInteractions(parameter, value)
                    break;
                case "objectives":
                    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    r = "true"
                    break;
                case "student_data":
                    r = "true";
                    break;
                case "suspend_data":
                    r = setValue('cmi.suspend_data', value);
                    break;
                case "student_preference":
                    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    r = "true";
                    break;
                default:
                    r = API.kFG.getOriginal13API().GetValue(parameter);
            }
    }

    return r + "";
}

function fixScore(aScore, what, val) {
    var nVal = parseFloat(val);

    if (!isNaN(nVal)) {
        switch (what) {
            case "raw":
                aScore.raw = val + "";
                var nMin = parseFloat(aScore.min);
                var nMax = parseFloat(aScore.max);
                if ((!isNaN(nMin)) && (!isNaN(nMax))) {
                    if (nVal <= nMax) {
                        aScore.scaled = ((val - nMin) / (nMax - nMin)) + "";
                    }
                }
                break;
            case "min":
                aScore.min = val + "";
                break;
            case "max":
                aScore.max = val + "";
                break;
        }
    }
    return 0
}

function setScormValue(param, val) {
    API.kFG.getOriginal13API().SetValue(param, val);
}

function FixCorrectResponsePattern(val, sType) {
    switch (sType) {
        case "true-false":
            val = val.substr(0, 1);
            if ((val == "t") || (val == "1")) {
                val = "true";
            } else if ((val == "f") || (val == "0")) {
                val = "false";
            } else {
                API.kFG._err = 201;
            }
            break;
        case "choice":
        case "sequencing":
            var a = val.split(",");
            if (a.length > 1) {
                val = a[0];
                for (i = 1; i < a.length; i++) {
                    val += "[,]" + a[i];
                }
            }
            break;
        case "fill-in":
            val = "{case_matters}" + val;
            break;
        case "likert":
            break;
        case "matching":
            var a = val.split(",");
            for (i = 0; i < a.length; i++) {
                b = a[i].split(".");
                a[i] = b[0] + "[.]" + b[1];
            }
            val = a[0];
            for (i = 1; i < a.length; i++) {
                val += "[,]" + a[i];
            }
            break;
        case "numeric":
            if (isNaN(parseFloat(val))) {
                API.kFG._err = 201;
            } // tbd check err code
            break;
        case "performance":
            // WAG
            var a = val.split(",");
            for (i = 0; i < a.length; i++) {
                a[i] = "[.]" + a[i];
            }
            val = a[0];
            for (i = 1; i < a.length; i++) {
                val += "[,]" + a[i];
            }
            break;
        default:
            API.kFG._err = 301;
    }
    return val;
}

function FixLearnerResponsePattern(val, sType) {
    switch (sType) {
        case "true-false":
            val = val.substr(0, 1);
            if ((val == "t") || (val == "1")) {
                val = "true";
            } else if ((val == "f") || (val == "0")) {
                val = "false";
            } else {
                API.kFG._err = 201;
            }
            break;
        case "choice":
        case "sequencing":
            var a = val.split(",");
            if (a.length > 1) {
                val = a[0];
                for (i = 1; i < a.length; i++) {
                    val += "[,]" + a[i];
                }
            }
            break;
        case "fill-in":
        case "likert":
            break;
        case "matching":
            var a = val.split(",");
            for (i = 0; i < a.length; i++) {
                b = a[i].split(".");
                a[i] = b[0] + "[.]" + b[1];
            }
            val = a[0];
            for (i = 1; i < a.length; i++) {
                val += "[,]" + a[i];
            }
            break;
        case "numeric":
            if (isNaN(parseFloat(val))) {
                API.kFG._err = 201;
            } // tbd check err code
            break;
        case "performance":
            // WAG
            var a = val.split(",");
            for (i = 0; i < a.length; i++) {
                a[i] = "[.]" + a[i];
            }
            val = a[0];
            for (i = 1; i < a.length; i++) {
                val += "[,]" + a[i];
            }
            break;
        default:
            API.kFG._err = 301;
    }
    return val;
}

function GetValueStudentDataItem(what) {
    var r = "";
    switch (what) {
        case "_children":
            r = "mastery_score,max_time_allowed,time_limit_action";
            break;
        case "mastery_score":
            var n = parseFloat(API.kFG.getOriginal13API().GetValue('cmi.scaled_passing_score'));
            if (isNaN(n)) {
                n = 1;
            }
            r = RawPassingScoreFromScaled(n);
            break;
        case "max_time_allowed":
            r = API.kFG.getOriginal13API().GetValue("cmi.max_time_allowed")
            r = centisecsToSCORM12Duration(ISODurationToCentisec(r));
            if (API.kFG._err != 0) {
                r = "";
            }
            break;
        case "time_limit_action":
            r = API.kFG.getOriginal13API().GetValue("cmi.time_limit_action");
            break;
    }
    return r + "";
}

var gaInteractionTypes = new Array();

function SetValueInteractions(parameter, value) {
    var r = "";
    var sType = "";
    var a = parameter.split(".");
    // a[0]='cmi';a[1]='interactions';a[2]=n; etc.

    var n = parseInt(a[2]);
    if (!isPositiveInt(n)) {
        return setScormValue(parameter, value);
    }

    switch (a[3]) {
        case "type":
            sType = value;
            if (!IsInList(value, "true-false,choice,fill-in,matching,performance,sequencing,likert,numeric")) {
                API.kFG._err = 201;
                return "false"
            }
            gaInteractionTypes[n] = value;
            r = setScormValue(parameter, value);
            break;
        case "student_response":
            break;
            value = FixLearnerResponsePattern(value, gaInteractionTypes[n]);
            if (API.kFG._err == 0) {
                r = setScormValue(parameter, value);
            } else {
                return "false";
            }
            break;
        case "correct_responses":
            if ((!isPositiveInt(a[4])) || (a[5] != "pattern")) {
                API.kFG._err = 201;
                return "false";
            }
            value = FixCorrectResponsePattern(value, gaInteractionTypes[n]);
            if (API.kFG._err == 0) {
                r = setScormValue(parameter, value);
            } else {
                return "false";
            }
            break;
        case "latency":
            r = setScormValue("cmi.interactions." + n + ".latency", scorm12DurationToISODuration(value));
            break;
        case "result":
            if (value == "wrong") {
                value = "incorrect";
            }
            r = setScormValue("cmi.interactions." + n + ".result", (value));
            break;
        case "time":
            r = setScormValue("cmi.interactions." + n + ".timestamp", FixTimeStamp(value));
            break;
        case "weighting":
            r = setScormValue("cmi.interactions." + n + ".weighting", (value));
            break;
        default:
            r = setScormValue(parameter, value);
    }
    return r
}

function centisecsToSCORM12Duration(n) {
    // Format is [HH]HH:MM:SS[.SS]
    var bTruncated = false;
    with (Math) {
        var nH = floor(n / 360000);
        var nCs = n - nH * 360000;
        var nM = floor(nCs / 6000);
        nCs = nCs - nM * 6000;
        var nS = floor(nCs / 100);
        nCs = nCs - nS * 100;
    }
    if (nH > 9999) {
        nH = 9999;
        bTruncated = true;
    }
    var str = "0000" + nH + ":";
    str = str.substr(str.length - 5, 5);
    if (nM < 10) {
        str += "0";
    }
    str += nM + ":";
    if (nS < 10) {
        str += "0";
    }
    str += nS;
    if (nCs > 0) {
        str += ".";
        if (nCs < 10) {
            str += "0";
        }
        str += nCs;
    }
    //if (bTruncated) alert ("Hours truncated to 9999 to fit HHHH:MM:SS.SS format")
    return str;
}

function SCORM12DurationToCs(str) {
    var a = str.split(":");
    var nS = 0, n = 0, i;
    var nMult = 1;
    var bErr = ((a.length < 2) || (a.length > 3));
    if (!bErr) {
        for (i = a.length - 1; i >= 0; i--) {
            n = parseFloat(a[i]);
            if (isNaN(n)) {
                bErr = true;
                break;
            }
            nS += n * nMult;
            nMult *= 60;
        }
    }
    if (bErr) {
        return NaN;
    }
    return Math.round(nS * 100);
}

var defaultISODuration = "PT0H0M0S";

function centisecsToISODuration(/*long*/ n) {
    n = Math.round(Math.max(n, 0)); // nu exista durate negative
    var isoDuration = "P";
    var centisecunde = n;
    var ani = Math.floor(centisecunde / 3155760000);
    centisecunde -= ani * 3155760000;
    var luni = Math.floor(centisecunde / 262980000);
    centisecunde -= luni * 262980000;
    var zile = Math.floor(centisecunde / 8640000);
    centisecunde -= zile * 8640000;
    var ore = Math.floor(centisecunde / 360000);
    centisecunde -= ore * 360000;
    var minute = Math.floor(centisecunde / 6000);
    centisecunde -= minute * 6000

    if (ani > 0) {
        isoDuration += ani + "Y";
    }
    if (luni > 0) {
        isoDuration += luni + "M";
    }
    if (zile > 0) {
        isoDuration += zile + "D";
    }

    if ((ore > 0) || (minute > 0) || (centisecunde > 0)) {
        isoDuration += "T";
        if (ore > 0) {
            isoDuration += ore + "H";
        }
        if (minute > 0) {
            isoDuration += minute + "M";
        }
        if (centisecunde > 0) {
            isoDuration += (centisecunde / 100) + "S";
        }
    }

    // PT0S ar trebui sa fie de ajuns insa in SCORM trebuie folosita varianta intreaga
    if (isoDuration == "P") {
        isoDuration = defaultISODuration;
    }
    return isoDuration;
}

function ISODurationToCentisec(isoDuration) {
    var eroare = false;
    var valoare = new Array(0, 0, 0, 0, 0, 0);
    var amT = false;

    // daca nu incepe cu o perioada
    if (isoDuration.indexOf("P") != 0) {
        eroare = true;
    }

    if (!eroare) {
        var timp = new Array("Y", "M", "D", "H", "M", "S");

        isoDuration = isoDuration.substr(1); //sarim perioada
        for (var i = 0; i < timp.length; i++) {
            if (isoDuration.indexOf("T") == 0) {
                isoDuration = isoDuration.substr(1);
                i = Math.max(i, 3);
                amT = true;
            }
            var p = isoDuration.indexOf(timp[i]);

            if (p > -1) {
                if ((i == 1) && (isoDuration.indexOf("T") > -1) && (isoDuration.indexOf("T") < p)) {
                    continue;
                }

                if (timp[i] == "S") {
                    valoare[i] = parseFloat(isoDuration.substr(0, p))
                }
                else {
                    valoare[i] = parseInt(isoDuration.substr(0, p))
                }

                if (isNaN(valoare[i])) {
                    eroare = true;
                    break;
                }
                else if ((i > 2) && (!amT)) {
                    eroare = true;
                    break;
                }
                isoDuration = isoDuration.substr(p + 1);
            }
        }
        if ((!eroare) && (isoDuration.length != 0)) {
            eroare = true;
        }
    }

    if (eroare) {
        API.kFG._err = 406; // de verificat daca 406 este eroarea corecta
        return 0
    }

    return valoare[0] * 3155760000 + valoare[1] * 262980000 + valoare[2] * 8640000 + valoare[3] * 360000 + valoare[4] * 6000 + Math.round(valoare[5] * 100);
}

function RawPassingScoreFromScaled(n) {
    // Synthesize a scaled score
    return n * (parseFloat(API.kFG.score.max) - parseFloat(API.kFG.score.min)) + parseFloat(API.kFG.score.min);
}

API.LMSGetDiagnostic = function (/*String*/ parameter) {

}

/**
 * Afiseaza descrierea erorii
 */
API.LMSGetErrorString = function (/*String*/ parameter) {
    return API.kFG.getOriginal13API().GetErrorString(parameter);
}

/**
 * Afiseaza ultima eroare
 * LMSGetLastError() changed to GetLastError()
 */
API.LMSGetLastError = function () {
    // traducem si returnam codul de eroare
    return API.kFG.get12Error(API.kFG.getOriginal13API().GetLastError());
}

/*
 * Traducere a codului de eroare de la 13 la 12
 */
API.kFG.get12Error = function (/*String*/ e13Code) {
    if (isNaN(e13Code)) {
        e13Code = parseInt(e13Code);
    }
    if (isNaN(e13Code)) {
        return "Unrecognized error value";
    }

    switch (e13Code) {
        case 0:
        case 201:
            break;
        case 102:
        case 112:
        case 122:
        case 132:
        case 142:
            e13Code = 301;
            break;
        case 402:
            e13Code = 401;
            break;
        case 404:
            e13Code = 403;
            break;
        // data element not initialized nu exista pe vremea aia
        case 403:
            e13Code = 0;
            break;
        case 405:
            e13Code = 404;
            break;
        case 406:
            e13Code = 406;
            break;
        case 407:
            e13Code = 201;
            break
        default:
            e13Code = 101;
    }
    return e13Code;
}

/*
 * translatare pe error string-uri
 */
API.kFG.get12ErrorString = function (/*String*/ parm) {
    var n = parseInt(parm + "");
    if (!API.kFG.isPositive(n)) {
        n = 101;
    }

    switch (n) {
        case 0:
            r = "No error";
            break;
        case 101:
            r = "General exception";
            break;
        case 201:
            r = "Invalid argument error";
            break;
        case 202:
            r = "Element cannot have children";
            break;
        case 203:
            r = "Element not an array - cannot have count";
            break;
        case 301:
            r = "Not initialized";
            break;
        case 401:
            r = "Not implemented error";
            break;
        case 402:
            r = "Invalid set value, element is a keyword";
            break;
        case 403:
            r = "Element is read only";
            break;
        case 404:
            r = "Element is write only";
            break;
        case 405:
            r = "Incorrect Data Type";
            break;
        default:
            r = "Unrecognized SCORM 1.2 error code";
    }
    return r;
}

/**
 * sesiunea scorm este activa?
 * pot sa fac get/set?
 */
API.kFG.scormSessionActive = function () {
    return ((API.kFG.sessionState == 2) || (API.kFG.sessionState == 3));
}

//--------------UTILITARE
/**
 * Afiseaza un mesaj
 */
API.kFG.showMessage = function (/*String*/ title, /*String*/ content) {
    alert(content);
}

API.kFG.isPositive = function (/*int*/ num) {
    return (
        (!isNaN(num)) &&
        (Math.round(num) == num) &&
        (num >= 0)
    );
}

API.kFG.trim = function (/*String*/ str) {
    if (str == null) {
        return "";
    }
    return str.replace(/^\s*(\b.*\b|)\s*$/, "$1");
}

API.kFG.compareStr = function (/*String*/ string1, /*String*/ string2) {
    if (string1 > string2) {
        return 1;
    }
    if (string1 < string2) {
        return -1;
    }
    return 0;
}

API.kFG.inList = function (/*String*/ s, /*String*/ sList) {
    var a = sList.split(",");
    for (i = 0; i < sList.length; i++) {
        if (a[i] == s) {
            return true;
        }
    }
    return false;
}

API.kFG.getScore = function (score, param) {
    var result = "";
    switch (param) {
        case "min":
            result = score.min;
            break;
        case "max":
            result = score.max;
            break;
        case "scaled":
            result = score.scaled;
            break;
        case "raw":
            result = score.raw;
            break;
        case "_children":
            r = "min,max,raw";
            break;
        default:
            API.kFG._err = 201;
    }
    return result;
}

/*
function proxy(method, name)
{
	return function(){
		
		console.log(new Date().toLocaleTimeString() + ': ' + name + ':', arguments);
		var ret = method.apply(this, arguments);
		console.log(new Date().toLocaleTimeString() + ': ' + name + ' returned : ' + ret);
		return ret;
	}
}


API.LMSInitialize = proxy(API.LMSInitialize, 'API.LMSInitialize');
API.LMSFinish = proxy(API.LMSFinish,'API.LMSFinish');
API.LMSCommit = proxy(API.LMSCommit,'API.LMSCommit');
API.LMSGetValue = proxy(API.LMSGetValue,'API.LMSGetValue');
API.LMSSetValue = proxy(API.LMSSetValue,'API.LMSSetValue');
*/

API_1484_11 = API