$(document).ready(function () {
    $('.collapsible').collapsible();
});
$("#exporterForm").on('submit', function (event) {
    event.preventDefault();

    var exportForm = $("#exporterForm");
    $.ajax({
        type: 'POST',
        url: exportForm.prop("action"),
        data: exportForm.serialize(),
        success: function (data, textStatus, jqXHR) {
            if (data.success) {
                $("#successMsg").text($(".messages [message-attr='" + data.resultMsg + "']").text());
                $("#successMsg").removeClass("hidden");
                $("#destinationFilePath").val("");
                if (!$("#errorMsg").hasClass("hidden")) {
                    !$("#errorMsg").addClass("hidden");
                }

            }
            else {
                $("#errorMsg").text($(".messages [message-attr='" + data.resultMsg + "']").text());
                $("#errorMsg").removeClass("hidden");
                if (!$("#successMsg").hasClass("hidden")) {
                    !$("#successMsg").addClass("hidden");
                }
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            $("#errorMsg").text($(".messages [message-attr='" + data.resultMsg + "']").text());
            $("#errorMsg").removeClass("hidden");

        }
    });
});

var $fileSelect = $("#fileSelect"),
    $fileElem = $("#fileElem");

$fileSelect.click(function (e) {
    if ($fileElem) {
        $fileElem.click();
    }
    e.preventDefault(); // prevent navigation to "#"
});
var fileToImport;
$fileElem.on("change", function (e) {
    if ($fileElem[0].files.length > 0) {
        $("#fileElemName").val($fileElem[0].files[0].name);
        fileToImport = $fileElem[0].files[0];
    }
});

$("#importBtn").on("click", function (e) {
    e.preventDefault();
    if ($fileElem[0].files.length > 0) {
        if ($fileElem[0].files[0].name.toLowerCase().indexOf("xls") == -1) {
            console.log("Error select file");
        }
        else {

            var name = fileToImport.name;
            var fData = new FormData();
            fData.append("importFile", fileToImport);

            var importForm = $("#importForm");

            $.ajax({
                type: 'POST',
                url: importForm.prop("action"),
                data: fData,
                dataType: 'json',
                contentType: false,
                processData: false,
                success: function (data, textStatus, jqXHR) {
                    if (data.success) {
                        $("#successMsg").text($(".messages [message-attr='" + data.resultMsg + "']").text());
                        $("#successMsg").removeClass("hidden");
                        $("#fileElem").val("");
                        $("#fileElemName").val("");
                        if (!$("#errorMsg").hasClass("hidden")) {
                            !$("#errorMsg").addClass("hidden");
                        }

                    }
                    else {
                        $("#errorMsg").text($(".messages [message-attr='" + data.resultMsg + "']").text());
                        $("#errorMsg").removeClass("hidden");
                        if (!$("#successMsg").hasClass("hidden")) {
                            !$("#successMsg").addClass("hidden");
                        }
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    $("#errorMsg").text($(".messages [message-attr='" + data.resultMsg + "']").text());
                    $("#errorMsg").removeClass("hidden");
                }
            });
        }
        $fileElem[0].value = null;
    }
});