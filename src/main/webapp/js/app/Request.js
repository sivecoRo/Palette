/**
 * User: AlexandruVi
 * Date: 2017-04-27
 */
/**
 * Request
 *
 * @param url A string containing the URL to which the request is sent.
 * @param data Data to be sent to the server. It is converted to a query string, if not already a string. It's appended
 *     to the url for GET-requests. See processData option to prevent this automatic processing. Object must be
 *     Key/Value pairs. If value is an Array, jQuery serializes multiple values with same key based on the value of the
 *     traditional setting.
 * @param firstSuccessHandler A success handler to be executed first
 * @param dataType [default=json] The type of data that you're expecting back from the server. If none is specified,
 *     jQuery will try to infer it based on the MIME type of the response (an XML MIME type will yield XML, in 1.4 JSON
 *     will yield a JavaScript object, in 1.4 script will execute the script, and anything else will be returned as a
 *     string)
 * @param method  [default=POST] The HTTP method to use for the request (e.g. "POST", "GET", "PUT").
 * @param opts  A set of key/value pairs that configure the Ajax request.
 * @constructor
 */
function Request(url, data, firstSuccessHandler, dataType, method, opts) {
    var me = this, xhr, errorHandler, getErrorFromJqXHR, sending = false, error;
    this.url = url;
    this.data = data;
    this.firstSuccessHandler = firstSuccessHandler;
    this.dataType = dataType ? dataType : "json";
    this.method = method ? method : "POST";
    this.processedError = null;

    getErrorFromJqXHR = function (dataType, jqXHR, textStatus, errorThrown) {
        var me = this, error;
        try {
            var parseJson = function () {
                try {
                    var json = $.parseJSON(jqXHR.responseText);
                } catch (e) {
                    return App.getInstance().g11n.lang.app.errors.generic;
                }
                return json;
            };
            if (jqXHR.readyState === 0 && textStatus !== "timeout") {
                error = App.getInstance().g11n.lang.app.errors.net;
                return error;
            } else if (textStatus === "timeout") {
                error = App.getInstance().g11n.lang.app.errors.timeout;
                error.messageArguments.timeout = parseInt(me.getTimeout() / 1000);
                error.option.handler = function (event, notificationWrapper) {
                    return me.resend();
                };
                return error;
            } else if (dataType === "json") {
                error = jqXHR.responseJSON ? typeof jqXHR.responseJSON.error === "object" ? jqXHR.responseJSON.error : jqXHR.responseJSON : parseJson();
                error.code = jqXHR.status;
                if (!error.title || error.title.indexOf("??") === 0) {
                    error.title = error.error;
                }
                return error;
            } else {
                error = parseJson(jqXHR.responseText);
                error.code = jqXHR.status;
                if (!error.title || error.title.indexOf("??") === 0) {
                    error.title = error.error;
                }
                if (!error.message || error.message === "No message available" && error.code === 404) {
                    error = App.getInstance().g11n.lang.app.errors.notFound;
                }
                return error;
            }
        }
        catch (e) {
            App.getInstance().defaultErrorHandler(e);
            return App.getInstance().g11n.lang.app.errors.generic;
        }
    };

    errorHandler = function (dataType, jqXHR, textStatus, errorThrown, handlerCallback) {
        var me = this, handler = typeof handlerCallback === "function" ? function (error) {
            EventsManager.getInstance().fire(EventsManager.AJAX_EXCEPTION, me.url, error);
            handlerCallback(error);
        } : App.getInstance().defaultErrorHandler;
        error = getErrorFromJqXHR.call(me, dataType, jqXHR, textStatus, errorThrown);
        if (error.messageArguments) {
            for (var messageArg in error.messageArguments) {
                if (error.messageArguments.hasOwnProperty(messageArg)) {
                    var messageArgValue = error.messageArguments[messageArg];
                    error.message =
                        error.message.replace(new RegExp("\{\{" + messageArg + "\}\}", 'g'), messageArgValue);
                }
            }
        }
        me.processedError = error;
        handler.call(App.getInstance(), error);
    };

    this.sendRequest = function (opts) {
        var me = this,
            successHandler = function (data, textStatus, jqXHR) {
                EventsManager.getInstance().fire(EventsManager.AJAX_REQUEST_SUCCESS, me.url, me.dataType, data, textStatus, jqXHR);
                if (typeof me.firstSuccessHandler === "function") {
                    if (!opts || !opts.skippRedirect) {
                        me.redirect();
                    }
                    return me.firstSuccessHandler(data, textStatus, jqXHR);
                } else {
                    return jqXHR;
                }
            },
            options = $.extend({}, {
                type: me.method,
                url: me.url,
                data: me.data,
                success: successHandler,
                dataType: me.dataType,
                useAppGenericErrorHandler: true,
                errorHandler: null,
                timeout: App.getInstance().preferences.request.ajaxTimeout
            }, opts);
        try {
            if (sending) {
                return this;
            }
            sending = true;
            EventsManager.getInstance().fire(EventsManager.AJAX_REQUEST_STARTED, me.url, me.dataType, data);
            xhr = $.ajax(options).fail(function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.status === 201) {
                    successHandler.call(me, {}, textStatus, jqXHR);
                } else if (options.useAppGenericErrorHandler) {
                    errorHandler.call(me, options.dataType, jqXHR, textStatus, errorThrown, options.errorHandler);
                } else {
                    App.getInstance().defaultErrorHandler(errorThrown);
                }
                return jqXHR;
            }).always(function (a, b, c) {
                if (!opts || !opts.skippRedirect) {
                    me.redirect();
                }
                sending = false;
                if (me.dataType === "text") {
                    // EventsManager.getInstance().fire(EventsManager.HTML_REQUEST, a);
                }
                EventsManager.getInstance().fire(EventsManager.AJAX_REQUEST_COMPLETED, me.url);
            });
            xhr.__request = me;
        } catch (e) {
            EventsManager.getInstance().fire(EventsManager.EXCEPTION, e);
            App.getInstance().defaultErrorHandler(e);
        } finally {
        }
        return this;
    };
    this.isSending = function () {
        return sending;
    };
    this.getError = function () {
        return error;
    };
    this.resend = function () {
        return this.sendRequest();
    };
    this.always = function (cb) {
        xhr.always(cb);
        return this;
    };
    this.done = function (cb) {
        xhr.done(cb);
        return this;
    };
    this.fail = function (cb) {
        xhr.fail(cb);
        return this;
    };
    this.redirect = function () {
        var redirect = xhr.getResponseHeader('Location');
        if (xhr && xhr.responseJSON && xhr.responseJSON.redirect) {
            redirect = xhr.responseJSON.redirect;
        } else if (xhr && xhr.responseJSON && xhr.responseJSON.error && xhr.responseJSON.error.redirect &&
            xhr.responseJSON.error.redirect.href) {
            redirect = xhr.responseJSON.error.redirect.href;
        }
        if (redirect) {
            window.location = redirect;
        }
    };
    this.getTimeout = function () {
        return opts && opts.timeout ? opts.timeout : 30000;
    }
}

/**
 *
 * @param url A string containing the URL to which the request is sent.
 * @param data Data to be sent to the server. It is converted to a query string, if not already a string. It's appended
 *     to the url for GET-requests. See processData option to prevent this automatic processing. Object must be
 *     Key/Value pairs. If value is an Array, jQuery serializes multiple values with same key based on the value of the
 *     traditional setting.
 * @param firstSuccessHandler A success handler to be executed first
 * @param dataType [default=json] The type of data that you're expecting back from the server. If none is specified,
 *     jQuery will try to infer it based on the MIME type of the response (an XML MIME type will yield XML, in 1.4 JSON
 *     will yield a JavaScript object, in 1.4 script will execute the script, and anything else will be returned as a
 *     string)
 * @param method  [default=POST] The HTTP method to use for the request (e.g. "POST", "GET", "PUT").
 * @param opts  A set of key/value pairs that configure the Ajax request.
 */
Request.send = function (url, data, firstSuccessHandler, dataType, method, opts) {
    var request = new Request(url, data, firstSuccessHandler, dataType, method, opts);
    return request.sendRequest(opts);
};