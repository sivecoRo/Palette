/**
 * User: AlexandruVi
 * Date: 2017-04-27
 */
/**
 * ModelStore
 * @param restMetaModel
 * @constructor
 */
function ModelStore(restMetaModel) {
    var store = {}, metaModel = restMetaModel, cache = {};
    this.getRestMetaModel = function () {
        return metaModel;
    };
    this.get = function (objectUID) {
        if (objectUID && store[objectUID]) {
            return store[objectUID];
        }
        return undefined;
    };
    this.set = function (objectUID, object, updateView) {
        if (object && object.objectUID) {
            store[objectUID] = object;
            if (updateView) {
                EventsManager.getInstance()
                    .fire(EventsManager.VIEW_UPDATE, metaModel.getModelName(), 'update', objectUID, object);
            }
        }
        return this;
    };
    this.create = function (object) {
        if (object && object.objectUID) {
            store[object.objectUID] = object;
            EventsManager.getInstance()
                .fire(EventsManager.VIEW_UPDATE, metaModel.getModelName(), 'create', object.objectUID, object);
        }
        return this;
    };
    this.update = function (object) {
        if (object && object.objectUID) {
            store[object.objectUID] = object;
            EventsManager.getInstance()
                .fire(EventsManager.VIEW_UPDATE, metaModel.getModelName(), 'update', object.objectUID, object);
        }
        return this;
    };
    this.remove = function (objectUID) {
        if (objectUID) {
            store[objectUID] = undefined;
            EventsManager.getInstance().fire(EventsManager.VIEW_UPDATE, metaModel.getModelName(), 'remove', objectUID);
        }
        return this;
    };
    this.findAll = function (filter) {
        var r = $.extend(true, {}, store);
        if (typeof filter === "function") {
            var results = {};
            $.each(r, function (objectUID, item) {
                if (false === filter(objectUID, item)) {
                    return;
                }
                results[objectUID] = item;
            });
            return results;

        }
        return r;
    };
    this.remoteGet = function (objectUID, triggerEvents, bypassCache) {
        var model = this.get(objectUID),
            deferred = $.Deferred();
        if (model && !bypassCache) {
            return deferred.resolveWith(model, [model]).promise(model);
        } else {
            this.remotePageRequest({id: objectUID}, triggerEvents, bypassCache).done(function (page) {
                if (page.content) {
                    $.each(page.content, function (index, item) {
                        if (item.objectUID === objectUID.toString()) {
                            model = item;
                            return false;
                        }
                    });
                }
                deferred.resolveWith(model, [model]);
            });
        }
        return deferred.promise(model);
    };
    this.remotePageRequest = function (requestParams, triggerEvents, bypassCache) {
        var store = this,
            action = (metaModel.getModelName().indexOf('DTO')>=0 ? "/api/web/dto/{endpoint}" : "/api/web/{endpoint}").replace('{endpoint}', metaModel.getModelName()),
            // action = (metaModel.getModelDescriptor().dto ? "/api/web/dto/{endpoint}" : "/api/web/{endpoint}").replace('{endpoint}', metaModel.getModelName()),
            // action = "/api/web/{endpoint}".replace('{endpoint}', metaModel.getModelName()),
            cacheKey = action + ':' + JSON.stringify(requestParams || {}),
            cached = cache[cacheKey],
            firstSuccessHandler = function (page) {
                store.updateStoreData(page, triggerEvents === undefined || triggerEvents, cacheKey);
            };
        if (!!bypassCache || !cached || cached.expiry < Date.now()) {
            return Request.send(action, requestParams, firstSuccessHandler, "json", "GET");
        } else {
            var deferred = $.Deferred();
            return deferred.resolveWith(this, [cached.data]).promise();
        }
    };
    this.remoteSearchRequest = function (searchTerm, viewName, filtersBeforeSearch, triggerEvents, bypassCache) {
        if (true/*metaModel.getModelDescriptor().searchOptions.enabled*/) {
            var searchCriteria = metaModel.getSearchCriteriaForSearchTerm(searchTerm, filtersBeforeSearch, viewName);
            return this.remotePageRequest(searchCriteria, triggerEvents === undefined ? false : triggerEvents, !!bypassCache);
        } else {
            throw new AppException("Search is not enabled for model [" + metaModel.getModelName() + "]");
        }

    };
    this.updateApi = function (modelDescriptor) {
        console.debug("updateModelStore [", metaModel.getModelName(), "]", modelDescriptor);
        metaModel.setModelDescriptor(modelDescriptor);
        this.available = true;
        return this;
    };
    this.updateStoreData = function (page, triggerEvents, cacheKey) {
        var store = this;
        if (cacheKey) {
            cache[cacheKey] = {
                expiry: Date.now() + metaModel.getCacheExpiry(),
                data: page
            };
        }
        $.each(page.content, function (index, object) {
            console.debug("updateModelStoreData [", metaModel.getModelName(), "] #", object.objectUID, " updateView: ", !!triggerEvents);
            store.set(object.objectUID, object, triggerEvents);
        });
    };
    this.remoteInit = function (viewName) {
        var store = this, action = "/api/web/store-init/{endpoint}".replace('{endpoint}', metaModel.getModelName()),
            firstSuccessHandler = function (modelDescriptor) {
                store.updateApi(modelDescriptor);
            };
        if (viewName && viewName !== RestMetaModel.DEFAULT_VIEW_NAME) {
            action += '?viewName=' + viewName;
        }
        return Request.send(action, null, firstSuccessHandler, "json", "GET");
    };
}

ModelStore._registry = {};
ModelStore._remoteInitJobs = {};
ModelStore.getForModel = function (modelName) {
    if (modelName && ModelStore._registry[modelName] instanceof ModelStore) {
        return ModelStore._registry[modelName];
    }
    return undefined;
};
ModelStore.promiseOf = function (modelName, remoteInit, modelDescriptor) {
    var store = ModelStore.getForModel(modelName), deferred = $.Deferred(),
        viewName = modelDescriptor && typeof modelDescriptor === "object" ? modelDescriptor.viewName : null;
    if (store && store.available) {
        if (store.getRestMetaModel().hasModelDescriptorForView(viewName)) {
            return deferred.resolveWith(store, [store]).promise(store);
        }
    }
    if (!store) {
        var metaModel = new RestMetaModel(modelName);
        store = new ModelStore(metaModel);
        store.available = false;
        ModelStore._registry[modelName] = store;
    }
    if (remoteInit) {
        var jobName = modelName + "#" + (viewName || RestMetaModel.DEFAULT_VIEW_NAME),
            job = ModelStore._remoteInitJobs[jobName];
        if (job) {
            return job;
        }
        ModelStore._remoteInitJobs[jobName] = deferred.promise(store);
        store.remoteInit(viewName).done(function () {
            delete ModelStore._remoteInitJobs[jobName];
            deferred.resolveWith(store, [store]);
        });
    } else if (modelDescriptor && typeof modelDescriptor === "object") {
        ModelStore._registry[modelName] = store.updateApi(modelDescriptor);
        deferred.resolveWith(store, [store]);
    }
    return ModelStore._remoteInitJobs[jobName] || deferred.promise(store);
};