function doOnAssessmentItemAdd(event,el) {
    debugger;
    var interactionType = $('#interactionType').val();
    event.preventDefault();
    var $collection = $(el).parents().filter('ul.assessment-item-list');
    var $clone = $collection.children().filter('li.collection-item-template').clone();
    var selectedInteractionType = ''; //TODO ia valoarea din select-ul pt interaction type
    /*if( ! (selectedInteractionType == 'SLIDER_INTERACTION') ) {$clone.find('.slider-interaction').remove();}
    if( ! (selectedInteractionType == 'EXTENDED_TEXT_INTERACTION') ) {$clone.find('.extended-text-interaction').remove();}
    if( ! (selectedInteractionType == 'CHOICE_INTERACTION')) {$clone.find('.choice-interaction').remove();}
    if( ! (selectedInteractionType == 'MULTIPLE_CHOICE_INTERACTION')) {$clone.find('.multiple-choice-interaction').remove();}*/
    var $template = $clone.toggleClass('collection-item-template collection-item');

    var collectionSize = $collection.nearest('.collection-item').length;
    getPreparedTemplate($template).appendTo($collection,collectionSize);
    $(el).off('click');
    EventsManager.getInstance().fire({
        name: EventsManager.HTML_REQUEST,
        selector: $template
    });

}

function getPreparedTemplate($template,collectionSize) {
    //Destroy all plugins
    $template.find('[data-app-plugin]').each(function () {
        resetAppPlugin($(this));
    });
    //Rename all ids and id references of the template
    $template.find('[id]').each(function () {
        var $elementWithId = $(this), currentId = $elementWithId.attr('id'),
            newId = currentId.replace('{#}', collectionSize);
        $elementWithId.attr('id', newId);
        $template.find('label[for="' + currentId + '"]').each(function () {
            var $label = $(this);
            $label.attr('for', newId);
        });
    });
    var $closeButton = $('<a href="#remove-item" class="secondary-content grey-text remove-item right no-ajax"><i class="material-icons">delete</i></a>');
    $closeButton.click(removeItem).appendTo($template);
    return $template;
}

function removeItem(e) {
    e.preventDefault();
    $(e.currentTarget).closest('.collection-item-template, .collection-item').toggleClass('removed-element').hide();
}

function resetAppPlugin($el, html, $controller) {
    var data = $el.data(), controllerData = $controller && $controller.length ? $controller.data() : {};
    if (true) {
        var $selector = data.appSelector === "this" || !data.appSelector ? $el : data.appSelectorRelative ? $el.find(data.appSelector) : $(data.appSelector);
        console.debug("resetAppPlugin", data.appPlugin, "for element:", ($selector.selector || $selector.context));
        switch (data.appPlugin) {
            case "selector":
                $selector.material_select('destroy');
                break;
            default:
                console.warn("No plugin found with name:", data.appPlugin);
                break;
        }
        return $selector;
    }
    return $el;
}