/**
 * Created by LiviuI on 4/4/2017.
 */


var addParticipant = function(event, data){
    var conf = { url: "", params: {} };
    if( data.node.original.type==1 ) {
        conf.url = "/users/get";
        conf.params.id = data.node.original.entityID;
    }

    if( data.node.original.type==1 ) {
        conf.url = "/users/component/list";
        conf.params.id = data.node.original.entityID;
    }

    $.ajax(conf).success(function(){

    });
};

var removeParticipant = function(node){
    console.log(node);
};


var $tree = $('#tree')
    .on("check_node.jstree", addParticipant)
    .on("uncheck_node.jstree", removeParticipant)
    .jstree({
    plugins: ["wholerow", "checkbox"],
    checkbox: {
        tie_selection : false // for checking without selecting and selecting without checking
    },

        core: {
        data: {
            url : function (node) {
                return '/component/tree/' + (node.id != "#"? node.original.entityID:"");
            },
            data: function (node, callback){
                console.log(node);
            }
        }
    }
});

var $list = $('#list');