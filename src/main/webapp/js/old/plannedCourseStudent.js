EventsManager.getInstance().on(EventsManager.HTML_REQUEST, function (event) {
    $('.carousel').carousel({indicators:true});
    if(window.location.search != "" && window.location.search.indexOf("type=")!=-1)
    {
        var status = getStatusFromUrl("type",window.location.search);
        $("#studentCourseFilter").val(status);
        $("#studentCourseFilter > option[value=" + status + "]").attr("selected",true);
    }
    $('select').material_select();

    $("#studentCourseFilter").on('change', function() {
        window.location = "/my-courses/?type="+$(this).val();
    });

});

function getStatusFromUrl( name, url ) {
    if (!url) url = location.href;
    name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
    var regexS = "[\\?&]"+name+"=([^&#]*)";
    var regex = new RegExp( regexS );
    var results = regex.exec( url );
    return results == null ? null : results[1];
}


function changePeriodForStudent(pcsId, pcsStartDate, pcsEndDate)
{
    $('#changePeriodModal').modal().modal('open');
    $("#studentId").val(pcsId);
    $("#startDateSelector").val(pcsStartDate);
    $("#endDateSelector").val(pcsEndDate);
}

$("#saveRunningPeriod").on('submit', function(event){
    event.preventDefault();

    var fData = new FormData();
    $("#saveRunningPeriod input").each(function(index, elm)
    {
        fData.append(elm.name, $(elm).val());
        console.log(elm.name+"="+elm.type+"="+elm.id+"="+$(elm).val());
    });
    if($("#saveRunningPeriod").valid() ) {
            $.ajax({
                url        : '/my-courses/changeUserRunning',
                type       : 'POST',
                data       : fData,
                cache      : false,
                dataType   : 'json',
                processData: false, // Don't process the files
                contentType: false, // Set content type to false as jQuery will tell the server its a query string request
                success    : function (data, textStatus, jqXHR) {
                    if (data.success) {
                        $('#changePeriodModal').modal('close');
                        $('#changePeriodModalSuccess').modal().modal('open');
                    }
                    else
                    {
                        $('#changePeriodModal').modal('close');
                        $('#changePeriodModalError').modal().modal('open');
                    }
                },
                error      : function (data, textStatus, jqXHR) {
                    $('#changePeriodModal').modal('close');
                    $('#changePeriodModalError').modal().modal('open');
                }
        });
    }

});

function closeChangePeriodModalError()
{
    $('#changePeriodModalError').modal('close');
}

function closeChangePeriodModalSuccess()
{
    $('#changePeriodModalSuccess').modal('close');
}

$("#saveRunningPeriod").validate({
    rules: {
        periodStartDate:{
            required: true,
            date: true
        },
        periodEndDate:{
            required: true,
            date: true
        }
    },
    messages: {
        periodStartDate: {
            required: $(".messages [message-attr='student.start.date.required']").text(),
            date: $(".messages [message-attr='student.start.date.not.valid']").text()
        },
        periodEndDate: {
            required: $(".messages [message-attr='student.end.date.required']").text(),
            date: $(".messages [message-attr='student.end.date.not.valid']").text()
        }
    },
    errorElement : 'div',
    errorClass: 'invalid red-text',
    errorPlacement: function(error, element) {
        $('#' + element.attr("id") + '-error').remove();
        var placement = $(element).data('error');
        if (placement) {
            $(placement).append(error)
        } else {
            error.insertAfter(element);
        }
    }
});


