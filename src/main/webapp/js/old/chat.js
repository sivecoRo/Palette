var stompClient = null;
var socket = null;
var username = null;
var userArray = [];
var userDisplayNameArray = [];
var userDisplayIconArray = [];
var newUserArray = [];
var allUsers = [];


function connect() {
	socket = new SockJS('/chatEndPoint');
	stompClient = Stomp.over(socket);
	stompClient.connect('', '', function(frame) {
		username = frame.headers['user-name'];
		
		stompClient.subscribe('/user/queue/messages', function(message) {
			showMessage(JSON.parse(message.body));
		});
		
		stompClient.subscribe('/user/queue/active_users', function(resp) {
			allUsers = resp.body;
			renderUserList(resp.body);
		});

		stompClient.subscribe('/user/queue/navbarNumbers', function(resp) {
			setNavbarNumbers(JSON.parse(resp.body));
		});
		
		stompClient.send('/aelChat/activeUsers', {}, '');
		
		stompClient.send('/aelChat/getNavbarNumbers', {}, '');
	});
}


function disconnect() {
	stompClient.disconnect();
}


function setNavbarNumbers(resp) {
	$( "#mailIconId" ).remove( "#mailNrId" );
	$( "#notificationIconId" ).remove( "#notificationNrId" );
	$("#mail-dropdown").html( "" ) ;
	$("#notification-dropdown").empty().append($('<li class="header"><div class="right"><a href="#"></a></div></li>'));

	if ( resp.unreadMsgNr!="0" ) {
		var elem = $('<span id="mailNrId" class="new badge red" data-badge-caption="" style="position: absolute; top: 20%; margin-left: 20px; ' +
				'padding:0; min-width: 16px; max-width: inherit; height: 16px; line-height: 1.4; text-align: center;">');
		elem.html(resp.unreadMsgNr);
		$("#mailIconId").append(elem);
		
		var list = resp.lastMsg;
		$.each(list, function(index, value) {
			$("#mail-dropdown").append( $('<li><span class="title">' + value.from +'</span><p>' + value.subject + '</p></li>'));
		});
		
	}	
	if ( resp.unreadNotifNr!="0" ) {
		var elem = $('<span id="notificationNrId" class="new badge red" data-badge-caption="" style="position: absolute; top: 20%; margin-left: 20px; ' +
				'padding:0; min-width: 16px; max-width: inherit; height: 16px; line-height: 1.4; text-align: center;">');
		elem.html(resp.unreadNotifNr);
		$("#notificationIconId").append(elem);
		
		var list = resp.lastNotif;
		$.each(list, function(index, value) {
			$("#notification-dropdown").append( $('<li><span class="title">' + value.from +'</span><p>' + value.subject + '</p></li>'));
		});
	}
}


function sendMessage(user) {
	var chatInput = '#input-chat-' + user;
	var message = $(chatInput).val();
	if (!message.length) {
		return;
	}
	
	stompClient.send("/aelChat/sendMessage", {}, JSON.stringify({
		'receiver': user,
		'message' : message,
		'creationDate': new Date()
	}));
	
	$(chatInput).val('');
	$(chatInput).focus();
}

  
function renderUserList(userList) {
	var searchString = $('input#search-user').val();
	if ( !searchString || searchString==undefined || searchString==="" || searchString.length==0 ) {
		searchString = null;
	}
	
	var selectedUser = $('.user-selected').text();
	var usersUnreadMessages = new Object();
	
	$.each($('.unread-messages'), function(index, value) {
		usersUnreadMessages[value.id.substring(5)] = true;
	});
	
	var users = $.parseJSON(userList);

	$('div#users-list .user-entry').remove();
	var usersListElem = $('div#users-list');
	
	$.each(users, function(index, value) {
		if ( searchString==null || String(value.name).toLowerCase().indexOf(searchString.toLowerCase())!==-1 ) {
		
			if ( userArray.indexOf(value.username)===-1 ) {
				userArray.push(value.username);
				userDisplayNameArray.push(value.name);
				newUserArray.push(value.username);
			}
			
			if (value.username === username) {
				return true;
			}
			
			var userElem = $('<div>', {id: 'user-' + value.username});
			userElem.addClass('user-entry');
			
			if (selectedUser === value.username) {
				userElem.addClass('user-selected');
			}
			else {
				userElem.addClass('user-unselected');
			}
			
			var userNameElem = $('<span>');
			userNameElem.html(value.name);
			
			if ( !value.active ) {
				userNameElem.addClass('disabled-elem');
				userElem.attr('title', $(".messages [message-attr='chat.user.offline']").text());
			}
			if ( value.hasImage ) {
				userDisplayIconArray.push('<img src="/user/profile/get_user_image/'+value.username+'" alt="" class="circle responsive-img" style="width: 40px; height: 40px";>');
				userElem.append($('<img src="/user/profile/get_user_image/'+value.username+'" alt="" class="circle responsive-img" style="width: 40px; height: 40px";>'));
			}
			else {
				userDisplayIconArray.push('<img src="/img/generic/generic-user.png" alt="" class="circle responsive-img" style="width: 40px; height: 40px";>');
				userElem.append($('<img src="/img/generic/generic-user.png" alt="" class="circle responsive-img" style="width: 40px; height: 40px";>'));
			}
			
			userElem.append(userNameElem);
	
			if ( value.active ) {
				userElem.click(function() {
					$('.chat-container').hide();
					$('.user-entry').removeClass('user-selected');
					$('.user-entry').addClass('user-unselected');
					
					userElem.removeClass('user-unselected');
					userElem.removeClass('unread-messages');
					userElem.addClass('user-selected');
					userElem.children('.unread-message').remove();
					
					var chatWindow = getChatWindow(value.username);
					chatWindow.show();
					
					chatWindow.find('.chat-input').focus();
					
					
					if ( newUserArray.indexOf(value.username)>-1 ) {
						newUserArray.splice(newUserArray.indexOf(value.username), 1);
						
						stompClient.send('/aelChat/getMessages', {}, JSON.stringify({
							'receiver': value.username,
							'sender' : username
						}));
					}
				});
			}
			
			if (value.username in usersUnreadMessages) {
				userElem.append(unreadMessage());
				userElem.addClass('unread-messages');
			}
			
			usersListElem.append(userElem);
		}
	});
}


function unreadMessage() {
	var unreadMsg = $('<span>', {class: 'unread-message'});
	unreadMsg.html('&#x2709;');
	return unreadMsg;
}
 

function getChatWindow(userName) {
	var chatContainerList = $('.chat-container');
	var elementId = 'chat-' + userName;
	var containerId = elementId + '-container';
	var selector = '#' + containerId;
	var inputId = 'input-' + elementId;
	
	if (!$(selector).length) {
		var chatContainer = $('<div>', {id: containerId, class: 'chat-container'});
		var chatWindow = $('<div>', {id: elementId, class: 'chat-window'});
		var chatInput = $('<input>', {id: inputId, type: 'text', class: 'chat-input active',  
			placeholder: 'Type a message...'});
        var chatSubmit = $('<button>', {id: 'submit-' + elementId, type: 'submit', class: 'chat-submit'});
		chatSubmit.html($(".messages [message-attr='chat.send.button']").text());
  
		chatInput.keypress(function(event) {
			if (event.which == 13) {
				var user = event.currentTarget.id.substring(11);
				event.preventDefault();
				sendMessage(user);
			}
		});
  
		chatSubmit.click(function(event) {
			var user = event.currentTarget.id.substring(12);
			sendMessage(user);
		});
  
		chatContainer.append(chatWindow);
		
		var chatInputSubmit = $('<div>', {id: containerId, class: 'chat-input-submit'});
		chatInputSubmit.append(chatSubmit);
		var chatInputDiv = $('<div>', {class: 'chat-input-div'});
		chatInputDiv.append(chatInput);
		chatInputSubmit.append(chatInputDiv);
		chatContainer.append(chatInputSubmit);

		if (chatContainerList.length) {
			chatContainer.hide();
		}
		
		$('#chat-body').append(chatContainer);
	}
    
	return $(selector);
}


function showMessage(message) {
	var chatWindowTarget = (message.receiver === username) ? message.sender : message.receiver;
	
	if ( newUserArray.indexOf(chatWindowTarget)===-1 ) {
		var chatContainer = getChatWindow(chatWindowTarget);
		var chatWindow = chatContainer.children('.chat-window');

		var messageDiv = $('<div>', {class: (message.sender === username ? 'chat-sender' : 'chat-receiver')});

		var userNameElem = $('<img src="/user/profile/get_user_image/'+message.sender+'" alt="" class="circle responsive-img" style="width: 40px; height: 40px";>');
		userNameElem.attr('title', userDisplayNameArray[userArray.indexOf(message.sender)] + '('+ moment(message.creationDate).format("DD-MMM-YYYY h:mm:ss") +')');

		var messageElem = $('<span>');
		messageElem.html(message.message);
		
		if ( message.sender === username ) {
			messageDiv.append(messageElem).append(' : ').append(userNameElem);
		}
		else {
			messageDiv.append(userNameElem).append(' : ').append(messageElem);
		}
		chatWindow.append(messageDiv).append('<br/><hr/><br/>');
		
		chatWindow.animate({ scrollTop: chatWindow[0].scrollHeight}, 1);
	}
	
	if (message.sender !== username) {
		var sender = $('#user-' + message.sender);
		if (!sender.hasClass('user-selected') && !sender.hasClass('unread-messages')) {
			sender.append(unreadMessage());
			sender.addClass('unread-messages');
		}
	}
}


$('input#search-user').on("change keyup paste", function () {
	renderUserList(allUsers);
});

$('#users-list a.btn-floating').on("click", function () {
	$('input#search-user').val("");
	renderUserList(allUsers);
});


$(document).ready(function() {
	connect();
});