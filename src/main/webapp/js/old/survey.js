$(".associateUsersForSurvey").on("click", function(e){
    e.preventDefault();
    $('select#usersForSurveySelector').empty();
    var currentMaterialId = e.toElement.attributes.materialId.value;
    $(".saveUsersForSurvey").attr("currentMaterialId", currentMaterialId);
    $.ajax({
        type: 'GET',
        url: '/surveys/getStudentsForSurvey/' + currentMaterialId,
        cache: false,
        processData: false,
        contentType: "application/json",
        success: function (response) {

            //Set the selector data
            var usersForSurveySelector = $('select#usersForSurveySelector');
            $.each(response.studentsList, function(i, plannedCourseStudent) {
                var auxOption = $('<option></option>');
                auxOption.val(plannedCourseStudent.entityId).text(plannedCourseStudent.entityName);
                $(auxOption).data("plannedCourseStudentId", plannedCourseStudent.id);
                $(auxOption).data("plannedCoursesStudentUserId", plannedCourseStudent.entityId);
                $(auxOption).data("isStudent", plannedCourseStudent.student);
                if(plannedCourseStudent.selected){
                    $(auxOption).attr('selected' ,'selected');
                }
                if(plannedCourseStudent.mustDisable){
                    $(auxOption).attr('disabled' ,true);
                }
                usersForSurveySelector.append(auxOption);
            });

            $(".select-dropdown").css("display","none");
            usersForSurveySelector.select2().on("select2:unselecting", function (e) {
                if(e.params.args.data.disabled){
                    e.preventDefault();
                }
            });

            //Open the modal
            var usersForSurveyModal = $('#usersForSurveyModal');
            usersForSurveyModal.modal();
            usersForSurveyModal.modal('open');
        },
        error: function (response) {
            alert(response.responseJSON.message);
        }
    });
});

$(".modal-footer").on("click",".closeAssociateUsersForSurvey", function(e) {
    e.preventDefault();
    $('#usersForSurveyModal').modal('close');
});

$(".modal-footer").on("click",".saveUsersForSurvey", function(e) {
    e.preventDefault();

    var objectToSave = {};
    var plannedCourseStudentDataList = [];
    $("#usersForSurveySelector > option").each(function(index, elem) {
        var plannedCoursesStudentId = $(this).data('plannedCourseStudentId');
        var plannedCoursesStudentUserId = $(this).data('plannedCoursesStudentUserId');
        var isStudent = $(this).data('isStudent');
        var userIsNowSelected = this.selected;
        /* <![CDATA[ */
        if((!plannedCoursesStudentId && userIsNowSelected) || (plannedCoursesStudentId && !userIsNowSelected)){
            /* ]]> */
            var currentStudentData = {};
            currentStudentData.id = plannedCoursesStudentId;
            currentStudentData.entityId = plannedCoursesStudentUserId;
            currentStudentData.selected = userIsNowSelected;
            currentStudentData.entityName = this.text;
            currentStudentData.isStudent = isStudent;
            plannedCourseStudentDataList.push(currentStudentData);
        }
    });
    objectToSave["plannedCourseStudentDataList"] = plannedCourseStudentDataList;
    objectToSave["surveyId"] = e.toElement.attributes.currentMaterialId.value;

    $.ajax({
        type: 'POST',
        url: '/surveys/addStudentsForSurvey',
        data : JSON.stringify(objectToSave),
        cache: false,
        processData: false,
        contentType: "application/json",
        success: function (response) {
            $('#usersForSurveyModal').modal('close');
            $('#participantsSavedSuccessfully').modal().modal('open');
        },
        error: function (response) {
            alert(response.responseJSON.message);
        }
    });

});

function closeModalParticipantsSavedSuccessfully() {
    $('#participantsSavedSuccessfully').modal('close');
}