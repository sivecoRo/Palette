/**
 * Created by LiviuI on 4/27/2017.
 */


SearchBar = (function($){

    var extractConf = function($box){
        var conf = {};
        conf = $.extend(conf, {path: $box.data("path")});
        conf = $.extend(conf, {value: $box.data("value")});
        conf = $.extend(conf, {search: $box.data("search")});
        conf = $.extend(conf, {fields: $box.data("fields").replace(" ", "").split(",")});
        return conf;
    };

    var transform = function( target, field, data ){
        var f = field.split(":");
        if( f[1] == "icon" ){
            target["icon"] = data[f[0]];
        }
    };

    var createStore = function( conf ) {
        var ajaxConfig = {url : conf.path + "?" + conf.search + "="};
        return ( function(){
            var _config = $.extend({}, ajaxConfig);
            var search = function( key, callback ){
                var config = $.extend({}, _config);
                config.url+=key;
                $.ajax(config)
                    .success( function(response) {
                        var rows = response.content.map(function(row){
                            var r={};
                            r[conf.value] = row[conf.value];
                            r.label = "";
                            $.each(conf.fields, function(i, field){
                                if( field.indexOf(":")>-1){
                                    transform(r, field, row);
                                }else{
                                    r.label += row[field]+ ' ';
                                }
                            });
                            return r;
                        });
                        callback && callback.call(this, rows);
                    })
                    .error( function(XMLHttpRequest, textStatus, errorThrown){
                        toastr && toastr.error(XMLHttpRequest.responseText + "<br/><button type='button' class='btn btn-flat clear'>Close</button>");
                        conf && conf.error && conf.error.call();
                    });
            };
            return {
                search: search
            };
        })();
    };

    $.fn.searchbox = function (options) {
        // Defaults
        var defaults = {
            data: {},
            limit: Infinity,
            onAutocomplete: null,
            minLength: 1
        };

        options = $.extend(defaults, options);

        return this.each(function () {
            var $input = $(this);
            var count = 0,
                activeIndex = -1,
                oldVal,
                $inputDiv = $input.closest('.input-field'); // Div to append on


            var conf = extractConf($input);
            var store = createStore(conf);

            /* append hidden input field with reqired name*/
            var $valueInput = $("<input type='hidden' name='" + conf.value + "'/>");
                $input.parent().append($valueInput);

            var $icon;
            var $iconDefault='<i class="material-icons small icon">portrait</i>';

            // append icon field
            if( conf.fields && conf.fields.indexOf("icon")){
                $icon=$('<span>' + $iconDefault + '</span>');
                $input.parent().prepend($icon);
                $input.css("width", "auto");
            }

            var $oldAutocomplete, $autocomplete = $('<ul class="autocomplete-content dropdown-content"></ul>');

            // Append autocomplete element.
            // Prevent double structure init.
            if ($inputDiv.length) {
                $oldAutocomplete = $inputDiv.children('.autocomplete-content.dropdown-content').first();
                if (!$oldAutocomplete.length) {
                    $inputDiv.append($autocomplete); // Set ul in body
                }
            } else {
                $oldAutocomplete = $input.next('.autocomplete-content.dropdown-content');
                if (!$oldAutocomplete.length) {
                    $input.after($autocomplete);
                }
            }
            if ($oldAutocomplete.length) {
                $autocomplete = $oldAutocomplete;
            }

            // Highlight partial match.
            var highlight = function (string, $el) {
                var img = $el.find('img');
                var matchStart = $el.text().toLowerCase().indexOf("" + string.toLowerCase() + ""),
                    matchEnd = matchStart + string.length - 1,
                    beforeMatch = $el.text().slice(0, matchStart),
                    matchText = $el.text().slice(matchStart, matchEnd + 1),
                    afterMatch = $el.text().slice(matchEnd + 1);
                $el.html("<span>" + beforeMatch + "<span class='highlight'>" + matchText + "</span>" + afterMatch + "</span>");
                if (img.length) {
                    $el.prepend(img);
                }
            };

            // Reset current element position
            var resetCurrentElement = function () {
                activeIndex = -1;
                $autocomplete.find('.active').removeClass('active');
            };

            // Remove autocomplete elements
            var removeAutocomplete = function () {
                $autocomplete.empty();
                resetCurrentElement();
                oldVal = undefined;
            };

            $input.off('blur.autocomplete').on('blur.autocomplete', function () {
                removeAutocomplete();
            });

            // Perform search
            $input.off('keyup.autocomplete focus.autocomplete').on('keyup.autocomplete focus.autocomplete', function (e) {
                // Reset count.
                count = 0;
                var val = $input.val().toLowerCase();

                // Don't capture enter or arrow key usage.
                if (e.which === 13 || e.which === 38 || e.which === 40) {
                    return;
                }


                // Check if the input isn't empty
                if (oldVal !== val) {
                    removeAutocomplete();

                    if (val.length >= options.minLength) {
                        store.search(val, function(rows){
                            $.each(rows, function(i,row){
                                var autocompleteOption = $('<li></li>');
                                if (row.icon) {
                                    autocompleteOption.append('<img src="' + row["icon"]+ '" class="right circle"><span>' + row.label + '</span>');
                                } else {
                                    autocompleteOption.append('<span>' + row.label + '</span>');
                                }
                                $autocomplete.append(autocompleteOption);
                                autocompleteOption.data("record", row);
                                highlight(val, autocompleteOption);
                                count++;
                            })
                        });
                    }
                }
                // Update oldVal
                oldVal = val;
            });

            $input.off('keydown.autocomplete').on('keydown.autocomplete', function (e) {
                // Arrow keys and enter key usage
                var keyCode = e.which,
                    liElement,
                    numItems = $autocomplete.children('li').length,
                    $active = $autocomplete.children('.active').first();

                // select element on Enter
                if (keyCode === 13 && activeIndex >= 0) {
                    liElement = $autocomplete.children('li').eq(activeIndex);
                    if (liElement.length) {
                        liElement.trigger('mousedown.autocomplete');
                        e.preventDefault();
                    }
                    return;
                }

                // Capture up and down key
                if (keyCode === 38 || keyCode === 40) {
                    e.preventDefault();

                    if (keyCode === 38 &&
                        activeIndex > 0) {
                        activeIndex--;
                    }

                    if (keyCode === 40 &&
                        activeIndex < (numItems - 1)) {
                        activeIndex++;
                    }

                    $active.removeClass('active');
                    if (activeIndex >= 0) {
                        $autocomplete.children('li').eq(activeIndex).addClass('active');
                    }
                }
            });

                // Set input value
            $autocomplete.on('mousedown.autocomplete touchstart.autocomplete', 'li', function () {
                var $item = $(this);
                var $record = $item.data("record");
                var text = $item.text().trim();
                $input.val(text);
                $valueInput.val($record.id);
                if( $record && $record.icon ){
                    $icon.html('<img class="small icon" src="'+ $record.icon +'"/>');
                }else{
                    $icon.html($iconDefault);
                }
                $input.trigger('change');
                removeAutocomplete();

                // Handle onAutocomplete callback.
                if (typeof(options.onAutocomplete) === "function") {
                    options.onAutocomplete.call(this, text);
                }
            });
        });
    }

})(jQuery);