$(function () {
    $(".dropdown-button").dropdown({
        inDuration: 300,
        outDuration: 225,
        constrainWidth: false, // Does not change width of dropdown to that of the activator
        hover: true, // Activate on hover
        gutter: 0, // Spacing from edge
        belowOrigin: true, // Displays dropdown below the button
        alignment: 'left', // Displays dropdown with edge aligned to the left of button
        stopPropagation: false // Stops event propagation
    });


    $(document).on("ready", function(){
        $('select').material_select();
        $('.advanced-menu').collapsible();
        //$('.upload-image-field').uploadPreview({
        //    input_field: "#image-upload",   // Default: .image-upload
        //    preview_box: "#image-preview",  // Default: .image-preview
        //    label_field: "#image-label",    // Default: .image-label
        //    label_default: "Choose File",   // Default: Choose File
        //    label_selected: "Change File",  // Default: Change File
        //    no_label: false                 // Default: false
        //});



    });
});