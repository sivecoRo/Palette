
var persist = function (config) {
    var $form = $(config.form);
    var createMode = isNaN($form.get("id"));
    var data = $.extend({}, config.data);
    $.each( $form.serializeArray(), function(i, field){ data[field.name] = field.value || null;});

    $.ajax({
        type : createMode? 'POST':'PUT',
        url : $form.attr("action") + ( createMode? "" : $form.getId()),
        data : JSON.stringify(data),
        cache : false,
        processData : false,
        contentType : "application/json",
        success : function(response) {
            config.success && config.success.call();
        },
        error : function(XMLHttpRequest, textStatus, errorThrown) {
            toastr && toastr.error(XMLHttpRequest.responseText + "<br/><button type='button' class='btn btn-flat clear'>Close</button>");
        }
    });
};

$(document).on("ready", function(){

    var ACTION = {
        ADD : 'ADD_MATERIAL',
        REMOVE: 'REMOVE_MATERIAL',
        MOVE: 'MOVE_MATERIAL'
    };

    var repository =$("#repository-view");
    var repositoryTemplate = Handlebars.compile($("#file-template").html());

    var tableOfContents = $("#table-of-contents");
    var tableOfContentsTemplate = Handlebars.compile($("#material-template").html());

    var save = $("#save-course");


    Handlebars.registerHelper('fileIconType', function(options) {
        var map = { LESSON: 'local_library', TEST : 'playlist_add_check', TEST_PATTERN : 'playlist_add_check', SURVEY : 'pie_chart', FILE : 'library_books'}
        return map[options.data.root.learningMaterial.materialType];
    });

    /* */

    var materialReducer = function(state, action){
        if (typeof state === 'undefined') {
            var href = window.location.href;
            var id = href.substr(href.lastIndexOf("/")+1);
            if( !isNaN(id)){
                var course = App.getInstance().getStore("course").get(id);
                if( course && course.courseRlos )
                    return course.courseRlos.slice();
            }
            return [];
        }
        switch (action.type) {
            case ACTION.ADD:
                if( state.indexOf(action.data) >= 0)
                    return state;
                var newState = state.slice(0);
                newState.push(action.data);
                return newState;
            case ACTION.REMOVE:
                var newState = Array.from(state);
                newState.splice(state.indexOf(action.data),1);
                return newState;
            case ACTION.MOVE:
                return state - 1;
            default:
                return state
        }
    };

    var courseMaterialStore = Redux.createStore(materialReducer);

    var render = function(){
        tableOfContents.empty();
        $.each(courseMaterialStore.getState(), function(i, material){
            var $element = $(tableOfContentsTemplate(material));
            $element.data("record", material);
            tableOfContents.append( $element );
        });
    };
    render();
    courseMaterialStore.subscribe(render);

    repository.on("click", ".card", function(e){
        e.preventDefault();
        var data  = $(e.currentTarget).data("record");
        courseMaterialStore.dispatch($.extend({}, {type: ACTION.ADD}, {data: data}));
    });

    tableOfContents.on("click", ".collection-item", function(e){
        e.preventDefault();
        var data  = $(e.currentTarget).data("record");
        courseMaterialStore.dispatch($.extend({}, {type: ACTION.REMOVE}, {data: data}));
    });

    save.on("click", function(){
        var courseRlos = [];
        $.each(courseMaterialStore.getState(), function(i, item){
            courseRlos.push({rloIndex: i, learningMaterial: item});
        });
        persist({ form: "#form", data: {courseRlos: courseRlos}, success : function(){
            toastr.success("saved");
        }});
    });

    //load by ajax:
    $.ajax({ url: "/repository/materials/"}).success(function(response, status) {
        $.each(response.content, function (i, row) {
            var $row= $(repositoryTemplate(row));
            $row.data( "record",  row);
            repository.append( $row );
        });
    });
});