$(document).ready(function() {
    $('select').material_select();
    $('ul.tabs').tabs();

    $("#createTenantAdmin").on('submit', function(event){
       event.preventDefault();

        var tenantId= $("#tenantId").val();

        var user= {};
        var fData = new FormData();
        $.each($(this).serializeArray(), function( i, element){
            fData.append(element.name, element.value);
        });

        fData.append("tenantId", tenantId);

        if ( $("#createTenantAdmin").valid() ) {
            $.ajax({
                type: 'POST',
                url: "/tenants/createTenantAdministrator",
                data: fData,
                dataType: 'json',
                contentType: false,
                processData: false,
                success: function (data, textStatus, jqXHR) {
                    if (data.success) {
                        if (data.tenantAdmin != undefined) {
                            $("#adminUsername").val(data.tenantAdmin.username);
                            $("#adminFirstName").val(data.tenantAdmin.firstName);
                            $("#adminLastName").val(data.tenantAdmin.lastName);
                            $("#adminEmail").val(data.tenantAdmin.email);
                            $('#modalAdminCreatedSuccessful').modal().modal('open');
                        }
                    }
                    else {
                        if (data.mailNotUnique) {
                            $('#modalAdminMailNotUniqueError').modal().modal('open');
                        }
                        else {
                            $("#adminUsername").val();
                            $("#adminFirstName").val();
                            $("#adminLastName").val();
                            $("#adminEmail").val();
                            $('#modalAdminCreatedError').modal().modal('open');
                        }
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    console.log("ERRORRRR");
                }
            });
        }

    });

    $("#createMailConfig").on('submit', function(event){
        event.preventDefault();

        var tenantId= $("#tenantId").val();

        var user= {};
        var fData = new FormData();
        $.each($(this).serializeArray(), function( i, element){
            fData.append(element.name, element.value);
        });

        var keyName = $("#keyId").val();
        var keyValue = $("#valueId").val();

        fData.append("tenantId", tenantId);
        fData.append("mailAttributeKey", keyName);
        fData.append("mailAttributeVal", keyValue);

        if ( $("#createMailConfig").valid() ) {
            $.ajax({
                type: 'POST',
                url: "/tenants/createTenantMailConfig",
                data: fData,
                dataType: 'json',
                contentType: false,
                processData: false,
                success: function (data, textStatus, jqXHR) {
                    if (data.success) {
                        if (data.mailConfigResult != undefined) {
                            $("#mailHost").val(data.mailConfigResult.host);
                            $("#mailPort").val(data.mailConfigResult.port);
                            $("#mailUsername").val(data.mailConfigResult.username);
                            $("#mailPassword").val(data.mailConfigResult.password);
                            $("#mailEncoding").val(data.mailConfigResult.defaultEncoding);
                            $("#keyId").val(data.mailConfigAttribResult.cheie);
                            $("#valueId").val(data.mailConfigAttribResult.value);
                            $('#modalMailCfgCreatedSuccessful').modal().modal('open');
                        }
                    }
                    else {
                        $("#mailHost").val();
                        $("#mailPort").val();
                        $("#mailUsername").val();
                        $("#mailPassword").val();
                        $("#mailEncoding").val();
                        $("#keyId").val();
                        $("#valueId").val();
                        $('#modalMailCfgCreatedError').modal().modal('open');
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    console.log("ERRORRRR");
                }
            });
        }
    });

    function closeModalAdminSuccessful() {
        $('#modalAdminCreatedSuccessful').modal('close');
    }

    function closeModalAdminError() {
        $('#modalAdminCreatedError').modal('close');
    }

    function closeModalAdminMailNotUniqueError() {
        $('#modalAdminMailNotUniqueError').modal('close');
    }

    function closeModalMailCfgCreatedSuccessful() {
        $('#modalMailCfgCreatedSuccessful').modal('close');
    }

    function closeModalMailCfgCreatedError() {
        $('#modalMailCfgCreatedError').modal('close');
    }


    $("#tenantCreateForm").validate({
        rules: {
            name:{
                required: true
            },
            phone:{
                required: true
            },
            fax:{
                required: true
            },
            email: {
                required: true,
                email:true
            },
            address: {
                required: true
            }
        },
        messages: {
            name: {
                required: $(".messages [message-attr='tenant.name.required']").text()
            },
            phone: {
                required: $(".messages [message-attr='tenant.phone.required']").text()
            },
            fax: {
                required: $(".messages [message-attr='tenant.fax.required']").text()
            },
            email: {
                required: $(".messages [message-attr='register.email.required']").text(),
                email: $(".messages [message-attr='register.email.required.valid']").text()
            },
            address: {
                required: $(".messages [message-attr='tenant.address.required']").text()
            }
        },
        errorElement : 'div',
        errorClass: 'invalid red-text',
        errorPlacement: function(error, element) {
            $('#' + element.attr("id") + '-error').remove();
            var placement = $(element).data('error');
            if (placement) {
                $(placement).append(error)
            } else {
                error.insertAfter(element);
            }
        }
    });


    $("#createTenantAdmin").validate({
        rules: {
            username:{
                required: true
            },
            firstName:{
                required: true
            },
            lastName:{
                required: true
            },
            email: {
                required: true,
                email:true
            }
        },
        messages: {
            username:{
                required: $(".messages [message-attr='tenant.admin.username.required']").text()
            },
            firstName:{
                required: $(".messages [message-attr='tenant.admin.firstname.required']").text()
            },
            lastName:{
                required: $(".messages [message-attr='tenant.admin.lastname.required']").text()
            },
            email: {
                required: $(".messages [message-attr='register.email.required']").text(),
                email: $(".messages [message-attr='register.email.required.valid']").text()
            }
        },
        errorElement : 'div',
        errorClass: 'invalid red-text',
        errorPlacement: function(error, element) {
            $('#' + element.attr("id") + '-error').remove();
            var placement = $(element).data('error');
            if (placement) {
                $(placement).append(error)
            } else {
                error.insertAfter(element);
            }
        }
    });

    $("#createMailConfig").validate({
        rules: {
            host:{
                required: true
            },
            port:{
                required: true,
                min: 0

            },
            username:{
                required: true
            },
            password: {
                required: true
            }
        },
        messages: {
            host:{
                required: $(".messages [message-attr='tenant.mail.host.required']").text()
            },
            port:{
                required: $(".messages [message-attr='tenant.mail.port.required']").text(),
                min: $(".messages [message-attr='tenant.mail.port.valid']").text()
            },
            username:{
                required: $(".messages [message-attr='tenant.mail.username.required']").text()
            },
            password: {
                required: $(".messages [message-attr='tenant.mail.password.required']").text()
            }
        },
        errorElement : 'div',
        errorClass: 'invalid red-text',
        errorPlacement: function(error, element) {
            $('#' + element.attr("id") + '-error').remove();
            var placement = $(element).data('error');
            if (placement) {
                $(placement).append(error);
            } else {
                error.insertAfter(element);
            }
        }
    });

    $("#adminEmail").on('blur', function(event) {
        var url = '/checkEmail?email=' + $(this).val();

        $.ajax({
            type : 'GET',
            url : url,
            cache : false,
            processData : false,
            contentType : "application/json",
            success : function(response) {
                var returnedData = $.parseJSON(response);
                if( returnedData.success === "false") {
                    $('#email-error').remove();
                    var placement = $("#adminEmailLabel").data('error');
                    var error = $('<div id="email-error" class="invalid red-text" th:utext="#{registration.failed.email}">Email already in use</div>');

                    if (placement) {
                        $(placement).append(error)
                    } else {
                        error.insertAfter($("#adminEmail"));
                    }

                    $('#adminEmail').removeClass("valid");
                    $("#adminEmail").addClass("invalid red-text");
                }
            },
            error : function(XMLHttpRequest, textStatus, errorThrown) {
                alert($(".messages [message-attr='registration.check.email.error']").text());
            }
        });
    });

    $("#tenantName").on('blur', function(event) {
        var url = '/tenants/checkTenantName?name=' + $(this).val();

        $.ajax({
            type : 'GET',
            url : url,
            cache : false,
            processData : false,
            contentType : "application/json",
            success : function(response) {
                var returnedData = $.parseJSON(response);
                if( returnedData.success === "false") {
                    $('#name-error').remove();
                    var placement = $("#tenantNameLabel").data('error');
                    var error = $('<div id="name-error" class="invalid red-text" th:utext="#{registration.failed.email}">Email already in use</div>');

                    if (placement) {
                        $(placement).append(error)
                    } else {
                        error.insertAfter($("#tenantName"));
                    }

                    $('#tenantName').removeClass("valid");
                    $("#tenantName").addClass("invalid red-text");
                }
                else
                {
                    $('#name-error').remove();
                }
            },
            error : function(XMLHttpRequest, textStatus, errorThrown) {
                alert($(".messages [message-attr='registration.check.email.error']").text());
            }
        });
    });

    var imageToUpload;
    $("input.editThumbnailSelector").on('change', uploadThumbnailOnEdit);

    function uploadThumbnailOnEdit(event) {
        event.stopImmediatePropagation();
        var file = event.target.files[0];
        if(!isFileValid(file)) {
            return;
        }

        imageToUpload=file;
        readURL(this);
    }

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#tenantImg').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#tenantCreateForm").on('submit', function(e){
        e.preventDefault();
        var tenantCreateForm = $("#tenantCreateForm");

        var fData = new FormData();
        $("#tenantCreateForm input, #tenantCreateForm textarea, #tenantCreateForm select").each(function(index, elm){
            if(elm.type=="checkbox")
            {
                fData.append(elm.name, $(elm).prop("checked"));
                console.log(elm.name+"="+elm.type+"="+elm.id+"="+$(elm).prop("checked"));
            }
            else
            {
                fData.append(elm.name, $(elm).val());
                console.log(elm.name+"="+elm.type+"="+elm.id+"="+$(elm).val());
            }
        });
        fData.append("imageToUpload", imageToUpload);

        if ( $("#tenantCreateForm").valid() ) {
            $.ajax({
                url: '/tenants/addTenant',
                type: 'POST',
                data: fData,
                cache: false,
                dataType: 'json',
                processData: false, // Don't process the files
                contentType: false, // Set content type to false as jQuery will tell the server its a query string request
                success: function (data, textStatus, jqXHR) {
                    if (data.success) {
                        if (data.tenantCreatedId != undefined) {
                            window.location = "/tenants/edit/" + data.tenantCreatedId;
                        }
                    }
                    else
                    {
                       if(data.cause=='existing_name')
                       {
                           $('#modalTenantCreatedError').modal().modal('open');
                       }
                    }
                },
                error: function (data, textStatus, jqXHR) {
                    console.log("errrrorrrrr");
                }
            });
        }
    });

    function closeModalTenantCreatedError()
    {
        $('#modalTenantCreatedError').modal('close');
    }


    $("#updateTenantId").on('submit', function(e){
        console.log("A intrat!!!");
        e.preventDefault();
        var tenantCreateForm = $("#updateTenantId");

        var fData = new FormData();
        $("#updateTenantId input, #updateTenantId textarea, #updateTenantId select").each(function(index, elm){
            if(elm.type=="checkbox")
            {
                fData.append(elm.name, $(elm).prop("checked"));
                console.log(elm.name+"="+elm.type+"="+elm.id+"="+$(elm).prop("checked"));
            }
            else
            {
                fData.append(elm.name, $(elm).val());
                console.log(elm.name+"="+elm.type+"="+elm.id+"="+$(elm).val());
            }
        });

        $.ajax({
            url: '/tenants/updateTenant',
            type: 'POST',
            data: fData,
            cache: false,
            dataType: 'json',
            processData: false, // Don't process the files
            contentType: false, // Set content type to false as jQuery will tell the server its a query string request
            success: function (data, textStatus, jqXHR) {
                if(data.success)
                {
                    window.location="/tenants/";
                }
            },
            error:function (data, textStatus, jqXHR) {
                console.log("errrrorrrrr");
            }
        });
    });


    function isFileValid(file){
        var fileType = file["type"];
        var acceptedTypes = ["image/jpg", "image/png", "image/jpeg", "image/gif"];
        if ($.inArray(fileType, acceptedTypes) < 0) {
            alert($(".messages [message-attr='user.profile.image.wrong-type.error']").text());
        }

        if (file.size > 10000000) {
            alert($(".messages [message-attr='user.profile.image.too-big.error']").text());
        }
        return true;
    }

    $("input.editThumbnailUpdateTenant").on('change', uploadThumbnailOnUpdateTenant);

    function uploadThumbnailOnUpdateTenant(event) {
        event.stopImmediatePropagation();
        var file = event.target.files[0];
        if(!isFileValid(file)) {
            return;
        }

        var tenantId= $("#tenantId").val();
        var data = new FormData();
        data.append("tenantId", tenantId);
        data.append("image", file);

        $.ajax({
            url: '/tenants/updateImage',
            type: 'POST',
            data: data,
            cache: false,
            dataType: 'json',
            processData: false, // Don't process the files
            contentType: false, // Set content type to false as jQuery will tell the server its a query string request
            success: function (data, textStatus, jqXHR) {
                console.log("--------added");
                var dc = "?_dc=" + new Date().getTime();
                if(data.success) {

                    if(data.hadImage)
                    {
                        $("#tenantImageOnUpdate").attr("src", "/tenants/getImage/" + $("#tenantId").val()+dc);
                    }
                    else
                    {
                        $("#tenantImg").attr("src", "/tenants/getImage/" + $("#tenantId").val()+dc);
                    }
                }

            },
            error: function (data, textStatus, jqXHR) {
                console.log("--------error");
            }
        });
    }

});