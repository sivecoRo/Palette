function rateCourse(plannedCourseStudentId, rateVal)
{
    var rate={};
    rate["ratingValue"]= rateVal;
    rate["plannedCourseStudentId"]=plannedCourseStudentId;

    $.ajax({
        type : 'POST',
        url :  '/my-courses/rateCourse',
        data: rate,

        success : function(data, textStatus, jqXHR) {
            if(data.success)
            {
                if(data.studentRating!=null){
                    $('input[name="over_rating"][value="' + data.studentRating + '"]').prop("checked", true);
                }
                else
                {
                    for(var i=0;i<=5;i+=0.5){
                        $('input[name="over_rating"][value="' + i + '"]').prop("checked", false);
                    }
                }
                $('#modalFeedbackSuccessful').modal().modal('open');
            }
        },
        error : function(XMLHttpRequest, textStatus, errorThrown) {
            console.log($(".messages [message-attr='plannedCourseStudent.not.found.error']").text());
        }
    });
}

function closeModalFeedbackSuccessful() {
    $('#modalFeedbackSuccessful').modal('close');
}

$('#ratingOptions').dropdown({
    constrain_width: false,
    hover: true
});


$(document).ready(function(){
    var enrollMsg = $("#userEnrolled").val();
    if(enrollMsg != undefined && enrollMsg != "")
    {
        $('#modalEnrollmentSuccessful').modal().modal('open');
    }
});

function closeModalEnrollmentSuccessful() {
    $('#modalEnrollmentSuccessful').modal('close');
}
