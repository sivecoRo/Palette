package ro.siveco.ael.web.interceptors;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import ro.siveco.ael.web.Cookies;
import ro.siveco.ael.web.components.WebPath;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Optional Interceptor setting a default cookie for cookie policy user agreement.
 * It also checks if the current request URI is whitelisted from showing the cookie policy pop-up.
 */
public class CookiePolicyInterceptor implements HandlerInterceptor {

    private final Logger logger = Logger.getLogger(getClass().toString());

    /**
     * Whitelisted URIs
     */
    private final List<String> whitelistedURIs = Arrays.asList(WebPath.TERMS_OF_USE, WebPath.COOKIE_POLICY);

    /**
     * Model name for the whitelisted flag
     */
    private static final String WHITELISTED = "whitelisted";


    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) {


        Optional<Cookie> privacyPolicyCookieOptional = getCookie(request);
        if (!privacyPolicyCookieOptional.isPresent()) {
            setInitialCookie(response);
        }

        logger.log(Level.INFO, "Request URI: " + request.getRequestURI());


        setIsWhitelistedFlag(modelAndView, request.getRequestURI());

    }

    private void setIsWhitelistedFlag(ModelAndView modelAndView, String requestURI){
        if (modelAndView != null){
            boolean whitelisted = isWhitelisted(requestURI);
            logger.log(Level.INFO, "Request URI: " + requestURI + " is whitelisted: " + whitelisted);
            modelAndView.addObject(WHITELISTED, whitelisted);
        }
    }

    private boolean isWhitelisted(final String requestURI){
        Optional<String> foundInWhitelisted = whitelistedURIs.stream()
                .filter(requestURI::equals)
                .findFirst();

        return foundInWhitelisted.isPresent();
    }


    /**
     * Create the initial cookie for the agreement if one does not exist yet
     * @param response HTTP response
     */
    private void setInitialCookie(HttpServletResponse response) {
        Cookie cookiePrivacyPolicy = new Cookie(Cookies.COOKIE_POLICY_NAME, Cookies.COOKIE_POLICY_DISAGREE_VALUE);
        cookiePrivacyPolicy.setMaxAge(Cookies.COOKIE_POLICY_MAX_AGE);
        cookiePrivacyPolicy.setPath(Cookies.COOKIE_POLICY_PATH);
        response.addCookie(cookiePrivacyPolicy);
    }

    /**
     * Get the cookie policy cookie
     * @param request user request
     * @return Optional of {@link Cookie}
     */
    private Optional<Cookie> getCookie(HttpServletRequest request) {
        Cookie[] cookieArray = request.getCookies();

        if (cookieArray == null){
            return Optional.empty();
        }


        return  Arrays.stream(cookieArray)
                .filter(c -> c.getName().equals(Cookies.COOKIE_POLICY_NAME))
                .findFirst();
    }


    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {

    }
}
