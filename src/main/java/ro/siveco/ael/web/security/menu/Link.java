package ro.siveco.ael.web.security.menu;

import java.io.Serializable;

/**
 * User: AlexandruVi
 * Date: 2017-08-30
 */
public class Link implements Serializable {

    private String href;

    private String content;

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
