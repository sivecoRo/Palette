package ro.siveco.ael.web.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.social.security.SpringSocialConfigurer;
import ro.siveco.ael.facebook.service.DefaultSocialUserDetailsService;
import ro.siveco.ael.lcms.domain.auth.service.UserService;
import ro.siveco.ael.service.security.AdditionalAuthenticationProvider;
import ro.siveco.ael.service.security.CustomAuthenticationFailureHandler;
import ro.siveco.ael.service.security.CustomLogoutSuccessHandler;
import ro.siveco.ael.web.components.WebPath;

/**
 * User: AlexandruVi
 * Date: 2017-02-20
 */
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private final UserDetailsService userDetailsService;

    private final AuthenticationSuccessHandler authenticationSuccessHandler;

    @Autowired
    private CustomLogoutSuccessHandler customLogoutSuccessHandler;

    @Autowired
    private UserService userService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    public WebSecurityConfig(UserDetailsService userDetailsService,
                             AuthenticationSuccessHandler authenticationSuccessHandler) {
        this.userDetailsService = userDetailsService;
        this.authenticationSuccessHandler = authenticationSuccessHandler;
    }

    /**
     * Configure Spring Web Security
     *
     * PROFILE_DELETED has to be permitted to all, as this view appears after the successful logout (after deleting
     * /anonymizing the profile).
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers(
                        WebPath.AUTHORIZE, /* sso */
                        "/img/**"/*the following are authorized by default in spring: , "/css/**","/js/**"*/
                )
                .permitAll()

    	    	.antMatchers(WebPath.REGISTER, WebPath.CHECK_EMAIL, WebPath.INDEX, WebPath.BECOME_MEMBER, WebPath.TRY_PALETTE,
                        WebPath.LOCATION, WebPath.INTERESTS, WebPath.CREATE_PROFILE, WebPath.RESULTS, WebPath.TERMS_OF_USE,
                        WebPath.GET_USERS_FOR_CITY, WebPath.GET_USERS_FOR_CITY_COORDINATES,
                        WebPath.COMPLETE_PROFILE, WebPath.VALIDATE_USERNAME, WebPath.VALIDATE_EMAIL, WebPath.PROFILE_DELETED,
                        WebPath.AGREE_TO_COOKIE_POLICY,
                        WebPath.COOKIE_POLICY,
                        WebPath.PRIVACY_POLICY,
                        WebPath.SERVER_DATA,
                        WebPath.REPORT_INAPPROPRIATE,
                        WebPath.DELETE_INAPPROPRIATE_COMMENT,
                        WebPath.REVIEW_ORGANIZER,
                        WebPath.REVIEW_PARTICIPANT,
                        WebPath.INFORMED_CONSENT,
                        WebPath.PROFILE_CREATED,
                        WebPath.PROFILE_VERIFICATION,
                        WebPath.RESEND_PROFILE_VERIFICATION,
                        WebPath.ERROR,
                        WebPath.FAQ,
                        WebPath.ERROR,
                        WebPath.FAVICON,
                        WebPath.NAVBAR_END_POINT).permitAll()
                .antMatchers(WebPath.FORGOT_PASSWORD, "/forgotPassword", "/resetPassword").permitAll()
    	    	.antMatchers("/accounts/**/reset.do", "/accounts/**/activate.do").permitAll()
                .antMatchers("/user/create?viewName=ForgotPasswordView").permitAll()
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage(WebPath.LOGIN)
                .failureHandler(new CustomAuthenticationFailureHandler("/login"))
                .defaultSuccessUrl(WebPath.INDEX)
                .successHandler(authenticationSuccessHandler)
                .permitAll()
                .and()
                .apply(new SpringSocialConfigurer().postLoginUrl("/courses/")
                        .alwaysUsePostLoginUrl(false))
                .and()
                .logout()
                .logoutSuccessHandler(customLogoutSuccessHandler)
                .permitAll();
        http.csrf().disable();
        http.headers().xssProtection().block(true);
    }

    @Override
    public void configure(WebSecurity security) {
        security.ignoring().antMatchers("/css/**", "/img/**", "/js/**", "/fonts/**", "/webjars/**");
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(new ShaPasswordEncoder());
    }

    @Bean
    public AdditionalAuthenticationProvider additionalAuthProvider() throws Exception {

        AdditionalAuthenticationProvider provider = new AdditionalAuthenticationProvider();
        provider.setUserDetailsService(userDetailsService);
        provider.setPasswordEncoder(new ShaPasswordEncoder());
        return provider;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {

        auth.authenticationProvider(additionalAuthProvider());
    }

    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public DefaultSocialUserDetailsService defaultSocialUserDetailsService() {
        return new DefaultSocialUserDetailsService(userService);
    }

}