package ro.siveco.ael.web.security.menu;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;
import ro.siveco.ael.lcms.domain.auth.model.AuthenticatedUser;
import ro.siveco.ael.lcms.domain.auth.model.User;
import ro.siveco.ael.lcms.domain.menu.model.MenuItem;
import ro.siveco.ael.lcms.domain.menu.model.MenuRenderType;
import ro.siveco.ael.lcms.domain.menu.service.MenuItemService;
import ro.siveco.ael.web.components.WebPath;
import ro.siveco.ael.web.controllers.WithUserBaseEntityController;
import ro.siveco.ram.starter.util.DomainMap;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Component
@Scope(scopeName = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class NavigationInitializer implements Serializable{

    private static String IMAGE_TEMPLATE = "<img src='##LOGO##' alt='AeL'/>";
    private static String LOGO_RGX = "##LOGO##";
    private final DomainMap domainMap;
    private final MenuItemService menuItemService;

    private Navigation navigation;

    @Autowired
    public NavigationInitializer(DomainMap domainMap, MenuItemService menuItemService) {
        this.domainMap = domainMap;
        this.menuItemService = menuItemService;
    }

    public void init() {
        navigation = new Navigation();
        List<MenuItem> menuItems = menuItemService.findAllByRights();
        menuItems.sort(Comparator.comparingInt(MenuItem::getOrder));
        //init elements:
        initBrandingLink();
        initMenu(menuItems);
        initMenuTabs();
        initMenuSide();
    }

    private void initBrandingLink() {
        Link brandLink = navigation.getBranding();
        brandLink.setHref(WebPath.INDEX);
        brandLink.setContent(IMAGE_TEMPLATE.replace(LOGO_RGX, "/img/product/cloud_logo.png?dc=" + getDC()));
        //for tenant:
        String tenantBrandingLink = getTenantBrandingLink();
        if (tenantBrandingLink != null) {
            brandLink.setContent(IMAGE_TEMPLATE.replace(LOGO_RGX, tenantBrandingLink));
        }
    }

    private void initMenu(List<MenuItem> menuItems) {
        menuItems.forEach(item -> {
                    switch (item.getType()) {
                        case MENU:
                            navigation.getMenu().add(item);
                            break;
                        case TAB:
                            navigation.getItems().put(item.getModelName(), item);
                            break;
                    }
                }
        );
    }

    private void initMenuTabs() {
        navigation.getMenu().forEach(menuItem -> {
            if (menuItem.getChildren() != null && menuItem.getChildren().size() > 0) {
                List<MenuItem> tabs =
                        menuItem.getChildren().stream().filter(m -> m.getType().equals(MenuRenderType.TAB))
                                .collect(Collectors.toList());
                navigation.getMenuTabs().put(menuItem, tabs);
                navigation.getMenuTabsByName().put(menuItem.getModelName(), tabs);
            }
        });
    }

    private void initMenuSide() {
        navigation.getMenu().forEach(menuItem -> {
            if (menuItem.getChildren() != null && menuItem.getChildren().size() > 0) {
                List<MenuItem> tabs =
                        menuItem.getChildren().stream().filter(m -> m.getType().equals(MenuRenderType.SIDE))
                                .collect(Collectors.toList());
                //navigation.getMenuTabs().put(menuItem, tabs);
                //navigation.getMenuTabsByName().put(menuItem.getLink(), tabs);
            }
        });
    }

    public String getDefaultUserPage() {
        if (navigation != null && navigation.getMenu().size() > 0) {
            return domainMap.getModelUrl(navigation.getMenu().get(0).getModelName());
        }
        return WebPath.PROFILE;
    }

    public String getDefaultPageAfterLoginForUser(AuthenticatedUser authenticatedUser) {

        if (authenticatedUser.hasRole("CourseTeacher")) {
            return WebPath.COURSES;
        }

        return WebPath.MY_COURSES_LINK;
    }

    private String getDC() {
        return "" + new Date().getTime();
    }

    private String getTenantBrandingLink() {
        User user = WithUserBaseEntityController.getUser();
        if (user.getTenant() != null && user.getTenant().getImage() != null && user.getTenant().getImage().length > 0) {
            return "/tenants/getImage/" + user.getTenant().getId() + getDC();
        }
        return null;
    }


    //getters
    public Navigation getNavigation() {
        return navigation;
    }

    public void setNavigation(Navigation navigation) {
        this.navigation = navigation;
    }

}
