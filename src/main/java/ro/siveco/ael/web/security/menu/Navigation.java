package ro.siveco.ael.web.security.menu;

import ro.siveco.ael.lcms.domain.menu.model.MenuItem;
import ro.siveco.ael.lcms.domain.menu.model.MenuRenderType;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Navigation implements Serializable{

    private final static List<MenuItem> EMPTY_ITEM_LIST = new ArrayList<>();

    private Link branding = new Link();
    private Map<String, MenuItem> items = new HashMap<>();
    private List<MenuItem> menu = new ArrayList<>();
    private Map<MenuItem, List<MenuItem>> menuTabs = new HashMap<>();
    private Map<String, List<MenuItem>> menuTabsByName = new HashMap<>();
    private Map<MenuItem, List<MenuItem>> menuSide = new HashMap<>();

    public Map<String, MenuItem> getItems() {
        return items;
    }

    public void setItems(Map<String, MenuItem> items) {
        this.items = items;
    }

    public Map<String, List<MenuItem>> getMenuTabsByName() {
        return menuTabsByName;
    }

    public void setMenuTabsByName(
            Map<String, List<MenuItem>> menuTabsByName) {
        this.menuTabsByName = menuTabsByName;
    }

    public List<MenuItem> getPageTabs(String menuLink) {
        if (items.containsKey(menuLink)) {
            MenuItem tab = items.get(menuLink);
            if (tab.getType().equals(MenuRenderType.TAB)) {
                return tab.getParent().getChildren()
                        .stream().filter(t -> t.getType().equals(MenuRenderType.TAB)).collect(Collectors.toList());
            }
        }
        return EMPTY_ITEM_LIST;
    }

    public boolean hasTabs(String menuLink) {
        return items.containsKey(menuLink) &&
                (items.get(menuLink).getType().equals(MenuRenderType.TAB) ||
                        items.get(menuLink).getType().equals(MenuRenderType.MENU));
    }

    public Link getBranding() {
        return branding;
    }

    public void setBranding(Link branding) {
        this.branding = branding;
    }

    public List<MenuItem> getMenu() {
        return menu;
    }

    public void setMenu(List<MenuItem> menu) {
        this.menu = menu;
    }

    public Map<MenuItem, List<MenuItem>> getMenuTabs() {
        return menuTabs;
    }

    public void setMenuTabs(
            Map<MenuItem, List<MenuItem>> menuTabs) {
        this.menuTabs = menuTabs;
    }

    public Map<MenuItem, List<MenuItem>> getMenuSide() {
        return menuSide;
    }

    public void setMenuSide(
            Map<MenuItem, List<MenuItem>> menuSide) {
        this.menuSide = menuSide;
    }

}
