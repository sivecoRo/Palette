package ro.siveco.ael.web.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

/**
 * User: AlexandruVi
 * Date: 2017-02-20
 */
@Configuration
@EnableWebSecurity
@Profile("ssl")
@Order(99)
public class SslWebSecurityConfig extends WebSecurityConfig {


    public SslWebSecurityConfig(UserDetailsService userDetailsService,
                                AuthenticationSuccessHandler authenticationSuccessHandler) {
        super(userDetailsService, authenticationSuccessHandler);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.requiresChannel().anyRequest().requiresSecure();
        super.configure(http);
    }
}
