package ro.siveco.ael.web.components.dd;

/**
 * User: AlexandruVi
 * Date: 2017-02-17
 */
public enum RequestAttribute {
    STATUS_CODE("javax.servlet.error.status_code");

    private final String value;

    RequestAttribute(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return this.name();
    }
}
