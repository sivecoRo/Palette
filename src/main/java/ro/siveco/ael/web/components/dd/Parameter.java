package ro.siveco.ael.web.components.dd;

/**
 * User: AlexandruVi
 * Date: 2017-02-17
 */
public enum Parameter {
    trace, lang, theme;
}
