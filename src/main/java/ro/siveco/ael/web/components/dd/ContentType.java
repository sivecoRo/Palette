package ro.siveco.ael.web.components.dd;

/**
 * User: AlexandruVi
 * Date: 2017-02-17
 */
public class ContentType {
    public static final String TEXT_HTML = "text/html";
    public static final String TEXT_XML = "text/xml";
    public static final String JSON = "application/json";
    public static final String PDF = "application/pdf";
}
