package ro.siveco.ael.web.components;

/**
 * User: AlexandruVi
 * Date: 2017-02-07
 */
public class Template {

    public final static String HOME = "home/home";
    public final static String INDEX = "home/index";
    public final static String BECOME_MEMBER = "home/become_member";
    public final static String LOGIN = "home/login";
    public final static String LOGIN_FRAGMENT = "home/login-fragment";
    public final static String HELLO = "home/hello";
    public final static String ERROR = "app/errors/error";
    public final static String REGISTER = "home/register";
    public final static String FORGOT_PASSWORD = "home/forgot-password";
    public final static String RESET_PASSWORD = "home/reset-password";
    public final static String API_WEB = "api/web";
    public final static String API_WEB_INDEX = API_WEB + "/index";
    public final static String CHANGE_PASSWORD = "content/aelUser/changePassword/index";
    public final static String ADMINISTRATION = "content/administration/index";
    public final static String TRY_PALETTE = "home/try_palette";
    public final static String LOCATION = "home/location";
    public final static String TERMS_OF_USE = "home/termsAndConditions";
    public final static String COOKIE_POLICY = "home/cookie-policy";
    public final static String PRIVACY_POLICY = "home/privacy-policy";
    public final static String FAQ = "home/faq";
    public final static String INFORMED_CONSENT = "home/informed-consent";
    public final static String INTERESTS = "home/interests";
    public final static String CREATE_PROFILE = "home/create-profile";
    public final static String PROFILE_CREATED = "home/profile-created";
    public final static String PROFILE_VERIFICATION = "home/profile-verification";
    public final static String RESEND_PROFILE_VERIFICATION = "home/resend-profile-verification";
    public final static String RESULTS = "home/results";
    public static final String DELETE_PROFILE = "home/delete-profile";
    public static final String PROFILE_DELETED = "home/profile-deleted";
    public static final String ACCESS_DENIED = "home/access-denied";



    /* PATHS */
    public final static String COURSE = "/course";
    public final static String MATERIAL = "/material";
    public final static String REPORT = "/report";

    /* OPERATIONS */
    public final static String LIST = "-list";
    public final static String VIEW = "-view";
    public final static String CREATE = "-create";
    public final static String PLAY = "-player";
    public final static String CATALOG = "-catalog";

    /* teaching */
    public final static String TEACHER = "/teacher";
    public final static String TEACHER_COURSE = TEACHER + COURSE + VIEW;
    public final static String TEACHER_COURSE_ADD = TEACHER + COURSE + CREATE;
    public final static String TEACHER_COURSE_LIST = TEACHER + COURSE + LIST;
    public final static String TEACHER_COURSE_VIEW = TEACHER + COURSE + VIEW;
    public final static String TEACHER_MATERIALS = TEACHER + MATERIAL + LIST;
    public final static String TEACHER_HOME = TEACHER_COURSE_LIST;

    /* learning */
    public final static String LEARNING = "/learning";
    public final static String LEARNING_COURSE_LIST = LEARNING + COURSE + LIST;
    public final static String LEARNING_COURSE_CATALOG = LEARNING + COURSE + CATALOG;
    public final static String LEARNING_COURSE_VIEW = LEARNING + COURSE + VIEW;

    /* course */
    public final static String COURSE_PLAYER_FRAGMENT = "overrides/course/player :: play";

    /* chat */
    public final static String CHAT = "/content/chat/index";

    /* properties exporter */
    public final static String PROPERTIES_EXPORTER = "/content/propertiesExportImport/index";
    public final static String PROPERTIES_IMPORTER = "/content/propertiesExportImport/importer";


    public final static String MY_COURSES = "/content/plannedCourseStudent/index";

    /* SEARCH */
    public final static String SEARCH_RESULT = "/search/search-result";

    /* user-messages */
    public final static String USER_MESSAGES = "/content/userMessage/index";

    /* user-notifications */
    public final static String USER_NOTIFICATIONS = "/content/userNotifications/index";
}
