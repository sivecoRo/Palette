package ro.siveco.ael.web.components.dd;

import ro.siveco.ael.domain.model.app.Theme;

import java.util.Arrays;
import java.util.List;

/**
 * User: AlexandruVi
 * Date: 2017-02-23
 */
public enum BuildTheme implements Theme {
    //    BOOTSTRAP3("bootstrap3"),
//    CLOUD_LEARNING("cloudLearning"),
    MATERIALIZE("materialize");

    private final String themeName;

    BuildTheme(String themeName) {

        this.themeName = themeName;
    }

    public static List<Theme> all() {
        return Arrays.asList(values());
    }

    public String getThemeName() {
        return themeName;
    }
}
