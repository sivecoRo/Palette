package ro.siveco.ael.web.components.dd;

/**
 * User: AlexandruVi
 * Date: 2017-02-07
 */
public enum FlashAttribute {
    message, alertLevel, files, isAdmin, user, loggedUserOptional, usedThemeName, redirectTo;
}
