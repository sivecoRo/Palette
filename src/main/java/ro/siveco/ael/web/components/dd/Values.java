package ro.siveco.ael.web.components.dd;

/**
 * User: AlexandruVi
 * Date: 2017-02-17
 */
public enum Values {
    FALSE("false");

    private final String value;

    Values(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return this.name();
    }
}
