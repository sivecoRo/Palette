package ro.siveco.ael.web.components;

/**
 * User: AlexandruVi
 * Date: 2017-02-07
 */
public class WebPath {

    public final static String INDEX = "/";
    public final static String BECOME_MEMBER = "/become_member";
    public final static String HELLO = "/hello";
    public final static String LOGIN = "/login";
    public final static String LOGOUT = "/logout";
    public final static String AUTHORIZE = "/authorize";
    public final static String ERROR = "/error";
    public final static String REDIRECT_ = "redirect:";
    public final static String REDIRECT_HOME = REDIRECT_ + INDEX;
    public final static String REDIRECT_TO_LOGIN = REDIRECT_ + LOGIN;
    public final static String REGISTER = "/register";
    public final static String CHECK_EMAIL = "/checkEmail";
    public final static String FORGOT_PASSWORD = "/forgot-password";
    public final static String API_WEB = "/api/web";
    public final static String TRY_PALETTE = "/try-palette";
    public final static String LOCATION = "/location";
    public final static String TERMS_OF_USE = "/terms-of-use";
    public final static String INTERESTS = "/interests";
    public static final String CREATE_PROFILE = "/create-profile";
    public static final String COMPLETE_PROFILE = "/completeProfile";
    public static final String VALIDATE_USERNAME= "/validateUsername";
    public static final String VALIDATE_EMAIL= "/validateEmail";
    public final static String RESULTS = "/results";
    public static final String PROFILE_DELETED = "/profile-deleted";
    public static final String AGREE_TO_COOKIE_POLICY = "/agreeToCookiePolicy";
    public static final String COOKIE_POLICY = "/cookie-policy";
    public static final String PRIVACY_POLICY = "/privacy-policy";
    public static final String INFORMED_CONSENT = "/informed-consent";
    public static final String FAQ = "/faq";

    public static final String FAVICON = "/favicon.ico";
    public static final String SERVER_DATA = "/serverData";
    public static final String REPORT_INAPPROPRIATE = "/reportInappropriate";
    public static final String REVIEW_ORGANIZER = "/reviewOrganizer";
    public static final String REVIEW_PARTICIPANT = "/reviewParticipant";
    public static final String DELETE_INAPPROPRIATE_COMMENT = "/deleteInappropriateComment";

    /* server names */
    public static final String SERVER_NAME_TRY = "try.palettev2.eu";
    public static final String SERVER_NAME_DEMO = "demo.palettev2.eu";
    public static final String SERVER_NAME_TEST = "test.palettev2.eu";

    public static final String PROFILE_CREATED = "/profile-created";
    public static final String PROFILE_VERIFICATION = "/profile-verification";
    public static final String RESEND_PROFILE_VERIFICATION = "/resend-profile-verification";

    public final static String CHANGE_PASSWORD = "/user/change-password";
	public static final String ADMINISTRATION = "/administration";
    public static final String PROFILE = "/user/profile";
    public static final String DELETE_PROFILE = "/user/delete-profile";
    public static final String NOTIFICATION_COURSES = "/courses/";
    public static final String NOTIFICATION_PLANNED_COURSES = "/sessions/";
    public static final String NOTIFICATION_PROFILE = "/profile/";
    public static final String DELETE_PROFILE_PICTURE = "/delete-picture";

    public static final String PAST_REVIEW_ATTENDEE = "/sessions";
    public static final String PAST_REVIEW_OWNER = "/courses";


    public static final String GET_USERS_FOR_CITY = "/getUsersForCity";
    public static final String GET_USERS_FOR_CITY_COORDINATES = "/getUsersForCityCoordinates";

    /* OPERATIONS */
    public final static String LIST = "/list";
    public final static String VIEW = "/view";
    public final static String CREATE = "/create";
    public final static String UPDATE = "/update";
    public final static String PLAY = "/play";
    public final static String CATALOG = "/catalog";
    public final static String QUOTA_MANAGEMENT = "/quotaManagement";

    /* PATHS */
    public final static String COURSE = "/course";
    public final static String MATERIAL = "/material";
    public final static String REPORT = "/report";

    /* Menu */
    public final static String SESSION = "/sessions";
    public final static String SESSIONS = "/sessions/";
    public final static String COURSES = "/courses/";
    public final static String TAXONOMIES = "/taxonomies/";
    public final static String TAXONOMY_LEVELS = "/taxonomyLevels/";
    public final static String COMPETENCES = "/competences/";
    public final static String JOB_TITLES = "/jobTitles/";
    public final static String JOB_TITLE_COMPETENCES = "/jobTitleCompetences/";
    public final static String USER_COMPETENCES = "/userCompetences/";
    public final static String COURSE_COMPETENCES = "/courseCompetences/";
    public final static String FUNCTIONS = "/functions/";
    public final static String ROLES = "/roles/";
    public final static String GROPUS = "/groups/";
    public final static String COMPONENT_USER_JOB_PATH = "/componentUserJobPath/";
    public final static String COMPONENT_USER_JOB = "/componentuserjob/";
    public final static String COMPONENT= "/components/";
    public final static String GRADING_TYPES = "/gradingTypes/";
    public final static String PROMOTIONS = "/promotions/";

    /* teaching */
    public final static String TEACHING_PATH = "/teaching";
    public final static String TEACHING_COURSE_LIST = TEACHING_PATH + COURSE + LIST;
    public final static String TEACHING_COURSE_VIEW = TEACHING_PATH + COURSE + VIEW;
    public final static String TEACHING_COURSE_CREATE = TEACHING_PATH + COURSE + CREATE;
    public final static String TEACHING_MATERIAL_LIST = TEACHING_PATH + MATERIAL + LIST;
    ;
    public final static String TEACHING_REPORT_LIST = TEACHING_PATH + REPORT + LIST;

    /* learning */
    public final static String LEARNING_PATH = "/learning";
    public final static String LEARNING_COURSE = LEARNING_PATH + COURSE;
    public final static String LEARNING_COURSE_LIST = LEARNING_PATH + COURSE + LIST;
    public final static String LEARNING_COURSE_VIEW = LEARNING_PATH + COURSE + VIEW;
    public final static String LEARNING_COURSE_PLAY = LEARNING_PATH + COURSE + PLAY;
    public final static String LEARNING_COURSE_CATALOG = LEARNING_PATH + COURSE + CATALOG;

    public final static String WORKSPACE_PATH = "/workspace";
    public final static String WORKSPACE_LIST = WORKSPACE_PATH + LIST;

    /* chat */
    public final static String CHAT_PATH = "/chat";

    /* properties exporter */
    public final static String PROPERTIES_EXPORTER_PATH = "/propertiesExporter";
    public final static String PROPERTIES_IMPORTER_PATH = "/propertiesImporter";

    /* user-messages */
    public final static String USER_MESSAGES_PATH = "/user_messages/";

    /* user-notifications */
    public final static String USER_NOTIFICATIONS_PATH = "/user_notifications";
    public final static String EMAIL_TEMPLATE = "/email/email_template.html";

    /* custom notifications */
    public final static String NOTIFICATIONS_PATH = "/notifications";

    public final static String PLANNED_NOTIFICATIONS_PATH = "/plannedNotifications";

    /* search */
    public final static String SEARCH_PATH = "/search";
    /* reporting */
    public final static String COURSE_LEARNING_MATERIAL_MATERIALS_PATH = "/materials";
    public final static String PLANNED_COURSES_FOR_RESOURCES_PATH = "/plannedCoursesForResources";
    public final static String PLANNED_COURSE_STUDENT_COMMENT_PATH = "/plannedCourseStudentComment";
    public final static String CREATE_PLANNED_COURSE_STUDENT_COMMENT_PATH = "/createPlannedCourseStudentComment";
    public final static String DELETE_PLANNED_COURSE_STUDENT_COMMENT_PATH = "/deletePlannedCourseStudentComment";

    public final static String COURSE_USER = "/courseUser";

    public final static String COURSE_DETAILS = "/courseDetails";

    public final static String USER_OVERVIEWS = "/userOverviews";

    /*metadata*/
    public final static String TAGS_PATH = "/tagList";
    public final static String METADATA_PATH = "/metadataList";
    public final static String CREATE_METADATA = "/createMetadata";
    public final static String DELETE_METADATA = "/deleteMetadata";

    /*export*/
    public final static String EXPORT = "/export";

    /* planned course students */
    public final static String CHANGE_USER_RUNNING_PERIOD = "/changeUserRunning";
    public final static String CHANGE_ALL_USERS_RUNNING_PERIOD = "/changeAllUsersRunning";
    public final static String CHANGE_COURSE_RUNNING_PERIOD_FOR_USER = "/changeCourseRunningPeriod";

    /* create tenant and tenant admin*/
    public final static String CREATE_TENANT_AND_ADMIN = "/createTenantAndAdmin";

    /* Tenant */
    public final static String TENANT_UPDATE_IMAGE = "/updateImage";
    public final static String TENANT_GET_IMAGE = "/getImage/{id}";
    public final static String TENANT_ADD = "/addTenant";
    public final static String TENANT_UPDATE = "/updateTenant";
    public final static String TENANT_CREATE_ADMINISTRATOR = "/createTenantAdministrator";
    public final static String TENANT_CREATE_MAIL_CONFIG = "/createTenantMailConfig";

    public static final String NAVBAR_END_POINT = "/navbarEndPoint";

    /*My courses*/
    public final static String MY_COURSES_LINK = "/my-courses/";

    public final static String DELETE_COMMENT = "/deleteComment";


    public static String redirect() {
        return redirect(INDEX);
    }

    public static String redirect(String to) {
        return REDIRECT_ + to;
    }
}
