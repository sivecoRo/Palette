package ro.siveco.ael.web.components.dd;

/**
 * User: AlexandruVi
 * Date: 2017-02-07
 */
public enum AlertLevel {
    SUCCESS("success"),
    INFO("info"),
    WARNING("warning"),
    DANGER("danger");

    private final String alertLevel;

    AlertLevel(String alertLevel) {

        this.alertLevel = alertLevel;
    }

    public String getAlertLevel() {
        return alertLevel;
    }

    @Override
    public String toString() {
        return alertLevel;
    }
}
