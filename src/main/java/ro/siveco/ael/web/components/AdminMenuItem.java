package ro.siveco.ael.web.components;

import java.util.List;


public class AdminMenuItem {
	private String href = "#";
	private String name;
	private String description;
	private String materializeIcon;
	private String iconPath;
	
	private List<AdminMenuItem> subMenu;
	

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getMaterializeIcon() {
		return materializeIcon;
	}

	public void setMaterializeIcon(String materializeIcon) {
		this.materializeIcon = materializeIcon;
	}

	public String getIconPath() {
		return iconPath;
	}

	public void setIconPath(String iconPath) {
		this.iconPath = iconPath;
	}

	public List<AdminMenuItem> getSubMenu() {
		return subMenu;
	}

	public void setSubMenu(List<AdminMenuItem> subMenu) {
		this.subMenu = subMenu;
	}

}
