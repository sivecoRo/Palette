package ro.siveco.ael.web.app;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * User: AlexandruVi
 * Date: 2017-02-23
 */
@Component
@ConfigurationProperties(prefix = "app")
public class AppProperties {

    private String name;

//    private List<BuildTheme> buildThemes;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

//    public List<BuildTheme> getBuildThemes() {
//        return buildThemes;
//    }
//
//    public void setBuildThemes(List<BuildTheme> buildThemes) {
//        this.buildThemes = buildThemes;
//    }
}
