package ro.siveco.ael.web;

/**
 * Utility class with cookie related values
 */
public abstract class Cookies {


    /*
     * Cookie related to user agreement to cookie policy
     */
    public static final String COOKIE_POLICY_NAME = "cookiePolicy";
    public static final String COOKIE_POLICY_AGREE_VALUE = "agreed";
    public static final String COOKIE_POLICY_DISAGREE_VALUE = "not_agreed";
    public static final int COOKIE_POLICY_MAX_AGE = 60 * 60 * 24 * 90; //90 days
    public static final String COOKIE_POLICY_PATH = "/";

}
