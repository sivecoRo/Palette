package ro.siveco.ael.web.mvc;


import nz.net.ultraq.thymeleaf.LayoutDialect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.core.env.Environment;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.ui.context.ThemeSource;
import org.springframework.ui.context.support.ResourceBundleThemeSource;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.ThemeResolver;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.theme.ThemeChangeInterceptor;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.spring4.SpringTemplateEngine;
import org.thymeleaf.spring4.templateresolver.SpringResourceTemplateResolver;
import org.thymeleaf.spring4.view.ThymeleafViewResolver;
import org.thymeleaf.templateresolver.ITemplateResolver;
import org.thymeleaf.templateresolver.TemplateResolver;
import ro.siveco.ael.service.locale.DefaultLocaleChangeInterceptor;
import ro.siveco.ael.service.locale.DefaultLocaleResolver;
import ro.siveco.ael.service.security.AuthenticatedUserFetcherService;
import ro.siveco.ael.web.components.dd.Cookie;
import ro.siveco.ael.web.components.dd.Parameter;
import ro.siveco.ael.web.interceptors.CookiePolicyInterceptor;

import javax.servlet.DispatcherType;
import javax.servlet.Filter;
import java.nio.charset.StandardCharsets;
import java.util.EnumSet;

/**
 * User: AlexandruVi
 * Date: 2017-02-20
 */
@Configuration
public class MvcConfig extends WebMvcConfigurerAdapter {

    private final MvcProperties properties;

    private final AuthenticatedUserFetcherService authenticatedUserFetcherService;

    //private final LrsAuthenticationFilter lrsAuthenticationFilter;

    @SuppressWarnings("SpringJavaAutowiringInspection")
    @Autowired
    public MvcConfig(MvcProperties properties, AuthenticatedUserFetcherService authenticatedUserFetcherService) {
        this.properties = properties;
        this.authenticatedUserFetcherService = authenticatedUserFetcherService;
    }

    @Bean
    public TemplateResolver templateResolver() {
        SpringResourceTemplateResolver templateResolver = new ThemeTemplateResolver();
        templateResolver.setCacheable(false);
        templateResolver.setTemplateMode("HTML5");
        templateResolver.setCharacterEncoding(StandardCharsets.UTF_8.displayName());
        templateResolver.setPrefix(properties.getTemplatesLocation() + "/templates/themes/");
        templateResolver.setSuffix(".html");
        return templateResolver;
    }

    /**
     * Enable the thymeleaf layout dialect for hierarchical templates and standard layouts
     *
     * @param environment
     * @return configured template engine with new dialect added
     */
    @Bean
    public SpringTemplateEngine templateEngine(Environment environment) {
        SpringTemplateEngine templateEngine = new SpringTemplateEngine();
        templateEngine.addTemplateResolver(templateResolver());
        templateEngine.addDialect(new AelThymeleafDialect(environment));
        templateEngine.addDialect(new LayoutDialect());
        return templateEngine;
    }

    @Bean
    @Autowired
    public ViewResolver viewResolver(Environment environment) {
        ThymeleafViewResolver viewResolver = new ThymeleafViewResolver();
        viewResolver.setTemplateEngine(templateEngine(environment));
        viewResolver.setCharacterEncoding(StandardCharsets.UTF_8.displayName());
        viewResolver.setOrder(1);

        return viewResolver;
    }

    @Bean
    public TemplateEngine emailTemplateEngine() {
        final SpringTemplateEngine templateEngine = new SpringTemplateEngine();
        templateEngine.addTemplateResolver(emailTemplateResolver());
        templateEngine.addDialect(new LayoutDialect());
        return templateEngine;
    }

    @Bean
    public ITemplateResolver emailTemplateResolver(){
        final SpringResourceTemplateResolver emailTemplateResolver = new SpringResourceTemplateResolver();
        emailTemplateResolver.setOrder(2);
        emailTemplateResolver.setCacheable(false);
        emailTemplateResolver.setTemplateMode("HTML5");
        emailTemplateResolver.setCharacterEncoding(StandardCharsets.UTF_8.displayName());
        emailTemplateResolver.setPrefix(properties.getTemplatesLocation() + "/templates/themes/materialize/");
        emailTemplateResolver.setSuffix(".html");
        return emailTemplateResolver;
    }

    @Bean
    public StringHttpMessageConverter stringHttpMessageConverter() {
        return new StringHttpMessageConverter(StandardCharsets.UTF_8);
    }

    @Bean
    public ThemeSource themeSource() {
        ResourceBundleThemeSource source = new ResourceBundleThemeSource();

        return source;
    }

    @Bean
    public ThemeResolver themeResolver() {
        AelThemeResolver resolver = new AelThemeResolver();
        resolver.setCookieMaxAge(properties.getCookieThemeMaxAgeInSeconds());
        resolver.setCookieName(Cookie.THEME);
        resolver.setDefaultThemeName(properties.getDefaultThemeName());
        resolver.setSupportedThemes(properties.getSupportedThemes());
        return resolver;
    }

    @Bean
    public ThemeChangeInterceptor themeChangeInterceptor() {
        ThemeChangeInterceptor themeChangeInterceptor = new ThemeChangeInterceptor();
        themeChangeInterceptor.setParamName(Parameter.theme.name());
        return themeChangeInterceptor;
    }


    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
//        registry.addViewController(WebPath.INDEX).setViewName(Template.HOME);
//        registry.addViewController(WebPath.HELLO).setViewName(Template.HELLO);
//        registry.addViewController(WebPath.LOGIN).setViewName(Template.LOGIN);
    }

    @Bean
    public CustomMessageSource messageSource() {
        CustomMessageSource messageSource = new CustomMessageSource();
//        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setBasenames("classpath:messages","classpath:notification/user-messages","classpath:certificate/certificate");
        messageSource.setCacheSeconds(properties.getMessagesCacheSeconds());
        return messageSource;
    }

    @Bean
    public LocaleResolver localeResolver() {
        DefaultLocaleResolver localeResolver = new DefaultLocaleResolver();
        localeResolver.setSupportedLocales(properties.getAvailableLocales());
        localeResolver.setDefaultLocale(properties.getDefaultLocale());
        return localeResolver;
    }

    @Bean
    public LocaleChangeInterceptor localeChangeInterceptor() {
        LocaleChangeInterceptor localeChangeInterceptor = new DefaultLocaleChangeInterceptor();
        localeChangeInterceptor.setParamName(Parameter.lang.name());
        localeChangeInterceptor.setLanguageTagCompliant(properties.getLanguageTagCompliant());
        return localeChangeInterceptor;
    }

    @Bean
    public CookiePolicyInterceptor cookiePolicyInterceptor(){
        return new CookiePolicyInterceptor();
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(localeChangeInterceptor());
        registry.addInterceptor(cookiePolicyInterceptor());
        //registry.addInterceptor(modelAttributesInterceptor());
    }

    @Bean
    @DependsOn({"springSecurityFilterChain"})
    public FilterRegistrationBean springSecurityFilterChainBindToError(
            @Qualifier("springSecurityFilterChain") Filter springSecurityFilterChain) {

        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(springSecurityFilterChain);
        registration.setDispatcherTypes(EnumSet.allOf(DispatcherType.class));
        return registration;
    }

    @Bean
    public ShaPasswordEncoder passwordEncoder() {
        return new ShaPasswordEncoder();
    }
}