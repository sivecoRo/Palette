package ro.siveco.ael.web.mvc;

import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;

/**
 * User: AlexandruVi
 * Date: 2017-04-20
 */
public class CustomMessageSource extends ReloadableResourceBundleMessageSource {

    /**
     * Resolves the given message code as key in the retrieved bundle files,
     * returning the value found in the bundle as-is (without MessageFormat parsing).
     *
     * @param code
     * @param locale
     */
    @Override
    protected String resolveCodeWithoutArguments(String code, Locale locale) {
        String firstTry = super.resolveCodeWithoutArguments(code, locale);
        if (firstTry == null) {
            String genericKey = resolveGenericKey(code);
            String secondTry = super.resolveCodeWithoutArguments(genericKey, locale);
            if (secondTry != null) {
                return secondTry;
            }
        }
        return firstTry;
    }

    /**
     * Resolves the given message code as key in the retrieved bundle files,
     * using a cached MessageFormat instance per message code.
     *
     * @param code
     * @param locale
     */
    @Override
    protected MessageFormat resolveCode(String code, Locale locale) {
        MessageFormat firstTry = super.resolveCode(code, locale);
        if (firstTry == null) {
            String genericKey = resolveGenericKey(code);
            MessageFormat secondTry = super.resolveCode(genericKey, locale);
            if (secondTry != null) {
                return secondTry;
            }
        }
        return firstTry;
    }

    protected String resolveGenericKey(String code) {
        return code.replaceAll("model\\.[a-z]\\w+", "model");
    }

    public Map<String, String> allMessages() {
        return allMessages(LocaleContextHolder.getLocale());
    }

    public Map<String, String> allMessages(Locale locale) {
        Map<String, String> messages = new HashMap<>();

        PropertiesHolder propertiesHolder = getMergedProperties(locale);
        Properties properties = propertiesHolder.getProperties();

        for (Object key : properties.keySet()) {
            messages.put((String) key, (String) properties.get(key));
        }
        return messages;
    }
}
