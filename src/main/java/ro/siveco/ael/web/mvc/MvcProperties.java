package ro.siveco.ael.web.mvc;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import ro.siveco.ael.domain.model.app.Theme;
import ro.siveco.ael.web.components.dd.BuildTheme;

import java.util.List;
import java.util.Locale;

/**
 * User: AlexandruVi
 * Date: 2017-02-21
 */
@Component
@ConfigurationProperties(prefix = "spring.mvc")
public class MvcProperties {

    private Locale defaultLocale;

    private List<Locale> availableLocales;

    private Boolean languageTagCompliant;

    private Integer messagesCacheSeconds = 30;

    private Integer cookieThemeMaxAgeInSeconds = 240;

    private String defaultThemeName = BuildTheme.MATERIALIZE.getThemeName();

    private List<Theme> supportedThemes = BuildTheme.all();

    private String templatesLocation = "classpath:";

    public Locale getDefaultLocale() {
        return defaultLocale;
    }

    public void setDefaultLocale(Locale defaultLocale) {
        this.defaultLocale = defaultLocale;
    }

    public List<Locale> getAvailableLocales() {
        return availableLocales;
    }

    public void setAvailableLocales(List<Locale> availableLocales) {
        this.availableLocales = availableLocales;
    }

    public Boolean getLanguageTagCompliant() {
        return languageTagCompliant;
    }

    public void setLanguageTagCompliant(Boolean languageTagCompliant) {
        this.languageTagCompliant = languageTagCompliant;
    }

    public Integer getMessagesCacheSeconds() {
        return messagesCacheSeconds;
    }

    public void setMessagesCacheSeconds(Integer messagesCacheSeconds) {
        this.messagesCacheSeconds = messagesCacheSeconds;
    }

    public Integer getCookieThemeMaxAgeInSeconds() {
        return cookieThemeMaxAgeInSeconds;
    }

    public void setCookieThemeMaxAgeInSeconds(Integer cookieThemeMaxAgeInSeconds) {
        this.cookieThemeMaxAgeInSeconds = cookieThemeMaxAgeInSeconds;
    }

    public String getDefaultThemeName() {
        return defaultThemeName;
    }

    public void setDefaultThemeName(String defaultThemeName) {
        this.defaultThemeName = defaultThemeName;
    }

    public List<Theme> getSupportedThemes() {
        return supportedThemes;
    }

    public void setSupportedThemes(List<Theme> supportedThemes) {
        this.supportedThemes = supportedThemes;
    }

    public String getTemplatesLocation() {
        return templatesLocation;
    }

    public void setTemplatesLocation(String templatesLocation) {
        this.templatesLocation = templatesLocation;
    }
}
