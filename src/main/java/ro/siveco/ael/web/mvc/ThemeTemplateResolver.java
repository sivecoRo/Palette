package ro.siveco.ael.web.mvc;

import org.springframework.web.servlet.support.RequestContextUtils;
import org.thymeleaf.TemplateProcessingParameters;
import org.thymeleaf.context.IWebContext;
import org.thymeleaf.resourceresolver.IResourceResolver;
import org.thymeleaf.spring4.templateresolver.SpringResourceTemplateResolver;
import org.thymeleaf.util.StringUtils;
import org.thymeleaf.util.Validate;
import ro.siveco.ael.web.components.dd.FlashAttribute;
import ro.siveco.ram.controller.util.TemplateName;

import javax.servlet.http.HttpServletRequest;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * User: AlexandruVi
 * Date: 2017-03-01
 */
public class ThemeTemplateResolver extends SpringResourceTemplateResolver {

    static final String OVERRIDES_ROOT = TemplateName.OVERRIDES_ROOT;

    static final String GENERIC_CONTENT_ROOT = TemplateName.GENERIC_CONTENT_ROOT;

    private final Logger logger = Logger.getLogger(getClass().toString());

    protected Logger getLogger() {

        return logger;
    }

    @Override
    protected String computeResourceName(final TemplateProcessingParameters templateProcessingParameters) {

        checkInitialized();

        final String templateName = templateProcessingParameters.getTemplateName();

        Validate.notNull(templateName, "Template name cannot be null");

        String unaliasedName = this.getTemplateAliases().get(templateName);
        if (unaliasedName == null) {
            unaliasedName = templateName;
        }

        String themeName;
        String defaultThemeName;
        AelThemeResolver themeResolver;
        HttpServletRequest request;
        try {
            IWebContext context = ((IWebContext) templateProcessingParameters.getProcessingContext().getContext());
            request = context.getHttpServletRequest();
            themeResolver = (AelThemeResolver) RequestContextUtils.getThemeResolver(request);
            defaultThemeName = themeResolver.getDefaultThemeName();
            themeName = RequestContextUtils.getTheme(request).getName();
        } catch (Throwable e) {
            getLogger().log(Level.SEVERE, e.getMessage(), e);
            throw e;
        }
        String resourceName = resolveTemplateResourceWithTheme(unaliasedName, themeName);

        final IResourceResolver resourceResolver = this.getResourceResolver();
        InputStream templateInputStream =
                resourceResolver.getResourceAsStream(templateProcessingParameters, resourceName);

        /* If the template does not exists, use a generic one (if it is a overrides/.. template) */
        if (templateInputStream == null && unaliasedName.indexOf(OVERRIDES_ROOT) == 0) {
            String specificPart = unaliasedName
                    .substring(unaliasedName.indexOf(OVERRIDES_ROOT) + OVERRIDES_ROOT.length());
            specificPart = specificPart.substring(specificPart.lastIndexOf("/"));
            String genericTemplateName = GENERIC_CONTENT_ROOT + specificPart;
            getLogger().warning(
                    "Template not found: [" + resourceName + "] use the one for [generic=" + genericTemplateName + "]");

            resourceName = resolveTemplateResourceWithTheme(genericTemplateName, themeName);
            templateInputStream =
                    resourceResolver.getResourceAsStream(templateProcessingParameters, resourceName);
        }

        /* If the template does not exists, use one from the default theme */
        if (templateInputStream == null) {
            request.setAttribute(FlashAttribute.usedThemeName.name(), defaultThemeName);
            getLogger().warning(
                    "Template not found: " + resourceName + " use the one for [defaultTheme=" + defaultThemeName + "]");
            resourceName = resolveTemplateResourceWithTheme(unaliasedName, defaultThemeName);
        } else {
            request.setAttribute(FlashAttribute.usedThemeName.name(), themeName);
        }

        getLogger().log(Level.INFO, "[Resolved template: " + resourceName + ']');
        return resourceName;

    }

    private String resolveTemplateResourceWithTheme(String templateName, String themeName) {

        final StringBuilder resourceName = new StringBuilder();
        if (!StringUtils.isEmptyOrWhitespace(this.getPrefix())) {
            resourceName.append(this.getPrefix());
        }
        resourceName.append(themeName).append("/");
        resourceName.append(templateName);
        if (!StringUtils.isEmptyOrWhitespace(this.getSuffix())) {
            resourceName.append(this.getSuffix());
        }
        getLogger().log(Level.INFO, "Find template: " + resourceName.toString());
        return resourceName.toString();
    }
}