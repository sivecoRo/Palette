package ro.siveco.ael.web.mvc;

import org.springframework.util.StringUtils;
import org.springframework.web.servlet.theme.CookieThemeResolver;
import org.springframework.web.util.WebUtils;
import ro.siveco.ael.domain.model.app.Theme;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * User: AlexandruVi
 * Date: 2017-03-01
 */
public class AelThemeResolver extends CookieThemeResolver {

    private List<Theme> supportedThemes;

    @Override
    public String resolveThemeName(HttpServletRequest request) {
        // Check request for preparsed or preset theme.
        String themeName = (String) request.getAttribute(THEME_REQUEST_ATTRIBUTE_NAME);
        if (themeName != null) {
            return themeName;
        }

        // Retrieve cookie value from request.
        Cookie cookie = WebUtils.getCookie(request, getCookieName());
        if (cookie != null) {
            String value = cookie.getValue();
            if (StringUtils.hasText(value)) {
                themeName = value;
            }
        }

        // Fall back to default theme.
        if (themeName == null || !isSupportedTheme(themeName)) {
            themeName = getDefaultThemeName();
        }
        request.setAttribute(THEME_REQUEST_ATTRIBUTE_NAME, themeName);
        return themeName;
    }

    private boolean isSupportedTheme(String themeName) {
        for (Theme supportedTheme : getSupportedThemes()) {
            if (supportedTheme.getThemeName().equals(themeName)) {
                return true;
            }
        }
        return false;
    }

    public List<Theme> getSupportedThemes() {
        return supportedThemes;
    }

    public void setSupportedThemes(List<Theme> supportedThemes) {
        this.supportedThemes = supportedThemes;
    }
}
