package ro.siveco.ael.web.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.core.Authentication;
import org.springframework.ui.context.Theme;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.thymeleaf.context.IContext;
import org.thymeleaf.context.IProcessingContext;
import org.thymeleaf.context.IWebContext;
import org.thymeleaf.extras.springsecurity4.auth.AuthUtils;
import org.thymeleaf.extras.springsecurity4.auth.Authorization;
import org.thymeleaf.extras.springsecurity4.dialect.SpringSecurityDialect;
import org.thymeleaf.util.ArrayUtils;
import ro.siveco.ael.facebook.model.SocialAuthenticatedUser;
import ro.siveco.ael.lcms.domain.auth.model.AuthenticatedUser;
import ro.siveco.ael.lcms.domain.auth.model.User;
import ro.siveco.ael.lcms.domain.auth.model.UserOptional;
import ro.siveco.ael.lcms.domain.tenant.model.Tenant;
import ro.siveco.ael.web.components.dd.FlashAttribute;
import ro.siveco.ram.controller.util.MappingUri;
import ro.siveco.ram.controller.util.ViewType;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * User: AlexandruVi
 * Date: 2017-02-22
 */
public class AelThymeleafDialect extends SpringSecurityDialect {

    private static final String LOGGED_USER_OPTIONAL_EXPRESSION_OBJECT_NAME = "loggedUserOptional";

    private static final String THEME_NAME = "themeName";

    private static final String USED_THEME_NAME = "usedThemeName";

    private static final String IS_PRODUCTION = "isProduction";

    private static final String APP_VERSION = "appVersion";

    private static final String APP_BASE_NAME = "appBaseName";

    private static final String PLATFORM_CREATED_ACCOUNT = "platformCreatedAccount";

    private static final String EXTERNAL_USER_NAME = "externalUserName";

    private static final String OVERRIDES_PATH = "overridesPath";

    private static final String GENERICS_PATH = "genericsPath";

    private static final String INDEX_FRAGMENT = "indexFragment";

    private static final String VIEW_FRAGMENT = "viewFragment";

    private static final String FORM_FRAGMENT = "formFragment";

    private final Environment environment;

    @Autowired
    public AelThymeleafDialect(Environment environment) {
        this.environment = environment;
    }

    public Map<String, Object> getAdditionalExpressionObjects(
            final IProcessingContext processingContext) {

        final IContext context = processingContext.getContext();
        final IWebContext webContext =
                (context instanceof IWebContext ? (IWebContext) context : null);

        final Map<String, Object> objects = new HashMap<String, Object>(3, 1.0f);

        /*
         * Create the #authentication, #authorization, #loggedUserOptional, #themeName, #usedThemeName expression objects
         */
        if (webContext != null) {

            final HttpServletRequest request = webContext.getHttpServletRequest();
            final HttpServletResponse response = webContext.getHttpServletResponse();
            final ServletContext servletContext = webContext.getServletContext();

            if (request != null && response != null && servletContext != null) {
                Theme theme = RequestContextUtils.getTheme(request);
                objects.put(THEME_NAME, theme.getName());
                objects.put(USED_THEME_NAME, request.getAttribute(FlashAttribute.usedThemeName.name()));

                final Authentication authentication = AuthUtils.getAuthenticationObject();
                final Authorization authorization =
                        new Authorization(processingContext, authentication, request, response, servletContext);

                objects.put(AUTHENTICATION_EXPRESSION_OBJECT_NAME, authentication);
                objects.put(AUTHORIZATION_EXPRESSION_OBJECT_NAME, authorization);
                final UserOptional loggedUserOptional;

                final Boolean isPlatformCreatedAccount;
                final String externalUserName;
                if (authentication != null) {
                    if (authentication.getPrincipal() instanceof AuthenticatedUser) {
                        loggedUserOptional =
                                UserOptional.of(((AuthenticatedUser) authentication.getPrincipal()).getUser());
                        isPlatformCreatedAccount = true;
                        externalUserName = null;
                    } else if (authentication.getPrincipal() instanceof SocialAuthenticatedUser) {
                        User user = ((SocialAuthenticatedUser) authentication.getPrincipal()).getUser();
                        loggedUserOptional =
                                UserOptional.of(user);
                        isPlatformCreatedAccount = false;
                        externalUserName = user.getUsername();
                    } else {
                        loggedUserOptional = UserOptional.empty();
                        isPlatformCreatedAccount = true;
                        externalUserName = null;
                    }
                } else {
                    loggedUserOptional = UserOptional.empty();
                    isPlatformCreatedAccount = true;
                    externalUserName = null;
                }

                if (!UserOptional.empty().equals(loggedUserOptional) && loggedUserOptional.get().getTenant() != null) {
                    Tenant tenant = loggedUserOptional.get().getTenant();
                    if (tenant.getImage() != null && tenant.getImage().length > 0) {
                        String dc = "?_dc=" + new Date().getTime();
                        String imageSrc = "/tenants/getImage/" + tenant.getId() + dc;
                        String img = "<img src=\"" + imageSrc + "\" alt=\"AeL\"/>";

                        //                        Link logoLink = (Link)webContext.getVariables().get("appNavBrandLink");
                        //                        logoLink.setContent(img);
                    }
                }

                objects.put(LOGGED_USER_OPTIONAL_EXPRESSION_OBJECT_NAME, loggedUserOptional);
                objects.put(PLATFORM_CREATED_ACCOUNT, isPlatformCreatedAccount);
                objects.put(EXTERNAL_USER_NAME, externalUserName);
                objects.put(IS_PRODUCTION, ArrayUtils.contains(environment.getActiveProfiles(), "production"));
                objects.put(APP_VERSION, environment.getProperty("app.version"));
                objects.put(APP_BASE_NAME, environment.getProperty("app.baseName"));
                objects.put(OVERRIDES_PATH, ThemeTemplateResolver.OVERRIDES_ROOT + MappingUri.SLASH);
                objects.put(GENERICS_PATH, ThemeTemplateResolver.GENERIC_CONTENT_ROOT);
                objects.put(INDEX_FRAGMENT, MappingUri.SLASH + ViewType.INDEX_FRAGMENT.getTemplateName());
                objects.put(VIEW_FRAGMENT, MappingUri.SLASH + ViewType.VIEW_FRAGMENT.getTemplateName());
                objects.put(FORM_FRAGMENT, MappingUri.SLASH + ViewType.FORM_FRAGMENT.getTemplateName());
            }

        }

        return objects;

    }
}
