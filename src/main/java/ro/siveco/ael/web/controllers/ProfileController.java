package ro.siveco.ael.web.controllers;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.util.Base64Utils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ro.siveco.ael.config.FeaturesConfiguration;
import ro.siveco.ael.lcms.domain.auth.model.AelUser;
import ro.siveco.ael.lcms.domain.auth.service.AelUserService;
import ro.siveco.ael.lcms.domain.auth.service.DefaultUserService;
import ro.siveco.ael.lcms.domain.auth.service.ProfileVerificationService;
import ro.siveco.ael.lcms.domain.metadata.model.*;
import ro.siveco.ael.lcms.domain.metadata.service.CountryService;
import ro.siveco.ael.lcms.domain.metadata.service.InterestService;
import ro.siveco.ael.lcms.domain.metadata.service.MetadataService;
import ro.siveco.ael.lcms.domain.preference.model.Preference;
import ro.siveco.ael.lcms.domain.preference.model.Preferences;
import ro.siveco.ael.lcms.domain.preference.service.PreferencesService;
import ro.siveco.ael.lcms.domain.profile.model.CountryDTO;
import ro.siveco.ael.lcms.domain.profile.model.Gender;
import ro.siveco.ael.lcms.domain.profile.model.UserDTO;
import ro.siveco.ael.lcms.domain.profile.model.UserDetails;
import ro.siveco.ael.lcms.domain.profile.service.UserDetailsService;
import ro.siveco.ael.web.components.Template;
import ro.siveco.ael.web.components.WebPath;
import ro.siveco.ael.web.mvc.MvcProperties;
import ro.siveco.ael.web.security.menu.NavigationInitializer;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.security.Principal;
import java.util.*;
import java.util.stream.IntStream;


@Controller
public class ProfileController extends BaseController {
    private final NavigationInitializer navigationInitializer;
    private final DefaultUserService defaultUserService;

    @Autowired
    private MvcProperties mvcProperties;

    @Autowired
    private Environment environment;

    @Autowired
    private InterestService interestService;

    @Autowired
    private AelUserService aelUserService;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private MetadataService metadataService;

    @Autowired
    private CountryService countryService;

    @Autowired
    private PreferencesService preferencesService;

    @Autowired
    private FeaturesConfiguration featuresConfiguration;

    @Autowired
    private ProfileVerificationService profileVerificationService;




    //Google places API key: AIzaSyDqH6hVDuuKXFSN2dHui2PtxwDbO1ow52Y
    //need to enable: Google Maps JavaScript API in api Google api manager

    public ProfileController(NavigationInitializer navigationInitializer,
                             DefaultUserService defaultUserService) {
        this.navigationInitializer = navigationInitializer;
        this.defaultUserService = defaultUserService;
    }

    @RequestMapping(value = WebPath.CREATE_PROFILE, method = RequestMethod.GET)
    public ModelAndView  showCreateProfile(Model model, Principal principal, HttpServletRequest request, HttpSession session) {
        List<Interest> interests = interestService.getAllI18NInterests(principal, request);
        model.addAttribute("interests", interests);

        String language="en";
        String languageString = (String) request.getHeader("accept-language");
        languageString = languageString.substring(0,2);

        final List<Locale.LanguageRange> languageRanges = Locale.LanguageRange.parse(languageString);
        if(languageRanges != null && languageRanges.size() > 0)
        {
            Locale.LanguageRange languageRange = languageRanges.get(0);
            Locale browserLocale = Locale.forLanguageTag(languageRange.getRange());
            //initial set to default language
            Preference globalLanguage =  preferencesService.getGlobalPreferenceByName(Preferences.GLOBAL_AELCLIENT_DEFAULT_APPLICATION_LOCALE.toString());
            language = globalLanguage.getValue();

            List<Locale> allowedLocales = mvcProperties.getAvailableLocales();
            for(int i = 0; i < allowedLocales.size(); i++)
            {
                if(allowedLocales.get(i).getLanguage() == browserLocale.getLanguage()) //check similarity by short language tag
                {
                    language = browserLocale.getLanguage();
                    break;
                }
            }

            /*if(allowedLocales.contains(browserLocale)) //this is not working, beacouse browserLocale is different than allowedLocales objects
            {
                language = browserLocale.getLanguage();
            }
            else
            {
                Preference globalLanguage =  preferencesService.getGlobalPreferenceByName(Preferences.GLOBAL_AELCLIENT_DEFAULT_APPLICATION_LOCALE.toString());
                language = globalLanguage.getValue();
            }*/
        }
        else
        {
            Preference globalLanguage =  preferencesService.getGlobalPreferenceByName(Preferences.GLOBAL_AELCLIENT_DEFAULT_APPLICATION_LOCALE.toString());
            language = globalLanguage.getValue();
        }

        model.addAttribute("sessionLanguage", language);

        List<Country> allCountries= countryService.getAllCountries(aelUserService.getLanguage(null, request));

        model.addAttribute("allCountries", allCountries);

        //options for birth date selection
        int[] days = IntStream.rangeClosed(1,31).toArray();
        int[] months = IntStream.rangeClosed(1,12).toArray();
        int[] years = IntStream.rangeClosed(1920,1980).toArray();

        model.addAttribute("allDays", days);
        model.addAttribute("allMonths", months);
        model.addAttribute("allYears", years);

        return new ModelAndView(Template.CREATE_PROFILE,"profileModel", model);
    }

    @RequestMapping(value = "/completeProfile", method = RequestMethod.POST)
    public ResponseEntity<String> createUser(@RequestBody UserDTO user) {
        Boolean success = true;
        String msg = "", resp;
        AelUser newUser = aelUserService.build();
        try {

            newUser.setFirstName(user.getFirst_name());
            newUser.setLastName(user.getLast_name());
            newUser.setUsername(user.getMail());
            newUser.setEmail(user.getMail());
            newUser.setPassword(DigestUtils.sha1Hex(user.getPassword()));
            newUser.setPhoneNo(user.getPhone());
            Date currentDate = new Date();
            newUser.setCreateDate(currentDate);

            if (featuresConfiguration.isProfileVerificationDisabled()){
                //If profile verification feature is disabled, set the verified date to current date
                //thus enabling the user to sign in right away
                newUser.setVerifiedDate(currentDate);
            }

            /* gender */
            if(user.getGender() != -1) {
                if(user.getGender() == 1) {
                    newUser.setGender(Gender.MALE);
                }
                else if(user.getGender() == 2) {
                    newUser.setGender(Gender.FEMALE);
                }
                else if (user.getGender() == 3) {
                    newUser.setGender(Gender.UNDEFINED);
                }
            }

            if(user.country != null) {
                CountryDTO countryDTO = new CountryDTO();
                countryDTO.setName(user.country);
                newUser.setCountry(countryDTO);
            }

            newUser.setCity(user.getCity());

            newUser.setDisplayName((user.getFirst_name() == null ? "" : user.getFirst_name()) + " " + (user.getLast_name() == null ? "" : user.getLast_name()));

            newUser = aelUserService.saveOrUpdate(newUser);


            //update user details -- user details row is create on user save?
            UserDetails userDetails = userDetailsService.getBasicData(newUser.getId());
            userDetails.setUser(newUser);
            userDetails.setBirthDate(user.birth_date);

            /* visibility */
            userDetails.setShow_birth_date(user.show_birth_date);
            userDetails.setShow_city(user.show_city);
            userDetails.setShow_country(user.show_country);
            userDetails.setShow_email(user.show_email);
            userDetails.setShow_gender(user.show_gender);
            userDetails.setShow_interests(user.show_interests);
            userDetails.setShow_phone(user.show_phone);
            userDetails.setCity_name(user.city_name);


            if(user.getImage() != null && user.getImage() != "")
            {
                //userDetails.setImage(user.getImage().getBytes());//write image as byte64
                userDetails.setImage(Base64Utils.decodeFromString(user.getImage()));
            }
            userDetailsService.saveOrUpdate(userDetails);

            //save user selected interests
            if(user.selectedInterests != null && user.selectedInterests.length > 0) { //if user choose any interests
                List<Metadata> metadataList = new ArrayList<Metadata>();
                List<Interest> interests = interestService.getAllInterests();
                for (int i = 0; i < user.selectedInterests.length; i++)
                {
                    Interest iTemp = null;
                    for(int j = 0; j < interests.size(); j++) //get interest data
                    {
                        if(interests.get(j).getId() == user.selectedInterests[i])//user.selectedInterests is array of selected interests IDs
                        {
                            iTemp = interests.get(j);
                            break;
                        }
                    }

                    Metadata metadata = new Metadata();
                    metadata.setEntityId(newUser.getId());
                    metadata.setEntityType(EntityType.USER);
                    metadata.setTag(iTemp.getTag());
                    metadata.setRelationType(RelationType.INCLUDED);

                    metadataList.add(metadata);
                }
                metadataService.save(metadataList);

            }

            //save user Preferences
            List<Preference> userPreferences = new ArrayList<Preference>();
            //userPreferences.add(createPreferenceInstance("city", user.preferences_city, newUser));
            //userPreferences.add(createPreferenceInstance("country", user.preferences_country, newUser));
            //userPreferences.add(createPreferenceInstance("gender", user.preferences_gender, newUser));
            //userPreferences.add(createPreferenceInstance("birth_date", user.preferences_birth_date, newUser));
            //userPreferences.add(createPreferenceInstance("phone", user.preferences_phone, newUser));
            //userPreferences.add(createPreferenceInstance("mail", user.preferences_mail, newUser));
            userPreferences.add(createPreferenceInstance("interests", user.preferences_interests, newUser));
            userPreferences.add(createPreferenceInstance("acceptTerms", user.getAcceptTerms().toString(), newUser));

            /* LANGUAGE */
            /* get the language from browser (default is english) */
            HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
            String languageString = request.getHeader("accept-language");
            languageString = languageString.substring(0, 2);
            String myLanguage = "en";
            final List<Locale.LanguageRange> languageRanges = Locale.LanguageRange.parse(languageString);
            if (languageRanges != null && languageRanges.size() > 0) {
                Locale.LanguageRange languageRange = languageRanges.get(0);
                Locale browserLocale = Locale.forLanguageTag(languageRange.getRange());
                List<Locale> allowedLocales = mvcProperties.getAvailableLocales();
                if (allowedLocales.contains(browserLocale)) {
                     myLanguage = browserLocale.getLanguage();
                }
            }
            userPreferences.add(createPreferenceInstance(Preferences.USER_LOCALE.toString(), myLanguage, newUser));

            preferencesService.save(userPreferences);
            defaultUserService.createComponentUserJobForUser(newUser,"Teacher");


            if (featuresConfiguration.isProfileVerificationEnabled()){
                profileVerificationService.createProfileVerification(newUser);

            } else {
                  /*
                     Log the user into the system after the profile has been created if profile validation
                     is disabled
                   */
                request.login(user.getMail(), user.getPassword());
            }


            setEmailInSession(request.getSession(), newUser.getEmail());
        }
        catch (Exception ex){
            success = false;
            msg = ex.getMessage();
        }

        String redirect = WebPath.SESSIONS;
        if (featuresConfiguration.isProfileVerificationEnabled()){
            redirect = WebPath.PROFILE_CREATED;
        }

        resp = "{\"success\": \"" + success + "\", \"msg\": \"" + msg + "\", \"redirect\": \"" + redirect +"\"}";




        return new ResponseEntity<String>(resp, HttpStatus.CREATED);
    }



    public  Preference createPreferenceInstance(String name, String value, AelUser user)
    {
        Preference p = new Preference();
        p.setName(name);
        p.setValue(value);
        p.setUser(user);
        p.setGlobal(false);

        return p;
    }

    @RequestMapping(value = "/validateUsername", method = RequestMethod.POST)
    public ResponseEntity<String> validateUsername(@RequestBody String username) {
        Boolean exists = true;
        String msg = "", resp;
        username = username.substring(1,username.length()-1);
        AelUser existsingUser = aelUserService.getAelUserByUsername(username);
        if(existsingUser == null)
        {
            exists = false;
        }
        else
        {
            exists = true;
        }
        resp = "{\"exists\": \"" + exists + "\", \"msg\": \"" + msg + "\"}";
        return new ResponseEntity<String>(resp, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/validateEmail", method = RequestMethod.POST)
    public ResponseEntity<String> validateEmail(@RequestBody String email) {
        Boolean exists = true;
        String msg = "", resp;
        email = email.substring(1,email.length()-1);
        Object existsingUser = aelUserService.findByEmail(email);
        if(existsingUser == null)
        {
            exists = false;
        }
        else
        {
            exists = true;
        }
        resp = "{\"exists\": \"" + exists + "\", \"msg\": \"" + msg + "\"}";
        return new ResponseEntity<String>(resp, HttpStatus.CREATED);
    }



    @GetMapping(WebPath.PROFILE_CREATED)
    public String getProfileCreatedPage(ModelMap model, HttpServletRequest request){

        String email = getEmailFromSession(request.getSession());
        if (email != null) {
            model.put(SESSION_EMAIL_ATTR, email);
        }

        return  Template.PROFILE_CREATED;
    }


}
