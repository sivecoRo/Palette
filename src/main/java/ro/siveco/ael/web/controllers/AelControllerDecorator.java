package ro.siveco.ael.web.controllers;

import org.springframework.security.core.context.SecurityContextHolder;
import ro.siveco.ael.lcms.domain.auth.model.AuthenticatedUser;
import ro.siveco.ael.lcms.domain.auth.model.Role;
import ro.siveco.ael.web.components.WebPath;

/**
 * User: AlexandruVi
 * Date: 2017-05-25
 */
public interface AelControllerDecorator {

    default String redirectHome() {
        return WebPath.REDIRECT_HOME;
    }

    default String redirectToLogin() {
        return WebPath.REDIRECT_TO_LOGIN;
    }

    default boolean hasRole(AuthenticatedUser authenticatedUser, String roleName) {
        if (authenticatedUser != null) {
            return authenticatedUser.hasRole(roleName);
        }
        return false;
    }

    default boolean authenticatedUserHasRole(String roleName) {
        AuthenticatedUser authenticatedUser = getAuthenticatedUser();
        return hasRole(authenticatedUser, roleName);
    }

    default boolean hasRole(AuthenticatedUser authenticatedUser, Role role) {
        return authenticatedUser.hasRole(role);
    }

    default AuthenticatedUser getAuthenticatedUser() {
        return (AuthenticatedUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }

}
