package ro.siveco.ael.web.controllers;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;
import ro.siveco.ael.web.components.WebPath;
import ro.siveco.ael.web.security.menu.Link;

import java.util.ArrayList;
import java.util.List;

/**
 * User: AlexandruVi
 * Date: 2017-04-10
 */
@ControllerAdvice
//TODO: Use MenuConfiguration instead. Change tenant logo.
public class ControllerCommon {

    @ModelAttribute
    public void commonModel(Model model) {
        Link appNavBrandLink = new Link();
        List<Link> appNavRightLinks = new ArrayList<>();

        Link chatLink = new Link();
        chatLink.setHref(WebPath.CHAT_PATH);
        model.addAttribute("chatLink", chatLink);
        
        Link userMessagesLink = new Link();
        userMessagesLink.setHref(WebPath.USER_MESSAGES_PATH);
        model.addAttribute("userMessagesLink", userMessagesLink);
        
        Link userNotificationsLink = new Link();
        userNotificationsLink.setHref(WebPath.USER_NOTIFICATIONS_PATH);
        model.addAttribute("userNotificationsLink", userNotificationsLink);

        Link userProfileLink = new Link();
        userProfileLink.setHref("/user/profile");
        userProfileLink.setContent("header.user.profile");
        model.addAttribute("userProfileLink", userProfileLink);

        Link userChangePasswordLink = new Link();
        userChangePasswordLink.setHref(WebPath.CHANGE_PASSWORD);
        userChangePasswordLink.setContent("header.user.change-password");
        model.addAttribute("userChangePasswordLink", userChangePasswordLink);
    }
}
