package ro.siveco.ael.web.controllers;

import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * User: AlexandruVi
 * Date: 2017-02-17
 */
@Component
public class BaseController extends ro.siveco.ram.starter.controller.BaseController implements AelControllerDecorator {

    public static final String SESSION_EMAIL_ATTR = "email";



    protected void setEmailInSession(HttpSession session, String email) {
        session.setAttribute(SESSION_EMAIL_ATTR, email);
    }

    protected String getEmailFromSession(HttpSession session){
        Object emailAsObject = session.getAttribute(ProfileController.SESSION_EMAIL_ATTR);

        if (emailAsObject == null){
            return null;
        } else {
            return (String) emailAsObject;
        }
    }
    
}
