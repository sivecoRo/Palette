package ro.siveco.ael.web.controllers;

import com.querydsl.core.types.dsl.BooleanExpression;
import org.springframework.util.MultiValueMap;
import ro.siveco.ael.lcms.domain.tenant.model.QTenant;
import ro.siveco.ael.lcms.domain.tenant.model.Tenant;
import ro.siveco.ael.lcms.domain.tenant.model.TenantHolder;
import ro.siveco.ram.starter.service.BaseEntityService;

import java.util.Optional;

/**
 * User: AlexandruVi
 * Date: 2017-04-26
 */
abstract public class TenantAwareBaseEntityController<ENTITY extends TenantHolder>
        extends WithUserBaseEntityController<ENTITY> {

    public TenantAwareBaseEntityController(BaseEntityService<ENTITY> restService) {
        super(restService);
    }
    
    @Override
    protected Optional<BooleanExpression> buildFilter(MultiValueMap<String, String> parameters) {
        Tenant userTenant = getUser().getTenant();
        QTenant tenantPath = getTenantPath();
        if (userTenant != null) {
            return Optional.of(tenantPath.eq(userTenant));
        } else {
            return Optional.of(tenantPath.isNull());
        }
    }

    @SuppressWarnings("WeakerAccess")
    abstract protected QTenant getTenantPath();

//    @Override
//    protected ENTITY onCreate(ENTITY entity) {
//        return onSave(entity);
//    }
//
//    private ENTITY onSave(ENTITY entity) {
//        User creator = getUser();
//        Tenant tenant = creator.getTenant();
//        entity.setTenant(tenant);
//        if (entity instanceof TenantCreatorUser) {
//            ((TenantCreatorUser) entity).setCreator(creator);
//        }
//        return entity;
//    }
//
//    @Override
//    protected ENTITY onUpdate(Long id, ENTITY entity) {
//        return onSave(entity);
//    }
}
