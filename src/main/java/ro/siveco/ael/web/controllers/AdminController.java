package ro.siveco.ael.web.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ro.siveco.ael.web.components.AdminMenuItem;
import ro.siveco.ael.web.components.Template;
import ro.siveco.ael.web.components.WebPath;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@Controller
public class AdminController {


    @GetMapping(WebPath.ADMINISTRATION)
    public String adminPage(Principal principal, HttpServletRequest request, HttpServletResponse response,
                           HttpSession session, Model model) {
    	
    	model.addAttribute("menu", createAdminMenu());

        return Template.ADMINISTRATION;
    }
    
    private List<AdminMenuItem> createAdminMenu() {
    	
    	List<AdminMenuItem> menu = new ArrayList<AdminMenuItem>();

    	AdminMenuItem tenantsItem = new AdminMenuItem();
    	tenantsItem.setName("admin.menu.tenants");
    	tenantsItem.setMaterializeIcon("people");

    	List<AdminMenuItem> tenantsSubMenu = new ArrayList<AdminMenuItem>();
    	
    	AdminMenuItem tenantsSubItem = new AdminMenuItem();
    	tenantsSubItem.setHref("/tenants/");
    	tenantsSubItem.setName("admin.menu.tenants.tenants");
    	tenantsSubItem.setMaterializeIcon("people");
    	tenantsSubMenu.add(tenantsSubItem);
    	
    	AdminMenuItem ipsSubItem = new AdminMenuItem();
    	ipsSubItem.setHref("/ips/");
    	ipsSubItem.setName("admin.menu.tenants.ips");
    	tenantsSubMenu.add(ipsSubItem);

    	tenantsItem.setSubMenu(tenantsSubMenu);
    	menu.add(tenantsItem);

    	
    	AdminMenuItem securityItem = new AdminMenuItem();
    	securityItem.setName("admin.menu.security");
    	securityItem.setMaterializeIcon("security");

    	List<AdminMenuItem> securitySubMenu = new ArrayList<AdminMenuItem>();

    	AdminMenuItem usersSubItem = new AdminMenuItem();
    	usersSubItem.setHref("/user/");
    	usersSubItem.setName("admin.menu.security.users");
    	usersSubItem.setMaterializeIcon("person");
    	securitySubMenu.add(usersSubItem);

    	AdminMenuItem groupsSubItem = new AdminMenuItem();
    	groupsSubItem.setHref("/groups/");
    	groupsSubItem.setName("admin.menu.security.groups");
    	groupsSubItem.setMaterializeIcon("group");
    	securitySubMenu.add(groupsSubItem);

    	AdminMenuItem rolesSubItem = new AdminMenuItem();
    	rolesSubItem.setHref("/roles/");
    	rolesSubItem.setName("admin.menu.security.roles");
    	securitySubMenu.add(rolesSubItem);

    	AdminMenuItem functionsSubItem = new AdminMenuItem();
    	functionsSubItem.setHref("/functions/");
    	functionsSubItem.setName("admin.menu.security.functions");
    	securitySubMenu.add(functionsSubItem);

    	securityItem.setSubMenu(securitySubMenu);
    	menu.add(securityItem);
    	

    	AdminMenuItem organizationItem = new AdminMenuItem();
    	organizationItem.setName("admin.menu.organization");
    	organizationItem.setMaterializeIcon("folder");

    	List<AdminMenuItem> organizationSubMenu = new ArrayList<AdminMenuItem>();
    	
    	AdminMenuItem jobTitlesSubItem = new AdminMenuItem();
    	jobTitlesSubItem.setHref("/jobTitles/");
    	jobTitlesSubItem.setName("admin.menu.organization.jobTitles");
    	organizationSubMenu.add(jobTitlesSubItem);
    	
    	organizationItem.setSubMenu(organizationSubMenu);
    	menu.add(organizationItem);
    	

    	AdminMenuItem educationItem = new AdminMenuItem();
    	educationItem.setName("admin.menu.education");
    	educationItem.setMaterializeIcon("school");

    	List<AdminMenuItem> educationSubMenu = new ArrayList<AdminMenuItem>();
    	
    	AdminMenuItem tagsSubItem = new AdminMenuItem();
    	tagsSubItem.setHref("/tags/");
    	tagsSubItem.setName("admin.menu.education.tags");
    	educationSubMenu.add(tagsSubItem);
    	
    	AdminMenuItem competencesSubItem = new AdminMenuItem();
    	competencesSubItem.setHref("/competences/");
    	competencesSubItem.setName("admin.menu.education.competences");
    	educationSubMenu.add(competencesSubItem);
    	
    	educationItem.setSubMenu(educationSubMenu);
    	menu.add(educationItem);
    	

    	AdminMenuItem reportsItem = new AdminMenuItem();
    	reportsItem.setName("admin.menu.reports");
    	reportsItem.setMaterializeIcon("description");

    	List<AdminMenuItem> reportsSubMenu = new ArrayList<AdminMenuItem>();
    	
    	reportsItem.setSubMenu(reportsSubMenu);
    	menu.add(reportsItem);
    	

    	AdminMenuItem settingsItem = new AdminMenuItem();
    	settingsItem.setName("admin.menu.settings");
    	settingsItem.setMaterializeIcon("settings");

    	List<AdminMenuItem> settingsSubMenu = new ArrayList<AdminMenuItem>();

    	AdminMenuItem userProfileSubItem = new AdminMenuItem();
    	userProfileSubItem.setHref("/user/profile/management");
    	userProfileSubItem.setName("admin.menu.settings.user-profile-management");
    	userProfileSubItem.setIconPath("/img/generic/generic-user.png");
    	settingsSubMenu.add(userProfileSubItem);
    	
    	settingsItem.setSubMenu(settingsSubMenu);
    	menu.add(settingsItem);

    	return menu;
    	
    }
}
