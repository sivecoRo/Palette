package ro.siveco.ael.web.controllers.teaching;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import ro.siveco.ael.lcms.domain.auth.model.User;
import ro.siveco.ael.lcms.domain.organization.model.Component;
import ro.siveco.ael.lcms.domain.organization.model.TreeNode;
import ro.siveco.ael.lcms.repository.ComponentRepository;
import ro.siveco.ael.lcms.repository.ComponentUserJobRepository;
import ro.siveco.ael.lcms.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by LiviuI on 4/4/2017.
 */
@Controller
public class TreeController {

    private final ComponentRepository componentRepository;

    private final ComponentUserJobRepository componentUserJobRepository;

    private final UserRepository userRepository;

    @Autowired
    public TreeController(ComponentRepository componentRepository,
                          ComponentUserJobRepository componentUserJobRepository, UserRepository userRepository) {
        this.componentRepository = componentRepository;
        this.componentUserJobRepository = componentUserJobRepository;
        this.userRepository = userRepository;
    }

    @ResponseBody
    @RequestMapping(value = {"/component/tree", "/component/tree/{nodeId}"}, produces = "application/json")
    public List<TreeNode> list(@PathVariable(name = "nodeId", required = false) Long componentId) {
        //list leaf (users)
        if (componentId == null) {
            //Component root = componentRepository.getRootComponent();
            //componentId = root.getId();
            componentId = 1L;
        }

        List<TreeNode> children = new ArrayList<>();

        List<TreeNode> nodes = componentRepository.getComponentChildTreeNodes(componentId);
        children.addAll(nodes);

        List<TreeNode> leafs = componentUserJobRepository.getComponentChildTreeNodes(componentId);
        children.addAll(leafs);

        return children;
    }


    @ResponseBody
    @RequestMapping(value = {"/users/get"}, produces = "application/json")
    public List<User> list1(@PathVariable(name = "nodeId", required = false) Long userId) {
        User user = userRepository.findOneById(userId);
        List<User> users = new ArrayList<>(1);
        users.add(user);
        return users;
    }

    @ResponseBody
    @RequestMapping(value = {"/users/get", "/users/component/get"}, produces = "application/json")
    public List<User> list2(@PathVariable(name = "nodeId", required = false) Long componentId) {
        Component component = componentRepository.getOne(componentId);
        List<Component> children = new ArrayList<>();

//        for( Component c : component.getChildren()){
//            children.add(c);
//            c.getUserJobs()
//        }
        //List<Component> components component.getChildren();
        return null;
    }
}
