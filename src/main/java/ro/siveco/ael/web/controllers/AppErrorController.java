package ro.siveco.ael.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import ro.siveco.ael.web.components.Template;
import ro.siveco.ael.web.components.WebPath;
import ro.siveco.ael.web.components.dd.ContentType;
import ro.siveco.ram.starter.errorHandler.ErrorHandler;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * User: AlexandruVi
 * Date: 2017-01-17
 */
@Controller
public class AppErrorController extends BaseController implements ErrorController {

    private final ErrorHandler errorHandler;

    @Autowired
    public AppErrorController(ErrorHandler errorHandler) {
        this.errorHandler = errorHandler;
    }

    /**
     * Supports the HTML Error Views
     *
     * @param request
     * @return
     */
    @RequestMapping(value = WebPath.ERROR, produces = ContentType.TEXT_HTML)
    public ModelAndView errorHtml(HttpServletRequest request) {
        Map<String, Object> body = errorHandler.getErrorAttributes(request);
        String method = request.getMethod();
        getLogger().warning("Error [" + body.get("message") + "] on: [" + method + "] " + body.get("path"));
        if (body.get("status") != null && body.get("status").equals(404)) {
            body.put("title", errorHandler
                    .i18nMessage("ro.siveco.ram.service.exception.ResourceNotFoundException.title", request));
        }
        return new ModelAndView(Template.ERROR, body);
    }

    /**
     * Supports other formats like JSON, XML
     *
     * @param request
     * @return
     */
    @RequestMapping(value = WebPath.ERROR)
    @ResponseBody
    public ResponseEntity<Map<String, Object>> error(HttpServletRequest request) {
        Map<String, Object> body = errorHandler.getErrorAttributes(request);
        HttpStatus status = getStatus(request);
        String method = request.getMethod();
        getLogger().warning("Error [" + body.get("message") + "] on: [" + method + "] " + body.get("path"));
        return new ResponseEntity<>(body, status);
    }

    /**
     * Returns the path of the error page.
     *
     * @return the error path
     */
    @Override
    public String getErrorPath() {
        return WebPath.ERROR;
    }

}
