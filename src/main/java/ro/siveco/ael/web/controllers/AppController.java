package ro.siveco.ael.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ro.siveco.ael.lcms.domain.auth.model.AelUser;
import ro.siveco.ael.lcms.domain.auth.service.AelUserService;
import ro.siveco.ael.lcms.domain.auth.service.DefaultUserService;
import ro.siveco.ael.lcms.domain.course.model.PlannedCourse;
import ro.siveco.ael.lcms.domain.course.model.PlannedCourseStudent;
import ro.siveco.ael.lcms.domain.course.service.PlannedCourseService;
import ro.siveco.ael.lcms.domain.course.service.PlannedCourseStudentService;
import ro.siveco.ael.lcms.domain.metadata.model.Comment;
import ro.siveco.ael.lcms.domain.metadata.model.CommentInappropriate;
import ro.siveco.ael.lcms.domain.metadata.model.Country;
import ro.siveco.ael.lcms.domain.metadata.model.Interest;
import ro.siveco.ael.lcms.domain.metadata.service.CommentService;
import ro.siveco.ael.lcms.domain.metadata.service.CommentInappropriateService;
import ro.siveco.ael.lcms.domain.metadata.service.CountryService;
import ro.siveco.ael.lcms.domain.metadata.service.InterestService;
import ro.siveco.ael.lcms.domain.preference.model.Preference;
import ro.siveco.ael.lcms.domain.preference.model.Preferences;
import ro.siveco.ael.lcms.domain.preference.service.PreferencesService;
import ro.siveco.ael.service.mail.EmailService;
import ro.siveco.ael.web.Cookies;
import ro.siveco.ael.web.components.Template;
import ro.siveco.ael.web.components.WebPath;
import ro.siveco.ael.web.components.dd.FlashAttribute;
import ro.siveco.ael.web.mvc.MvcProperties;
import ro.siveco.ael.web.security.menu.NavigationInitializer;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * User: AlexandruVi
 * Date: 2017-02-17
 */
@Controller
public class AppController extends BaseController {

    private final NavigationInitializer navigationInitializer;
    private final DefaultUserService defaultUserService;

    @Autowired
    private InterestService interestService;
    @Autowired
    private CountryService countryService;
    @Autowired
    private PlannedCourseService plannedCourseService;
    @Autowired
    private PlannedCourseStudentService plannedCourseStudentService;
    @Autowired
    private AelUserService aelUserService;
    @Autowired
    private MvcProperties mvcProperties;
    @Autowired
    private PreferencesService preferencesService;
    @Autowired
    private CommentService commentService;
    @Autowired
    private CommentInappropriateService commentInappropriateService;
    @Autowired
    private EmailService emailService;

    public AppController(NavigationInitializer navigationInitializer,
                         DefaultUserService defaultUserService,
                         PreferencesService preferencesService) {
        this.navigationInitializer = navigationInitializer;
        this.defaultUserService = defaultUserService;
        this.preferencesService = preferencesService;
    }

    @GetMapping(WebPath.INDEX)
    public String index(Principal principal, HttpServletRequest request, HttpSession session) {
        if(principal == null) {
            return Template.INDEX;
        }
        else
        {
            return WebPath.redirect(WebPath.SESSIONS);
        }
    }

    @GetMapping(WebPath.BECOME_MEMBER)
    public String becomeMember(Principal principal, HttpServletRequest request, HttpSession session) {


        return Template.BECOME_MEMBER;
    }

    @GetMapping(WebPath.TRY_PALETTE)
    public String tryPalette(Principal principal, HttpServletRequest request, HttpSession session) {
        return Template.TRY_PALETTE;
    }

    @GetMapping(WebPath.LOCATION)
    public String location(Principal principal, HttpServletRequest request, HttpSession session,  ModelMap model) {
        List<Country> allCountries= countryService.getAllCountries(aelUserService.getLanguage(null, request));
        model.put("allCountries", allCountries);
        return Template.LOCATION;
    }

    @GetMapping(WebPath.TERMS_OF_USE)
    public String getTermsAndConditionsPage(Principal principal, HttpServletRequest request, ModelMap model) {
        addSessionLanguageToModelMap(principal, request, model);

        return Template.TERMS_OF_USE;
    }


    /*@AjaxStructuralMapping(path = WebPath.RESULTS, method = RequestMethod.POST)
    public ResponseEntity<String> results(@ModelAttribute("test") String test, ModelMap model) {

        model.put("allPlannedCourses",999);
        String resp = "{\"success\": \"" + true + "\"}";
        return new ResponseEntity<>(resp, HttpStatus.OK);
    }*/

    @GetMapping(WebPath.INTERESTS)
    public String interests(Principal principal, HttpServletRequest request, HttpSession session, ModelMap model) {
        List<Interest> allInterests = interestService.getAllI18NInterests(principal, request);
        model.put("allInterests", allInterests);
        return Template.INTERESTS;
    }

    @GetMapping(WebPath.RESULTS)
    public String results(@RequestParam MultiValueMap<String, String> parameters, ModelMap model) {
        String country = parameters.get("country").get(0);
        String coordinates = parameters.get("coordinates").get(0);
        String interests = parameters.get("interests").get(0);
        Long coursesNoByCountry = plannedCourseService.getPlannedCoursesCountByCountryAndInterests(country, interests);
        Long coursesNoByLocation = plannedCourseService.getPlannedCoursesCountByLocationAndInterests(coordinates, interests);
        model.put("plannedCoursesCount", coursesNoByLocation);
        model.put("plannedCourses", plannedCourseService.getTopPlannedCoursesForLocationAndInterests(country, coordinates, interests, coursesNoByCountry, coursesNoByLocation));
        return Template.RESULTS;
    }

    @RequestMapping(WebPath.LOGIN)
    public String login(Principal principal, HttpServletRequest request, HttpServletResponse response,
                        HttpSession session, ModelMap modelMap) {
        if (principal != null && principal instanceof Authentication) {
            return redirectHome();
        }
        String redirectUriAfterLogin = null;
        if (session != null) {
            SavedRequest savedRequest = (SavedRequest) session.getAttribute("SPRING_SECURITY_SAVED_REQUEST");
            if (savedRequest != null) {
                redirectUriAfterLogin = filterRedirectUri(request, savedRequest.getRedirectUrl());
            }
        }

        if (redirectUriAfterLogin == null) {
            redirectUriAfterLogin = getRedirectUriFromRequest(request);
        }

        if (redirectUriAfterLogin != null) {
            if (session != null) {
                session.setAttribute(FlashAttribute.redirectTo.name(), redirectUriAfterLogin);
            }
        }

        if (isRequestAjax(request)) {
            /* Force redirect on client side */
            response.setHeader(HttpHeaders.LOCATION, WebPath.LOGIN);

            return Template.LOGIN_FRAGMENT;
        }

        if (session != null) {
            Object loginErrors = session.getAttribute("loginError");
            if (loginErrors != null && !loginErrors.toString().trim().equals("")) {
                session.setAttribute("loginError", null);
                modelMap.put("errorLogin", loginErrors.toString());
            }
        }

        return Template.LOGIN;
    }

    /* Normally, this request takes no parameters -- then view is displayed `as it is`.
     * The two parameters are used in case of errors, to convey the appropriate message to the user:
     *  - passwordCorrect=false, when wrong password is provided,
     *  - deleteSuccess=false, when, by some reason, transactional operations on data stored in database tables fail.
     *
     *  Situation when the parameters have value `true` is not predicted/anticipated here, in that case program runs
     *  normally and information about the success is displayed to the user elsewhere (in separate view, see
     *  deleteProfile method in UserController).
     */
    @RequestMapping(WebPath.DELETE_PROFILE)
    public ModelAndView deleteProfile(@RequestParam(value = "passwordCorrect", required = false) Boolean passwordCorrect,
                                      @RequestParam(value = "deleteSuccess", required = false) Boolean deleteSuccess) {

        // Create model and view with delete profile name.
        ModelAndView dPmav = new ModelAndView(Template.DELETE_PROFILE);

        // Value `true` is not considered here, it could be resolved by `!= null` alone.
        if (passwordCorrect != null && passwordCorrect == false) {
            // Add object to this template.
            dPmav.addObject("passwordCorrect", passwordCorrect);
        }

        // Value `true` is not considered here, it could be resolved by `!= null` alone.
        if (deleteSuccess != null && deleteSuccess == false) {
            // Add object to this template.
            dPmav.addObject("deleteFailure", deleteSuccess);
        }

        // Return ModelAndView for delete profile.
        return dPmav;
    }

    /**
     * This view (template) is returned on successful deletion of a profile.
     * It is rather separate view than toast because the user may want to: (1) go to home page;
     * (2) sign in with another account. Moreover, by separate view user is reassured that his/her account
     * was deleted once for all.
     */
    @RequestMapping(WebPath.PROFILE_DELETED)
    public String profileDeleted(Principal principal, HttpServletRequest request, HttpSession session) {

        return Template.PROFILE_DELETED;
    }

    private String getRedirectUriFromRequest(HttpServletRequest request) {
        String redirectUriFromReferrer = getRedirectUriFromReferrer(request);

        if (redirectUriFromReferrer != null) {
            return redirectUriFromReferrer;
        }
        return null;
    }

    private String getRedirectUriFromReferrer(HttpServletRequest request) {
        String referrer = request.getHeader(HttpHeaders.REFERER);
        if (referrer != null) {
            return filterRedirectUri(request, referrer);
        }
        return null;
    }

    private String filterRedirectUri(HttpServletRequest request, String candidate) {
        String appBaseUrl = request.getScheme() + "://" + request.getServerName();
        if (candidate != null) {
            if (!candidate.equals(appBaseUrl) && candidate.startsWith(appBaseUrl)) {
                /* Further referrer filtering */
                if (candidate.equals(appBaseUrl + WebPath.LOGIN)) {
                    return null;
                } else if (candidate.equals(appBaseUrl + WebPath.ERROR)) {
                    return null;
                } else if (candidate.equals(appBaseUrl + WebPath.CHAT_PATH)) {
                    return null;
                } else if (candidate.equals(appBaseUrl + WebPath.USER_MESSAGES_PATH)) {
                    return null;
                } else if (candidate.equals(appBaseUrl + WebPath.USER_NOTIFICATIONS_PATH)) {
                    return null;
                }
                return candidate;
            }
        }
        return null;
    }

    @GetMapping(WebPath.REGISTER)
    public String register(Principal principal, HttpServletRequest request, HttpServletResponse response,
                           HttpSession session, ModelMap modelMap) {

        return Template.REGISTER;
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ResponseEntity<String> register(@RequestBody AelUser user) {
        Boolean success = true;
        String msg = "", resp;

        if (!user.getPassword().equals(user.getConfirmPassword())) {
            success = false;
            msg = "duplicate_key";
        } else {
            if (defaultUserService.existsEmail(user.getEmail())) {
                success = false;
                msg = "email";
            } else {
                defaultUserService.createUserAccount(user);
            }
        }

        resp = "{\"success\": \"" + success + "\", \"msg\": \"" + msg + "\"}";

        return new ResponseEntity<String>(resp, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/checkEmail", method = RequestMethod.GET)
    public ResponseEntity<String> showMessage(String email) {
        Boolean success = true;
        String msg = "", resp;

        if (defaultUserService.existsEmail(email)) {
            success = false;
            msg = "email";
        }

        resp = "{\"success\": \"" + success + "\", \"msg\": \"" + msg + "\"}";

        return new ResponseEntity<String>(resp, HttpStatus.CREATED);
    }

    @RequestMapping(value ="/getComment", method = RequestMethod.POST)
    @ResponseBody
    public Map getCommentForCommentsTable( @RequestParam(name = "comment") String comment) {

        Map<String, Object> modelMap = new HashMap<>();
        modelMap.put("success", Boolean.TRUE);
        List<Comment>text=commentService.getAllComments();
        //List<Comment> allComments= commentService.getAllComments();
        modelMap.put("text", text);

        return modelMap;
    }

    @RequestMapping(value ="/getUsersForCity", method = RequestMethod.POST)
    @ResponseBody
    public Map getUsersCountForCity( @RequestParam(name = "cityName") String cityName) {

        Map<String, Object> modelMap = new HashMap<>();
        modelMap.put("success", Boolean.TRUE);
        Long allUsers=aelUserService.findUserCountForLocation(cityName);
        modelMap.put("userResponseCount", allUsers);

        return modelMap;
    }

    @RequestMapping(value ="/getUsersForCityCoordinates", method = RequestMethod.POST)
    @ResponseBody
    public Map getUsersForCityCoordinates( @RequestParam(name = "coordinates") String coordinates) {

        Map<String, Object> modelMap = new HashMap<>();
        modelMap.put("success", Boolean.TRUE);
        Long allUsers = 0L;
        if (coordinates != null) {
            allUsers = aelUserService.findUserCountForLocationCoordinates(coordinates);
        }
        modelMap.put("userResponseCount", allUsers);

        return modelMap;
    }

    @GetMapping(WebPath.FORGOT_PASSWORD)
    public String forgotPassword(Principal principal, HttpServletRequest request, HttpServletResponse response,
                                 HttpSession session, ModelMap modelMap) {

        return Template.FORGOT_PASSWORD;
    }

    @RequestMapping(value = "/forgotPassword", method = RequestMethod.POST)
    public ResponseEntity<String> forgotPassword(@RequestBody String email) {
        Boolean success = true;
        String msg = "", resp;

        if (!defaultUserService.existsEmail(email)) {
            success = false;
            msg = "email";
        } else {
            defaultUserService.resetAndSendPassword(email);
        }

        resp = "{\"success\": \"" + success + "\", \"msg\": \"" + msg + "\"}";

        return new ResponseEntity<String>(resp, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/resetPassword", method = RequestMethod.GET)
    public String resetPassword() {
        return WebPath.redirect(WebPath.FORGOT_PASSWORD);
    }

    @GetMapping(WebPath.CHANGE_PASSWORD)
    public String changePassword(Principal principal, HttpServletRequest request, HttpServletResponse response,
                                 HttpSession session, ModelMap modelMap) {

        return Template.CHANGE_PASSWORD;
    }

    @GetMapping("/accounts/{mail}/{token}/reset.do")
    public String resetPasswordForMailAndToken(@PathVariable("mail") String mail,
                                               @PathVariable("token") String resetToken,
                                               Model model) throws IOException {

        if (mail != null && !"".equals(mail) && resetToken != null) {
            //	----- resetare parola din AeL 7 facuta in 2 pasi; trimitere de 2 mail-uri -----
            //	defaultUserService.resetPasswordForMailAndToken(mail, resetToken);

            if (defaultUserService.checkMailAndTokenBeforeResetPassword(mail, resetToken)) {
                model.addAttribute("email", mail);

                return Template.RESET_PASSWORD;
            }
        }

        return WebPath.redirect(WebPath.LOGIN);
    }

    @GetMapping("/accounts/{username}/{registrationId}/activate.do")
    public String activateAccount(@PathVariable("username") String username,
                                  @PathVariable("registrationId") String registrationId) {
        if (username != null && !"".equals(username) && registrationId != null) {
            defaultUserService.activateAccount(username, registrationId);
        }

        return WebPath.redirect(WebPath.LOGIN);
    }


    @RequestMapping(value = WebPath.AGREE_TO_COOKIE_POLICY, method = RequestMethod.POST)
    public ResponseEntity<String> agreeToCookiesAndPrivacyPolicy(HttpServletResponse response) {


        Cookie cookiePrivacyPolicy = new Cookie(Cookies.COOKIE_POLICY_NAME, Cookies.COOKIE_POLICY_AGREE_VALUE);
        cookiePrivacyPolicy.setMaxAge(Cookies.COOKIE_POLICY_MAX_AGE);
        cookiePrivacyPolicy.setPath(Cookies.COOKIE_POLICY_PATH);
        response.addCookie(cookiePrivacyPolicy);



        return ResponseEntity
                .status(HttpStatus.CREATED)
                .build();
    }


    @GetMapping(WebPath.COOKIE_POLICY)
    public String getCookiePolicyPage(Principal principal, HttpServletRequest request, ModelMap model){
        addSessionLanguageToModelMap(principal, request, model);
        return Template.COOKIE_POLICY;
    }

    @GetMapping(WebPath.PRIVACY_POLICY)
    public String getPrivacyPolicyPage(Principal principal, HttpServletRequest request, ModelMap model){
        addSessionLanguageToModelMap(principal, request, model);
        return Template.PRIVACY_POLICY;
    }

    @GetMapping(WebPath.INFORMED_CONSENT)
    public String getInformedConsentPage(Principal principal, HttpServletRequest request, HttpSession session, ModelMap model){
        addSessionLanguageToModelMap(principal, request, model);
        return Template.INFORMED_CONSENT;
    }

    @GetMapping(WebPath.FAQ)
    public String getFaqPage(Principal principal, HttpServletRequest request, HttpSession session, ModelMap model){
        addSessionLanguageToModelMap(principal, request, model);
        return Template.FAQ;
    }

    private void addSessionLanguageToModelMap(Principal principal, HttpServletRequest request, ModelMap model) {
        String language = aelUserService.getLanguage(principal, request);
        model.put("sessionLanguage", language);
    }

    @RequestMapping(value = WebPath.SERVER_DATA, method = RequestMethod.GET)
    public ResponseEntity<String> serverData() {

        String gaTracking = "";
        Preference pref = preferencesService.getByName(Preferences.GLOBAL_AEL_SERVER_HOST.get());
        String serverName = pref.getValue();

        if(serverName.equals(WebPath.SERVER_NAME_TRY)) {
            gaTracking = "UA-122850346-1";
        }
        else if(serverName.equals(WebPath.SERVER_NAME_DEMO)) {
            gaTracking = "UA-122850346-2";
        }
        else if(serverName.equals(WebPath.SERVER_NAME_TEST)) {
            gaTracking = "UA-122850346-3";
        }
        else { /* do nothing */ }


        String resp = "{\"gaTracking\": \"" + gaTracking + "\"}";
        return new ResponseEntity<String>(resp, HttpStatus.CREATED);
    }

    @RequestMapping(value = WebPath.REPORT_INAPPROPRIATE, method = RequestMethod.POST)
    public ResponseEntity<String> reportInappropriate(@RequestBody String reportParams) {

        Boolean success = true;

        /* reportParams: URL | Course ID | Report Type | Report Message | Comment Data (User|Comment|Date) */

        String[] arr = reportParams.split("\\|");
        String strUrl = arr[0];
        Long courseId = Long.valueOf(arr[1]);
        if (strUrl != null && strUrl.contains("/courses/")) {
            strUrl = strUrl.replace("/courses/","/sessions/");
            List<PlannedCourse> plannedCourses = plannedCourseService.findByCourseId(courseId);
            if (plannedCourses != null && plannedCourses.size() > 0) {
                Long plannedCourseId = plannedCourses.get(0).getId();
                strUrl = strUrl.replace("/" + courseId, "/" + plannedCourseId);
                courseId = plannedCourseId;
            }
        }
        String strType = arr[2];
        String strMessage = arr[3];

        String mailTo = "";
        String mailSubject = "[Palette] Report Inappropriate";
        String mailBody = "Report Inappropriate <br /><br />" +
                "Item: " +
                "<ul>" +
                "<li>Url: " + strUrl + "</li>" +
                "<li>Type: " + strType + "</li>" +
                "<li>Message: " + strMessage + "</li>" +
                "</ul>";

        String strCommentUser = "";
        String strCommentText = "";
        String strCommentDate = "";
        String strCommentId = "";

        if(arr.length > 4) {
            strCommentUser = arr[4];
            strCommentText = arr[5];
            strCommentDate = arr[6];
            strCommentId = arr[7];

            String deleteInappropriateCommentAction = strUrl + "?deleteCommentId=" + strCommentId;

            mailBody = mailBody +
                    "Comment:" +
                    "<ul>" +
                    "<li>User: " + strCommentUser + "</li>" +
                    "<li>Text: " + strCommentText + "</li>" +
                    "<li>Date: " + strCommentDate + "</li>" +
                    "<li>Action: " + deleteInappropriateCommentAction + "</li>" +
                    "</ul>";
        }

        mailBody = mailBody + "<br />" + "Best Regards, <br /> Palette Team";

        /* get the appropriate moderator's email based on item's country */
        PlannedCourse plannedCourse = plannedCourseService.findOneById(courseId);

        /* TODO: get emails  from db */
        mailTo = getModeratorEmailByCountry(plannedCourse.getCountryCode());

        /* send the email */
        emailService.sendEmail(mailTo, mailSubject, mailBody);

        String resp = "{\"success\": \"" + success + "\"}";
        return new ResponseEntity<String>(resp, HttpStatus.CREATED);
    }

    @RequestMapping(value = WebPath.REVIEW_ORGANIZER, method = RequestMethod.POST)
    public ResponseEntity<String> reviewOrganizer(@RequestBody String val) {

        Boolean success = true;

        /* organizer */
        String[] arr = val.split("\\|");
        Long courseId = Long.valueOf(arr[0]);
        Boolean didOrganize = Boolean.valueOf(arr[1]);

        /* store organizer's review */
        PlannedCourse plannedCourse = plannedCourseService.findOneById(courseId);
        if(plannedCourse != null)
        {
            plannedCourse.setReviewed(didOrganize);
            plannedCourse = plannedCourseService.save(plannedCourse);
        }

        String resp = "{\"success\": \"" + success + "\"}";
        return new ResponseEntity<String>(resp, HttpStatus.CREATED);
    }

    @RequestMapping(value = WebPath.REVIEW_PARTICIPANT, method = RequestMethod.POST)
    public ResponseEntity<String> reviewParticipant(@RequestBody String val) {

        Boolean success = true;

        /* participant */
        String[] arr = val.split("\\|");
        Long userId = Long.valueOf(arr[0]);
        Boolean didParticipate = Boolean.valueOf(arr[1]);

        /* store participant's review */
        PlannedCourseStudent plannedCourseStudent = plannedCourseStudentService.findOneById(userId);
        if(plannedCourseStudent != null) {
            plannedCourseStudent.setReviewed(didParticipate);
            plannedCourseStudent = plannedCourseStudentService.saveOrUpdate(plannedCourseStudent);
        }

        String resp = "{\"success\": \"" + success + "\"}";
        return new ResponseEntity<String>(resp, HttpStatus.CREATED);
    }

    private String getModeratorEmailByCountry(String countryCode)
    {
        String email = "";
        if(countryCode.toUpperCase().equals("EN")) {
            email = "lea@prava-poteza.si";
        }
        else if(countryCode.toUpperCase().equals("NL")) {
            email = "office@re-flexion.eu";
        }
        else if(countryCode.toUpperCase().equals("SI")) {
            email = "lea@prava-poteza.si";
        }
        else if(countryCode.toUpperCase().equals("RO")) {
            email = "elisabetatoma26@gmail.com";
        }
        else if(countryCode.toUpperCase().equals("CH")) {
            email = "equipepalette@gmail.com";
        }
        else if(countryCode.toUpperCase().equals("PL")) {
            email = "palette@man.poznan.pl";
        }
        return email;
    }

    @RequestMapping(value = WebPath.DELETE_INAPPROPRIATE_COMMENT, method = RequestMethod.POST)
    public ResponseEntity<String> deleteInappropriateComment(@RequestBody String val) {

        Boolean success = true;

        Long commentId = Long.parseLong(val);

        /* save comment to comments_inappropriate table (history) */
        Comment c = commentService.get(commentId);
        CommentInappropriate ci = new CommentInappropriate();
        ci.setCommentId(commentId);
        ci.setCommentDate(c.getCommentDate());
        ci.setCommentText(c.getCommentText());
        ci.setPlannedCourse(c.getPlannedCourse());
        ci.setAelUser(c.getAelUser());
        commentInappropriateService.saveCommentInappropriate(ci);

        /* delete inappropriate comment */
        commentService.remove(commentId);

        String resp = "{\"success\": \"" + success + "\"}";
        return new ResponseEntity<String>(resp, HttpStatus.CREATED);
    }
}