package ro.siveco.ael.web.controllers;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.social.security.SocialAuthenticationToken;
import ro.siveco.ael.facebook.model.SocialAuthenticatedUser;
import ro.siveco.ael.lcms.domain.auth.model.AuthenticatedUser;
import ro.siveco.ael.lcms.domain.auth.model.User;
import ro.siveco.ram.model.Entity;
import ro.siveco.ram.starter.controller.BaseEntityController;
import ro.siveco.ram.starter.service.BaseEntityService;

import java.security.Principal;
import java.util.Locale;

/**
 * User: AlexandruVi
 * Date: 2017-04-26
 */
abstract public class WithUserBaseEntityController<ENTITY extends Entity<Long>> extends BaseEntityController<ENTITY> {

    public WithUserBaseEntityController(BaseEntityService<ENTITY> restService) {
        super(restService);
    }

    public static User getUser() {
        Principal principal = SecurityContextHolder.getContext().getAuthentication();

        if( principal instanceof SocialAuthenticationToken) {
            return ((SocialAuthenticatedUser)((SocialAuthenticationToken)principal).getPrincipal()).getUser();
        }

        return ((AuthenticatedUser) ((AbstractAuthenticationToken) principal).getPrincipal()).getUser();
       // return ((AuthenticatedUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUser();
    }

    public static Locale getLocale() {

        Principal principal = SecurityContextHolder.getContext().getAuthentication();

        if (principal instanceof SocialAuthenticationToken) {
            return ((SocialAuthenticatedUser) ((SocialAuthenticationToken) principal).getPrincipal()).getUser()
                    .getLocale();
        }
        return ((AuthenticatedUser) ((AbstractAuthenticationToken) principal).getPrincipal()).getUser().getLocale();
    }
}
