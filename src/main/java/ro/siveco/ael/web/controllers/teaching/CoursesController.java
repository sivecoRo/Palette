package ro.siveco.ael.web.controllers.teaching;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import ro.siveco.ael.lcms.domain.course.model.Course;
import ro.siveco.ael.lcms.domain.course.model.PlannedCourse;
import ro.siveco.ael.lcms.repository.CourseRepository;
import ro.siveco.ael.lcms.repository.PlannedCourseRepository;
import ro.siveco.ael.repository.specifications.course.TeacherCourseSpecification;
import ro.siveco.ael.repository.specifications.plannedCourse.TeacherCourseSessionsSpecification;
import ro.siveco.ael.web.components.Template;
import ro.siveco.ael.web.components.WebPath;
import ro.siveco.ael.web.controllers.WithUserBaseEntityController;

import java.security.Principal;
import java.util.List;

/**
 * Created by LiviuI on 3/7/2017.
 */
@Controller
public class CoursesController {

    private final CourseRepository courseRepository;
    private final PlannedCourseRepository plannedCourseRepository;

    @Autowired
    public CoursesController(CourseRepository courseRepository,
                             PlannedCourseRepository plannedCourseRepository) {
        this.courseRepository = courseRepository;
        this.plannedCourseRepository = plannedCourseRepository;
    }

    @GetMapping(WebPath.TEACHING_COURSE_LIST)
    public String list(Principal principal, Model model) {
        Pageable pageParams = getPageRequest();
        model.addAttribute("content",
                courseRepository
                        .findAll(new TeacherCourseSpecification(WithUserBaseEntityController.getUser()), pageParams));
        return Template.TEACHER_COURSE_LIST;
    }

    @GetMapping(WebPath.TEACHING_COURSE_VIEW + "/{courseId}")
    public String load(@PathVariable(name = "courseId") Long courseId, Principal principal, Model model) {
        Course course = courseRepository.findOne(courseId);
        List<PlannedCourse> sessions = plannedCourseRepository.findAll(
                new TeacherCourseSessionsSpecification(WithUserBaseEntityController.getUser(), course));
        model.addAttribute("course", course);
        model.addAttribute("sessions", sessions);
        return Template.TEACHER_COURSE_VIEW;
    }


    @GetMapping(WebPath.TEACHING_PATH + WebPath.COURSE + "/details/{sessionId}")
    public String loadSessionDetails(@PathVariable(name = "sessionId") Long sessionId, Model model) {
        PlannedCourse plannedCourse = plannedCourseRepository.get(sessionId);
        model.addAttribute("sessionData", plannedCourse);
        return "/teacher/session-details-panel";
    }


    @GetMapping(WebPath.TEACHING_PATH + "/session/add")
    public String addSession(Principal principal, Model model) {
        return "/teacher/session-add";
    }

    private PageRequest getPageRequest() {
        return new PageRequest(0, 255);
    }

}
