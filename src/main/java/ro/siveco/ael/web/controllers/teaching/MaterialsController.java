package ro.siveco.ael.web.controllers.teaching;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import ro.siveco.ael.web.components.Template;
import ro.siveco.ael.web.components.WebPath;

/**
 * Created by LiviuI on 3/21/2017.
 */
@Controller
public class MaterialsController {

    @GetMapping(WebPath.TEACHING_MATERIAL_LIST)
    public String view() {
        return Template.TEACHER_MATERIALS;
    }
}
