package ro.siveco.ael.web.controllers.notification;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.social.security.SocialAuthenticationToken;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ro.siveco.ael.facebook.model.SocialAuthenticatedUser;
import ro.siveco.ael.lcms.domain.auth.model.AelUser;
import ro.siveco.ael.lcms.domain.auth.model.AuthenticatedUser;
import ro.siveco.ael.lcms.domain.chat.web.ChatController;
import ro.siveco.ael.lcms.domain.notification.model.UserMessage;
import ro.siveco.ael.lcms.domain.notification.model.UserMessageModel;
import ro.siveco.ael.lcms.domain.notification.service.UserMessageService;
import ro.siveco.ael.lcms.domain.profile.model.UserDetails;
import ro.siveco.ael.lcms.domain.profile.service.UserDetailsService;
import ro.siveco.ael.service.ImageService;
import ro.siveco.ael.web.components.Template;
import ro.siveco.ael.web.components.WebPath;
import ro.siveco.ael.web.controllers.WithUserBaseEntityController;

import java.security.Principal;
import java.util.List;
import java.util.Optional;


@Controller
public class UserNotificationsController {

    private final UserMessageService userMessageService;
    private ChatController chatController;
    private ImageService imageService;
    private UserDetailsService userDetailsService;

    @Autowired
    public UserNotificationsController(UserMessageService userMessageService, ImageService imageService, UserDetailsService userDetailsService) {
        this.userMessageService = userMessageService;
        this.imageService = imageService;
        this.userDetailsService = userDetailsService;
    }

    @Autowired
    public void setChatController(ChatController chatController) {
        this.chatController = chatController;
    }

    public ChatController getChatController() {
        return chatController;
    }
    

    @GetMapping(WebPath.USER_NOTIFICATIONS_PATH)
    public String showAllNotifications(Principal principal, Model model) {
        List<UserMessageModel> notifications = userMessageService.getAllNotificationsByUserId(getUserId(principal), true);
        AelUser currentUser = (AelUser) WithUserBaseEntityController.getUser();
        Optional<UserDetails> userDetails = userDetailsService.getByAelUser(currentUser);

        model.addAttribute("notifications", notifications);
        model.addAttribute("userDisplayName", currentUser.getDisplayName());
        model.addAttribute("userInitials", currentUser.getInitials());
        if (userDetails.isPresent()) {
            model.addAttribute("userAvatarSrc", userDetails.get().getImage() == null ? null : imageService.imageSrcFromBytes(userDetails.get().getImage()));
        }

        return Template.USER_NOTIFICATIONS;
    }


    @RequestMapping(value = "/user_notifications/markAllAsRead", method = RequestMethod.POST)
    public ResponseEntity<String> markAllAsRead(Principal principal) {
    	userMessageService.markAllNotificationsAsRead(getUserId(principal));

        HttpHeaders headers = new HttpHeaders();
        return new ResponseEntity<String>(headers, HttpStatus.ACCEPTED);
    }


    @RequestMapping(value = "/user_notifications/markAsRead", method = RequestMethod.POST)
    public ResponseEntity<String> markAsRead(@RequestBody Long[] ids) {

        for (Long id : ids) {
        	userMessageService.markAsRead(id);

            UserMessage um = userMessageService.get(id);
            chatController.sendNavbarNumbers(um.getTo().getUsername());
        }

        HttpHeaders headers = new HttpHeaders();
        return new ResponseEntity<String>(headers, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/user_notifications/deleteMessages", method = RequestMethod.POST)
    public ResponseEntity<String> deleteMessages(@RequestBody Long[] ids) {

        for (Long id : ids) {
            UserMessage um = userMessageService.get(id);
            String toUsername = um.getTo().getUsername();

            userMessageService.deleteMessage(id);

            chatController.sendNavbarNumbers(toUsername);
        }

        HttpHeaders headers = new HttpHeaders();
        return new ResponseEntity<String>(headers, HttpStatus.CREATED);
    }
    

    private Long getUserId(Principal principal) {
        if( principal instanceof SocialAuthenticationToken) {
            return ((SocialAuthenticatedUser)((SocialAuthenticationToken)principal).getPrincipal()).getUser().getId();
        }

        return ((AuthenticatedUser) ((AbstractAuthenticationToken) principal).getPrincipal()).getUser().getId();
    }

}
