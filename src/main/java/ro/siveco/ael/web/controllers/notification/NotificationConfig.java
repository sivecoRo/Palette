package ro.siveco.ael.web.controllers.notification;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import ro.siveco.ael.lcms.domain.chat.web.ChatController;
import ro.siveco.ael.lcms.domain.notification.service.UserMessageService;
import ro.siveco.ael.lcms.repository.NotificationScheduleRepository;
import ro.siveco.ael.service.mail.EmailService;
import ro.siveco.ael.service.mail.utils.MailUtils;

@Configuration
@EnableScheduling
public class NotificationConfig {

	protected SessionFactory hibernateSessionFactory;
	private NotificationScheduleRepository notificationScheduleRepository;
	private UserMessageService userMessageService;
	private EmailService emailService;
	private MailUtils mailUtils;
    	private ChatController chatController;

    @Autowired
    public NotificationConfig(SessionFactory hibernateSessionFactory, NotificationScheduleRepository notificationScheduleRepository,
    								UserMessageService userMessageService, EmailService emailService, MailUtils mailUtils,
    								ChatController chatController) {
        this.hibernateSessionFactory = hibernateSessionFactory;
        this.notificationScheduleRepository = notificationScheduleRepository;
        this.userMessageService = userMessageService;
        this.emailService = emailService;
        this.mailUtils = mailUtils;
        this.chatController = chatController;
    }
    
//	@Bean
//	public NotificationsJobService job() {
//    	return new NotificationsJobService(hibernateSessionFactory, notificationScheduleRepository, userMessageService, emailService,
//    			mailUtils, chatController);
//	}

}
