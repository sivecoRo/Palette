package ro.siveco.ael.web.controllers.search;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ro.siveco.ael.service.search.SearchService;
import ro.siveco.ael.service.search.utils.SearchCategory;
import ro.siveco.ael.service.search.utils.SearchResult;
import ro.siveco.ael.web.components.Template;
import ro.siveco.ael.web.components.WebPath;

import java.security.Principal;
import java.util.Map;

/**
 * Created by razvann on 5/15/2017.
 */
@Controller
public class SearchController {

    private final SearchService searchService;

    @Autowired
    public SearchController(SearchService searchService) {
        this.searchService = searchService;
    }

    @GetMapping(path = WebPath.SEARCH_PATH)
    public String loadSearchResults(@RequestParam("search") String searchString, Model model, Principal principal) {
        Map<SearchCategory, SearchResult> results = searchService.loadSearchResults(principal, searchString);
        model.addAttribute("searchResults", results);
        return Template.SEARCH_RESULT;
    }
}
