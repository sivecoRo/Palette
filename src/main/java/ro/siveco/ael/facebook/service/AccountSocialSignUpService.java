package ro.siveco.ael.facebook.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionSignUp;
import org.springframework.social.connect.UserProfile;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.twitter.api.impl.TwitterTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import ro.siveco.ael.facebook.util.FacebookProfileUtil;
import ro.siveco.ael.lcms.domain.auth.model.User;
import ro.siveco.ael.lcms.domain.auth.service.UserService;

import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;
import java.util.UUID;

@Service
@Transactional
public class AccountSocialSignUpService implements ConnectionSignUp {

    private final UserService userService;

    private final HttpSession httpSession;
    protected final Log logger = LogFactory.getLog(getClass());

    @Autowired
    protected MessageSource messageSource;

    @Autowired
    public AccountSocialSignUpService(UserService userService, HttpSession httpSession) {
        this.userService = userService;
        this.httpSession = httpSession;
    }


    @Override
    public String execute(Connection<?> connection) {
        org.springframework.social.connect.UserProfile profile = null;
        if(connection.getApi() instanceof Facebook) {
            Facebook facebook = (Facebook) connection.getApi();
            String[] fields = {"id", "name", "first_name", "last_name", "email"};
            FacebookProfileUtil facebookProfileUtil = facebook.fetchObject("me", FacebookProfileUtil.class, fields);
            profile = new UserProfile(facebookProfileUtil.getId(), facebookProfileUtil.getName(),
                    facebookProfileUtil.getFirst_name(), facebookProfileUtil.getLast_name(),
                    facebookProfileUtil.getEmail(),
                    facebookProfileUtil.getFirst_name() + " " + facebookProfileUtil.getLast_name());
        }
        else
        {
            profile = connection.fetchUserProfile();
            if(connection.getApi() instanceof TwitterTemplate){
                TwitterTemplate twitterTemplate = (TwitterTemplate)connection.getApi();
                RestTemplate restTemplate = twitterTemplate.getRestTemplate();
                String response = restTemplate.getForObject("https://api.twitter.com/1.1/account/verify_credentials.json?include_email=true", String.class);
                String email = extractEmailFromResponse(response);
                profile=new UserProfile(profile.getId(), profile.getName(),profile.getFirstName(), profile.getLastName(), email, profile.getUsername());
            }
        }
        String userId = UUID.randomUUID().toString();
        User user = null;

        try{
           user = userService.createSocialUser(userId, profile);
        }
        catch (Exception e){
            logger.error("Eroare salvare utilizator: ", e);
            return null;
        }
        if(user != null)
        {
            return user.getUserSocialId();
        }
        return userId;
    }

    private String extractEmailFromResponse(String response){
        JSONObject jsonObject= null;
        String email =null;
        try {
            jsonObject = new JSONObject(response);
            email=(String)jsonObject.get("email");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return email;
    }
}
