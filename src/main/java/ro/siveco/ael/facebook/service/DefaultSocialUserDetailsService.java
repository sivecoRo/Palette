package ro.siveco.ael.facebook.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.social.security.SocialUserDetails;
import org.springframework.social.security.SocialUserDetailsService;
import org.springframework.stereotype.Service;
import ro.siveco.ael.facebook.model.SocialAuthenticatedUser;
import ro.siveco.ael.lcms.domain.auth.model.Role;
import ro.siveco.ael.lcms.domain.auth.model.User;
import ro.siveco.ael.lcms.domain.auth.service.PermissionService;
import ro.siveco.ael.lcms.domain.auth.service.UserService;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class DefaultSocialUserDetailsService implements SocialUserDetailsService {

    private UserDetailsService userDetailsService;

    @Autowired
    private PermissionService permissionService;

    private final UserService userService;

    @Autowired
    public DefaultSocialUserDetailsService(UserService userService) {
        this.userService = userService;
    }

    @Override
    public SocialUserDetails loadUserByUserId(String userId) throws UsernameNotFoundException {
        User user = userService.findOneByUserSocialId(userId);
        return new SocialAuthenticatedUser(user, permissionService.loadUserPermissions( user) );
    }

    private List<GrantedAuthority> getUserPermissions( User user ){
        List<GrantedAuthority> permissions = new ArrayList<>();

        List<Role> roles = user.getRoles();
        roles.stream().forEach(role -> {
            //add the role:
            permissions.add(new SimpleGrantedAuthority(role.getName()));
            //add each function assigned to this role:
            if (role.getFunctions() != null) {
                role.getFunctions().stream().forEach(function -> {
                    permissions.add(new SimpleGrantedAuthority(function.getName()));
                });
            }
        });

        return permissions;
    }
}
