package ro.siveco.ael.facebook.model;

import javax.persistence.*;

@Entity
@Table(name="userconnection")
@SequenceGenerator(name = "SEQ_GEN", sequenceName = "user_connections_seq")
public class UserConnection {

    @Id
    @GeneratedValue(generator = "SEQ_GEN", strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name="userid")
    private String userId;

    @Column(name="providerid")
    private String providerId;

    @Column(name="provideruserid")
    private String providerUserId;

    @Column(name="rank")
    private int rank;

    @Column(name="displayname")
    private String displayName;

    @Column(name="profileurl")
    private String profileUrl;

    @Column(name="imageurl")
    private String imageUrl;

    @Column(name="accesstoken")
    private String accessToken;

    @Column(name="secret")
    private String secret;

    @Column(name="refreshtoken")
    private String refreshToken;

    @Column(name="expiretime")
    private Long expireTime;

    public UserConnection(String userId, String providerId, String providerUserId, int rank, String displayName,
                          String profileUrl, String imageUrl, String accessToken, String secret, String refreshToken,
                          Long expireTime) {
        this.userId = userId;
        this.providerId = providerId;
        this.providerUserId = providerUserId;
        this.rank = rank;
        this.displayName = displayName;
        this.profileUrl = profileUrl;
        this.imageUrl = imageUrl;
        this.accessToken = accessToken;
        this.secret = secret;
        this.refreshToken = refreshToken;
        this.expireTime = expireTime;
    }

    public Long getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(Long expireTime) {
        this.expireTime = expireTime;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getProviderId() {
        return providerId;
    }

    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }

    public String getProviderUserId() {
        return providerUserId;
    }

    public void setProviderUserId(String providerUserId) {
        this.providerUserId = providerUserId;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getProfileUrl() {
        return profileUrl;
    }

    public void setProfileUrl(String profileUrl) {
        this.profileUrl = profileUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public String toString() {
        return
                "userId = " + userId +
                        ", providerId = " + providerId +
                        ", providerUserId = " + providerUserId +
                        ", rank = " + rank +
                        ", displayName = " + displayName +
                        ", profileUrl = " + profileUrl +
                        ", imageUrl = " + imageUrl +
                        ", accessToken = " + accessToken +
                        ", secret = " + secret +
                        ", refreshToken = " + refreshToken +
                        ", expireTime = " + expireTime;
    }
}