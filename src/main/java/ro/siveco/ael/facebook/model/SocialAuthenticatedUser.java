package ro.siveco.ael.facebook.model;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.social.security.SocialUser;
import ro.siveco.ael.lcms.domain.auth.model.Role;
import ro.siveco.ael.lcms.domain.auth.model.User;

import java.util.Collection;
import java.util.List;

/**
 * Created by AlinD on 9/28/2016.
 */
public class SocialAuthenticatedUser extends SocialUser{

    private User user;

    public SocialAuthenticatedUser(User user) {
        super(user.getUserSocialId(), user.getPassword(), AuthorityUtils.createAuthorityList());
        this.user=user;
    }


    public SocialAuthenticatedUser(User user, Collection<? extends GrantedAuthority> authorities) {
        super(user.getUserSocialId(), user.getPassword(), authorities);
        this.user=user;
    }

    @Override
    public boolean isEnabled() {
        return !user.getDisabled();
    }

    @Override
    public boolean isAccountNonLocked() {
        return !user.getDisabled();
    }


    public User getUser() {
        return user;
    }

    public Long getId() {
        return user.getId();
    }

    public List<Role> getRoles() {
        return user.getRoles();
    }
}
