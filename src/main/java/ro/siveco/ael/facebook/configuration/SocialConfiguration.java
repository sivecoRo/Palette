package ro.siveco.ael.facebook.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.security.crypto.encrypt.Encryptors;
import org.springframework.security.oauth2.client.token.grant.code.AuthorizationCodeResourceDetails;
import org.springframework.social.UserIdSource;
import org.springframework.social.config.annotation.ConnectionFactoryConfigurer;
import org.springframework.social.config.annotation.EnableSocial;
import org.springframework.social.config.annotation.SocialConfigurer;
import org.springframework.social.connect.ConnectionFactoryLocator;
import org.springframework.social.connect.UsersConnectionRepository;
import org.springframework.social.connect.jdbc.JdbcUsersConnectionRepository;
import org.springframework.social.facebook.connect.FacebookConnectionFactory;
import org.springframework.social.google.connect.GoogleConnectionFactory;
import org.springframework.social.security.AuthenticationNameUserIdSource;
import org.springframework.social.twitter.connect.TwitterConnectionFactory;
import ro.siveco.ael.facebook.service.AccountSocialSignUpService;
import ro.siveco.ael.lcms.domain.auth.service.UserService;

import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

@Configuration
@EnableSocial
public class SocialConfiguration implements SocialConfigurer {

    @Autowired
    private DataSource dataSource;

    @Autowired
    private UserService userService;

    @Autowired
    HttpSession httpSession;


    @Override
    public void addConnectionFactories(ConnectionFactoryConfigurer connectionFactoryConfigurer, Environment environment) {
        FacebookConnectionFactory fcf = new FacebookConnectionFactory(
                environment.getProperty("facebook.client.clientId"),
                environment.getProperty("facebook.client.clientSecret"));
        fcf.setScope("public_profile,email");
        connectionFactoryConfigurer.addConnectionFactory(fcf);

        TwitterConnectionFactory tcf = new TwitterConnectionFactory(environment.getProperty("twitter.client.clientId"),
                environment.getProperty("twitter.client.clientSecret"));
        connectionFactoryConfigurer.addConnectionFactory(tcf);

        GoogleConnectionFactory gcf = new GoogleConnectionFactory(environment.getProperty("google.client.clientId"),
                environment.getProperty("google.client.clientSecret"));
        gcf.setScope(
                "https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo#email https://www.googleapis.com/auth/plus.me");
        connectionFactoryConfigurer.addConnectionFactory(gcf);
    }

    @Bean
    @ConfigurationProperties("facebook.client")
    public AuthorizationCodeResourceDetails facebook() {
        return new AuthorizationCodeResourceDetails();
    }

    @Override
    public UserIdSource getUserIdSource() {
        return new AuthenticationNameUserIdSource();
    }

    @Override
    public UsersConnectionRepository getUsersConnectionRepository(ConnectionFactoryLocator connectionFactoryLocator) {
        JdbcUsersConnectionRepository repository = new JdbcUsersConnectionRepository(dataSource,connectionFactoryLocator, Encryptors
                .noOpText());
        repository.setConnectionSignUp(new AccountSocialSignUpService(userService, httpSession));
        return repository;
    }
}
