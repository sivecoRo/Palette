package ro.siveco.ael.properties.exporter.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.*;
import org.springframework.stereotype.Service;
import ro.siveco.ael.properties.OrderedProperties;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.*;

/**
 * Created by AndradaC on 7/1/2017.
 */
@Service
public class PropertiesExporterService {
    private static Log log = LogFactory.getLog(PropertiesExporterService.class);

    // Pentru conversia aliasurilor coloanelor
    private String convertColumnAliasesFromCharset = null;
    private String convertColumnAliasesToCharset = null;

    public void populateSheetWithSheetData( HSSFWorkbook workbook, String sheetName, String sourceFolderName, Set skipFolderNames, Set skippedLocalesSet )
            throws IOException
    {
        List<String> filesNames = listFilesAndFolders( sourceFolderName, skipFolderNames );
        Map<String, Map<String, Map<String, String>>> dataMap = new LinkedHashMap<>();
        Set<String> localeSet = new HashSet<String>();

        for( String fileName : filesNames )
        {
            if( fileName.indexOf( "_", 0 ) == -1 )
            {
                continue;
            }

            String strippedFileName = fileName.substring(sourceFolderName.length(), fileName.indexOf("_", 0));
            Map<String, Map<String, String>> propertyKeysMap;

            if( !dataMap.containsKey( strippedFileName ) )
            {
                propertyKeysMap = new LinkedHashMap<>();
                dataMap.put( strippedFileName, propertyKeysMap );
            }
            else
            {
                propertyKeysMap = dataMap.get( strippedFileName );
            }

            String locale = getPropertyFileLocaleName( fileName );
            if( !skippedLocalesSet.contains( locale ) )
            {
                if( !localeSet.contains( locale ) )
                {
                    localeSet.add( locale );
                }

                OrderedProperties p = new OrderedProperties();
                p.load( new FileInputStream( fileName ) );
                Enumeration<String> keysEnumeration = p.propertyNames();
                while( keysEnumeration.hasMoreElements() )
                {
                    String key = keysEnumeration.nextElement();
                    Map<String, String> localeValueMap;

                    if( !propertyKeysMap.containsKey( key ) )
                    {
                        localeValueMap = new HashMap<>();
                        propertyKeysMap.put( key, localeValueMap );
                    }
                    else
                    {
                        localeValueMap = propertyKeysMap.get( key );
                    }

                    String value = ( String ) p.getProperty( key );
                    localeValueMap.put( locale, value );
                }

            }
        }

        List<String[]> rows = new ArrayList<String[]>();
        String[] firstRow = new String[2 + localeSet.size()];
        firstRow[0] = "File path";
        firstRow[1] = "Property key";
        int i = 2;

        Map<String,Integer> localeColumnMap = new HashMap<String,Integer>();
        for( String localeName : localeSet )
        {
            firstRow[i++] = "Property value for " + localeName;
            localeColumnMap.put( localeName, i );
        }
        rows.add( firstRow );

        for( String fileName : dataMap.keySet() )
        {
            Map<String, Map<String, String>> keysMap = dataMap.get( fileName );
            for( String key : keysMap.keySet() )
            {
                Map<String, String> localesMap = keysMap.get( key );
                String[] row = new String[2 + localeSet.size()];
                row[0] = fileName;
                row[1] = key;
                int index = 2;
                for( String localeColumnKey : localesMap.keySet() )
                {
                    row[localeColumnMap.get( localeColumnKey ) - 1] = localesMap.get( localeColumnKey );
                }

                rows.add( row );
            }

        }
        writeSheet( workbook, sheetName, rows.toArray( new String[rows.size()][2 + localeSet.size()] ) );
    }

    private List<String> listFilesAndFolders( String sourceFolderName, Set skipFolderNames )
    {
        File sourceFolder = new File( sourceFolderName );
        List<String> fileNamesList = new ArrayList<String>();

        if( !sourceFolder.exists() || !sourceFolder.isDirectory() )
        {
            throw new RuntimeException( sourceFolder + " is not a directory!" );
        }

        File[] fileArray = sourceFolder.listFiles();
        for( File file : fileArray )
        {
            if( file.isDirectory() )
            {
                continue;
            }
            else
            {
                if(file.getPath().endsWith(".properties")) {
                    fileNamesList.add(file.getPath());
                }
            }
        }
        return fileNamesList;
    }


    private String getPropertyFileLocaleName( String fileName )
    {
        return fileName.substring( 1 + fileName.indexOf( "_" ), fileName.indexOf( ".properties" ) );
    }

    // Scrie o celula intr-un rand
    private void writeCell( HSSFRow row, short cellNum, String cellData )
    {
        HSSFCell cell = row.createCell(cellNum);
        if( cellData == null )
        {
            cell.setCellValue( new HSSFRichTextString( "" ) );
        }
        else
        {
            cell.setCellValue( new HSSFRichTextString( cellData ) );
        }
    }

    // Scrie un rand intr-o foaie de lucru
    private void writeRow( HSSFSheet sheet, int rowNum, String[] rowData )
    {
        HSSFRow row = sheet.createRow(rowNum);

        for( short cellNum = 0; cellNum < rowData.length; cellNum++ )
        {
            String cellData = rowData[cellNum];

            if( ( rowNum == 0 ) && ( cellData != null ) &&
                    ( convertColumnAliasesFromCharset != null ) &&
                    ( convertColumnAliasesToCharset != null ) )
            {
                try
                {
                    cellData = new String( cellData.getBytes( convertColumnAliasesFromCharset ),
                            convertColumnAliasesToCharset );
                }
                catch( UnsupportedEncodingException e )
                {
                    log.error( "Unsupported encoding for column header cell " +
                            ( cellNum + 1 ) + ".", e );
                }
            }

            writeCell(row, cellNum, cellData);
        }
    }

    // Scrie o foaie de lucru intr-un excel
    private void writeSheet( HSSFWorkbook workbook,
                             String sheetName, String[][] sheetData )
    {
        HSSFSheet sheet = workbook.createSheet( sheetName );

        for( int rowNum = 0; rowNum < sheetData.length; rowNum++ )
        {
            writeRow( sheet, rowNum, sheetData[rowNum] );
        }
    }
}
