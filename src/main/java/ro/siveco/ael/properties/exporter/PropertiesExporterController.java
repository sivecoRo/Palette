package ro.siveco.ael.properties.exporter;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ro.siveco.ael.properties.exporter.service.PropertiesExporterService;
import ro.siveco.ael.web.components.Template;
import ro.siveco.ael.web.components.WebPath;

import javax.inject.Inject;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.Principal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

/**
 * Created by AndradaC on 7/1/2017.
 */
@Controller
public class PropertiesExporterController {
    private final PropertiesExporterService propertiesExporterService;

    @Inject
    public PropertiesExporterController(PropertiesExporterService propertiesExporterService)
    {
        this.propertiesExporterService = propertiesExporterService;
    }

    @GetMapping(WebPath.PROPERTIES_EXPORTER_PATH)
    public String loadPropertiesExporterPage(Principal principal, Model model) {
        return Template.PROPERTIES_EXPORTER;
    }

    @RequestMapping(value="/exportProperties", method= RequestMethod.POST)
    @ResponseBody
    public Map exportPropertiesToExcel(@RequestParam(value = "destinationFilePath", required = true) String destinationFilePath,
                                        Model model)
    {
        String sheetName = "translations";

        //calea unde se gasesc fisierele properties
        String propertiesSourcePath = (String)System.getProperty("user.dir") + "\\src\\main\\resources";

        Map<String, Object> modelMap = new HashMap<>();

        HSSFWorkbook hssfWorkbook = new HSSFWorkbook();
        try {
            propertiesExporterService.populateSheetWithSheetData(hssfWorkbook, sheetName, propertiesSourcePath, new HashSet(Arrays
                    .asList(".svn")), new HashSet());
            FileOutputStream fos = new FileOutputStream(new File(destinationFilePath));
            hssfWorkbook.write(fos);
            fos.close();

        } catch (IOException e) {
            modelMap.put("success", Boolean.FALSE);
            modelMap.put("resultMsg", "texts.export.error");
            return modelMap;
        }

        modelMap.put("success", Boolean.TRUE);
        modelMap.put("resultMsg", "texts.exported.successfully");
        return modelMap;
    }



}
