package ro.siveco.ael.properties.importer;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import ro.siveco.ael.properties.importer.service.PropertiesImporterService;
import ro.siveco.ael.web.components.Template;
import ro.siveco.ael.web.components.WebPath;

import javax.inject.Inject;
import java.io.IOException;
import java.io.InputStream;
import java.security.Principal;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by AndradaC on 7/2/2017.
 */
@Controller
public class PropertiesImporterController {
    private final PropertiesImporterService propertiesImporterService;

    @Inject
    public PropertiesImporterController(PropertiesImporterService propertiesImporterService) {
        this.propertiesImporterService = propertiesImporterService;
    }

    @GetMapping(WebPath.PROPERTIES_IMPORTER_PATH)
    public String loadPropertiesImporterPage(Principal principal, Model model) {
        return Template.PROPERTIES_IMPORTER;
    }

    @RequestMapping(value="/importProperties", method= RequestMethod.POST)
    @ResponseBody
    public Map importProperties(MultipartFile importFile)
    {
        Map<String, Object> modelMap = new HashMap<>();
        try {
            InputStream is = importFile.getInputStream();

            //calea unde se gasesc fisierele properties
            String propertiesPath = (String)System.getProperty("user.dir") + "\\src\\main\\resources";
            Map<String, Map<String, String>> map = propertiesImporterService.retrieveDataFromXLS(is);
            propertiesImporterService.updateResourceBundles( map, propertiesPath );
        } catch (IOException e) {
            e.printStackTrace();
            modelMap.put("success", Boolean.FALSE);
            modelMap.put("resultMsg", "texts.import.error");
            return modelMap;
        }

        modelMap.put("success", Boolean.TRUE);
        modelMap.put("resultMsg", "texts.imported.successfully");
        return modelMap;
    }
}
