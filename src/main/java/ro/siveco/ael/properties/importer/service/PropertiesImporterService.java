package ro.siveco.ael.properties.importer.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.springframework.stereotype.Service;
import ro.siveco.ael.properties.OrderedProperties;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

/**
 * Created by AndradaC on 7/2/2017.
 */
@Service
public class PropertiesImporterService {
    private static Log log = LogFactory.getLog(PropertiesImporterService.class);

    public Map<String, Map<String, String>> retrieveDataFromXLS( InputStream content ) throws IOException
    {
        POIFSFileSystem fs = new POIFSFileSystem( content );
        HSSFWorkbook wb = new HSSFWorkbook( fs );
        HSSFSheet sheet = wb.getSheetAt( 0 );
        Map<Integer, String> localePositionMap = new HashMap<Integer, String>();
        Map<String, Map<String, String>> dataMap = new TreeMap<String, Map<String, String>>();

        Iterator rowIterator = sheet.rowIterator();

        if( rowIterator.hasNext() )
        {
            HSSFRow row = ( HSSFRow ) rowIterator.next();
            short index = 2;

            HSSFCell cell;
            while( ( row.getCell( index ) != null ) && ( ( cell = row.getCell( index ) ).getCellType() != HSSFCell.CELL_TYPE_BLANK ) )
            {
                localePositionMap.put( ( int ) index, cell.getStringCellValue().substring( "Property value for ".length() ) );
                index++;
            }
        }
        else
        {
            throw new IllegalArgumentException( "Fisierul .xls nu contine date." );
        }

        while( rowIterator.hasNext() )
        {
            HSSFRow row = ( HSSFRow ) rowIterator.next();
            HSSFCell fileNameCell = row.getCell( ( short ) 0 );
            HSSFCell propertyKeyCell = row.getCell( ( short ) 1 );
            if( fileNameCell == null )
            {
                break;
            }

            String strippedFileName = fileNameCell.getStringCellValue();
            Map<String, String> propertyKeysMap;

            short index = 2;
            while( ( row.getCell( index ) != null ) && ( ( row.getCell( index ) ).getCellType() != HSSFCell.CELL_TYPE_BLANK ) )
            {
                String fileName = strippedFileName + "_" + localePositionMap.get( ( int ) index ) + ".properties";

                if( !dataMap.containsKey( fileName ) )
                {
                    propertyKeysMap = new TreeMap<String, String>();
                    dataMap.put( fileName, propertyKeysMap );
                }
                else
                {
                    propertyKeysMap = dataMap.get( fileName );
                }

                propertyKeysMap.put( propertyKeyCell.getStringCellValue(), row.getCell( index ).getStringCellValue() );
                index++;
            }
        }


        return dataMap;
    }

    public void updateResourceBundles( Map<String, Map<String, String>> dataMap, String destinationFolderName ) throws IOException
    {
        Set<String> fileList = dataMap.keySet();
        int messages = 0,files = 0;

        for( String fileName : fileList )
        {
            File dir = new File(destinationFolderName.substring( 0, destinationFolderName.length() - 1 ));
            System.out.println(destinationFolderName);
            if( !dir.exists() )
            {
                dir.mkdirs();
            }
            File file = new File( destinationFolderName + fileName );

            if( !file.exists() )
            {
                Path path = Paths.get(destinationFolderName + fileName);
                Files.createFile(path);
//                if(!file.createNewFile())
//                {
//                    throw new FileNotFoundException( "Fisierul cu numele " + file.getName()  + " nu exista si nu a putut fi creat");
//                }
            }

            Set<String> keySet = dataMap.get( fileName ).keySet();

            OrderedProperties p = new OrderedProperties();
            p.load( new FileInputStream( file ) );

            for( String key : keySet )
            {
                String value = dataMap.get(fileName).get(key);
                if (value != null && value.contains("{") && value.contains("}") && value.contains("'")) {
                    value = value.replace("'","''");
                }
                if( p.containsProperty(key) )
                {
                    if( !p.getProperty(key).equals(value ) )
                    {
                        messages++;
                        System.out.println("Am inlocuit proprietatea " + key + " in fisierul " + file );
                        p.setProperty(key, value);
                    }
                }
                else
                {
                    messages++;
                    System.out.println("Am adaugat proprietatea " + key + " in fisierul " + file );
                    p.setProperty( key, value);
                }
            }

            p.store( new FileOutputStream( file ), null );
            files++;

        }

        System.out.println("Am inlocuit  " + messages + " mesaje in " + files + " de fisiere" );
    }
}
