package ro.siveco.ael.config;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import ro.siveco.ael.web.components.WebPath;
import ro.siveco.ram.starter.util.DomainMapController;

/**
 * User: AlexandruVi
 * Date: 2017-09-08
 */
@Controller
@RequestMapping(WebPath.API_WEB)
public class ApiController extends DomainMapController {

}
