package ro.siveco.ael.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.siveco.ael.lcms.domain.auth.model.AelFunction;
import ro.siveco.ael.lcms.domain.auth.service.AelFunctionService;
import ro.siveco.ram.security.permission.ManagementAction;
import ro.siveco.ram.starter.util.DomainMap;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by dianan on 10/25/2017.
 */

@Service
public class GenerateFunctionFromEntity {

    @Autowired
    private DomainMap domainMap;

    @Autowired
    private AelFunctionService functionService;

    public void generateFunctionsForEntities() {

        List<AelFunction> newFunctions = new ArrayList<AelFunction>();
        List<ManagementAction> permissions = new ArrayList<>(Arrays.asList(ManagementAction.LIST,
                ManagementAction.CREATE, ManagementAction.READ, ManagementAction.UPDATE, ManagementAction.DELETE));

        domainMap.getRestControllers().forEach((modelName, restController) -> {
            permissions.forEach(permission -> {
                String fName = "type:ENTITY_MANAGEMENT:" + modelName + ":" + permission;
                AelFunction f = functionService.findByName(fName);
                if (f == null) {
                    f = new AelFunction();
                    f.setName(fName);
                    newFunctions.add(f);
                }
            });
        });
        functionService.save(newFunctions);
    }
}
