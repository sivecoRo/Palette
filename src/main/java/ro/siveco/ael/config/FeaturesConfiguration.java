package ro.siveco.ael.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Configuration storing info whether some features are enabled or not
 * @author maciejb
 */
@Configuration
@ConfigurationProperties(prefix = "features")
public class FeaturesConfiguration {


    private boolean profileVerificationEnabled;


    public boolean isProfileVerificationEnabled() {
        return profileVerificationEnabled;
    }

    public boolean isProfileVerificationDisabled() {
        return !isProfileVerificationEnabled();
    }

    public void setProfileVerificationEnabled(boolean profileVerificationEnabled) {
        this.profileVerificationEnabled = profileVerificationEnabled;
    }
}
