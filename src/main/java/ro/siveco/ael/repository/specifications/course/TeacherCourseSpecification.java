package ro.siveco.ael.repository.specifications.course;

import ro.siveco.ael.lcms.domain.auth.model.User;
import ro.siveco.ael.lcms.domain.course.model.Course;
import ro.siveco.ael.repository.specifications.generic.GenericTenantAwareSpecification;

import javax.persistence.criteria.*;

/**
 * Created by LiviuI on 3/7/2017.
 */
public class TeacherCourseSpecification extends GenericTenantAwareSpecification<Course> {

    public TeacherCourseSpecification(User teacher) {
        super(teacher);
    }

    @Override
    public Predicate buildPredicate(Root<Course> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
        return cb.equal(root.get("creator"), user);
    }

    @Override
    protected Path<Course> tenantPath(Root<Course> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
        return root.get("creator").get("tenant");
    }

}
