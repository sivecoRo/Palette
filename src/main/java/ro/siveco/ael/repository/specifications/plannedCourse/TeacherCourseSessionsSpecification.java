package ro.siveco.ael.repository.specifications.plannedCourse;

import ro.siveco.ael.lcms.domain.auth.model.User;
import ro.siveco.ael.lcms.domain.course.model.Course;
import ro.siveco.ael.lcms.domain.course.model.PlannedCourse;
import ro.siveco.ael.repository.specifications.generic.GenericTenantAwareSpecification;

import javax.persistence.criteria.*;

/**
 * Created by LiviuI on 3/8/2017.
 */
public class TeacherCourseSessionsSpecification extends GenericTenantAwareSpecification<PlannedCourse> {

    private Course course;

    public TeacherCourseSessionsSpecification(User teacher, Course course) {
        super(teacher);
        this.course = course;
    }

    @Override
    public Predicate buildPredicate(Root<PlannedCourse> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
        //@TODO: adaugat filtrele din interfata
        return cb.and(
                cb.equal(root.get("teacher").get("user"), user),
                cb.equal(root.get("course"), course)
        );
    }

    @Override
    protected Path<PlannedCourse> tenantPath(Root<PlannedCourse> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
        return root.get("teacher").get("user").get("tenant");
    }
}
