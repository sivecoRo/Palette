package ro.siveco.ael.repository.specifications.generic;

import ro.siveco.ael.lcms.domain.auth.model.User;
import ro.siveco.ael.lcms.domain.tenant.model.Tenant;

import javax.persistence.criteria.*;

/**
 * Created by LiviuI on 3/28/2017.
 */
public abstract class GenericTenantAwareSpecification<T> extends GenericSpecification<T> {

    protected User user;

    public GenericTenantAwareSpecification(User user) {
        this.user = user;
    }

    @Override
    public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
        Predicate main = buildPredicate(root, query, cb);
        Predicate tenant = tenantPredicate(root, query, cb);
        return cb.and(main, tenant);
    }

    protected Predicate tenantPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
        Path<T> path = tenantPath(root, query, cb);
        if (isTenantPresent()) {
            return cb.equal(path.get("id"), getTenantId());
        } else {
            return cb.isNull(path);
        }
    }

    protected Path<T> tenantPath(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
        return root.get("user").get("tenant");
    }

    protected Long getTenantId() {
        Tenant tenant = user.getTenant();
        if (tenant != null) {
            return tenant.getId();
        }
        return null;
    }

    private boolean isTenantPresent() {
        return getTenantId() != null;
    }

}
