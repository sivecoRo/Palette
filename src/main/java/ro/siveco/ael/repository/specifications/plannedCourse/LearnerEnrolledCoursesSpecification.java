package ro.siveco.ael.repository.specifications.plannedCourse;

import ro.siveco.ael.lcms.domain.auth.model.User;
import ro.siveco.ael.lcms.domain.course.model.PlannedCourse;
import ro.siveco.ael.lcms.domain.course.model.PlannedCourseStudent;
import ro.siveco.ael.repository.specifications.generic.GenericTenantAwareSpecification;

import javax.persistence.criteria.*;

/**
 * Created by LiviuI on 3/28/2017.
 */
public class LearnerEnrolledCoursesSpecification extends GenericTenantAwareSpecification<PlannedCourse> {

    public LearnerEnrolledCoursesSpecification(User learner) {
        super(learner);
    }

    @Override
    public Predicate buildPredicate(Root<PlannedCourse> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
        From<PlannedCourse, PlannedCourseStudent> from = root.join("plannedCourseStudents");
        return cb.equal(from.get("componentUserJob").get("user"), user.getId());
    }

    /* nu e nevoie de tenant filter aici */
}
