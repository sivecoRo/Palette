package ro.siveco.ael.repository.specifications.generic;

import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 * Created by LiviuI on 3/28/2017.
 */
public abstract class GenericSpecification<T> implements Specification<T> {

    @Override
    public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
        return buildPredicate(root, query, cb);
    }

    public abstract Predicate buildPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder cb);
}
