package ro.siveco.ael.repository.specifications.survey;

import ro.siveco.ael.lcms.domain.auth.model.User;
import ro.siveco.ael.lcms.domain.materials.model.Survey;
import ro.siveco.ael.repository.specifications.generic.GenericTenantAwareSpecification;

import javax.persistence.criteria.*;

/**
 * Created by AndradaC on 4/27/2017.
 */
public class SurveySpecification extends GenericTenantAwareSpecification<Survey> {

    public SurveySpecification(User creator) {
        super(creator);
    }

    @Override
    public Predicate buildPredicate(Root<Survey> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
        return cb.or(
                cb.equal(root.get("creator"), user),
                cb.equal(root.get("isPublic"), Boolean.TRUE)
        );
    }

    @Override
    protected Path<Survey> tenantPath(Root<Survey> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
        return root.get("creator").get("tenant");
    }
}
