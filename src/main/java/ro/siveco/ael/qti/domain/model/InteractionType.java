package ro.siveco.ael.qti.domain.model;

/**
 * Created by IuliaP on 13.08.2017.
 */
public enum InteractionType {
    SINGLE_CHOICE,
    MULTIPLE_CHOICE,
    ESSAY,
    SLIDER
}
