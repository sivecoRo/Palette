package ro.siveco.ael.service;

import org.apache.batik.transcoder.TranscoderException;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayOutputStream;

@Service
public class MultimediaService {
    private ImageService imageService;
	
	public MultimediaService(ImageService imageService) {
		this.imageService = imageService;
		
	}

	public ByteArrayOutputStream processThumbnail(MultipartFile file) throws TranscoderException {
		return imageService.processThumbnail(file);
	}

}
