package ro.siveco.ael.service.reporting.model;

import ro.siveco.ael.lcms.domain.materials.model.UserSurveyAnswer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IuliaP on 02.11.2017.
 */
public class SurveyReport{

    private List<UserSurveyAnswer> userSurveyAnswers = new ArrayList<>();

    public List<UserSurveyAnswer> getUserSurveyAnswers() {

        return userSurveyAnswers;
    }

    public void setUserSurveyAnswers(
            List<UserSurveyAnswer> userSurveyAnswers) {

        this.userSurveyAnswers = userSurveyAnswers;
    }
}
