package ro.siveco.ael.service.reporting;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.siveco.ael.lcms.domain.materials.service.UserSurveyAnswerService;
import ro.siveco.ael.service.BaseService;
import ro.siveco.ael.service.reporting.model.SurveyReport;

/**
 * User: LucianR
 * Date: 2017-09-19
 */

@Service
public class BaseReportingService extends BaseService {

    private UserSurveyAnswerService userSurveyAnswerService;

    @Autowired
    public BaseReportingService(UserSurveyAnswerService userSurveyAnswerService) {

        this.userSurveyAnswerService = userSurveyAnswerService;
    }

    private void createOrUpdateUserSurveyAnswers(SurveyReport surveyReport) {
        if( surveyReport != null ) {
            userSurveyAnswerService.save(surveyReport.getUserSurveyAnswers());
        }
    }
}
