package ro.siveco.ael.service;

import org.apache.batik.transcoder.TranscoderException;
import org.apache.batik.transcoder.TranscoderInput;
import org.apache.batik.transcoder.TranscoderOutput;
import org.apache.batik.transcoder.image.PNGTranscoder;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.imgscalr.Scalr;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;

/**
 * User: AlexandruVi
 * Date: 2016-11-02
 */
@Service
public class ImageService {

    private final Log log = LogFactory.getLog(getClass());

    @Value("${content.image.saveExtension}")
    private String saveExtension;

    @Value("${content.image.thumbnailSaveWidth}")
    private int thumbnailSaveWidth;

    @Value("${content.image.thumbnailSaveHeight}")
    private int thumbnailSaveHeight;

    @Value("${content.image.maxSaveWidth}")
    private int maxSaveWidth;

    @Value("${content.image.maxSaveHeight}")
    private int maxSaveHeight;

    public ByteArrayOutputStream processThumbnail(MultipartFile file) throws TranscoderException {
        return resizeImage(file, thumbnailSaveWidth, thumbnailSaveHeight);
    }

    public ByteArrayOutputStream resizeImage(MultipartFile file, int width, int height) throws TranscoderException {
        BufferedImage original = getImage(file, false);
        return resizeImage(original, width, height);
    }

    public ByteArrayOutputStream resizeImage(BufferedImage image, int width, int height) {
        BufferedImage newImage = null;
        try {
            newImage = resizeTo(image, width, height);
            return getImageBytes(newImage);
        } catch (IOException e) {
            throw new ServiceException("image.resize.failed");
        } finally {
            image.flush();
            if (newImage != null) {
                newImage.flush();
            }
        }
    }

    public ByteArrayOutputStream fitImage(BufferedImage image, int width, int height) {
        log.debug("[Image][original:" + image.getWidth() + "x" + image.getHeight() + " fit to:" + width + "x" + height + "]");
        if (image.getHeight() > image.getWidth()) {
            if (image.getHeight() > height) {
                image = resizeToHeight(image, height);
                log.debug("[Image][resizeToHeight:" + image.getWidth() + "x" + image.getHeight() + "]");
            }
            if (image.getWidth() > width) {
                image = resizeToWidth(image, width);
                log.debug("[Image][resizeToWidth:" + image.getWidth() + "x" + image.getHeight() + "]");
            }
        } else {
            if (image.getWidth() > width) {
                image = resizeToWidth(image, width);
                log.debug("[Image][resizeToWidth:" + image.getWidth() + "x" + image.getHeight() + "]");
            }
            if (image.getHeight() > height) {
                image = resizeToHeight(image, height);
                log.debug("[Image][resizeToHeight:" + image.getWidth() + "x" + image.getHeight() + "]");
            }
        }

        log.debug("[Image][save to:" + image.getWidth() + "x" + image.getHeight() + " fits to:" + width + "x" + height + "]");
        return getImageBytes(image);
    }

    public ByteArrayOutputStream getImageBytes(BufferedImage image) {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        try {
            ImageIO.write(image, saveExtension, output);
        } catch (IOException e) {
            throw new ServiceException("image.process.failed");
        }
        return output;
    }

    public ByteArrayOutputStream processImage(MultipartFile file, Boolean shouldConvert) throws TranscoderException {
        BufferedImage original = getImage(file, shouldConvert);
        return fitImage(original, maxSaveWidth, maxSaveHeight);
    }

    public ByteArrayOutputStream fitImage(BufferedImage bufferedImage){

        return fitImage(bufferedImage, maxSaveWidth, maxSaveHeight);

    }

    public BufferedImage getImage(MultipartFile file, Boolean shouldConvert) throws TranscoderException {
        try {
            byte[] bytes =file.getBytes();
            String currentExtension = FilenameUtils.getExtension(file.getOriginalFilename());
            saveExtension = shouldConvert?saveExtension:(currentExtension!=null?currentExtension:saveExtension);
            return (shouldConvert&&file.getContentType().contains("svg"))?convertSvgToPngForPuzzle(bytes):ImageIO.read(new ByteArrayInputStream(bytes));
        } catch (IOException e) {
            throw new ServiceException("image.get.from.multipart.failed");
        }

    }

    public BufferedImage convertSvgToPngForPuzzle(byte[] fileBytes) throws IOException, TranscoderException {

        TranscoderInput inputSvgImage = new TranscoderInput(new ByteArrayInputStream(fileBytes));
        ByteArrayOutputStream resultByteStream = new ByteArrayOutputStream();
        TranscoderOutput outputPngImage = new TranscoderOutput(resultByteStream);
        PNGTranscoder converter=new PNGTranscoder();
        converter.transcode(inputSvgImage, outputPngImage);
//        converter.writeImage(ImageIO.read(new ByteArrayInputStream(fileBytes)), outputPngImage);
        resultByteStream.flush();

        return ImageIO.read(new ByteArrayInputStream(resultByteStream.toByteArray()));
    }


    public BufferedImage getImage(byte[] bytes) {
        try {
            return ImageIO.read(new ByteArrayInputStream(bytes));
        } catch (IOException e) {
            throw new ServiceException("image.get.from.bytes.failed");
        }
    }

    public BufferedImage getImage(InputStream stream) {
        try {
            byte[] bytes = IOUtils.toByteArray(stream);
            return ImageIO.read(new ByteArrayInputStream(bytes));
        } catch (IOException e) {
            throw new ServiceException("image.get.from.stream.failed");
        }

    }

    public BufferedImage resizeTo(BufferedImage image, int targetWidth, int targetHeight) throws IOException {
        log.debug("[Image][convertFrom:" + image.getWidth() + "x" + image.getHeight() + " to:" + targetWidth + "x" + targetHeight + "]");
        BufferedImage newImage = null;
        if (image.getHeight() > targetHeight || image.getWidth() > targetWidth) {
            if (image.getHeight() > image.getWidth()) {
                newImage = resizeToWidth(image, targetWidth);
                newImage = _doCropFromTopLeft(newImage, newImage.getWidth(), Math.min(targetHeight, newImage.getHeight()));
            } else {
                newImage = resizeToHeight(image, targetHeight);
                newImage = _doCropFromTopLeft(newImage, Math.min(targetWidth, newImage.getWidth()), newImage.getHeight());
            }
        } else {
            if (image.getHeight() > image.getWidth()) {
                newImage = resizeToWidth(image, targetWidth);
                if (newImage.getHeight() < targetHeight) {
                    newImage = resizeToHeight(newImage, targetHeight);
                    newImage = _doCropFromTopLeft(newImage, targetWidth, newImage.getHeight());
                } else {
                    newImage = _doCropFromTopLeft(newImage, newImage.getWidth(), targetHeight);
                }

            } else {
                newImage = resizeToHeight(image, targetHeight);
                if (newImage.getWidth() < targetWidth) {
                    newImage = resizeToHeight(newImage, newImage.getHeight());
                    newImage = _doCropFromTopLeft(newImage, newImage.getWidth(), targetHeight);

                } else {
                    newImage = _doCropFromTopLeft(newImage, targetWidth, newImage.getHeight());
                }
            }
        }
        if (newImage != null)
            log.debug("[Image][newImage:" + newImage.getWidth() + "x" + newImage.getHeight() + "]");
        return newImage != null ? newImage : image;
    }

    protected BufferedImage _doCrop(BufferedImage image, int width, int height, int topLeftX, int topLeftY) {
        try {
            return Scalr.crop(image, topLeftX, topLeftY, width, height, Scalr.OP_ANTIALIAS);
        } catch (IllegalArgumentException e) {
            log.debug(e.getMessage());
            throw new ServiceException("image.crop.widthOrHeight.invalid");
        }
    }

    protected BufferedImage _doCropFromTopLeft(BufferedImage image, int width, int height) {
        try {
            return Scalr.crop(image, width, height, Scalr.OP_ANTIALIAS);
        } catch (IllegalArgumentException e) {
            log.debug(e.getMessage());
            throw new ServiceException("image.crop.widthOrHeight.invalid");
        }
    }

    protected BufferedImage resize(BufferedImage image, int width, int height) {
        try {
            return Scalr.resize(image, Scalr.Method.ULTRA_QUALITY, Scalr.Mode.AUTOMATIC, width, height, Scalr.OP_ANTIALIAS);
        } catch (IllegalArgumentException e) {
            log.debug(e.getMessage());
            throw new ServiceException("image.crop.widthOrHeight.invalid");
        }
    }

    public BufferedImage resizeToWidth(BufferedImage image, int width) {
        try {
            return Scalr.resize(image, Scalr.Method.ULTRA_QUALITY, Scalr.Mode.FIT_TO_WIDTH, width, image.getHeight(), Scalr.OP_ANTIALIAS);
        } catch (IllegalArgumentException e) {
            log.debug(e.getMessage());
            throw new ServiceException("image.resize.width.invalid");
        }
    }

    public BufferedImage resizeToHeight(BufferedImage image, int height) {
        try {
            return Scalr.resize(image, Scalr.Method.ULTRA_QUALITY, Scalr.Mode.FIT_TO_HEIGHT, image.getWidth(), height, Scalr.OP_ANTIALIAS);
        } catch (IllegalArgumentException e) {
            log.debug(e.getMessage());
            throw new ServiceException("image.resize.width.invalid");
        }
    }

    public boolean isAcceptedImageType(MultipartFile file) {
        String contentType = file.getContentType();
        return isAcceptedImageType(contentType);
    }

    public boolean isAcceptedImageType(String contentType) {
        return contentType.contains("image")&&!contentType.contains("svg");
    }

    public String imageSrcFromBytes(byte[] bytes) {

        String imageSrc = "data:image/" + saveExtension + ";base64," + Base64.getEncoder().encodeToString(bytes);
        return imageSrc;
    }
}
