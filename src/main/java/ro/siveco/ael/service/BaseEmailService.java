package ro.siveco.ael.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import ro.siveco.ael.lcms.domain.auth.model.AelUser;
import ro.siveco.ael.lcms.domain.preference.model.Preference;
import ro.siveco.ael.lcms.domain.preference.model.Preferences;
import ro.siveco.ael.lcms.domain.preference.service.PreferencesService;
import ro.siveco.ael.web.components.WebPath;
import ro.siveco.ram.starter.service.BaseService;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.logging.Logger;

/**
 * Base service for handling sending e-mails to users.
 *
 * @author mikolajb
 * @since 2019-03-15
 */
@Service
public class BaseEmailService extends BaseService {

    // Relative path to the service logo image.
    private static final String SERVICE_LOGO_RELATIVE_URL = "/img/palette_new_logo.png";
    // Relative path to background image.
    private static final String EMAIL_BACKGROUND_RELATIVE_URL = "/css/img/body_background_gradient.jpg";
    protected final Logger logger = Logger.getLogger(getClass().getSimpleName());
    private PreferencesService preferencesService;

    @Autowired
    public BaseEmailService(PreferencesService preferencesService) {

        this.preferencesService = preferencesService;

        Assert.notNull(preferencesService, "Supplied PreferencesService is null.");

        Preference serverHostPref = preferencesService.getGlobalPreferenceByName(Preferences.GLOBAL_AEL_SERVER_HOST.toString());

        Assert.hasLength(serverHostPref.getValue(), "No global.AEL_SERVER_HOST preference set in the database.");
        getLogger().info("Service Initialized");
    }

    /**
     * Wrap message into layout.
     *
     * @param user    user for which to get locale
     * @param message message to send
     */
    public String putMessageIntoLayout(AelUser user, String message) throws Exception {

        Locale locale = getUserPreferredLocale(user);

        String baseUrl = getBaseUrl();
        String logoUrl = getLogoUrl(baseUrl);
        String backgroundUrl = getBackgroundUrl(baseUrl);
        String dontRespond = getMessageSource().getMessage("email.footer.dont.respond", null, locale);
        String visitFaq = getMessageSource().getMessage("email.footer.visit.faq", null, locale);
        String faqUrl = getFAQUrl(baseUrl);
        String faq = getMessageSource().getMessage("email.footer.faq", null, locale);
        String dateTime = getDateTime();

        Object[] params = {backgroundUrl, baseUrl, logoUrl, message, dontRespond, visitFaq, faqUrl, faq, dateTime};

        String layout = getLayout();

        return MessageFormat.format(layout, params).replaceAll("(\\r|\\n)", "");
    }

    /**
     * Get preferred locale for specified user
     *
     * @param user user
     * @return preferred locale, or the default locale if preferred one is not found
     */
    public Locale getUserPreferredLocale(AelUser user) {
        Preference langPreference = preferencesService.getByNameAndUser(Preferences.USER_LOCALE.toString(), user);

        if (langPreference == null) {
            //TODO Get from global preferences
            return Locale.ENGLISH;
        }

        return new Locale(langPreference.getValue());
    }

    /**
     * Get the baseUrl for creating email links. It's constructed from a Preferences.GLOBAL_AEL_SERVER_HOST
     * and Preferences.GLOBAL_HTTP_SCHEME_TYPE.
     *
     * @return string baseUrl
     * @throws Exception if Preferences.GLOBAL_AEL_SERVER_HOST is not found in database. If scheme preference is not
     *                   found, "http://" is used.
     */
    protected String getBaseUrl() throws Exception {

        Preference serverHostPref = preferencesService.getGlobalPreferenceByName(Preferences.GLOBAL_AEL_SERVER_HOST.toString());

        if (serverHostPref == null || serverHostPref.getValue() == null || "".equals(serverHostPref.getValue())) {
            throw new Exception("No " + Preferences.GLOBAL_AEL_SERVER_HOST +
                    " preference defined. Won't send verification email.");
        }


        String httpSchemeType = "http://";
        Preference httpSchemeTypePref = preferencesService.getGlobalPreferenceByName(Preferences.GLOBAL_HTTP_SCHEME_TYPE.toString());
        if (httpSchemeTypePref != null && httpSchemeTypePref.getValue() != null && !"".equals(httpSchemeTypePref.getValue())) {
            httpSchemeType = httpSchemeTypePref.getValue();
        }

        return httpSchemeType + serverHostPref.getValue();
    }

    /**
     * Get current date and time.
     * <p>
     * This is required in the footer of the message, otherwise (if the messages would be exactly the same) e-mail
     * clients, e.g., g-mail, does't show the most recent messages.
     */
    private String getDateTime() {

        Date date = Calendar.getInstance().getTime();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm");

        return dateFormat.format(date);
    }

    /**
     * Get service logo URL
     *
     * @param baseUrl base URL
     * @return string logo URL
     */
    private String getLogoUrl(String baseUrl) {
        return baseUrl + SERVICE_LOGO_RELATIVE_URL;
    }

    /**
     * Get background URL
     *
     * @param baseUrl base URL
     * @return string background URL
     */
    private String getBackgroundUrl(String baseUrl) {
        return baseUrl + EMAIL_BACKGROUND_RELATIVE_URL;
    }

    /**
     * Get FAQ URL
     *
     * @param baseUrl base URL
     * @return string FAQ URL
     */
    private String getFAQUrl(String baseUrl) {
        return baseUrl + WebPath.FAQ;
    }

    /**
     * Get content of the file with e-mail template.
     */
    private String getLayout() throws IOException {
        return "<div style=\"width: 600px;\n" +
                "            text-align: left;\n" +
                "            overflow: visible;\n" +
                "            background: url({0}) no-repeat center top;\n" +
                "            \">\n" +
                "    <div style=\"padding: 9%;\">\n" +
                "\n" +
                "        <!--Header-->\n" +
                "        <a href=\"{1}\"><img src=\"{2}\" height=\"50\" style=\"padding-bottom: 40px\"/><br/></a>\n" +
                "\n" +
                "        <!--Message-->\n" +
                "        <div style=\"padding-bottom: 20px; font-size: 1.5rem;\">\n" +
                "            {3}\n" +
                "        </div>\n" +
                "\n" +
                "        <hr>\n" +
                "\n" +
                "        <!--Footer-->\n" +
                "        <div style=\"font-style: italic;\">\n" +
                "            <p>\n" +
                "                {4}\n" +
                "            </p>\n" +
                "            <p>\n" +
                "                {5}<br/>\n" +
                "                <a href=\"{6}\">{7}</a>.\n" +
                "            </p>\n" +
                "            <p>\n" +
                "                {8}\n" +
                "            </p>\n" +
                "        </div>\n" +
                "    </div>\n" +
                "</div>\n";
    }
}
