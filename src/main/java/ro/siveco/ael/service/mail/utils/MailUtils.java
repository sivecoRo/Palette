package ro.siveco.ael.service.mail.utils;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.auth.model.AelUser;
import ro.siveco.ael.lcms.domain.preference.model.Preference;
import ro.siveco.ael.lcms.domain.preference.model.Preferences;
import ro.siveco.ael.lcms.domain.preference.service.PreferencesService;

import java.util.Map;


@Component
public class MailUtils
{
	private PreferencesService preferencesService;
	protected static final Logger logger = Logger.getLogger(MailUtils.class);


	public MailUtils( PreferencesService preferencesService )
	{
		this.preferencesService = preferencesService;
	}
	
	public boolean isMailEnabled( AelUser user )
	{
		if( user != null )
		{
			Preference preference = preferencesService.getByNameAndUser(Preferences.SEND_EMAIL.get(), user);
			return preference != null && preference.getValue() != null && preference.getValue().equals(Boolean.TRUE.toString());
		}
		return false;
	}

	public String getEmailForUser( AelUser user )
	{
		if( user != null )
		{
			Map<String, String> prefMap = preferencesService.getPreferencesMap( user.getId() );

			if( prefMap.containsKey( Preferences.SEND_EMAIL.get() ) )
			{
				String preferredEmailAddress = prefMap.get( Preferences.PERSONALIZED_EMAIL_ADDRESS.get() );
                String emailAddress = StringUtils.isBlank(preferredEmailAddress) ? user.getEmail() : preferredEmailAddress;

                if (StringUtils.isBlank(emailAddress))
                    emailAddress= user.getDisplayName() + "@undefinedemail.local";

                return emailAddress;
			} 
		}
		return null;
	}
}

