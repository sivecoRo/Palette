package ro.siveco.ael.service.mail.providers.impl;

import ro.siveco.ael.service.mail.providers.EmailProvider;
import ro.siveco.ael.service.mail.utils.MailResponse;
import ro.siveco.ael.service.mail.utils.SimpleAsyncHandler;

import java.util.Map;


public class NoEmailClient implements EmailProvider {

    public MailResponse send(Map<String, Object> emailParams) {
        return new MailResponse("No email service");
    }

    public void sendAsync(Map<String, Object> emailParams, SimpleAsyncHandler asyncHandler){
        asyncHandler.onError(new Exception("No email service"));
    }

    @Override
    public String getDefaultFrom() {
        return null;
    }
}
