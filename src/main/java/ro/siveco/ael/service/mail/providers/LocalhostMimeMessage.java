package ro.siveco.ael.service.mail.providers;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;

/**
 * Created by razvann on 11/28/2016.
 */
public class LocalhostMimeMessage extends MimeMessage {
    private static int id = 0;

    public LocalhostMimeMessage(Session session) {
        super(session);
    }

    @Override
    protected void updateMessageID() throws MessagingException {
        setHeader("Message-ID",
                "<" + getUniqueMessageIDValue(session) + ">");
    }

    private String getUniqueMessageIDValue(Session ssn) {
        String suffix = "no-reply@wand.education";
        StringBuilder s = new StringBuilder();
        s.append(s.hashCode()).append('.').append(getUniqueId()).append('.').
                append(System.currentTimeMillis()).append('.').
                append(suffix);
        return s.toString();
    }

    public int getUniqueId() {
        return id++;
    }
}
