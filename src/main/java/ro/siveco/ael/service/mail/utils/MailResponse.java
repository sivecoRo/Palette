package ro.siveco.ael.service.mail.utils;


public class MailResponse {

    private Object response;

    public MailResponse() {
    }

    public MailResponse(Object response) {
        this.response = response;
    }

    public Object getResponse() {
        return response;
    }

    public void setResponse(Object response) {
        this.response = response;
    }
}
