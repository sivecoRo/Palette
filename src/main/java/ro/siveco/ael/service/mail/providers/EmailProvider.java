package ro.siveco.ael.service.mail.providers;

import ro.siveco.ael.service.mail.utils.MailResponse;
import ro.siveco.ael.service.mail.utils.SimpleAsyncHandler;

import java.util.Map;

/**
 * Created by liviui on 5/16/2016.
 */
public interface EmailProvider {

    public static final String FROM = "from";
    public static final String TO = "to";
    public static final String SUBJECT = "subject";
    public static final String HTML = "html";

    public MailResponse send(Map<String, Object> emailProps);

    public void sendAsync(Map<String, Object> emailProps, SimpleAsyncHandler asyncHandler);

    public String getDefaultFrom();

}
