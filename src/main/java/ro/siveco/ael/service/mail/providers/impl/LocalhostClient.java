package ro.siveco.ael.service.mail.providers.impl;

import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import ro.siveco.ael.service.mail.providers.EmailProvider;
import ro.siveco.ael.service.mail.providers.MailSender;
import ro.siveco.ael.service.mail.utils.MailResponse;
import ro.siveco.ael.service.mail.utils.SimpleAsyncHandler;

import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class LocalhostClient implements EmailProvider {

    private static final int DEFAULT_THREAD_POOL_SIZE = 50;

    private String defaultFrom;


    private ExecutorService executorService;
    private JavaMailSenderImpl mailSender;

    public LocalhostClient(Map<String, String> configuration) {
        if (configuration != null) {
            mailSender = new MailSender();
            mailSender.setHost(configuration.get("mail.host"));
            mailSender.setDefaultEncoding("UTF-8");
            mailSender.setPort(Integer.parseInt(configuration.get("mail.port")));
            mailSender.setUsername(configuration.get("mail.username"));
            mailSender.setUsername(configuration.get("mail.password"));
            mailSender.setProtocol(configuration.get("mail.protocol"));

            Properties properties = new Properties();
            properties.setProperty("mail.smtp.auth", configuration.get("mail.smtp.auth"));
            properties.setProperty("mail.smtp.starttls.enable", configuration.get("mail.smtp.starttls.enable"));
            mailSender.setJavaMailProperties(properties);

            defaultFrom = configuration.get("mail.from");
        }

    }

    @Override
    public MailResponse send(Map<String, Object> params) {
        String from = params.containsKey(FROM) ? params.get(FROM).toString() : defaultFrom;
        String to = params.containsKey(TO) ? params.get(TO).toString() : "";
        String subject = params.containsKey(SUBJECT) ? params.get(SUBJECT).toString() : "";
        String content = params.containsKey(HTML) ? params.get(HTML).toString() : "";

        this.mailSender.send(mimeMessage -> {
            MimeMessageHelper message = new MimeMessageHelper(mimeMessage, "UTF-8");
            message.setTo(to.split(","));
            message.setFrom(from);
            message.setSubject(subject);
            message.setText(content, true);
        });

        return new MailResponse(null);
    }

    @SuppressWarnings("unchecked")
	@Override
    public void sendAsync(Map<String, Object> emailProps, SimpleAsyncHandler asyncHandler) {
        initExecutorService();
        executorService
                .submit((Callable) () -> {
                    MailResponse result;

                    try {
                        result = send(emailProps);
                    } catch (Exception ex) {
                        if (asyncHandler != null) {
                            asyncHandler.onError(ex);
                        }
                        throw ex;
                    }

                    if (asyncHandler != null) {
                        asyncHandler.onSuccess(result);
                    }
                    return result;
                });
    }

    @Override
    public String getDefaultFrom() {
        return defaultFrom;
    }

    private synchronized void initExecutorService() {
        if (executorService == null) {
            executorService = Executors.newFixedThreadPool(DEFAULT_THREAD_POOL_SIZE);
        }
    }
}
