package ro.siveco.ael.service.mail;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.context.IContext;
import org.thymeleaf.templateresolver.TemplateResolver;
import ro.siveco.ael.lcms.domain.auth.model.AelUser;
import ro.siveco.ael.service.mail.providers.EmailProvider;
import ro.siveco.ael.service.mail.utils.SimpleAsyncHandler;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
public class EmailService {


    private final EmailProvider emailProvider;

    private final TemplateEngine emailTemplateEngine;

    @Autowired
    public EmailService(EmailProvider emailProvider, TemplateResolver templateResolver,
                        TemplateEngine emailTemplateEngine) {
        this.emailProvider = emailProvider;
        this.emailTemplateEngine = emailTemplateEngine;
    }
    
    public void sendEmail(AelUser from, AelUser to, String subject, String content) {
    	Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put(EmailProvider.FROM, emailProvider.getDefaultFrom());
        parameters.put(EmailProvider.TO, to.getEmail());
        parameters.put(EmailProvider.SUBJECT, subject);
        parameters.put( EmailProvider.HTML, content);

        emailProvider.sendAsync(parameters, new SimpleAsyncHandler(parameters));
    }

    public void sendEmail(String to, String subject, String content) {
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put(EmailProvider.FROM, emailProvider.getDefaultFrom());
        parameters.put(EmailProvider.TO, to);
        parameters.put(EmailProvider.SUBJECT, subject);
        parameters.put( EmailProvider.HTML, content);

        emailProvider.sendAsync(parameters, new SimpleAsyncHandler(parameters));
    }

    public void sendEmail(AelUser to, String subject, String content) {
        sendEmail((String)null, to, subject, content);
    }

    public void sendEmail(String from, AelUser to, String subject, String content) {
    	Map<String, Object> parameters = new HashMap<String, Object>();
        String fromAddress = emailProvider.getDefaultFrom();
    	if (from !=null && !from.isEmpty()) {
            fromAddress = from;
        }
        parameters.put(EmailProvider.FROM, fromAddress);
        parameters.put(EmailProvider.TO, to.getEmail());
        parameters.put(EmailProvider.SUBJECT, subject);
        parameters.put( EmailProvider.HTML, content);

        emailProvider.sendAsync(parameters, new SimpleAsyncHandler(parameters));
    }

    public void sendNoReplayFromTemplate(List<Map<String, Object>> params, String template) {
        String noReplyEmail = getFromAddress();
        params.forEach(mailParam ->{
            String content = processTemplate( template, mailParam);
            sendEmail(noReplyEmail, (AelUser) mailParam.get("to"), "Platform message", content );
        });
    }

    public void sendFromTemplate(List<Map<String, Object>> params, String template, String from) {
        params.forEach(mailParam ->{
            String content = processTemplate( template, mailParam);
            sendEmail(from, (AelUser) mailParam.get("to"), "Platform message", content );
        });
    }
    private String processTemplate( String template, Map<String, Object> params ){
        IContext context = new Context();
        context.getVariables().putAll(params);
        return emailTemplateEngine.process(template, context);
    }

    public String getFromAddress() {

        String fromAddress = "no-reply@ael.ro";
        if (System.getProperty("mail.sender.address") != null &&
                !"".equals(System.getProperty("mail.sender.address"))) {
            fromAddress = System.getProperty("mail.sender.address");
        }
        return fromAddress;
    }
}