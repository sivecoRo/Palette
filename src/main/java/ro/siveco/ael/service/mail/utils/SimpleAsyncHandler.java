package ro.siveco.ael.service.mail.utils;

import javax.xml.ws.AsyncHandler;
import javax.xml.ws.Response;
import java.util.Map;


public class SimpleAsyncHandler implements AsyncHandler {

    private Map<String, Object> params;


    public SimpleAsyncHandler(Map<String, Object> params) {
        this.params = params;
    }

    public void onError(Exception exception) {
    }

    public void onSuccess(MailResponse response) {
    }

	@Override
	public void handleResponse(Response arg0) {
		// TODO Auto-generated method stub
	}

}