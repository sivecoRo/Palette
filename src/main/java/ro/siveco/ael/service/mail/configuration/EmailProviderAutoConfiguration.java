package ro.siveco.ael.service.mail.configuration;

import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ro.siveco.ael.service.mail.providers.EmailProvider;
import ro.siveco.ael.service.mail.providers.impl.LocalhostClient;
import ro.siveco.ael.service.mail.providers.impl.MailgunClient;
import ro.siveco.ael.service.mail.providers.impl.NoEmailClient;

import java.util.Map;


@Configuration
public class EmailProviderAutoConfiguration {

    @Autowired
    EmailProviderProperties providers;

    @Bean(autowire = Autowire.BY_NAME)
    public EmailProvider mailSender() {

        String providerName = providers.getActiveProvider();

        Map<String, String> props = providers.getActiveProviderProperties();
        if( "mailgun".equals(providerName)){
            return new MailgunClient(props);
        } else if ("localhost".equals(providerName)) {
            return new LocalhostClient(props);
        }
        return new NoEmailClient();
    }
}
