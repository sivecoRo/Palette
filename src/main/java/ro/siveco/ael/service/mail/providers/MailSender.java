package ro.siveco.ael.service.mail.providers;

import org.springframework.mail.javamail.JavaMailSenderImpl;

import javax.mail.internet.MimeMessage;

/**
 * Created by razvann on 11/28/2016.
 */
public class MailSender extends JavaMailSenderImpl {
    @Override
    public MimeMessage createMimeMessage() {
        return new LocalhostMimeMessage(getSession());
    }
}
