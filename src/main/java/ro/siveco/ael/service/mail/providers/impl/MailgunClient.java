package ro.siveco.ael.service.mail.providers.impl;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import ro.siveco.ael.service.mail.providers.EmailProvider;
import ro.siveco.ael.service.mail.utils.MailResponse;
import ro.siveco.ael.service.mail.utils.SimpleAsyncHandler;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.ws.rs.core.MediaType;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class MailgunClient implements EmailProvider {

    private Log log = LogFactory.getLog(getClass());

    private static final int DEFAULT_THREAD_POOL_SIZE = 50;

    private ExecutorService executorService;

    private String mailgunApiKey;

    private String mailgunRoot;

    private String defaultFrom;

    TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
        @Override
        public void checkClientTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {
        }

        @Override
        public void checkServerTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {
        }

        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
            return null;
        }
    }};

    public MailgunClient(Map<String, String> configuration ) {
        if( configuration != null ){
            mailgunRoot = configuration.get("endpoint");
            mailgunApiKey = configuration.get("secretKey");
            defaultFrom = configuration.get("defaultFrom");
        }
    }

    public MailResponse send( Map<String, Object> emailParams) {

        try {
            SSLContext sc = SSLContext.getInstance("TLS");
            sc.init(null, trustAllCerts, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {
            log.error(e);
        }

        Client client = Client.create();
        client.addFilter(new HTTPBasicAuthFilter("api", mailgunApiKey));

        WebResource webResource = client.resource(mailgunRoot + "/messages");

        MultivaluedMapImpl formData = getRequestData(emailParams);

        ClientResponse clientResponse = webResource.type(MediaType.APPLICATION_FORM_URLENCODED).post(ClientResponse.class, formData);

        return new MailResponse(clientResponse.getEntity(String.class));
    }

    @SuppressWarnings("unchecked")
	public void sendAsync(Map<String, Object> emailParams, SimpleAsyncHandler asyncHandler){
        initExecutorService();
        executorService
            .submit((Callable) () -> {
                MailResponse result;

                try {
                    result = send(emailParams);
                } catch (Exception ex) {
                    if (asyncHandler != null) {
                        asyncHandler.onError(ex);
                    }
                    throw ex;
                }

                if (asyncHandler != null) {
                    asyncHandler.onSuccess(result);
                }
                return result;
            });

    }

    @Override
    public String getDefaultFrom() {
        return defaultFrom;
    }

    private synchronized void initExecutorService(){
        if(executorService==null){
            executorService = Executors.newFixedThreadPool(DEFAULT_THREAD_POOL_SIZE);
        }
    }

    private MultivaluedMapImpl getRequestData(Map<String, Object> params) {
        String from = params.containsKey(FROM) ? params.get(FROM).toString() : defaultFrom;
        String to = params.containsKey(TO) ? params.get(TO).toString() : "";
        String subject = params.containsKey(SUBJECT) ? params.get(SUBJECT).toString() : "";
        String content = params.containsKey(HTML) ? params.get(HTML).toString() : "";

        MultivaluedMapImpl formData = new MultivaluedMapImpl();
        formData.add(FROM, from);
        formData.add(TO, to);
        formData.add(SUBJECT, subject);
        formData.add(HTML, content);

        return formData;
    }

}
