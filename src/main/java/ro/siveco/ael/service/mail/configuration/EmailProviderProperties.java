package ro.siveco.ael.service.mail.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by liviui on 5/17/2016.
 */
@Configuration
@ConfigurationProperties(prefix = "mailService")
public class EmailProviderProperties {

    String activeProvider;

    Map<String, Map<String, String>> providers;

    public Map<String, Map<String, String>> getProviders() {
        return providers;
    }

    public String getActiveProvider() {
        return activeProvider;
    }

    public void setActiveProvider(String activeProvider) {
        this.activeProvider = activeProvider;
    }

    public void setProviders(Map<String, Map<String, String>> providers) {
        this.providers = providers;
    }

    public Map<String, String> getActiveProviderProperties(){
        if( providers.containsKey(activeProvider)){
            return providers.get(activeProvider);
        }
        return new HashMap<>();
    }
}
