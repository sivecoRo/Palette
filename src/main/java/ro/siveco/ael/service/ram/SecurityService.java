package ro.siveco.ael.service.ram;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import ro.siveco.ael.lcms.domain.auth.model.AelUser;
import ro.siveco.ael.lcms.domain.auth.model.DefaultAuthenticatedUser;
import ro.siveco.ram.security.exception.NotAuthenticatedException;
import ro.siveco.ram.security.exception.RestSecurityException;
import ro.siveco.ram.starter.service.BaseSecurityService;

/**
 * User: AlexandruVi
 * Date: 2017-04-10
 */
@Service
public class SecurityService extends BaseSecurityService {

    public AelUser getCurrentAuthenticatedUser() throws RestSecurityException {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        try {
            Object principal = auth.getPrincipal();
            if (principal instanceof String) {
                throw new NotAuthenticatedException(principal.toString());
            }
            return (AelUser) ((DefaultAuthenticatedUser) principal).getUser();
        } catch (RestSecurityException e) {
            throw e;
        } catch (Exception e) {
            throw new NotAuthenticatedException(e);
        }
    }
}
