package ro.siveco.ael.service.search;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;
import ro.siveco.ael.lcms.domain.auth.service.DefaultUserService;
import ro.siveco.ael.lcms.domain.course.service.PlannedCourseService;
import ro.siveco.ael.lcms.domain.course.service.PlannedCourseStudentService;
import ro.siveco.ael.service.BaseService;
import ro.siveco.ael.service.search.utils.SearchCategory;
import ro.siveco.ael.service.search.utils.SearchResult;
import ro.siveco.ram.model.Entity;

import java.security.Principal;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by razvann on 5/15/2017.
 */
@Service
public class SearchService extends BaseService {


    private static final Integer MAX_RESULTS = 5;

    private final PlannedCourseService plannedCourseService;
    private final PlannedCourseStudentService plannedCourseStudentService;
    private final DefaultUserService userService;


    @Autowired
    public SearchService(PlannedCourseService plannedCourseService,
                         PlannedCourseStudentService plannedCourseStudentService, DefaultUserService userService) {
        this.plannedCourseService = plannedCourseService;
        this.plannedCourseStudentService = plannedCourseStudentService;
        this.userService = userService;
    }

    public Map<SearchCategory, SearchResult> loadSearchResults(Principal principal, String searchString) {
        Map<SearchCategory, SearchResult> items = new HashMap<>();

        //        String[] words = searchString.split(" ");
        //        User user = getUser(principal);
        //        if (hasRole(principal, "LEARNER")) {
        //            Page<PlannedCourseStudent> assignedCourses =
        //                    plannedCourseStudentService.paginate(new AssignedCourseSpecification(user, words), MAX_RESULTS);
        //            collectItems(items, assignedCourses, SearchCategory.ASSIGNED_COURSE);
        //
        //        }
        //        if (hasRole(principal, "TEACHER")) {
        //            Page<AelUser> users = userService.paginate(new UserSpecification(user, words), MAX_RESULTS);
        //            collectItems(items, users, SearchCategory.USER);
        //            Page<PlannedCourse> courses =
        //                    plannedCourseService.paginate(new TrainerCourseSpecification(user, words), MAX_RESULTS);
        //            collectItems(items, courses, SearchCategory.COURSE);
        //
        //        }
        //

        return items;
    }

    protected void collectItems(Map<SearchCategory, SearchResult> items, Page<? extends Entity> page,
                                SearchCategory searchCategory) {
        if (page != null && page.getTotalElements() > 0) {
            items.put(searchCategory, new SearchResult(page.getContent()));
        }
    }


    public boolean hasRole(Principal principal, String role) {
        if (role != null) {
            Collection<GrantedAuthority> authorities = ((AbstractAuthenticationToken) principal).getAuthorities();
            return authorities.contains(new SimpleGrantedAuthority(role));
        }
        return false;
    }
}
