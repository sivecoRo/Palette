package ro.siveco.ael.service.search.utils;

/**
 * Created by razvann on 5/15/2017.
 */
public enum SearchCategory {
    ASSIGNED_COURSE("search.assigned_course.title"),COURSE("search.course.title"), CONTENT("search.content.title"), USER("search.user.title");
    private String categoryKey;

    SearchCategory(String categoryKey) {
        this.categoryKey = categoryKey;
    }

    public String getCategoryKey() {
        return categoryKey;
    }
}