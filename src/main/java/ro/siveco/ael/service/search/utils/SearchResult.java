package ro.siveco.ael.service.search.utils;

import ro.siveco.ram.model.Entity;

import java.util.List;

/**
 * Created by razvann on 5/15/2017.
 */
//ester un wrapper peste rezultatele cautarii. l-am facut in ideea ca mai adaugam si alte informatii in el.
public class SearchResult {
    private List<? extends Entity> searchItems;

    public SearchResult() {
    }

    public SearchResult(List<? extends Entity> searchItems) {
        this.searchItems = searchItems;
    }


    public List<? extends Entity> getSearchItems() {
        return searchItems;
    }

    public void setSearchItems(List<? extends Entity> searchItems) {
        this.searchItems = searchItems;
    }

}
