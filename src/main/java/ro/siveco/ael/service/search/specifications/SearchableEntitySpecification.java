package ro.siveco.ael.service.search.specifications;

import ro.siveco.ael.lcms.domain.auth.model.User;
import ro.siveco.ael.repository.specifications.generic.GenericTenantAwareSpecification;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by razvann on 5/16/2017.
 */
public abstract class SearchableEntitySpecification<T> extends GenericTenantAwareSpecification<T> {
    private String[] words;

    public SearchableEntitySpecification(User user, String[] words) {
        super(user);
        this.words = words;
    }

    public abstract String[] getSearchableProperties();


    @Override
    public Predicate buildPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
        String[] searchableProperties = getSearchableProperties();
        List<Predicate> predicates = buildPredicates(root, cb, searchableProperties);
        return cb.or(predicates.toArray(new Predicate[predicates.size()]));
    }

    protected List<Predicate> buildPredicates(Path root, CriteriaBuilder cb, String[] searchableProperties) {
        List<Expression<String>> searchablePaths = buildPaths(searchableProperties, root);
        List<Predicate> predicates = new ArrayList<>();
        for (String word : words) {
            predicates.addAll(searchablePaths.stream()
                    .map(path -> cb.like(cb.lower(path), ("%" + word + "%").toLowerCase()))
                    .collect(Collectors.toList()));
        }
        return predicates;
    }

    protected List<Expression<String>> buildPaths(String[] searchableProperties, Path root) {
        return Arrays.stream(searchableProperties).map(property -> buildPath(root, property))
                .collect(Collectors.toList());
    }

    @SuppressWarnings("unchecked")
    protected Expression<String> buildPath(Path root, String property) {
        if (property.contains(".")) {
            String[] parts = property.split("\\.");
            Path path = root;
            for (String part : parts) {
                path = path.get(part);
            }
            return path;
        } else {
            return root.get(property);
        }
    }
}
