package ro.siveco.ael.service.utils;

public class FilterCategories {
    public static final String FILTER_MY_SEARCH = "mysearch";
    public static final String FILTER_SEARCH = "search";
    public static final String FILTER_FAVORITES = "favorites";
    public static final String FILTER_TYPE = "type";
    public static final String FILTER_DATE = "dateFilter";
    public static final String FILTER_DISTANCE = "distanceFilter";
    public static final String FILTER_TAG = "tagFilter";
    public static final String FILTER_ORGANIZER = "organizerFilter";
    public static final String FILTER_CALENDAR = "calendarFilter";
}
