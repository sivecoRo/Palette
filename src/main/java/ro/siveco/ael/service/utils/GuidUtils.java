package ro.siveco.ael.service.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;

/**
 * Created by IntelliJ IDEA.
 * User: AlexandruM
 * Date: 08.02.2011
 * Time: 14:39:24
 */
public class GuidUtils
{
	private static Random myRand;

	static
	{
		myRand = new Random( new SecureRandom().nextLong() );
	}

	/**
	 * Returns a standard format Guid
	 * Example: C2FEEEAC-CFCD-11D1-8B05-00600806D9B6
	 * @return the generated Guid
	 */
	public static String generateGuid()
	{
		return convertToGuidFormat( generateGuidInternal() );
	}

	private static String convertToGuidFormat( String guid )
	{
		String raw = guid.toUpperCase();
		StringBuffer sb = new StringBuffer();
		sb.append( raw.substring( 0, 8 ) );
		sb.append( "-" );
		sb.append( raw.substring( 8, 12 ) );
		sb.append( "-" );
		sb.append( raw.substring( 12, 16 ) );
		sb.append( "-" );
		sb.append( raw.substring( 16, 20 ) );
		sb.append( "-" );
		sb.append( raw.substring( 20 ) );

		return sb.toString();
	}

	private static String generateGuidInternal()
	{
		String valueBeforeMD5;
		String valueAfterMD5 = "";
		MessageDigest md5 = null;
		StringBuffer sbValueBeforeMD5 = new StringBuffer();

		try
		{
			md5 = MessageDigest.getInstance( "MD5" );
		}
		catch( NoSuchAlgorithmException e )
		{
			System.out.println( "Error: " + e );
		}

		try
		{
			long time = System.currentTimeMillis();
			long rand;

			rand = myRand.nextLong();

			sbValueBeforeMD5.append( Long.toString( time ) );
			sbValueBeforeMD5.append( ":" );
			sbValueBeforeMD5.append( Long.toString( rand ) );

			valueBeforeMD5 = sbValueBeforeMD5.toString();
			md5.update( valueBeforeMD5.getBytes() );

			byte[] array = md5.digest();
			StringBuffer sb = new StringBuffer();
			for( int j = 0; j < array.length; ++j )
			{
				int b = array[ j ] & 0xFF;
				if( b < 0x10 ) sb.append( '0' );
				sb.append( Integer.toHexString( b ) );
			}

			valueAfterMD5 = sb.toString();
		}
		catch( Exception e )
		{
			System.out.println( "Error:" + e );
		}

		return valueAfterMD5;
	}
}
