package ro.siveco.ael.service.utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.auth.model.AelUser;
import ro.siveco.ael.web.controllers.WithUserBaseEntityController;
import ro.siveco.ram.starter.service.BaseService;

import java.security.Principal;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.util.*;

/**
 * User: AlexandruVi
 * Date: 2017-08-02
 */
@SuppressWarnings("WeakerAccess")
@Component
public class DatesService extends BaseService {

    public static final String ISO8601_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZ";

    @Value("${app.config.view.dates.longDateTimeFormat:EEE, d MMMM yyyy HH:mm:ss}")
    private String longDateTimeFormat;

    @Value("${app.config.view.dates.shortDateTimeFormat:dd MMM yyyy HH:mm}")
    private String shortDateTimeFormat;

    @Value("${app.config.view.dates.longDateFormat:EEE, d MMMM yyyy}")
    private String longDateFormat;

    @Value("${app.config.view.dates.shortDateFormat:dd MMM yyyy}")
    private String shortDateFormat;

    @Value("${app.config.view.dates.timeFormat:HH:mm:ss}")
    private String timeFormat;

    @Value("${app.config.view.dates.shortTimeFormat:HH:mm}")
    private String shortTimeFormat;

    private String machineDateTimeFormat = ISO8601_DATE_FORMAT;

    @Value("${app.config.view.dates.shortDateTimeFormat:HH:mm EEE, d MMMM yyyy}")
    private String commentDateTimeFormat;

    private static final String ROMANIA = "romania";
    private static final String SWITZERLAND = "suisse";
    private static final String NETHERLANDS = "netherlands";
    private static final String POLAND = "poland";
    private static final String SLOVENIA = "slovenia";

    private static final String ROMANIA_ZONE_ID = "Europe/Bucharest";
    private static final String SWITZERLAND_ZONE_ID = "Europe/Zurich";
    private static final String NETHERLANDS_ZONE_ID = "Europe/Amsterdam";
    private static final String POLAND_ZONE_ID = "Europe/Warsaw";
    private static final String SLOVENIA_ZONE_ID = "Europe/Ljubljana";
    private static final String UTC_ZONE_ID = "UTC";

    public String formatDate(Date date) {

        return formatDate(date, DateFormatType.SHORT_DATE);
    }

    public String formatDate(Date date, DateFormatType dateFormatType) {

        if (dateFormatType != null) {
            switch (dateFormatType) {
                case LONG_DATE_TIME:
                    return formatLongDateTime(date);
                case SHORT_DATE_TIME:
                    return formatShortDateTime(date);
                case LONG_DATE:
                    return formatLongDate(date);
                case SHORT_DATE:
                    return formatShortDate(date);
                case TIME:
                    return formatTime(date);
                case SHORT_TIME:
                    return formatShortTime(date);
                case MACHINE_READY:
                    return formatMachineDateTime(date);
                case COMMENT_DATE_TIME:
                    return formatCommentDateTime(date);
            }
        }
        return formatShortDate(date);
    }

    public String formatCommentDateTime(Date date) {

        Locale locale = LocaleContextHolder.getLocale();
        SimpleDateFormat sdf = new SimpleDateFormat(commentDateTimeFormat, locale);
        setTimezone(sdf);
        return sdf.format(date);
    }

    public String formatLongDateTime(Date date) {

        Locale locale = LocaleContextHolder.getLocale();
        SimpleDateFormat sdf = new SimpleDateFormat(longDateTimeFormat, locale);
        setTimezone(sdf);
        return sdf.format(date);
    }

    public String formatShortDateTime(Date date, String clientTimeZoneId ) {
        Locale locale = LocaleContextHolder.getLocale();
        SimpleDateFormat sdf = new SimpleDateFormat(shortDateTimeFormat, locale);
        sdf.setTimeZone(TimeZone.getTimeZone(ZoneId.of(clientTimeZoneId)));
        return sdf.format(date);
    }

    public String formatShortDateTime(Date date, String clientTimeZoneId, Locale locale ) {
        SimpleDateFormat sdf = new SimpleDateFormat(shortDateTimeFormat, locale);
        sdf.setTimeZone(TimeZone.getTimeZone(ZoneId.of(clientTimeZoneId)));
        return sdf.format(date);
    }

    public String formatShortDate(Date date, String clientTimeZoneId, Locale locale ) {
        SimpleDateFormat sdf = new SimpleDateFormat(shortDateFormat, locale);
        sdf.setTimeZone(TimeZone.getTimeZone(ZoneId.of(clientTimeZoneId)));
        return sdf.format(date);
    }

    public String formatShortDateTime(Date date) {

        Locale locale = LocaleContextHolder.getLocale();
        SimpleDateFormat sdf = new SimpleDateFormat(shortDateTimeFormat, locale);
        setTimezone(sdf);
        return sdf.format(date);

    }

    public String formatLongDate(Date date, String country) {

        Locale locale = LocaleContextHolder.getLocale();
        SimpleDateFormat sdf = new SimpleDateFormat(longDateFormat, locale);
        if (country != null) {
            String zoneId = getTimezoneId(country);
            setTimezone(sdf, zoneId);
        }
        return sdf.format(date);

    }

    public String formatLongDate(Date date) {

        Locale locale = LocaleContextHolder.getLocale();
        SimpleDateFormat sdf = new SimpleDateFormat(longDateFormat, locale);
        setTimezone(sdf);
        return sdf.format(date);

    }

    public String formatShortDate(Date date) {

        Locale locale = LocaleContextHolder.getLocale();
        SimpleDateFormat sdf = new SimpleDateFormat(shortDateFormat, locale);
        setTimezone(sdf);
        return sdf.format(date);
    }

    public String formatShortDate(Date date, Locale locale) {
        SimpleDateFormat sdf = new SimpleDateFormat(shortDateFormat, locale);
        setTimezone(sdf);
        return sdf.format(date);
    }

    public String formatTime(Date date) {

        Locale locale = LocaleContextHolder.getLocale();
        SimpleDateFormat sdf = new SimpleDateFormat(timeFormat, locale);
        //setTimezone(sdf);
        return sdf.format(date);

    }

    public String formatShortTime(Date date) {

        Locale locale = LocaleContextHolder.getLocale();
        SimpleDateFormat sdf= new SimpleDateFormat(shortTimeFormat, locale);
        //setTimezone(sdf);
        return sdf.format(date);
    }

    public String formatMachineDateTime(Date date) {

        Locale locale = LocaleContextHolder.getLocale();
        SimpleDateFormat sdf = new SimpleDateFormat(machineDateTimeFormat, locale);
        setTimezone(sdf);
        return sdf.format(date);
    }

    public Map<DateFormatType, String> getDateFormats() {

        Map<DateFormatType, String> dateFormatTypeStringMap = new HashMap<>();
        dateFormatTypeStringMap.put(DateFormatType.LONG_DATE_TIME, longDateTimeFormat);
        dateFormatTypeStringMap.put(DateFormatType.SHORT_DATE_TIME, shortDateTimeFormat);
        dateFormatTypeStringMap.put(DateFormatType.LONG_DATE, longDateFormat);
        dateFormatTypeStringMap.put(DateFormatType.SHORT_DATE, shortDateFormat);
        dateFormatTypeStringMap.put(DateFormatType.TIME, timeFormat);
        dateFormatTypeStringMap.put(DateFormatType.SHORT_TIME, shortTimeFormat);
        dateFormatTypeStringMap.put(DateFormatType.MACHINE_READY, machineDateTimeFormat);
        dateFormatTypeStringMap.put(DateFormatType.COMMENT_DATE_TIME, commentDateTimeFormat);
        return dateFormatTypeStringMap;
    }

    public enum DateFormatType {
        LONG_DATE_TIME,
        SHORT_DATE_TIME,
        LONG_DATE,
        SHORT_DATE,
        TIME,
        SHORT_TIME,
        MACHINE_READY,
        COMMENT_DATE_TIME
    }

    public String getCurrentUserTimezoneId() {
        Principal principal = SecurityContextHolder.getContext().getAuthentication();
        if (principal != null) {
            AelUser currentUser = (AelUser) WithUserBaseEntityController.getUser();
            return getTimezoneId(currentUser);
        }
        return null;
    }

    public String getCurrentUserTimezoneId(String country) {
        if( country != null) {
            return getTimezoneId(country);
        }
        return null;
    }

    public String getTimezoneId(AelUser user) {
        String zoneId = NETHERLANDS_ZONE_ID;
        if (user != null) {
            if (user.getCountry() != null) {
                String country = user.getCountry().getName();
                zoneId = getZoneId(zoneId, country);
            }
        }
        return zoneId;
    }

    private String getZoneId(String zoneId, String country) {
        zoneId = getTimezoneId(country);
        return zoneId;
    }

    public String getTimezoneId(String country) {
        String zoneId = NETHERLANDS_ZONE_ID;
        if (country != null) {
            switch (country.toLowerCase()) {
                case ROMANIA:
                    zoneId = ROMANIA_ZONE_ID;
                    break;
                case SWITZERLAND:
                    zoneId = SWITZERLAND_ZONE_ID;
                    break;
                case POLAND:
                    zoneId = POLAND_ZONE_ID;
                    break;
                case SLOVENIA:
                    zoneId = SLOVENIA_ZONE_ID;
                    break;
                case NETHERLANDS:
                    zoneId = NETHERLANDS_ZONE_ID;
                    break;
            }
        }
        return zoneId;
    }

    private void setTimezone(SimpleDateFormat simpleDateFormat) {
        if (simpleDateFormat != null) {
            String zoneId = getCurrentUserTimezoneId();
            if (zoneId != null) {
                simpleDateFormat.setTimeZone(TimeZone.getTimeZone(ZoneId.of(zoneId)));
            }
        }
    }

    private void setTimezone(SimpleDateFormat simpleDateFormat, String zoneId) {
        if (simpleDateFormat != null) {
            if (zoneId != null) {
                simpleDateFormat.setTimeZone(TimeZone.getTimeZone(ZoneId.of(zoneId)));
            }
        }
    }

    public Date parseDate(String dateToParse, Locale locale) {
        SimpleDateFormat sdf = new SimpleDateFormat(longDateFormat, locale);
        setTimezone(sdf);
        if (dateToParse != null) {
            try {
                return sdf.parse(dateToParse);
            }
            catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public Date formatDate(Date dateToFormat, String clientTimeZoneId) {
        if (dateToFormat != null) {
            SimpleDateFormat sdf = new SimpleDateFormat(shortDateFormat);
            SimpleDateFormat parseSdf = new SimpleDateFormat(shortDateFormat);
            sdf.setTimeZone(TimeZone.getTimeZone(ZoneId.of(clientTimeZoneId)));
            String formattedDate = sdf.format(dateToFormat);
            setTimezone(parseSdf);
            try {
                return parseSdf.parse(formattedDate);
            }
            catch (ParseException e) {
                e.printStackTrace();
                return null;
            }
        }
        return null;
    }

    public Date formatDateToUTC(Date dateToFormat) {
        return formatDate(dateToFormat, UTC_ZONE_ID);
    }

    public Date formatDateWithServerTimezone(Date dateToFormat, String clientTimeZoneId) {
        if (dateToFormat != null) {
            SimpleDateFormat sdf = new SimpleDateFormat(shortDateFormat);
            SimpleDateFormat parseSdf = new SimpleDateFormat(shortDateFormat);
            sdf.setTimeZone(TimeZone.getTimeZone(ZoneId.of(clientTimeZoneId)));
            String formattedDate = sdf.format(dateToFormat);
            try {
                return parseSdf.parse(formattedDate);
            }
            catch (ParseException e) {
                e.printStackTrace();
                return null;
            }
        }
        return null;
    }

    public Time parseTime(String timeToFormat) {
        if (timeToFormat != null && !timeToFormat.equals("")) {
            return Time.valueOf(timeToFormat);
        }
        return null;
    }

    public Date getYesterday() {
        LocalTime midnight = LocalTime.MIDNIGHT;
        LocalDate today = LocalDate.now(ZoneId.of(UTC_ZONE_ID));
        LocalDateTime todayMidnight = LocalDateTime.of(today, midnight);
        LocalDateTime yesterdayMidnight = todayMidnight.minusDays(1);
        return Date.from(yesterdayMidnight.atZone(ZoneId.of(UTC_ZONE_ID)).toInstant());
    }

    public Date getYesterdayNight(ZoneId zoneId) {
        LocalTime midnight = LocalTime.MIDNIGHT;
        LocalDate today = LocalDate.now(zoneId);
        LocalDateTime todayMidnight = LocalDateTime.of(today, midnight);
        LocalDateTime yesterdayNight = todayMidnight.minusMinutes(1);
        return Date.from(yesterdayNight.atZone(zoneId).toInstant());
    }

    public Date getYesterdayNight() {
        return getYesterdayNight(ZoneId.of(getCurrentUserTimezoneId()));
    }

    public Date getUTCYesterdayNight() {
        return getYesterdayNight(ZoneId.of(UTC_ZONE_ID));
    }

    public Date getTomorrow() {
        LocalTime midnight = LocalTime.MIDNIGHT;
        LocalDate today = LocalDate.now(ZoneId.of(UTC_ZONE_ID));
        LocalDateTime todayMidnight = LocalDateTime.of(today, midnight);
        LocalDateTime tomorrowMidnight = todayMidnight.plusDays(1);
        return Date.from(tomorrowMidnight.atZone(ZoneId.of(UTC_ZONE_ID)).toInstant());
    }

    public Boolean isInThePast(Date d, Time t, Integer hoursOffset){
        LocalDate ld =  d.toInstant().atZone(ZoneId.of(getCurrentUserTimezoneId())).toLocalDate();
        LocalDateTime ldt = ld.atStartOfDay();
        ldt = ldt.plusHours(t.getHours() + hoursOffset).plusMinutes(t.getMinutes()).plusSeconds(t.getSeconds());
        LocalDateTime now = LocalDateTime.now(ZoneId.of(getCurrentUserTimezoneId()));
        return ldt.isBefore(now);

    }

    public Integer getCurrentOffset() {
        return (ZoneId.of(getCurrentUserTimezoneId()).getRules().getOffset(Instant.now()).getTotalSeconds()
                - ZoneId.of(TimeZone.getDefault().getID()).getRules().getOffset(Instant.now()).getTotalSeconds()) / 3600 ;
    }
}
