package ro.siveco.ael.service.utils;

import java.util.Random;

/**
 * Created by IulianB on 1/27/2015.
 */
public class PasswordGeneratorHelper {

    private static final String alphabet = "ACDEFGHIJKLMNOPQRSTUVWXYZabcdefgijklmnoprstuvwxyz1234567890";
    private static final Random RANDOM = new Random();

    public static String generatePassword() {
        return random(6, alphabet);
    }

    public static String generatePassword(int noLetters) {
        return random(noLetters, alphabet);
    }


    public static String random(int count, String chars) {
        if (chars == null) {
            return random(count, 0, 0, false, false, null, RANDOM);
        } else {
            return random(count, chars.toCharArray());
        }
    }

    public static String random(int count, char chars[]) {
        if (chars == null) {
            return random(count, 0, 0, false, false, null, RANDOM);
        } else {
            return random(count, 0, chars.length, false, false, chars, RANDOM);
        }
    }

    public static String random(int count, int start, int end, boolean letters, boolean numbers, char chars[], Random random) {
        if (count == 0) {
            return "";
        }
        if (count < 0) {
            throw new IllegalArgumentException("Requested random string length " + count + " is less than 0.");
        }
        if (start == 0 && end == 0) {
            end = 123;
            start = 32;
            if (!letters && !numbers) {
                start = 0;
                end = 0x7fffffff;
            }
        }
        StringBuffer buffer = new StringBuffer();
        int gap = end - start;
        while (count-- != 0) {
            char ch;
            if (chars == null) {
                ch = (char) (random.nextInt(gap) + start);
            } else {
                ch = chars[random.nextInt(gap) + start];
            }
            if (letters && numbers && Character.isLetterOrDigit(ch) || letters && Character.isLetter(ch) ||
                    numbers && Character.isDigit(ch) || !letters && !numbers) {
                buffer.append(ch);
            } else {
                count++;
            }
        }
        return buffer.toString();
    }

}
