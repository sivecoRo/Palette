package ro.siveco.ael.service;

import java.util.logging.Logger;

/**
 * User: AlexandruVi
 * Date: 2017-02-21
 */
abstract public class BaseService {

    private final Logger logger = Logger.getLogger(getClass().toString());

    public BaseService() {
        getLogger().info("Service Initialized");
    }

    protected Logger getLogger() {
        return logger;
    }
}
