package ro.siveco.ael.service.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.stereotype.Component;
import ro.siveco.ael.web.components.WebPath;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * This class handles logout. Specifically, if the proper argument is provided, it redirects to "Your profile was
 * successfully deleted" information. Otherwise (if the profile has not been just deleted), logout redirects to login
 * page.
 * <p>
 * This has to be done as redirecting request response. Using, "redirect:" e.g., "index" from WebPath class, won't work.
 */
@Component
public class CustomLogoutSuccessHandler implements LogoutSuccessHandler {
    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {

        // getParameter returns null if the parameter does not exist.
        if (request.getParameter("profileDeleted") != null && request.getParameter("profileDeleted").equals("true")) {
            redirectResponse(request, response, WebPath.PROFILE_DELETED);
        } else {
            redirectResponse(request, response, WebPath.LOGIN);
        }
    }

    private void redirectResponse(HttpServletRequest request, HttpServletResponse response, String destination) {
        response.setStatus(HttpServletResponse.SC_MOVED_PERMANENTLY);
        response.setHeader("Location", request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + destination);
    }
}
