package ro.siveco.ael.service.security;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by AndradaC on 7/25/2017.
 */

public class CustomAuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler
{
    private String defaultFailureUrl;
    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();


    public CustomAuthenticationFailureHandler(String defaultFailureUrl) {
        this.defaultFailureUrl = defaultFailureUrl;
    }

    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws
            IOException, ServletException {
        saveException(request, exception);
        String urlPostfix = "";

        switch(exception.getClass().getSimpleName()){
            case "DisabledException":
                urlPostfix = "disabled.user.error";
                break;
            case "BadCredentialsException":
                urlPostfix="incorrect.credetials.error";
                String username = request.getParameter("username");
                String passw = request.getParameter("password");
                break;
            case "AccountExpiredException":
                urlPostfix="account.expired.error";
                break;
            case "UnapprovedClientAuthenticationException":
                urlPostfix="ip.not.recognized.error";
                break;
            case "AccountNotVerifiedException":
                urlPostfix="account.not.verified.error";
                break;
        }
        request.getSession().setAttribute("loginError", urlPostfix);
        redirectStrategy.sendRedirect(request, response, defaultFailureUrl);

    }


}
