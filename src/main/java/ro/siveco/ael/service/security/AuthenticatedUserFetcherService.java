package ro.siveco.ael.service.security;

import org.springframework.security.core.Authentication;
import ro.siveco.ael.lcms.domain.auth.model.AuthenticatedUser;
import ro.siveco.ael.lcms.domain.auth.model.User;
import ro.siveco.ael.lcms.domain.auth.model.UserOptional;

import java.util.Optional;

/**
 * User: AlexandruVi
 * Date: 2017-02-22
 */
public interface AuthenticatedUserFetcherService {

    User getLoggedUser();

    User getLoggedUser(Authentication auth);

    Optional<User> getLoggedUserOptional();

    Optional<User> getLoggedUserOptional(Authentication auth);

    UserOptional getLoggedUserOptional2();

    UserOptional getLoggedUserOptional2(Authentication auth);

    AuthenticatedUser getAuthenticatedUser() throws WebSecurityException;

    AuthenticatedUser getAuthenticatedUser(Authentication auth) throws WebSecurityException;
}
