package ro.siveco.ael.service.security.annotation;

import java.lang.annotation.*;

/**
 * Copyright: Copyright (c) 2007 Company: SIVECO Romania SA
 * @author RobertR
 * @version : 1.0 / Sep 11, 2007
 */

@Target( {ElementType.FIELD} )
@Retention( RetentionPolicy.RUNTIME )
@Inherited
public @interface Groups
{
	Default[] value();
	public static enum Default
	{
		Teachers( "Teachers" ),
		Students( "Students" ),
		Parents( "Parents" ),
		Administrators( "Administrators" ),
		Secretary( "Secretary" ),
		System( "System" ),
		Everybody( "Everybody" ),

        MultipleConnectors("MultipleConnectors"),
        Supervisors("Supervisors"),
		ContentApprover("ContentApprover"),
        TenantAdministrators("TenantAdministrators"),
		TeacherSupervisor("TeacherSupervisor");

		Default( String groupName )
		{
			this.groupName = groupName;
		}


		Default( String groupName, Boolean readOnly, Boolean imported )
		{
			this(groupName);
			this.readOnly = readOnly;
			this.imported = imported;
		}


		public Boolean getReadOnly()
		{
			return readOnly;
		}

		public void setReadOnly( Boolean readOnly )
		{
			this.readOnly = readOnly;
		}

		public Boolean getImported()
		{
			return imported;
		}

		public void setImported( Boolean imported )
		{
			this.imported = imported;
		}

		private String groupName;
		private Boolean readOnly = Boolean.TRUE;
		private Boolean imported = Boolean.FALSE;

		public String get()
		{
			return groupName;
		}

		@Override
		public String toString()
		{
			return groupName;
		}
	}

}
