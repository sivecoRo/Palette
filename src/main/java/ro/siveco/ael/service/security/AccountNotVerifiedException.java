package ro.siveco.ael.service.security;

import org.springframework.security.core.AuthenticationException;

public class AccountNotVerifiedException extends AuthenticationException {

    public AccountNotVerifiedException(String msg, Throwable t) {
        super(msg, t);
    }

    public AccountNotVerifiedException(String msg) {
        super(msg);
    }
}
