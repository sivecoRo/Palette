package ro.siveco.ael.service.security;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.social.security.SocialAuthenticationToken;
import org.springframework.stereotype.Service;
import ro.siveco.ael.facebook.model.SocialAuthenticatedUser;
import ro.siveco.ael.lcms.domain.auth.model.AuthenticatedUser;
import ro.siveco.ael.lcms.domain.auth.model.User;
import ro.siveco.ael.lcms.domain.auth.model.UserOptional;
import ro.siveco.ael.service.BaseService;

import java.util.Optional;

/**
 * User: AlexandruVi
 * Date: 2017-02-22
 */
@Service
public class DefaultAuthenticatedUserFetcherService extends BaseService implements AuthenticatedUserFetcherService {

    @Override
    public User getLoggedUser() {
        try {
            return getAuthenticatedUser().getUser();
        } catch (Exception e) {
            getLogger().warning(e.getClass() + ": " + e.getMessage());
            throw new NotAuthenticatedException(e.getMessage(), e);
        }
    }

    @Override
    public User getLoggedUser(Authentication auth) {
        try {
            return getAuthenticatedUser(auth).getUser();
        } catch (Exception e) {
            getLogger().warning(e.getClass() + ": " + e.getMessage());
            throw new NotAuthenticatedException(e.getMessage(), e);
        }
    }

    @Override
    public Optional<User> getLoggedUserOptional() {
        try {
            return Optional.of(getAuthenticatedUser().getUser());
        } catch (Exception e) {
            return Optional.empty();
        }
    }

    @Override
    public Optional<User> getLoggedUserOptional(Authentication auth) {
        try {
            return Optional.of(getAuthenticatedUser(auth).getUser());
        } catch (Exception e) {
            return Optional.empty();
        }
    }

    @Override
    public UserOptional getLoggedUserOptional2() {
        try {
            return UserOptional.of(getAuthenticatedUser().getUser());
        } catch (Exception e) {
            return UserOptional.empty();
        }
    }

    @Override
    public UserOptional getLoggedUserOptional2(Authentication auth) {
        try {

            User user = null;
            if( auth instanceof SocialAuthenticationToken) {
                user =  ((SocialAuthenticatedUser)((SocialAuthenticationToken)auth).getPrincipal()).getUser();
            } else {
                user = ((AuthenticatedUser) ((AbstractAuthenticationToken) auth).getPrincipal()).getUser();
            }

            return UserOptional.of(user);


          //  return UserOptional.of(getAuthenticatedUser(auth).getUser());
        } catch (Exception e) {
            return UserOptional.empty();
        }
    }

    @Override
    public AuthenticatedUser getAuthenticatedUser() throws WebSecurityException {
        return getAuthenticatedUser(SecurityContextHolder.getContext().getAuthentication());
    }

    @Override
    public AuthenticatedUser getAuthenticatedUser(Authentication auth) throws WebSecurityException {
        try {
            Object principal = auth.getPrincipal();
            if (principal instanceof String) {
                throw new NotAuthenticatedException(principal.toString());
            }
            return (AuthenticatedUser) principal;
        } catch (WebSecurityException e) {
            throw e;
        } catch (Exception e) {
            throw new NotAuthenticatedException(e.getMessage(), e);
        }
    }
}
