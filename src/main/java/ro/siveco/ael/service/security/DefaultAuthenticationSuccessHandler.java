package ro.siveco.ael.service.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.stereotype.Service;
import ro.siveco.ael.lcms.domain.auth.model.UserOptional;
import ro.siveco.ael.service.BaseService;
import ro.siveco.ael.web.components.WebPath;
import ro.siveco.ael.web.components.dd.FlashAttribute;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * User: AlexandruVi
 * Date: 2017-02-22
 */
@Service
public class DefaultAuthenticationSuccessHandler extends BaseService implements AuthenticationSuccessHandler {

    private final AuthenticatedUserFetcherService authenticatedUserFetcherService;

    @Autowired
    public DefaultAuthenticationSuccessHandler(AuthenticatedUserFetcherService authenticatedUserFetcherService) {
        this.authenticatedUserFetcherService = authenticatedUserFetcherService;
    }

    /**
     * Called when a user has been successfully authenticated.
     *
     * @param request        the request which caused the successful authentication
     * @param response       the response
     * @param authentication the <tt>Authentication</tt> object which was created during
     */
    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
                                        Authentication authentication) throws IOException, ServletException {

        try {
            UserOptional loggedUserOptional = authenticatedUserFetcherService.getLoggedUserOptional2(authentication);
            request.setAttribute(FlashAttribute.loggedUserOptional.name(), loggedUserOptional);
            loggedUserOptional.ifPresent(user -> {
                        try {
                            SavedRequest savedRequest = ((SavedRequest)request.getSession().getAttribute("SPRING_SECURITY_SAVED_REQUEST"));
                            String savedRequestUrl = savedRequest == null ? null : savedRequest.getRedirectUrl();
                            if (savedRequestUrl != null) {
                                response.sendRedirect(savedRequestUrl);
                            }
                            else {
                                response.sendRedirect(WebPath.INDEX);
                            }
                        } catch (IOException e) {
                            //@todo
                        }
                    }
            );

        } catch (WebSecurityException e) {
            getLogger().warning(e.getMessage());
        }
    }
}
