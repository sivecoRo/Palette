package ro.siveco.ael.service.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.oauth2.common.exceptions.UnapprovedClientAuthenticationException;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import ro.siveco.ael.config.FeaturesConfiguration;
import ro.siveco.ael.lcms.domain.auth.model.AelUser;
import ro.siveco.ael.lcms.domain.auth.model.User;
import ro.siveco.ael.lcms.domain.auth.service.AelUserService;
import ro.siveco.ael.lcms.domain.tenant.model.Tenant;
import ro.siveco.ael.lcms.domain.tenant.service.IpService;

import javax.servlet.http.HttpServletRequest;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import static org.springframework.web.servlet.i18n.SessionLocaleResolver.LOCALE_SESSION_ATTRIBUTE_NAME;

/**
 * Created by AndradaC on 7/26/2017.
 */
public class AdditionalAuthenticationProvider extends DaoAuthenticationProvider {

    @Autowired
    private AelUserService aelUserService;

    @Autowired
    private IpService ipService;

    @Autowired
    private Environment environment;

    @Autowired
    private ro.siveco.ael.lcms.domain.profile.service.UserDetailsService aelUserDetailsService;

    @Autowired
    private FeaturesConfiguration featuresConfiguration;

    public AdditionalAuthenticationProvider() {

    }

    @Override
    protected void additionalAuthenticationChecks(UserDetails userDetails,
                                                  UsernamePasswordAuthenticationToken authentication)
            throws AuthenticationException {

        super.additionalAuthenticationChecks(userDetails, authentication);

        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes())
                .getRequest();
        String stringLocale = request.getParameter("lang");
        Locale locale = null;
        String userName = userDetails.getUsername();
        Optional<User> user = aelUserService.findByUsername(userName);

        if (!user.isPresent()) {
            throw new BadCredentialsException(messages.getMessage(
                    "AbstractUserDetailsAuthenticationProvider.badCredentials",
                    "Bad credentials"));
        }

        stringLocale = aelUserService.getLanguageForLogin(user, request);
        Locale l = Locale.forLanguageTag(stringLocale);
        request.getSession().setAttribute(LOCALE_SESSION_ATTRIBUTE_NAME, l);

        aelUserDetailsService.createIfNotExist((AelUser) user.get());

        String ip = null;
        try {
            ip = InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {

            ip = request.getRemoteAddr();
        }

        List<String> ipList = new ArrayList<>();
        Boolean checkIpOnLogin = Boolean.FALSE;

        Tenant tenant = user.get().getTenant();
        if (tenant == null) {
            String value = environment.getProperty("tenantRoot.config.checkIPOnLogin");
            checkIpOnLogin = Boolean.parseBoolean(value);
        } else {
            checkIpOnLogin = tenant.getCheckIPOnLogin();
        }

        if (checkIpOnLogin) {
            ipList = ipService.getByTenant(tenant);

            if (ipList != null && ipList.size() > 0 && !ipList.contains(ip)) {
                throw new UnapprovedClientAuthenticationException("The IP is not valid");
            }
        }

        checkAccountVerified((AelUser) user.get());

    }


    /**
     * <p>Check if user account has been verified. The check will only be performed if
     * <code>features.profile-verification-enabled</code> is set to <code>true</code></p>
     *
     * <p>If the user is not verified, the {@link AccountNotVerifiedException} is thrown</p>
     * @param aelUser user to check for verification
     */
    private void checkAccountVerified(AelUser aelUser){

        if (featuresConfiguration.isProfileVerificationEnabled()){
            if (aelUser.getVerifiedDate() == null){
                throw new AccountNotVerifiedException("Account has not been verified yet.");
            }
        }


    }


    @Override
    public void setUserDetailsService(UserDetailsService userDetailsService) {

        super.setUserDetailsService(userDetailsService);
    }
}
