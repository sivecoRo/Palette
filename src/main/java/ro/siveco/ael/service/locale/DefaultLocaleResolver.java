package ro.siveco.ael.service.locale;

import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;

/**
 * User: AlexandruVi, Maciej Bogdanski
 * Date: 2017-02-21
 */
public class DefaultLocaleResolver extends SessionLocaleResolver {

    private List<Locale> supportedLocales;

    /**
     * <p>Determine the default locale for the given request,
     * Called if no Locale session attribute has been found.
     * </p>
     *
     * <p>The default implementation attempts to set the language
     * based on the request's accept-language header. If none of
     * the languages listed there are supported, the default locale
     * is returned.
     * </p>
     *
     *
     * @param request the request to resolve the locale for
     * @return the default locale (never {@code null})
     * @see #setDefaultLocale
     * @see javax.servlet.http.HttpServletRequest#getLocale()
     */
    protected Locale determineDefaultLocale(HttpServletRequest request) {

        return getBestOptionLocale(request, getDefaultLocale());
    }

    /**
     * Find best locale for the request. Go over all client accepted locales and
     * compare them against supported locales. Return the one that matches one of
     * the accepted client locales the best (both by country and language, or at
     * least by language).
     * <p>
     * If no suitable locale is found for client locale, return the default fallback
     * option.
     *
     * @param request  request
     * @param fallback default locale to fall back to if client acceptable locale
     *                 is not found among supported locales
     * @return Default locale to set. If requested locale is not found among supported locales,
     * return fallback locale.
     */
    private Locale getBestOptionLocale(HttpServletRequest request, Locale fallback) {
        Enumeration<Locale> requestLocales = request.getLocales();
        while (requestLocales.hasMoreElements()) {
            Locale requestedLocale = requestLocales.nextElement();
            Locale supportedLocale = getAsSupportedLocale(requestedLocale);

            if (isValidLocale(supportedLocale)) {
                return supportedLocale;
            }

        }
        return fallback;
    }


    private boolean isValidLocale(Locale locale) {
        return locale != null;
    }


    /**
     * Match locales by both language & country
     */
    private static final boolean STRICT_MATCH = true;

    /**
     * Match locales by only language
     */
    private static final boolean NON_STRICT_MATCH = !STRICT_MATCH;


    /**
     * Get best supported locale based on the client requested locale.
     * <p>
     * Check if the specified requested locale is supported:
     * - By doing a full locale comparison - i.e. including both the language and country of
     * the requestedLocale against supported locales.
     * - If this is not supported, compare by language only.
     *
     * @param requestedLocale requested locale to check for support
     * @return supported locale that is the closest match to requestedLocale,
     *         or <code>null</code> if requestedLocale wasn't found among
     *         supported locales
     */
    private Locale getAsSupportedLocale(Locale requestedLocale) {

        Locale supportedLocale = getAsSupportedLocale(requestedLocale, STRICT_MATCH);

        if (!isValidLocale(supportedLocale)){
            //Maybe we can find the best locale just by a matching language?
            //In this case, first locale matching just the language (in the supported locales
            //list) is returned.
            supportedLocale = getAsSupportedLocale(requestedLocale, NON_STRICT_MATCH);
        }


        return supportedLocale;
    }


    /**
     * Get best supported locale based on the client requested locale.
     * <p>
     * Check if the specified requested locale is supported:
     * - By doing a full locale comparison - i.e. including both the language and country of
     * the requestedLocale against supported locales (strictMatch = true)
     *
     * OR:
     *
     * - By comparing only the languages between requestedLocale and supported locales (strictMatch = false)
     *
     * @param requestedLocale requested locale to check for support
     * @param strictMatch <code>true</code> if matching should be done by both country and language,
     *                    <code>false</code> if matching should only be done by language
     * @return supported locale that is the closest match to requestedLocale,
     *         or <code>null</code> if requestedLocale wasn't found among
     *         supported locales
     */
    private Locale getAsSupportedLocale(Locale requestedLocale, boolean strictMatch) {
        List<Locale> supportedLocales = getSupportedLocales();

        if (supportedLocales == null || supportedLocales.isEmpty()) {
            throw new IllegalArgumentException("No supported locales have been defined.");
        }

        for (Locale supportedLocal : supportedLocales) {

            if (strictMatch) {
                if (isSupportedByCountryAndLanguage(requestedLocale, supportedLocal)) {
                    return supportedLocal;
                }
            } else {
                if (isSupportedByLanguage(requestedLocale, supportedLocal)) {
                    return supportedLocal;
                }
            }

        }

        //Haven't found any matches
        return null;
    }

    private boolean isSupportedByCountryAndLanguage(Locale locale, Locale supportedLocale) {
        return supportedLocale.equals(locale);
    }


    private boolean isSupportedByLanguage(Locale locale, Locale supportedLocale) {
        return supportedLocale.getLanguage().equals(locale.getLanguage());
    }

    /**
     * Return the configured list of supported locales.
     */
    public List<Locale> getSupportedLocales() {
        return this.supportedLocales;
    }

    public void setSupportedLocales(List<Locale> supportedLocales) {
        this.supportedLocales = supportedLocales;
    }
}
