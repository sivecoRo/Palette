package ro.siveco.ael;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.web.SpringDataWebAutoConfiguration;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.web.WebApplicationInitializer;
import ro.siveco.ael.config.GenerateFunctionFromEntity;
import ro.siveco.ram.starter.RamStarterConfiguration;
import ro.siveco.ram.starter.repository.SimpleEntityRepository;

import java.util.logging.Logger;

@SpringBootApplication(exclude = {SpringDataWebAutoConfiguration.class})
@ComponentScan
@EnableJpaRepositories(repositoryBaseClass = SimpleEntityRepository.class)
@Import(RamStarterConfiguration.class)
@EnableGlobalMethodSecurity
public class Ael7EdgeApplication extends SpringBootServletInitializer implements WebApplicationInitializer {

    static {
        //for localhost testing only
        javax.net.ssl.HttpsURLConnection.setDefaultHostnameVerifier(
                (hostname, sslSession) -> {
                    //return hostname.equals("some-domain-with-invalid-ssl-certificate.com");
                    return false;
                });
    }

    private final Logger logger = Logger.getLogger(getClass().toString());

//	@Override
//	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
//		return application.sources(Ael7EdgeApplication.class);
//	}

    public static void main(String[] args) {
        SpringApplication.run(Ael7EdgeApplication.class, args);
    }

    private Logger getLogger() {
        return logger;
    }

    @Bean
    CommandLineRunner init(GenerateFunctionFromEntity generateFunctionService) {
        return (args) -> {
            getLogger().info("JAVA_HOME = " + System.getenv("JAVA_HOME"));
            getLogger().info("SPRING_PROFILES_ACTIVE = " + System.getenv("SPRING_PROFILES_ACTIVE"));
            //someService.doSomething();
            generateFunctionService.generateFunctionsForEntities();
        };
    }
}
