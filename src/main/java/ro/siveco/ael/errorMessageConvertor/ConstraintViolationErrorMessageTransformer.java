package ro.siveco.ael.errorMessageConvertor;

import org.springframework.stereotype.Component;
import ro.siveco.ram.starter.errorHandler.BaseErrorMessageTransformer;

import java.sql.SQLException;
import java.util.regex.Pattern;

/**
 * User: AlexandruVi
 * Date: 2017-06-29
 */
@Component
public class ConstraintViolationErrorMessageTransformer extends BaseErrorMessageTransformer<SQLException> {

    @Override
    public Pattern getPattern() {
        return Pattern.compile(
                "update or delete on table \"(\\w+)\" violates foreign key constraint \"(\\w+)\" on table \"(\\w+)\"");
    }

    @Override
    public String getMessageTemplate() {
        return "error.sql.constraint.violation.%s.%3$s";
    }
}
