package ro.siveco.ael.lcms.repository;

import org.springframework.stereotype.Repository;
import ro.siveco.ael.lcms.domain.notification.model.UserMessageFormat;
import ro.siveco.ram.repository.BaseEntityRepository;

/**
 * Created by IuliaP on 28.09.2017.
 */
@Repository
public interface NotificationRepository extends BaseEntityRepository<UserMessageFormat> {

}
