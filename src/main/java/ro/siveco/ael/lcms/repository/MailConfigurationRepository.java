package ro.siveco.ael.lcms.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ro.siveco.ael.lcms.domain.mail.model.MailConfiguration;
import ro.siveco.ram.repository.BaseEntityRepository;

import java.util.List;

/**
 * Created by AndradaC on 5/29/2017.
 */
@Repository
public interface MailConfigurationRepository extends BaseEntityRepository<MailConfiguration> {

    MailConfiguration findOneByTenantId(Long tenantId);

    @Query("select mc from MailConfiguration mc where mc.tenant.id is null")
    List<MailConfiguration> getMailConfiguration();
}
