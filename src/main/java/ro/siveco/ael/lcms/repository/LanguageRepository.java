package ro.siveco.ael.lcms.repository;

import ro.siveco.ael.lcms.domain.metadata.model.Language;
import ro.siveco.ram.repository.BaseEntityRepository;

import java.util.List;

/**
 * Created by IuliaP on 15.06.2017.
 */
public interface LanguageRepository extends BaseEntityRepository<Language> {

    List<Language> findAllByName(String name);

}
