package ro.siveco.ael.lcms.repository;

import org.springframework.stereotype.Repository;
import ro.siveco.ael.lcms.domain.menu.model.MenuItem;
import ro.siveco.ram.repository.BaseEntityRepository;

@Repository
public interface MenuItemRepository extends BaseEntityRepository<MenuItem> {

}
