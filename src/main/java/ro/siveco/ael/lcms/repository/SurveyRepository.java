package ro.siveco.ael.lcms.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ro.siveco.ael.lcms.domain.auth.model.User;
import ro.siveco.ael.lcms.domain.materials.model.Survey;
import ro.siveco.ael.lcms.domain.materials.model.UserSurveyAnswer;
import ro.siveco.ram.repository.BaseEntityRepository;

import java.util.List;

/**
 * Created by AndradaC on 4/27/2017.
 */
@Repository
public interface SurveyRepository extends BaseEntityRepository<Survey> {

    Survey findOneByDefinitionAndCreator(String name, User creator);

    Survey findOneById(Long id);

    @Query("select usa from UserSurveyAnswer usa where usa.plannedCourse.id = ?1 and usa.user.id=?2")
    List<UserSurveyAnswer> getByPlannedCourseAndStudent(Long plannedCourseId, Long userId);
}
