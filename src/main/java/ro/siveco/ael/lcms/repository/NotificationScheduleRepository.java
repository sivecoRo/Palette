package ro.siveco.ael.lcms.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ro.siveco.ael.lcms.domain.notification.model.NotificationSchedule;
import ro.siveco.ael.lcms.domain.notification.model.PlannedNotification;
import ro.siveco.ael.lcms.domain.notification.model.UserMessageType;
import ro.siveco.ram.repository.BaseEntityRepository;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;


@Repository
public interface NotificationScheduleRepository extends BaseEntityRepository<NotificationSchedule> {
	
	@Query("select ns.id from NotificationSchedule ns where ns.status=false and ns.date<?1")
	public List<Long> getAllNotificationScheduleToSend(Date date);

	@Query("delete from NotificationSchedule where userMessageType=?1 and toUser.id=?2 and entityId=?3 and status=false")
	public void deleteUnsentNotificationScheduleByTypeAndUserAndEntityId( UserMessageType userMessageType, Long userId, Long entityId );

	@Transactional
	@Query("delete from NotificationSchedule where plannedNotification.id=?1")
	@Modifying
	void deleteNotificationScheduleByPlannedNotification( Long plannedNotificationId );

	List<NotificationSchedule> findAllByPlannedNotification(PlannedNotification plannedNotification);

	@Query("select count(*) from NotificationSchedule ns where ns.status=true and ns.plannedNotification.id=?1")
	Long countAllSentByPlannedNotification(Long plannedNotificationId);
	
}
