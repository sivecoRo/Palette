package ro.siveco.ael.lcms.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ro.siveco.ael.lcms.domain.profile.model.UserCharacteristicValue;
import ro.siveco.ram.repository.BaseEntityRepository;

import java.util.List;

/**
 * User: CatalinS
 * Date: 2017-06-19
 */
@Repository
public interface UserCharacteristicValueRepository extends BaseEntityRepository<UserCharacteristicValue> {


	@Query("select ucv from UserCharacteristicValue ucv where ucv.characteristic.id=?1 and ucv.user.id=?2")
	List<UserCharacteristicValue> getCharacteristicValuesByCharactAndUser(Long id, Long userId);

	@Query("select ucv from UserCharacteristicValue ucv where ucv.characteristic.id=?1 and ucv.user.id=?2 and ucv.metadata.id=?3")
	List<UserCharacteristicValue> getCharacteristicValuesByCharactAndUserAndMetadata(Long id, Long userId,
			Long metadataId);

	@Query("select ucv from UserCharacteristicValue ucv where ucv.user.id=?1")
	List<UserCharacteristicValue> getCharacteristicValuesBydUser(Long userId);
}
