package ro.siveco.ael.lcms.repository;

import org.springframework.stereotype.Service;
import ro.siveco.ael.lcms.domain.notification.model.UserMessageFormat;
import ro.siveco.ram.repository.BaseEntityRepository;

/**
 * Created by IuliaP on 29.09.2017.
 */
@Service
public interface UserMessageFormatRepository extends BaseEntityRepository<UserMessageFormat> {

    public UserMessageFormat findOneByCode(String code);

    public UserMessageFormat findOneByName(String name);
}
