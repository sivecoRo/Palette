package ro.siveco.ael.lcms.repository;

import org.springframework.stereotype.Repository;
import ro.siveco.ael.lcms.domain.notification.model.PlannedNotification;
import ro.siveco.ram.repository.BaseEntityRepository;

import java.util.List;

/**
 * Created by IuliaP on 28.09.2017.
 */
@Repository
public interface PlannedNotificationRepository extends BaseEntityRepository<PlannedNotification> {

    List<PlannedNotification> findAllByEntityIdAndSendByEvent(Long entityId, Boolean sendByEvent);
}
