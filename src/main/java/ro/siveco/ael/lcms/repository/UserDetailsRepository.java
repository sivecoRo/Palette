package ro.siveco.ael.lcms.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ro.siveco.ael.lcms.domain.auth.model.AelUser;
import ro.siveco.ael.lcms.domain.profile.model.UserDetails;
import ro.siveco.ram.repository.BaseEntityRepository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserDetailsRepository extends BaseEntityRepository<UserDetails> {

	@Query("select ud from UserDetails ud where ud.user.id=?1")
	List<UserDetails> getByUserId(Long userId);

    Optional<UserDetails>  findOneByUser(AelUser aelUser);

    @Query("select count(c) from AelUser u, Course c, PlannedCourse pc where c.creator.id = u.id and u.id = ?1 and pc.course.id = c.id and pc.reviewed is not null and pc.reviewed = true")
	Long getCreatedItemsNo(Long userId);

	@Query("select count(pcs) from AelUser u, PlannedCourseStudent pcs where pcs.user.id = u.id and u.id = ?1 and pcs.attending is not null and pcs.attending = true " +
			" and pcs.reviewed is not null and pcs.reviewed = true")
	Long getFavoredItemsNo(Long userId);
	
}
