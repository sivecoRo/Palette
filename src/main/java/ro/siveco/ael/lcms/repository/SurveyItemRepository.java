package ro.siveco.ael.lcms.repository;

import org.springframework.stereotype.Repository;
import ro.siveco.ael.lcms.domain.materials.model.SurveyItem;
import ro.siveco.ram.repository.BaseEntityRepository;

import java.util.List;

/**
 * Created by AndradaC on 5/2/2017.
 */
@Repository
public interface SurveyItemRepository extends BaseEntityRepository<SurveyItem> {
    SurveyItem findOneById(Long id);

    List<SurveyItem> findBySurveyId(Long id);
}
