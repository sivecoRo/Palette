package ro.siveco.ael.lcms.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ro.siveco.ael.lcms.domain.chat.model.ChatMessage;
import ro.siveco.ram.repository.BaseEntityRepository;

import java.util.List;


@Repository
public interface ChatMessageRepository extends BaseEntityRepository<ChatMessage> {

	@Query("select cm from ChatMessage cm where (cm.sender=?1 and cm.receiver=?2) or (cm.sender=?2 and cm.receiver=?1) "
			+ "order by cm.creationDate")
    List<ChatMessage> getMessagesBetweenTwoUsers(String user1, String user2);
}
