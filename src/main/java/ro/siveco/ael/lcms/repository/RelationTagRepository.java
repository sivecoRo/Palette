package ro.siveco.ael.lcms.repository;

import ro.siveco.ael.lcms.domain.metadata.model.Relation;
import ro.siveco.ael.lcms.domain.metadata.model.RelationTag;
import ro.siveco.ael.lcms.domain.metadata.model.Tag;
import ro.siveco.ram.repository.BaseEntityRepository;

import java.util.List;

/**
 * Created by IuliaP on 15.06.2017.
 */
public interface RelationTagRepository extends BaseEntityRepository<RelationTag> {

    List<RelationTag> findAllBySourceTagAndAndDestinationTagAndRelation(Tag sourceTag, Tag destinationTag, Relation relation);
}
