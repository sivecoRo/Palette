package ro.siveco.ael.lcms.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.stereotype.Service;
import ro.siveco.ael.lcms.domain.profile.model.LanguageDTO;
import ro.siveco.ael.web.mvc.MvcProperties;
import ro.siveco.ram.starter.repository.BaseDummyRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@NoRepositoryBean
@Service
public class LanguageDTORepository extends BaseDummyRepository<LanguageDTO, String> {

    private final MvcProperties mvcProperties;

    @Autowired
    public LanguageDTORepository(MvcProperties mvcProperties) {

        this.mvcProperties = mvcProperties;
    }

    @Override
    public Page<LanguageDTO> findAll(Pageable pageable) {

        List<LanguageDTO> languageDTOList = new ArrayList<>();
        for (Locale locale : mvcProperties.getAvailableLocales()) {
            LanguageDTO languageDTO = new LanguageDTO();
            languageDTO.setName(locale.getDisplayLanguage());
            languageDTO.setId(locale.getLanguage());
            languageDTOList.add(languageDTO);
        }

        return new PageImpl<>(languageDTOList, pageable, languageDTOList.size());
    }
}
