package ro.siveco.ael.lcms.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ro.siveco.ael.lcms.domain.auth.model.AelUser;
import ro.siveco.ael.lcms.domain.auth.model.User;
import ro.siveco.ael.lcms.domain.tenant.model.Tenant;
import ro.siveco.ram.repository.BaseEntityRepository;

import java.util.List;
import java.util.Optional;

/**
 * User: AlexandruVi
 * Date: 2017-02-20
 */
@Repository
public interface AelUserRepository extends BaseEntityRepository<AelUser>, UserRepository {

    Optional<User> findOneByUsername(String username);

    User findOneById(Long id);

    AelUser findByUsernameAndTenant(String username, Tenant tenant);

    @Query("select af from AelUser af where af.id in ?1")
    List<AelUser> findByIdUsers(List<Long> id);

    AelUser findByMarca(String marca);

    User findOneByUserSocialId(String userSocialId);

    User findOneByEmail(String email);

    @Query("select au.username from AelUser au where au.tenant=?1")
    List<AelUser> findAllByTenant(Tenant tenant);

    @Query("select af from AelUser af ")
    List<AelUser> findAll();

    @Query("select COUNT(af.id) from AelUser af where af.city=?1")
    Long findUserCountForLocation(String location);

    @Query("select COUNT(af.id) from AelUser af where af.city is not null and (trim(af.city)=?1 or ( length(af.city) > 2 and trim(substring(af.city,0,length(af.city))) = trim(substring(?1,0,length(?1)))))")
    Long findUserCountForLocationCoordinates(String coordinates);

    @Query("select af from AelUser af where af.email in ?1")
    List<AelUser> findAllByEmail(List<String> emails);

    @Query("select au.tenant.id from AelUser au where au.username=?1")
    Long getTenantIdByUsername(String username);

    @Query("select au from AelUser au where au.username=?1")
    AelUser getAelUserByUsername(String username);

    @Query("select au.username from AelUser au where au.tenant.id=?1")
    List<String> getUsernamesByTenant(Long tenantId);

    @Query("select distinct u from AelUser u join u.groups g where g.name=?1")
    List<AelUser> getUsersByGroup(String groupName);

    @Query("select au from AelUser au where au.email=?1")
    User getByMailAddress(String email);

    @Query("select distinct au from AelUser au where au.tenant.id=?1 and au.disabled=false order by id asc")
    List<AelUser> getUsersByTenantId(Long tenantId);

    @Query("select distinct au from AelUser au where au.tenant is null and au.disabled=false order by id asc")
    List<AelUser> getUsersByTenantNull();

    @Query("select distinct au from AelUser au join au.groups g where g.id=?1 and au.tenant.id=?2 and au.disabled=false ")
    List<AelUser> getUsersByGroupIdAndTenantId(Long groupId, Long tenantId);

    @Query("select distinct au from AelUser au join au.groups g where g.id=?1 and au.tenant is null and au.disabled=false ")
    List<AelUser> getUsersByGroupIdAndTenantNull(Long groupId);
}
