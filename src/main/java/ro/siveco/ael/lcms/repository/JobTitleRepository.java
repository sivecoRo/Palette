package ro.siveco.ael.lcms.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ro.siveco.ael.lcms.domain.organization.model.JobTitle;
import ro.siveco.ael.lcms.domain.tenant.model.Tenant;
import ro.siveco.ram.repository.BaseEntityRepository;

import java.util.List;

/**
 * Created by AndradaC on 5/29/2017.
 */
@Repository
public interface JobTitleRepository extends BaseEntityRepository<JobTitle>
{

	@Query("select jt from JobTitle jt where jt.name=?1")
	List<JobTitle> getByName(String name);

	@Query("select jt from JobTitle jt where (?1 is null and jt.tenant.id is null) or (?1 is not null and jt.tenant.id=?1) order by jt.id asc")
	List<JobTitle> findJobTitlesByTenant(Long tenantId);

	JobTitle findByNameAndTenant(String name, Tenant tenant);
}
