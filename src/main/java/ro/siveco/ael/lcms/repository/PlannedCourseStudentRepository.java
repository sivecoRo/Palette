package ro.siveco.ael.lcms.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ro.siveco.ael.lcms.domain.auth.model.AelUser;
import ro.siveco.ael.lcms.domain.course.model.PlannedCourse;
import ro.siveco.ael.lcms.domain.course.model.PlannedCourseStudent;
import ro.siveco.ram.repository.BaseEntityRepository;

import java.util.Date;
import java.util.List;

/**
 * Created by LiviuI on 3/29/2017.
 */
@Repository
public interface PlannedCourseStudentRepository extends BaseEntityRepository<PlannedCourseStudent> {

    @Query("select pcs from PlannedCourseStudent pcs where pcs.plannedCourse.id=?1 and pcs.user.id=?2")
    PlannedCourseStudent findByPlanificationAndStundent(Long plannedCourseId, Long studentId);

    PlannedCourseStudent findOneById(Long plannedCourseStudentId);

    List<PlannedCourseStudent> findByPlannedCourseId(Long plannedCourseId);

    @Query("select count(pcs.id) from PlannedCourseStudent pcs where pcs.user.id=?1")
    Long getTotalCourseNumberForStudent(Long userId);

    @Query("select count(pcs.id) from PlannedCourseStudent pcs where pcs.user.id=?1 and " +
            "pcs.startDate < ?2 and pcs.endDate <= ?2")
    Long getEndedCoursesNumberForStudent(Long userId, Date currentDate);

    @Query("select count(pcs.id) from PlannedCourseStudent pcs where pcs.user.id=?1 and " +
            "pcs.startDate > ?2 and pcs.endDate > ?2")
    Long getFutureCoursesNumberForStudent(Long userId, Date currentDate);

    @Query("select count(pcs.id) from PlannedCourseStudent pcs where pcs.user.id=?1 and " +
            "pcs.startDate <= ?2 and pcs.endDate > ?2")
    Long getOpenCoursesNumberForStudent(Long userId, Date currentDate);

    @Query("select count(pcs.id) from PlannedCourseStudent pcs where pcs.plannedCourse.id=?1")
	Long getTotalCourseStudentsNumber(Long plannedCourseId);

    @Query("select pcs.user from PlannedCourseStudent pcs where pcs.plannedCourse.id=?1")
	List<AelUser> getTotalCourseStudents(Long id);

    @Query("select count(distinct pcs.user.id) " +
            " from PlannedCourseStudent pcs, PlannedCourse pc " +
            " where pcs.plannedCourse.id=pc.id and pc.course.id = ?1")
    public Integer getStudentsNumberForCourse(Long courseId);

    @Query("select count(distinct pcs.user.id) " +
            " from PlannedCourseStudent pcs, PlannedCourse pc " +
            " where pcs.plannedCourse.id=pc.id and pc.course.id = ?1 and pcs.addedToFavorites=true")
    public Integer getFavoritesStudentsNumberForCourse(Long courseId);

    @Query("select count(distinct pcs.user.id) " +
            " from PlannedCourseStudent pcs, PlannedCourse pc " +
            " where pcs.plannedCourse.id=pc.id and pc.course.id = ?1 and pcs.likeItem=true")
    public Integer getItemLikesNumber(Long courseId);

    @Query("select count(distinct pcs.user.id) " +
            " from PlannedCourseStudent pcs, PlannedCourse pc " +
            " where pcs.plannedCourse.id=pc.id and pc.course.id = ?1 and pcs.dislikeItem=true")
    public Integer getItemDislikesNumber(Long courseId);

    PlannedCourseStudent findPlannedCourseStudentByPlannedCourseAndUserAndRemoveItem(PlannedCourse plannedCourse, AelUser user, Boolean removeItem);

    @Query("select count(distinct pcs.user.id) " +
            " from PlannedCourseStudent pcs, PlannedCourse pc " +
            " where pcs.plannedCourse.id=pc.id and pc.course.id = ?1 and pcs.attending=true")
    public Integer getAttendingStudentsNumberForCourse(Long courseId);

    @Query("select distinct pcs.user " +
            " from PlannedCourseStudent pcs, PlannedCourse pc " +
            " where pcs.plannedCourse.id=pc.id and pc.course.id = ?1 and pcs.attending=true")
    public List<AelUser> getAttendingStudentsForCourse(Long courseId);

    @Query("select distinct pcs " +
            " from PlannedCourseStudent pcs, PlannedCourse pc " +
            " where pcs.plannedCourse.id=pc.id and pc.course.id = ?1 and pcs.attending=true")
    public List<PlannedCourseStudent> getAttendingPCStudentsForCourse(Long courseId);
}
