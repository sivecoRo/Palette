package ro.siveco.ael.lcms.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ro.siveco.ael.lcms.domain.tenant.model.Ip;
import ro.siveco.ael.lcms.domain.tenant.model.Tenant;
import ro.siveco.ram.repository.BaseEntityRepository;

import java.util.List;

@Repository
public interface IpRepository extends BaseEntityRepository<Ip> {

    @Query("select ip.value from Ip ip where ip.tenant=?1")
    public List<String> getByTenant(Tenant tenant);

    @Query("select ip.value from Ip ip where ip.tenant is null")
    public List<String> getByTenantNull();

    public Ip findOneByValueAndTenant(String value, Tenant tenant);

}
