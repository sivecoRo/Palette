package ro.siveco.ael.lcms.repository;

import org.springframework.data.jpa.repository.Query;
import ro.siveco.ael.lcms.domain.metadata.model.CommentInappropriate;
import ro.siveco.ram.repository.BaseEntityRepository;

import java.util.List;

/**
 * Created by primozk on 3/20/2019.
 */
public interface CommentInappropriateRepository extends BaseEntityRepository<CommentInappropriate> {
    @Query("select c from CommentInappropriate c")
    List<CommentInappropriate> findAll();

    @Query("select c from CommentInappropriate c where c.plannedCourse.id=?1 order by c.commentDate")
    List<CommentInappropriate> findByPlannedCourseId(Long plannedCourseId);

    @Query("select c from CommentInappropriate c where c.aelUser.id=?1")
    List<CommentInappropriate> findByUserId(Long userId);

    @Query("select c from CommentInappropriate c where c.plannedCourse.course.id=?1 order by c.commentDate")
    List<CommentInappropriate> findByCourseId(Long courseId);
}
