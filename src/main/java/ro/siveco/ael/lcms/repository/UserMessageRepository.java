package ro.siveco.ael.lcms.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ro.siveco.ael.lcms.domain.notification.model.UserMessage;
import ro.siveco.ael.lcms.domain.notification.model.UserMessageType;
import ro.siveco.ram.repository.BaseEntityRepository;

import java.util.List;

@Repository
public interface UserMessageRepository extends BaseEntityRepository<UserMessage> {
	
	@Query("select um from UserMessage um where um.to.id=?1 and um.sent=true and um.ownerCopy=false"
			+ " order by um.sentDate desc")
	List<UserMessage> getAllReceivedMessagesByUserId(Long userId);
	
	@Query("select um from UserMessage um where um.to.id=?1 and um.sent=true and um.ownerCopy=false"
			+ " and um.userMessageType=?2 order by um.sentDate desc")
	List<UserMessage> getAllReceivedMessagesByUserIdAndType(Long userId, UserMessageType userMessageType);

	@Query("select um from UserMessage um where um.from.id=?1 and um.sent=true and um.ownerCopy=false"
			+ " and um.userMessageType=?2 order by um.sentDate desc")
	List<UserMessage> getAllSentMessagesByUserIdAndType(Long userId, UserMessageType userMessageType);

	@Query("select um from UserMessage um where um.from.id=?1 and um.sent=false"
			+ " and um.userMessageType=?2 order by um.sentDate desc")
	List<UserMessage> getAllDraftMessagesByUserIdAndType(Long userId, UserMessageType userMessageType);
	
	@Query("select um from UserMessage um where um.source.id=?1")
	List<UserMessage>getAllMessagesRelatedToSource(Long sourceId);
	
	@Query("select um from UserMessage um where um.to.id=?1 and um.sent=true and um.ownerCopy=false"
			+ " and um.userMessageType<>?2 order by um.sentDate desc")
	List<UserMessage> getAllNotificationsByUserId(Long userId, UserMessageType userMessageType);

}
