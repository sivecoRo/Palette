package ro.siveco.ael.lcms.repository;

import org.springframework.stereotype.Repository;
import ro.siveco.ael.lcms.domain.notification.model.MessageTemplate;
import ro.siveco.ram.repository.BaseEntityRepository;

import java.util.List;

/**
 * Created by IuliaP on 27.09.2017.
 */
@Repository
public interface MessageTemplateRepository extends BaseEntityRepository<MessageTemplate> {

    List<MessageTemplate> findByName(String name);

    List<MessageTemplate> findByNameAndLocale(String name, String locale);

    List<MessageTemplate> findByNameAndLocaleAndTenantId(String name, String locale, Long tenantId);

}
