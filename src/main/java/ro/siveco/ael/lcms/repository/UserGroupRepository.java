package ro.siveco.ael.lcms.repository;

import org.springframework.stereotype.Repository;
import ro.siveco.ael.lcms.domain.auth.model.UserGroup;
import ro.siveco.ram.repository.BaseEntityRepository;

/**
 * User: AlexandruVi
 * Date: 2018-01-22
 */
@Repository
public interface UserGroupRepository extends BaseEntityRepository<UserGroup> {

}
