package ro.siveco.ael.lcms.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ro.siveco.ael.lcms.domain.taxonomies.model.Competence;
import ro.siveco.ael.lcms.domain.tenant.model.Tenant;
import ro.siveco.ram.repository.BaseEntityRepository;

import java.util.List;

/**
 * Created by JohnB on 16/06/2017.
 */

@Repository
public interface CompetenceRepository extends BaseEntityRepository<Competence> {

    @Query("select c from Competence c where (?1 is null and c.tenant.id is null) or (?1 is not null and c.tenant.id=?1)")
    List<Competence> findCompetencesByTenant(Long tenantId);

    Competence findByLabelAndTenant(String label, Tenant tenant);

    Competence findById(Long id);
}