package ro.siveco.ael.lcms.repository;

import org.springframework.stereotype.Repository;
import ro.siveco.ael.lcms.domain.profile.model.Characteristic;
import ro.siveco.ram.repository.BaseEntityRepository;
/**
 * User: CatalinS
 * Date: 2017-07-05
 */
@Repository
public interface CharacteristicRepository extends BaseEntityRepository<Characteristic> {

}
