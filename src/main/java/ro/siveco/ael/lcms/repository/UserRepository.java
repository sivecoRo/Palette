package ro.siveco.ael.lcms.repository;

import ro.siveco.ael.lcms.domain.auth.model.User;

import java.io.Serializable;
import java.util.Optional;

/**
 * User: AlexandruVi
 * Date: 2017-02-20
 */
public interface UserRepository {

    Optional<User> findOneByUsername(String username);

    User findOneByUserSocialId(String userSocialId);

    User findOneByRelatedUid(String relatedUid);

    User findOneById(Serializable id);
}
