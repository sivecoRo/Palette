package ro.siveco.ael.lcms.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ro.siveco.ael.lcms.domain.auth.model.AelFunction;
import ro.siveco.ram.repository.BaseEntityRepository;

import java.util.List;

@Repository
public interface AelFunctionRepository extends BaseEntityRepository<AelFunction> {

    AelFunction findByName(String name);

    AelFunction findById(Long id);

    @Query("select af from AelFunction af where af.id in ?1")
    List<AelFunction> findByIdFunctions(List<Long> id);

    @Query("select af from AelFunction af ")
    List<AelFunction> findAll();

}
