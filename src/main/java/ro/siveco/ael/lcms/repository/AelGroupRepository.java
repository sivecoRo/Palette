package ro.siveco.ael.lcms.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ro.siveco.ael.lcms.domain.auth.model.AelGroup;
import ro.siveco.ael.lcms.domain.auth.model.Group;
import ro.siveco.ael.lcms.domain.tenant.model.Tenant;
import ro.siveco.ram.repository.BaseEntityRepository;

import java.util.List;

/**
 * Created by AndradaC on 5/29/2017.
 */
@Repository
public interface AelGroupRepository extends BaseEntityRepository<AelGroup> {
    AelGroup findById(Long id);

    AelGroup findByName(String name);

    AelGroup findByNameAndTenant(String name, Tenant tenant);

    List<AelGroup> findAllByTenant(Tenant tenant);

    @Query("select ag from AelGroup ag")
    List<AelGroup> findAll();

    @Query("select ag from AelGroup ag")
    List<Group> findAllGroup();

    @Query("select ag from AelGroup ag where ag.id in ?1")
    List<AelGroup> findByIdGroups(List<Long> id);

    @Query("select ag from AelGroup ag where ag.name in ?1")
    List<AelGroup> findByNameGroups(String nameGroups);

    @Query("select ag from AelGroup ag where ag.tenant in ?1")
    List<Group> findTenant(Tenant tenant);
}
