package ro.siveco.ael.lcms.repository;

import org.springframework.stereotype.Repository;
import ro.siveco.ael.lcms.domain.materials.model.WebLink;
import ro.siveco.ram.repository.BaseEntityRepository;

/**
 * Created by AndradaC on 5/5/2017.
 */
@Repository
public interface WebLinkRepository extends BaseEntityRepository<WebLink>
{
    WebLink findOneById(Long id);
    WebLink findOneByLearningMaterialNodeId(String nodeId);
}
