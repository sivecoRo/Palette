package ro.siveco.ael.lcms.repository;

import org.springframework.data.jpa.repository.Query;
import ro.siveco.ael.lcms.domain.notification.model.NotificationPreference;
import ro.siveco.ael.lcms.domain.notification.model.UserMessageType;
import ro.siveco.ram.repository.BaseEntityRepository;

import java.util.List;

public interface NotificationPreferenceRepository extends BaseEntityRepository<NotificationPreference> {

	@Query("delete from NotificationPreference where user.id = ?1")
	public void deleteNotificationPreferencesByUserId( Long userId );
	
	@Query("select np from NotificationPreference np where np.user.id=?1")
	public List<NotificationPreference> getNotificationPreferencesByUserId( Long userId );
	
	@Query("select np from NotificationPreference np where np.user.id=?1 and np.userMessageType=?2")
	public NotificationPreference getNotificationPreferencesByUserIdAndByType( Long userId, UserMessageType userMessageType );

}
