package ro.siveco.ael.lcms.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ro.siveco.ael.lcms.domain.tenant.model.Tenant;
import ro.siveco.ram.repository.BaseEntityRepository;

import java.util.List;

/**
 * Created by andradac on 4/13/2017.
 */
@Repository
public interface TenantRepository extends BaseEntityRepository<Tenant> {

    Tenant findOneById(Long id);

    @Query("select t from Tenant t where t.active = true ")
    List<Tenant> findAllActiveTenants();

    Tenant findOneByName(String name);
}