package ro.siveco.ael.lcms.repository;

import org.springframework.stereotype.Repository;
import ro.siveco.ael.lcms.domain.metadata.model.Country;
import ro.siveco.ram.repository.BaseEntityRepository;

import java.util.List;

/**
 * Created by albertb on 2/8/2018.
 */
@Repository
public interface CountryRepository extends BaseEntityRepository<Country> {
    List<Country> findAll();

    List<Country> findAllByCountryName(String countryName);

    List<Country> findAllByCode(String countryCode);
}
