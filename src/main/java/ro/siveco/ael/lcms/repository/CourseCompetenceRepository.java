package ro.siveco.ael.lcms.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ro.siveco.ael.lcms.domain.course.model.CourseCompetence;
import ro.siveco.ael.lcms.domain.taxonomies.model.Competence;
import ro.siveco.ael.lcms.domain.taxonomies.model.TaxonomyLevel;
import ro.siveco.ram.repository.BaseEntityRepository;

import java.util.List;

@Repository
public interface CourseCompetenceRepository extends BaseEntityRepository<CourseCompetence> {

    @Query("select cc from CourseCompetence cc where cc.id <> ?3 and cc.course.id  = ?1 and cc.competence.id=?2")
    CourseCompetence findDuplicateConnexion(Long courseId, Long competenceId, Long id);

    @Query("select c from CourseCompetence c where (c.competence = ?1)")
    List<CourseCompetence> findByCompetence(Competence competence);

    @Query("select c from CourseCompetence c where c.taxonomyLevel = ?1")
    List<CourseCompetence> findByTaxonomyLevel(TaxonomyLevel taxonomyLevel);
}