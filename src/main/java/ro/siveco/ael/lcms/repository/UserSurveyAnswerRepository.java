package ro.siveco.ael.lcms.repository;

import org.springframework.stereotype.Repository;
import ro.siveco.ael.lcms.domain.materials.model.UserSurveyAnswer;
import ro.siveco.ram.repository.BaseEntityRepository;

/**
 * Created by IuliaP on 02.11.2017.
 */
@Repository
public interface UserSurveyAnswerRepository extends BaseEntityRepository<UserSurveyAnswer> {

}
