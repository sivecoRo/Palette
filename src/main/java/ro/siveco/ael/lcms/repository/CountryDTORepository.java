package ro.siveco.ael.lcms.repository;

import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.stereotype.Service;
import ro.siveco.ael.lcms.domain.profile.model.CountryDTO;
import ro.siveco.ram.starter.repository.BaseDummyRepository;

@NoRepositoryBean
@Service
public class CountryDTORepository extends BaseDummyRepository<CountryDTO, String> {

}
