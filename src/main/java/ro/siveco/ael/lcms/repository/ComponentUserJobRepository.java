package ro.siveco.ael.lcms.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ro.siveco.ael.lcms.domain.organization.model.ComponentUserJob;
import ro.siveco.ael.lcms.domain.organization.model.JobTitle;
import ro.siveco.ael.lcms.domain.organization.model.TreeNode;
import ro.siveco.ram.repository.BaseEntityRepository;

import java.util.List;

/**
 * User: AlexandruVi
 * Date: 2017-04-04
 */
@Repository
public interface ComponentUserJobRepository extends BaseEntityRepository<ComponentUserJob> {

    @Query("SELECT new ro.siveco.ael.lcms.domain.organization.model.TreeNode( cuj.id, cuj.user.displayName, 1 ) FROM ComponentUserJob cuj where cuj.component.id=?1")
    public List<TreeNode> getComponentChildTreeNodes(Long componentId);

    public ComponentUserJob findByUserIdAndActive(Long userId, Boolean active);

    @Query("select cuj from ComponentUserJob cuj where cuj.user.id = ?1 and cuj.component.id=?2 and cuj.jobTitle.id=?3")
    public ComponentUserJob getComponentUserJobForUserComponentAndJobTitle(Long userId, Long componentId, Long jobTitleId);

    @Query("select cuj from ComponentUserJob cuj where cuj.user.id = ?1 and cuj.active=true")
    public List<ComponentUserJob> getActiveComponentUserJobForUser(Long userId);

    @Query("select cuj from ComponentUserJob cuj where cuj.jobTitle = ?1")
    public List<ComponentUserJob> findByJobTitle(JobTitle jobTitle);

    public ComponentUserJob findById(Long id);

}   