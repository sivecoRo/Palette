package ro.siveco.ael.lcms.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ro.siveco.ael.lcms.domain.auth.model.UserCompetence;
import ro.siveco.ael.lcms.domain.taxonomies.model.Competence;
import ro.siveco.ael.lcms.domain.taxonomies.model.TaxonomyLevel;
import ro.siveco.ram.repository.BaseEntityRepository;

import java.util.List;

/**
 * Created by RoxanneS on 29/06/2017.
 */

@Repository
public interface UserCompetenceRepository extends BaseEntityRepository<UserCompetence> {

    @Query("select uc from UserCompetence uc where uc.id <> ?3 and uc.user.id  = ?1 and uc.competence.id=?2")
    UserCompetence findDuplicateConnexion(Long userId, Long competenceId, Long id);

    @Query("select uc from UserCompetence uc where uc.user.id = ?1")
    List<UserCompetence> findUserCompetencesByUser(Long userId);

    @Query("select u from UserCompetence u where (u.competence = ?1)")
    List<UserCompetence> findByCompetence(Competence competenceId);

    @Query("select u from UserCompetence u where u.taxonomyLevel = ?1")
    List<UserCompetence> findByTaxonomyLevel(TaxonomyLevel taxonomyLevel);
}