package ro.siveco.ael.lcms.repository;

import ro.siveco.ael.lcms.domain.metadata.model.Relation;
import ro.siveco.ram.repository.BaseEntityRepository;

import java.util.List;

/**
 * Created by IuliaP on 15.06.2017.
 */
public interface RelationRepository extends BaseEntityRepository<Relation> {

    List<Relation> findAllByName(String name);

}
