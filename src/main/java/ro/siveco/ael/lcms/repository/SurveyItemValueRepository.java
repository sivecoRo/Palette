package ro.siveco.ael.lcms.repository;

import org.springframework.stereotype.Repository;
import ro.siveco.ael.lcms.domain.materials.model.SurveyItem;
import ro.siveco.ael.lcms.domain.materials.model.SurveyItemValue;
import ro.siveco.ram.repository.BaseEntityRepository;

import java.util.List;

/**
 * User: AlexandruVi
 * Date: 2017-04-04
 */
@Repository
public interface SurveyItemValueRepository extends BaseEntityRepository<SurveyItemValue> {
    SurveyItemValue findOneById(Long id);

    List<SurveyItemValue> findBySurveyItemOrderByDefinitionAsc(SurveyItem surveyItem);

}   