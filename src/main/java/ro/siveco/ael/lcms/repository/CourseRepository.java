package ro.siveco.ael.lcms.repository;


import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ro.siveco.ael.lcms.domain.auth.model.AelUser;
import ro.siveco.ael.lcms.domain.course.model.Course;
import ro.siveco.ram.repository.BaseEntityRepository;

import java.util.Date;
import java.util.List;

/**
 * Created by LiviuI on 3/6/2017.
 */

@Repository
public interface CourseRepository extends BaseEntityRepository<Course> {

	List<Course> findByCreatorId(Long creatorId);

	@Query("select pcs.plannedCourse.course from PlannedCourseStudent pcs where pcs.user.id=?1")
	List<Course> getAsocCoursesForUser(Long userId);

	@Query("select count(id) from Course c where c.owner.id=?1")
	Integer getCourseNumberForOwner(Long ownerId);

	@Query("select pc.course.id " +
			"from PlannedCourse pc " +
			"where (pc.startDate <= ?1 and pc.endDate >= ?1) or " +
			"(pc.startDate <= ?2 and pc.endDate >= ?2) or " +
			"(pc.startDate >= ?1 and pc.startDate <= ?2 and pc.endDate >= ?1 and pc.endDate <= ?2) " +
			"group by pc.course.id")
	List<Long> getCourseIdsForPeriod(Date startDatePeriod, Date endDatePeriod);

	@Query("select pc.course.id " +
			"from PlannedCourse pc " +
			"where pc.endDate is null or pc.endDate > ?1 " +
			"group by pc.course.id")
	List<Long> getCourseIdsExpiresAfterDate(Date endDate);

	@Query("select pc.course " +
			"from PlannedCourse pc " +
			"where pc.course.creator.id = ?1 and pc.endDate is not null and pc.endDate <= ?2 " +
			"order by pc.startDate desc")
	List<Course> getCoursesExpiresBeforeDate(Long creatorId, Date endDate);

	@Query("select c from Course c where c.name = ?1 and c.creator = ?2 and c.id <> ?3")
	List<Course> findCoursesByNameAndCreator(String name, AelUser creator, Long courseId);
}
