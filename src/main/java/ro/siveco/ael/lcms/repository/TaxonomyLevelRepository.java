package ro.siveco.ael.lcms.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ro.siveco.ael.lcms.domain.taxonomies.model.TaxonomyLevel;
import ro.siveco.ael.lcms.domain.tenant.model.Tenant;
import ro.siveco.ram.repository.BaseEntityRepository;

import java.util.List;

/**
 * Created by JohnB on 22/06/2017.
 */

@Repository
public interface TaxonomyLevelRepository extends BaseEntityRepository<TaxonomyLevel> {
    @Query("select tl from TaxonomyLevel tl where (?1 is null and tl.tenant.id is null) or (?1 is not null and tl.tenant.id=?1)")
    List<TaxonomyLevel> findTaxonomyLevelsByTenant(Long tenantId);

    TaxonomyLevel findByNameAndTenant(String name, Tenant tenant);

    TaxonomyLevel findByOrderNumberAndTenant(Integer orderNumber, Tenant tenant);
}