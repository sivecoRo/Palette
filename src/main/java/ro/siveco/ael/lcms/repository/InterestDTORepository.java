package ro.siveco.ael.lcms.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.stereotype.Service;
import ro.siveco.ael.lcms.domain.profile.model.InterestDTO;
import ro.siveco.ael.web.mvc.MvcProperties;
import ro.siveco.ram.starter.repository.BaseDummyRepository;
import ro.siveco.ram.starter.service.BaseDummyService;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by albertb on 2/2/2018.
 */

@NoRepositoryBean
@Service
public class InterestDTORepository extends BaseDummyRepository<InterestDTO, String> {

    private final MvcProperties mvcProperties;

    @Autowired
    public InterestDTORepository(MvcProperties mvcProperties) {

        this.mvcProperties = mvcProperties;
    }

    @Override
    public Page<InterestDTO> findAll(Pageable pageable) {

        List<InterestDTO> languageDTOList = new ArrayList<>();
        for (Locale locale : mvcProperties.getAvailableLocales()) {
            InterestDTO interestDTO = new InterestDTO();
            interestDTO.setId(locale.getLanguage());
            interestDTO.setTag_id(locale.getDisplayLanguage());
            languageDTOList.add(interestDTO);
        }

        return new PageImpl<>(languageDTOList, pageable, languageDTOList.size());
    }

}
