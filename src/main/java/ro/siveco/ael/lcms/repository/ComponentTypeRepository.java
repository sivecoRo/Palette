package ro.siveco.ael.lcms.repository;

import org.springframework.stereotype.Repository;
import ro.siveco.ael.lcms.domain.organization.model.ComponentType;
import ro.siveco.ael.lcms.domain.tenant.model.Tenant;
import ro.siveco.ram.repository.BaseEntityRepository;

import java.util.List;

/**
 * Created by AndradaC on 5/29/2017.
 */
@Repository
public interface ComponentTypeRepository extends BaseEntityRepository<ComponentType>
{

    List<ComponentType> findAllByTenant(Tenant tenant);
}
