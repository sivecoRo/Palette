package ro.siveco.ael.lcms.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ro.siveco.ael.lcms.domain.preference.model.Preference;
import ro.siveco.ram.repository.BaseEntityRepository;

import java.util.List;


@Repository
public interface PreferencesRepository extends BaseEntityRepository<Preference> {

	@Query("select p from Preference p where p.user.id is null and p.tenant.id is null and p.global=false")
	List<Preference> getAllDefault();
	
	@Query("select p from Preference p where p.user.id=?1")
	List<Preference> getAllByUserId(Long userId);

	@Query("select p1 from Preference p1 where p1.name=?1")
	List<Preference> getByName(String prefName);

	@Query("select p1 from Preference p1 where p1.name=?1 and p1.tenant.id=?2")
	List<Preference> getByName( String prefName, Long tenantId );

	@Query("select p1 from Preference p1 where p1.name=?1 and p1.user.id=?2")
	public List<Preference> getByNameAndUser( String prefName, Long userId);

	@Query("select p from Preference p where p.name=?1 and p.global=true")
	public List<Preference> getGlobalPreferenceByName(String name);
}
