package ro.siveco.ael.lcms.repository;

import org.springframework.data.jpa.repository.Query;
import ro.siveco.ael.lcms.domain.metadata.model.Comment;
import ro.siveco.ram.repository.BaseEntityRepository;

import java.util.List;

/**
 * Created by albertb on 2/23/2018.
 */
public interface CommentRepository extends BaseEntityRepository<Comment> {
    @Query("select c from Comment c")
    List<Comment> findAll();

    @Query("select c from Comment c where c.plannedCourse.id=?1 order by c.commentDate")
    List<Comment> findByPlannedCourseId(Long plannedCourseId);

    @Query("select c from Comment c where c.aelUser.id=?1")
    List<Comment> findByUserId(Long userId);

    @Query("select c from Comment c where c.plannedCourse.course.id=?1 order by c.commentDate")
    List<Comment> findByCourseId(Long courseId);
}
