package ro.siveco.ael.lcms.repository;

import ro.siveco.ael.lcms.domain.auth.model.AelUser;
import ro.siveco.ael.lcms.domain.auth.model.ProfileVerification;
import ro.siveco.ram.repository.BaseEntityRepository;

public interface ProfileVerificationRepository extends BaseEntityRepository<ProfileVerification> {


    ProfileVerification findByToken(String token);
    ProfileVerification findByUser(AelUser user);
    Long deleteByUser(AelUser user);
}
