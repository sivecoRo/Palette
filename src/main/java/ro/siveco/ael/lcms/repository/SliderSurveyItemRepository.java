package ro.siveco.ael.lcms.repository;

import org.springframework.stereotype.Repository;
import ro.siveco.ael.lcms.domain.materials.model.SliderSurveyItem;
import ro.siveco.ael.lcms.domain.materials.model.SurveyItem;
import ro.siveco.ram.repository.BaseEntityRepository;

/**
 * Created by AndradaC on 5/2/2017.
 */
@Repository
public interface SliderSurveyItemRepository extends BaseEntityRepository<SliderSurveyItem> {
    SliderSurveyItem findOneById(Long id);

    SliderSurveyItem findOneBySurveyItem(SurveyItem surveyItem);
}
