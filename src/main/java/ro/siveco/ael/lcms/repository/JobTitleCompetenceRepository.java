package ro.siveco.ael.lcms.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ro.siveco.ael.lcms.domain.organization.model.JobTitleCompetence;
import ro.siveco.ael.lcms.domain.taxonomies.model.Competence;
import ro.siveco.ael.lcms.domain.taxonomies.model.TaxonomyLevel;
import ro.siveco.ram.repository.BaseEntityRepository;

import java.util.List;

/**
 * Created by JohnB on 21/06/2017.
 */

@Repository
public interface JobTitleCompetenceRepository extends BaseEntityRepository<JobTitleCompetence> {
    @Query("select jtc from JobTitleCompetence jtc where (?1 is null and jtc.jobTitle.tenant.id is null) or (?1 is not null and jtc.jobTitle.tenant.id=?1)")
    List<JobTitleCompetence> findJobTitleCompetencesByTenant(Long tenantId);

    @Query("select jtc from JobTitleCompetence jtc where jtc.id <> ?3 and jtc.jobTitle.id  = ?1 and jtc.competence.id=?2")
    JobTitleCompetence findDuplicateConnexion(Long jobTitleId, Long competenceId, Long id);

    @Query("select jtc from JobTitleCompetence jtc where (jtc.competence = ?1)")
    List<JobTitleCompetence> findByCompetence(Competence competence);

    @Query("select jtc from JobTitleCompetence jtc where taxonomyLevel = ?1")
    List<JobTitleCompetence> findByTaxonomyLevel(TaxonomyLevel taxonomyLevel);
}