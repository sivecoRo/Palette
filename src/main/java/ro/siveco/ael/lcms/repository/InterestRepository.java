package ro.siveco.ael.lcms.repository;

import org.springframework.stereotype.Repository;
import ro.siveco.ael.lcms.domain.metadata.model.Interest;
import ro.siveco.ram.repository.BaseEntityRepository;

import java.util.List;

/**
 * Created by albertb on 2/2/2018.
 */
@Repository
public interface InterestRepository  extends BaseEntityRepository<Interest> {
    List<Interest> findAll();

}
