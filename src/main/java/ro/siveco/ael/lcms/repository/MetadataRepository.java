package ro.siveco.ael.lcms.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ro.siveco.ael.lcms.domain.metadata.model.EntityType;
import ro.siveco.ael.lcms.domain.metadata.model.Metadata;
import ro.siveco.ael.lcms.domain.metadata.model.RelationType;
import ro.siveco.ael.lcms.domain.metadata.model.Tag;
import ro.siveco.ram.repository.BaseEntityRepository;

import java.util.List;

/**
 * Created by IuliaP on 12.06.2017.
 */
@Repository
public interface MetadataRepository extends BaseEntityRepository<Metadata> {
    List<Metadata> findByEntityIdAndEntityTypeAndRelationType(Long entityId, EntityType entityType, RelationType relationType);
    List<Metadata> findByTag(Tag tag);

    @Query("select distinct(m.entityId) from Metadata m where m.entityType=:entityType and m.relationType=:relationType and m.tag.id in (:tagIds)")
    List<Long> getEntityIdsForTags(@Param("entityType") EntityType entityType, @Param("relationType") RelationType relationType, @Param("tagIds") List<Long> tagIds);
}