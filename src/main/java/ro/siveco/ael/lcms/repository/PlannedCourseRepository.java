package ro.siveco.ael.lcms.repository;


import com.querydsl.core.types.Predicate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ro.siveco.ael.lcms.domain.course.model.PlannedCourse;
import ro.siveco.ael.lcms.domain.course.model.PlannedCourseStatus;
import ro.siveco.ram.repository.BaseEntityRepository;

import java.util.Date;
import java.util.List;

/**
 * Created by LiviuI on 3/6/2017.
 */

@Repository
public interface PlannedCourseRepository extends BaseEntityRepository<PlannedCourse>
{
    List<PlannedCourse> findByCourseId(Long courseId);

    List<PlannedCourse> findByStatus(PlannedCourseStatus status);

    List<PlannedCourse> findByCourseIdAndStatus(Long courseId, PlannedCourseStatus status);

    List<PlannedCourse> findByCreatorId(Long creatorId);

    List<PlannedCourse> findAll();

    @Query("select pc " +
            "from PlannedCourse pc, Metadata pcMetadata, Metadata userMetadata " +
            "where pc.course.owner is not null and pc.course.owner.id <> ?1 " +
            " and (pc.countryCode is not null and exists (select user from AelUser user, Country country where user.id = ?1 and user.country is not null and lower(user.country) = lower(country.countryName)  and lower(pc.countryCode) = lower(country.code)))" +
            " and (exists (select pcs from PlannedCourseStudent pcs where pcs.user.id = ?1 and pcs.plannedCourse.id = pc.id) " +
            " or (pcMetadata.entityId = pc.course.id and pcMetadata.entityType = 1 and pcMetadata.relationType = 0 " +
            " and userMetadata.entityId = ?1 and userMetadata.entityType = 2 and userMetadata.relationType = 0 " +
            " and (pcMetadata.tag.id = userMetadata.tag.id or exists (select rt from RelationTag rt where upper(rt.relation.name) = 'EQUALS' " +
            " and ( (rt.sourceTag.id = pcMetadata.tag.id and rt.destinationTag.id = userMetadata.tag.id) or (rt.destinationTag.id = pcMetadata.tag.id and rt.sourceTag.id = userMetadata.tag.id))))))" +
            " group by pc")
    List<PlannedCourse> findAllWithInterests(Long userId);

    @Query("select count(pc.id) " +
            "from PlannedCourse pc " +
            "where pc.course.id = ?1 ")
    public Integer getPlannedCourseNumberForCourse(Long courseId);

    @Query("select new PlannedCourse(pc.id, pc.course, pc.course.image, pc.startDate, pc.scheduleName, pc.location, pc.creator, pc.teacher, pc.status, pc.rating, pc.teachingMode, " +
            " count(*)) " +
            "from PlannedCourse pc " +
            "where pc.course.owner is not null and pc.course.owner.id <> ?1" +
            " and pc.startDate >= CURRENT_DATE" +
            " and (pc.countryCode is not null and exists (select user from AelUser user, Country country where user.id = ?1 and user.country is not null and lower(user.country) = lower(country.countryName)  and lower(pc.countryCode) = lower(country.code)))" +
            " and not exists (select pcs from PlannedCourseStudent pcs where pcs.user.id = ?1 and pcs.plannedCourse.id = pc.id and pcs.removeItem = true) " +
            " and not exists (select 1 from Metadata pcMetadata, Metadata userMetadata where pcMetadata.entityId = pc.course.id and pcMetadata.entityType = 1 and pcMetadata.relationType = 0 " +
                    " and userMetadata.entityId = ?1 and userMetadata.entityType = 2 and userMetadata.relationType = 0 " +
            " and (pcMetadata.tag.id = userMetadata.tag.id or exists (select rt from RelationTag rt where upper(rt.relation.name) = 'EQUALS' " +
            "           and ( (rt.sourceTag.id = pcMetadata.tag.id and rt.destinationTag.id = userMetadata.tag.id) or (rt.destinationTag.id = pcMetadata.tag.id and rt.sourceTag.id = userMetadata.tag.id)))))" +
            " group by pc.id, pc.course, pc.course.image, pc.startDate, pc.scheduleName, pc.location, pc.creator, pc.teacher, pc.status, pc.rating, pc.teachingMode")
    List<PlannedCourse> getRecommendedPlannedCourses(Long userId);

    /*@Query("select count(*) " +
            " from PlannedCourse pc, Metadata pcMetadata " +
            " where pcMetadata.entityId = pc.course.id and pcMetadata.entityType = 1 and pcMetadata.relationType = 0 " +
            " and pcMetadata.tag.id IN (:ids)"
    )
    Long getRecommendedPlannedCoursesCountForTags(List<Long> ids);*/

    @Query("select new PlannedCourse(pc.id, pc.course, pc.course.image, pc.startDate) " +
            "from PlannedCourseStudent pcs inner join pcs.plannedCourse pc " +
            "where pcs.user.id = ?1 and pcs.removeItem = false and pcs.addedToFavorites = true " +
            "and ((pc.endDate is null and pc.startDate <= ?2) or (pc.endDate is not null and pc.endDate <= ?2))" +
            " group by pc.id, pc.course, pc.course.image, pc.startDate")
    List<PlannedCourse> getPastPlannedCourses(Long userId, Date date);

    @Query("select pc from PlannedCourse pc" +
            " where not exists (select pcs from PlannedCourseStudent pcs where pcs.plannedCourse.id = pc.id and pcs.user.id = :userId and pcs.removeItem = true)")
    Page<PlannedCourse> findAllNotRemoved(@Param("userId") Long userId, Pageable pageable);

    @Query("select new PlannedCourse(pc.id, pc.course, pc.course.image, pc.startDate," +
            " (CAST(" +
                "6371 * acos(" +
                    " cos(radians(pc.creator.lat)) *" +
                    " cos(radians(pc.lat)) *" +
                    " cos(radians(pc.creator.lon) - radians(pc.lon)) +" +
                    " sin(radians(pc.creator.lat)) *" +
                    " sin(radians(pc.lat))" +
                ") as float)" +
            " ) as distance)" +
            " from PlannedCourse pc" +
            " where pc.id in (:ids) and not exists (select pcs from PlannedCourseStudent pcs where pcs.plannedCourse.id = pc.id and pcs.user.id = :userId and pcs.removeItem = true)" +
            " order by distance, pc.startDate")
    Page<PlannedCourse> findAllNotRemovedFiltered(@Param("userId") Long userId, @Param("ids") List<Long> ids, Pageable pageable);

    @Query("select count(*) " +
            " from PlannedCourse pc" +
            " where pc.id in (:ids) and not exists (select pcs from PlannedCourseStudent pcs where pcs.plannedCourse.id = pc.id and pcs.user.id = :userId and pcs.removeItem = true)")
    Long countAllNotRemovedFiltered(@Param("userId") Long userId, @Param("ids") List<Long> ids);

    @Query("select pc from PlannedCourse pc" +
            " where (pc.creator is not null and pc.creator.id = ?1) " +
            " or (pc.teacher is not null and pc.teacher.id = ?1) " +
            " or (pc.registrationUser is not null and pc.registrationUser.id = ?1) " +
            " or (pc.registrationApprovalUser is not null and pc.registrationApprovalUser.id = ?1)")
    Page<PlannedCourse> findAllByCurrentUser(Long userId, Pageable pageable);

    @Query("select pc from PlannedCourse pc" +
            " where (pc.creator is not null and pc.creator.id = ?1)")
    List<PlannedCourse> findAllByUserOrderByStartDate(Long userId);

    @Query("select new PlannedCourse(pc.id, pc.course, pc.course.image, pc.startDate) " +
            "from PlannedCourseStudent pcs inner join pcs.plannedCourse pc " +
            "where pcs.user.id = ?1 and pcs.attending = true " +
            "and pc.endDate is not null and pc.endDate <= ?2 " +
            "group by pc.id, pc.course, pc.course.image, pc.startDate " +
            "order by pc.startDate desc")
    List<PlannedCourse> getAttendedPlannedCourses(Long userId, Date endDate);

    @Query("select new PlannedCourse(pc.id, pc.course, pc.course.image, pc.startDate) " +
            "from PlannedCourseStudent pcs inner join pcs.plannedCourse pc " +
            "where pcs.user.id = ?1 and pcs.attending = true " +
            "and pc.endDate is not null and pc.endDate > ?2 " +
            "group by pc.id, pc.course, pc.course.image, pc.startDate " +
            "order by pc.startDate desc")
    List<PlannedCourse> getAttendingPlannedCourses(Long userId, Date endDate);

    @Query("select pc from PlannedCourse pc" +
            " where pc.endDate is not null and pc.endDate <= ?1" +
            " and (pc.reviewEmailSent is null or pc.reviewEmailSent = false)")
    List<PlannedCourse> getPlannedCoursesForPastReview(Date endDate);

    @Query("select pc.id " +
            "from PlannedCourse pc " +
            " where pc.locationCoordinates = ?1" +
            " and exists (select 1 from Metadata pcMetadata where pcMetadata.entityId = pc.course.id and pcMetadata.entityType = 1 and pcMetadata.relationType = 0 " +
            " and (pcMetadata.tag.id = ?2 or exists (select rt from RelationTag rt where upper(rt.relation.name) = 'EQUALS' " +
            "           and ( (rt.sourceTag.id = pcMetadata.tag.id and rt.destinationTag.id = ?2) or (rt.destinationTag.id = pcMetadata.tag.id and rt.sourceTag.id = ?2)))))" +
            " group by pc.id" +
            " order by pc.startDate desc")
    List<Long> getPlannedCoursesForLocationAndInterest(String coordinates, Long tagId);

    @Query("select pc.id " +
            "from PlannedCourse pc " +
            " where pc.countryCode = ?1" +
            " and exists (select 1 from Metadata pcMetadata where pcMetadata.entityId = pc.course.id and pcMetadata.entityType = 1 and pcMetadata.relationType = 0 " +
            " and (pcMetadata.tag.id = ?2 or exists (select rt from RelationTag rt where upper(rt.relation.name) = 'EQUALS' " +
            " and ( (rt.sourceTag.id = pcMetadata.tag.id and rt.destinationTag.id = ?2) or (rt.destinationTag.id = pcMetadata.tag.id and rt.sourceTag.id = ?2)))))" +
            " and (pc.endDate is null or pc.endDate > ?3)" +
            " group by pc.id" +
            " order by pc.startDate desc")
    List<Long> getPlannedCoursesForCountryAndInterest(String country, Long tagId, Date endDate);

    @Query("select count(pc.id) " +
            "from PlannedCourse pc " +
            " where pc.countryCode = ?1" +
            " and exists (select 1 from Metadata pcMetadata where pcMetadata.entityId = pc.course.id and pcMetadata.entityType = 1 and pcMetadata.relationType = 0 " +
            " and (pcMetadata.tag.id = ?2 or exists (select rt from RelationTag rt where upper(rt.relation.name) = 'EQUALS' " +
            " and ( (rt.sourceTag.id = pcMetadata.tag.id and rt.destinationTag.id = ?2) or (rt.destinationTag.id = pcMetadata.tag.id and rt.sourceTag.id = ?2)))))" +
            " and (pc.endDate is null or pc.endDate > ?3)")
    Long getPlannedCoursesCountForCountryAndInterest(String country, Long tagId, Date endDate);

    @Query("select count(pc.id)" +
            "from PlannedCourse pc " +
            " where pc.locationCoordinates is not null and (trim(pc.locationCoordinates)=?1 or ( length(pc.locationCoordinates) > 2 and trim(substring(pc.locationCoordinates,0,length(pc.locationCoordinates))) = trim(substring(?1,0,length(?1)))))" +
            " and exists (select 1 from Metadata pcMetadata where pcMetadata.entityId = pc.course.id and pcMetadata.entityType = 1 and pcMetadata.relationType = 0 " +
            " and (pcMetadata.tag.id = ?2 or exists (select rt from RelationTag rt where upper(rt.relation.name) = 'EQUALS' " +
            "           and ( (rt.sourceTag.id = pcMetadata.tag.id and rt.destinationTag.id = ?2) or (rt.destinationTag.id = pcMetadata.tag.id and rt.sourceTag.id = ?2)))))" +
            " and (pc.endDate is null or pc.endDate > ?3)")
    Long getPlannedCoursesCountForLocationAndInterest(String coordinates, Long tagId, Date endDate);

    @Query(value = "select tab2.id from " +
            " (select floor(random()* (max(rn)) + 1) maxrn" +
            " from " +
            " (select pc.id, row_number() over (order by pc.id) rn" +
            " from planned_courses pc" +
            " inner join courses c on c.id = pc.course_id" +
            " where pc.creator_id is not null and pc.creator_id <> :userId" +
            " and c.owner_id is not null and c.owner_id <> :userId" +
            " and (pc.end_date is null or pc.end_date > :endDate)" +
            " and pc.country_code is not null and pc.country_code = :countryCode" +
            " and c.type = 2" +
            " and exists (select 1 from metadata m where m.entity_type = 1 and m.relation_type = 0 and m.tag_id in (:tagIds) and m.entity_id = c.id)" +
            " and not exists (select 1 from planned_course_students pcs where pcs.planned_course_id = pc.id and pcs.user_id = :userId and pcs.remove_item = true)) tab) tab1" +
            "  inner join" +
            " (select pc.id, row_number() over (order by pc.id) rn" +
            " from planned_courses pc" +
            " inner join courses c on c.id = pc.course_id" +
            " where pc.creator_id is not null and pc.creator_id <> :userId" +
            " and c.owner_id is not null and c.owner_id <> :userId" +
            " and (pc.end_date is null or pc.end_date > :endDate)" +
            " and pc.country_code is not null and pc.country_code = :countryCode" +
            " and c.type = 2" +
            " and exists (select 1 from metadata m where m.entity_type = 1 and m.relation_type = 0 and m.tag_id in (:tagIds) and m.entity_id = c.id)" +
            " and not exists (select 1 from planned_course_students pcs where pcs.planned_course_id = pc.id and pcs.user_id = :userId and pcs.remove_item = true)) tab2 on tab2.rn = tab1.maxrn", nativeQuery = true)
    Long getRandomService(@Param("userId") Long userId, @Param("endDate") Date endDate, @Param("countryCode") String countryCode, @Param("tagIds") List<Long> tagIds);

    @Query(value = " select id from (" +
            " select pc.id, row_number() over (order by CAST(6371 * acos(cos(radians(u.lat)) *cos(radians(pc.lat)) *cos(radians(u.lon) - radians(pc.lon)) +sin(radians(u.lat)) * sin(radians(pc.lat))) as float)) rn" +
            " from planned_courses pc" +
            " inner join courses c on c.id = pc.course_id" +
            " inner join users u on u.id = pc.creator_id" +
            " where pc.creator_id is not null and pc.creator_id <> :userId" +
            " and c.owner_id is not null and c.owner_id <> :userId" +
            " and (pc.end_date is null or pc.end_date > :endDate)" +
            " and pc.country_code is not null and pc.country_code = :countryCode" +
            " and c.type = 0" +
            " and exists (select 1 from metadata m where m.entity_type = 1 and m.relation_type = 0 and m.tag_id in (:tagIds) and m.entity_id = c.id)" +
            " and not exists (select 1 from planned_course_students pcs where pcs.planned_course_id = pc.id and pcs.user_id = :userId and pcs.remove_item = true)" +
            ") tab where rn = 1", nativeQuery = true)
    Long getClosestCourse(@Param("userId") Long userId, @Param("endDate") Date endDate, @Param("countryCode") String countryCode, @Param("tagIds") List<Long> tagIds);

    @Query(value = "select id from " +
            "(select tab2.id, row_number() over(order by tab2.favorites desc) rn from" +
            " (select max(attending) maxa, max(favorites) maxf from (" +
            " select pc.id, sum(case when pcs.attending = true then 1 else 0 end) attending, sum(case when pcs.added_to_favorites = true then 1 else 0 end) favorites" +
            " from planned_courses pc" +
            " inner join courses c on c.id = pc.course_id" +
            " inner join planned_course_students pcs on pcs.planned_course_id = pc.id" +
            " where pc.creator_id is not null and pc.creator_id <> :userId" +
            " and c.owner_id is not null and c.owner_id <> :userId" +
            " and (pc.end_date is null or pc.end_date > :endDate)" +
            " and pc.country_code is not null and pc.country_code = :countryCode" +
            " and c.type = 3" +
            " and exists (select 1 from metadata m where m.entity_type = 1 and m.relation_type = 0 and m.tag_id in (:tagIds) and m.entity_id = c.id)" +
            " and not exists (select 1 from planned_course_students pcs where pcs.planned_course_id = pc.id and pcs.user_id = :userId and pcs.remove_item = true)" +
            " group by pc.id" +
            ") tab ) tab1 " +
            " inner join" +
            "(select pc.id, sum(case when pcs.attending = true then 1 else 0 end) attending, sum(case when pcs.added_to_favorites = true then 1 else 0 end) favorites" +
            " from planned_courses pc" +
            " inner join courses c on c.id = pc.course_id" +
            " inner join planned_course_students pcs on pcs.planned_course_id = pc.id" +
            " where pc.creator_id is not null and pc.creator_id <> :userId" +
            " and c.owner_id is not null and c.owner_id <> :userId" +
            " and (pc.end_date is null or pc.end_date > :endDate)" +
            " and pc.country_code is not null and pc.country_code = :countryCode" +
            " and c.type = 3" +
            " and exists (select 1 from metadata m where m.entity_type = 1 and m.relation_type = 0 and m.tag_id in (:tagIds) and m.entity_id = c.id)" +
            " and not exists (select 1 from planned_course_students pcs where pcs.planned_course_id = pc.id and pcs.user_id = :userId and pcs.remove_item = true)" +
            " group by pc.id" +
            ") tab2 on tab2.attending = tab1.maxa or (tab1.maxa = 0 and tab2.favorites = tab1.maxf)" +
            ") tab3 where rn = 1", nativeQuery = true)
    Long getMostPopularMeeting(@Param("userId") Long userId, @Param("endDate") Date endDate, @Param("countryCode") String countryCode, @Param("tagIds") List<Long> tagIds);

    @Query(value = "select id from (" +
            " select pc.id,c.creation_date, row_number() over (order by c.creation_date desc) rn" +
            " from planned_courses pc" +
            " inner join courses c on c.id = pc.course_id" +
            " where pc.creator_id is not null and pc.creator_id <> :userId" +
            " and c.owner_id is not null and c.owner_id <> :userId" +
            " and (pc.end_date is null or pc.end_date > :endDate)" +
            " and pc.country_code is not null and pc.country_code = :countryCode" +
            " and c.type = 1" +
            " and exists (select 1 from metadata m where m.entity_type = 1 and m.relation_type = 0 and m.tag_id in (:tagIds) and m.entity_id = c.id)" +
            " and not exists (select 1 from planned_course_students pcs where pcs.planned_course_id = pc.id and pcs.user_id = :userId and pcs.remove_item = true)" +
            ") tab where rn = 1", nativeQuery = true)
    Long getMostRecentActivity(@Param("userId") Long userId, @Param("endDate") Date endDate, @Param("countryCode") String countryCode, @Param("tagIds") List<Long> tagIds);
}

