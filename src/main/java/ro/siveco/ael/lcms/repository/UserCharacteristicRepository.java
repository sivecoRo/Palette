package ro.siveco.ael.lcms.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ro.siveco.ael.lcms.domain.metadata.model.EntityType;
import ro.siveco.ael.lcms.domain.metadata.model.Metadata;
import ro.siveco.ael.lcms.domain.profile.model.*;
import ro.siveco.ram.repository.BaseEntityRepository;

import java.util.List;

/**
 * User: CatalinS
 * Date: 2017-06-19
 */
@Repository
public interface UserCharacteristicRepository extends BaseEntityRepository<UserCharacteristic> {
	
	@Query("select uct from UserCharacteristicTab uct where uct.active=true order by uct.id")
	List<UserCharacteristicTab> getActiveTabs();
	
	@Query("select uct from UserCharacteristicTab uct order by uct.id")
	List<UserCharacteristicTab> getAllTabs();

	@Query("select uc from UserCharacteristic uc where uc.tab.id=?1 order by uc.id")
	List<UserCharacteristic> getCharacteristicsByTab(Long tabId);

	@Query("select uc from UserCharacteristic uc where uc.active=true and uc.tab.id=?1 order by uc.id")
	List<UserCharacteristic> getActiveCharacteristicsByTab(Long tabId);
	
	@Query("select uc from UserCharacteristic uc order by uc.id")
	List<UserCharacteristic> getCharacteristics();

	@Query("select m from Metadata m where m.entityId=?1 and m.entityType=?2 and m.tag.language.name=?3")
	List<Metadata> getMetadatasByCharacteristicIdAndLanguage(Long id, EntityType type, String lang);

	@Query("select cv from CharacteristicValue cv where cv.characteristic.id=?1 and cv.language.name=?2")
	List<CharacteristicValue> getCharacteristicByIdAndLanguage(Long id, String language);

	@Query("select cv from CharacteristicValue cv where cv.characteristic.id=?1")
	List<CharacteristicValue> getCharacteristicById(Long id);

	@Query("select c from Characteristic c where c.type=?1")
	List<Characteristic> getCharacteristicsByType(CharacteristicType type);
}
