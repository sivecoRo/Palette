package ro.siveco.ael.lcms.repository;


import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ro.siveco.ael.lcms.domain.auth.model.AelRole;
import ro.siveco.ael.lcms.domain.auth.model.Group;
import ro.siveco.ael.lcms.domain.tenant.model.Tenant;
import ro.siveco.ram.repository.BaseEntityRepository;

import java.util.ArrayList;
import java.util.List;

@Repository
public interface AelRoleRepository extends BaseEntityRepository<AelRole> {
    AelRole findByNameAndTenant(String name, Tenant tenant);

    AelRole findById(Long id);

    List<AelRole> findByName(String name);

    List<AelRole> findAllByTenant(Tenant tenant);

    @Query("select af from AelRole af where af.id in ?1")
    List<AelRole> findByidRoles(List<Long> id);

    @Query("select af from AelRole af where af.tenant is null")
    List<AelRole> findAll();
}
