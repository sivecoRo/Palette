package ro.siveco.ael.lcms.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ro.siveco.ael.lcms.domain.organization.model.ComponentUserJobPath;
import ro.siveco.ael.lcms.domain.organization.model.ComponentUserJobStatus;
import ro.siveco.ram.repository.BaseEntityRepository;

import java.util.List;

@Repository
public interface ComponentUserJobPathRepository extends BaseEntityRepository<ComponentUserJobPath> {

    @Query("select cujp from ComponentUserJobPath cujp where (cujp.id = ?1)")
    public ComponentUserJobPath findCujPathById(Long cujPathEntryId);

    @Query("select cujp from ComponentUserJobPath cujp where cujp.status = ?2 and cujp.componentUserJob.user.id = ?1")
    public ComponentUserJobPath getCurrentComponentUserJobPathForUser(Long userId, ComponentUserJobStatus currentStatus);

    @Query("select cujp from ComponentUserJobPath cujp where cujp.componentUserJob.user.id = ?1 order by insertedAtDate desc")
    List<ComponentUserJobPath> getAllCujPathDataForUser(Long userId);

    @Query("select cujp from ComponentUserJobPath cujp where cujp.componentUserJob.id = ?1 and cujp.status = ?2 and cujp.removedAtDate is null")
    public ComponentUserJobPath findNotDeletedFutureEntryForComponentUserJob(Long componentUserJobId, ComponentUserJobStatus futureStatus);

    @Query("select cujp from ComponentUserJobPath cujp where cujp.componentUserJob.user.id = ?1 and status = ?2 order by insertedAtDate desc")
    List<ComponentUserJobPath> getCurrentCujPathDataForUser(Long userId, ComponentUserJobStatus status);

    @Query("select cujp from ComponentUserJobPath cujp where cujp.componentUserJob.user.id = ?1 and status = ?2 order by insertedAtDate desc")
    List<ComponentUserJobPath> getFutureCujPathDataForUser(Long userId, ComponentUserJobStatus status);
}