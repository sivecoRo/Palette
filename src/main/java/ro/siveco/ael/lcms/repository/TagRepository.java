package ro.siveco.ael.lcms.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ro.siveco.ael.lcms.domain.metadata.model.EntityType;
import ro.siveco.ael.lcms.domain.metadata.model.Language;
import ro.siveco.ael.lcms.domain.metadata.model.RelationType;
import ro.siveco.ael.lcms.domain.metadata.model.Tag;
import ro.siveco.ram.repository.BaseEntityRepository;

import java.util.List;

/**
 * Created by IuliaP on 09.06.2017.
 */
@Repository
public interface TagRepository extends BaseEntityRepository<Tag> {
    List<Tag> findAllByValue(String value);
    List<Tag> findAllByLanguage(Language language);
    List<Tag> findAllByValueAndLanguage(String value, Language language);

    @Query("select t from Tag t where not exists (select 1 from Metadata m where m.tag = t and m.entityId = ?1 and m.entityType=?2)")
    List<Tag> findAllWithoutMetadata(Long entityId, EntityType entityType);

    @Query("select t from Interest i join i.tag t where not exists (select 1 from Metadata m where m.tag = t and m.entityId = ?1 and m.entityType=?2)")
    List<Tag> findAllWithoutMetadataWithInterest(Long entityId, EntityType entityType);

    @Query("select mt.tag from Metadata mt where mt.entityId= ?1 and mt.entityType = ?2 and mt.relationType = ?3")
    List<Tag> getAllTagsByEntityIdTypeAndRelation(Long entityId, EntityType entityType, RelationType relationType);

    @Query("select rt.destinationTag from RelationTag rt where rt.sourceTag.id = ?1 and rt.destinationTag.language.id = ?3 " +
            " and rt.relation.id = ?2")
    List<Tag> findAllByTagAndRelationAndLanguage(Long tagId, Long relationId, Long languageId);
}
