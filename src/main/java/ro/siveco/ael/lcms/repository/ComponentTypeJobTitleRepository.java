package ro.siveco.ael.lcms.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ro.siveco.ael.lcms.domain.organization.model.ComponentTypeJobTitle;
import ro.siveco.ael.lcms.domain.organization.model.JobTitle;
import ro.siveco.ram.repository.BaseEntityRepository;

import java.util.List;

/**
 * Created by AndradaC on 5/29/2017.
 */
@Repository
public interface ComponentTypeJobTitleRepository extends BaseEntityRepository<ComponentTypeJobTitle>
{

    @Query("select cj.jobTitle from ComponentTypeJobTitle cj where cj.componentType.id=?1")
    List<JobTitle> getJobTitlesByComponentTypeId( Long componentTypeId );
}
