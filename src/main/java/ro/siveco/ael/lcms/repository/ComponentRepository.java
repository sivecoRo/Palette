package ro.siveco.ael.lcms.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ro.siveco.ael.lcms.domain.organization.model.Component;
import ro.siveco.ael.lcms.domain.organization.model.TreeNode;
import ro.siveco.ael.lcms.domain.tenant.model.Tenant;
import ro.siveco.ram.repository.BaseEntityRepository;

import java.util.List;

/**
 * Created by LiviuI on 4/4/2017.
 */
@Repository
public interface ComponentRepository extends BaseEntityRepository<Component> {

    @Query("SELECT new ro.siveco.ael.lcms.domain.organization.model.TreeNode( c.id, c.name, 0 ) FROM Component c where c.parent.id=?1")
    public List<TreeNode> getComponentChildTreeNodes(Long componentId);

    @Query("select c from Component c where c.parent is null")
    Component getRootComponent();

    @Query("select c from Component c where c.parent.parent is null and c.tenant=?1")
    Component getTenantRootComponent(Tenant tenant);

    @Query("select c from Component c where c.tenant.id=?1")
    public Component getByTenantId(Long tenantId);

    public Component findByNameAndTenant(String name, Tenant tenant);
}
