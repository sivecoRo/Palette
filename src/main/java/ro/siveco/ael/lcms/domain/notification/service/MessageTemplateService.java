package ro.siveco.ael.lcms.domain.notification.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.siveco.ael.lcms.domain.notification.model.MessageTemplate;
import ro.siveco.ael.lcms.repository.MessageTemplateRepository;
import ro.siveco.ram.starter.service.BaseEntityService;

import java.util.List;

/**
 * Created by IuliaP on 27.09.2017.
 */
@Service
public class MessageTemplateService extends BaseEntityService<MessageTemplate> {

    @Autowired
    public MessageTemplateService(MessageTemplateRepository messageTemplateRepository) {

        super(MessageTemplate.class, messageTemplateRepository);
    }

    public List<MessageTemplate> findByName(String name) {

        return ((MessageTemplateRepository) getResourceRepository()).findByName(name);
    }

    public List<MessageTemplate> findByNameAndLocale(String name, String locale) {

        return ((MessageTemplateRepository) getResourceRepository()).findByNameAndLocale(name, locale);
    }

    public List<MessageTemplate> findByNameAndLocaleAndTenantId(String name, String locale, Long tenantId) {

        return ((MessageTemplateRepository) getResourceRepository())
                .findByNameAndLocaleAndTenantId(name, locale, tenantId);
    }
}
