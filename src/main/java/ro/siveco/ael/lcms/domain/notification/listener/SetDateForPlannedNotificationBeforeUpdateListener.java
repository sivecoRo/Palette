package ro.siveco.ael.lcms.domain.notification.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.notification.model.PlannedNotification;
import ro.siveco.ael.lcms.domain.notification.service.PlannedNotificationService;
import ro.siveco.ram.event.listener.type.single.BaseBeforeUpdateListener;
import ro.siveco.ram.event.type.single.BeforeUpdateEvent;

/**
 * Created by IuliaP on 12.10.2017.
 */
@Order(value = 1)
@Component
public class SetDateForPlannedNotificationBeforeUpdateListener extends BaseBeforeUpdateListener<PlannedNotification> {

    @Autowired
    PlannedNotificationService plannedNotificationService;

    @Override
    public void onApplicationEvent(BeforeUpdateEvent<PlannedNotification> event) {
        PlannedNotification plannedNotification = event.getDomainModel();
        plannedNotificationService.validateForm( plannedNotification );
        plannedNotificationService.setDate( plannedNotification );
    }
}

