package ro.siveco.ael.lcms.domain.profile.model;

public class GenderSelectModel {
	public Gender code;
	public String value;
	
	public GenderSelectModel(Gender code, String value) {
		this.code = code;
		this.value = value;
	}
}
