package ro.siveco.ael.lcms.domain.metadata.model;

import org.springframework.data.domain.Sort;
import ro.siveco.ram.starter.ui.config.PropertyViewDescriptor;
import ro.siveco.ram.starter.ui.config.Sorter;
import ro.siveco.ram.starter.ui.config.ViewDescriptor;
import ro.siveco.ram.starter.ui.model.IndexViewType;

import javax.persistence.*;

/**
 * Created by IuliaP on 15.06.2017.
 */
@Entity
@Table(name = "relations_tags",
        uniqueConstraints = { @UniqueConstraint( columnNames = { "source_tag_id", "destination_tag_id", "relation_id" } ) } )
@SequenceGenerator(name = "relations_tags_seq", sequenceName = "relations_tags_seq")
@ViewDescriptor(pageSize = 10, viewTypes = {IndexViewType.GRID}, searchEnabled = true,
        defaultSearchProperty = "sourceTag.value",
        sort = {@Sorter(propertyName = "sourceTag.value", direction = Sort.Direction.ASC),
                @Sorter(propertyName = "destinationTag.value", direction = Sort.Direction.ASC),
                @Sorter(propertyName = "relation.name", direction = Sort.Direction.ASC)})
public class RelationTag extends ro.siveco.ram.starter.model.base.BaseEntity {

    private Tag sourceTag;
    private Tag destinationTag;
    private Relation relation;

    @Id
    @GeneratedValue(generator = "relations_tags_seq", strategy = GenerationType.AUTO)
    public Long getId() {
        return super.getId();
    }

    @ManyToOne
    @JoinColumn(name = "source_tag_id", nullable = false)
    @PropertyViewDescriptor(order=1)
    public Tag getSourceTag() {
        return sourceTag;
    }

    @ManyToOne
    @JoinColumn(name = "destination_tag_id")
    @PropertyViewDescriptor(order=3)
    public Tag getDestinationTag() {
        return destinationTag;
    }

    @ManyToOne
    @JoinColumn(name = "relation_id")
    @PropertyViewDescriptor(order=2)
    public Relation getRelation() {
        return relation;
    }

    public void setSourceTag(Tag sourceTag) {
        this.sourceTag = sourceTag;
    }

    public void setDestinationTag(Tag destinationTag) {
        this.destinationTag = destinationTag;
    }

    public void setRelation(Relation relation) {
        this.relation = relation;
    }
}
