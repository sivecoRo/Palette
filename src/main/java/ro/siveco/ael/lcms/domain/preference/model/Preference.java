package ro.siveco.ael.lcms.domain.preference.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import ro.siveco.ael.domain.annotations.ColumnComment;
import ro.siveco.ael.domain.annotations.TableComment;
import ro.siveco.ael.domain.annotations.Tenantable;
import ro.siveco.ael.lcms.domain.auth.model.AelUser;
import ro.siveco.ael.lcms.domain.tenant.model.Tenant;

import javax.persistence.*;

/**
 * Created by IntelliJ IDEA.
 * User: AlexandruM
 * Date: 03.04.2007
 * Time: 13:15:15
 */
@Entity
@Table( name = "preferences" )
@SequenceGenerator( name="preferences_seq", sequenceName = "preferences_seq")
@TableComment(comment = "Stores the application preferences")
@Tenantable
public class Preference extends ro.siveco.ram.starter.model.base.BaseEntity
{
	private Long id;

	@ColumnComment(comment = "The user's id (if this is a user preference)")
	private AelUser user;

	@ColumnComment(comment = "The preference's display name")
	private String name;

	@ColumnComment(comment = "The preference's value")
	private String value;

	@ColumnComment(comment = "Whether this is an application preference")
	private boolean global;

    private Tenant tenant;

	public Preference( Long id, String name, String value, boolean global )
	{
		this.id = id;
		this.name = name;
		this.value = value;
		this.global = global;
	}

	public Preference( String name, AelUser user, String value, boolean global )
	{
		this.name = name;
		this.user = user;
		this.value = value;
		this.global = global;
	}

	//default constructor
	public Preference()
	{
		
	}

	@Id
	@GeneratedValue( generator = "preferences_seq", strategy = GenerationType.AUTO )
	public Long getId()
	{
		return id;
	}

	public void setId( Long id )
	{
		this.id = id;
	}

	@ManyToOne
	@JoinColumn( name = "user_id" )
	public AelUser getUser()
	{
		return user;
	}

	public void setUser( AelUser _user )
	{
		this.user = _user;
	}

	@Column( name = "name", nullable = false, length = 500 )
	public String getName()
	{
		return name;
	}

	public void setName( String _name )
	{
		this.name = _name;
	}

	@Column( name = "value", nullable = false, length = 500 )
	public String getValue()
	{
		return value;
	}

	public void setValue( String _value )
	{
		this.value = _value;
	}

	@Column ( name="global" )
	public boolean getGlobal()
	{
		return global;
	}

	public void setGlobal( boolean _global )
	{
		this.global = _global;
	}

    @ManyToOne
    @JoinColumn( name = "tenant_id", nullable = true )
    public Tenant getTenant() {
        return tenant;
    }

    public void setTenant(Tenant tenant) {
        this.tenant = tenant;
    }

	public String toString()
	{
		return new ToStringBuilder( this,
											  ToStringStyle.MULTI_LINE_STYLE )
                .append( this.getId() )
				.append( this.getUser() )
				.append( this.getName() )
				.append( this.getValue() )
				.append( this.getGlobal() )
                .toString();
	}

	public boolean equals( Object other )
	{
		if( ( this == other ) )
		{
			return true;
		}
		if( !( other instanceof Preference ) )
		{
			return false;
		}
		Preference castOther = ( Preference ) other;
		return new EqualsBuilder()
				.append( this.getId(), castOther.getId() )
				.append( this.getUser(), castOther.getUser() )
				.append( this.getName(), castOther.getName() )
				.append( this.getValue(), castOther.getValue() )
				.append( this.getGlobal(), castOther.getGlobal() )
				.isEquals();
	}

	public int hashCode()
	{
		return new HashCodeBuilder()
				.append( getId() )
				.append( getUser() )
				.append( getName() )
				.append( getValue() )
				.append( getGlobal() )
				.toHashCode();
	}
}
