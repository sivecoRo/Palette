package ro.siveco.ael.lcms.domain.profile.model;

import ro.siveco.ram.model.Named;

/**
 * User: AlexandruVi
 * Date: 2017-01-09
 */
public interface ProfilePropertyName extends Named {

    /**
     * @return The profile category
     */
    public ProfileCategory getCategory();

}
