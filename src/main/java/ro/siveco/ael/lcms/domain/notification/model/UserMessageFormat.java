package ro.siveco.ael.lcms.domain.notification.model;

import ro.siveco.ram.starter.model.base.BaseEntity;
import ro.siveco.ram.starter.ui.config.PropertyViewDescriptor;
import ro.siveco.ram.starter.ui.config.ViewDescriptor;
import ro.siveco.ram.starter.ui.model.IndexViewType;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by IuliaP on 28.09.2017.
 */
@Entity
@Table(name = "user_message_formats",
        uniqueConstraints = {@UniqueConstraint(columnNames = {"code"}), @UniqueConstraint(columnNames = {"name"})})
@SequenceGenerator(name = "user_message_formats_seq", sequenceName = "user_message_formats_seq")
@ViewDescriptor(pageSize = 10, viewTypes = {IndexViewType.CARD}, defaultSearchProperty = "name")
public class UserMessageFormat extends BaseEntity {

    private String code;

    private String name;

    private Boolean application;

    @Id
    @GeneratedValue(generator = "user_message_formats_seq", strategy = GenerationType.SEQUENCE)
    public Long getId() {

        return super.getId();
    }

    @Column(length = 255)
    @NotNull
    @PropertyViewDescriptor(order = 1)
    public String getCode() {

        return code;
    }

    public void setCode(String code) {

        this.code = code;
    }

    @Column(length = 255)
    @NotNull
    @PropertyViewDescriptor(order = 1)
    public String getName() {

        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

    @Column
    @NotNull
    @PropertyViewDescriptor(hidden = true)
    public Boolean getApplication() {

        return application;
    }

    public void setApplication(Boolean application) {

        this.application = application;
    }

    @Override
    public String toString() {

        return getName();
    }
}
