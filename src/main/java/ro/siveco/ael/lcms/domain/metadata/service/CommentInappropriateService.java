package ro.siveco.ael.lcms.domain.metadata.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.siveco.ael.lcms.domain.metadata.model.CommentInappropriate;
import ro.siveco.ael.lcms.repository.CommentInappropriateRepository;
import ro.siveco.ram.starter.service.BaseEntityService;

/**
 * Created by primozk on 3/20/2019.
 */

@Service
public class CommentInappropriateService extends BaseEntityService<CommentInappropriate> {
    @Autowired
    public CommentInappropriateService(CommentInappropriateRepository commentInappropriateRepository) {
        super(CommentInappropriate.class, commentInappropriateRepository);
    }

    public void saveCommentInappropriate(CommentInappropriate comment){
        save(comment);
    }
}
