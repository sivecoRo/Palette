package ro.siveco.ael.lcms.domain.profile.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import ro.siveco.ael.lcms.domain.profile.model.CountryDTO;
import ro.siveco.ael.lcms.domain.profile.service.CountryDTOService;
import ro.siveco.ram.controller.annotation.AjaxStructuralMapping;
import ro.siveco.ram.controller.util.MappingUri;
import ro.siveco.ram.starter.controller.BaseDummyController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Controller
@RequestMapping("/country")
public class CountryDTOController extends BaseDummyController<CountryDTO, String> {

    @Autowired
    public CountryDTOController(CountryDTOService service) {

        super(service);
    }

    @Override
    @AjaxStructuralMapping(path = MappingUri.INDEX_URI)
    public ResponseEntity<Page<?>> paginate(ModelMap model, Pageable pageable) {

        List<CountryDTO> countries = new ArrayList<CountryDTO>();
        String[] countryCodes = Locale.getISOCountries();

        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes())
                .getRequest();

        String searchField = request.getParameter(MappingUri.SEARCH_PARAM);
        String searchVal = searchField != null && !"".equals(searchField) ?
                request.getParameter(MappingUri.SEARCH_PARAM).split("_~_")[1] : null;

        for (String countryCode : countryCodes) {
            Locale obj = new Locale("", countryCode);

            if (searchVal == null ||
                    (searchVal != null && obj.getDisplayCountry().toLowerCase().contains(searchVal.toLowerCase()))) {
                CountryDTO countryDTO = new CountryDTO();
                countryDTO.setName(obj.getDisplayCountry());
                countries.add(countryDTO);
            }
        }

        Page<CountryDTO> page = new PageImpl<CountryDTO>(countries);
        return new ResponseEntity<>(page, HttpStatus.OK);
    }
}
