package ro.siveco.ael.lcms.domain.course.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.course.model.PlannedCourse;
import ro.siveco.ael.lcms.domain.course.service.PlannedCourseService;
import ro.siveco.ram.event.listener.type.single.BaseBeforeUpdateListener;
import ro.siveco.ram.event.type.single.BeforeUpdateEvent;

@Component
public class SetInformationForPlannedCourseBeforeUpdateListener extends BaseBeforeUpdateListener<PlannedCourse> {

    @Autowired
    private PlannedCourseService plannedCourseService;

    @Override
    public void onApplicationEvent(BeforeUpdateEvent<PlannedCourse> event) {

        PlannedCourse plannedCourse = event.getDomainModel();
        PlannedCourse dbPlannedCourse = event.getStoredDomainModel();
        Boolean isValid = plannedCourseService.validateUpdatePlannedCourseDates(plannedCourse, dbPlannedCourse);
        if(isValid)
        {
            if(!plannedCourse.getSelfRegistration())
            {
                if(plannedCourse.getRegistrationUser()==null)
                {
                    plannedCourse.setRegistrationUser(dbPlannedCourse.getRegistrationUser());
                }

                if(plannedCourse.getRegistrationApprovalUser() == null)
                {
                    plannedCourse.setRegistrationApprovalUser(dbPlannedCourse.getRegistrationApprovalUser());
                }
            }
            else
            {
                if(plannedCourse.getSignupStartDate()==null)
                {
                    plannedCourse.setSignupStartDate(dbPlannedCourse.getSignupStartDate());
                }

                if(plannedCourse.getSignupEndDate() == null)
                {
                    plannedCourse.setSignupEndDate(dbPlannedCourse.getSignupEndDate());
                }
            }
        }
    }
}
