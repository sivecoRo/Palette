package ro.siveco.ael.lcms.domain.course.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.course.model.Course;
import ro.siveco.ael.lcms.domain.course.model.PlannedCourse;
import ro.siveco.ael.lcms.domain.course.service.PlannedCourseService;
import ro.siveco.ram.event.listener.type.single.BaseAfterCreateListener;
import ro.siveco.ram.event.type.single.AfterCreateEvent;

@Component
public class CreatePlannedCourseForCourseAfterCreateListener extends BaseAfterCreateListener<Course> {

    @Autowired
    private PlannedCourseService plannedCourseService;


    @Override
    public void onApplicationEvent(AfterCreateEvent<Course> event) {
        Course course = event.getDomainModel();
        PlannedCourse plannedCourse = null;
       plannedCourseService.createPlannedCourseForCourse(course, plannedCourse);
    }

}
