package ro.siveco.ael.lcms.domain.auth.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.auth.model.AelFunction;
import ro.siveco.ael.lcms.domain.auth.model.AelRole;
import ro.siveco.ael.lcms.domain.auth.model.MultiSelectDTO;
import ro.siveco.ael.lcms.domain.auth.service.AelRoleService;
import ro.siveco.ram.event.listener.type.single.BaseAfterReadListener;
import ro.siveco.ram.event.type.single.AfterReadEvent;

import java.util.List;

/**
 * Created by AndradaC on 9/1/2017.
 */
@Component
public class SetMultiselectRoleListForFunctionAfterReadListener extends BaseAfterReadListener<AelFunction>{

    @Autowired
    private AelRoleService aelRoleService;

    @Override
    public void onApplicationEvent(AfterReadEvent<AelFunction> event) {
        AelFunction aelFunction = event.getDomainModel();

        List<AelRole> roleList = aelRoleService.findAll();
        List<MultiSelectDTO> multiSelectDTOs = aelRoleService.getMultiSelectDTOS(roleList);

        aelFunction.setListAllRole(multiSelectDTOs);
    }
}
