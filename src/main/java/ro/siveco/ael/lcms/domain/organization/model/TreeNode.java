package ro.siveco.ael.lcms.domain.organization.model;

/**
 * Created by LiviuI on 4/4/2017.
 */
public class TreeNode {

    public TreeNode(Long id, String text, Integer type) {
        this.id = (type == 0 ? "C":"U") + id;
        this.entityID = id;
        this.text = text;
        this.type = type;
        this.children = type == 0;
    }

    private String id;

    private Long entityID;

    private String text;

    private Integer type;

    private Boolean children;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getEntityID() {
        return entityID;
    }

    public void setEntityID(Long entityID) {
        this.entityID = entityID;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Boolean getChildren() {
        return children;
    };

    public void setChildren(Boolean children) {
        this.children = children;
    }
}
