package ro.siveco.ael.lcms.domain.profile.service;

import org.apache.commons.lang3.RandomStringUtils;
import org.joda.time.LocalDate;
import org.joda.time.Years;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;
import ro.siveco.ael.lcms.domain.auth.model.AelUser;
import ro.siveco.ael.lcms.domain.auth.service.AelUserService;
import ro.siveco.ael.lcms.domain.metadata.model.*;
import ro.siveco.ael.lcms.domain.metadata.service.CountryService;
import ro.siveco.ael.lcms.domain.metadata.service.InterestService;
import ro.siveco.ael.lcms.domain.metadata.service.MetadataService;
import ro.siveco.ael.lcms.domain.metadata.service.TagService;
import ro.siveco.ael.lcms.domain.profile.model.*;
import ro.siveco.ael.lcms.repository.*;
import ro.siveco.ael.service.MultimediaService;
import ro.siveco.ael.service.utils.DatesService;
import ro.siveco.ael.web.controllers.WithUserBaseEntityController;
import ro.siveco.ram.app.exception.AppException;
import ro.siveco.ram.repository.BaseEntityRepository;
import ro.siveco.ram.starter.service.BaseEntityService;

import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayOutputStream;
import java.security.Principal;
import java.util.*;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

@Service
public class UserDetailsService extends BaseEntityService<UserDetails> {

    private AelUserRepository aelUserRepository;

    private MetadataRepository metadataRepository;

    private TagRepository tagRepository;

    private UserCharacteristicRepository userCharacteristicRepository;

    private UserCharacteristicValueRepository userCharacteristicValueRepository;

    private MultimediaService multimediaService;

    private LanguageRepository languageRepository;

    private CharacteristicRepository characteristicRepository;

    private CharacteristicValueRepository characteristicValueRepository;

    private UserCharacteristicTabRepository userCharacteristicTabRepository;

    private ShaPasswordEncoder passwordEncoder;

    private TagService tagService;

    private MetadataService metadataService;

    private AelUserService aelUserService;

    private CountryService countryService;

    private InterestService interestService;

    private PlannedCourseRepository plannedCourseRepository;

    private DatesService datesService;

    @Autowired
    public UserDetailsService(BaseEntityRepository<UserDetails> resourceRepository,
                              AelUserRepository aelUserRepository,
                              MetadataRepository metadataRepository,
                              TagRepository tagRepository,
                              UserCharacteristicRepository userCharacteristicRepository,
                              UserCharacteristicValueRepository userCharacteristicValueRepository,
                              MultimediaService multimediaService,
                              LanguageRepository languageRepository,
                              CharacteristicRepository characteristicRepository,
                              CharacteristicValueRepository characteristicValueRepository,
                              UserCharacteristicTabRepository userCharacteristicTabRepository,
                              ShaPasswordEncoder passwordEncoder,
                              TagService tagService,
                              MetadataService metadataService,
                              AelUserService aelUserService,
                              CountryService countryService,
                              InterestService interestService,
                              PlannedCourseRepository plannedCourseRepository,
                              DatesService datesService) {

        super(UserDetails.class, resourceRepository);
        this.aelUserRepository = aelUserRepository;
        this.metadataRepository = metadataRepository;
        this.tagRepository = tagRepository;
        this.userCharacteristicRepository = userCharacteristicRepository;
        this.userCharacteristicValueRepository = userCharacteristicValueRepository;
        this.multimediaService = multimediaService;
        this.languageRepository = languageRepository;
        this.characteristicRepository = characteristicRepository;
        this.characteristicValueRepository = characteristicValueRepository;
        this.userCharacteristicTabRepository = userCharacteristicTabRepository;
        this.passwordEncoder = passwordEncoder;
        this.tagService = tagService;
        this.metadataService = metadataService;
        this.aelUserService = aelUserService;
        this.countryService = countryService;
        this.interestService = interestService;
        this.plannedCourseRepository = plannedCourseRepository;
        this.datesService = datesService;
    }

    public AelUser getUserById(Long userId) {

        return aelUserRepository.findOne(userId);
    }

    public Optional<UserDetails> getByAelUser(AelUser aelUser) {

        return ((UserDetailsRepository) getResourceRepository()).findOneByUser(aelUser);
    }

    public UserDetails createIfNotExist(AelUser aelUser) {

        Optional<UserDetails> userDetailsOptional = getByAelUser(aelUser);

        if (!userDetailsOptional.isPresent()) {
            UserDetails userDetails = new UserDetails();
            userDetails.setUser(aelUser);
            return getResourceRepository().save(userDetails);
        } else {
            return  userDetailsOptional.get();
        }
    }

    public UserDetails getBasicData(Long userId) {

        UserDetails ud = null;
        List<UserDetails> list = ((UserDetailsRepository) getResourceRepository()).getByUserId(userId);

        if (list != null && list.size() > 0) {
            ud = list.get(0);

            ud.setFirstName(ud.getUser().getFirstName());
            ud.setLastName(ud.getUser().getLastName());
            ud.setGender(ud.getUser().getGender());
            ud.setAddress(ud.getUser().getAddress());
            ud.setPhoneNo(ud.getUser().getPhoneNo());
            ud.setHasImage(ud.getImage() != null);

            if (ud.getBirthDate() != null) {
                Years age = Years.yearsBetween(new LocalDate(ud.getBirthDate()), new LocalDate());
                ud.setAge(age.getYears());
            }
        } else {
            ud = new UserDetails();
            AelUser user = aelUserRepository.findOne(userId);

            ud.setUser(user);
            save(ud);

            ud.setFirstName(user.getFirstName());
            ud.setLastName(user.getLastName());
            ud.setGender(user.getGender());
            ud.setAddress(user.getAddress());
            ud.setPhoneNo(user.getPhoneNo());
            ud.setHasImage(false);
        }

        return ud;
    }

    public void saveBasic(UserDetails user, Long userId) {

        UserDetails ud = null;
        List<UserDetails> list = ((UserDetailsRepository) getResourceRepository()).getByUserId(userId);

        if (list != null && list.size() > 0) {
            ud = list.get(0);

            ud.setBirthDate(user.getBirthDate());
            ud.setSchool(user.getSchool());
            ud.setUserClass(user.getUserClass());
            ud.setSchoolType(user.getSchoolType());

            save(ud);

            AelUser u = ud.getUser();

            u.setFirstName(user.getFirstName());
            u.setLastName(user.getLastName());
            u.setGender(user.getGender());
            u.setAddress(user.getAddress());
            u.setPhoneNo(user.getPhoneNo());

            aelUserRepository.save(u);
        }
    }

    /**
     * User (re)authentication
     *
     * This method was created in order to re-check the user password (for the purpose of account deletion).
     * Normally, I'd use "throw new AppException("error.deleteProfile.wrong.password");", however, since request
     * parameter for password form has to be handled without ajax ("no-ajax" class), and AppException requires ajax
     * to convey information as a pop-up, the other way of presenting this information has to be thought of.
     *
     */
    public boolean checkPassword(String password, AelUser user) {
        Boolean passwordCorrect = null;

        // Encode password posted with HTML input field.
        String encodedPassword = passwordEncoder.encodePassword(password, null);

        if (encodedPassword.equals(user.getPassword())) {
            passwordCorrect = true;
        } else {
            passwordCorrect = false;
        }

        return passwordCorrect;
    }

    /**
     * Anonymize sensitive user information to "remove" an account.
     *
     * @param user user whose account is to be removed
     */
    public void anonymizeProfile(AelUser user) {

        // Find user details corresponding to the user.
        Optional<UserDetails> optionalUserDetails = ((UserDetailsRepository) getResourceRepository()).findOneByUser(user);

        if (optionalUserDetails.isPresent()) {
            // Get user details.
            UserDetails userDetails = optionalUserDetails.get();

            // Anonymize user information in "users" table.
            anonymizeUser(user);

            // Anonymize user information in "user_details" table.
            anonymizeUserDetails(userDetails);
        }
    }

    /**
     * User anonymization
     *
     * @param user object containing a user whose account is to be removed
     */
    private void anonymizeUser(AelUser user) {

        // Get user ID.
        Long userId = user.getId();

        // What to display as first name and last name.
        String anonymous = "Anonymous";

        // Anonymize data from "users" table.
        // Set random string as password.
        String generatedString = RandomStringUtils.randomAlphanumeric(40);
        user.setPassword(generatedString);
        // Disable account, so that no one can log in.
        user.setDisabled(Boolean.TRUE);
        // Username has constraints, hence ID.
        user.setUsername((String) anonymous + userId);
        user.setFirstName((String) anonymous);
        user.setLastName((String) anonymous);
        user.setEmail(null);
        user.setGender(null);
        user.setDisplayName((String) anonymous);
        user.setPhoneNo(null);
        user.setCity(null);
        user.setCountry(null);

        // Update record in "users" table.
        aelUserRepository.save(user);
    }

    /**
     * User details anonymization
     *
     * @param userDetails information like date of birth and profile image
     */
    private void anonymizeUserDetails(UserDetails userDetails) {

        // Change birthday date to deletion date.
        userDetails.setBirthDate(new Date());
        userDetails.setImage(null);
        userDetails.setShow_city(Boolean.FALSE);
        userDetails.setShow_country(Boolean.FALSE);
        userDetails.setShow_phone(Boolean.FALSE);
        userDetails.setShow_gender(Boolean.FALSE);
        userDetails.setShow_birth_date(Boolean.FALSE);
        userDetails.setShow_email(Boolean.FALSE);
        userDetails.setShow_interests(Boolean.FALSE);
        userDetails.setCity_name(null);

        // Update record in "user_details" table.
        save(userDetails);
    }

    public CharacteristicValue getCharacteristicValue(Long characteristicId, Locale locale) {

        List<CharacteristicValue> chVals =
                userCharacteristicRepository.getCharacteristicByIdAndLanguage(characteristicId, locale.getLanguage());

        if (chVals != null && chVals.size() > 0) {
            return chVals.get(0);
        } else {
            chVals = userCharacteristicRepository.getCharacteristicById(characteristicId);

            if (chVals != null && chVals.size() > 0) {
                return chVals.get(0);
            }
        }

        return null;
    }

    public List<UserCharacteristicModel> getActiveTabs(Long userId) {

        List<UserCharacteristicTab> list = userCharacteristicRepository.getActiveTabs();
        List<UserCharacteristicModel> response = new ArrayList<UserCharacteristicModel>();
        Locale locale = aelUserRepository.findOneById(userId).getLocale();

        for (int i = 0; i < list.size(); i++) {
            CharacteristicValue chVal = getCharacteristicValue(list.get(i).getTab().getId(), locale);
            response.add(new UserCharacteristicModel(list.get(i).getId(), chVal.getValue(), chVal.getDescription()));
        }

        return response;
    }

    public List<UserCharacteristicModel> getAllTabs(Long userId) {

        List<UserCharacteristicTab> list = userCharacteristicRepository.getAllTabs();
        List<UserCharacteristicModel> response = new ArrayList<UserCharacteristicModel>();
        Locale locale = aelUserRepository.findOneById(userId).getLocale();

        for (int i = 0; i < list.size(); i++) {
            CharacteristicValue chVal = getCharacteristicValue(list.get(i).getTab().getId(), locale);
            response.add(new UserCharacteristicModel(list.get(i).getId(), chVal.getValue(), chVal.getDescription(),
                    list.get(i).getActive()));
        }

        return response;
    }

    public List<UserCharacteristicModel> getTabCharacteristics(Long tabId, Long userId) {

        List<UserCharacteristicModel> list = new ArrayList<UserCharacteristicModel>();
        List<UserCharacteristic> ucList = userCharacteristicRepository.getActiveCharacteristicsByTab(tabId);
        List<Long> catList = new ArrayList<Long>();
        List<List<Long>> subcatList = new ArrayList<List<Long>>();

        List<Long> existingUcIds = new ArrayList<Long>();
        List<UserCharacteristicValue> existingUcvList =
                userCharacteristicValueRepository.getCharacteristicValuesBydUser(userId);
        Locale locale = aelUserRepository.findOneById(userId).getLocale();

        for (int i = 0; i < existingUcvList.size(); i++) {
            existingUcIds.add(existingUcvList.get(i).getCharacteristic().getId());
        }

        for (int i = 0; i < ucList.size(); i++) {
            UserCharacteristic uc = ucList.get(i);

            if (!catList.contains(uc.getCategory().getId())) {
                CharacteristicValue chVal = getCharacteristicValue(uc.getCategory().getId(), locale);
                UserCharacteristicModel cat = new UserCharacteristicModel(chVal.getValue(), chVal.getDescription());

                if (uc.getCharacteristic() != null) {
                    chVal = getCharacteristicValue(uc.getCharacteristic().getId(), locale);
                    buildProfileSubcategory(subcatList, existingUcIds, existingUcvList, locale, uc, chVal, cat);
                } else {
                    setMetadataListForCategory(locale, uc, cat);
                    updateCategoryWithInformation(existingUcIds, existingUcvList, uc, cat);
                }

                list.add(cat);

                catList.add(uc.getCategory().getId());
            } else {
                int index = catList.indexOf(uc.getCategory().getId());
                UserCharacteristicModel cat = list.get(index);

                if (uc.getCharacteristic() != null) {
                    if (!subcatList.get(index).contains(uc.getCharacteristic().getId())) {
                        CharacteristicValue chVal = getCharacteristicValue(uc.getCharacteristic().getId(), locale);
                        buildProfileSubcategory(subcatList, existingUcIds, existingUcvList, locale, uc, chVal, cat);
                    } else {
                        int index2 = subcatList.get(index).indexOf(uc.getCategory().getId());
                        UserCharacteristicModel subcat = list.get(index).getList().get(index2);

                        if (uc.getSpecific() != null) {
                            CharacteristicValue chVal = getCharacteristicValue(uc.getSpecific().getId(), locale);
                            UserCharacteristicModel subcat2 =
                                    new UserCharacteristicModel(uc.getId(), chVal.getValue(), chVal.getDescription(),
                                            uc.getType());
                            if (uc.getType().equals(UserCharacteristicType.TAG) ||
                                    uc.getType().equals(UserCharacteristicType.TAG_LIST)) {
                                subcat2.setMetadataList(userCharacteristicRepository
                                        .getMetadatasByCharacteristicIdAndLanguage(uc.getId(), EntityType.USER,
                                                locale.getLanguage()));
                            }
                            updateCategoryWithInformation(existingUcIds, existingUcvList, uc, subcat2);
                            subcat.getList().add(subcat2);
                        } else {
                            setMetadataListForCategory(locale, uc, subcat);
                            updateCategoryWithInformation(existingUcIds, existingUcvList, uc, subcat);
                        }

                        cat.getList().set(index2, subcat);
                    }
                } else {
                    setMetadataListForCategory(locale, uc, cat);
                    updateCategoryWithInformation(existingUcIds, existingUcvList, uc, cat);
                }

                list.set(index, cat);
            }
        }

        return list;
    }

    public void setMetadataListForCategory(Locale locale, UserCharacteristic uc, UserCharacteristicModel cat) {

        cat.setId(uc.getId());
        cat.setType(uc.getType());
        if (uc.getType().equals(UserCharacteristicType.TAG) || uc.getType().equals(UserCharacteristicType.TAG_LIST)) {
            cat.setMetadataList(userCharacteristicRepository
                    .getMetadatasByCharacteristicIdAndLanguage(uc.getId(), EntityType.USER, locale.getLanguage()));
        }
    }

    private void updateCategoryWithInformation(List<Long> existingUcIds, List<UserCharacteristicValue> existingUcvList,
                                               UserCharacteristic uc, UserCharacteristicModel cat) {

        if (existingUcIds.contains(uc.getId())) {
            if (uc.getType().equals(UserCharacteristicType.INTEGER) ||
                    uc.getType().equals(UserCharacteristicType.STRING)) {
                cat.setValueS(existingUcvList.get(existingUcIds.indexOf(uc.getId())).getValueString());
            }
            if (uc.getType().equals(UserCharacteristicType.BOOLEAN)) {
                cat.setValueB(existingUcvList.get(existingUcIds.indexOf(uc.getId())).getValueBoolean());
            }
            if (uc.getType().equals(UserCharacteristicType.TAG)) {
                cat.setValueL(existingUcvList.get(existingUcIds.indexOf(uc.getId())).getMetadata().getId());
            }
            if (uc.getType().equals(UserCharacteristicType.TAG_LIST)) {
                for (int k = 0; k < existingUcvList.size(); k++) {
                    if (existingUcvList.get(k).getCharacteristic().getId().equals(uc.getId())) {
                        for (int j = 0; j < cat.getMetadataList().size(); j++) {
                            if (cat.getMetadataList().get(j).getId() == existingUcvList.get(k).getMetadata().getId()) {
                                cat.getMetadataList().get(j).setChecked(true);
                            }
                        }
                    }
                }
            }
        }
    }

    private void buildProfileSubcategory(List<List<Long>> subcatList, List<Long> existingUcIds,
                                         List<UserCharacteristicValue> existingUcvList, Locale locale,
                                         UserCharacteristic uc, CharacteristicValue chVal,
                                         UserCharacteristicModel cat) {

        UserCharacteristicModel subcat = new UserCharacteristicModel(chVal.getValue(), chVal.getDescription());

        if (uc.getSpecific() != null) {
            chVal = getCharacteristicValue(uc.getSpecific().getId(), locale);
            UserCharacteristicModel subcat2 =
                    new UserCharacteristicModel(uc.getId(), chVal.getValue(), chVal.getDescription(), uc.getType());

            if (uc.getType().equals(UserCharacteristicType.TAG) ||
                    uc.getType().equals(UserCharacteristicType.TAG_LIST)) {
                subcat2.setMetadataList(userCharacteristicRepository
                        .getMetadatasByCharacteristicIdAndLanguage(uc.getId(), EntityType.USER, locale.getLanguage()));
            }
            updateCategoryWithInformation(existingUcIds, existingUcvList, uc, subcat2);

            subcat.getList().add(subcat2);
        } else {
            setMetadataListForCategory(locale, uc, subcat);
            updateCategoryWithInformation(existingUcIds, existingUcvList, uc, subcat);
        }

        cat.getList().add(subcat);

        List<Long> subcatNew = new ArrayList<Long>();
        subcatNew.add(uc.getCharacteristic().getId());
        subcatList.add(subcatNew);
    }

    public void saveCharacteristics(List<UserCharacteristicModel> list, Long userId) {

        AelUser user = aelUserRepository.findOne(userId);

        for (UserCharacteristicModel ucm : list) {
            if (ucm.getValue() != null) {
                if (ucm.getName().contains("_")) {
                    Long ucId = Long.parseLong(ucm.getName().substring(0, ucm.getName().indexOf('_')));
                    Long metadataId = Long.parseLong(
                            ucm.getName().substring(ucm.getName().indexOf('_') + 1, ucm.getName().length()));

                    UserCharacteristic uc = userCharacteristicRepository.findOne(ucId);
                    List<UserCharacteristicValue> ucvList = userCharacteristicValueRepository
                            .getCharacteristicValuesByCharactAndUserAndMetadata(uc.getId(), userId, metadataId);
                    UserCharacteristicValue ucv;

                    if (ucvList == null || ucvList.size() == 0) {
                        ucv = new UserCharacteristicValue();
                        ucv.setUser(user);
                        ucv.setCharacteristic(uc);
                        ucv.setMetadata(metadataRepository.findOne(metadataId));

                        if (ucm.getValue().equals("true")) {
                            userCharacteristicValueRepository.save(ucv);
                        }
                    } else {
                        ucv = ucvList.get(0);

                        if (ucm.getValue().equals("false")) {
                            userCharacteristicValueRepository.delete(ucv);
                        }
                    }
                } else {
                    UserCharacteristic uc = userCharacteristicRepository.findOne(Long.parseLong(ucm.getName()));
                    List<UserCharacteristicValue> ucvList = userCharacteristicValueRepository
                            .getCharacteristicValuesByCharactAndUser(uc.getId(), userId);
                    UserCharacteristicValue ucv;

                    if (ucvList == null || ucvList.size() == 0) {
                        ucv = new UserCharacteristicValue();
                        ucv.setUser(user);
                        ucv.setCharacteristic(uc);
                    } else {
                        ucv = ucvList.get(0);
                    }

                    if (uc.getType().equals(UserCharacteristicType.INTEGER) ||
                            uc.getType().equals(UserCharacteristicType.STRING)) {
                        ucv.setValueString(ucm.getValue());
                    }
                    if (uc.getType().equals(UserCharacteristicType.BOOLEAN)) {
                        if (ucm.getValue().equals("true")) {
                            ucv.setValueBoolean(true);
                        } else if (ucm.getValue().equals("false")) {
                            ucv.setValueBoolean(false);
                        }
                    }
                    if (uc.getType().equals(UserCharacteristicType.TAG)) {
                        ucv.setMetadata(metadataRepository.findOne(Long.parseLong(ucm.getValue())));
                    }

                    userCharacteristicValueRepository.save(ucv);
                }
            }
        }
    }

    public void addImage(Long userId, MultipartFile file) throws Exception {

        List<UserDetails> list = ((UserDetailsRepository) getResourceRepository()).getByUserId(userId);

        if (list != null && list.size() > 0) {
            UserDetails ud = list.get(0);

            ByteArrayOutputStream baos = multimediaService.processThumbnail(file);
            byte[] binaryData = baos.toByteArray();

            ud.setImage(binaryData);
            save(ud);
        }
    }

    public byte[] getImage(Long userId) {

        List<UserDetails> list = ((UserDetailsRepository) getResourceRepository()).getByUserId(userId);

        if (list != null && list.size() > 0) {
            UserDetails ud = list.get(0);
            return ud.getImage();
        }

        return null;
    }

    public List<UserCharacteristicManagementModel> getAllCharacteristics(Long userId) {

        List<UserCharacteristic> ucList = userCharacteristicRepository.getCharacteristics();
        List<UserCharacteristicManagementModel> list = new ArrayList<UserCharacteristicManagementModel>();
        Locale locale = aelUserRepository.findOneById(userId).getLocale();

        for (int i = 0; i < ucList.size(); i++) {
            UserCharacteristic uc = ucList.get(i);

            UserCharacteristicManagementModel ucm =
                    new UserCharacteristicManagementModel(uc.getId(), uc.getCategory().getId(),
                            uc.getCharacteristic() != null ? uc.getCharacteristic().getId() : null,
                            uc.getSpecific() != null ? uc.getSpecific().getId() : null, uc.getTab().getId(),
                            uc.getType(), uc.getActive());

            CharacteristicValue chVal = getCharacteristicValue(uc.getCategory().getId(), locale);
            ucm.setCategory(chVal.getValue());

            if (uc.getCharacteristic() != null) {
                chVal = getCharacteristicValue(uc.getCharacteristic().getId(), locale);
                ucm.setCharacteristic(chVal.getValue());
            }
            if (uc.getSpecific() != null) {
                chVal = getCharacteristicValue(uc.getSpecific().getId(), locale);
                ucm.setSpecific(chVal.getValue());
            }

            chVal = getCharacteristicValue(uc.getTab().getId(), locale);
            ucm.setTab(chVal.getValue());

            list.add(ucm);
        }

        return list;
    }

    public List<Characteristic> getCharacteristicsByType(Long userId, CharacteristicType type) {

        List<Characteristic> list = userCharacteristicRepository.getCharacteristicsByType(type);
        Locale locale = aelUserRepository.findOneById(userId).getLocale();

        for (int i = 0; i < list.size(); i++) {
            CharacteristicValue chVal = getCharacteristicValue(list.get(i).getId(), locale);
            list.get(i).setId(list.get(i).getId());
            list.get(i).setValue(chVal.getValue());
            list.get(i).setDescription(chVal.getDescription());
        }

        return list;
    }

    public List<Characteristic> getCharacteristicTabs(Long userId) {

        List<UserCharacteristicTab> list = userCharacteristicRepository.getActiveTabs();
        List<Characteristic> result_list = new ArrayList<Characteristic>();
        Locale locale = aelUserRepository.findOneById(userId).getLocale();

        for (int i = 0; i < list.size(); i++) {
            CharacteristicValue chVal = getCharacteristicValue(list.get(i).getTab().getId(), locale);
            Characteristic newCha = new Characteristic();

            newCha.setId(list.get(i).getId());
            newCha.setValue(chVal.getValue());
            newCha.setDescription(chVal.getDescription());

            result_list.add(newCha);
        }

        return result_list;
    }

    public UserCharacteristicModel saveCharacteristic(List<UserCharacteristicModel> list, String type, Long userId) {

        List<CharacteristicValue> cvList = new ArrayList<CharacteristicValue>();
        List<Long> languageIds = new ArrayList<Long>();

        for (int i = 0; i < list.size(); i++) {
            UserCharacteristicModel ucm = list.get(i);

            if (ucm.getValue() != null && !ucm.getValue().isEmpty() && !ucm.getValue().equals("")) {
                Long languageId = Long.parseLong(ucm.getName().substring(0, ucm.getName().indexOf('_')));
                String field = ucm.getName().substring(ucm.getName().indexOf('_') + 1, ucm.getName().length());

                if (languageIds.contains(languageId)) {
                    if (field.equals("value")) {
                        cvList.get(languageIds.indexOf(languageId)).setValue(ucm.getValue());
                    } else {
                        cvList.get(languageIds.indexOf(languageId)).setDescription(ucm.getValue());
                    }
                } else {
                    CharacteristicValue newCV = new CharacteristicValue();

                    if (field.equals("value")) {
                        newCV.setValue(ucm.getValue());
                    } else {
                        newCV.setDescription(ucm.getValue());
                    }

                    languageIds.add(languageId);
                    cvList.add(newCV);
                }
            }
        }

        Characteristic characteristic = new Characteristic(CharacteristicType.CHARACTERISTIC);
        if (type.equals("tab")) {
            characteristic.setType(CharacteristicType.TAB);
        }
        characteristic = characteristicRepository.saveAndFlush(characteristic);

        for (int i = 0; i < languageIds.size(); i++) {
            cvList.get(i).setCharacteristic(characteristic);
            cvList.get(i).setLanguage(languageRepository.findOne(languageIds.get(i)));

            characteristicValueRepository.saveAndFlush(cvList.get(i));
        }

        Locale locale = aelUserRepository.findOneById(userId).getLocale();
        CharacteristicValue chVal = getCharacteristicValue(characteristic.getId(), locale);
        UserCharacteristicModel ucm = new UserCharacteristicModel();
        ucm.setId(characteristic.getId());
        ucm.setValue(chVal.getValue());

        if (type.equals("tab")) {
            UserCharacteristicTab uct = new UserCharacteristicTab(characteristic);
            uct = userCharacteristicTabRepository.saveAndFlush(uct);

            ucm.setId(uct.getId());
        }

        return ucm;
    }

    public void addUserCharacteristic(UserCharacteristicManagementModel ucmm) {

        UserCharacteristic uc = new UserCharacteristic();

        if (ucmm.getCategoryId() == null || ucmm.getTabId() == null || ucmm.getTypeCode() == null) {
            return;
        }
        uc.setCategory(characteristicRepository.findOne(ucmm.getCategoryId()));
        uc.setTab(userCharacteristicTabRepository.findOne(ucmm.getTabId()));
        uc.setType(ucmm.getTypeCode());
        uc.setActive(true);

        if (ucmm.getCharacteristicId() != null) {
            uc.setCharacteristic(characteristicRepository.findOne(ucmm.getCharacteristicId()));
        }
        if (ucmm.getSpecificId() != null) {
            uc.setSpecific(characteristicRepository.findOne(ucmm.getSpecificId()));
        }

        userCharacteristicRepository.save(uc);
    }

    public void changeCharactetisticState(Long ucID, Boolean state) {

        UserCharacteristic uc = userCharacteristicRepository.findOne(ucID);

        uc.setActive(state);
        userCharacteristicRepository.save(uc);
    }

    public List<Metadata> getCaracteristicTagList(Long ucID, Long userId) {

        Locale locale = aelUserRepository.findOneById(userId).getLocale();

        return userCharacteristicRepository
                .getMetadatasByCharacteristicIdAndLanguage(ucID, EntityType.USER, locale.getLanguage());
    }

    public CharacteristicValue addTagsToCharacteristic(List<UserCharacteristicModel> list, Long entityId, Long userId) {

        Locale locale = aelUserRepository.findOneById(userId).getLocale();
        CharacteristicValue wantedCV = new CharacteristicValue();

        for (int i = 0; i < list.size(); i++) {
            UserCharacteristicModel ucm = list.get(i);

            if (ucm.getValue() != null && !ucm.getValue().isEmpty() && !ucm.getValue().equals("")) {
                Long languageId = Long.parseLong(ucm.getName().substring(0, ucm.getName().indexOf('_')));
                Language lang = languageRepository.findOne(languageId);

                String field = ucm.getName().substring(ucm.getName().indexOf('_') + 1, ucm.getName().length());

                Tag tag = new Tag();
                tag.setLanguage(lang);
                tag.setValue(ucm.getValue());

                tag = tagRepository.saveAndFlush(tag);

                Metadata mtd = new Metadata();
                mtd.setEntityId(entityId);
                mtd.setEntityType(EntityType.USER);
                mtd.setTag(tag);
                mtd.setRelationType(RelationType.INCLUDED);
                mtd = metadataRepository.saveAndFlush(mtd);

                if (lang.getName().equals(locale.getLanguage())) {
                    wantedCV.setValue(ucm.getValue());
                    wantedCV.setId(mtd.getId());
                }
            }
        }

        return wantedCV;
    }

    public void changeTabState(Long tabID, boolean state) {

        UserCharacteristicTab uct = userCharacteristicTabRepository.findOne(tabID);

        uct.setActive(state);
        userCharacteristicTabRepository.save(uct);
    }

    public String validateNewPassword(UserDetails userDetails) {

        if ((userDetails.getOldPassword() != null || userDetails.getPassword() != null ||
                userDetails.getConfirmPassword() != null)) {
            if (userDetails.getOldPassword() == null) {
                throw new AppException("error.userDetails.check.old.password");
            }
            if (userDetails.getPassword() == null) {
                throw new AppException("error.userDetails.check.new.password");
            }
            if (userDetails.getConfirmPassword() == null) {
                throw new AppException("error.userDetails.check.confirm.password");
            }
        }

        if (userDetails.getOldPassword() != null && userDetails.getPassword() != null &&
                userDetails.getConfirmPassword() != null) {
            String oldPassword =
                    passwordEncoder.encodePassword(userDetails.getOldPassword(), null);
            if (!oldPassword.equals(userDetails.getUser().getPassword())) {
                throw new AppException("error.userDetails.change.old.password.match");
            } else if (!userDetails.getPassword().equals(userDetails.getConfirmPassword())) {
                throw new AppException("error.userDetails.change.new.password.match");
            } else if (!validatePasswordPattern(userDetails.getPassword())) {
                throw new AppException("error.userDetails.change.password.match.pattern");
            } else {
                return passwordEncoder.encodePassword(userDetails.getPassword(), null);
            }
        }
        return null;
    }

    public UserDetails saveOrUpdate(UserDetails userDetails)
    {
        if(userDetails.getId() == null)
        {
            return save(userDetails);
        }
        else
        {
            return update(userDetails);
        }
    }

    public Boolean validateUserBirthDate(UserDetails userDetails) {
        if (userDetails != null && userDetails.getBirthDate() != null && userDetails.getBirthDate().after(new Date())) {
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }

    public Boolean validateUserFirstName(UserDetails userDetails) {
        if (userDetails != null && (userDetails.getFirstName() == null || userDetails.getFirstName().isEmpty() || userDetails.getFirstName().trim().isEmpty())) {
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }

    public Boolean validateUserLastName(UserDetails userDetails) {
        if (userDetails != null && (userDetails.getLastName() == null || userDetails.getLastName().isEmpty() || userDetails.getLastName().trim().isEmpty())) {
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }

    public Boolean validateUserLanguage(UserDetails userDetails) {
        if (userDetails != null && userDetails.getLanguage() == null) {
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }

    public Boolean validateUserCountry(UserDetails userDetails) {
        if (userDetails != null && userDetails.getCountry() == null) {
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }

    public Boolean validateUserCity(UserDetails userDetails) {
        if (userDetails != null && userDetails.getCity() == null) {
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }

    public Boolean validatePhone(UserDetails userDetails) {
        if (userDetails != null && userDetails.getPhoneNo() != null && !userDetails.getPhoneNo().isEmpty()) {
            Pattern pDigit = Pattern.compile("^[0-9]+");
            return (pDigit.matcher(userDetails.getPhoneNo()).matches());
        }
        return Boolean.TRUE;
    }

    public void populateUserDetails(UserDetails userDetails) {
        if (userDetails != null) {
            populateUserInterests(userDetails);
        }
    }

    public void populateUserInterests(UserDetails userDetails) {
        if (userDetails != null) {
            userDetails.setIncludedTags(tagService.getTags(metadataService
                    .findByEntityIdAndEntityTypeAndRelationType(userDetails.getUser().getId(), EntityType.COURSE,
                            RelationType.INCLUDED)));
        }
    }

    public void populateUserAllInterests(UserDetails userDetails) {
        if (userDetails != null) {
            HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
            Principal principal = SecurityContextHolder.getContext().getAuthentication();

            userDetails.setAllInterests(interestService.getAllI18NInterests(principal, request));
            setSelectedInterests(userDetails);
        }
    }

    private void setSelectedInterests(UserDetails userDetails) {
        if (userDetails != null && userDetails.getIncludedTags() != null && userDetails.getAllInterests() != null) {
            userDetails.getAllInterests().forEach(i -> i.setSelected(isIncludedInterest(i, userDetails)));
        }
    }

    private Boolean isIncludedInterest(Interest interest, UserDetails userDetails) {
        if (userDetails != null && interest != null && interest.getTag() != null && userDetails.getIncludedTags() != null) {
            for(Tag tag : userDetails.getIncludedTags()) {
                if (interest.getTag().getId().equals(tag.getId())) {
                    return true;
                }
            }
        }
        return false;
    }

    public void validateUserDetails(UserDetails userDetails) {
        if (userDetails != null) {
            if (!validateUserBirthDate(userDetails)) {
                throw new AppException("error.userDetails.birthDate.after.currentDate");
            }
            if (!validateUserFirstName(userDetails)) {
                throw new AppException("error.userDetails.firstName.null");
            }
            if (!validateUserCountry(userDetails)) {
                throw new AppException("error.userDetails.country.null");
            }
            if (!validateUserCity(userDetails)) {
                throw new AppException("error.userDetails.city.null");
            }
            if (!validateUserLanguage(userDetails)) {
                throw new AppException("error.userDetails.language.null");
            }
            if (!validatePhone(userDetails)) {
                throw new AppException("create.profile.invalidPhoneNumber");
            }
        }
    }

    private Country getCountryByCountryDTO(CountryDTO countryDTO, AelUser aelUser) {
        if (countryDTO == null) {
            return null;
        }
        List<Country> countries = countryService.findAllByCountryName(countryDTO.getName());
        if (countries != null && countries.size() >0) {
            Country country = countries.get(0);
            countryService.setDisplayName(country,aelUserService.getLocaleForUser(aelUser).getLanguage());
            return country;
        }
        return null;

    }

    public void populateUserDetails(UserDetails userDetails, AelUser aelUser) {
        if (userDetails != null) {
            userDetails.setFirstName(aelUser.getFirstName());
            userDetails.setLastName(aelUser.getLastName());
            userDetails.setUsername(aelUser.getUsername());
            userDetails.setEmail(aelUser.getEmail());
            userDetails.setPhoneNo(aelUser.getPhoneNo());

            userDetails.setCountry(getCountryByCountryDTO(aelUser.getCountry(), aelUser));
            userDetails.setCity(aelUser.getCity());
            userDetails.setGender(aelUser.getGender());
            if (userDetails.getBirthDate() != null) {
                Years age = Years.yearsBetween(new LocalDate(userDetails.getBirthDate()), new LocalDate());
                userDetails.setAge(age.getYears());
            }
            userDetails.setCreatedItemsNo( ((UserDetailsRepository)getResourceRepository()).getCreatedItemsNo(aelUser.getId()) );
            userDetails.setFavoredItemsNo( ((UserDetailsRepository)getResourceRepository()).getFavoredItemsNo(aelUser.getId()) );

            Locale locale = aelUserService.getLocaleForUser(aelUser);
            if (locale != null) {
                LanguageDTO languageDTO = new LanguageDTO();
                languageDTO.setName(getLocalizedLanguageName(locale.getLanguage(), locale));
                languageDTO.setId(locale.getLanguage());
                userDetails.setLanguage(languageDTO);
            }

            metadataService.populateUserTags(userDetails);
            populateUserAllInterests(userDetails);
            userDetails.setCurrentUserId(WithUserBaseEntityController.getUser().getId());
            if (userDetails.getBirthDate() != null) {
                userDetails.setDisplayBirthDate(datesService.formatLongDate(userDetails.getBirthDate()));
            }
        }
    }

    private Boolean validatePasswordPattern(String password) {
        if (password == null || password.length() < 6) {
            return false;
        }

        Pattern pDigit = Pattern.compile("[0-9]");
        Matcher mDigit = pDigit.matcher(password);

        Pattern pLowerLetter = Pattern.compile("[a-z]");
        Matcher mLowerLetter = pLowerLetter.matcher(password);

        Pattern pUpper = Pattern.compile("[A-Z]");
        Matcher mUpper = pUpper.matcher(password);
        return  (mDigit.find() && mLowerLetter.find() && mUpper.find());
    }

    public String getLocalizedLanguageName(String language, Locale locale) {
        return getMessageSource().getMessage("language." + language + ".label", null, locale);
    }

    public void deleteProfilePicture(Long userId) {
        if (userId == null) {
            throw new AppException("error.profile.delete.picture.userId.not.valid");
        }
        Optional<UserDetails> currentUserDetails = getByAelUser((AelUser)WithUserBaseEntityController.getUser());
        if (!currentUserDetails.isPresent()) {
            throw new AppException("error.profile.delete.picture.userId.not.valid");
        }
        if (!userId.equals(currentUserDetails.get().getId())) {
            throw new AppException("access.denied.message");
        }

        UserDetails userDetails = currentUserDetails.get();
        userDetails.setImage(null);
        save(userDetails);
    }
}
