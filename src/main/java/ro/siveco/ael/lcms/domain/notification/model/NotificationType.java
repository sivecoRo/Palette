package ro.siveco.ael.lcms.domain.notification.model;

/**
 * Created by IntelliJ IDEA.
 * User: SorinT
 * Date: 06.08.2007
 * Time: 12:41:04
 * To change this template use File | Settings | File Templates.
 */
public enum NotificationType {
    MESSAGE,
    MAIL,
    MESSAGE_AND_MAIL
}
