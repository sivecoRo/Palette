package ro.siveco.ael.lcms.domain.organization.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.siveco.ael.lcms.domain.auth.model.UserCompetence;
import ro.siveco.ael.lcms.domain.auth.service.UserCompetenceService;
import ro.siveco.ael.lcms.domain.organization.model.*;
import ro.siveco.ael.lcms.domain.profile.service.UserDetailsService;
import ro.siveco.ael.lcms.domain.taxonomies.model.TaxonomyLevel;
import ro.siveco.ael.lcms.domain.taxonomies.service.TaxonomyLevelService;
import ro.siveco.ael.lcms.repository.ComponentUserJobPathRepository;
import ro.siveco.ram.starter.service.BaseEntityService;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class ComponentUserJobPathService extends BaseEntityService<ComponentUserJobPath> {

    private ComponentUserJobService componentUserJobService;

    @Autowired
    public UserDetailsService userDetailsService;

    @Autowired
    public UserCompetenceService userCompetenceService;

    @Autowired
    public JobTitleService jobTitleService;

    @Autowired
    public TaxonomyLevelService taxonomyLevelService;

    @Autowired
    public ComponentUserJobPathService(ComponentUserJobPathRepository resourceRepository, ComponentUserJobService componentUserJobService) {
        super(ComponentUserJobPath.class, resourceRepository);
        this.componentUserJobService = componentUserJobService;
    }

    @Override
    protected ComponentUserJobPathRepository getResourceRepository() {
        return (ComponentUserJobPathRepository) super.getResourceRepository();
    }

    public List<JobTitle> getRecommendedJobTitles(Map<JobTitle, Double> jobTitleDoubleMap) {

        List<JobTitle> jobTitles = new ArrayList<>();
        jobTitleDoubleMap = jobTitleDoubleMap.entrySet().stream()
                .sorted((Map.Entry.comparingByValue(Collections.reverseOrder())))
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue,
                        (e1, e2) -> e1,
                        LinkedHashMap::new
                ));
        for(Map.Entry<JobTitle, Double> entry : jobTitleDoubleMap.entrySet()) {
            if(entry.getValue().compareTo((Double)0.00) == 1) {
                jobTitles.add(entry.getKey());
            }
        }
        return jobTitles;
    }

    public List<ComponentUserJobPath> getAllCujPathDataForUser(Long userId) {
        return ((ComponentUserJobPathRepository) getResourceRepository()).getAllCujPathDataForUser(userId);
    }

    public List<ComponentUserJobPath> getCurrentCujPathDataForUser(Long userId, ComponentUserJobStatus status) {
        return ((ComponentUserJobPathRepository) getResourceRepository()).getCurrentCujPathDataForUser(userId, status);
    }

    public List<ComponentUserJobPath> getFutureCujPathDataForUser(Long userId, ComponentUserJobStatus status) {
        return ((ComponentUserJobPathRepository) getResourceRepository()).getFutureCujPathDataForUser(userId, status);
    }

    public List<ComponentUserJobPath> getCujPathDataForUser(Long userId, Boolean retrieveFuture, Boolean onlyCurrentDataSet) {
        if(retrieveFuture.equals(Boolean.FALSE) && onlyCurrentDataSet.equals(Boolean.FALSE)) {
            return getAllCujPathDataForUser(userId);
        } else if(retrieveFuture.equals(Boolean.TRUE) && onlyCurrentDataSet.equals(Boolean.FALSE)) {
            return getFutureCujPathDataForUser(userId, ComponentUserJobStatus.FUTURE);
        } else if(retrieveFuture.equals(Boolean.FALSE) && onlyCurrentDataSet.equals(Boolean.TRUE)) {
            List<ComponentUserJobPath> returnList = new ArrayList<>();
            List<ComponentUserJobPath> currentComponentUserJobPath = getCurrentCujPathDataForUser(userId, ComponentUserJobStatus.CURRENT);
            if(currentComponentUserJobPath.size() != 1) {
                return null;
            }
            returnList.add(currentComponentUserJobPath.get(0).getPreviousComponentUserJobPath());
            returnList.add(currentComponentUserJobPath.get(0));
            returnList.addAll(getFutureCujPathDataForUser(userId, ComponentUserJobStatus.FUTURE));
            return returnList;
        } else {
            return null;
        }
    }



    public List<JobTitle> getJobRecommendationData(Long userId) {
        Map<JobTitle, Double> jobTitleScores = new HashMap<>();

        List<JobTitle> jobTitleList = jobTitleService.findJobTitlesByTenant(userDetailsService.getUserById(userId).getTenant() != null ? userDetailsService.getUserById(userId).getTenant().getId() : null);
        List<UserCompetence> userCompetenceList = userCompetenceService.findUserCompetencesByUser(userId);

        Map<Long, TaxonomyLevel> userCompetenceLevelMap = new HashMap<>();
        for(UserCompetence userCompetence : userCompetenceList) {
            userCompetenceLevelMap.put(userCompetence.getCompetence().getId(), userCompetence.getTaxonomyLevel());
        }

        for(JobTitle jobTitle : jobTitleList) {
            Double sum = 0.0;
            Integer aux = 0;
            for(JobTitleCompetence jobTitleCompetence : jobTitle.getJobTitleCompetenceList()) {
                sum = sum + taxonomyLevelService.calculateRatio(
                        userCompetenceLevelMap.get(jobTitleCompetence.getCompetence().getId()), jobTitleCompetence.getTaxonomyLevel());
                aux++;
            }
            jobTitleScores.put(jobTitle, (Double) sum/aux);
        }

        return getRecommendedJobTitles(jobTitleScores);
    }

    private ComponentUserJobPath addPathForComponentUserJob(ComponentUserJob componentUserJob){
        ComponentUserJobPath componentUserJobPath = new ComponentUserJobPath();
        componentUserJobPath.setComponentUserJob(componentUserJob);
        componentUserJobPath.setInsertedAtDate(new Date());
        componentUserJobPath.setStatus(ComponentUserJobStatus.FUTURE);
        return save(componentUserJobPath);
    }

    private ComponentUserJobPath getCurrentComponentUserJobPathForUser(Long userId){
        return this.getResourceRepository().getCurrentComponentUserJobPathForUser(userId, ComponentUserJobStatus.CURRENT);
    }

    private void changeStatusForComponentUserJobPath(ComponentUserJobPath componentUserJobPath, ComponentUserJobPath connectedComponentUserJobPath){
        if(componentUserJobPath.getStatus().equals(ComponentUserJobStatus.CURRENT)){
            componentUserJobPath.setStatus(ComponentUserJobStatus.PAST);
            componentUserJobPath.setNextComponentUserJobPath(connectedComponentUserJobPath);
        }else{
            componentUserJobPath.setStatus(ComponentUserJobStatus.CURRENT);
            componentUserJobPath.setPreviousComponentUserJobPath(connectedComponentUserJobPath);
        }
        save(componentUserJobPath);
    }

    private void setComponentUserJobPathAsCurrent(ComponentUserJobPath componentUserJobPathToSetAsCurrent){
        Long userId = componentUserJobPathToSetAsCurrent.getComponentUserJob().getUser().getId();
        ComponentUserJob currentlyActiveComponentUserJob = this.componentUserJobService.getActiveComponentUserJobForUser(userId);
        ComponentUserJobPath currentComponentUserJobPath = getCurrentComponentUserJobPathForUser(userId);
        if(currentComponentUserJobPath != null && currentlyActiveComponentUserJob != null){
            this.componentUserJobService.changeActiveStatus(currentlyActiveComponentUserJob, false);
            changeStatusForComponentUserJobPath(currentComponentUserJobPath, componentUserJobPathToSetAsCurrent);
        }
        this.componentUserJobService.changeActiveStatus(componentUserJobPathToSetAsCurrent.getComponentUserJob(), true);
        changeStatusForComponentUserJobPath(componentUserJobPathToSetAsCurrent, currentComponentUserJobPath);
    }

    public ComponentUserJobPath addNewComponentUserJobPath(ComponentUserJobPathDTO componentUserJobPathDTO){
        Long userId = componentUserJobPathDTO.getUserId();
        Long componentId = componentUserJobPathDTO.getComponentId();
        Long jobTitleId = componentUserJobPathDTO.getJobTitleId();
        Boolean setAsCurrent = componentUserJobPathDTO.getSetAsActive();
        //Fields validation
        if(userId == null){
            throw new RuntimeException("error.componentUserJobPath.add.missingField.user");
        }else if(componentId == null){
            throw new RuntimeException("error.componentUserJobPath.add.missingField.component");
        }else if(jobTitleId == null){
            throw new RuntimeException("error.componentUserJobPath.add.missingField.jobTitle");
        }else if(setAsCurrent == null){
            throw new RuntimeException("error.componentUserJobPath.add.missingField.currentCheckbox");
        }
        //setAsCurrent access validation
        if(setAsCurrent && 1<0
                //user is not admin
                ){
            throw new RuntimeException("error.componentUserJobPath.add.insufficientCredentials.admin");
        }
        ComponentUserJob componentUserJob = this.componentUserJobService.getComponentUserJobForUserComponentAndJobTitle(userId, componentId, jobTitleId);
        if(componentUserJob.getActive()){
            throw new RuntimeException("error.componentUserJobPath.add.invalidOperation.duplicateActive");
        }else{
            ComponentUserJobPath notDeletedFutureEntryForComponentUserJob = getResourceRepository().findNotDeletedFutureEntryForComponentUserJob(
                    componentUserJob.getId(), ComponentUserJobStatus.FUTURE);
            if(notDeletedFutureEntryForComponentUserJob != null){
                throw new RuntimeException("error.componentUserJobPath.add.invalidOperation.duplicateFuture");
            }
        }
        ComponentUserJobPath newComponentUserJobPath = addPathForComponentUserJob(componentUserJob);
        if(setAsCurrent){
            setComponentUserJobPathAsCurrent(newComponentUserJobPath);
        }
        return newComponentUserJobPath;
    }

    public void deleteFutureComponentUserJobPath(Long componentUserJobPathId){
        ComponentUserJobPath componentUserJobPath = getResourceRepository().findOne(componentUserJobPathId);
        //Validations
        if(componentUserJobPath == null){
            throw new RuntimeException("error.componentUserJobPath.delete.entityNotFound");
        }
        if(!componentUserJobPath.getStatus().equals(ComponentUserJobStatus.FUTURE)){
            throw new RuntimeException("error.componentUserJobPath.delete.invalidOperation.nonFuturePath");
        }
        if(componentUserJobPath.getRemovedAtDate()!=null){
            throw new RuntimeException("error.componentUserJobPath.delete.invalidOperation.deletedFuture");
        }
        componentUserJobPath.setRemovedAtDate(new Date());
        save(componentUserJobPath);
    }

    public void setComponentUserJobPathAsCurrent(Long componentUserJobPathId){
        ComponentUserJobPath componentUserJobPath = getResourceRepository().findOne(componentUserJobPathId);
        //Validations
        if(componentUserJobPath == null){
            throw new RuntimeException("error.componentUserJobPath.setAsActive.entityNotFound");
        }
        if(componentUserJobPath.getStatus().equals(ComponentUserJobStatus.CURRENT)){
            throw new RuntimeException("error.componentUserJobPath.setAsActive.invalidOperation.duplicateActive");
        }else if(componentUserJobPath.getStatus().equals(ComponentUserJobStatus.FUTURE) && componentUserJobPath.getRemovedAtDate()!=null){
            throw new RuntimeException("error.componentUserJobPath.setAsActive.invalidOperation.deletedFuture");
        }
        //Set as current
        if(componentUserJobPath.getStatus().equals(ComponentUserJobStatus.FUTURE)){
            componentUserJobPath.setRemovedAtDate(new Date());
            save(componentUserJobPath);
        }
        ComponentUserJobPath newComponentUserJobPath = addPathForComponentUserJob(componentUserJobPath.getComponentUserJob());
        setComponentUserJobPathAsCurrent(newComponentUserJobPath);
    }
}