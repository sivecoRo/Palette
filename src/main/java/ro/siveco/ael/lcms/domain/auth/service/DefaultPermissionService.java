package ro.siveco.ael.lcms.domain.auth.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;
import ro.siveco.ael.lcms.domain.auth.model.Role;
import ro.siveco.ael.lcms.domain.auth.model.User;
import ro.siveco.ael.lcms.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;

/**
 * User: AlexandruVi
 * Date: 2017-02-20
 */
@Service
public class DefaultPermissionService implements PermissionService {

    private final UserRepository userRepository;

    @Autowired
    public DefaultPermissionService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<GrantedAuthority> loadUserPermissions(User user) {
        List<GrantedAuthority> permissions = new ArrayList<>();
        user = userRepository.findOneById(user.getId());
        if (user != null) {
            List<Role> roles = user.getRoles();
            roles.forEach(role -> {
                permissions.add(new SimpleGrantedAuthority(role.getName()));
                if (role.getFunctions() != null) {
                    role.getFunctions().forEach(function -> {
                        permissions.add(new SimpleGrantedAuthority(function.getName()));
                    });
                }
            });
        }
        return permissions;
    }
}
