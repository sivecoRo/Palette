package ro.siveco.ael.lcms.domain.materials.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.siveco.ael.lcms.domain.materials.model.SliderSurveyItem;
import ro.siveco.ael.lcms.domain.materials.model.SurveyItem;
import ro.siveco.ael.lcms.repository.SliderSurveyItemRepository;
import ro.siveco.ram.repository.BaseEntityRepository;
import ro.siveco.ram.starter.service.BaseEntityService;

/**
 * Created by AndradaC on 5/2/2017.
 */
@Service
public class SliderSurveyItemService extends BaseEntityService<SliderSurveyItem> {
    @Autowired
    public SliderSurveyItemService(BaseEntityRepository<SliderSurveyItem> resourceRepository) {
        super(SliderSurveyItem.class, resourceRepository);
    }

    public SliderSurveyItem findOneById(Long id) {
        return ((SliderSurveyItemRepository) getResourceRepository()).findOneById(id);
    }

    public SliderSurveyItem findOneBySurveyItem(SurveyItem surveyItem) {
        return ((SliderSurveyItemRepository) getResourceRepository()).findOneBySurveyItem(surveyItem);
    }
}
