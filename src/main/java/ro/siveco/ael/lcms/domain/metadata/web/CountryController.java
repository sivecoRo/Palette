package ro.siveco.ael.lcms.domain.metadata.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import ro.siveco.ael.lcms.domain.metadata.model.Country;
import ro.siveco.ael.lcms.domain.metadata.service.CountryService;
import ro.siveco.ael.web.controllers.WithUserBaseEntityController;

/**
 * Created by albertb on 2/8/2018.
 */
@Controller
@RequestMapping("/location")
public class CountryController extends WithUserBaseEntityController<Country> {
    @Autowired
    public CountryController(CountryService restService) {
        super(restService);
    }
}
