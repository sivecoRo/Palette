package ro.siveco.ael.lcms.domain.auth.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ro.siveco.ael.lcms.domain.auth.model.AelUser;
import ro.siveco.ael.lcms.domain.organization.model.Component;
import ro.siveco.ael.lcms.domain.organization.model.ComponentUserJob;
import ro.siveco.ael.lcms.domain.organization.model.JobTitle;
import ro.siveco.ael.lcms.domain.organization.service.ComponentService;
import ro.siveco.ael.lcms.domain.organization.service.ComponentUserJobService;
import ro.siveco.ael.lcms.domain.organization.service.JobTitleService;
import ro.siveco.ael.lcms.domain.profile.service.UserDetailsService;
import ro.siveco.ael.lcms.domain.tenant.model.Tenant;
import ro.siveco.ram.app.exception.AppException;

import javax.transaction.Transactional;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

@Service
public class AelUserImporterService {

    @Autowired
    private AelUserService aelUserService;

    @Autowired
    private ComponentService componentService;

    @Autowired
    private JobTitleService jobTitleService;

    @Autowired
    private ComponentUserJobService componentUserJobService;

    @Autowired
    private UserDetailsService userDetailsService;


    public void importUsers (MultipartFile usersFile, Tenant tenant) throws Exception {
            InputStream inputStream = usersFile.getInputStream();
            String fileName = usersFile.getOriginalFilename();
            List<AelUser> usersToImport = new ArrayList<>();
            if (fileName.endsWith(".xls")) {
                usersToImport = aelUserService.importUsersFromXLS(inputStream, tenant);
            } else if (fileName.endsWith(".csv")) {
                usersToImport = aelUserService.importUsersFromCSV(inputStream, tenant);
            } else {
                throw new AppException("error.model.aelUser.importUsers.file.format");
            }

            if(usersToImport.size() > 0)
            {
                saveUsers(usersToImport);
            }
    }

    @Transactional
    protected void saveUsers(List<AelUser> users)
    {
        for(AelUser user : users)
        {
            //TODO: de completat cu valoarea din fisier
            String componentName = "coloana din fisier";
            String jobTitle = "coloana din fisier";

            if (!aelUserService.validateEmail(user)) {
                throw new AppException("error.aelUser.import.duplicateEmail",new Object[]{user.getEmail()});
            } else if (!aelUserService.validateUsername(user)) {
                throw new AppException("error.aelUser.import.duplicateUsername", new Object[]{user.getUsername()});
            }

            AelUser newUser = aelUserService.save(user);
            if(newUser.getId() != null)
            {
               saveCujForUser(newUser, componentName, jobTitle);
            }

        }
    }
    
    private void saveCujForUser(AelUser newUser, String componentName, String jobTitleName)
    {
        Component component = componentService.findByNameAndTenant(componentName, newUser.getTenant());
        if(component != null)
        {
            JobTitle jobTitle = jobTitleService.findByNameAndTenant(jobTitleName, newUser.getTenant());
            if(jobTitle != null)
            {
                ComponentUserJob componentUserJob = new ComponentUserJob();
                componentUserJob.setUser(newUser);
                componentUserJob.setComponent(component);
                componentUserJob.setJobTitle(jobTitle);
                componentUserJob.setActive(Boolean.TRUE);
                componentUserJob.setImported(Boolean.TRUE);
                componentUserJobService.saveOrUpdate(componentUserJob);
            }
        }
    }



}
