package ro.siveco.ael.lcms.domain.metadata.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.metadata.model.Relation;
import ro.siveco.ael.lcms.domain.metadata.service.RelationService;
import ro.siveco.ram.event.listener.type.single.BaseBeforeCreateListener;
import ro.siveco.ram.event.type.single.BeforeCreateEvent;

/**
 * Created by IuliaP on 13.10.2017.
 */
@Component
@Order(value = 1)
public class ValidateRelationBeforeCreateListener extends BaseBeforeCreateListener<Relation> {

    @Autowired
    RelationService relationService;

    @Override
    public void onApplicationEvent(BeforeCreateEvent<Relation> event) {
        relationService.validateRelation( event.getDomainModel() );
    }
}
