package ro.siveco.ael.lcms.domain.metadata.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.siveco.ael.lcms.domain.auth.model.AelUser;
import ro.siveco.ael.lcms.domain.auth.service.AelUserService;
import ro.siveco.ael.lcms.domain.metadata.model.*;
import ro.siveco.ael.lcms.repository.TagRepository;
import ro.siveco.ael.web.controllers.WithUserBaseEntityController;
import ro.siveco.ram.app.exception.AppException;
import ro.siveco.ram.starter.service.BaseEntityService;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by IuliaP on 09.06.2017.
 */
@Service
public class TagService extends BaseEntityService<Tag> {

    @Autowired
    public TagService(TagRepository tagRepository) {
        super(Tag.class, tagRepository);
    }

    @Autowired
    private AelUserService aelUserService;

    @Autowired
    private RelationService relationService;

    @Autowired
    private LanguageService languageService;

    private final String EQUALS_RELATION_NAME = "EQUALS";

    public List<Tag> findAllByValue(String value) {
        return ((TagRepository)getResourceRepository()).findAllByValue(value);
    }

    public List<Tag> findAllByLanguage(Language language) {
        return ((TagRepository)getResourceRepository()).findAllByLanguage(language);
    }

    public List<Tag> findAllByValueAndLanguage(String value, Language language) {
        return ((TagRepository)getResourceRepository()).findAllByValueAndLanguage(value, language);
    }

    public Tag findOne(Long id) {
        return getResourceRepository().findOne(id);
    }

    public List<Tag> findAllWithoutMetadata(Long entityId, EntityType entityType) {
        return ((TagRepository)getResourceRepository()).findAllWithoutMetadata(entityId,entityType);
    }

    public void validateTag(Tag tag) {
        if( tag != null ) {
            List<Tag> tags = ((TagRepository)getResourceRepository()).findAllByValue( tag.getValue() );
            if( tags != null && tags.size() > 0 ) {
                throw new AppException("error.tag.duplicateValue");
            }
        }
    }

    public String getI18NTagName(Tag tag, String language) {
        if (language != null && tag != null) {
            if (language != null) {
                List<Language> languages = languageService.findAllByName(language);
                if (languages != null && languages.size() > 0 ) {
                    Language dbLanguage = languages.get(0);
                    List<Relation> relations = relationService.findAllByName(EQUALS_RELATION_NAME);
                    if (relations != null && relations.size() > 0) {
                        Relation relation = relations.get(0);
                        List<Tag> dbTags = findAllByTagAndRelationAndLanguage(tag.getId(), relation.getId(), dbLanguage.getId());
                        if (dbTags != null && dbTags.size() > 0) {
                            return dbTags.get(0).getValue();
                        }
                    }
                }
                else {
                    return tag.getValue();
                }
            }
        }
        return null;
    }

    public List<Tag> getAllTagsByEntityIdTypeAndRelation(Long entityId, EntityType entityType, RelationType relationType)
    {
        List<Tag> tags = ((TagRepository)getResourceRepository()).getAllTagsByEntityIdTypeAndRelation(entityId,entityType,relationType);
        if (tags != null) {
            AelUser currentUser = (AelUser) WithUserBaseEntityController.getUser();
            String language = aelUserService.getLocaleForUser(currentUser).getLanguage();
            tags.forEach(tag -> tag.setDisplayName(getI18NTagName(tag,language)));
        }
        return tags;
    }

    public List<Tag> getTags(List<Metadata> metadataList) {

        List<Tag> tags = new ArrayList<>();
        for (Metadata metadata : metadataList) {
            tags.add(metadata.getTag());
        }
        return tags;
    }

    public List<Tag> findAllByTagAndRelationAndLanguage(Long tagId, Long relationId, Long languageId) {
        return ((TagRepository)getResourceRepository()).findAllByTagAndRelationAndLanguage(tagId, relationId, languageId);
    }

    public List<Tag> findAllWithoutMetadataWithInterest(Long entityId, EntityType entityType) {
        return ((TagRepository)getResourceRepository()).findAllWithoutMetadataWithInterest(entityId, entityType);
    }

    public void setTagDisplayName(Tag tag, String language) {
        if (tag != null && language != null) {
            tag.setDisplayName(getI18NTagName(tag, language));
        }
    }
}

