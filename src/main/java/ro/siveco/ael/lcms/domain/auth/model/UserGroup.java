package ro.siveco.ael.lcms.domain.auth.model;

import ro.siveco.ram.starter.model.base.BaseEntity;
import ro.siveco.ram.starter.ui.config.ActionDescriptor;
import ro.siveco.ram.starter.ui.config.PropertyViewDescriptor;
import ro.siveco.ram.starter.ui.config.ViewDescriptor;
import ro.siveco.ram.starter.ui.model.ActionType;
import ro.siveco.ram.starter.ui.model.FormInputType;
import ro.siveco.ram.starter.ui.model.ViewRendererType;

import javax.persistence.*;
import java.util.List;

/**
 * User: AlexandruVi
 * Date: 2018-01-22
 */
@Entity
@Table(name = "users")
@ViewDescriptor(
        defaultActions = {
                @ActionDescriptor(type = ActionType.OPEN_EDIT_FORM),
                @ActionDescriptor(type = ActionType.UPDATE, forwardFor = "{modelName}-OPEN_EDIT_FORM",
                        availableOnlyInForm = true),
                @ActionDescriptor(type = ActionType.LIST, availableInForm = true,
                        modelName = "aelUser", backToFor = {
                        "userGroup-OPEN_EDIT_FORM",
                        "userGroup-UPDATE"
                })
        }
)
public class UserGroup extends BaseEntity {

    private List<AelGroup> groups;

    private String username;

    @Override
    @Id
    @PropertyViewDescriptor(hidden = true, hiddenOnForm = true, required = false)
    public Long getId() {

        return super.getId();
    }

    @PropertyViewDescriptor(
            order = -5, rendererType = ViewRendererType.STRING,
            orderInForm = -5, inputType = FormInputType.TEXT, readonly = true, disabled = true
    )
    @Column(name = "username", unique = true, nullable = false, length = 500)
    public String getUsername() {

        return username;
    }

    @ManyToMany(
            cascade = {CascadeType.PERSIST, CascadeType.MERGE},
            targetEntity = AelGroup.class
    )
    @JoinTable(
            name = "group_user",
            joinColumns = {@JoinColumn(name = "user_id")},
            inverseJoinColumns = {@JoinColumn(name = "group_id")}
    )
    @PropertyViewDescriptor(hiddenOnPresentation = true,hiddenOnCard = true,hiddenOnCreate = true,hiddenOnGrid = true,hiddenOnList = true)
    public List<AelGroup>getGroups(){
        return groups;
    }

    public void setGroups(List<AelGroup> groups) {

        this.groups = groups;
    }

    public void setUsername(String username) {

        this.username = username;
    }
}
