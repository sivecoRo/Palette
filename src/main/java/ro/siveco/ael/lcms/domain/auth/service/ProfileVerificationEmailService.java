package ro.siveco.ael.lcms.domain.auth.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import ro.siveco.ael.lcms.domain.auth.model.AelUser;
import ro.siveco.ael.lcms.domain.preference.service.PreferencesService;
import ro.siveco.ael.service.BaseEmailService;
import ro.siveco.ael.service.mail.EmailService;
import ro.siveco.ael.web.components.WebPath;

import java.net.URLEncoder;
import java.util.Locale;

/**
 * Service for sending profile verification emails.
 *
 * @author maciejb
 * @author mikolajb
 * @since 2019-03-15
 */
@Service
public class ProfileVerificationEmailService extends BaseEmailService {

    private EmailService emailService;

    @Autowired
    public ProfileVerificationEmailService(EmailService emailService, PreferencesService preferencesService) {

        super(preferencesService);

        Assert.notNull(emailService, "Supplied EmailService is null.");

        this.emailService = emailService;
    }


    /**
     * Send email with profile verification link.
     *
     * @param user  user to send the email to
     * @param token verification token
     * @throws Exception in case of an error
     */
    public void sendProfileVerificationEmail(AelUser user, String token) throws Exception {

        Assert.notNull(user, "Can't send profile verification email for null user.");
        Assert.notNull(token, "Can't send profile verification email for null token.");

        Locale locale = getUserPreferredLocale(user);
        String subject = getLocalizedSubject(locale);

        String baseUrl = getBaseUrl();
        String verificationLinkUrl = getVerificationUrl(baseUrl, token);
        // There are two templates: one, general, for emails (the same for notifications, verification, registration),
        // and the second, lower level, for information that is sent during verification procedure.
        // Below the 1-st level template is filled with the verification link.
        String localizedBodyText = getLocalizedVerificationTemplate(locale, verificationLinkUrl);

        // The 2-nd level template is wrapped around verification information.
        String body = putMessageIntoLayout(user, localizedBodyText);

        emailService.sendEmail(
                user,
                subject,
                body);
    }


    /**
     * Generate the verification URL based on the <code>baseUrl</code> and <code>token</code>
     *
     * @param baseUrl base URL
     * @param token   verification token
     * @return verification URL
     * @throws Exception if there's a problem encoding URL-encoding the token
     */
    private String getVerificationUrl(String baseUrl, String token) throws Exception {
        return baseUrl + WebPath.PROFILE_VERIFICATION + "?token=" + URLEncoder.encode(token, "UTF-8");
    }


    /**
     * Get localized text for the email subject
     *
     * @param locale locale to use
     * @return localized subject text
     */
    private String getLocalizedSubject(Locale locale) {
        return getMessageSource().getMessage("profile.verification.email.subject", null, locale);
    }

    /**
     * Get localized text for the email body
     *
     * @param locale            locale to use
     * @param activationLinkUrl profile activation link URL
     * @return localized body content
     */
    private String getLocalizedVerificationTemplate(Locale locale, String activationLinkUrl) {
        Object[] params = {activationLinkUrl};

        return getMessageSource().getMessage("profile.verification.email.body.template", params, locale);
    }

}
