package ro.siveco.ael.lcms.domain.metadata.service;

import com.querydsl.core.types.Predicate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ro.siveco.ael.lcms.domain.metadata.model.Country;
import ro.siveco.ael.lcms.repository.CountryRepository;
import ro.siveco.ram.security.annotation.CanListEntity;
import ro.siveco.ram.starter.service.BaseEntityService;

import java.util.List;

/**
 * Created by albertb on 2/8/2018.
 */

@Service
public class CountryService extends BaseEntityService<Country> {

    @Autowired
    public CountryService(CountryRepository countryRepository) {
        super(Country.class, countryRepository);
    }

    public List<Country> getAllCountries()
    {
        List<Country> countries = getResourceRepository().findAll();
        if (countries != null) {
            countries.forEach(country -> country.setDisplayName(country.getCountryName()));
        }
        return countries;
    }

    public List<Country> getAllCountries(String language)
    {
        List<Country> countries = getResourceRepository().findAll();
        if (countries != null) {
            countries.forEach(country -> setDisplayName(country, language));
        }
        return countries;
    }

    public List<Country> findAllByCountryName(String countryName) {
        return ((CountryRepository)getResourceRepository()).findAllByCountryName(countryName);
    }

    public List<Country> findAllByCountryCode(String countryCode) {
        return ((CountryRepository)getResourceRepository()).findAllByCode(countryCode);
    }


    public Country setDisplayName(Country country, String language) {
        if (country != null && language != null) {
            switch (language.toLowerCase()) {
                case "en" : country.setDisplayName(country.getNameEn()); break;
                case "ro" : country.setDisplayName(country.getNameRo()); break;
                case "pl" : country.setDisplayName(country.getNamePl()); break;
                case "fr" : country.setDisplayName(country.getNameFr()); break;
                case "nl" : country.setDisplayName(country.getNameNl()); break;
                case "sl" : country.setDisplayName(country.getNameSl()); break;
                default : country.setDisplayName(country.getNameEn());
            }
        }
        return country;
    }

    @CanListEntity
    public Page<Country> paginate(Predicate predicate, Pageable pageable) {
        Page<Country> page = this.getResourceRepository().findAll(this.revisePageable(pageable));
        page.forEach(this::triggerAfterRead);
        return page;
    }
}
