package ro.siveco.ael.lcms.domain.course.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.course.model.PlannedCourse;
import ro.siveco.ael.lcms.domain.course.service.PlannedCourseService;
import ro.siveco.ram.event.listener.type.single.BaseAfterReadListener;
import ro.siveco.ram.event.type.single.AfterReadEvent;

/**
 * Created by IuliaP on 11.09.2017.
 */
@Component
public class SetPlannedCourseDetailsAfterReadListener extends BaseAfterReadListener<PlannedCourse> {

    private final PlannedCourseService plannedCourseService;

    @Autowired
    public SetPlannedCourseDetailsAfterReadListener(PlannedCourseService plannedCourseService) {

        this.plannedCourseService = plannedCourseService;
    }

    @Override
    public void onApplicationEvent(AfterReadEvent<PlannedCourse> event) {

        PlannedCourse plannedCourse = event.getDomainModel();

        plannedCourseService.decorateCourseSession(plannedCourse);
    }
}
