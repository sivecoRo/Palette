package ro.siveco.ael.lcms.domain.auth.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.siveco.ael.lcms.domain.auth.model.AelGroup;
import ro.siveco.ael.lcms.domain.auth.model.Group;
import ro.siveco.ael.lcms.domain.auth.model.MultiSelectDTO;
import ro.siveco.ael.lcms.domain.tenant.model.Tenant;
import ro.siveco.ael.lcms.repository.AelGroupRepository;
import ro.siveco.ram.app.exception.AppException;
import ro.siveco.ram.starter.service.BaseEntityService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by AndradaC on 5/29/2017.
 */
@Service
public class AelGroupService extends BaseEntityService<AelGroup> {

    @Autowired
    public AelGroupService(AelGroupRepository aelGroupRepository) {
        super(AelGroup.class, aelGroupRepository);
    }

    public AelGroup getByName(String name) {
        return ((AelGroupRepository) getResourceRepository()).findByName(name);
    }

    public AelGroup findById(Long id) {
        return ((AelGroupRepository) getResourceRepository()).findById(id);
    }

    public List<AelGroup> findAll() {
        return ((AelGroupRepository) getResourceRepository()).findAll();
    }

    public List<Group> findAllGroup() {
        return ((AelGroupRepository) getResourceRepository()).findAllGroup();
    }

    public List<AelGroup> findByidGroups(List<Long> idGroups) {
        return ((AelGroupRepository) getResourceRepository()).findByIdGroups(idGroups);
    }

    public List<AelGroup> findByidGroups(String nameGroups) {
        return ((AelGroupRepository) getResourceRepository()).findByNameGroups(nameGroups);
    }

    public List<AelGroup> findAllByTenant(Tenant tenant) {
        return ((AelGroupRepository) getResourceRepository()).findAllByTenant(tenant);
    }

    public List<Group> findTenant(Tenant tenant) {
        return ((AelGroupRepository) getResourceRepository()).findTenant(tenant);
    }

    public AelGroup findByNameAndTenant(String name, Tenant tenant) {
        return ((AelGroupRepository) getResourceRepository()).findByNameAndTenant(name, tenant);
    }

    public void validateGroup(Group group){
        if(group.getName() == null || "".equals(group.getName())){
            throw new AppException("error.group.null");
        }
        AelGroup existingGroup = ((AelGroupRepository) getResourceRepository()).findByName(group.getName());
        if(existingGroup != null){
            if (group.isNew() || group.getId().compareTo(existingGroup.getId()) != 0) {
                throw new AppException("error.group.exists");
            }
        }
    }
}
