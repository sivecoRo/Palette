package ro.siveco.ael.lcms.domain.auth.model;

import ro.siveco.ram.model.Entity;

import java.util.List;

/**
 * User: AlexandruVi
 * Date: 2016-12-07
 */
public interface Role extends Entity<Long> {
    String getName();

    List<AelFunction> getFunctions();
}
