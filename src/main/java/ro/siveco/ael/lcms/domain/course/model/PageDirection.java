package ro.siveco.ael.lcms.domain.course.model;

/**
 * Created by LiviuI on 10/7/2015.
 */
public enum PageDirection {
    LANDSCAPE,
    PORTRAIT
}
