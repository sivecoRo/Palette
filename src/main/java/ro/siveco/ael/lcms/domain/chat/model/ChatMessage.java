package ro.siveco.ael.lcms.domain.chat.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table( name = "chat_messages" )
@SequenceGenerator( name = "chat_messages_seq", sequenceName = "chat_messages_seq" )
public class ChatMessage extends ro.siveco.ram.starter.model.base.BaseEntity {

	
	private Long id;
	
	private String sender;
	private String receiver;
	private String message;
	
	private Date creationDate;
	

	@Id
	@GeneratedValue( generator = "chat_messages_seq", strategy = GenerationType.SEQUENCE )
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column( nullable = false, length = 500 )
	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	@Column( nullable = false, length = 500 )
	public String getReceiver() {
		return receiver;
	}

	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}

	@Column( length = 2000 )
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Column( name = "creation_date", nullable = false )
	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
}
