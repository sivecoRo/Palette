package ro.siveco.ael.lcms.domain.tenant.model;

/**
 * User: AlexandruVi
 * Date: 2017-04-26
 */
public interface TenantAndCreatorHolder extends TenantHolder, TenantCreatorUser {
}
