package ro.siveco.ael.lcms.domain.auth.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.auth.model.AelGroup;
import ro.siveco.ael.lcms.domain.auth.model.User;
import ro.siveco.ael.lcms.domain.auth.service.AelGroupService;
import ro.siveco.ael.lcms.domain.tenant.model.Tenant;
import ro.siveco.ael.web.controllers.WithUserBaseEntityController;
import ro.siveco.ram.app.exception.AppException;
import ro.siveco.ram.event.listener.type.single.BaseBeforeCreateListener;
import ro.siveco.ram.event.type.single.BeforeCreateEvent;

/**
 * Created by AndradaC on 9/1/2017.
 */
@Component
public class ValidateGroupAndSetInformationBeforeCreateListener extends BaseBeforeCreateListener<AelGroup> {

    @Autowired
    private AelGroupService aelGroupService;

    @Override
    public void onApplicationEvent(BeforeCreateEvent<AelGroup> event) {

        AelGroup aelGroup = event.getDomainModel();
        User currentUser = WithUserBaseEntityController.getUser();
        Tenant tenant = currentUser.getTenant();

        aelGroupService.validateGroup(aelGroup);

        aelGroup.setTenant(tenant);
        aelGroup.setRelatedUid(aelGroup.getName());
    }
}
