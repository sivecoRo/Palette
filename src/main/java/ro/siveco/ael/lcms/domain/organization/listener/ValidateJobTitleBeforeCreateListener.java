package ro.siveco.ael.lcms.domain.organization.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.organization.model.JobTitle;
import ro.siveco.ael.lcms.domain.organization.service.JobTitleService;
import ro.siveco.ram.app.exception.AppException;
import ro.siveco.ram.event.listener.type.single.BaseBeforeCreateListener;
import ro.siveco.ram.event.type.single.BeforeCreateEvent;

/**
 * Created by AndradaC on 9/3/2017.
 */
@Component
public class ValidateJobTitleBeforeCreateListener extends BaseBeforeCreateListener<JobTitle> {

    @Autowired
    private JobTitleService jobTitleService;

    @Override
    public void onApplicationEvent(BeforeCreateEvent<JobTitle> event) {
        JobTitle jobTitle = event.getDomainModel();

        if(!jobTitleService.validateJobTitle(jobTitle)){
            throw new AppException("error.jobTitle.create.duplicateEntry");
        }

    }
}
