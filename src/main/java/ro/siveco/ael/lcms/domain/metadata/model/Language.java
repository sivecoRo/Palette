package ro.siveco.ael.lcms.domain.metadata.model;

import org.springframework.data.domain.Sort;
import ro.siveco.ram.starter.ui.config.PropertyViewDescriptor;
import ro.siveco.ram.starter.ui.config.Sorter;
import ro.siveco.ram.starter.ui.config.ViewDescriptor;
import ro.siveco.ram.starter.ui.model.IndexViewType;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by IuliaP on 15.06.2017.
 */
@Entity
@Table(name = "languages")
@SequenceGenerator(name = "languages_seq", sequenceName = "languages_seq")
@ViewDescriptor(pageSize = 10, viewTypes = {IndexViewType.GRID}, searchEnabled = true, defaultSearchProperty = "name",
        sort = {@Sorter(propertyName = "name", direction = Sort.Direction.ASC)})
public class Language extends ro.siveco.ram.starter.model.base.BaseEntity {

    private String name;

    @Id
    @GeneratedValue(generator = "languages_seq", strategy = GenerationType.AUTO)
    public Long getId() {
        return super.getId();
    }

    @Column(name = "name", nullable = false)
    @NotNull
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    @PropertyViewDescriptor(hidden = true)
    @Transient
    public String getObjectLabel() {
        return getName();
    }
}
