package ro.siveco.ael.lcms.domain.auth.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.auth.model.AelFunction;
import ro.siveco.ael.lcms.domain.auth.model.AelRole;
import ro.siveco.ael.lcms.domain.auth.model.MultiSelectDTO;
import ro.siveco.ael.lcms.domain.auth.service.AelFunctionService;
import ro.siveco.ram.event.listener.type.single.BaseAfterReadListener;
import ro.siveco.ram.event.type.single.AfterReadEvent;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by AndradaC on 9/1/2017.
 */
@Component
public class SetMultiselectFunctionListForRoleAfterReadListener extends BaseAfterReadListener<AelRole>{

    @Autowired
    AelFunctionService aelFunctionService;

    @Override
    public void onApplicationEvent(AfterReadEvent<AelRole> event) {
        AelRole aelRole = event.getDomainModel();

        List<AelFunction> aelFunctionList = aelFunctionService.getAelFunctions();
        List<MultiSelectDTO> multiSelectDTOs = new ArrayList<>();

        for (AelFunction aelFunction : aelFunctionList) {
            MultiSelectDTO multiSelectDTO = new MultiSelectDTO();
            multiSelectDTO.setId(aelFunction.getId());
            multiSelectDTO.setName(aelFunction.getName());
            multiSelectDTO.setStatus(0L);
            multiSelectDTOs.add(multiSelectDTO);
        }

        aelRole.setListAllFunction(multiSelectDTOs);
 //       aelRole.setFunctions(aelFunctionList);
    }
}
