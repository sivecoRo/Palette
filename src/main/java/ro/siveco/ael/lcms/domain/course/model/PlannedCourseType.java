package ro.siveco.ael.lcms.domain.course.model;

/**
 * User: IoanB
 * Date: May 28, 2007
 * Time: 4:13:20 PM
 */
public enum PlannedCourseType {
    //synchronous course
    SYNCHRONOUS,
    // asinchronous course (self_registration = false)
    ASYNCHRONOUS_SRF,
    // asinchronous course (self_registration = true)
    ASYNCHRONOUS_SRT    
}
