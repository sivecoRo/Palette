package ro.siveco.ael.lcms.domain.metadata.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.metadata.model.RelationTag;
import ro.siveco.ael.lcms.domain.metadata.service.RelationTagService;
import ro.siveco.ram.event.listener.type.single.BaseBeforeCreateListener;
import ro.siveco.ram.event.type.single.BeforeCreateEvent;

/**
 * Created by IuliaP on 13.10.2017.
 */
@Component
@Order(value = 1)
public class ValidateRelationTagBeforeCreateListener extends BaseBeforeCreateListener<RelationTag>{

    @Autowired
    RelationTagService relationTagService;

    @Override
    public void onApplicationEvent(BeforeCreateEvent<RelationTag> event) {
        relationTagService.validateRelationTag( event.getDomainModel() );
    }
}
