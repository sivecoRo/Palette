package ro.siveco.ael.lcms.domain.materials.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.hibernate.annotations.Cascade;
import ro.siveco.ael.qti.domain.model.InteractionType;
import ro.siveco.ram.starter.ui.config.PropertyViewDescriptor;
import ro.siveco.ram.starter.ui.model.FormInputType;
import ro.siveco.ram.starter.ui.model.ViewRendererType;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: razvanni
 * Date: 31.05.2010
 * Time: 16:54:57
 * To change this template use File | Settings | File Templates.
 */

@Entity
@Table( name = "survey_item")
@SequenceGenerator( name = "SURVEY_ITEM_SEQ", sequenceName = "survey_item_seq" )
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class SurveyItem extends ro.siveco.ram.starter.model.base.BaseEntity implements Serializable
{

	private String code;
	private String definition;
	private SurveyItemType itemType;
	private SelectionMethod selectionMethod;
    @Column(length = 1000)
	private String scoreCalculation;

	private List<SurveyItemValue> surveyItemValues;

	private Long order = 1L;

	@JsonIgnore
	private Survey survey;

	private InteractionType interactionType;

	private Integer sliderMinVal;

	private Integer sliderMaxVal;

	@Id
	@GeneratedValue( generator = "SURVEY_ITEM_SEQ", strategy = GenerationType.AUTO )
	public Long getId()
	{
		return super.getId();
	}

	@Column( name = "CODE" )
	@PropertyViewDescriptor(ignore = true)
	public String getCode()
	{
		return code;
	}

	public void setCode( String code )
	{
		this.code = code;
	}

	@Column( name = "DEFINITION" )
	@PropertyViewDescriptor(inputType = FormInputType.TEXT, rendererType = ViewRendererType.STRING,
            editInline = false, order = 2, required = true)
    public String getDefinition()
	{
		return definition;
	}

	public void setDefinition( String definition )
	{
		this.definition = definition;
	}

	@Column( name = "ITEM_TYPE" )
	@PropertyViewDescriptor(ignore = true)
	public SurveyItemType getItemType()
	{
		return itemType;
	}

	public void setItemType( SurveyItemType itemType )
	{
		this.itemType = itemType;
	}

	@Column( name = "SELECTION_METHOD" )
	@PropertyViewDescriptor(ignore = true)
	public SelectionMethod getSelectionMethod()
	{
		return selectionMethod;
	}

	public void setSelectionMethod( SelectionMethod selectionMethod )
	{
		this.selectionMethod = selectionMethod;
	}

	@Column( name = "SCORE_CALCULATION" )
	@PropertyViewDescriptor(ignore = true)
	public String getScoreCalculation()
	{
		return scoreCalculation;
	}

	public void setScoreCalculation( String scoreCalculation )
	{
		this.scoreCalculation = scoreCalculation;
	}

	@Column( name = "ITEM_ORDER" )
	@PropertyViewDescriptor(ignore = true)
	public Long getOrder()
	{
		return order;
	}

	public void setOrder( Long order )
	{
		this.order = order;
	}

	@ManyToOne
	@JoinColumn( name = "ID_SURVEY", nullable = false )
	@PropertyViewDescriptor(ignore = true)
	public Survey getSurvey()
	{
		return survey;
	}

	public void setSurvey( Survey survey )
	{
		this.survey = survey;
	}

	@OneToMany( mappedBy = "surveyItem", cascade = {CascadeType.ALL} )
	@Cascade( {org.hibernate.annotations.CascadeType.DELETE_ORPHAN} )
    @OrderBy("order ASC")
    @PropertyViewDescriptor(order = 4, rendererType = ViewRendererType.VALUE_OBJECT_MODEL_COLLECTION)
    public List<SurveyItemValue> getSurveyItemValues() {

        return surveyItemValues;
    }

    public void setSurveyItemValues(List<SurveyItemValue> surveyItemValues) {
        this.surveyItemValues = surveyItemValues;
    }

	@Transient
	@PropertyViewDescriptor(ignore = true)
	public Long getIdEntitate()
	{
		return getId();
	}

	public void setIdEntitate( Long idEntitate )
	{
		setId( idEntitate );
	}

	@Transient
    @PropertyViewDescriptor(order = 3)
    public InteractionType getInteractionType() {

        return interactionType;
    }

	public void setInteractionType(InteractionType interactionType) {
		this.interactionType = interactionType;
	}

	@Transient
    @PropertyViewDescriptor(rendererType = ViewRendererType.INTEGER,
            editInline = false, order = 3, required = true)
    public Integer getSliderMinVal() {
		return sliderMinVal;
	}

	public void setSliderMinVal(Integer sliderMinVal) {
		this.sliderMinVal = sliderMinVal;
	}

	@Transient
    @PropertyViewDescriptor(rendererType = ViewRendererType.INTEGER,
            editInline = false, order = 4, required = true)
    public Integer getSliderMaxVal() {
		return sliderMaxVal;
	}

	public void setSliderMaxVal(Integer sliderMaxVal) {
		this.sliderMaxVal = sliderMaxVal;
	}


	@Override
	public boolean equals( Object o )
	{
		if( this == o )
		{
			return true;
		}
		if( o == null || getClass() != o.getClass() )
		{
			return false;
		}

		SurveyItem that = ( SurveyItem ) o;

		if( getId() != null ? !getId().equals( that.getId() ) : that.getId() != null )
		{
			return false;
		}

		return true;
	}

	@Override
	public int hashCode()
	{
		return getId() != null ? getId().hashCode() : 0;
	}

}
