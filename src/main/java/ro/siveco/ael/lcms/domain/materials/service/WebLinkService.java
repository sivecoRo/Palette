package ro.siveco.ael.lcms.domain.materials.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.siveco.ael.lcms.domain.materials.model.WebLink;
import ro.siveco.ael.lcms.repository.WebLinkRepository;
import ro.siveco.ram.repository.BaseEntityRepository;
import ro.siveco.ram.starter.service.BaseEntityService;

/**
 * Created by AndradaC on 5/5/2017.
 */
@Service
public class WebLinkService extends BaseEntityService<WebLink>
{
    @Autowired
    public WebLinkService(BaseEntityRepository<WebLink> resourceRepository) {
        super(WebLink.class, resourceRepository);
    }

    public WebLink findOneById(Long id)
    {
        return ((WebLinkRepository) getResourceRepository()).findOneById(id);
    }

    public WebLink findOneByLearningMaterialNodeId(String nodeId)
    {
        return ((WebLinkRepository) getResourceRepository()).findOneByLearningMaterialNodeId(nodeId);
    }

    public WebLink saveOrUpdate( WebLink webLink )
    {
        if(webLink.getId() != null)
        {
            this.update(webLink);
        }
        else
        {
            this.save(webLink);
        }
        return webLink;
    }

}
