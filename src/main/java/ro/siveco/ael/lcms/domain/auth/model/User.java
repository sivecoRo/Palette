package ro.siveco.ael.lcms.domain.auth.model;

import org.springframework.util.StringUtils;
import ro.siveco.ael.domain.model.core.Record;
import ro.siveco.ael.lcms.domain.tenant.model.TenantHolder;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * User: AlexandruVi
 * Date: 2016-12-07
 */
public interface User extends Record<Long>, TenantHolder, Serializable {

    List<Role> getRoles();

    String getPassword();

    List<AelGroup> getGroups();

    String getUsername();

    String getUserSocialId();

    Boolean getDisabled();

    Date getExpireDate();

    default String getAlwaysNonEmptyDisplayName() {
        String alwaysNonEmptyDisplayName = getDisplayName();
        if (alwaysNonEmptyDisplayName == null) {
            if (!StringUtils.isEmpty(getUsername())) {
                alwaysNonEmptyDisplayName = getUsername();
            } else {
                alwaysNonEmptyDisplayName = "User #" + getId();
            }
        }
        return alwaysNonEmptyDisplayName;
    }

    default String getDisplayName() {
        return "";
    }

    default String getRelatedUid() {
        return "";
    }

    default Locale getLocale() {
        return new Locale("en", "US");
    }

    String getEmail();

    String getAvatarSrc();

    String getInitials();
}
