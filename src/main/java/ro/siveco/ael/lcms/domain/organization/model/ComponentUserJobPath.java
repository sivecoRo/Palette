package ro.siveco.ael.lcms.domain.organization.model;

import ro.siveco.ram.starter.ui.config.PropertyViewDescriptor;
import ro.siveco.ram.starter.ui.config.ViewDescriptor;
import ro.siveco.ram.starter.ui.model.FormInputType;
import ro.siveco.ram.starter.ui.model.IndexViewType;
import ro.siveco.ram.starter.ui.model.ViewRendererType;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table( name = "component_user_job_paths" )

@SequenceGenerator( name = "component_user_job_paths_seq", sequenceName = "component_user_job_paths_seq" )
@ViewDescriptor(paginationEnabled = true, viewTypes = {IndexViewType.LIST, IndexViewType.GRID, IndexViewType.CARD}, defaultViewType = IndexViewType.LIST,
        searchEnabled = true)
public class ComponentUserJobPath extends ro.siveco.ram.starter.model.base.BaseEntity
{
	private Long id;
	private ComponentUserJob componentUserJob;
	private Date insertedAtDate;
	private Date removedAtDate;
	private ComponentUserJobStatus status;
	private ComponentUserJobPath previousComponentUserJobPath;
	private ComponentUserJobPath nextComponentUserJobPath;

	@Id
	@GeneratedValue( generator = "component_user_job_paths_seq", strategy = GenerationType.AUTO )
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne
	@JoinColumn(name = "next_id", nullable = true )
	@PropertyViewDescriptor(hidden = true, hiddenOnForm = true)
	public ComponentUserJobPath getNextComponentUserJobPath() {
		return nextComponentUserJobPath;
	}

	public void setNextComponentUserJobPath(ComponentUserJobPath nextComponentUserJobPath) {
		this.nextComponentUserJobPath = nextComponentUserJobPath;
	}

	@ManyToOne
	@JoinColumn(name = "previous_id", nullable = true )
	@PropertyViewDescriptor(hidden = true, hiddenOnForm = true)
	public ComponentUserJobPath getPreviousComponentUserJobPath() {
		return previousComponentUserJobPath;
	}

	public void setPreviousComponentUserJobPath(ComponentUserJobPath previousComponentUserJobPath) {
		this.previousComponentUserJobPath = previousComponentUserJobPath;
	}

	@Column(name = "status" )
	@PropertyViewDescriptor(inputType = FormInputType.TEXT, rendererType = ViewRendererType.STRING )
	public ComponentUserJobStatus getStatus() {
		return status;
	}

	public void setStatus(ComponentUserJobStatus status) {
		this.status = status;
	}

	@Column(name = "removed_at_date" )
	@PropertyViewDescriptor(hiddenOnForm = true, rendererType = ViewRendererType.DATETIME)
	public Date getRemovedAtDate() {
		return removedAtDate;
	}

	public void setRemovedAtDate(Date removedAtDate) {
		this.removedAtDate = removedAtDate;
	}

	@Column(name = "inserted_at_date" )
	@PropertyViewDescriptor(hiddenOnForm = true, rendererType = ViewRendererType.DATETIME)
	public Date getInsertedAtDate() {
		return insertedAtDate;
	}

	public void setInsertedAtDate(Date insertedAtDate) {
		this.insertedAtDate = insertedAtDate;
	}

	@ManyToOne
	@JoinColumn(name = "component_user_job_id", nullable = false )
	@PropertyViewDescriptor(inputType = FormInputType.SELECT, rendererType = ViewRendererType.MODEL,
            editInline = false, hidden = false, required = true, hiddenOnForm = false)
    public ComponentUserJob getComponentUserJob() {
		return componentUserJob;
	}

	public void setComponentUserJob(ComponentUserJob componentUserJob) {
		this.componentUserJob = componentUserJob;
	}

	@Override
	public String toString() {
		return this.getComponentUserJob().getJobTitle().getName();
	}
}