package ro.siveco.ael.lcms.domain.materials.service;

import org.springframework.stereotype.Service;
import ro.siveco.ael.lcms.domain.materials.model.UserSurveyAnswer;
import ro.siveco.ram.repository.BaseEntityRepository;
import ro.siveco.ram.starter.service.BaseEntityService;

/**
 * Created by IuliaP on 02.11.2017.
 */
@Service
public class UserSurveyAnswerService extends BaseEntityService<UserSurveyAnswer> {

    public UserSurveyAnswerService(BaseEntityRepository<UserSurveyAnswer> resourceRepository) {

        super(UserSurveyAnswer.class, resourceRepository);
    }
}
