package ro.siveco.ael.lcms.domain.menu.model;

public enum MenuRenderType {
    MENU, // renders in main navigation menu
    TAB,  // renders in a page as a tab
    SIDE  // renders in a page as a side navigation item
}