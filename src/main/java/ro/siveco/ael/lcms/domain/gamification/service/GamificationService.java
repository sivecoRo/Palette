package ro.siveco.ael.lcms.domain.gamification.service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import ro.siveco.ael.lcms.domain.gamification.model.Container;
import ro.siveco.ael.lcms.domain.gamification.model.ContainerResult;
import ro.siveco.ael.lcms.domain.gamification.model.UserCurrentStatus;
import ro.siveco.ael.service.BaseService;
import ro.siveco.ael.service.ServiceException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by LucianR on 23.10.2017.
 */

@Service
public class GamificationService extends BaseService {

    @Value("${gamification.enable}")
    private boolean enable;

    @Value("${gamification.schema}")
    private String schema;

    @Value("${gamification.endpoint}")
    private String endpoint;

    @Value("${gamification.authKey}")
    private String authKey;

    @Value("${gamification.token}")
    private String token;

    public static final String query_path = "/console";

    public static final String gengine_path = "/gengine";

    public List<Container> getContainers() {

        List<Container> containerList = new ArrayList<>();
        if (!enable) {
            return containerList;
        }

        try {
            String response = executeHttpGet(query_path, null);
            if (response != null) {

                ObjectMapper mapper = new ObjectMapper();
                mapper.enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);

                ContainerResult containerResult = mapper.readValue(response,
                        ContainerResult.class);
                containerList = containerResult.getList();
            }
        } catch (Exception e) {
            throw new ServiceException(e.getMessage());
        }

        return containerList;
    }

    public Container getContainer(String containerId) {

        Container container = null;
        if (!enable) {
            return null;
        }

        try {
            String response = executeHttpGet(query_path + "/" + containerId, null);
            if (response != null) {

                ObjectMapper mapper = new ObjectMapper();
                mapper.enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);

                container = mapper.readValue(response,
                        Container.class);
            }
        } catch (Exception e) {
            throw new ServiceException(e.getMessage());
        }

        return container;

    }

    public String setUserAction(String registrationId, String containerId, String actionId) {
        //TODO objectId
        String jsonBody = "{\"gameId\":\"" + containerId +
                "\",\"userId\": \"" + registrationId +
                "\",\"actionId\": \"" + actionId + "\"}";
        String ret = null;
        try {
            ret = executeHttpPost(gengine_path + "/execute", jsonBody);
        } catch (Exception e) {
            throw new ServiceException(e.getMessage());
        }
        return ret;
    }

    public UserCurrentStatus getUserCurrentStatus(String containerId, String userId){
        UserCurrentStatus userCurrentStatus = null;
        if (!enable) {
            return null;
        }

        try {
            Map params = new HashMap<String, String>();
            params.put("userId",userId);
            String response = executeHttpGet(gengine_path + "/" + containerId + "/status", params);
            if (response != null) {

                ObjectMapper mapper = new ObjectMapper();
                mapper.enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);

                userCurrentStatus = mapper.readValue(response,
                        UserCurrentStatus.class);
            }
        } catch (Exception e) {
            throw new ServiceException(e.getMessage());
        }

        return userCurrentStatus;
    }

    private String executeHttpGet(String path, Map params) throws Exception {

        String response = null;
        CloseableHttpClient httpClient = HttpClients.custom().build();

        URIBuilder builder = new URIBuilder();
        builder.setScheme(schema).setHost(endpoint).setPath(path);
        if(params!=null) {
            params.forEach((k, v) -> builder.addParameter((String)k,(String)v));
        }
        HttpGet httpGet = new HttpGet(builder.build());

        httpGet.setHeader(authKey, token);
        response = executeHttp(httpClient, httpGet);

        return response;
    }

    private String executeHttpPost(String path, String jsonBody) throws Exception {

        String response = null;
        CloseableHttpClient httpClient = HttpClients.custom().build();

        URIBuilder builder = new URIBuilder();
        builder.setScheme(schema).setHost(endpoint).setPath(path);
        HttpPost httpPost = new HttpPost(builder.build());

        httpPost.setHeader(authKey, token);
        httpPost.setHeader("Content-Type", "application/json");

        StringEntity entity = new StringEntity(jsonBody);
        httpPost.setEntity(entity);

        response = executeHttp(httpClient, httpPost);

        return response;
    }

    private String executeHttp(CloseableHttpClient httpClient, HttpUriRequest request) throws Exception {

        CloseableHttpResponse httpResponse = null;

        String response = null;
        try {
            httpResponse = httpClient.execute(request);

            if (httpResponse.getStatusLine().getStatusCode() == HttpStatus.OK.value()) {
                response = EntityUtils.toString(httpResponse.getEntity());
            }

        } finally {
            if (httpResponse != null) {
                httpResponse.close();
            }
            if (httpClient != null) {
                httpClient.close();
            }
        }
        return response;
    }
}
