package ro.siveco.ael.lcms.domain.taxonomies.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.taxonomies.model.Competence;
import ro.siveco.ael.lcms.domain.taxonomies.service.CompetenceService;
import ro.siveco.ram.app.exception.AppException;
import ro.siveco.ram.event.listener.type.single.BaseBeforeDeleteListener;
import ro.siveco.ram.event.type.single.BeforeDeleteEvent;

/**
 * Created by AndradaC on 9/1/2017.
 */
@Component
public class CheckCompetenceConnectionsBeforeDeleteListener extends BaseBeforeDeleteListener<Competence>{
    @Autowired
    private CompetenceService competenceService;

    @Override
    public void onApplicationEvent(BeforeDeleteEvent<Competence> event) {
        Competence competence = event.getDomainModel();

        String returnValue = competenceService.connexionsExist(competence.getId());
        if (returnValue != null) {
            throw new AppException(returnValue);
        }
    }
}
