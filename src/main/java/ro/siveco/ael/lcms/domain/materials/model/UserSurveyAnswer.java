package ro.siveco.ael.lcms.domain.materials.model;

import ro.siveco.ael.lcms.domain.auth.model.AelUser;
import ro.siveco.ael.lcms.domain.auth.model.User;
import ro.siveco.ael.lcms.domain.course.model.PlannedCourse;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table( name = "user_survey_answer")
@Inheritance( strategy = InheritanceType.SINGLE_TABLE )
@SequenceGenerator( name = "USER_SURVEY_ANSWER_SEQ", sequenceName = "user_survey_answer_seq" )
@DiscriminatorColumn(
        name = "discriminator",
        discriminatorType = DiscriminatorType.STRING
)
@DiscriminatorValue( value = "C" )
public class UserSurveyAnswer extends ro.siveco.ram.starter.model.base.BaseEntity implements Serializable, SurveyAnswer
{
	private Survey survey;
	private SurveyItemValue surveyItemValue;
    private SurveyItem surveyItem;
	private User user;
	private PlannedCourse plannedCourse;

	private Boolean booleanValue;
	private String textValue;
	private Long longValue;

	private Long rowID;
    private Date submissionDate;

	@Id
	@GeneratedValue( generator = "USER_SURVEY_ANSWER_SEQ", strategy = GenerationType.AUTO )
	public Long getId()
	{
		return super.getId();
	}

	public void setId(Long id) {
		super.setId(id);
	}

	@ManyToOne
	@JoinColumn( name = "ID_SURVEY", nullable = false )
	public Survey getSurvey()
	{
		return survey;
	}

	public void setSurvey( Survey survey )
	{
		this.survey = survey;
	}

	@ManyToOne
	@JoinColumn( name = "ID_SURVEY_ITEM_VALUE", nullable = true )
	public SurveyItemValue getSurveyItemValue()
	{
		return surveyItemValue;
	}

	public void setSurveyItemValue( SurveyItemValue surveyItemValue )
	{
		this.surveyItemValue = surveyItemValue;
	}

    @ManyToOne
	@JoinColumn( name = "ID_SURVEY_ITEM", nullable = true )
    public SurveyItem getSurveyItem() {
        return surveyItem;
    }

    public void setSurveyItem(SurveyItem surveyItem) {
        this.surveyItem = surveyItem;
    }

    @ManyToOne(targetEntity = AelUser.class)
	@JoinColumn( name = "ID_SCHOOL", nullable = false )
	public User getUser()
	{
		return user;
	}

	public void setUser( User user )
	{
		this.user = user;
	}


	@ManyToOne
	@JoinColumn( name = "ID_PLANNED_COURSE", nullable = false)
	public PlannedCourse getPlannedCourse()
	{
		return plannedCourse;
	}

	public void setPlannedCourse( PlannedCourse plannedCourse )
	{
		this.plannedCourse = plannedCourse;
	}

	@Column( name = "BOOLEAN_VALUE" )
	public Boolean getBooleanValue()
	{
		return booleanValue;
	}

	public void setBooleanValue( Boolean booleanValue )
	{
		this.booleanValue = booleanValue;
	}

	@Column( name = "TEXT_VALUE", length=2000 )
	public String getTextValue()
	{
		return textValue;
	}

	public void setTextValue( String textValue )
	{
		this.textValue = textValue;
	}

	@Column( name = "LONG_VALUE" )
	public Long getLongValue()
	{
		return longValue;
	}

	public void setLongValue( Long longValue )
	{
		this.longValue = longValue;
	}

	@Column( name = "ROW_ID" )
	public Long getRowID()
	{
		return rowID;
	}

	public void setRowID( Long rowID )
	{
		this.rowID = rowID;
	}

    @Column( name = "submission_date" )
    public Date getSubmissionDate() {
        return submissionDate;
    }

    public void setSubmissionDate(Date submissionDate) {
        this.submissionDate = submissionDate;
    }

}
