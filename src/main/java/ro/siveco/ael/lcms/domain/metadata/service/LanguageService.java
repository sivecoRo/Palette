package ro.siveco.ael.lcms.domain.metadata.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.siveco.ael.lcms.domain.metadata.model.Language;
import ro.siveco.ael.lcms.repository.LanguageRepository;
import ro.siveco.ram.app.exception.AppException;
import ro.siveco.ram.starter.service.BaseEntityService;

import java.util.List;

/**
 * Created by IuliaP on 15.06.2017.
 */
@Service
public class LanguageService extends BaseEntityService<Language> {

    @Autowired
    public LanguageService(LanguageRepository languageRepository) {
        super(Language.class, languageRepository);
    }

    public void validateLanguage(Language language) {
        if( language != null ) {
            List<Language> languages = ((LanguageRepository)getResourceRepository()).findAllByName( language.getName() );
            if( languages != null && languages.size() > 0 ) {
                throw new AppException("error.language.duplicateName");
            }
        }
    }

    public List<Language> findAllByName(String name) {
        return ((LanguageRepository)getResourceRepository()).findAllByName(name);
    }
}