package ro.siveco.ael.lcms.domain.materials.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import ro.siveco.ram.starter.ui.config.PropertyViewDescriptor;
import ro.siveco.ram.starter.ui.model.FormInputType;
import ro.siveco.ram.starter.ui.model.ViewRendererType;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by IntelliJ IDEA.
 * User: razvanni
 * Date: 31.05.2010
 * Time: 19:16:18
 * To change this template use File | Settings | File Templates.
 */

@Entity
@Table( name = "survey_item_value")
@SequenceGenerator( name = "SURVEY_ITEM_VALUE_SEQ", sequenceName = "survey_item_value_seq" )
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class SurveyItemValue extends ro.siveco.ram.starter.model.base.BaseEntity implements Serializable
{
	@JsonIgnore
	private SurveyItem surveyItem;
	private String code;

    @Column(length = 1000)
	private String definition;

	private Long order = 1L;

	private Boolean hasText = Boolean.FALSE;



	@Id
	@GeneratedValue( generator = "SURVEY_ITEM_VALUE_SEQ", strategy = GenerationType.AUTO )
	public Long getId()
	{
		return super.getId();
	}

	@ManyToOne
	@JoinColumn( name = "ID_SURVEY_ITEM", nullable = false )
	@PropertyViewDescriptor(ignore = true)
	public SurveyItem getSurveyItem()
	{
		return surveyItem;
	}

	public void setSurveyItem( SurveyItem surveyItem )
	{
		this.surveyItem = surveyItem;
	}

	@Column( name = "CODE" )
	@PropertyViewDescriptor(ignore = true)
	public String getCode()
	{
		return code;
	}

	public void setCode( String code )
	{
		this.code = code;
	}

	@Column( name = "DEFINITION" )
	@PropertyViewDescriptor(inputType = FormInputType.TEXT, rendererType = ViewRendererType.STRING,
            editInline = false, order = 2, required = true)
    public String getDefinition()
	{
		return definition;
	}

	public void setDefinition( String definition )
	{
		this.definition = definition;
	}

	@Column( name = "ITEM_ORDER" )
	@PropertyViewDescriptor(ignore = true)
	public Long getOrder()
	{
		return order;
	}

	public void setOrder( Long order )
	{
		this.order = order;
	}

	@Column( name = "HAS_TEXT" )
	@PropertyViewDescriptor(ignore = true)
	public Boolean getHasText()
	{
		return hasText;
	}

	public void setHasText( Boolean hasText )
	{
		this.hasText = hasText;
	}

	@Transient
	@PropertyViewDescriptor(ignore = true)
	public Long getIdEntitate()
	{
		return getId();
	}

	public void setIdEntitate( Long idEntitate )
	{
		setId( idEntitate );
	}


	@Override
	public boolean equals( Object o )
	{
		if( this == o )
		{
			return true;
		}
		if( o == null || getClass() != o.getClass() )
		{
			return false;
		}

		SurveyItemValue that = ( SurveyItemValue ) o;

		if( getId() != null ? !getId().equals( that.getId() ) : that.getId() != null )
		{
			return false;
		}

		return true;
	}

	@Override
	public int hashCode()
	{
		return getId() != null ? getId().hashCode() : 0;
	}
}
