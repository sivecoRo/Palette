package ro.siveco.ael.lcms.domain.materials.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.materials.model.Survey;
import ro.siveco.ael.lcms.domain.materials.service.SurveyService;
import ro.siveco.ram.event.listener.type.single.BaseAfterCreateListener;
import ro.siveco.ram.event.type.single.AfterCreateEvent;

/**
 * Created by AndradaC on 8/31/2017.
 */
@Component
public class PopulateSurveyAdiacentTablesAfterCreateListener extends BaseAfterCreateListener<Survey>{

    @Autowired
    private SurveyService surveyService;

    @Override
    public void onApplicationEvent(AfterCreateEvent<Survey> event) {

        Survey createdSurvey = event.getDomainModel();
        surveyService.addSurveySlider(createdSurvey);
    }
}
