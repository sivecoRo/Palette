package ro.siveco.ael.lcms.domain.metadata.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import ro.siveco.ram.starter.ui.config.PropertyViewDescriptor;
import ro.siveco.ram.starter.ui.config.ViewDescriptor;
import ro.siveco.ram.starter.ui.model.IndexViewType;
import ro.siveco.ram.starter.ui.model.ViewRendererType;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by IuliaP on 09.06.2017.
 */

@Entity
@Table(name = "metadata")
@SequenceGenerator(name = "metadata_seq", sequenceName = "metadata_seq")
@ViewDescriptor(paginationEnabled = true, viewTypes = {IndexViewType.GRID}, defaultViewType = IndexViewType.GRID,
        defaultSearchProperty = "tag.value")
public class Metadata extends ro.siveco.ram.starter.model.base.BaseEntity {

    private Long entityId;
    private EntityType entityType;
    private Tag tag;
    private Boolean checked;
    private RelationType relationType;
    private String nodeId;

    @Id
    @GeneratedValue(generator = "metadata_seq", strategy = GenerationType.AUTO)
    public Long getId() {
        return super.getId();
    }

    @Column(name = "entity_id", nullable = false)
    @NotNull
    @PropertyViewDescriptor(hidden = true)
    public Long getEntityId() {
        return entityId;
    }

    public void setEntityId(Long entityId) {

        this.entityId = entityId;
    }

    @Column(name = "entity_type", nullable = false)
    @NotNull
    @PropertyViewDescriptor(hidden = true, order = 1)
    public EntityType getEntityType() {
        return entityType;
    }

    public void setEntityType(EntityType entityType) {

        this.entityType = entityType;
    }

    @ManyToOne
    @JoinColumn(name = "tag_id", nullable = false)
    @NotNull
    @PropertyViewDescriptor(hiddenOnPresentation = true)
    public Tag getTag() {
        return tag;
    }

    @Transient
    @PropertyViewDescriptor(order = 2, rendererType = ViewRendererType.CHIP)
    String getTagValue() {

        return getTag().getValue();
    }

    @Column(name = "relation_type", nullable = false)
    @NotNull
    @PropertyViewDescriptor(hidden = true, order = 3)
    public RelationType getRelationType() {
        return relationType;
    }

    public void setRelationType(RelationType relationType) {

        this.relationType = relationType;
    }

    public void setTag(Tag tag) {
        this.tag = tag;
    }

    @Transient
    @PropertyViewDescriptor(hidden = true)
    public Boolean getChecked() {

        return checked;
    }

    public void setChecked(Boolean checked) {

        this.checked = checked;
    }

    @Transient
    @PropertyViewDescriptor(hidden = true)
    public String getNodeId() {

        return nodeId;
    }

    public void setNodeId(String nodeId) {

        this.nodeId = nodeId;
    }

    public boolean equals(Object o) {

        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final Metadata metadata = (Metadata) o;

        return new EqualsBuilder()
                .append(entityId, metadata.getEntityId())
                .append(entityType, metadata.getEntityType())
                .append(relationType, metadata.getRelationType())
                .append(tag, metadata.getTag())
                .isEquals();
    }

    public int hashCode() {

        return new HashCodeBuilder(17, 31)
                .append(entityId)
                .append(entityType)
                .append(relationType)
                .append(tag)
                .toHashCode();
    }
}
