package ro.siveco.ael.lcms.domain.notification.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.course.model.PlannedCourse;
import ro.siveco.ael.lcms.domain.course.service.PlannedCourseService;
import ro.siveco.ael.lcms.domain.notification.model.PlannedNotification;
import ro.siveco.ram.event.listener.type.single.BaseAfterReadListener;
import ro.siveco.ram.event.type.single.AfterReadEvent;

/**
 * Created by IuliaP on 29.09.2017.
 */
@Order(value = 1)
@Component
public class SetPropertiesForPlannedNotificationAfterReadListener extends BaseAfterReadListener<PlannedNotification> {

    @Autowired
    PlannedCourseService plannedCourseService;

    @Override
    public void onApplicationEvent(AfterReadEvent<PlannedNotification> event) {

        PlannedNotification plannedNotification = event.getDomainModel();
        if (plannedNotification.getId() != null) {
            PlannedCourse plannedCourse = plannedCourseService.findOne(plannedNotification.getEntityId());
            plannedNotification.setPlannedCourse( plannedCourse );
            plannedNotification.setPlannedCourseName( plannedCourse.getCourse().getName() );
        }
    }
}
