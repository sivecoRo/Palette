package ro.siveco.ael.lcms.domain.materials.model;

/**
 * Created by IntelliJ IDEA.
 * User: CristinaIo
 * Date: Apr 10, 2012
 * Time: 1:09:50 PM
 * To change this template use File | Settings | File Templates.
 */
public interface MessageResolvableEnum {

    String getKey();
}
