package ro.siveco.ael.lcms.domain.notification.model;

import ro.siveco.ael.domain.annotations.ColumnComment;
import ro.siveco.ael.lcms.domain.auth.model.AelUser;

import javax.persistence.*;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: RazvanS
 * Date: 8/6/14
 * Time: 11:25 AM
 */
@Entity
@Table( name = "notification_schedule" )
@SequenceGenerator( name = "notification_schedule_seq", sequenceName = "notification_schedule_seq" )
public class NotificationSchedule extends ro.siveco.ram.starter.model.base.BaseEntity
{
	private Long id;

	@ColumnComment( comment = "The user from id" )
	private AelUser fromUser;

	@ColumnComment( comment = "The user to id" )
	private AelUser toUser;

	@ColumnComment( comment = "The subject arguments object serialized" )
	private byte[] subjectArgs;

	@ColumnComment( comment = "The message body arguments serialized" )
	private byte[] messageBodyArgs;

	@ColumnComment( comment = "The user message type" )
	private UserMessageType userMessageType;

	@ColumnComment( comment = "The user message format" )
    private ApplicationUserMessageFormat userMessageFormat;

	@ColumnComment( comment = "The notification type" )
	private NotificationType notificationType;

	@ColumnComment( comment = "The creator id and title" )
	private String creatorIdAndTitle;

	@ColumnComment( comment = "The status of the notification - 0-not sent and 1-sent" )
	private Boolean status;

	@ColumnComment( comment = "The date to send the notification" )
	private Date date;

	@ColumnComment( comment = "The entity id" )
	private Long entityId;

    @ColumnComment(comment = "The associated planned notification")
    private PlannedNotification plannedNotification;

	@Id
	@GeneratedValue( generator = "notification_schedule_seq", strategy = GenerationType.AUTO )
	public Long getId()
	{
		return id;
	}

	public void setId( Long id )
	{
		this.id = id;
	}

	@ManyToOne
	@JoinColumn( name = "from_user_id", nullable = false )
	public AelUser getFromUser()
	{
		return fromUser;
	}

	public void setFromUser( AelUser fromUser )
	{
		this.fromUser = fromUser;
	}

	@ManyToOne
	@JoinColumn( name = "to_user_id", nullable = false )
	public AelUser getToUser()
	{
		return toUser;
	}

	public void setToUser( AelUser toUser )
	{
		this.toUser = toUser;
	}

	@Column( name = "subject_args" )
	public byte[] getSubjectArgs()
	{
		return subjectArgs;
	}

	public void setSubjectArgs( byte[] subjectArgs )
	{
		this.subjectArgs = subjectArgs;
	}

	@Column( name = "message_body_args" )
	public byte[] getMessageBodyArgs()
	{
		return messageBodyArgs;
	}

	public void setMessageBodyArgs( byte[] messageBodyArgs )
	{
		this.messageBodyArgs = messageBodyArgs;
	}

	@Column( name = "user_message_type", nullable = false )
	public UserMessageType getUserMessageType()
	{
		return userMessageType;
	}

	public void setUserMessageType( UserMessageType userMessageType )
	{
		this.userMessageType = userMessageType;
	}

    /*@ManyToOne
    @JoinColumn( name = "user_message_format", nullable = false )*/
    @Column(name = "user_message_format", nullable = false)
    public ApplicationUserMessageFormat getUserMessageFormat() {

        return userMessageFormat;
    }

    public void setUserMessageFormat(ApplicationUserMessageFormat userMessageFormat) {

        this.userMessageFormat = userMessageFormat;
	}

	@Column( name = "notification_type", nullable = false )
	public NotificationType getNotificationType()
	{
		return notificationType;
	}

	public void setNotificationType( NotificationType notificationType )
	{
		this.notificationType = notificationType;
	}

	@Column( name = "creator_id_and_title" )
	public String getCreatorIdAndTitle()
	{
		return creatorIdAndTitle;
	}

	public void setCreatorIdAndTitle( String creatorIdAndTitle )
	{
		this.creatorIdAndTitle = creatorIdAndTitle;
	}

	@Column( name = "status", nullable = false )
	public Boolean getStatus()
	{
		return status;
	}

	public void setStatus( Boolean status )
	{
		this.status = status;
	}

	@Column( name = "date_to_send", nullable = false )
	public Date getDate()
	{
		return date;
	}

	public void setDate( Date date )
	{
		this.date = date;
	}

	@Column( name = "entity_id" )
	public Long getEntityId()
	{
		return entityId;
	}

	public void setEntityId( Long entityId )
	{
		this.entityId = entityId;
	}

    @ManyToOne
    @JoinColumn(name = "planned_notification_id")
    public PlannedNotification getPlannedNotification() {

        return plannedNotification;
    }

    public void setPlannedNotification(PlannedNotification plannedNotification) {

        this.plannedNotification = plannedNotification;
    }
}
