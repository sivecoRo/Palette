package ro.siveco.ael.lcms.domain.notification.model;

/**
 * User: cristianc
 * Date: 18.06.2007
 * Time: 15:19:29
 */
public enum UserMessageType
{
	GENERIC_MESSAGE( "generic.message" ),
	GRADE_RELATED_MESSAGE( "grade.related.message" ),
	ABSENCE_RELATED_MESSAGE( "absence.related.message" ),
	COURSE_RELATED_MESSAGE( "course.related.message" ),
	ASSIGNMENT_RELATED_MESSAGE( "assignment.related.message" ),
    USER_MESSAGE( "user.message" ),
	DEADLINE_NOTIFICATION( "deadline.notification" ),
	FAILED_ASSESSMENTS( "failed.assessments" ),
	ATTENDANCE_BELOW_THRESHOLD( "attendance.below.threshold" ),

	ORANGE_GENERIC_MESSAGE( "orange.generic.message" ),
	COURSE_PARTICIPATION_TEMPLATES( "course.participation.templates");

	UserMessageType( String name )
	{
		this.name = name;
	}

	public String getName()
	{
		return name;
	}

	private String name;

}
