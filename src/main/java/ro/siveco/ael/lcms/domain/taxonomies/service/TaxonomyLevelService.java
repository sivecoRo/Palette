package ro.siveco.ael.lcms.domain.taxonomies.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.siveco.ael.lcms.domain.auth.service.UserCompetenceService;
import ro.siveco.ael.lcms.domain.course.service.CourseCompetenceService;
import ro.siveco.ael.lcms.domain.organization.service.JobTitleCompetenceService;
import ro.siveco.ael.lcms.domain.taxonomies.model.TaxonomyLevel;
import ro.siveco.ael.lcms.domain.tenant.model.Tenant;
import ro.siveco.ael.lcms.repository.TaxonomyLevelRepository;
import ro.siveco.ram.starter.service.BaseEntityService;

/**
 * Created by JohnB on 22/06/2017.
 */

@Service
public class TaxonomyLevelService extends BaseEntityService<TaxonomyLevel> {

    @Autowired
    CourseCompetenceService courseCompetenceService;

    @Autowired
    UserCompetenceService userCompetenceService;

    @Autowired
    JobTitleCompetenceService jobTitleCompetenceService;

    @Autowired
    public TaxonomyLevelService(TaxonomyLevelRepository resourceRepository) {
        super(TaxonomyLevel.class, resourceRepository);
    }

    private TaxonomyLevel findByOrderNumberAndTenant(Integer orderNumber, Tenant tenant){
        return ((TaxonomyLevelRepository)getResourceRepository()).findByOrderNumberAndTenant(orderNumber, tenant);
    }

    private TaxonomyLevel findByNameAndTenant(String levelName, Tenant tenant){
        return ((TaxonomyLevelRepository)getResourceRepository()).findByNameAndTenant(levelName, tenant);
    }

    public String validateTaxonomyLevel(TaxonomyLevel taxonomyLevel){
        Boolean isNewTaxonomyLevel = taxonomyLevel.getId() == null;
        if(taxonomyLevel.getOrderNumber() == null || taxonomyLevel.getOrderNumber() <=0){
            return "error.taxonomyLevel.invalidFieldValue.orderNumber";
        }else{
            TaxonomyLevel taxonomyLevelWithSameName = findByNameAndTenant(taxonomyLevel.getName(), taxonomyLevel.getTenant());
            if(taxonomyLevelWithSameName != null && (isNewTaxonomyLevel || !(taxonomyLevel.getId().equals(taxonomyLevelWithSameName.getId())))){
                return "error.taxonomyLevel.duplicateField.name";
            }else{
                TaxonomyLevel taxonomyLevelWithSameOrderNumber = findByOrderNumberAndTenant(taxonomyLevel.getOrderNumber(), taxonomyLevel.getTenant());
                if(taxonomyLevelWithSameOrderNumber != null &&
                        (isNewTaxonomyLevel || !(taxonomyLevel.getId().equals(taxonomyLevelWithSameOrderNumber.getId())))){
                    return "error.taxonomyLevel.duplicateField.orderNumber";
                }
            }
        }
        return "";
    }

    public Double calculateRatio(TaxonomyLevel t1, TaxonomyLevel t2) {

        int lv1, lv2;
        if(t1 == null)
            return new Double(0.0);
        lv1 = t1.getOrderNumber().intValue();
        lv2 = t2.getOrderNumber().intValue();

        if((double)lv1/lv2 > 1)
            return new Double(1.0);

        return new Double(lv1/lv2);
    }

    public TaxonomyLevel findById(Long taxonomyLevelId) {
        return ((TaxonomyLevelRepository) getResourceRepository()).findOne(taxonomyLevelId);
    }

    public String connexionExists(Long taxonomyLevelId) {
        TaxonomyLevel taxonomyLevel = findById(taxonomyLevelId);
        if(courseCompetenceService.taxonomyLevelExists(taxonomyLevel)) {
            return "error.taxonomyLevel.delete.courseAssociation";
        }
        if(userCompetenceService.taxonomyLevelExists(taxonomyLevel)) {
            return "error.taxonomyLevel.delete.userAssociation";
        }
        if(jobTitleCompetenceService.taxonomyLevelExists(taxonomyLevel)) {
            return "error.taxonomyLevel.delete.jobTitleAssociation";
        }
        return null;
    }
}