package ro.siveco.ael.lcms.domain.organization.model;

public enum ComponentUserJobStatus {

    PAST, CURRENT, FUTURE;
}
