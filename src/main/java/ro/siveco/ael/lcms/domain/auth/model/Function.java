package ro.siveco.ael.lcms.domain.auth.model;

import ro.siveco.ram.model.Entity;

/**
 * User: AlexandruVi
 * Date: 2017-02-20
 */
public interface Function extends Entity<Long> {
    String getName();
}
