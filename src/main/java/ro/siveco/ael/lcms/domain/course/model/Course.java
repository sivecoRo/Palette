package ro.siveco.ael.lcms.domain.course.model;

//import org.apache.commons.lang.builder.EqualsBuilder;
//import org.apache.commons.lang.builder.HashCodeBuilder;
//import org.apache.commons.lang.builder.ToStringBuilder;
//import org.apache.commons.lang.builder.ToStringStyle;
//import org.codehaus.jackson.annotate.JsonIgnore;
//import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.*;
import org.hibernate.type.BinaryType;
import org.springframework.data.domain.Sort;
import org.springframework.web.multipart.MultipartFile;
import ro.siveco.ael.lcms.domain.auth.model.AelUser;
import ro.siveco.ael.lcms.domain.metadata.model.*;
import ro.siveco.ram.model.TargetImplementation;
import ro.siveco.ram.starter.ui.config.ActionDescriptor;
import ro.siveco.ram.starter.ui.config.PropertyViewDescriptor;
import ro.siveco.ram.starter.ui.config.Sorter;
import ro.siveco.ram.starter.ui.config.ViewDescriptor;
import ro.siveco.ram.starter.ui.model.*;

import javax.persistence.CascadeType;
import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: Siveco Romania SA</p>
 *
 * @author RobertR
 * @version : 1.0 / Nov 29, 2006
 */

@Entity
@Table(name = "courses", uniqueConstraints = {@UniqueConstraint(columnNames = {"name", "creator_id"})})
@SequenceGenerator(name = "courses_seq", sequenceName = "courses_seq")
@FilterDefs({
        @FilterDef(name = "tenantCourseFilter", parameters = @ParamDef(name = "tenantId", type = "long")),
        @FilterDef(name = "courseFilter")
})
@Filters({
        @Filter(name = "tenantCourseFilter",
                condition = "exists (select 1 from users u where u.id=creator_id and u.tenant_id=:tenantId)"),
        @Filter(name = "courseFilter",
                condition = "exists (select 1 from users u where u.id=creator_id and u.tenant_id is null)")
})
@ViewDescriptor(
        pageSize = 9,
        viewTypes = {IndexViewType.CARD}, defaultViewType = IndexViewType.CARD,
        cardSizeLarge = GridSize.THREE_PER_ROW,
        searchEnabled = false, defaultSearchProperty = "name",
        sort = {@Sorter(propertyName = "creationDate", direction = Sort.Direction.ASC)},
        defaultActions = {
                @ActionDescriptor( type = ActionType.OPEN_CREATE_FORM, fixedAction = true, order=1),
                @ActionDescriptor(type = ActionType.OPEN_CREATE_FORM, fixedAction = true, order=2,
                        actionId = "createActivityId", view = ro.siveco.ael.lcms.domain.course.view.Views.ActivityView.class),
                @ActionDescriptor(type = ActionType.OPEN_CREATE_FORM, fixedAction = true,order=3,
                        actionId = "createServiceId", view = ro.siveco.ael.lcms.domain.course.view.Views.ServiceView.class),
                @ActionDescriptor(type = ActionType.OPEN_CREATE_FORM, fixedAction = true,order=4,
                        actionId = "createMeetingId", view = ro.siveco.ael.lcms.domain.course.view.Views.MeetingView.class),
                @ActionDescriptor( type = ActionType.OPEN_EDIT_FORM, mainAction = true,order=5,
                        cssClass = "generic-action action-open-edit-form action-course-open-edit-form btn meta-controller edit-course-class"),
                @ActionDescriptor( type = ActionType.OPEN_EDIT_FORM, mainAction = true,
                        actionId = "updateActivityId",
                        order = 6,
                        cssClass = "generic-action action-open-edit-form action-course-open-edit-form btn meta-controller edit-activity-class",
                        view = ro.siveco.ael.lcms.domain.course.view.Views.ActivityView.class),
                @ActionDescriptor( type = ActionType.OPEN_EDIT_FORM, mainAction = true,
                        actionId = "updateServiceId",
                        order = 7,
                        cssClass = "generic-action action-open-edit-form action-course-open-edit-form btn meta-controller edit-service-class",
                        view = ro.siveco.ael.lcms.domain.course.view.Views.ServiceView.class),
                @ActionDescriptor( type = ActionType.OPEN_EDIT_FORM, mainAction = true,
                        actionId = "updateMeetingId",
                        order = 8,
                        cssClass = "generic-action action-open-edit-form action-course-open-edit-form btn meta-controller edit-meeting-class",
                        view = ro.siveco.ael.lcms.domain.course.view.Views.MeetingView.class),
                @ActionDescriptor( type = ActionType.DELETE, mainAction = true, order = 9, requiresConfirmation = true),
                @ActionDescriptor( type = ActionType.OPEN_PRESENTATION, availableOnlyInPresentation = true, order=10),
                @ActionDescriptor( type = ActionType.CREATE, forwardFor = {"{modelName}-OPEN_CREATE_FORM"}, availableOnlyInForm = true, order=11),
                @ActionDescriptor( type = ActionType.UPDATE, forwardFor = {"{modelName}-OPEN_EDIT_FORM"}, availableOnlyInForm = true, order=12),
                @ActionDescriptor(type = ActionType.CUSTOM_GET,  uri = "/courses", availableOnlyInForm = true, availableInForm = true, order = 13),
                @ActionDescriptor( type = ActionType.LIST, availableInForm = true, availableInPresentation = true, order=14,
                        cssClass = "generic-action action-list action-course-list btn secondary meta-controller left back-action",
                backToFor = {"{modelName}-OPEN_CREATE_FORM",
                        "{modelName}-CREATE",
                        "{modelName}-OPEN_PRESENTATION",
                        "{modelName}-OPEN_EDIT_FORM",
                        "{modelName}-UPDATE",
                        "{modelName}-DELETE"}
        )}
//        otherActions = {
//                @ActionDescriptor(type = ActionType.OPEN_CREATE_FORM, modelName = "plannedCourse",
//                        propertyName = "course")}
)
@ViewDescriptor(
        view = ro.siveco.ael.lcms.domain.course.view.Views.ActivityView.class,
        searchEnabled = false, defaultSearchProperty = "name",
        defaultActions = {
                @ActionDescriptor(type = ActionType.OPEN_CREATE_FORM, fixedAction = true,order=1,
                        actionId = "createActivityId", view = ro.siveco.ael.lcms.domain.course.view.Views.ActivityView.class),
                @ActionDescriptor(type = ActionType.CREATE,order=2,
                        forwardFor = "createActivityId", availableOnlyInForm = true, view = ro.siveco.ael.lcms.domain.course.view.Views.ActivityView.class),
                @ActionDescriptor(type = ActionType.OPEN_EDIT_FORM, fixedAction = true,order=3,
                        actionId = "updateActivityId",
                        cssClass = "generic-action action-open-edit-form action-course-open-edit-form btn meta-controller edit-activity-class",
                        view = ro.siveco.ael.lcms.domain.course.view.Views.ActivityView.class),
                @ActionDescriptor(type = ActionType.UPDATE,order=4,
                        forwardFor = "updateActivityId", availableOnlyInForm = true, view = ro.siveco.ael.lcms.domain.course.view.Views.ActivityView.class),
                @ActionDescriptor(type = ActionType.CUSTOM_GET,  uri = "/courses", availableOnlyInForm = true,availableInForm = true, order = 5),
                @ActionDescriptor(type = ActionType.LIST, availableInForm = true, inheritViewFromWrapper = false,order=6,
                        cssClass = "generic-action action-list action-course-list btn secondary meta-controller left back-action",
                        view = ro.siveco.ram.starter.ui.model.Views.__default.class, backToFor = {
                        "createActivityId",
                        "course-CREATE-ActivityView",
                        "updateActivityId",
                        "course-UPDATE-ActivityView"
                })
        }
)
@ViewDescriptor(
        view = ro.siveco.ael.lcms.domain.course.view.Views.ServiceView.class,
        searchEnabled = false, defaultSearchProperty = "name",
        defaultActions = {
                @ActionDescriptor(type = ActionType.OPEN_CREATE_FORM, fixedAction = true,order=1,
                        actionId = "createServiceId", view = ro.siveco.ael.lcms.domain.course.view.Views.ServiceView.class),
                @ActionDescriptor(type = ActionType.CREATE,order=2,
                        forwardFor = "createServiceId", availableOnlyInForm = true, view = ro.siveco.ael.lcms.domain.course.view.Views.ServiceView.class),
                @ActionDescriptor(type = ActionType.OPEN_EDIT_FORM, fixedAction = true,order=3,
                        actionId = "updateServiceId",
                        cssClass = "generic-action action-open-edit-form action-course-open-edit-form btn meta-controller edit-service-class",
                        view = ro.siveco.ael.lcms.domain.course.view.Views.ServiceView.class),
                @ActionDescriptor(type = ActionType.UPDATE,order=4,
                        forwardFor = "updateServiceId", availableOnlyInForm = true, view = ro.siveco.ael.lcms.domain.course.view.Views.ServiceView.class),
                @ActionDescriptor(type = ActionType.CUSTOM_GET,  uri = "/courses", availableOnlyInForm = true, availableInForm = true, order = 5),
                @ActionDescriptor(type = ActionType.LIST, availableInForm = true, inheritViewFromWrapper = false,order=6,
                        cssClass = "generic-action action-list action-course-list btn secondary meta-controller left back-action",
                        view = ro.siveco.ram.starter.ui.model.Views.__default.class, backToFor = {
                        "createServiceId",
                        "course-CREATE-ServiceView",
                        "updateServiceId",
                        "course-UPDATE-ServiceView"
                })
        }
)
@ViewDescriptor(
        view = ro.siveco.ael.lcms.domain.course.view.Views.MeetingView.class,
        searchEnabled = false, defaultSearchProperty = "name",
        defaultActions = {
                @ActionDescriptor(type = ActionType.OPEN_CREATE_FORM, fixedAction = true,order=1,
                        actionId = "createMeetingId", view = ro.siveco.ael.lcms.domain.course.view.Views.MeetingView.class),
                @ActionDescriptor(type = ActionType.CREATE,order=2,
                        forwardFor = "createMeetingId", availableOnlyInForm = true, view = ro.siveco.ael.lcms.domain.course.view.Views.MeetingView.class),
                @ActionDescriptor(type = ActionType.OPEN_EDIT_FORM, fixedAction = true,order=3,
                        actionId = "updateMeetingId",
                        cssClass = "generic-action action-open-edit-form action-course-open-edit-form btn meta-controller edit-meeting-class",
                        view = ro.siveco.ael.lcms.domain.course.view.Views.MeetingView.class),
                @ActionDescriptor(type = ActionType.UPDATE,order=4,
                        forwardFor = "updateMeetingId", availableOnlyInForm = true, view = ro.siveco.ael.lcms.domain.course.view.Views.MeetingView.class),
                @ActionDescriptor(type = ActionType.CUSTOM_GET,  uri = "/courses/", availableOnlyInForm = true, availableInForm = true, order = 5),
                @ActionDescriptor(type = ActionType.LIST, availableInForm = true, inheritViewFromWrapper = false,order=6,
                        cssClass = "generic-action action-list action-course-list btn secondary meta-controller left back-action",
                        view = ro.siveco.ram.starter.ui.model.Views.__default.class, backToFor = {
                        "createMeetingId",
                        "course-CREATE-MeetingView",
                        "updateMeetingId",
                        "course-UPDATE-MeetingView"
                })
        }
)
public class Course extends ro.siveco.ram.starter.model.base.BaseEntity implements Taggable {

    List<AelUser> studentsList;

    private String name;

    private String description;

    private byte[] image;

    private String addImageSrc = "/img/create-profile/add_photo.png"; // add

    private String imageSrc = "/img/generic/generic-course-light.png"; // default

    private AelUser creator;

    private Date creationDate;

    private Date updateDate;

    private Boolean active;

    private Boolean isPublic = Boolean.FALSE;

    private String objective;

    private Boolean isMandatory;

    private Boolean isPeriodical;

    private Date lastPlanificationDate;

    private Integer numberOfMonths;

    private Boolean isOpenCourse;

    private CourseType type = CourseType.COURSE;

    private List<CourseCompetence> courseCompetences = new ArrayList<CourseCompetence>();

    private List<PlannedCourse> plannedCourses = new ArrayList<>();

    private Integer studentsNumber;

    private Integer plannedCoursesNumber;

    private List<Tag> includedTags;

    private List<Tag> excludedTags;

    private List<Tag> optionalTags;

    private MultipartFile[] imageMultipart;

    private Date plannedCourseStartDate;

    private Date plannedCourseEndDate;

    private String displayPlannedCourseStartDate;

    private String displayPlannedCourseEndDate;

    private AelUser owner;

    private Time plannedCourseStartTime;

    private Time plannedCourseEndTime;

    private String itemLocation;

    private String itemLocationCoordinates;

    private Integer minUserNumber;

    private Integer maxUserNumber;

    private String ownerImageSrc = "/img/generic/generic-user.png";

    private String ownerName;

    private String ownerEmail;

    private String ownerPhone;

    private Integer ownerItemsNumber;

    private String itemSchedulePeriod;

    private String itemScheduleTimePeriod;

    private Integer itemLikeNumber;

    private Integer itemDislikeNumber;

    private String readMoreLink;

    private String country;

    private String countryCode;

    private List<Interest> allInterests;

    private String startDateRaw;

    private String endDateRaw;

    private String startTimeRaw;

    private String endTimeRaw;

    private String clientTimeZoneId;

    private String displayStudentsNumber;

    private Boolean showOwnerPhone;

    private Boolean showOwnerEmail;

    private List<Comment> comments= new ArrayList<>();

    private Boolean attending;

    private String attendingBtn= "";

    private Integer usersAttending;

    private String displayUsersAttending;

    private String allAttendingBtn= "";

    private List<AelUser> allAttendingStudents;

    private List<AelUser>  attendingStudentsForDisplay;



    @Id
    @GeneratedValue(generator = "courses_seq", strategy = GenerationType.SEQUENCE)
    public Long getId() {

        return super.getId();
    }

    @Column(length = 2000)
    @PropertyViewDescriptor(
            hiddenOnList = true, hiddenOnGrid = true, hiddenOnCard = true,
            order = 6, secondaryContent = true,
            rendererType = ViewRendererType.HTML, inputType = FormInputType.TEXTAREA,
            required = false, orderInForm = 4, groupName = "itemDescription")
    @PropertyViewDescriptor(
            view = ro.siveco.ael.lcms.domain.course.view.Views.ActivityView.class,
            hiddenOnList = true, hiddenOnGrid = true, hiddenOnCard = true,
            order = 6, displayLabelInPresentation = false, secondaryContent = true,
            rendererType = ViewRendererType.HTML,
            inputType = FormInputType.TEXTAREA, required = false, orderInForm = 4)
    @PropertyViewDescriptor(
            view = ro.siveco.ael.lcms.domain.course.view.Views.ServiceView.class,
            hiddenOnList = true, hiddenOnGrid = true, hiddenOnCard = true,
            order = 6, displayLabelInPresentation = false, secondaryContent = true,
            rendererType = ViewRendererType.HTML,
            inputType = FormInputType.TEXTAREA, required = false, orderInForm = 4)
    @PropertyViewDescriptor(
            view = ro.siveco.ael.lcms.domain.course.view.Views.MeetingView.class,
            hiddenOnList = true, hiddenOnGrid = true, hiddenOnCard = true,
            order = 6, displayLabelInPresentation = false, secondaryContent = true,
            rendererType = ViewRendererType.HTML,
            inputType = FormInputType.TEXTAREA, required = false, orderInForm = 4)
    public String getDescription() {

        return description;
    }

    public void setDescription(String description) {

        this.description = description;
    }

    @ManyToOne(targetEntity = AelUser.class)
    @PropertyViewDescriptor(
            hiddenOnCard = true, hiddenOnList = true, hiddenOnPresentation = true,
            displayLabelInPresentation = false,
            rendererType = ViewRendererType.MODEL_AVATAR, groupName = "courseCreator",
            hiddenOnForm = true)
    public AelUser getCreator() {

        return creator;
    }

    public void setCreator(AelUser creator) {

        this.creator = creator;
    }

    @Column(name = "creation_date", nullable = false)
    @PropertyViewDescriptor(
            hidden = true,
            rendererType = ViewRendererType.DATE,
            hiddenOnForm = true)
    public Date getCreationDate() {

        return creationDate;
    }

    public void setCreationDate(Date creationDate) {

        this.creationDate = creationDate;
    }

    @Column(name = "update_date")
    @PropertyViewDescriptor(hidden = true)
    public Date getUpdateDate() {

        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {

        this.updateDate = updateDate;
    }

    @Column(name = "active")
    @PropertyViewDescriptor(hidden = true)
    public Boolean getActive() {

        return active;
    }

    public void setActive(Boolean active) {

        this.active = active;
    }

    @Column(name = "is_public")
    @PropertyViewDescriptor( hidden = true)
    public Boolean getIsPublic() {

        return isPublic;
    }

    public void setIsPublic(Boolean isPublic) {

        this.isPublic = isPublic;
    }

    @Column(name = "objective", length = 2000)
    @PropertyViewDescriptor(hiddenOnCard = true, hiddenOnList = true, hiddenOnGrid = true,
            rendererType = ViewRendererType.HTML,
            inputType = FormInputType.TEXTAREA, required = false, orderInForm = 6, groupName = "itemObjective")
    public String getObjective() {

        return objective;
    }

    public void setObjective(String objective) {

        this.objective = objective;
    }

    @Column(name = "is_mandatory")
    @PropertyViewDescriptor(hidden = true)
    public Boolean getIsMandatory() {

        return isMandatory;
    }

    public void setIsMandatory(Boolean mandatory) {

        isMandatory = mandatory;
    }

    @Column(name = "is_periodical")
    @PropertyViewDescriptor(hidden = true)
    public Boolean getIsPeriodical() {

        return isPeriodical;
    }

    public void setIsPeriodical(Boolean periodical) {

        isPeriodical = periodical;
    }

    @Column(name = "no_months")
    @PropertyViewDescriptor(hidden = true)
    public Integer getNumberOfMonths() {

        return numberOfMonths;
    }

    public void setNumberOfMonths(Integer numberOfMonths) {

        this.numberOfMonths = numberOfMonths;
    }

    @Column(name = "last_planification_date")
    @PropertyViewDescriptor(hidden = true)
    public Date getLastPlanificationDate() {

        return lastPlanificationDate;
    }

    public void setLastPlanificationDate(Date lastPlanificationDate) {

        this.lastPlanificationDate = lastPlanificationDate;
    }

    @Column(name = "is_open_course")
    @PropertyViewDescriptor(
            hidden = true)
    public Boolean getIsOpenCourse() {

        return isOpenCourse;
    }

    public void setIsOpenCourse(Boolean isOpenCourse) {

        this.isOpenCourse = isOpenCourse;
    }

    @Column(name = "type")
    @PropertyViewDescriptor(order = 1, displayLabelInPresentation = false, hiddenOnForm = true, displayLabelInForm = false,
            rendererType = ViewRendererType.ENUM, groupName = "itemType")
    public CourseType getType() {

        return type;
    }

    public void setType(CourseType type) {

        this.type = type;
    }

    @Transient
    @PropertyViewDescriptor(hidden = true)
    public EntityType getEntityType() {

        return EntityType.COURSE;
    }

    @Override
    @Transient
    @PropertyViewDescriptor(
            order = 3,/* First property */
            hiddenOnForm = true,
            hiddenOnGrid = true,
            hiddenOnCard = true,
            displayLabelInPresentation = false,
            rendererType = ViewRendererType.OBJECT_LABEL,
            groupName = "itemNameAndFavorites",
            clickAction = {
                    @ActionDescriptor(type = ActionType.OPEN_PRESENTATION),
                    @ActionDescriptor(type = ActionType.LIST, backToFor = "{modelName}-OPEN_PRESENTATION")})
    public String getObjectLabel() {

        return getName();
    }

    @Column(nullable = false, length = 500)
    @PropertyViewDescriptor(
            order=5, displayLabelInPresentation = false,
            inputFieldSize = GridSize.TWO_PER_ROW,
            hiddenOnList = true, hiddenOnGrid = true, hiddenOnPresentation = true,
            rendererType = ViewRendererType.HTML,
            inputType = FormInputType.TEXT, required = true, orderInForm = 1)
    @PropertyViewDescriptor(
            inputFieldSize = GridSize.TWO_PER_ROW,
            view = ro.siveco.ael.lcms.domain.course.view.Views.ActivityView.class,
            hiddenOnList = true, hiddenOnGrid = true, hiddenOnCard = true, hiddenOnPresentation = true,
            rendererType = ViewRendererType.HTML,
            inputType = FormInputType.TEXT, required = true, orderInForm = 1)
    @PropertyViewDescriptor(
            inputFieldSize = GridSize.TWO_PER_ROW,
            view = ro.siveco.ael.lcms.domain.course.view.Views.ServiceView.class,
            hiddenOnList = true, hiddenOnGrid = true, hiddenOnCard = true, hiddenOnPresentation = true,
            rendererType = ViewRendererType.HTML,
            inputType = FormInputType.TEXT, required = true, orderInForm = 1)
    @PropertyViewDescriptor(
            inputFieldSize = GridSize.TWO_PER_ROW,
            view = ro.siveco.ael.lcms.domain.course.view.Views.MeetingView.class,
            hiddenOnList = true, hiddenOnGrid = true, hiddenOnCard = true, hiddenOnPresentation = true,
            rendererType = ViewRendererType.HTML,
            inputType = FormInputType.TEXT, required = true, orderInForm = 1)
    public String getName() {

        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

    @Lob
    @Column(name = "image", columnDefinition = "bytea")
    @Type(type = "org.hibernate.type.BinaryType")
    @TargetImplementation(targetEntity = BinaryType.class)
    @PropertyViewDescriptor(
            hidden = true, rendererType = ViewRendererType.IMAGE, order = 17)
    public byte[] getImage() {

        return image;
    }

    public void setImage(byte[] image) {

        this.image = image;
    }

    @Transient
    @PropertyViewDescriptor(
            hiddenOnForm=true, displayLabelInForm = false,
            order = -3, displayLabelInPresentation = false,
            rendererType = ViewRendererType.IMAGE, groupName = "itemImage",
            inputType = FormInputType.IMAGE_UPLOAD, required = false)
    public String getImageSrc() {

        return imageSrc;
    }

    public void setImageSrc(String imageSrc) {

        this.imageSrc = imageSrc;
    }

    @Transient
    @PropertyViewDescriptor(
            hiddenOnForm=true, displayLabelInForm = false, hidden = true,
            order = -33, displayLabelInPresentation = false,
            rendererType = ViewRendererType.IMAGE, groupName = "itemImage",
            inputType = FormInputType.IMAGE_UPLOAD, required = false)
    public String getAddImageSrc() {

        return getImageSrc();
    }

    public void setAddImageSrc(String addImageSrc) {

        this.addImageSrc = addImageSrc;
    }

    @OneToMany(mappedBy = "course", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    @JsonIgnore
    @PropertyViewDescriptor(hidden = true)
    public List<CourseCompetence> getCourseCompetences() {

        return courseCompetences;
    }

    public void setCourseCompetences(List<CourseCompetence> _courseCompetences) {

        this.courseCompetences = _courseCompetences;
    }

    @Transient
    //    @JsonManagedReference("sessions")
    @PropertyViewDescriptor(
            hidden = true)
    public List<PlannedCourse> getPlannedCourses() {

        return plannedCourses;
    }

    public void setPlannedCourses(List<PlannedCourse> plannedCourses) {

        this.plannedCourses = plannedCourses;
    }

    @Transient
    @PropertyViewDescriptor(
            hidden = true)
    public Integer getStudentsNumber() {

        return studentsNumber;
    }

    public void setStudentsNumber(Integer studentsNumber) {

        this.studentsNumber = studentsNumber;
    }

    @Transient
    @PropertyViewDescriptor(
            hiddenOnCard = true, hiddenOnGrid = true, hiddenOnPresentation = true, displayLabelInPresentation = false,
            hiddenOnForm = true)
    public Integer getPlannedCoursesNumber() {

        return plannedCoursesNumber;
    }

    public void setPlannedCoursesNumber(Integer plannedCoursesNumber) {

        this.plannedCoursesNumber = plannedCoursesNumber;
    }

    @Transient
    @PropertyViewDescriptor(
            hiddenOnCard = true, hiddenOnGrid = true, hiddenOnList = true, hiddenOnPresentation = true,
            displayLabelInPresentation = false, inputType = FormInputType.LIST_OF_VALUES_MULTI_SELECT,
            order = 18, rendererType = ViewRendererType.MODEL_COLLECTION, groupName = "includedTags",
            orderInForm = 5)
    @PropertyViewDescriptor(
            hiddenOnCard = true, hiddenOnGrid = true, hiddenOnList = true, hiddenOnPresentation = true,
            displayLabelInPresentation = false, inputType = FormInputType.LIST_OF_VALUES_MULTI_SELECT,
            order = 18, rendererType = ViewRendererType.MODEL_COLLECTION, groupName = "includedTags",
            orderInForm = 5, view = ro.siveco.ael.lcms.domain.course.view.Views.ActivityView.class)
    @PropertyViewDescriptor(
            hiddenOnCard = true, hiddenOnGrid = true, hiddenOnList = true, hiddenOnPresentation = true,
            displayLabelInPresentation = false, inputType = FormInputType.LIST_OF_VALUES_MULTI_SELECT,
            order = 18, rendererType = ViewRendererType.MODEL_COLLECTION, groupName = "includedTags",
            orderInForm = 5, view = ro.siveco.ael.lcms.domain.course.view.Views.ServiceView.class)
    @PropertyViewDescriptor(
            hiddenOnCard = true, hiddenOnGrid = true, hiddenOnList = true, hiddenOnPresentation = true,
            displayLabelInPresentation = false, inputType = FormInputType.LIST_OF_VALUES_MULTI_SELECT,
            order = 18, rendererType = ViewRendererType.MODEL_COLLECTION, groupName = "includedTags",
            orderInForm = 5, view = ro.siveco.ael.lcms.domain.course.view.Views.MeetingView.class)
    public List<Tag> getIncludedTags() {

        return includedTags;
    }

    public void setIncludedTags(List<Tag> includedTags) {

        this.includedTags = includedTags;
    }

    @Transient
    @PropertyViewDescriptor(
            hidden = true, displayLabelInPresentation = false,
            order = 19, rendererType = ViewRendererType.MODEL_COLLECTION, groupName = "excludedTags",
            hiddenOnCreate = true)
    public List<Tag> getExcludedTags() {

        return excludedTags;
    }

    public void setExcludedTags(List<Tag> excludedTags) {

        this.excludedTags = excludedTags;
    }

    @Transient
    @PropertyViewDescriptor(hidden = true)
    public List<Tag> getOptionalTags() {

        return optionalTags;
    }

    public void setOptionalTags(List<Tag> optionalTags) {

        this.optionalTags = optionalTags;
    }

    @Transient
    @PropertyViewDescriptor(
            hiddenOnCard = true, hiddenOnList = true, hiddenOnGrid = true,
            order = 20, hiddenOnPresentation = true, displayLabelInPresentation = false,
            hiddenOnForm = true)
    public List<AelUser> getStudentsList() {

        return studentsList;
    }

    public void setStudentsList(List<AelUser> studentsList) {

        this.studentsList = studentsList;
    }

    @Transient
    @PropertyViewDescriptor(hiddenOnGrid = true, hiddenOnPresentation = true, hiddenOnList = true, hiddenOnCard = true,
            inputType = FormInputType.IMAGE_UPLOAD, displayLabelInForm = false,
            inputFieldSize = GridSize.TWO_PER_ROW,
            orderInForm = 2)
    @PropertyViewDescriptor(
            view = ro.siveco.ael.lcms.domain.course.view.Views.ActivityView.class,
            hiddenOnGrid = true, hiddenOnPresentation = true, hiddenOnList = true, hiddenOnCard = true,
            inputType = FormInputType.IMAGE_UPLOAD, displayLabelInForm = false,
            inputFieldSize = GridSize.TWO_PER_ROW,
            orderInForm = 2)
    @PropertyViewDescriptor(
            view = ro.siveco.ael.lcms.domain.course.view.Views.ServiceView.class,
            hiddenOnGrid = true, hiddenOnPresentation = true, hiddenOnList = true, hiddenOnCard = true,
            inputType = FormInputType.IMAGE_UPLOAD, displayLabelInForm = false,
            inputFieldSize = GridSize.TWO_PER_ROW,
            orderInForm = 2)
    @PropertyViewDescriptor(
            view = ro.siveco.ael.lcms.domain.course.view.Views.MeetingView.class,
            hiddenOnGrid = true, hiddenOnPresentation = true, hiddenOnList = true, hiddenOnCard = true,
            inputType = FormInputType.IMAGE_UPLOAD, displayLabelInForm = false,
            inputFieldSize = GridSize.TWO_PER_ROW,
            orderInForm = 2)
    public MultipartFile[] getImageMultipart() {

        return imageMultipart;
    }

    public void setImageMultipart(MultipartFile[] imageMultipart) {

        this.imageMultipart = imageMultipart;
    }


    @ManyToOne(targetEntity = AelUser.class)
    @JoinColumn(name = "owner_id")
    @PropertyViewDescriptor(hidden = true)
    public AelUser getOwner() {
        return owner;
    }

    public void setOwner(AelUser owner) {
        this.owner = owner;
    }

    @Transient
    @PropertyViewDescriptor(order = 2, rendererType = ViewRendererType.DATE,
            inputType = FormInputType.DATE, orderInForm = 7,
            inputFieldSize = GridSize.TWO_PER_ROW, hiddenOnPresentation = true,
            displayLabelInPresentation = false, hiddenOnCard = true)
    @PropertyViewDescriptor(
            view = ro.siveco.ael.lcms.domain.course.view.Views.ActivityView.class,
            order = 2, rendererType = ViewRendererType.DATE,
            inputType = FormInputType.DATE, orderInForm = 6,
            inputFieldSize = GridSize.TWO_PER_ROW,
            displayLabelInPresentation = false, hiddenOnCard = true)
    @PropertyViewDescriptor(
            view = ro.siveco.ael.lcms.domain.course.view.Views.ServiceView.class,
            order = 2, rendererType = ViewRendererType.DATE,
            inputType = FormInputType.DATE, orderInForm = 6,
            inputFieldSize = GridSize.TWO_PER_ROW,
            displayLabelInPresentation = false, hiddenOnCard = true)
    @PropertyViewDescriptor(
            view = ro.siveco.ael.lcms.domain.course.view.Views.MeetingView.class,
            order = 2, rendererType = ViewRendererType.DATE,
            inputType = FormInputType.DATE, orderInForm = 6,
            inputFieldSize = GridSize.TWO_PER_ROW,
            displayLabelInPresentation = false, hiddenOnCard = true)
    public Date getPlannedCourseStartDate() {
        return plannedCourseStartDate;
    }

    public void setPlannedCourseStartDate(Date plannedCourseStartDate) {
        this.plannedCourseStartDate = plannedCourseStartDate;
    }

    @Transient
    @PropertyViewDescriptor(order = 3, hiddenOnCard = true, rendererType = ViewRendererType.DATE,
            inputType = FormInputType.DATE, orderInForm = 8,
            inputFieldSize = GridSize.TWO_PER_ROW, hiddenOnPresentation = true)
    @PropertyViewDescriptor(
            view = ro.siveco.ael.lcms.domain.course.view.Views.ActivityView.class,
            order = 3, hiddenOnCard = true, rendererType = ViewRendererType.DATE,
            inputType = FormInputType.DATE, orderInForm = 7,
            inputFieldSize = GridSize.TWO_PER_ROW)
    @PropertyViewDescriptor(
            view = ro.siveco.ael.lcms.domain.course.view.Views.ServiceView.class,
            order = 3, hiddenOnCard = true, rendererType = ViewRendererType.DATE,
            inputType = FormInputType.DATE, orderInForm = 7,
            inputFieldSize = GridSize.TWO_PER_ROW)
    @PropertyViewDescriptor(
            view = ro.siveco.ael.lcms.domain.course.view.Views.MeetingView.class,
            order = 3, hiddenOnCard = true, rendererType = ViewRendererType.DATE,
            inputType = FormInputType.DATE, orderInForm = 7,
            inputFieldSize = GridSize.TWO_PER_ROW)
    public Date getPlannedCourseEndDate() {
        return plannedCourseEndDate;
    }

    public void setPlannedCourseEndDate(Date plannedCourseEndDate) {
        this.plannedCourseEndDate = plannedCourseEndDate;
    }

    @Transient
    @PropertyViewDescriptor(hiddenOnCard = true, rendererType = ViewRendererType.TIME,
            inputType = FormInputType.TIME, orderInForm = 9,
            inputFieldSize = GridSize.TWO_PER_ROW, hiddenOnPresentation = true)
    @PropertyViewDescriptor(
            view = ro.siveco.ael.lcms.domain.course.view.Views.ActivityView.class,
            hiddenOnCard = true, rendererType = ViewRendererType.TIME,
            inputType = FormInputType.TIME, orderInForm = 8,
            inputFieldSize = GridSize.TWO_PER_ROW)
    @PropertyViewDescriptor(
            view = ro.siveco.ael.lcms.domain.course.view.Views.ServiceView.class,
            hiddenOnCard = true, rendererType = ViewRendererType.TIME,
            inputType = FormInputType.TIME, orderInForm = 8,
            inputFieldSize = GridSize.TWO_PER_ROW)
    @PropertyViewDescriptor(
            view = ro.siveco.ael.lcms.domain.course.view.Views.MeetingView.class,
            hiddenOnCard = true, rendererType = ViewRendererType.TIME,
            inputType = FormInputType.TIME, orderInForm = 8,
            inputFieldSize = GridSize.TWO_PER_ROW)
    public Time getPlannedCourseStartTime() {
        return plannedCourseStartTime;
    }

    public void setPlannedCourseStartTime(Time plannedCourseStartTime) {
        this.plannedCourseStartTime = plannedCourseStartTime;
    }

    @Transient
    @PropertyViewDescriptor(hiddenOnCard = true, rendererType = ViewRendererType.TIME,
            inputType = FormInputType.TIME, orderInForm = 10,
            inputFieldSize = GridSize.TWO_PER_ROW, hiddenOnPresentation = true)
    @PropertyViewDescriptor(
            view = ro.siveco.ael.lcms.domain.course.view.Views.ActivityView.class,
            hiddenOnCard = true, rendererType = ViewRendererType.TIME,
            inputType = FormInputType.TIME, orderInForm = 9,
            inputFieldSize = GridSize.TWO_PER_ROW)
    @PropertyViewDescriptor(
            view = ro.siveco.ael.lcms.domain.course.view.Views.ServiceView.class,
            hiddenOnCard = true, rendererType = ViewRendererType.TIME,
            inputType = FormInputType.TIME, orderInForm = 9,
            inputFieldSize = GridSize.TWO_PER_ROW)
    @PropertyViewDescriptor(
            view = ro.siveco.ael.lcms.domain.course.view.Views.MeetingView.class,
            hiddenOnCard = true, rendererType = ViewRendererType.TIME,
            inputType = FormInputType.TIME, orderInForm = 9,
            inputFieldSize = GridSize.TWO_PER_ROW)
    public Time getPlannedCourseEndTime() {
        return plannedCourseEndTime;
    }

    public void setPlannedCourseEndTime(Time plannedCourseEndTime) {
        this.plannedCourseEndTime = plannedCourseEndTime;
    }

    @Transient
    @PropertyViewDescriptor(
            order=1, groupName = "scheduleLocation",
            hiddenOnCard = true, rendererType = ViewRendererType.HTML,
            inputType = FormInputType.TEXT, orderInForm = 11)
    @PropertyViewDescriptor(
            view = ro.siveco.ael.lcms.domain.course.view.Views.ActivityView.class,
            hiddenOnCard = true, rendererType = ViewRendererType.HTML, inputType = FormInputType.TEXT, orderInForm = 10)
    @PropertyViewDescriptor(
            view = ro.siveco.ael.lcms.domain.course.view.Views.ServiceView.class,
            hiddenOnCard = true, rendererType = ViewRendererType.HTML, inputType = FormInputType.TEXT, orderInForm = 10)
    @PropertyViewDescriptor(
            view = ro.siveco.ael.lcms.domain.course.view.Views.MeetingView.class,
            hiddenOnCard = true, rendererType = ViewRendererType.HTML, inputType = FormInputType.TEXT, orderInForm = 10)
    public String getItemLocation() {
        return itemLocation;
    }

    public void setItemLocation(String itemLocation) {
        this.itemLocation = itemLocation;
    }

    @Transient
    @PropertyViewDescriptor(inputType = FormInputType.HIDDEN, hiddenOnCard = true, hiddenOnGrid = true, hiddenOnList = true, hiddenOnPresentation = true, displayLabelInForm = false)
    @PropertyViewDescriptor(
            view = ro.siveco.ael.lcms.domain.course.view.Views.ActivityView.class,
            inputType = FormInputType.HIDDEN, hiddenOnCard = true, hiddenOnGrid = true, hiddenOnList = true, hiddenOnPresentation = true, displayLabelInForm = false)
    @PropertyViewDescriptor(
            view = ro.siveco.ael.lcms.domain.course.view.Views.ServiceView.class,
            inputType = FormInputType.HIDDEN, hiddenOnCard = true, hiddenOnGrid = true, hiddenOnList = true, hiddenOnPresentation = true, displayLabelInForm = false)
    @PropertyViewDescriptor(
            view = ro.siveco.ael.lcms.domain.course.view.Views.MeetingView.class,
            inputType = FormInputType.HIDDEN, hiddenOnCard = true, hiddenOnGrid = true, hiddenOnList = true, hiddenOnPresentation = true, displayLabelInForm = false)
    public String getItemLocationCoordinates() {
        return itemLocationCoordinates;
    }

    public void setItemLocationCoordinates(String itemLocationCoordinates) {
        this.itemLocationCoordinates = itemLocationCoordinates;
    }

    @Transient
    @PropertyViewDescriptor(hiddenOnCard = true, rendererType = ViewRendererType.INTEGER,
            inputType = FormInputType.NUMBER, orderInForm = 12, required = false,
            inputFieldSize = GridSize.TWO_PER_ROW, hiddenOnPresentation = true)
    @PropertyViewDescriptor(
            view = ro.siveco.ael.lcms.domain.course.view.Views.ActivityView.class,
            hiddenOnCard = true, rendererType = ViewRendererType.INTEGER,
            inputType = FormInputType.NUMBER, orderInForm = 11, required = false,
            inputFieldSize = GridSize.TWO_PER_ROW)
    @PropertyViewDescriptor(
            view = ro.siveco.ael.lcms.domain.course.view.Views.ServiceView.class,
            hiddenOnCard = true, rendererType = ViewRendererType.INTEGER,
            inputType = FormInputType.NUMBER, orderInForm = 11, required = false,
            inputFieldSize = GridSize.TWO_PER_ROW)
    @PropertyViewDescriptor(
            view = ro.siveco.ael.lcms.domain.course.view.Views.MeetingView.class,
            hiddenOnCard = true, rendererType = ViewRendererType.INTEGER,
            inputType = FormInputType.NUMBER, orderInForm = 11, required = false,
            inputFieldSize = GridSize.TWO_PER_ROW)
    public Integer getMinUserNumber() {
        return minUserNumber;
    }

    public void setMinUserNumber(Integer minUserNumber) {
        this.minUserNumber = minUserNumber;
    }

    @Transient
    @PropertyViewDescriptor(hiddenOnCard = true, rendererType = ViewRendererType.INTEGER,
            inputType = FormInputType.NUMBER, orderInForm = 13, required = false,
            inputFieldSize = GridSize.TWO_PER_ROW, hiddenOnPresentation = true)
    @PropertyViewDescriptor(
            view = ro.siveco.ael.lcms.domain.course.view.Views.ActivityView.class,
            hiddenOnCard = true, rendererType = ViewRendererType.INTEGER,
            inputType = FormInputType.NUMBER, orderInForm = 12, required = false,
            inputFieldSize = GridSize.TWO_PER_ROW)
    @PropertyViewDescriptor(
            view = ro.siveco.ael.lcms.domain.course.view.Views.ServiceView.class,
            hiddenOnCard = true, rendererType = ViewRendererType.INTEGER,
            inputType = FormInputType.NUMBER, orderInForm = 12,  required = false,
            inputFieldSize = GridSize.TWO_PER_ROW)
    @PropertyViewDescriptor(
            view = ro.siveco.ael.lcms.domain.course.view.Views.MeetingView.class,
            hiddenOnCard = true, rendererType = ViewRendererType.INTEGER,
            inputType = FormInputType.NUMBER, orderInForm = 12,  required = false,
            inputFieldSize = GridSize.TWO_PER_ROW)
    public Integer getMaxUserNumber() {
        return maxUserNumber;
    }

    public void setMaxUserNumber(Integer maxUserNumber) {
        this.maxUserNumber = maxUserNumber;
    }

    @Transient
    @PropertyViewDescriptor(order=21, rendererType = ViewRendererType.BUTTON,
            hiddenOnForm = true, hiddenOnPresentation = true,
            cssClass = "property-presentation model-course-property model-property-value model-property-readMoreLink " +
                    "generic-action action-open-presentation action-course-open-presentation btn meta-controller " +
                    "property-input-wrapper read-more-class",
            clickAction = {
                @ActionDescriptor( type = ActionType.OPEN_PRESENTATION),
                @ActionDescriptor(type = ActionType.LIST, backToFor = "{modelName}-OPEN_PRESENTATION", availableInPresentation = true)
            })
    @PropertyViewDescriptor(
            view = ro.siveco.ael.lcms.domain.course.view.Views.ActivityView.class,
            order=21, rendererType = ViewRendererType.BUTTON,
            hiddenOnForm = true, hiddenOnPresentation = true,
            cssClass = "property-presentation model-course-property model-property-value model-property-readMoreLink " +
                    "generic-action action-open-presentation action-course-open-presentation btn meta-controller " +
                    "property-input-wrapper read-more-class",
            clickAction = {
                    @ActionDescriptor( type = ActionType.OPEN_PRESENTATION),
                    @ActionDescriptor(type = ActionType.LIST, backToFor = "{modelName}-OPEN_PRESENTATION", availableInPresentation = true)
            })
    @PropertyViewDescriptor(
            view = ro.siveco.ael.lcms.domain.course.view.Views.ServiceView.class,
            order=21, rendererType = ViewRendererType.BUTTON,
            hiddenOnForm = true, hiddenOnPresentation = true,
            cssClass = "property-presentation model-course-property model-property-value model-property-readMoreLink " +
                    "generic-action action-open-presentation action-course-open-presentation btn meta-controller " +
                    "property-input-wrapper read-more-class",
            clickAction = {
                    @ActionDescriptor( type = ActionType.OPEN_PRESENTATION),
                    @ActionDescriptor(type = ActionType.LIST, backToFor = "{modelName}-OPEN_PRESENTATION", availableInPresentation = true)
            })
    @PropertyViewDescriptor(
            view = ro.siveco.ael.lcms.domain.course.view.Views.MeetingView.class,
            order=21, rendererType = ViewRendererType.BUTTON,
            hiddenOnForm = true, hiddenOnPresentation = true,
            cssClass = "property-presentation model-course-property model-property-value model-property-readMoreLink " +
                    "generic-action action-open-presentation action-course-open-presentation btn meta-controller " +
                    "property-input-wrapper read-more-class",
            clickAction = {
                    @ActionDescriptor( type = ActionType.OPEN_PRESENTATION),
                    @ActionDescriptor(type = ActionType.LIST, backToFor = "{modelName}-OPEN_PRESENTATION", availableInPresentation = true)
            })
    public String getReadMoreLink() {
        return readMoreLink;
    }

    public void setReadMoreLink(String readMoreLink) {
        this.readMoreLink = readMoreLink;
    }

    @Transient
    @PropertyViewDescriptor(
            order = 4, hiddenOnGrid = true, hiddenOnList = true, hiddenOnCard = true,
            hiddenOnForm = true, groupName = "itemOwner")
    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    @Transient
    @PropertyViewDescriptor(
            order = 5, hiddenOnGrid = true, hiddenOnList = true, hiddenOnCard = true,
            hiddenOnForm = true, groupName = "itemOwner")
    public Integer getOwnerItemsNumber() {
        return ownerItemsNumber;
    }

    public void setOwnerItemsNumber(Integer ownerItemsNumber) {
        this.ownerItemsNumber = ownerItemsNumber;
    }

    @Transient
    @PropertyViewDescriptor(
            order = 6, hiddenOnGrid = true, hiddenOnList = true, hiddenOnCard = true,
            hiddenOnForm = true, groupName = "itemOwnerDetails")
    public String getOwnerEmail() {
        return ownerEmail;
    }

    public void setOwnerEmail(String ownerEmail) {
        this.ownerEmail = ownerEmail;
    }

    @Transient
    @PropertyViewDescriptor(
            order = 7, hiddenOnGrid = true, hiddenOnList = true, hiddenOnCard = true,
            hiddenOnForm = true, groupName = "itemOwnerDetails")
    public String getOwnerPhone() {
        return ownerPhone;
    }

    public void setOwnerPhone(String ownerPhone) {
        this.ownerPhone = ownerPhone;
    }

    @Transient
    @PropertyViewDescriptor(
            order = 8, hiddenOnGrid = true, hiddenOnList = true, hiddenOnCard = true,
            hiddenOnForm = true, groupName = "schedulePeriod")
    public String getItemSchedulePeriod() {
        return itemSchedulePeriod;
    }

    public void setItemSchedulePeriod(String itemSchedulePeriod) {
        this.itemSchedulePeriod = itemSchedulePeriod;
    }

    @Transient
    @PropertyViewDescriptor(
            order = 9, hiddenOnGrid = true, hiddenOnList = true, hiddenOnCard = true,
            hiddenOnForm = true, groupName = "scheduleTimePeriod")
    public String getItemScheduleTimePeriod() {
        return itemScheduleTimePeriod;
    }

    public void setItemScheduleTimePeriod(String itemScheduleTimePeriod) {
        this.itemScheduleTimePeriod = itemScheduleTimePeriod;
    }

    @Transient
    @PropertyViewDescriptor(
            hiddenOnList = true, hiddenOnGrid = true, hiddenOnCard = true,
            rendererType = ViewRendererType.STRING,
            hiddenOnForm = true,
            order = 11, groupName = "itemRatingLike")
    public Integer getItemLikeNumber() {
        return itemLikeNumber;
    }

    public void setItemLikeNumber(Integer itemLikeNumber) {
        this.itemLikeNumber = itemLikeNumber;
    }

    @Transient
    @PropertyViewDescriptor(
            hiddenOnList = true, hiddenOnGrid = true, hiddenOnCard = true,
            rendererType = ViewRendererType.STRING,
            hiddenOnForm = true,
            order = 13, groupName = "itemRatingDislike")
    public Integer getItemDislikeNumber() {
        return itemDislikeNumber;
    }

    public void setItemDislikeNumber(Integer itemDislikeNumber) {
        this.itemDislikeNumber = itemDislikeNumber;
    }

    @Transient
    @PropertyViewDescriptor(
            hiddenOnCard = true, hiddenOnForm=true, displayLabelInForm = false,
            order = 14, displayLabelInPresentation = false,
            rendererType = ViewRendererType.IMAGE, groupName = "itemOwnerPicture")
    public String getOwnerImageSrc() {
        return ownerImageSrc;
    }

    public void setOwnerImageSrc(String ownerImageSrc) {
        this.ownerImageSrc = ownerImageSrc;
    }

    @Transient
    @PropertyViewDescriptor(inputType = FormInputType.HIDDEN, hiddenOnCard = true, hiddenOnGrid = true, hiddenOnList = true, hiddenOnPresentation = true, displayLabelInForm = false)
    @PropertyViewDescriptor(
            view = ro.siveco.ael.lcms.domain.course.view.Views.ActivityView.class,
            inputType = FormInputType.HIDDEN, hiddenOnCard = true, hiddenOnGrid = true, hiddenOnList = true, hiddenOnPresentation = true, displayLabelInForm = false)
    @PropertyViewDescriptor(
            view = ro.siveco.ael.lcms.domain.course.view.Views.ServiceView.class,
            inputType = FormInputType.HIDDEN, hiddenOnCard = true, hiddenOnGrid = true, hiddenOnList = true, hiddenOnPresentation = true, displayLabelInForm = false)
    @PropertyViewDescriptor(
            view = ro.siveco.ael.lcms.domain.course.view.Views.MeetingView.class,
            inputType = FormInputType.HIDDEN, hiddenOnCard = true, hiddenOnGrid = true, hiddenOnList = true, hiddenOnPresentation = true, displayLabelInForm = false)
    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Transient
    @PropertyViewDescriptor(inputType = FormInputType.HIDDEN, hiddenOnCard = true, hiddenOnGrid = true, hiddenOnList = true, hiddenOnPresentation = true, displayLabelInForm = false,
    cssClass = "input-field model-course-property model-property-value model-property-countryCode property-input-wrapper field-display-mode-inline")
    @PropertyViewDescriptor(
            view = ro.siveco.ael.lcms.domain.course.view.Views.ActivityView.class,
            inputType = FormInputType.HIDDEN, hiddenOnCard = true, hiddenOnGrid = true, hiddenOnList = true, hiddenOnPresentation = true, displayLabelInForm = false,
            cssClass = "input-field model-course-property model-property-value model-property-countryCode property-input-wrapper field-display-mode-inline")
    @PropertyViewDescriptor(
            view = ro.siveco.ael.lcms.domain.course.view.Views.ServiceView.class,
            inputType = FormInputType.HIDDEN, hiddenOnCard = true, hiddenOnGrid = true, hiddenOnList = true, hiddenOnPresentation = true, displayLabelInForm = false,
            cssClass = "input-field model-course-property model-property-value model-property-countryCode property-input-wrapper field-display-mode-inline")
    @PropertyViewDescriptor(
            view = ro.siveco.ael.lcms.domain.course.view.Views.MeetingView.class,
            inputType = FormInputType.HIDDEN, hiddenOnCard = true, hiddenOnGrid = true, hiddenOnList = true, hiddenOnPresentation = true, displayLabelInForm = false,
            cssClass = "input-field model-course-property model-property-value model-property-countryCode property-input-wrapper field-display-mode-inline")
    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    @Transient
    @PropertyViewDescriptor(hidden = true)
    public List<Interest> getAllInterests() {
        return allInterests;
    }

    public void setAllInterests(List<Interest> allInterests) {
        this.allInterests = allInterests;
    }

    @Transient
    @PropertyViewDescriptor(
            view = ro.siveco.ael.lcms.domain.course.view.Views.ActivityView.class,
            order = 2, rendererType = ViewRendererType.STRING, hiddenOnCreate = true, hiddenOnUpdate = true, hiddenOnForm = true, displayLabelInPresentation = false, hiddenOnPresentation = true
    )
    @PropertyViewDescriptor(
            view = ro.siveco.ael.lcms.domain.course.view.Views.ServiceView.class,
            order = 2, rendererType = ViewRendererType.STRING, hiddenOnCreate = true, hiddenOnUpdate = true, hiddenOnForm = true, displayLabelInPresentation = false, hiddenOnPresentation = true
    )
    @PropertyViewDescriptor(
            view = ro.siveco.ael.lcms.domain.course.view.Views.MeetingView.class,
            order = 2, rendererType = ViewRendererType.STRING, hiddenOnCreate = true, hiddenOnUpdate = true, hiddenOnForm = true, displayLabelInPresentation = false, hiddenOnPresentation = true
    )
    @PropertyViewDescriptor(
            order = 2, rendererType = ViewRendererType.STRING, hiddenOnCreate = true, hiddenOnUpdate = true, hiddenOnForm = true, displayLabelInPresentation = false, hiddenOnPresentation = true
    )
    public String getDisplayPlannedCourseStartDate() {
        return displayPlannedCourseStartDate;
    }

    public void setDisplayPlannedCourseStartDate(String displayPlannedCourseStartDate) {
        this.displayPlannedCourseStartDate = displayPlannedCourseStartDate;
    }

    @Transient
    @PropertyViewDescriptor(hidden = true)
    @PropertyViewDescriptor(
            view = ro.siveco.ael.lcms.domain.course.view.Views.ActivityView.class,
            hidden = true)
    @PropertyViewDescriptor(
            view = ro.siveco.ael.lcms.domain.course.view.Views.ServiceView.class,
            hidden = true)
    @PropertyViewDescriptor(
            view = ro.siveco.ael.lcms.domain.course.view.Views.MeetingView.class,
            hidden = true)
    public String getDisplayPlannedCourseEndDate() {
        return displayPlannedCourseEndDate;
    }

    public void setDisplayPlannedCourseEndDate(String displayPlannedCourseEndDate) {
        this.displayPlannedCourseEndDate = displayPlannedCourseEndDate;
    }

    @Transient
    @PropertyViewDescriptor(hidden = true)
    @PropertyViewDescriptor(
            view = ro.siveco.ael.lcms.domain.course.view.Views.ActivityView.class,
            hidden = true)
    @PropertyViewDescriptor(
            view = ro.siveco.ael.lcms.domain.course.view.Views.ServiceView.class,
            hidden = true)
    @PropertyViewDescriptor(
            view = ro.siveco.ael.lcms.domain.course.view.Views.MeetingView.class,
            hidden = true)
    public String getStartDateRaw() {
        return startDateRaw;
    }

    public void setStartDateRaw(String startDateRaw) {
        this.startDateRaw = startDateRaw;
    }

    @Transient
    @PropertyViewDescriptor(hidden = true)
    @PropertyViewDescriptor(
            view = ro.siveco.ael.lcms.domain.course.view.Views.ActivityView.class,
            hidden = true)
    @PropertyViewDescriptor(
            view = ro.siveco.ael.lcms.domain.course.view.Views.ServiceView.class,
            hidden = true)
    @PropertyViewDescriptor(
            view = ro.siveco.ael.lcms.domain.course.view.Views.MeetingView.class,
            hidden = true)
    public String getEndDateRaw() {
        return endDateRaw;
    }

    public void setEndDateRaw(String endDateRaw) {
        this.endDateRaw = endDateRaw;
    }

    @Transient
    @PropertyViewDescriptor(hidden = true)
    @PropertyViewDescriptor(
            view = ro.siveco.ael.lcms.domain.course.view.Views.ActivityView.class,
            hidden = true)
    @PropertyViewDescriptor(
            view = ro.siveco.ael.lcms.domain.course.view.Views.ServiceView.class,
            hidden = true)
    @PropertyViewDescriptor(
            view = ro.siveco.ael.lcms.domain.course.view.Views.MeetingView.class,
            hidden = true)
    public String getStartTimeRaw() {
        return startTimeRaw;
    }

    public void setStartTimeRaw(String startTimeRaw) {
        this.startTimeRaw = startTimeRaw;
    }

    @Transient
    @PropertyViewDescriptor(hidden = true)
    @PropertyViewDescriptor(
            view = ro.siveco.ael.lcms.domain.course.view.Views.ActivityView.class,
            hidden = true)
    @PropertyViewDescriptor(
            view = ro.siveco.ael.lcms.domain.course.view.Views.ServiceView.class,
            hidden = true)
    @PropertyViewDescriptor(
            view = ro.siveco.ael.lcms.domain.course.view.Views.MeetingView.class,
            hidden = true)
    public String getEndTimeRaw() {
        return endTimeRaw;
    }

    public void setEndTimeRaw(String endTimeRaw) {
        this.endTimeRaw = endTimeRaw;
    }

    @Transient
    @PropertyViewDescriptor(hidden = true)
    @PropertyViewDescriptor(
            view = ro.siveco.ael.lcms.domain.course.view.Views.ActivityView.class,
            hidden = true)
    @PropertyViewDescriptor(
            view = ro.siveco.ael.lcms.domain.course.view.Views.ServiceView.class,
            hidden = true)
    @PropertyViewDescriptor(
            view = ro.siveco.ael.lcms.domain.course.view.Views.MeetingView.class,
            hidden = true)
    public String getClientTimeZoneId() {
        return clientTimeZoneId;
    }

    public void setClientTimeZoneId(String clientTimeZoneId) {
        this.clientTimeZoneId = clientTimeZoneId;
    }

    @Transient
    @PropertyViewDescriptor(order = 4, hiddenOnGrid = true, hiddenOnList = true, hiddenOnCreate = true, hiddenOnUpdate = true, hiddenOnForm = true,
            groupName = "itemNameAndFavorites")
    public String getDisplayStudentsNumber() {
        return displayStudentsNumber;
    }

    public void setDisplayStudentsNumber(String displayStudentsNumber) {
        this.displayStudentsNumber = displayStudentsNumber;
    }

    @Transient
    @PropertyViewDescriptor(hidden = true)
    public Boolean getShowOwnerPhone() {
        return showOwnerPhone;
    }

    public void setShowOwnerPhone(Boolean showOwnerPhone) {
        this.showOwnerPhone = showOwnerPhone;
    }

    @Transient
    @PropertyViewDescriptor(hidden = true)
    public Boolean getShowOwnerEmail() {
        return showOwnerEmail;
    }

    public void setShowOwnerEmail(Boolean showOwnerEmail) {
        this.showOwnerEmail = showOwnerEmail;
    }

    @Transient
    @PropertyViewDescriptor(
            hiddenOnList = true, hiddenOnGrid = true, hiddenOnCard = true,hiddenOnPresentation = true,
            displayLabelInForm = false,rendererType = ViewRendererType.STRING,
            hiddenOnForm = true,  displayLabelInPresentation = false,
            order = 14, groupName = "courseComments")
    public List<Comment> getComments(){return comments;}

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    @Transient
    @PropertyViewDescriptor(hidden = true)
    public Boolean getAttending() {
        return attending;
    }

    public void setAttending(Boolean attending) {
        this.attending = attending;
    }

    @Transient
    @PropertyViewDescriptor(
            hiddenOnList = true, hiddenOnGrid = true, hiddenOnCard = true,
            rendererType = ViewRendererType.BUTTON,
            hiddenOnForm = true,
            cssClass = "property-presentation model-course-property model-property-value model-property-attendingBtn meta-controller property-input-wrapper btn no-preloader",
            order = 5,
            groupName = "itemNameAndFavorites",
            clickAction = @ActionDescriptor(actionId = "attendItemActionId", type = ActionType.CUSTOM, uri="/courses/attendItem/{id}", availableInPresentation=true))
    public String getAttendingBtn() {
        return attendingBtn;
    }

    @Transient
    @PropertyViewDescriptor(
            order = 6, hiddenOnGrid = true, hiddenOnList = true, hiddenOnCard = true, hiddenOnPresentation = true, displayLabelInPresentation = false,
            hiddenOnForm = true, groupName = "itemNameAndFavorites")
    public Integer getUsersAttending() {
        return usersAttending;
    }

    public void setUsersAttending(Integer usersAttending) {
        this.usersAttending = usersAttending;
    }

    @Transient
    @PropertyViewDescriptor(order = 7, hiddenOnGrid = true, hiddenOnList = true, hiddenOnCreate = true, hiddenOnUpdate = true, hiddenOnForm = true, hiddenOnCard = true,
            displayLabelInPresentation = false, groupName = "itemNameAndFavorites")
    public String getDisplayUsersAttending() {
        return displayUsersAttending;
    }

    public void setDisplayUsersAttending(String displayUsersAttending) {
        this.displayUsersAttending = displayUsersAttending;
    }

    @Transient
    @PropertyViewDescriptor(
            hiddenOnList = true, hiddenOnGrid = true, hiddenOnCard = true,
            rendererType = ViewRendererType.BUTTON,
            hiddenOnForm = true,
            cssClass = "property-presentation model-course-property model-property-value model-property-allAttendingBtn meta-controller property-input-wrapper btn no-preloader",
            order = 10,
            groupName = "itemNameAndFavorites")
    public String getAllAttendingBtn() {
        return allAttendingBtn;
    }

    @Transient
    @PropertyViewDescriptor(
            hiddenOnList = true, hiddenOnGrid = true, hiddenOnCard = true, order = 8,
            displayLabelInPresentation = false, hiddenOnForm = true, groupName = "itemNameAndFavorites")
    public List<AelUser> getAttendingStudentsForDisplay() {
        return attendingStudentsForDisplay;
    }

    public void setAttendingStudentsForDisplay(List<AelUser> attendingStudentsForDisplay) {
        this.attendingStudentsForDisplay = attendingStudentsForDisplay;
    }

    @Transient
    @PropertyViewDescriptor(
            hiddenOnList = true, hiddenOnGrid = true, hiddenOnCard = true, order = 9,
            displayLabelInPresentation = false, hiddenOnForm = true, groupName = "itemNameAndFavorites")
    public List<AelUser> getAllAttendingStudents() {
        return allAttendingStudents;
    }

    public void setAllAttendingStudents(List<AelUser> allAttendingStudents) {
        this.allAttendingStudents = allAttendingStudents;
    }
}
