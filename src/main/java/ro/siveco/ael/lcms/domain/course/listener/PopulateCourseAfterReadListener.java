package ro.siveco.ael.lcms.domain.course.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.course.model.Course;
import ro.siveco.ael.lcms.domain.course.service.CourseDecoratorService;
import ro.siveco.ram.event.listener.type.single.BaseAfterReadListener;
import ro.siveco.ram.event.type.single.AfterReadEvent;

/**
 * Created by AndradaC on 9/12/2017.
 */
@Component
public class PopulateCourseAfterReadListener extends BaseAfterReadListener<Course> {

    @Autowired
    private CourseDecoratorService courseDecoratorService;

    @Override
    public void onApplicationEvent(AfterReadEvent<Course> event) {

        Course course = event.getDomainModel();
        courseDecoratorService.populateCourse(course);
    }
}
