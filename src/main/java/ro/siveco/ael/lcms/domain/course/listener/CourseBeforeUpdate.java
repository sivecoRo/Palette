package ro.siveco.ael.lcms.domain.course.listener;

import org.apache.batik.transcoder.TranscoderException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.course.model.Course;
import ro.siveco.ael.lcms.domain.course.model.PlannedCourse;
import ro.siveco.ael.lcms.domain.course.service.CourseService;
import ro.siveco.ael.lcms.domain.course.service.PlannedCourseService;
import ro.siveco.ael.lcms.domain.metadata.service.MetadataService;
import ro.siveco.ael.service.MultimediaService;
import ro.siveco.ram.event.listener.type.single.BaseBeforeUpdateListener;
import ro.siveco.ram.event.type.single.BeforeUpdateEvent;

import java.io.ByteArrayOutputStream;
import java.util.List;

/**
 * User: AlexandruVi
 * Date: 2017-11-13
 */
@Component
public class CourseBeforeUpdate extends BaseBeforeUpdateListener<Course> {

    @Autowired
    private MultimediaService multimediaService;

    @Autowired
    private PlannedCourseService plannedCourseService;

    @Autowired
    private CourseService courseService;

    @Autowired
    private MetadataService metadataService;

    /**
     * Handle an application event.
     *
     * @param event the event to respond to
     */
    @Override
    public void onApplicationEvent(BeforeUpdateEvent<Course> event) {
        Course courseToUpdate = event.getDomainModel();
        Course courseFromDb = event.getStoredDomainModel();
        PlannedCourse plannedCourse = null;
        List<PlannedCourse> plannedCourses = plannedCourseService.findByCourseId(courseToUpdate.getId());
        if (plannedCourses.size() > 0) {
            plannedCourse = plannedCourses.get(0);
        }
        //plannedCourseService.updateDatesWithTimeZone(courseToUpdate);
        courseService.validateCourse(courseToUpdate, courseFromDb);

        if (courseToUpdate.getImageMultipart() != null && courseToUpdate.getImageMultipart().length > 0) {
            try {
                ByteArrayOutputStream baos = multimediaService.processThumbnail(courseToUpdate.getImageMultipart()[0]);
                courseToUpdate.setImage(baos.toByteArray());
            } catch (TranscoderException e) {
                e.printStackTrace();
            }
        } else {
            courseToUpdate.setImage(courseFromDb.getImage());
        }

        if (!courseToUpdate.getPlannedCourseStartDate().equals(plannedCourse.getStartDate()) ||
                (courseToUpdate.getPlannedCourseEndDate() == null && plannedCourse.getEndDate() != null) ||
                (courseToUpdate.getPlannedCourseEndDate() != null && plannedCourse.getEndDate() == null) ||
                (courseToUpdate.getPlannedCourseEndDate() != null && plannedCourse.getEndDate() != null && !courseToUpdate.getPlannedCourseEndDate().equals(plannedCourse.getEndDate())) ||
                (courseToUpdate.getPlannedCourseStartTime() != null && plannedCourse.getStartTime() == null) ||
                (courseToUpdate.getPlannedCourseStartTime() == null && plannedCourse.getStartTime() != null) ||
                (courseToUpdate.getPlannedCourseStartTime() != null && plannedCourse.getStartTime() != null && !courseToUpdate.getPlannedCourseStartTime().equals(plannedCourse.getStartTime())) ||
                (courseToUpdate.getPlannedCourseEndTime() != null && plannedCourse.getEndTime() == null) ||
                (courseToUpdate.getPlannedCourseEndTime() == null && plannedCourse.getEndTime() != null) ||
                (courseToUpdate.getPlannedCourseEndTime() != null && plannedCourse.getEndTime() != null && !courseToUpdate.getPlannedCourseEndTime().equals(plannedCourse.getEndTime()))
                ) {
            plannedCourseService.notifyAttendeesOnUpdateAvailability(courseToUpdate);
        }

        if ( (courseToUpdate.getItemLocation() != null && plannedCourse.getLocation() == null) ||
                (courseToUpdate.getItemLocation() == null && plannedCourse.getLocation() != null) ||
                (courseToUpdate.getItemLocation() != null && plannedCourse.getLocation() != null && !courseToUpdate.getItemLocation().equals(plannedCourse.getLocation()))
                ) {
            plannedCourseService.notifyAttendeesOnUpdateLocation(courseToUpdate);
        }

        plannedCourseService.updateDates(courseToUpdate);
        metadataService.updateMetadata(courseToUpdate);

    }
}
