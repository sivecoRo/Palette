package ro.siveco.ael.lcms.domain.materials.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.materials.model.Survey;
import ro.siveco.ael.lcms.domain.materials.service.SurveyService;
import ro.siveco.ram.event.listener.type.single.BaseBeforeUpdateListener;
import ro.siveco.ram.event.type.single.BeforeUpdateEvent;

/**
 * Created by AndradaC on 8/31/2017.
 */
@Component
public class SetSurveyInformationBeforeUpdateListener extends BaseBeforeUpdateListener<Survey>{

    @Autowired
    private SurveyService surveyService;

    @Override
    public void onApplicationEvent(BeforeUpdateEvent<Survey> event) {
        Survey survey = event.getDomainModel();
        Survey databaseSurvey = event.getStoredDomainModel();

        survey = surveyService.updateSurveyInformation(survey);
    }
}
