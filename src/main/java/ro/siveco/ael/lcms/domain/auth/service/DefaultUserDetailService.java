package ro.siveco.ael.lcms.domain.auth.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ro.siveco.ael.lcms.domain.auth.model.DefaultAuthenticatedUser;
import ro.siveco.ael.lcms.domain.auth.model.User;
import ro.siveco.ael.service.BaseService;

import javax.transaction.Transactional;

/**
 * User: AlexandruVi
 * Date: 2017-02-20
 */
@Service
@Transactional
public class DefaultUserDetailService extends BaseService implements UserDetailsService {

    private static final String USER_NOT_FOUND_TEMPLATE = "User with username=%s was not found!";

    private final UserService userService;
    private final PermissionService permissionService;

    @Autowired
    public DefaultUserDetailService(UserService userService, PermissionService permissionService) {
        this.userService = userService;
        this.permissionService = permissionService;
    }

    /**
     * Locates the user based on the username. In the actual implementation, the search
     * may possibly be case sensitive, or case insensitive depending on how the
     * implementation instance is configured. In this case, the <code>UserDetails</code>
     * object that comes back may have a username that is of a different case than what
     * was actually requested..
     *
     * @param username the username identifying the user whose data is required.
     * @return a fully populated user record (never <code>null</code>)
     * @throws UsernameNotFoundException if the user could not be found or the user has no
     *                                   GrantedAuthority
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = null;
        try {
            user = userService.getUserByUsername(username)
                    .orElseThrow(() -> new UsernameNotFoundException(
                            String.format(USER_NOT_FOUND_TEMPLATE, username)));
            return new DefaultAuthenticatedUser(user, permissionService.loadUserPermissions(user));
        } catch (UsernameNotFoundException e) {
            getLogger().warning(e.getMessage());
            throw e;
        }

    }
}
