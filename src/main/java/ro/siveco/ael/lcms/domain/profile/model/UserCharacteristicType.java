package ro.siveco.ael.lcms.domain.profile.model;

public enum UserCharacteristicType {
	INTEGER( "user.profile.characteristic-type.number" ) , 
	STRING( "user.profile.characteristic-type.string" ) , 
	BOOLEAN( "user.profile.characteristic-type.boolean" ), 
	TAG( "user.profile.characteristic-type.tag" ), 
	TAG_LIST( "user.profile.characteristic-type.tag-list" );
	
	
	UserCharacteristicType( String name )
	{
		this.name = name;
	}

	public String getName()
	{
		return name;
	}

	private String name;
}
