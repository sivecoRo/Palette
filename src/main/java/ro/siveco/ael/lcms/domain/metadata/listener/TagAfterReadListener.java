package ro.siveco.ael.lcms.domain.metadata.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.auth.model.AelUser;
import ro.siveco.ael.lcms.domain.auth.service.AelUserService;
import ro.siveco.ael.lcms.domain.metadata.model.Tag;
import ro.siveco.ael.lcms.domain.metadata.service.TagService;
import ro.siveco.ael.web.controllers.WithUserBaseEntityController;
import ro.siveco.ram.event.listener.type.single.BaseAfterReadListener;
import ro.siveco.ram.event.type.single.AfterReadEvent;

import java.util.Locale;

@Component
public class TagAfterReadListener extends BaseAfterReadListener<Tag> {


    @Autowired
    TagService tagService;

    @Autowired
    AelUserService aelUserService;

    @Override
    public void onApplicationEvent(AfterReadEvent<Tag> event) {
        Tag tag = event.getDomainModel();
        AelUser currentUser = (AelUser) WithUserBaseEntityController.getUser();
        Locale userLocale = aelUserService.getLocaleForUser(currentUser);
        String language = userLocale.getLanguage();
        tagService.setTagDisplayName(tag, language);
    }
}
