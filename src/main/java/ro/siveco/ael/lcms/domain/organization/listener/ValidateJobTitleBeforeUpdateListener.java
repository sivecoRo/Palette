package ro.siveco.ael.lcms.domain.organization.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.organization.model.JobTitle;
import ro.siveco.ael.lcms.domain.organization.service.JobTitleService;
import ro.siveco.ram.app.exception.AppException;
import ro.siveco.ram.event.listener.type.single.BaseBeforeUpdateListener;
import ro.siveco.ram.event.type.single.BeforeUpdateEvent;

/**
 * Created by AndradaC on 9/3/2017.
 */
@Component
public class ValidateJobTitleBeforeUpdateListener extends BaseBeforeUpdateListener<JobTitle>{

    @Autowired
    private JobTitleService jobTitleService;

    @Override
    public void onApplicationEvent(BeforeUpdateEvent<JobTitle> event) {
        JobTitle jobTitle = event.getDomainModel();
        if(!jobTitleService.validateJobTitle(jobTitle)){
            throw new AppException("error.jobTitle.update.duplicateEntry");
        }
    }
}
