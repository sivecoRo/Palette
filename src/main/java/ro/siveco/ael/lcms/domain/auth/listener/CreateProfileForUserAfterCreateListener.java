package ro.siveco.ael.lcms.domain.auth.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.auth.model.AelUser;
import ro.siveco.ael.lcms.domain.profile.model.UserDetails;
import ro.siveco.ael.lcms.domain.profile.service.UserDetailsService;
import ro.siveco.ram.event.listener.type.single.BaseAfterCreateListener;
import ro.siveco.ram.event.type.single.AfterCreateEvent;

@Component
public class CreateProfileForUserAfterCreateListener extends BaseAfterCreateListener<AelUser>{

    @Autowired
    private UserDetailsService userDetailsService;

    @Override
    public void onApplicationEvent(AfterCreateEvent<AelUser> event) {
        AelUser user = event.getDomainModel();
        if(user != null)
        {
            UserDetails userDetails = new UserDetails();
            userDetails.setUser(user);
            userDetailsService.save(userDetails);
        }
    }
}
