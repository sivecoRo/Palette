package ro.siveco.ael.lcms.domain.notification.listener;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.notification.model.PlannedNotification;
import ro.siveco.ram.event.listener.type.single.BaseBeforeUpdateListener;
import ro.siveco.ram.event.type.single.BeforeUpdateEvent;

/**
 * Created by IuliaP on 29.09.2017.
 */
@Order(value = 3)
@Component
public class SetPropertiesForPlannedNotificationBeforeUpdateListener
        extends BaseBeforeUpdateListener<PlannedNotification> {

    @Override
    public void onApplicationEvent(BeforeUpdateEvent<PlannedNotification> event) {

        PlannedNotification plannedNotification = event.getDomainModel();
        plannedNotification.setEntityId(plannedNotification.getPlannedCourse().getId());
    }
}
