package ro.siveco.ael.lcms.domain.auth.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.auth.model.AelGroup;
import ro.siveco.ael.lcms.domain.auth.model.AelRole;
import ro.siveco.ael.lcms.domain.auth.model.MultiSelectDTO;
import ro.siveco.ael.lcms.domain.auth.service.AelRoleService;
import ro.siveco.ael.lcms.domain.tenant.model.Tenant;
import ro.siveco.ael.web.controllers.WithUserBaseEntityController;
import ro.siveco.ram.event.listener.type.single.BaseAfterReadListener;
import ro.siveco.ram.event.type.single.AfterReadEvent;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by AndradaC on 9/1/2017.
 */
@Component
public class SetMultiselectRoleListForGroupAfterReadListener extends BaseAfterReadListener<AelGroup>{

    @Autowired
    private AelRoleService aelRoleService;

    @Override
    public void onApplicationEvent(AfterReadEvent<AelGroup> event) {
        AelGroup aelGroup = event.getDomainModel();

        Tenant tenant = WithUserBaseEntityController.getUser().getTenant();
        List<AelRole> aelRoleList = aelRoleService.getAelRoles(tenant);
        List<MultiSelectDTO> multiSelectDTOs = new ArrayList<>();

        for (AelRole aelRole : aelRoleList) {
            MultiSelectDTO multiSelectDTO = new MultiSelectDTO();
            multiSelectDTO.setId(aelRole.getId());
            multiSelectDTO.setName(aelRole.getName());
            multiSelectDTO.setStatus(0L);
            multiSelectDTOs.add(multiSelectDTO);
        }

        aelGroup.setListAllRoles(multiSelectDTOs);

    }

}
