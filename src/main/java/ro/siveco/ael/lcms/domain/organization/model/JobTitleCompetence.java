package ro.siveco.ael.lcms.domain.organization.model;

import org.springframework.data.domain.Sort;
import ro.siveco.ael.lcms.domain.taxonomies.model.Competence;
import ro.siveco.ael.lcms.domain.taxonomies.model.TaxonomyLevel;
import ro.siveco.ram.starter.ui.config.PropertyViewDescriptor;
import ro.siveco.ram.starter.ui.config.Sorter;
import ro.siveco.ram.starter.ui.config.ViewDescriptor;
import ro.siveco.ram.starter.ui.model.FormInputType;
import ro.siveco.ram.starter.ui.model.IndexViewType;
import ro.siveco.ram.starter.ui.model.ViewRendererType;

import javax.persistence.*;

/**
 * Created by JohnB on 21/06/2017.
 */

@Entity
@Table( name = "job_title_competences" )
@SequenceGenerator( name = "job_title_competences_seq", sequenceName = "job_title_competences_seq" )
//@TableComment(comment = "Stores the system's associations between job titles and competences")

@ViewDescriptor(paginationEnabled = true, viewTypes = {IndexViewType.GRID}, defaultViewType = IndexViewType.GRID,
        searchEnabled = true, defaultSearchProperty = "competence.label",
        sort = {@Sorter(propertyName = "competence.label", direction = Sort.Direction.ASC),
                @Sorter(propertyName = "taxonomyLevel.name", direction = Sort.Direction.ASC)})
public class JobTitleCompetence extends ro.siveco.ram.starter.model.base.BaseEntity
{
	private JobTitle jobTitle;

	private Competence competence;

	private TaxonomyLevel taxonomyLevel;

	public JobTitleCompetence(){}

	public JobTitleCompetence(JobTitle jobTitle, Competence competence, TaxonomyLevel taxonomyLevel) {
		this.jobTitle = jobTitle;
		this.competence = competence;
		this.taxonomyLevel = taxonomyLevel;
	}

	@Id
	@GeneratedValue( generator = "job_title_competences_seq", strategy = GenerationType.AUTO )
	public Long getId()
	{
		return super.getId();
	}

    @ManyToOne
    @JoinColumn(name = "job_title_id", nullable = false )
    @PropertyViewDescriptor(inputType = FormInputType.LIST_OF_VALUES, rendererType = ViewRendererType.MODEL,
            editInline = false, hidden = true, required = true, hiddenOnForm = true)
    public JobTitle getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(JobTitle jobTitle) {
        this.jobTitle = jobTitle;
    }

	@ManyToOne
	@JoinColumn(name = "competence_id", nullable = false )
	@PropertyViewDescriptor(inputType = FormInputType.LIST_OF_VALUES, rendererType = ViewRendererType.MODEL,
            editInline = false, hidden = false, required = true, hiddenOnForm = false)
    public Competence getCompetence() {
		return competence;
	}

	public void setCompetence(Competence competence) {
		this.competence = competence;
	}

	@ManyToOne
	@JoinColumn(name = "taxonomy_level_id", nullable = true )
	@PropertyViewDescriptor(inputType = FormInputType.LIST_OF_VALUES, rendererType = ViewRendererType.MODEL,
            editInline = false, hidden = false, required = true, hiddenOnForm = false)
    public TaxonomyLevel getTaxonomyLevel() {
		return taxonomyLevel;
	}

	public void setTaxonomyLevel(TaxonomyLevel taxonomyLevel) {
		this.taxonomyLevel = taxonomyLevel;
	}

    @Override
    public String toString() {
        return "Competence " + (this.getCompetence() != null ? this.getCompetence().getLabel() : "") + " for job title " +  (this.getJobTitle()!=null ? this.getJobTitle().getName() : "");
    }
}