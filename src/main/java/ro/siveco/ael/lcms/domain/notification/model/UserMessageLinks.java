package ro.siveco.ael.lcms.domain.notification.model;

/**
 * Created with IntelliJ IDEA.
 * User: razvans
 * Date: 8/12/13
 * Time: 2:11 PM
 */
public class UserMessageLinks
{
	public static final String LEARNING_COURSE_SUMMARY_LINK = "/training-management/learning/opel/showOpelCourse.action?plannedCourseId={0}";
	public static final String TEACHING_COURSE_SUMMARY_LINK = "/training-management/teaching/summary.action?plannedCourse.id={0}";
	public static final String PARENTS_ACTION_LINK = "/menu/parents.action";
	public static final String LEARNING_SHOW_ASSIGNMENT = "/training-management/learning/assignments/show-assignment-page.action?studentAssignment.id={0}";
	public static final String TEACHING_ASSIGNMENT_REPLY_LINK = "/training-management/assignments/showReply.action?solvedAssignment.id={0}&target=OwnAssignments&learningAndShowSolved=false";
	public static final String SELECT_PARTICIPANT_LINK = "/training-management/teaching/schedule/manageParticipants/getParticipationFilters.action?backUrl=/ael/training-management/teaching/summary.action&plannedCourse.id={0}";
	public static final String EVALUATION_LINK = "/training-management/learning/opel/showOpelCourse.action?plannedCourseId={0}";
	public static final String UPLOAD_DOWNLOAD_MANAGER_GUIDE_FILE_LINK = "/training/download.do?plannedCourse.id={0}";
	public static final String LEARNING_GRADES_LINK = "/training-management/learning/grades/filter.action";
	public static final String LEARNING_ABSENCES_LINK = "/training-management/learning/absences/filter.action";
	public static final String CONTENT_APPROVAL_LINK = "/crf/approvecontent/list.action";
	public static final String CONTENT_SHARED_LINK = "/crf/bookmarks/list.action";
	public static final String LEARNING_COURSE_GRADES_LINK = "/training-management/learning/grades/filter.action?activeTab=2";
	public static final String LEARNING_SUBJECTS_GRADES_LINK = "/training-management/learning/grades/filter.action?activeTab=0";
	public static final String LEARNING_ASSIGNMENTS_GRADES_LINK = "/training-management/learning/grades/filter.action?activeTab=4";
	public static final String QUOTA_UPDATED_LINK = "/crf/list.action";
	public static final String MOJ_COURSE_PROPOSALS_REPORT_LINK = "/menu/nomenclator.do?report=courseProposal";
	public static final String MOJ_MY_COURSE_PROPOSALS_LINK = "/menu/nomenclator.do?entity=topicProposalEntity";
    //public static final String TEACHING_TEST_REPORT_LINK = "/training-management/reporting/listLessonsReport.action?plannedCourse.id={0}";
    public static final String TEACHING_TEST_REPORT_LINK = "/menu/nomenclator.do?report=rlo&plannedCourse={0}";
    public static final String TEACHING_TEST_RESULT_EXPORT_LINK = "/qti/authoring/export/exportStudentAnswers.do?learner.id={1}&plannedCourse.id={0}&componentId={2}";

	public static final String PROMOTE_TO_PUBLIC_LINK = "/crf/list.action#/{0}";
	public static final String ACTIVATE_ACCOUNT_LINK = "/accounts/{0}/activate.do";
	public static final String RESET_PASSWORD_LINK = "/accounts/{0}/reset.do";
}
