package ro.siveco.ael.lcms.domain.auth.service;

public class ProfileVerificationResult {

    public static final String WRONG_TOKEN_ERR = "profile.verification.error.wrong_token";
    public static final String EMAIL_NOT_FOUND_ERR = "profile.verification.error.email_not_found";
    public static final String BAD_EMAIL_ERR = "profile.verification.error.bad_email";
    public static final String ALREADY_VERIFIED_ERR = "profile.verification.error.already_verified";
    public static final String GENERAL_ERR = "profile.verification.error.general";
    public static final String TOO_MANY_VERIFICATION_EMAIL_RESEND_REQUESTS_ERR
            = "profile.verification.error.too_many_verification_email_resend_requests";

    private String errorMessage;
    private String errorType;


    public ProfileVerificationResult(String errorType) {
        this.errorType = errorType;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public boolean isSuccess(){
        return errorType == null;
    }

    public boolean isError(){
        return !isSuccess();
    }


    public static final ProfileVerificationResult  SUCCESS = new ProfileVerificationResult(null);

    public static final ProfileVerificationResult  ERROR = new ProfileVerificationResult(GENERAL_ERR);

    public static ProfileVerificationResult wrongToken(String errorMessage){

        return build(WRONG_TOKEN_ERR, errorMessage);

    }

    public static ProfileVerificationResult emailNotFound(String errorMessage){

        return build(EMAIL_NOT_FOUND_ERR, errorMessage);

    }

    public static ProfileVerificationResult badEmail(String errorMessage){

        return build(BAD_EMAIL_ERR, errorMessage);

    }

    public static ProfileVerificationResult alreadyVerified(String errorMessage){

        return build(ALREADY_VERIFIED_ERR, errorMessage);

    }

    public static ProfileVerificationResult tooManyVerificationEmailResendRequests(String errorMessage){

        return build(TOO_MANY_VERIFICATION_EMAIL_RESEND_REQUESTS_ERR, errorMessage);

    }

    public static ProfileVerificationResult build(String errorType, String errorMessage){

        ProfileVerificationResult result = new ProfileVerificationResult(errorType);
        result.setErrorMessage(errorMessage);

        return result;

    }




}
