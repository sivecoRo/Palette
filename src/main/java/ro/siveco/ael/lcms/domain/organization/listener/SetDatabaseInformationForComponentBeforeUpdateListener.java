package ro.siveco.ael.lcms.domain.organization.listener;

import org.springframework.core.annotation.Order;
import ro.siveco.ael.lcms.domain.organization.model.Component;
import ro.siveco.ram.event.listener.type.single.BaseBeforeUpdateListener;
import ro.siveco.ram.event.type.single.BeforeUpdateEvent;

/**
 * Created by AndradaC on 9/1/2017.
 */
@Order(value = 1)
@org.springframework.stereotype.Component
public class SetDatabaseInformationForComponentBeforeUpdateListener extends BaseBeforeUpdateListener<Component>{

    @Override
    public void onApplicationEvent(BeforeUpdateEvent<Component> event) {
        Component component = event.getDomainModel();
        Component dbComponent = event.getStoredDomainModel();

        component.setTenant(dbComponent.getTenant());
        component.setRelatedEntityId(dbComponent.getRelatedEntityId());
        component.setParent(dbComponent.getParent());
//        component.setChildren(dbComponent.getChildren());
        component.setExternalId(dbComponent.getExternalId());
        component.setCompleteName(component.getName());
    }
}
