package ro.siveco.ael.lcms.domain.organization.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.organization.model.JobTitleCompetence;
import ro.siveco.ael.lcms.domain.organization.service.JobTitleCompetenceService;
import ro.siveco.ram.app.exception.AppException;
import ro.siveco.ram.event.listener.type.single.BaseBeforeUpdateListener;
import ro.siveco.ram.event.type.single.BeforeUpdateEvent;

/**
 * Created by AndradaC on 9/3/2017.
 */
@Component
public class ValidateJobTitleCompetenceBeforeUpdateListener extends BaseBeforeUpdateListener<JobTitleCompetence>{

    @Autowired
    private JobTitleCompetenceService jobTitleCompetenceService;

    @Override
    public void onApplicationEvent(BeforeUpdateEvent<JobTitleCompetence> event) {
        JobTitleCompetence jobTitleCompetence = event.getDomainModel();
        if(jobTitleCompetenceService.previousConnexionExists(jobTitleCompetence)){
            throw new AppException("error.jobTitleCompetence.update.duplicateEntry");
        }
    }
}
