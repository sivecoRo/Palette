package ro.siveco.ael.lcms.domain.notification.model;

import ro.siveco.ram.starter.ui.config.ViewDescriptor;
import ro.siveco.ram.starter.ui.model.IndexViewType;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by IuliaP on 27.09.2017.
 */
@Entity
@Table(name = "message_templates",
        uniqueConstraints = {@UniqueConstraint(columnNames = {"name", "locale", "tenant_id"})})
@SequenceGenerator(name = "message_templates_seq", sequenceName = "message_templates_seq")
@ViewDescriptor(pageSize = 10, viewTypes = {IndexViewType.CARD}, searchEnabled = true, defaultSearchProperty = "name")
public class MessageTemplate extends ro.siveco.ram.starter.model.base.BaseEntity {

    private String name;

    private String locale;

    private Long tenantId;

    private String content;

    @Id
    @GeneratedValue(generator = "message_templates_seq", strategy = GenerationType.SEQUENCE)
    public Long getId() {

        return super.getId();
    }

    @Column(length = 255)
    @NotNull
    public String getName() {

        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

    @Column(length = 100)
    public String getLocale() {

        return locale;
    }

    public void setLocale(String locale) {

        this.locale = locale;
    }

    @Column(name = "tenant_id")
    public Long getTenantId() {

        return tenantId;
    }

    public void setTenantId(Long tenantId) {

        this.tenantId = tenantId;
    }

    @Column
    public String getContent() {

        return content;
    }

    public void setContent(String content) {

        this.content = content;
    }
}
