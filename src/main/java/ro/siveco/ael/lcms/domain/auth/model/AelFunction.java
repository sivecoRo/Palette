package ro.siveco.ael.lcms.domain.auth.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.domain.Sort;
import ro.siveco.ram.starter.ui.config.PropertyViewDescriptor;
import ro.siveco.ram.starter.ui.config.Sorter;
import ro.siveco.ram.starter.ui.config.ViewDescriptor;
import ro.siveco.ram.starter.ui.model.IndexViewType;
import ro.siveco.ram.starter.ui.model.ViewRendererType;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "functions")
@SequenceGenerator(name = "SEQ_GEN", sequenceName = "functions_seq")
@ViewDescriptor(pageSize = 10, viewTypes = {IndexViewType.LIST},
        defaultViewType = IndexViewType.LIST,
        searchEnabled = true, defaultSearchProperty = "name",
        sort = {@Sorter(propertyName = "name", direction = Sort.Direction.ASC)},
        defaultActions = {})
//@TableComment(comment = "Stores the system's security functions")
public class AelFunction extends ro.siveco.ram.starter.model.base.BaseEntity implements Function {

    //@ColumnComment(comment = "The security function's display name")
    private String name;

//    private List<AelRole> roles = new ArrayList<>();

    //transient fields for form
    List<MultiSelectDTO> listAllRole = new ArrayList<>();

    @Id
    @PropertyViewDescriptor(hidden = true, required = false, hiddenOnForm = true)
    @GeneratedValue(generator = "functions_seq", strategy = GenerationType.SEQUENCE)
    public Long getId() {
        return super.getId();
    }

    @Column(unique = true, nullable = false, length = 500)
    @PropertyViewDescriptor(hidden = true)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

//    @ManyToMany(
//            targetEntity = AelRole.class,
//            cascade = {CascadeType.ALL}
//    )
//    @JoinTable(
//            name = "role_function",
//            joinColumns = {@JoinColumn(name = "function_id")},
//            inverseJoinColumns = {@JoinColumn(name = "role_id")},
//            uniqueConstraints = {@UniqueConstraint(columnNames = {"role_id", "function_id"})}
//    )
//    @JsonIgnore
//    @PropertyViewDescriptor(hidden = true)
//    public List<AelRole> getRoles() {
//        return roles;
//    }
//
//    public void setRoles(List<AelRole> roles) {
//        this.roles = roles;
//    }

    @Transient
    @PropertyViewDescriptor(hiddenOnList = true)
    public List<MultiSelectDTO> getListAllRole() {
        return listAllRole;
    }

    public void setListAllRole(List<MultiSelectDTO> listAllRole) {
        this.listAllRole = listAllRole;
    }

    @Override
    public String toString() {
        return this.getName();
    }

    @Override
    @Transient
    @PropertyViewDescriptor(
            order = -2,/* First property */
            hiddenOnForm = true,
            hiddenOnGrid = true,
            displayLabelInPresentation = false,
            rendererType = ViewRendererType.OBJECT_LABEL,
            clickAction = {})
    public String getObjectLabel() {

        return super.getObjectLabel();
    }
}
