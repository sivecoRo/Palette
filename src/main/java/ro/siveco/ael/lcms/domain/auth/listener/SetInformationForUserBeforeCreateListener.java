package ro.siveco.ael.lcms.domain.auth.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import ro.siveco.ael.lcms.domain.auth.model.AelUser;
import ro.siveco.ael.lcms.domain.auth.model.User;
import ro.siveco.ael.lcms.domain.auth.service.AelUserImporterService;
import ro.siveco.ael.lcms.domain.auth.service.AelUserService;
import ro.siveco.ael.lcms.domain.auth.web.UserController;
import ro.siveco.ael.lcms.domain.tenant.model.Tenant;
import ro.siveco.ael.web.controllers.WithUserBaseEntityController;
import ro.siveco.ram.app.exception.AppException;
import ro.siveco.ram.event.listener.type.single.BaseBeforeCreateListener;
import ro.siveco.ram.event.type.single.BeforeCreateEvent;
import ro.siveco.ram.starter.ui.model.Views;

import java.util.Date;

/**
 * Created by AndradaC on 9/1/2017.
 */
@Order(value = 2)
@Component
public class SetInformationForUserBeforeCreateListener extends BaseBeforeCreateListener<AelUser> {

    @Autowired
    private ShaPasswordEncoder passwordEncoder;

    @Autowired
    private AelUserService aelUserService;

    @Autowired
    private AelUserImporterService aelUserImporterService;

    @Override
    public void onApplicationEvent(BeforeCreateEvent<AelUser> event) {

        if (event.getOptionalDomainModel().isPresent()) {
            AelUser aelUser = event.getOptionalDomainModel().get();
            User creator = WithUserBaseEntityController.getUser();
            Tenant tenant = creator.getTenant();

            UserController controller = (UserController) event.getSource();
            Class<? extends Views.View> view = controller.getRequestedView();
            if (ro.siveco.ael.lcms.domain.auth.view.Views.ImportUserView.class.getName().equals(view.getName())) {
                MultipartFile[] importFiles = aelUser.getImportUserTemplate();
                if (importFiles != null && importFiles.length > 0) {
                    MultipartFile usersFile = importFiles[0];
                    try {
                        aelUserImporterService.importUsers(usersFile, tenant);
                    }
                    catch (AppException e)
                    {
                        throw e;
                    }
                    catch (Exception e)
                    {
                        
                    }
                }

                event.setDomainModel(null);

            } else {

                String password = passwordEncoder.encodePassword(aelUser.getPassword(), null);
                aelUser.setPassword(password);
                aelUser.setDisplayName(aelUserService.getUserDisplayName(aelUser));
                aelUser.setRelatedUid(aelUser.getUsername());
                aelUser.setTenant(tenant);
                aelUser.setCreateDate(new Date());
            }

        }
    }
}
