package ro.siveco.ael.lcms.domain.notification.service;

/**
 * Created by IuliaP on 29.09.2017.
 */

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.siveco.ael.lcms.domain.notification.model.UserMessageFormat;
import ro.siveco.ael.lcms.repository.UserMessageFormatRepository;
import ro.siveco.ram.starter.service.BaseEntityService;

@Service
public class UserMessageFormatService extends BaseEntityService<UserMessageFormat> {

    @Autowired
    public UserMessageFormatService(UserMessageFormatRepository userMessageFormatRepository) {

        super(UserMessageFormat.class, userMessageFormatRepository);
    }

    public UserMessageFormat findOneByCode(String code) {

        return ((UserMessageFormatRepository) getResourceRepository()).findOneByCode(code);
    }

    public UserMessageFormat findOneByName(String name) {

        return ((UserMessageFormatRepository) getResourceRepository()).findOneByName(name);
    }

}
