package ro.siveco.ael.lcms.domain.metadata.model;

import org.springframework.data.domain.Sort;
import ro.siveco.ram.starter.ui.config.PropertyViewDescriptor;
import ro.siveco.ram.starter.ui.config.Sorter;
import ro.siveco.ram.starter.ui.config.ViewDescriptor;
import ro.siveco.ram.starter.ui.model.IndexViewType;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by IuliaP on 09.06.2017.
 */
@Entity
@Table(name = "tags", uniqueConstraints = { @UniqueConstraint( columnNames = { "value", "language_id" } ) } )
@SequenceGenerator(name = "tag_seq", sequenceName = "tag_seq")
@ViewDescriptor(pageSize = 10,  viewTypes = {IndexViewType.GRID},
    searchEnabled = true, defaultSearchProperty = "value",
        sort = {@Sorter(propertyName = "value", direction = Sort.Direction.ASC),
                @Sorter(propertyName = "language.name", direction = Sort.Direction.ASC)})
public class Tag extends ro.siveco.ram.starter.model.base.BaseEntity {

    private String value;
    private Language language;
    private String displayName;

    @Id
    @GeneratedValue(generator = "tag_seq", strategy = GenerationType.AUTO)
    public Long getId() {
        return super.getId();
    }

    @Column(name = "value", nullable = false)
    @NotNull
    @PropertyViewDescriptor(order=1)
    public String getValue() {
        return value;
    }

    @ManyToOne
    @JoinColumn(name = "language_id", nullable = false)
    @PropertyViewDescriptor(order=2)
    public Language getLanguage() {
        return language;
    }

    public void setValue(String value) {
        if( value != null )
            this.value = value.toLowerCase();
        else this.value = null;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    @Override
    @PropertyViewDescriptor(hidden = true)
    @Transient
    public String getObjectLabel() {
        return getDisplayName();
    }

    @Transient
    @PropertyViewDescriptor(hidden = true)
    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
}
