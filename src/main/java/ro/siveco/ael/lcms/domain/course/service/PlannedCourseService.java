package ro.siveco.ael.lcms.domain.course.service;

import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.DateTimePath;
import com.querydsl.core.types.dsl.EnumPath;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.jpa.JPAExpressions;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import ro.siveco.ael.lcms.domain.auth.model.AelUser;
import ro.siveco.ael.lcms.domain.auth.model.User;
import ro.siveco.ael.lcms.domain.auth.service.AelUserService;
import ro.siveco.ael.lcms.domain.course.model.*;
import ro.siveco.ael.lcms.domain.course.view.SessionsView;
import ro.siveco.ael.lcms.domain.metadata.model.*;
import ro.siveco.ael.lcms.domain.metadata.service.*;
import ro.siveco.ael.lcms.domain.notification.model.ApplicationUserMessageFormat;
import ro.siveco.ael.lcms.domain.notification.model.UserMessageType;
import ro.siveco.ael.lcms.domain.notification.service.NotificationManagerService;
import ro.siveco.ael.lcms.domain.organization.service.ComponentUserJobService;
import ro.siveco.ael.lcms.domain.preference.model.Preference;
import ro.siveco.ael.lcms.domain.preference.model.Preferences;
import ro.siveco.ael.lcms.domain.preference.service.PreferencesService;
import ro.siveco.ael.lcms.domain.profile.model.UserDetails;
import ro.siveco.ael.lcms.domain.profile.service.UserDetailsService;
import ro.siveco.ael.lcms.repository.PlannedCourseRepository;
import ro.siveco.ael.service.BaseEmailService;
import ro.siveco.ael.service.ImageService;
import ro.siveco.ael.service.mail.EmailService;
import ro.siveco.ael.service.utils.DatesService;
import ro.siveco.ael.service.utils.FilterCategories;
import ro.siveco.ael.web.components.WebPath;
import ro.siveco.ael.web.controllers.WithUserBaseEntityController;
import ro.siveco.ram.app.exception.AppException;
import ro.siveco.ram.security.annotation.CanListEntity;
import ro.siveco.ram.starter.service.BaseEntityService;
import ro.siveco.ram.starter.ui.model.CommonConstants;
import ro.siveco.ram.starter.ui.model.Views;
import ro.siveco.ram.starter.ui.service.ModelDescriptorService;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.Principal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.TemporalAdjusters;
import java.util.*;
import java.util.stream.Collectors;
import java.util.logging.Level;

import static ro.siveco.ael.lcms.domain.course.model.QPlannedCourse.plannedCourse;


/**
 * Created by LiviuI on 4/19/2017.
 */

@Service
public class PlannedCourseService extends BaseEntityService<PlannedCourse> {

    static final Integer TRY_PALETTE_ITEMS_NO = 3;

    static final String DEFAULT_COUNTRY_NAME = "Netherlands";
    private static final String LANGUAGE_ENGLISH = "en";

    private final ComponentUserJobService componentUserJobService;

    private final PlannedCourseStudentService plannedCourseStudentService;

    private final CourseService courseService;

    private final EmailService emailService;

    @Autowired
    private ImageService imageService;

    @Autowired
    private DatesService datesService;

    @Autowired
    private Environment environment;

    @Autowired
    private CommentService commentService;

    /* @Autowired
     private Comment comment;*/
    @Autowired
    private UserDetailsService userDetailsService;

    private final MetadataService metadataService;

    private final AelUserService aelUserService;

    private final NotificationManagerService notificationManagerService;

    private final PreferencesService preferencesService;

    private final CountryService countryService;

    private final LanguageService languageService;

    private final TagService tagService;

    private final BaseEmailService baseEmailService;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    public PlannedCourseService(PlannedCourseRepository resourceRepository,
                                ComponentUserJobService componentUserJobService,
                                PlannedCourseStudentService plannedCourseStudentService,
                                CourseService courseService,
                                EmailService emailService,
                                MetadataService metadataService,
                                AelUserService aelUserService,
                                NotificationManagerService notificationManagerService,
                                PreferencesService preferencesService,
                                CountryService countryService,
                                LanguageService languageService,
                                TagService tagService,
                                BaseEmailService baseEmailService) {
        super(PlannedCourse.class, resourceRepository);
        this.componentUserJobService = componentUserJobService;
        this.plannedCourseStudentService = plannedCourseStudentService;
        this.courseService = courseService;
        this.emailService = emailService;
        this.metadataService = metadataService;
        this.aelUserService = aelUserService;
        this.notificationManagerService = notificationManagerService;
        this.preferencesService = preferencesService;
        this.countryService = countryService;
        this.languageService = languageService;
        this.tagService = tagService;
        this.baseEmailService = baseEmailService;
    }

    public PlannedCourse findOne(Long id) {

        return ((PlannedCourseRepository) getResourceRepository()).findOne(id);
    }

    public List<PlannedCourse> findByCourseId(Long courseId) {
        return ((PlannedCourseRepository) getResourceRepository()).findByCourseId(courseId);
    }

    public PlannedCourse findOneById(Long plannedCourseId) {
        return ((PlannedCourseRepository) getResourceRepository()).findOne(plannedCourseId);
    }

    public List<PlannedCourse> findByCourseIdAndStatus(Long courseId, PlannedCourseStatus status) {
        return ((PlannedCourseRepository) getResourceRepository()).findByCourseIdAndStatus(courseId, status);
    }

    public PlannedCourse createPlannedCourseWithDefaultInformation(Course course) {
        AelUser courseCreator = course.getCreator();
        updateDatesWithTimeZone(course);
        updateTimes(course);
        Date scheduleStartDate = course.getPlannedCourseStartDate();
        Date scheduleEndDate = course.getPlannedCourseEndDate();

        PlannedCourse plannedCourse = new PlannedCourse();
        plannedCourse.setCourse(course);
        plannedCourse.setCreator(courseCreator);
        plannedCourse.setTeacher(courseCreator);
        plannedCourse.setRegistrationUser(courseCreator);
        plannedCourse.setRegistrationApprovalUser(courseCreator);
        plannedCourse.setSelfRegistration(true);
        plannedCourse.setScheduleName(null);
        plannedCourse.setStatus(PlannedCourseStatus.RUNNING);
        plannedCourse.setSignupStartDate(null);
        plannedCourse.setSignupEndDate(null);
        plannedCourse.setStartDate(scheduleStartDate);
        plannedCourse.setEndDate(scheduleEndDate);
        plannedCourse.setStartTime(course.getPlannedCourseStartTime());
        plannedCourse.setEndTime(course.getPlannedCourseEndTime());
        plannedCourse.setMinNrOfStudents(course.getMinUserNumber());
        plannedCourse.setMaxNrOfStudents(course.getMaxUserNumber());
        plannedCourse.setLocation(course.getItemLocation());
        plannedCourse.setLocationCoordinates(course.getItemLocationCoordinates());
        setCountryInformation(plannedCourse);
        plannedCourse.setCountry(course.getCountry());
        plannedCourse.setCountryCode(course.getCountryCode());
        setCountryInformation(plannedCourse);

        plannedCourse = save(plannedCourse);
        return plannedCourse;
    }

    public PlannedCourse updateSchedule(Course course, PlannedCourse plannedCourse) {
        plannedCourse.setStartDate(course.getPlannedCourseStartDate());
        plannedCourse.setEndDate(course.getPlannedCourseEndDate());
        plannedCourse.setStartTime(course.getPlannedCourseStartTime());
        plannedCourse.setEndTime(course.getPlannedCourseEndTime());
        plannedCourse.setMinNrOfStudents(course.getMinUserNumber());
        plannedCourse.setMaxNrOfStudents(course.getMaxUserNumber());
        plannedCourse.setLocation(course.getItemLocation());
        plannedCourse.setLocationCoordinates(course.getItemLocationCoordinates());
        plannedCourse.setCountry(course.getCountry());
        plannedCourse.setCountryCode(course.getCountryCode());

        plannedCourse = update(plannedCourse);
        return plannedCourse;
    }

    public void updateDates(Course course) {

        updateTimes(course);
        Date start = course.getPlannedCourseStartDate();
        Date end = course.getPlannedCourseEndDate();

        List<PlannedCourse> plannedCourses = findByCourseId(course.getId());
        if (plannedCourses != null && plannedCourses.size() > 0) {
            PlannedCourse firstPlannedCourse = plannedCourses.get(0);

            validatePlannedCourseDates(firstPlannedCourse, start, end, course.getClientTimeZoneId());

            firstPlannedCourse.setStartDate(course.getPlannedCourseStartDate());
            firstPlannedCourse.setEndDate(course.getPlannedCourseEndDate());
            firstPlannedCourse.setStartTime(course.getPlannedCourseStartTime());
            firstPlannedCourse.setEndTime(course.getPlannedCourseEndTime());
            firstPlannedCourse.setMinNrOfStudents(course.getMinUserNumber());
            firstPlannedCourse.setMaxNrOfStudents(course.getMaxUserNumber());
            firstPlannedCourse.setLocation(course.getItemLocation());
            firstPlannedCourse.setLocationCoordinates(course.getItemLocationCoordinates());
            firstPlannedCourse.setCountry(course.getCountry());
            firstPlannedCourse.setCountryCode(course.getCountryCode());
            setCountryInformation(firstPlannedCourse);
            update(firstPlannedCourse);
        }
        else {
            throw new AppException("error.item.schedule.not.found");
        }
    }

    public void validatePlannedCourseDates(PlannedCourse firstPlannedCourse, Date start, Date end, String clientTimezoneId) {
        Calendar calendar = new GregorianCalendar();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        Date currentDate = calendar.getTime();

        currentDate = datesService.formatDate(currentDate, TimeZone.getDefault().getID());

        if (end != null && end.before(start)) {
            throw new AppException("model.plannedCourse.error.endDate.before.startDate");
        }
        if (end != null && end.before(currentDate)) {
            throw new AppException("model.plannedCourse.error.endDate.before.currentDate");
        }
    }

    public PlannedCourse createPlannedCourseForOpenCourse(Course openCourse) {
        AelUser creator = openCourse.getCreator();
        PlannedCourse plannedCourse = new PlannedCourse();
        plannedCourse.setCourse(openCourse);
        plannedCourse.setCreator(creator);
        plannedCourse.setTeacher(creator);
        try {
            Locale locale = creator.getLocale();
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm", locale);
            plannedCourse.setStartDate(dateFormat.parse(dateFormat.format(new Date())));
            plannedCourse.setEndDate(dateFormat.parse("9999-12-31 23:59"));
        }
        catch (ParseException e) {
            throw new RuntimeException(e.getMessage());
        }

        plannedCourse.setMaxNrOfStudents(null);

        plannedCourse.setRegistrationApprovalUser(null);
        plannedCourse.setRegistrationUser(null);
        plannedCourse.setSelfRegistration(false);
        plannedCourse.setScheduleName(null);
        plannedCourse.setStatus(PlannedCourseStatus.RUNNING);
        plannedCourse.setSignupStartDate(null);
        plannedCourse.setSignupEndDate(null);
        save(plannedCourse);
        return plannedCourse;
    }

    public Integer getPlannedCourseNumberForCourse(Course course) {

        return ((PlannedCourseRepository) getResourceRepository()).getPlannedCourseNumberForCourse(course.getId());
    }

    public void decorateCourseSession(PlannedCourse plannedCourse) {
        Course course = plannedCourse.getCourse();
        AelUser currentUser = (AelUser) WithUserBaseEntityController.getUser();
        plannedCourse.setCourseType(course.getType());
        setDisplayUsersAddedToFavorites(plannedCourse, currentUser);

        Integer usersAttending = plannedCourseStudentService.getAttendingStudentsNumberForCourse(course);
        plannedCourse.setUsersAttending(usersAttending);
        plannedCourse.setDisplayUsersAttending(usersAttending == null ? null : usersAttending.toString() + " " + getMessageSource().getMessage(getDisplayUsersAttendingLabel(usersAttending), null, LocaleContextHolder.getLocale()));
        plannedCourse.setAllAttendingStudents(getAllAttendingStudents(plannedCourse.getPlannedCourseStudents()));
        plannedCourse.setAttendingStudentsForDisplay(getAttendingStudentsForDisplay(plannedCourse.getAllAttendingStudents()));

        String startDateFormat = getDisplayStartDate(plannedCourse);
        plannedCourse.setDisplayStartDate(startDateFormat);
        String endDateFormat = plannedCourse.getEndDate() == null ? "" : datesService.formatLongDate(plannedCourse.getEndDate());
        plannedCourse.setItemSchedulePeriod(startDateFormat + "-" + endDateFormat);

        String startTimeFormat = plannedCourse.getStartTime() == null ? "" : datesService.formatShortTime(plannedCourse.getStartTime());
        String endTimeFormat = plannedCourse.getEndTime() == null ? "" : datesService.formatShortTime(plannedCourse.getEndTime());
        plannedCourse.setItemScheduleTimePeriod(startTimeFormat + "-" + endTimeFormat);

        if (plannedCourse.getLocation() == null || "".equals(plannedCourse.getLocation())) {
            plannedCourse.setLocation("-");
        }

        Integer noLikes = plannedCourseStudentService.getItemLikesNumber(plannedCourse.getCourse());
        Integer noDislikes = plannedCourseStudentService.getItemDislikesNumber(plannedCourse.getCourse());
        plannedCourse.setItemLikeNumber(noLikes);
        plannedCourse.setItemDislikeNumber(noDislikes);

        AelUser owner = course.getOwner();
        plannedCourse.setOwnerName(owner.getDisplayName());
        plannedCourse.setOwnerEmail(owner.getEmail());
        if (owner.getPhoneNo() == null || "".equals(owner.getPhoneNo())) {
            plannedCourse.setOwnerPhone("-");
        }
        else {
            plannedCourse.setOwnerPhone(owner.getPhoneNo());
        }
        Optional<UserDetails> optionalUserDetails = userDetailsService.getByAelUser(owner);
        if (optionalUserDetails.isPresent()) {
            UserDetails userDetails = optionalUserDetails.get();
            plannedCourse.setShowOwnerEmail(userDetails.getShow_email());
            plannedCourse.setShowOwnerPhone(userDetails.getShow_phone());
        }
        plannedCourse.setOwnerItemsNumber(courseService.getCourseNumberForOwner(course.getOwner()));

        plannedCourse.setComments(commentService.getCommentDetails(plannedCourse.getId()));
        // plannedCourse.setCommentDetails(commentService.getCommentDetails());

        AelUser itemOwner = plannedCourse.getCourse().getOwner();
        UserDetails itemOwnerDetails = userDetailsService.getBasicData(itemOwner.getId());
        if (itemOwnerDetails.getImage() != null && itemOwnerDetails.getImage().length > 0) {
            plannedCourse.setOwnerImageSrc(imageService.imageSrcFromBytes(itemOwnerDetails.getImage()));
        }

        if (plannedCourse.getPlannedCourseStudents() != null) {
            for(PlannedCourseStudent plannedCourseStudent : plannedCourse.getPlannedCourseStudents()) {
                AelUser user = plannedCourseStudent.getUser();
                Optional<UserDetails> userDetails = userDetailsService.getByAelUser(user);
                if (userDetails.isPresent() && userDetails.get().getImage() != null) {
                    user.setAvatarSrc(imageService.imageSrcFromBytes(userDetails.get().getImage()));
                }
            }
        }
        decorateWithActions(plannedCourse, currentUser);
        if (course.getImage() != null && course.getImage().length > 0) {
            plannedCourse.setImageSrc(imageService.imageSrcFromBytes(course.getImage()));
        }
    }

    public String getDisplayUsersAttendingLabel(Integer usersAttending) {
        return usersAttending == null ? null : (usersAttending == 1 ? "displayUsersAttending.one.label" : "displayUsersAttending.label");
    }

    private String getDisplayStartDate(PlannedCourse plannedCourse) {
        return plannedCourse.getStartDate() == null ? "" : datesService.formatLongDate(plannedCourse.getStartDate());
    }

    private void setDisplayUsersAddedToFavorites(PlannedCourse plannedCourse, AelUser user) {
        Locale locale = aelUserService.getLocaleForUser(user);
        setDisplayUsersAddedToFavorites(plannedCourse, locale);
    }

    private void setDisplayUsersAddedToFavorites(PlannedCourse plannedCourse, Locale locale) {
        Integer usersAddedToFavorites = plannedCourseStudentService.getFavoritesStudentsNumberForCourse(plannedCourse.getCourse());
        plannedCourse.setUsersAddedToFavorites(usersAddedToFavorites);
        plannedCourse.setDisplayUsersAddedToFavorites(usersAddedToFavorites == null ? null : usersAddedToFavorites.toString() + " " + getMessageSource().getMessage("displayUsersAddedToFavorites.label", null, locale));
    }

    private void decorateWithActions(PlannedCourse plannedCourse, AelUser currentUser) {
        PlannedCourseStudent plannedCourseStudent = plannedCourseStudentService.findByPlanificationAndStundent(plannedCourse.getId(), currentUser.getId());
        Locale locale = aelUserService.getLocaleForUser(currentUser);
        if (plannedCourseStudent != null) {
            plannedCourse.setAddedToFavorite(plannedCourseStudent.getAddedToFavorites());
            plannedCourse.setRemoved(plannedCourseStudent.getRemoveItem());
            plannedCourse.setAttending(plannedCourseStudent.getAttending());
        }
        Integer usersAddedToFavorites = plannedCourseStudentService.getFavoritesStudentsNumberForCourse(plannedCourse.getCourse());
        plannedCourse.setUsersAddedToFavorites(usersAddedToFavorites);
        plannedCourse.setDisplayUsersAddedToFavorites((usersAddedToFavorites == null ? "0" : usersAddedToFavorites.toString()) + " " + getMessageSource().getMessage("displayUsersAddedToFavorites.label", null, locale));
        setDisplayStartDate(plannedCourse);
        courseService.setDefaultImage(plannedCourse.getCourse());
        plannedCourse.setImage(plannedCourse.getCourse().getImage());
        plannedCourse.setImageSrc(plannedCourse.getCourse().getImageSrc());
    }

    public Boolean validatePlannedCourseDates(PlannedCourse plannedCourse) {
        if (plannedCourse.getStartDate().before(new Date())) {
            throw new AppException("model.plannedCourse.error.startDate.before.currentDate");
        }

        if (plannedCourse.getEndDate() != null && plannedCourse.getEndDate().before(new Date())) {
            throw new AppException("model.plannedCourse.error.endDate.before.currentDate");
        }

        if (plannedCourse.getEndDate() != null && plannedCourse.getEndDate().before(plannedCourse.getStartDate())) {
            throw new AppException("model.plannedCourse.error.endDate.before.startDate");
        }

        return Boolean.TRUE;
    }

    public Boolean validateUpdatePlannedCourseDates(PlannedCourse plannedCourse, PlannedCourse dbPlannedCourse) {

        if (plannedCourse.getStartDate().before(dbPlannedCourse.getStartDate())) {
            throw new RuntimeException("model.plannedCourse.error.startDate.before.persisted.startDate");
        }

        if (plannedCourse.getEndDate() != null && plannedCourse.getEndDate().before(dbPlannedCourse.getStartDate())) {
            throw new RuntimeException("model.plannedCourse.error.endDate.before.persisted.startDate");
        }

        if (plannedCourse.getEndDate() != null && plannedCourse.getEndDate().before(plannedCourse.getStartDate())) {
            throw new RuntimeException("model.plannedCourse.error.endDate.before.startDate");
        }

        return Boolean.TRUE;
    }

    public List<PlannedCourse> getAllPlannedCourses() {
        return ((PlannedCourseRepository) getResourceRepository()).findAll();
    }

    /************************************************************************/
    /* haversin formula */
    private static final int EARTH_RADIUS = 6371; // Approx Earth radius in KM
    private static double getDistanceByHaversin(double startLat, double startLong, double endLat, double endLong) {
        double dLat = Math.toRadians((endLat - startLat));
        double dLong = Math.toRadians((endLong - startLong));
        startLat = Math.toRadians(startLat);
        endLat = Math.toRadians(endLat);
        double a = getHaversin(dLat) + Math.cos(startLat) * Math.cos(endLat) * getHaversin(dLong);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        return EARTH_RADIUS * c;
    }
    private static double getHaversin(double val) {
        return Math.pow(Math.sin(val / 2), 2);
    }
    /************************************************************************/

    private Long getDistanceBetweenLocations(String userLocation, String itemLocation) {
        String distanceApiKey = environment.getProperty("google.client.distance.apiKey");
        String distanceApiMainUrl = environment.getProperty("google.client.distance.url");
        String distanceApiTravelMode = environment.getProperty("google.client.distance.travelMode");
        userLocation = userLocation.replaceAll(" ", "+").replaceAll(";", ",");
        itemLocation = itemLocation.replaceAll(" ", "+").replaceAll(";", ",");

        String urlString = distanceApiMainUrl + "origins=" + userLocation + "&destinations=" + itemLocation +
                "&mode=" + distanceApiTravelMode + "&language=en-US&key=" + distanceApiKey;
        String response = getGoogleMapsResponse(urlString);

        Long distanceValue = getDistanceFromGoogleMapsResponse(response);
        if (distanceValue != null) {
            return distanceValue / 1000;
        }
        return null;
    }

    private String getGoogleMapsResponse(String urlString) {
        String response = "";
        try {
            URL url = new URL(urlString);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            String line;
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(conn.getInputStream()));
            while ((line = reader.readLine()) != null) {
                response += line;
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return response;
    }

    private Long getDistanceFromGoogleMapsResponse(String outputString) {
        Long distanceValue = null;
        try {
            JSONObject obj = new JSONObject(outputString);
            JSONArray rows = obj.getJSONArray("rows");
            if (rows.length() > 0) {
                JSONObject firstRow = rows.getJSONObject(0);
                JSONArray elements = firstRow.getJSONArray("elements");
                if (elements.length() > 0) {
                    JSONObject firstElement = elements.getJSONObject(0);
                    JSONObject distance = firstElement.getJSONObject("distance");
                    distanceValue = distance.getLong("value");
                }
            }
        }
        catch (JSONException e) {
            e.printStackTrace();
        }

        return distanceValue;
    }

    public void sendMailToTeacher(Long plannedCourseId, String body) {
        PlannedCourse dbPlannedCourse = get(plannedCourseId);
        AelUser student = (AelUser) WithUserBaseEntityController.getUser();
        sendMailToTeacher(dbPlannedCourse, body, student);
    }

    /**
     * Send e-mail to the creator of the item (event) by some other user.
     *
     * Note, `body` is just a message that the user put in the message box.
     * Creator of the item is derived from the item itself.
     *
     * @param plannedCourse item about which the message is sent
     * @param messageFromUser message sent by the user
     * @param student user that sends the message
     */
    public void sendMailToTeacher(PlannedCourse plannedCourse, String messageFromUser, AelUser student) {
        List<Map<String, Object>> params = new ArrayList<>();

        // User (creator of the item).
        AelUser teacher = plannedCourse.getTeacher();

        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
        Principal principal = SecurityContextHolder.getContext().getAuthentication();

        // User (the one sending a message).
        AelUser currentUser = (AelUser)WithUserBaseEntityController.getUser();

        // Locale of the creator of the item (the one to send the message to).
        Locale locale = aelUserService.getLocaleForUser(teacher);

        // `sent you a message` phrase from the translations (message_*.properties).
        String sentYouMessage = messageSource.getMessage("email.template.message", null,
                                                         "email.template.message", locale);

        // Course name.
        String courseName = plannedCourse.getCourse().getName();

        // Name of the course.
        Object[] param = {courseName};
        String subject = messageSource.getMessage("user-notifications.item.message.subject", param, locale);

        // Message from the creator is a compilation of the other user message and some info like: "X sent you
        // a messge about item Y".
        String messageForCreator = student.getDisplayName() + " " + sentYouMessage + " " + courseName;
        messageForCreator +=  "<br /><br /><em>" + messageFromUser + "</em>";

        try {
            // Wrap the message into layout before sending.
            String body = baseEmailService.putMessageIntoLayout(currentUser, messageForCreator);

            emailService.sendEmail(currentUser.getUsername(), teacher, subject, body);
        } catch (Throwable e) {
            getLogger().log(Level.SEVERE, "Error sending email to the user: " + e.getMessage(), e);
        }

        notificationManagerService.notifyUser(currentUser, currentUser, null,
                new Object[]{new Date(), getUserUrlForNotification(teacher), messageFromUser},
                UserMessageType.COURSE_RELATED_MESSAGE, ApplicationUserMessageFormat.COURSE_STUDENT_EMAIL_SENT);

    }

    public List<PlannedCourse> getRecommendedPlannedCourses(AelUser user) {
        List<PlannedCourse> recommendedPlannedCourses = new ArrayList<>();
        if (user != null) {
            List<PlannedCourse> plannedCourses = ((PlannedCourseRepository) getResourceRepository()).getRecommendedPlannedCourses(user.getId());
            sortPlannedCourses(recommendedPlannedCourses, plannedCourses, Comparator.comparing(PlannedCourse::getMatchMetadataNo));
            recommendedPlannedCourses.forEach(plannedCourse -> decorateWithActions(plannedCourse, (AelUser) WithUserBaseEntityController.getUser()));
        }
        return recommendedPlannedCourses;
    }

    public List<PlannedCourse> getPastPlannedCourses(AelUser user) {
        List<PlannedCourse> pastPlannedCourses = new ArrayList<>();
        if (user != null) {
            List<PlannedCourse> plannedCourses = ((PlannedCourseRepository) getResourceRepository()).getPastPlannedCourses(user.getId(), datesService.getYesterdayNight());
            sortPlannedCourses(pastPlannedCourses, plannedCourses, Comparator.comparing(PlannedCourse::getStartDate));
            pastPlannedCourses.forEach(plannedCourse -> decorateWithActions(plannedCourse, (AelUser) WithUserBaseEntityController.getUser()));
        }
        return pastPlannedCourses;
    }

    private void sortPlannedCourses(List<PlannedCourse> finalPlannedCourses, List<PlannedCourse> plannedCourses, Comparator<PlannedCourse> comparing) {
        if (plannedCourses != null) {
            plannedCourses.sort(comparing);
            Collections.reverse(plannedCourses);
            decorateWithFavoritesUsers(plannedCourses);
            finalPlannedCourses.addAll(plannedCourses);
        }
    }

    private void decorateWithFavoritesUsers(List<PlannedCourse> plannedCourses) {
        for (PlannedCourse plannedCourse : plannedCourses) {
            Integer addedToFavoritesNumber = plannedCourseStudentService.getFavoritesStudentsNumberForCourse(plannedCourse.getCourse());
            plannedCourse.setUsersAddedToFavorites(addedToFavoritesNumber);
        }
    }

    @Transactional
    public void createPlannedCourseForCourse(Course course, PlannedCourse plannedCourse) {
        if (!course.isNew()) {
            List<PlannedCourse> associatedPlannedCourses = findByCourseId(course.getId());
            if (associatedPlannedCourses == null || associatedPlannedCourses.size() == 0) {
                plannedCourse = createPlannedCourseWithDefaultInformation(course);
            }
        }

        metadataService.updateMetadata(course);

        if (plannedCourse.getEndDate() == null || plannedCourse.getEndDate().after(new Date())) {
            List<Tag> includedTags = course.getIncludedTags();
            if (includedTags != null && includedTags.size() > 0) {
                List<Long> tagIds = new ArrayList<>();
                for (Tag tag : includedTags) {
                    tagIds.add(tag.getId());
                }
                List<Long> userIds = metadataService.getEntityIdsForTags(EntityType.USER, RelationType.INCLUDED, tagIds);
                AelUser userFrom = (AelUser) WithUserBaseEntityController.getUser();
                for (Long userId : userIds) {
                    AelUser user = (AelUser) aelUserService.findOneById(userId);
                    if (!userFrom.getId().equals(user.getId()) && userFrom.getCountry() != null && user.getCountry() != null && userFrom.getCountry().getName().equalsIgnoreCase(user.getCountry().getName())) {
                        notificationManagerService.notifyUser(userFrom, user, null,
                                new Object[]{plannedCourse.getStartDate(), plannedCourse.getEndDate(),
                                        getPlannedCourseUrlForNotification(plannedCourse)},
                                UserMessageType.COURSE_RELATED_MESSAGE, ApplicationUserMessageFormat.COURSE_SUBMISSION_MESSAGE);
                    }
                }
            }
        }
    }

    @CanListEntity
    public Page<PlannedCourse> paginate(Pageable pageable) {
        return getPlannedCourses(null, pageable);
    }

    @CanListEntity
    public Page<PlannedCourse> paginate(Predicate predicate, Pageable pageable) {
        return getPlannedCourses(predicate, pageable);
    }

    private Page<PlannedCourse> getPlannedCourses(Predicate predicate, Pageable pageable) {
        Page<PlannedCourse> page;
        AelUser currentUser = (AelUser) WithUserBaseEntityController.getUser();
        Class<? extends Views.View> currentView = getRequestedView();
        List<Long> filteredIds = new ArrayList<>();
        List<PlannedCourse> relevantItems = null;
        Long filteredNo = 0L;

        if (SessionsView.class.getName().equals(currentView.getName())) {
            page = findAllByCurrentUser(currentUser.getId(), pageable);
        }
        else {
            if (predicate != null) {
                if (!isFilterRequest()) {
                    relevantItems = getRelevantPlannedCourses(currentUser);
                    BooleanExpression relevantItemsFilter = getNotRelevantPlannedCourseFilterExpression(relevantItems);
                    if (relevantItemsFilter != null) {
                        predicate = relevantItemsFilter.and(predicate);
                    }
                }

                Iterable<PlannedCourse> filteredPlannedCourses = getResourceRepository().findAll(predicate);
                if (filteredPlannedCourses != null) {
                    for (PlannedCourse plannedCourse : filteredPlannedCourses) {
                        filteredIds.add(plannedCourse.getId());
                    }
                }
            }

            if (filteredIds.size() > 0) {
                page = findAllNotRemovedFiltered(currentUser.getId(), filteredIds, super.revisePageable(pageable));
                filteredNo = countAllNotRemovedFiltered(currentUser.getId(), filteredIds);
            }
            else {
                page = new PageImpl<>(new ArrayList<>(), pageable, filteredIds.size());
            }
        }
        if (relevantItems != null && relevantItems.size() > 0) {
            //if first page
            if (pageable.getPageNumber() == 0) {
                //add relevant items to first page
                List<PlannedCourse> pageContent = page.getContent();
                List<PlannedCourse> orderedPageContent = new LinkedList<>();
                orderedPageContent.addAll(relevantItems);
                orderedPageContent.addAll(pageContent);
                if (orderedPageContent.size() > pageable.getPageSize() && filteredNo <= pageable.getPageSize()) {
                    page = new PageImpl<>(orderedPageContent, new PageRequest(page.getNumber(), orderedPageContent.size()), filteredNo);
                }
                else {
                    page = new PageImpl<>(orderedPageContent, new PageRequest(page.getNumber(), pageable.getPageSize()), filteredNo);
                }
            }
            //not first page
            else {
                page = new PageImpl<>(page.getContent(), new PageRequest(page.getNumber(), pageable.getPageSize()), filteredNo);
            }
        }

        page.forEach(this::triggerAfterRead);
        return page;
    }

    public List<PlannedCourse> getRelevantPlannedCourses(AelUser currentUser) {
        List<PlannedCourse> relevantItems = new ArrayList<>();
        List<Long> tags = getTagIdsForUser(currentUser.getId());
        if (tags == null || tags.size() == 0) {
            tags = getDefaultTagIds();
        }
        String userCountryCode = getUserCountryCode(currentUser);
        Date date = datesService.getYesterdayNight();

        //get 4 relevant items
        PlannedCourse randomService = getRandomService(currentUser.getId(), date, userCountryCode, tags);
        if (randomService != null) {
            relevantItems.add(randomService);
        }

        PlannedCourse closestCourse = getClosestCourse(currentUser.getId(), date, userCountryCode, tags);
        if (closestCourse != null) {
            relevantItems.add(closestCourse);
        }

        PlannedCourse mostPopularMeeting = getMostPopularMeeting(currentUser.getId(), date, userCountryCode, tags);
        if (mostPopularMeeting != null) {
            relevantItems.add(mostPopularMeeting);
        }

        PlannedCourse mostRecentActivity = getMostRecentActivity(currentUser.getId(), date, userCountryCode, tags);
        if (mostRecentActivity != null) {
            relevantItems.add(mostRecentActivity);
        }
        return relevantItems;
    }

    private List<Long> getDefaultTagIds() {
        List<Language> languages = languageService.findAllByName(LANGUAGE_ENGLISH);
        List<Long> tags = new ArrayList<>();
        if (languages != null && languages.size() > 0) {
            tags = tagService.findAllByLanguage(languages.get(0)).stream().map(Tag::getId).collect(Collectors.toList());
        }
        return tags;
    }

    private Page<PlannedCourse> findAllNotRemovedFiltered(Long userId, List<Long> ids, Pageable pageable) {
        return ((PlannedCourseRepository) getResourceRepository()).findAllNotRemovedFiltered(userId, ids, pageable);
    }

    private Long countAllNotRemovedFiltered(Long userId, List<Long> ids) {
        return ((PlannedCourseRepository) getResourceRepository()).countAllNotRemovedFiltered(userId, ids);
    }

    public Page<PlannedCourse> findAllNotRemoved(Long userId, Pageable pageable) {
        return ((PlannedCourseRepository) getResourceRepository()).findAllNotRemoved(userId, pageable);
    }

    private Page<PlannedCourse> findAllByCurrentUser(Long userId, Pageable pageable) {
        return ((PlannedCourseRepository) getResourceRepository()).findAllByCurrentUser(userId, pageable);
    }

    private Class<? extends Views.View> getRequestedView() {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        Class<? extends Views.View> viewFromRequest = ModelDescriptorService.extractViewFromRequest(this.getDomainClass(), request);
        return viewFromRequest != null ? viewFromRequest : CommonConstants.DEFAULT_VIEW;
    }

    private BooleanExpression getNotRelevantPlannedCourseFilterExpression(List<PlannedCourse> relevantItems) {
        BooleanExpression filterExpression = null;
        NumberPath idPath = plannedCourse.id;
        List<Long> plannedCoursesId;
        if (relevantItems != null && relevantItems.size() > 0) {
            plannedCoursesId = relevantItems.stream().map(PlannedCourse::getId).collect(Collectors.toList());
            filterExpression = idPath.notIn(plannedCoursesId);
        }
        return filterExpression;
    }
    public BooleanExpression getSearchFilterExpression(List<String> filterValues) {
        BooleanExpression searchExpression = null;
        com.querydsl.core.types.dsl.StringPath courseNamePath = plannedCourse.course.name;
        com.querydsl.core.types.dsl.StringPath courseDescriptionPath = plannedCourse.course.description;
        for (String value : filterValues) {
            /* title */
            String[] strArray = value.split("\\s+");
            for(int i = 0; i < strArray.length; i++) {
                if(i == 0) { searchExpression = (courseNamePath.upper().like("%" + strArray[i].toUpperCase() + "%")); }
                else { searchExpression = searchExpression.or((courseNamePath.upper().like("%" + strArray[i].toUpperCase() + "%"))); }
            }
            /* description */
            for(int i = 0; i < strArray.length; i++) {
                if (i == 0) { searchExpression = searchExpression.or((courseDescriptionPath.upper().like("%" + strArray[i].toUpperCase() + "%"))); }
                else { searchExpression = searchExpression.or((courseDescriptionPath.upper().like("%" + strArray[i].toUpperCase() + "%"))); }
            }
            break;
        }
        return searchExpression;
    }

    public BooleanExpression getTypeFilterExpression(List<String> filterValues, Boolean isCourseFilter) {
        BooleanExpression typeExpression = null;
        EnumPath<CourseType> courseTypePath = null;
        if (!isCourseFilter) {
            courseTypePath = plannedCourse.course.type;
        }
        else {
            courseTypePath = QCourse.course.type;
        }
        for (String type : filterValues) {
            if (typeExpression == null) {
                typeExpression = (courseTypePath.eq(getCourseTypeFromFilter(type)));
            }
            else {
                typeExpression = typeExpression.or(courseTypePath.eq(getCourseTypeFromFilter(type)));
            }
        }

        return typeExpression;
    }

    public CourseType getCourseTypeFromFilter(String type) {
        if (type.equals(CourseType.COURSE.toString())) {
            return CourseType.COURSE;
        }
        else if (type.equals(CourseType.ACTIVITY.toString())) {
            return CourseType.ACTIVITY;
        }
        else if (type.equals(CourseType.SERVICE.toString())) {
            return CourseType.SERVICE;
        }
        else {
            return CourseType.MEETING;
        }
    }

    public BooleanExpression getDateFilterExpression(List<String> filterValues, Boolean isCourseFilter) {
        BooleanExpression dateExpression = null;
        LocalDateTime periodStartDate = null;
        LocalDateTime periodEndDate = null;

        LocalDateTime now = LocalDateTime.now(); // current date and time

        for (String filter : filterValues) {
            if (!"CALENDAR".equals(filter)) {
                if ("TODAY".equals(filter)) {
                    periodStartDate = now.toLocalDate().atStartOfDay();
                    LocalDateTime tomorrowMidnight = periodStartDate.plusDays(1);
                    periodEndDate = tomorrowMidnight.minusSeconds(1);
                }
                else if ("THIS_WEEK".equals(filter)) {
                    LocalDateTime firstDayOfWeek = now.with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY));
                    periodStartDate = firstDayOfWeek.toLocalDate().atStartOfDay();
                    LocalDateTime firstDayOfNextWeek = periodStartDate.plusWeeks(1);
                    periodEndDate = firstDayOfNextWeek.minusSeconds(1);
                }
                else if ("THIS_MONTH".equals(filter)) {
                    LocalDateTime firstDayOfMonth = now.with(TemporalAdjusters.firstDayOfMonth());
                    periodStartDate = firstDayOfMonth.toLocalDate().atStartOfDay();
                    LocalDateTime firstDayOfNextMonth = periodStartDate.plusMonths(1);
                    periodEndDate = firstDayOfNextMonth.minusSeconds(1);
                }

                dateExpression = getDateExpressionForPeriod(periodStartDate, periodEndDate, isCourseFilter);
            }
        }
        return dateExpression;
    }

    public BooleanExpression getCalendarFilterExpression(List<String> filterValues, Boolean isCourseFilter) {
        BooleanExpression calendarExpression = null;
        LocalDateTime periodStartDate = null;
        LocalDateTime periodEndDate = null;

        if (filterValues != null && filterValues.size() == 2) {

            Date calendarStart = new Date(Long.parseLong(filterValues.get(0)));
            periodStartDate = calendarStart.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();

            Date calendarEnd = new Date(Long.parseLong(filterValues.get(1)));
            periodEndDate = calendarEnd.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();

            calendarExpression = getDateExpressionForPeriod(periodStartDate, periodEndDate, isCourseFilter);
        }

        return calendarExpression;
    }

    public BooleanExpression getDateExpressionForPeriod(LocalDateTime periodStart, LocalDateTime periodEnd, Boolean isCourseFilter) {
        BooleanExpression dateExpression = null;
        Date periodStartDate = Date.from(periodStart.atZone(ZoneId.of(datesService.getCurrentUserTimezoneId())).toInstant());
        Date periodEndDate = Date.from(periodEnd.atZone(ZoneId.of(datesService.getCurrentUserTimezoneId())).toInstant());

        if (!isCourseFilter) {
            DateTimePath<Date> startDatePath = plannedCourse.startDate;
            DateTimePath<Date> endDatePath = plannedCourse.endDate;
            dateExpression = ((startDatePath.loe(periodStartDate)).and(endDatePath.goe(periodStartDate))).or((startDatePath.loe(periodEndDate)).and(endDatePath.goe(periodEndDate))).
                    or((startDatePath.goe(periodStartDate)).and(startDatePath.loe(periodEndDate)).and(endDatePath.goe(periodStartDate)).and(endDatePath.loe(periodEndDate))).or((startDatePath.goe(periodEndDate)).and(endDatePath.isNull()));
        }
        else {
            NumberPath<Long> courseIdPath = QCourse.course.id;
            List<Long> courseIds = courseService.getCourseIdsForPeriod(periodStartDate, periodEndDate);
            dateExpression = courseIdPath.in(courseIds);
        }

        return dateExpression;
    }

    public BooleanExpression getTagFilterExpression(List<String> filterValues, Boolean isCourseFilter) {
        BooleanExpression tagExpression = null;
        List<Long> tagIds = new ArrayList<>();
        for (String value : filterValues) {
            tagIds.add(Long.parseLong(value));
        }
        List<Long> courseIds = metadataService.getEntityIdsForTags(EntityType.COURSE, RelationType.INCLUDED, tagIds);
        NumberPath<Long> courseIdPath;
        if (!isCourseFilter) {
            courseIdPath = plannedCourse.course.id;
        }
        else {
            courseIdPath = QCourse.course.id;
        }
        tagExpression = courseIdPath.in(courseIds);

        return tagExpression;
    }

    public Long getMaxDistanceFromFilter(List<String> filterValues) {
        Long distance = 0L;
        for (String filter : filterValues) {
            if ("DISTANCE_1".equals(filter)) {
                distance = 1L;
            }else if ("DISTANCE_2".equals(filter)) {
                distance = 2L;
            }else if ("DISTANCE_5".equals(filter)) {
                distance = 5L;
            }else if ("DISTANCE_10".equals(filter)) {
                distance = 10L;
            }else if ("DISTANCE_15".equals(filter)) {
                distance = 15L;
            }else if ("DISTANCE_25".equals(filter)) {
                distance = 25L;
            }else if ("DISTANCE_35".equals(filter)) {
                distance = 35L;
            }else if ("DISTANCE_50".equals(filter)) {
                distance = 50L;
            }else if ("DISTANCE_100".equals(filter)) {
                distance = 100L;
            }else if ("DISTANCE_150".equals(filter)) {
                distance = 150L;
            }else if ("DISTANCE_200".equals(filter)) {
                distance = 200L;
            }
        }
        return distance;
    }

    public BooleanExpression getDistanceFilterExpression(List<String> filterValues, Boolean isCourseFilter) {

        Long maxDistance = getMaxDistanceFromFilter(filterValues);
        BooleanExpression distanceExpression = null;
        NumberPath<Long> courseIdPath;

        List<Long> courseIds = getCoursesFilteredByDistance(maxDistance);
        if (!isCourseFilter) {
            courseIdPath = plannedCourse.course.id;
        }
        else {
            courseIdPath = QCourse.course.id;
        }
        distanceExpression = courseIdPath.in(courseIds);

        return distanceExpression;
    }

    public List<Long> getCoursesFilteredByDistance(Long maxDistance) {
        User currentUser = WithUserBaseEntityController.getUser();
        String userLocation = ((AelUser) currentUser).getCity();
        List<Long> courseIds = new ArrayList<>();
        String[] userCoordinates = userLocation.split(";");
        if (userLocation != null && !"".equals(userLocation)) {
            List<PlannedCourse> allItems = getAllPlannedCourses();
            for (PlannedCourse plannedCourse : allItems) {
                String itemLocation = plannedCourse.getLocationCoordinates();
                if (itemLocation != null && !"".equals(itemLocation) && !"-".equals(itemLocation)) {
                    String[] itemCoordinates = itemLocation.split(";");
                    double distance = getDistanceByHaversin(
                            Double.parseDouble(userCoordinates[0]),
                            Double.parseDouble(userCoordinates[1]),
                            Double.parseDouble(itemCoordinates[0]),
                            Double.parseDouble(itemCoordinates[1]));
                    //Long distance = getDistanceBetweenLocations(userLocation, itemLocation); /* removed google places api since we now use the haversin formula to calculate the distance */
                    if (/*distance != null && */distance <= maxDistance && !courseIds.contains(plannedCourse.getCourse().getId())) {
                        courseIds.add(plannedCourse.getCourse().getId());
                    }
                }
            }
        }
        return courseIds;
    }

    private static List<String> getFilterCategories() {
        List<String> filterCategories = new ArrayList();
        filterCategories.add(FilterCategories.FILTER_SEARCH);
        filterCategories.add(FilterCategories.FILTER_FAVORITES);
        filterCategories.add(FilterCategories.FILTER_TYPE);
        filterCategories.add(FilterCategories.FILTER_DATE);
        filterCategories.add(FilterCategories.FILTER_DISTANCE);
        filterCategories.add(FilterCategories.FILTER_TAG);
        filterCategories.add(FilterCategories.FILTER_ORGANIZER);
        filterCategories.add(FilterCategories.FILTER_CALENDAR);
        return filterCategories;
    }

    private Boolean isFilterRequest() {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
        Map<String,String[]> parameters = request.getParameterMap();
        if (parameters != null && parameters.size() > 0) {
            for(String parameter : parameters.keySet()) {
                if (getFilterCategories().contains(parameter)) {
                    return true;
                }
            }
        }
        return false;
    }

    public Optional<BooleanExpression> createFilterForParameters(MultiValueMap<String, String> parameters, BooleanExpression filterExpression) {
        User currentUser = WithUserBaseEntityController.getUser();
        QPlannedCourseStudent qPlannedCourseStudent = QPlannedCourseStudent.plannedCourseStudent;
        QPlannedCourse qPlannedCourse = plannedCourse;

        for (Map.Entry<String, List<String>> entry : parameters.entrySet()) {
            String filterCategory = entry.getKey();
            List<String> filterValues = entry.getValue();

            switch (filterCategory) {
                case FilterCategories.FILTER_SEARCH: {
                    BooleanExpression searchExpression = getSearchFilterExpression(filterValues);
                    filterExpression = filterExpression.and(searchExpression);
                    break;
                }
                case FilterCategories.FILTER_FAVORITES: {
                    Predicate predicate = JPAExpressions
                            .selectOne()
                            .from(qPlannedCourseStudent)
                            .where(qPlannedCourseStudent.user.id.eq(currentUser.getId()),
                                    qPlannedCourseStudent.plannedCourse.id.eq(qPlannedCourse.id),
                                    qPlannedCourseStudent.addedToFavorites.eq(true)).exists();

                    filterExpression = filterExpression.and(predicate);
                    break;
                }
                case FilterCategories.FILTER_TYPE: {
                    BooleanExpression typeExpression = getTypeFilterExpression(filterValues, false);
                    filterExpression = filterExpression.and(typeExpression);
                    break;
                }
                case FilterCategories.FILTER_DATE: {
                    BooleanExpression dateExpression = getDateFilterExpression(filterValues, false);
                    BooleanExpression calendarExpression = null;
                    if (filterValues.contains("CALENDAR")) {
                        List<String> dateValues = parameters.get(FilterCategories.FILTER_CALENDAR);
                        calendarExpression = getCalendarFilterExpression(dateValues, false);
                    }
                    if (dateExpression != null) {
                        if (calendarExpression != null) {
                            dateExpression = dateExpression.or(calendarExpression);
                        }
                        filterExpression = filterExpression.and(dateExpression);
                    }
                    else {
                        if (calendarExpression != null) {
                            filterExpression = filterExpression.and(calendarExpression);
                        }
                    }
                    break;
                }
                case FilterCategories.FILTER_DISTANCE: {
                    BooleanExpression distanceExpression = getDistanceFilterExpression(filterValues, false);
                    if (distanceExpression != null) {
                        filterExpression = filterExpression.and(distanceExpression);
                    }
                    break;
                }
                case FilterCategories.FILTER_TAG: {
                    BooleanExpression tagExpression = getTagFilterExpression(filterValues, false);
                    filterExpression = filterExpression.and(tagExpression);
                    break;
                }
                case FilterCategories.FILTER_ORGANIZER: {
                    break;
                }
            }
        }

        return Optional.of(filterExpression);
    }

    public BooleanExpression getTagsFilterExpression(Long userId) {
        List<Long> tagIds = getTagIdsForUser(userId);
        if (tagIds.size() > 0) {
            return getTagFilterExpression(tagIds.stream().map(Object::toString).collect(Collectors.toList()), false);
        }
        return null;
    }

    public List<Long> getTagIdsForUser(Long userId) {
        List<Metadata> metadata = metadataService.findByEntityIdAndEntityTypeAndRelationType(userId, EntityType.USER, RelationType.INCLUDED);
        List<Long> tagIds = new ArrayList<>(0);
        if (metadata != null) {
            tagIds = metadata.stream().map(Metadata::getTag).collect(Collectors.toList()).stream().map(Tag::getId).collect(Collectors.toList());
        }
        return tagIds;
    }

    public BooleanExpression getTagsFilterExpression(Long userId, List<String> tagIds) {
        List<Long> userTagIds = getTagIdsForUser(userId);
        if (tagIds != null) {
            userTagIds.addAll(tagIds.stream().map(Long::valueOf).collect(Collectors.toList()));
        }
        if (userTagIds.size() > 0) {
            return getTagFilterExpression(userTagIds.stream().map(Object::toString).collect(Collectors.toList()), false);
        }
        return null;
    }

    public void setCountryInformation(PlannedCourse plannedCourse) {
        if (plannedCourse != null) {
            AelUser currentUser = (AelUser) WithUserBaseEntityController.getUser();
            if (plannedCourse.getCountry() == null) {
                plannedCourse.setCountry(currentUser.getCountry().getName());
            }
            if (plannedCourse.getCountryCode() == null) {
                String countryName = plannedCourse.getCountry();
                List<Country> countries = countryService.findAllByCountryName(countryName);
                if (countries != null && countries.size() > 0) {
                    Country country = countries.get(0);
                    plannedCourse.setCountryCode(country.getCode());
                }
            }
        }
    }

    public List<PlannedCourse> getUserPlannedCourses(AelUser user) {
        if ( user != null && user.getId() != null) {
            return ((PlannedCourseRepository) getResourceRepository()).findAllByUserOrderByStartDate(user.getId());
        }
        return new ArrayList<>();
    }

    public List<PlannedCourse> findAllByUserOrderByStartDate(Long userId) {
        List<PlannedCourse> plannedCourses =  ((PlannedCourseRepository) getResourceRepository()).findAllByUserOrderByStartDate(userId);
        User currentUser = WithUserBaseEntityController.getUser();
        Locale locale = aelUserService.getLocaleForUser((AelUser) currentUser);
        if (plannedCourses != null) {
            for(PlannedCourse plannedCourse : plannedCourses) {
                Integer usersAddedToFavorites = plannedCourseStudentService.getFavoritesStudentsNumberForCourse(plannedCourse.getCourse());
                plannedCourse.setDisplayUsersAddedToFavorites(usersAddedToFavorites == null ? null : usersAddedToFavorites.toString() + " " + getMessageSource().getMessage("displayUsersAddedToFavorites.label", null, locale));
                PlannedCourseStudent plannedCourseStudent = plannedCourseStudentService.findByPlanificationAndStundent(plannedCourse.getId(), currentUser.getId());
                plannedCourse.setAddedToFavorite(plannedCourseStudent != null && plannedCourseStudent.getAddedToFavorites());
                Course course = plannedCourse.getCourse();
                courseService.setDefaultImage(course);
                plannedCourse.setImage(course.getImage());
                plannedCourse.setImageSrc(course.getImageSrc());
                setDisplayStartDate(plannedCourse);
            }
        }
        return plannedCourses;
    }

    public PlannedCourse attendItem(Long plannedCourseId) {
        PlannedCourse plannedCourse = findOne(plannedCourseId);

        if (plannedCourse.getEndDate() != null && plannedCourse.getEndTime() != null && datesService.isInThePast(plannedCourse.getEndDate(), plannedCourse.getEndTime(), 0)) {
            if (plannedCourse.getAttending() != null && plannedCourse.getAttending()) {
                throw new AppException("error.not.attending.after.end.date");
            }
            else {
                if (plannedCourse.getAttending() == null || !plannedCourse.getAttending()) {
                    throw new AppException("error.attending.after.end.date");
                }
            }
        }
        User currentUser = WithUserBaseEntityController.getUser();
        PlannedCourseStudent plannedCourseStudent = plannedCourseStudentService.findByPlanificationAndStundent(plannedCourseId, currentUser.getId());

        if(plannedCourseStudent == null)
        {
            plannedCourseStudent = plannedCourseStudentService.createPlannedCourseStudent(currentUser, plannedCourse, Boolean.TRUE, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE, Boolean.TRUE);
        }
        else
        {
            plannedCourseStudent.setAttending(plannedCourseStudent.getAttending() == null || !plannedCourseStudent.getAttending());
            plannedCourse.setAttending(plannedCourseStudent.getAttending());
            plannedCourseStudent.setRemoveItem(Boolean.FALSE);
            plannedCourseStudentService.update(plannedCourseStudent);
        }

        Integer addedToFavoritesNumber = plannedCourseStudentService.getFavoritesStudentsNumberForCourse(plannedCourse.getCourse());
        Integer attendingNumber = plannedCourseStudentService.getAttendingStudentsNumberForCourse(plannedCourse.getCourse());
        plannedCourse.setUsersAddedToFavorites(addedToFavoritesNumber);
        plannedCourse.setAttending(plannedCourseStudent.getAttending());
        plannedCourse.setUsersAttending(attendingNumber);
        plannedCourse.setDisplayUsersAttending(attendingNumber == null ? null : attendingNumber.toString() + " " + getMessageSource().getMessage(getDisplayUsersAttendingLabel(attendingNumber), null, LocaleContextHolder.getLocale()));

        ApplicationUserMessageFormat messageFormat = plannedCourseStudent.getAttending()
                ? ApplicationUserMessageFormat.COURSE_STUDENT_ATTENDING
                : ApplicationUserMessageFormat.COURSE_STUDENT_NOT_ATTENDING;
        notificationManagerService.notifyUser((AelUser) currentUser, plannedCourse.getCourse().getOwner(), null,
                new Object[]{getUserUrlForNotification((AelUser)currentUser), getCourseUrlForNotification(plannedCourse.getCourse()),
                        new Date()},
                UserMessageType.COURSE_RELATED_MESSAGE,
                messageFormat);

        return plannedCourse;
    }

    public void updateDatesWithTimeZone(Course course) {
        if (course != null && course.getClientTimeZoneId() != null) {
            if (course.getPlannedCourseStartDate() != null) {
                course.setPlannedCourseStartDate(datesService.formatDate(course.getPlannedCourseStartDate(), course.getClientTimeZoneId()));
            }
            if (course.getPlannedCourseEndDate() != null) {
                course.setPlannedCourseEndDate(datesService.formatDate(course.getPlannedCourseEndDate(), course.getClientTimeZoneId()));
            }
        }
    }

    private void updateTimes(Course course) {
        if (course != null) {
            String startTimeRaw = course.getStartTimeRaw();
            String endTimeRaw = course.getEndTimeRaw();
            if (startTimeRaw != null && startTimeRaw.length() == 5) {
                startTimeRaw = startTimeRaw + ":00";
            }
            if (endTimeRaw != null && endTimeRaw.length() == 5) {
                endTimeRaw = endTimeRaw + ":00";
            }
            course.setPlannedCourseStartTime(datesService.parseTime(startTimeRaw));
            course.setPlannedCourseEndTime(datesService.parseTime(endTimeRaw));
        }
    }


    public void removePlannedCoursesAndComments(AelUser aelUser) {

        // Get list of planned courses created by specified user.
        List<PlannedCourse> plannedCourses = ((PlannedCourseRepository)getResourceRepository()).findByCreatorId(aelUser.getId());

        for(PlannedCourse plannedCourse : plannedCourses) {
            // Remove comments under this item.
            commentService.removeByPlannedCourseId(plannedCourse.getId());

            // Remove item itself (row/record from planned course table).
            super.remove(plannedCourse.getId());
        }

        // Finally, remove all comments this user posted under different items/offers.
        commentService.removeByUserId(aelUser.getId());
    }

    private String getCourseUrlForNotification(Course course) {
        if (course != null) {
            Preference hostPreference = preferencesService.getByName(Preferences.GLOBAL_AEL_SERVER_HOST.get());
            Preference schemePreference = preferencesService.getByName(Preferences.GLOBAL_HTTP_SCHEME_TYPE.get());
            String courseUrl = "";
            if (hostPreference != null && hostPreference.getValue() != null && schemePreference != null && schemePreference.getValue() != null) {
                courseUrl = schemePreference.getValue() + hostPreference.getValue() + WebPath.NOTIFICATION_COURSES + course.getId();
            }
            return "<a href=\"" + courseUrl + "\">" + course.getName()+ "</a>";
        }
        return null;
    }

    private String getPlannedCourseUrlForNotification(PlannedCourse course) {
        if (course != null) {
            Preference hostPreference = preferencesService.getByName(Preferences.GLOBAL_AEL_SERVER_HOST.get());
            Preference schemePreference = preferencesService.getByName(Preferences.GLOBAL_HTTP_SCHEME_TYPE.get());
            String courseUrl = "";
            if (hostPreference != null && hostPreference.getValue() != null && schemePreference != null && schemePreference.getValue() != null) {
                courseUrl = schemePreference.getValue() + hostPreference.getValue() + WebPath.NOTIFICATION_PLANNED_COURSES + course.getId();
            }
            return "<a href=\"" + courseUrl + "\">" + course.getName()+ "</a>";
        }
        return null;
    }

    private String getUserUrlForNotification(AelUser user) {
        if (user != null) {
            Preference hostPreference = preferencesService.getByName(Preferences.GLOBAL_AEL_SERVER_HOST.get());
            Preference schemePreference = preferencesService.getByName(Preferences.GLOBAL_HTTP_SCHEME_TYPE.get());
            String userUrl = "";
            if (hostPreference != null && hostPreference.getValue() != null && schemePreference != null && schemePreference.getValue() != null) {
                userUrl = schemePreference.getValue() + hostPreference.getValue() + WebPath.NOTIFICATION_PROFILE + user.getId();
            }
            return "<a href=\"" + userUrl + "\">" + user.getDisplayName()+ "</a>";
        }
        return null;
    }

    public void addCourseToFavorites(Long id, PlannedCourse plannedCourse, User currentUser) {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
        PlannedCourseStudent plannedCourseStudent = plannedCourseStudentService.findByPlanificationAndStundent(id, currentUser.getId());

        if(plannedCourseStudent == null)
        {
            plannedCourseStudent = plannedCourseStudentService.createPlannedCourseStudent(currentUser, plannedCourse, Boolean.TRUE, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE);
        }
        else
        {
            plannedCourseStudent.setAddedToFavorites(!plannedCourseStudent.getAddedToFavorites());
            plannedCourse.setAddedToFavorite(plannedCourseStudent.getAddedToFavorites());
            plannedCourseStudent.setRemoveItem(Boolean.FALSE);
            plannedCourseStudentService.update(plannedCourseStudent);
        }

        Integer addedToFavoritesNumber = plannedCourseStudentService.getFavoritesStudentsNumberForCourse(plannedCourse.getCourse());
        plannedCourse.setUsersAddedToFavorites(addedToFavoritesNumber);
        plannedCourse.setAddedToFavorite(plannedCourseStudent.getAddedToFavorites());

        ApplicationUserMessageFormat messageFormat = plannedCourseStudent.getAddedToFavorites()
                ? ApplicationUserMessageFormat.COURSE_STUDENT_ADDED_TO_FAVORITES
                : ApplicationUserMessageFormat.COURSE_STUDENT_REMOVED_FROM_FAVORITES;
        notificationManagerService.notifyUser((AelUser) currentUser, plannedCourse.getCourse().getOwner(), null,
                new Object[]{getUserUrlForNotification((AelUser)currentUser), getCourseUrlForNotification(plannedCourse.getCourse()),
                        new Date()},
                UserMessageType.COURSE_RELATED_MESSAGE,
                messageFormat);
    }

    public void saveComment(AelUser user, PlannedCourse plannedCourse, String comment) {
        commentService.saveComment(plannedCourse, comment);

        if (user != null && plannedCourse.getCourse().getOwner() != null && !user.getId().equals(plannedCourse.getCourse().getOwner().getId())) {
            notificationManagerService.notifyUser(user, plannedCourse.getCourse().getOwner(), null,
                    new Object[]{getUserUrlForNotification(user), getCourseUrlForNotification(plannedCourse.getCourse()),
                            new Date()},
                    UserMessageType.COURSE_RELATED_MESSAGE,
                    ApplicationUserMessageFormat.COURSE_STUDENT_ADD_COMMENT);
        }
    }

    public void notifyAttendeesOnDelete(List<AelUser> attendees, Course course, Date startDate, Date endDate) {
        if (attendees != null) {
            for (AelUser attendee : attendees) {
                notificationManagerService.notifyUser(course.getOwner(), attendee, null,
                        new Object[]{course.getName(), startDate, endDate == null ? "-" : endDate, getUserUrlForNotification(course.getOwner())},
                        UserMessageType.COURSE_RELATED_MESSAGE,
                        ApplicationUserMessageFormat.COURSE_STUDENT_DELETE_ITEM);
            }
        }
    }

    public void notifyAttendeesOnUpdateAvailability(Course course) {
        List<AelUser> attendees = plannedCourseStudentService.getAttendingStudentsForCourse(course.getId());
        List<PlannedCourse> plannedCourses = findByCourseId(course.getId());
        PlannedCourse plannedCourse = null;
        if (plannedCourses != null && plannedCourses.size() > 0) {
            plannedCourse = plannedCourses.get(0);
        }
        if (attendees != null) {
            for (AelUser attendee : attendees) {
                notificationManagerService.notifyUser(course.getOwner(), attendee, null,
                        new Object[]{getPlannedCourseUrlForNotification(plannedCourse), getUserUrlForNotification(course.getOwner())},
                        UserMessageType.COURSE_RELATED_MESSAGE,
                        ApplicationUserMessageFormat.COURSE_STUDENT_UPDATE_ITEM);
            }
        }
    }

    public void notifyAttendeesOnUpdateLocation(Course course) {
        List<AelUser> attendees = plannedCourseStudentService.getAttendingStudentsForCourse(course.getId());
        List<PlannedCourse> plannedCourses = findByCourseId(course.getId());
        PlannedCourse plannedCourse = null;
        if (plannedCourses != null && plannedCourses.size() > 0) {
            plannedCourse = plannedCourses.get(0);
        }
        if (attendees != null) {
            for (AelUser attendee : attendees) {
                notificationManagerService.notifyUser(course.getOwner(), attendee, null,
                        new Object[]{getPlannedCourseUrlForNotification(plannedCourse), getUserUrlForNotification(course.getOwner())},
                        UserMessageType.COURSE_RELATED_MESSAGE,
                        ApplicationUserMessageFormat.COURSE_STUDENT_UPDATE_ITEM_LOCATION);
            }
        }
    }

    public List<PlannedCourse> getAttendedPlannedCourses(AelUser user) {
        List<PlannedCourse> plannedCourses = ((PlannedCourseRepository)getResourceRepository()).getAttendedPlannedCourses(user.getId(), datesService.getYesterday());
        setDefaultInfo(user, plannedCourses);
        return plannedCourses;
    }

    private void setDefaultInfo(AelUser user, List<PlannedCourse> plannedCourses) {
        for(PlannedCourse plannedCourse : plannedCourses) {
            setDisplayUsersAddedToFavorites(plannedCourse, user);
            setDisplayStartDate(plannedCourse);
            courseService.setDefaultImage(plannedCourse.getCourse());
            plannedCourse.setImage(plannedCourse.getCourse().getImage());
            plannedCourse.setImageSrc(plannedCourse.getCourse().getImageSrc());
        }
    }

    public List<PlannedCourse> getAttendingPlannedCourses(AelUser user) {
        List<PlannedCourse> plannedCourses = ((PlannedCourseRepository)getResourceRepository()).getAttendingPlannedCourses(user.getId(), datesService.getYesterday());
        setDefaultInfo(user, plannedCourses);
        return plannedCourses;
    }

    private void setDisplayStartDate(PlannedCourse plannedCourse) {
        if (plannedCourse != null) {
            plannedCourse.setDisplayStartDate(getDisplayStartDate(plannedCourse));
        }
    }

    public List<Long> getPlannedCoursesForLocationAndInterest(String coordinates, Long tagId) {
        return ((PlannedCourseRepository)getResourceRepository()).getPlannedCoursesForLocationAndInterest(coordinates, tagId);
    }

    public List<Long> getPlannedCoursesForCountryAndInterest(String coordinates, Long tagId, Date endDate) {
        return ((PlannedCourseRepository)getResourceRepository()).getPlannedCoursesForCountryAndInterest(coordinates, tagId, endDate);
    }

    public List<PlannedCourse> getTopPlannedCoursesForLocationAndInterests(String country, String coordinates, String interests, Long maxCoursesNoForCountry, Long maxCoursesNoForLocation) {
        List<PlannedCourse> topPlannedCourses = new ArrayList<>();
        List<Long> topPlannedCourseIds = new ArrayList<>();
        List<Country> countries = countryService.findAllByCountryCode(country);
        String countryName = DEFAULT_COUNTRY_NAME;
        if (countries != null && countries.size() > 0) {
            countryName = countries.get(0).getCountryName();
        }
        StringTokenizer st = new StringTokenizer(interests,",");
        List<Long> interestsIds = new ArrayList<>();
        while (st.hasMoreElements()) {
            interestsIds.add( Long.valueOf((String)st.nextElement()));
        }

        if (coordinates != null) {

            filterPlannedCoursesByCoordinates(coordinates, topPlannedCourseIds, interestsIds, maxCoursesNoForLocation);
            if(topPlannedCourseIds.size() < TRY_PALETTE_ITEMS_NO) {
                filterPlannedCoursesByCountry(country, topPlannedCourseIds, interestsIds, maxCoursesNoForCountry);
            }
        }
        if (country != null && topPlannedCourseIds.size() < TRY_PALETTE_ITEMS_NO) {
            filterPlannedCoursesByCountry(country, topPlannedCourseIds, interestsIds, maxCoursesNoForCountry);
        }

        for (Long topPlannedCourseId : topPlannedCourseIds) {
            PlannedCourse plannedCourse = findOneById(topPlannedCourseId);
            decorateCourseSessionForCardOnly(plannedCourse, countryName);
            topPlannedCourses.add(plannedCourse);
        }
        return topPlannedCourses;
    }

    private void filterPlannedCoursesByCoordinates(String coordinates, List<Long> topPlannedCourseIds, List<Long> interestsIds, Long maxCoursesNo) {
        Integer plannedCoursesNo = 0;
        while(topPlannedCourseIds.size() < TRY_PALETTE_ITEMS_NO && plannedCoursesNo < maxCoursesNo) {
            for (Long interestId : interestsIds) {
                if (topPlannedCourseIds.size() == TRY_PALETTE_ITEMS_NO) {
                    break;
                }
                List<Long> plannedCoursesIds = getPlannedCoursesForLocationAndInterest(coordinates, interestId);
                plannedCoursesNo += plannedCoursesIds.size();
                filterPlannedCourse(topPlannedCourseIds, plannedCoursesIds);
            }
        }
    }

    private void filterPlannedCoursesByCountry(String country, List<Long> topPlannedCourseIds, List<Long> interestsIds, Long maxCoursesNo) {
        Integer plannedCoursesNo = 0;
        while(topPlannedCourseIds.size() < TRY_PALETTE_ITEMS_NO && plannedCoursesNo < maxCoursesNo) {
            for (Long interestId : interestsIds) {
                if (topPlannedCourseIds.size() == TRY_PALETTE_ITEMS_NO) {
                    break;
                }
                List<Long> plannedCoursesIds = getPlannedCoursesForCountryAndInterest(country, interestId, datesService.getUTCYesterdayNight());
                plannedCoursesNo += plannedCoursesIds.size();
                filterPlannedCourse(topPlannedCourseIds, plannedCoursesIds);
            }
        }
    }

    private void filterPlannedCourse(List<Long> topPlannedCourseIds, List<Long> plannedCoursesIds) {
        if (plannedCoursesIds.size() > 0) {
            for (Long plannedCourseId : plannedCoursesIds) {
                if (!topPlannedCourseIds.contains(plannedCourseId)) {
                    topPlannedCourseIds.add(plannedCourseId);
                    break;
                }
            }
        }
    }

    private Long getPlannedCoursesCountForCountryAndInterest(String country, Long tagId, Date endDate) {
        return ((PlannedCourseRepository)getResourceRepository()).getPlannedCoursesCountForCountryAndInterest(country, tagId, endDate);
    }

    private Long getPlannedCoursesCountForLocationAndInterest(String country, Long tagId, Date endDate) {
        return ((PlannedCourseRepository)getResourceRepository()).getPlannedCoursesCountForLocationAndInterest(country, tagId, endDate);
    }

    public Long getPlannedCoursesCountByCountryAndInterests(String country, String interests) {
        Long plannedCoursesCount = 0L;
        StringTokenizer st = new StringTokenizer(interests,",");
        List<Long> interestsIds = new ArrayList<>();
        while (st.hasMoreElements()) {
            interestsIds.add( Long.valueOf((String)st.nextElement()));
        }
        for (Long interestId : interestsIds) {
            plannedCoursesCount += getPlannedCoursesCountForCountryAndInterest(country, interestId, datesService.getUTCYesterdayNight());
        }
        return plannedCoursesCount;
    }

    public Long getPlannedCoursesCountByLocationAndInterests(String coordinates, String interests) {
        Long plannedCoursesCount = 0L;
        StringTokenizer st = new StringTokenizer(interests,",");
        List<Long> interestsIds = new ArrayList<>();
        while (st.hasMoreElements()) {
            interestsIds.add( Long.valueOf((String)st.nextElement()));
        }
        for (Long interestId : interestsIds) {
            plannedCoursesCount += getPlannedCoursesCountForLocationAndInterest(coordinates, interestId, datesService.getUTCYesterdayNight());
        }
        return plannedCoursesCount;
    }

    public List<AelUser> getAllAttendingStudents(List<PlannedCourseStudent> plannedCourseStudents) {

        List<AelUser> attendingStudents = new ArrayList<>();
        for(PlannedCourseStudent plannedCourseStudent : plannedCourseStudents) {
            UserDetails userDetails = userDetailsService.getByAelUser(plannedCourseStudent.getUser()).get();
            if (plannedCourseStudent.getAttending() != null && plannedCourseStudent.getAttending() && userDetails.getShowAttendingItems() != null && userDetails.getShowAttendingItems()) {
                attendingStudents.add(plannedCourseStudent.getUser());
            }
        }
        return attendingStudents;
    }

    public List<AelUser> getAttendingStudentsForDisplay(List<AelUser> attendingStudents) {

        List<AelUser> firstAttendingStudents = new ArrayList<>();
        Integer length;
        if (attendingStudents.size() >= 4) {
            length = 4;
        }
        else {
            length = attendingStudents.size();
        }

        for (int i = 0; i < length; i++ ) {
            firstAttendingStudents.add(attendingStudents.get(i));
        }
        return firstAttendingStudents;
    }

    private void decorateCourseSessionForCardOnly(PlannedCourse plannedCourse, String country) {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
        Locale locale = Locale.forLanguageTag(aelUserService.getLanguage(null, request));
        Course course = plannedCourse.getCourse();
        courseService.setDefaultImage(plannedCourse.getCourse());
        if (course.getImage() != null && course.getImage().length > 0) {
            plannedCourse.setImageSrc(imageService.imageSrcFromBytes(course.getImage()));
            plannedCourse.setImage(course.getImage());
        }
        else {
            plannedCourse.setImageSrc(course.getImageSrc());
        }
        plannedCourse.setCourseType(course.getType());
        setDisplayUsersAddedToFavorites(plannedCourse, locale);

        String startDateFormat = plannedCourse.getStartDate() == null ? "" : datesService.formatLongDate(plannedCourse.getStartDate(), country);
        plannedCourse.setDisplayStartDate(startDateFormat);
        Integer usersAddedToFavorites = plannedCourseStudentService.getFavoritesStudentsNumberForCourse(plannedCourse.getCourse());
        plannedCourse.setUsersAddedToFavorites(usersAddedToFavorites);
        plannedCourse.setDisplayUsersAddedToFavorites((usersAddedToFavorites == null ? "0" : usersAddedToFavorites.toString()) + " " + getMessageSource().getMessage("displayUsersAddedToFavorites.label", null, locale));
    }

    public Boolean addCourseComment(String comment, String courseId) {
        if (comment == null || comment.trim().length() == 0) {
            return false;
        }

        AelUser currentUser = (AelUser) WithUserBaseEntityController.getUser();

        List<PlannedCourse> plannedCourses = findByCourseId(Long.parseLong(courseId));
        if (plannedCourses != null && plannedCourses.size() > 0) {
            PlannedCourse plannedCourse = plannedCourses.get(0);
            saveComment(currentUser, plannedCourse, comment);
        }
        return true;
    }

    public Boolean addPlannedCourseComment(String comment, String courseId) {
        if (comment == null || comment.trim().length() == 0) {
            return false;
        }

        AelUser currentUser = (AelUser) WithUserBaseEntityController.getUser();
        PlannedCourse plannedCourse = findOneById(Long.parseLong(courseId));
        saveComment(currentUser, plannedCourse, comment);
        return true;
    }

    private PlannedCourse getRandomService(Long userId, Date endDate, String countryCode, List<Long> tagIds) {
        Long plannedCourseId = ((PlannedCourseRepository)getResourceRepository()).getRandomService(userId, endDate, countryCode, tagIds);
        if (plannedCourseId != null && plannedCourseId > 0) {
            return findOneById(plannedCourseId);
        }
        return null;
    }

    private PlannedCourse getClosestCourse(Long userId, Date endDate, String countryCode, List<Long> tagIds) {
        Long plannedCourseId = ((PlannedCourseRepository)getResourceRepository()).getClosestCourse(userId, endDate, countryCode, tagIds);
        if (plannedCourseId != null && plannedCourseId > 0) {
            return findOneById(plannedCourseId);
        }
        return null;
    }

    private PlannedCourse getMostPopularMeeting(Long userId, Date endDate, String countryCode, List<Long> tagIds) {
        Long plannedCourseId = ((PlannedCourseRepository)getResourceRepository()).getMostPopularMeeting(userId, endDate, countryCode, tagIds);
        if (plannedCourseId != null && plannedCourseId > 0) {
            return findOneById(plannedCourseId);
        }
        return null;
    }

    private PlannedCourse getMostRecentActivity(Long userId, Date endDate, String countryCode, List<Long> tagIds) {
        Long plannedCourseId = ((PlannedCourseRepository)getResourceRepository()).getMostRecentActivity(userId, endDate, countryCode, tagIds);
        if (plannedCourseId != null && plannedCourseId > 0) {
            return findOneById(plannedCourseId);
        }
        return null;
    }

    public String getUserCountryCode(AelUser user) {
        String currentUserCountry = user.getCountry().getName();
        if (currentUserCountry != null) {
            List<Country> countries = countryService.findAllByCountryName(currentUserCountry);
            if (countries != null && countries.size() > 0) {
                return countries.get(0).getCode();
            }
        }
        return null;
    }
}

