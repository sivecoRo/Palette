package ro.siveco.ael.lcms.domain.materials.model;


//import ro.siveco.ael.model.annotations.Filterable;
//import ro.siveco.ael.model.annotations.Tableable;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by IntelliJ IDEA.
 * User: razvanni
 * Date: 03.06.2010
 * Time: 21:15:13
 * To change this template use File | Settings | File Templates.
 */

@Entity
@Table(name = "survey_item_rule")
@SequenceGenerator(name = "SURVEY_ITEM_RULE_SEQ", sequenceName = "survey_item_rule_seq")
public class SurveyItemRule extends ro.siveco.ram.starter.model.base.BaseEntity implements Serializable {
    private Long id;
    private SurveyItem child;
    //@Tableable
    //@Filterable
    private SurveyItem parent;
    //@Tableable
    private String triggerRule;

    @Id
    @GeneratedValue(generator = "SURVEY_ITEM_RULE_SEQ", strategy = GenerationType.AUTO)
    @Column(name = "ID_SURVEY_ITEM_RULE")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne
    @JoinColumn(name = "ID_CHILD_ITEM", nullable = false)
    public SurveyItem getChild() {
        return child;
    }

    public void setChild(SurveyItem child) {
        this.child = child;
    }

    @ManyToOne
    @JoinColumn(name = "ID_PARENT_ITEM", nullable = false)
    public SurveyItem getParent() {
        return parent;
    }

    public void setParent(SurveyItem parent) {
        this.parent = parent;
    }

    @Column(name = "TRIGGER_RULE")
    public String getTriggerRule() {
        return triggerRule;
    }

    public void setTriggerRule(String triggerRule) {
        this.triggerRule = triggerRule;
    }

    @Transient
    public Long getIdEntitate() {
        return getId();
    }

    public void setIdEntitate(Long idEntitate) {
        setId(idEntitate);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SurveyItemRule that = (SurveyItemRule) o;

        if (id != null ? !id.equals(that.id) : that.id != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
