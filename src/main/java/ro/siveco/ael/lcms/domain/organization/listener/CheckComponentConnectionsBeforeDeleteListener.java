package ro.siveco.ael.lcms.domain.organization.listener;

import org.springframework.beans.factory.annotation.Autowired;
import ro.siveco.ael.lcms.domain.organization.model.Component;
import ro.siveco.ael.lcms.domain.organization.service.ComponentService;
import ro.siveco.ram.app.exception.AppException;
import ro.siveco.ram.event.listener.type.single.BaseBeforeDeleteListener;
import ro.siveco.ram.event.type.single.BeforeDeleteEvent;

/**
 * Created by AndradaC on 9/1/2017.
 */
@org.springframework.stereotype.Component
public class CheckComponentConnectionsBeforeDeleteListener extends BaseBeforeDeleteListener<Component>{

    @Autowired
    private ComponentService componentService;

    @Override
    public void onApplicationEvent(BeforeDeleteEvent<Component> event) {
        Component component = event.getDomainModel();

        String returnValue = componentService.connexionsExist(component.getId());
        if (returnValue != null) {
            throw new AppException(returnValue);
        }
    }
}
