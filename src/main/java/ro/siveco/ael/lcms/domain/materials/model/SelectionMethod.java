package ro.siveco.ael.lcms.domain.materials.model;



import java.io.Serializable;

/**
 * Created by IntelliJ IDEA.
 * User: razvanni
 * Date: 31.05.2010
 * Time: 18:14:34
 * To change this template use File | Settings | File Templates.
 */
public enum SelectionMethod implements Serializable, MessageResolvableEnum
{
	MULTIPLE("multiple.selection"),
	SINGLE("single.selection");
	String key;

	SelectionMethod(String key)
	{
		this.key = key;
	}

	public String getKey()
	{
		return key;
	}
}
