package ro.siveco.ael.lcms.domain.tenant.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.tenant.model.Ip;
import ro.siveco.ael.lcms.domain.tenant.service.IpService;
import ro.siveco.ram.event.listener.type.single.BaseBeforeUpdateListener;
import ro.siveco.ram.event.type.single.BeforeUpdateEvent;

/**
 * Created by AndradaC on 8/31/2017.
 */
@Component
public class SetTenantForIpBeforeUpdateListener extends BaseBeforeUpdateListener<Ip>{
    @Autowired
    private IpService ipService;

    @Override
    public void onApplicationEvent(BeforeUpdateEvent<Ip> event) {
        Ip ip = event.getDomainModel();
        Ip databaseIp = event.getStoredDomainModel();

        ipService.checkIpValidation(ip);
        if (!ip.getValue().equals(databaseIp.getValue())) {
            ipService.checkIfIpExists(ip.getValue(), databaseIp.getTenant());
        }

        ip.setId(databaseIp.getId());
        ip.setTenant(databaseIp.getTenant());

        ipService.checkIpValidation(ip);
    }
}
