package ro.siveco.ael.lcms.domain.notification.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.siveco.ael.lcms.domain.auth.model.AelUser;
import ro.siveco.ael.lcms.domain.auth.model.User;
import ro.siveco.ael.lcms.domain.course.model.PlannedCourse;
import ro.siveco.ael.lcms.domain.course.model.PlannedCourseStudent;
import ro.siveco.ael.lcms.domain.course.service.PlannedCourseService;
import ro.siveco.ael.lcms.domain.course.service.PlannedCourseStudentService;
import ro.siveco.ael.lcms.domain.notification.model.*;
import ro.siveco.ael.lcms.domain.preference.model.Preference;
import ro.siveco.ael.lcms.domain.preference.model.Preferences;
import ro.siveco.ael.lcms.domain.preference.service.PreferencesService;
import ro.siveco.ael.lcms.repository.NotificationPreferenceRepository;
import ro.siveco.ael.lcms.repository.NotificationScheduleRepository;
import ro.siveco.ael.lcms.repository.PlannedCourseRepository;
import ro.siveco.ael.lcms.repository.PlannedCourseStudentRepository;
import ro.siveco.ael.service.utils.DatesService;
import ro.siveco.ael.web.components.WebPath;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.*;

/**
 * Created by IntelliJ IDEA.
 * AelUser: SorinT
 * Date: 06.08.2007
 */
@Service
public class NotificationManagerService {
    @Autowired
    private NotificationScheduleRepository notificationScheduleRepository;
    @Autowired
    private NotificationPreferenceRepository notificationPreferenceRepository;

    @Autowired
    private UserMessageFormatService userMessageFormatService;

    @Autowired
    private PlannedCourseStudentRepository plannedCourseStudentRepository;

    @Autowired
    private PlannedCourseRepository plannedCourseRepository;

    @Autowired
    private DatesService datesService;

    @Autowired
    private PlannedCourseService plannedCourseService;

    @Autowired
    private PlannedCourseStudentService plannedCourseStudentService;

    @Autowired
    PreferencesService preferencesService;

    private final Log logger = LogFactory.getLog(NotificationManagerService.class);

    public void notifyUser(AelUser from, AelUser to, Object[] subjectArgs, Object[] messageBodyArgs,
                           UserMessageType userMessageType,
                           ApplicationUserMessageFormat userMessageFormat) {
        notifyUser(from, to, subjectArgs, messageBodyArgs, userMessageType, userMessageFormat,
                NotificationType.MESSAGE_AND_MAIL, null);
    }

    public void notifyUser(AelUser from, List<AelUser> to, Object[] subjectArgs, Object[] messageBodyArgs,
                           UserMessageType userMessageType,
                           ApplicationUserMessageFormat userMessageFormat) {
        notifyUser(from, to, subjectArgs, messageBodyArgs, userMessageType, userMessageFormat,
                NotificationType.MESSAGE_AND_MAIL, null);
    }

    public void notifyUser(AelUser from, AelUser to, Object[] subjectArgs, Object[] messageBodyArgs,
                           UserMessageType userMessageType,
                           ApplicationUserMessageFormat userMessageFormat, Long entityId, String creatorIdAndTitle) {
        notifyUser(from, new AelUser[]{to}, subjectArgs, messageBodyArgs, userMessageType, userMessageFormat,
                NotificationType.MESSAGE_AND_MAIL, entityId, creatorIdAndTitle, null);

    }

    public void notifyUser(AelUser from, AelUser[] to, Object[] subjectArgs, Object[] messageBodyArgs,
                           UserMessageType userMessageType,
                           ApplicationUserMessageFormat userMessageFormat, Long entityId, String creatorIdAndTitle) {
        notifyUser(from, to, subjectArgs, messageBodyArgs, userMessageType, userMessageFormat,
                NotificationType.MESSAGE_AND_MAIL, entityId, creatorIdAndTitle, null);
    }

    public void notifyUser(AelUser from, List<AelUser> to, Object[] subjectArgs, Object[] messageBodyArgs,
                           UserMessageType userMessageType,
                           ApplicationUserMessageFormat userMessageFormat, Long entityId, String creatorIdAndTitle) {
        AelUser[] userToList = to.toArray(new AelUser[to.size()]);
        notifyUser(from, userToList, subjectArgs, messageBodyArgs, userMessageType, userMessageFormat,
                NotificationType.MESSAGE_AND_MAIL, entityId, creatorIdAndTitle, null);
    }

    public void notifyUser(AelUser from, AelUser to, Object[] subjectArgs, Object[] messageBodyArgs,
                           UserMessageType userMessageType,
                           ApplicationUserMessageFormat userMessageFormat, NotificationType notificationType,
                           String creatorIdAndTitle) {
        notifyUser(from, new AelUser[]{to}, subjectArgs, messageBodyArgs, userMessageType, userMessageFormat,
                notificationType, null, creatorIdAndTitle, null);
    }

    public void notifyUser(AelUser from, List<AelUser> to, Object[] subjectArgs, Object[] messageBodyArgs,
                           UserMessageType userMessageType,
                           ApplicationUserMessageFormat userMessageFormat, NotificationType notificationType,
                           String creatorIdAndTitle) {
        AelUser[] userToList = to.toArray(new AelUser[to.size()]);
        notifyUser(from, userToList, subjectArgs, messageBodyArgs, userMessageType, userMessageFormat, notificationType,
                null, creatorIdAndTitle, null);
    }

    public void saveNotificationPreferencesForUser(Map<String, Object> notificationPrefMap, AelUser user) {
        notificationPreferenceRepository.deleteNotificationPreferencesByUserId(user.getId());
        for (Map.Entry<String, Object> entryPref : notificationPrefMap.entrySet()) {
            String prefKey = entryPref.getKey();
            String prefValue = entryPref.getValue() instanceof Boolean ? String.valueOf(entryPref.getValue()) :
                    entryPref.getValue().toString();
            if (prefValue != null && !prefValue.equals("") && !prefKey.equals("threshold")) {
                NotificationPreference notificationPreference = new NotificationPreference();
                notificationPreference.setUser(user);
                notificationPreference
                        .setUserMessageType(UserMessageType.valueOf(prefKey.toUpperCase().replace('.', '_')));
                notificationPreference.setValue(prefKey.equals("attendance.below.threshold") ?
                        prefValue.equals("false") ? prefValue : notificationPrefMap.get("threshold").toString() :
                        prefValue);
                notificationPreferenceRepository.save(notificationPreference);
            }
        }
    }

    public Map<String, Object> getNotificationPreferencesForUser(AelUser user) {
        Map<String, Object> userNotificationPrefMap = new HashMap<String, Object>();
        List<NotificationPreference> notificationPreferences =
                notificationPreferenceRepository.getNotificationPreferencesByUserId(user.getId());
        for (NotificationPreference notificationPreference : notificationPreferences) {
            userNotificationPrefMap
                    .put(notificationPreference.getUserMessageType().getName(), notificationPreference.getValue());
        }
        return userNotificationPrefMap;
    }

    protected boolean isPlannedCourseSuitableForAttendanceBelowThresholdNotification(PlannedCourse plannedCourse) {
        return true;
    }

    public void sendDeadlinesNotification(List<AelUser> participants, PlannedCourse plannedCourse) {
        Date dateToSchedule = getDateToScheduleDeadlineNotification(plannedCourse);
        if (dateToSchedule != null) {
            for (AelUser user : participants) {
                if (!user.getId().equals(plannedCourse.getTeacher().getId())) {
                    notificationScheduleRepository.deleteUnsentNotificationScheduleByTypeAndUserAndEntityId(
                            UserMessageType.DEADLINE_NOTIFICATION, user.getId(), plannedCourse.getId());
                    notifyUser(plannedCourse.getTeacher(),
                            user, null,
                            new Object[]
                                    {plannedCourse.getCourse().getName(),
                                            plannedCourse.getStartDate(),
                                            plannedCourse.getEndDate()
                                    },
                            UserMessageType.DEADLINE_NOTIFICATION,
                            ApplicationUserMessageFormat.DEADLINE_PLANNED_COURSE, plannedCourse.getId(), null,
                            dateToSchedule);
                }
            }
        }
    }

    public void deleteDeadlineNotifications(Set<AelUser> users, PlannedCourse plannedCourse) {
        for (AelUser user : users) {
            notificationScheduleRepository
                    .deleteUnsentNotificationScheduleByTypeAndUserAndEntityId(UserMessageType.DEADLINE_NOTIFICATION,
                            user.getId(), plannedCourse.getId());
        }
    }

    public void notifyUserOnceForQuotaExceed(Optional<User> oneByUsername, User user) {
        //TODO: de implementat
    }

    public void notifyUserOnceUploadSuccess(User user) {
        //TODO: de implementat
    }

    public void notifyUserOnceUploadFail(User user) {
        //TODO: de implementat
    }

    public void notifyUserOnceImportContentFail(User user){
        //TODO: de implementat
    }

    public void notifyUserOnceImportContentSuccess(User user){
        //TODO: de implementat
    }

    protected void notifyUser(AelUser from, AelUser to, Object[] subjectArgs, Object[] messageBodyArgs,
                              UserMessageType userMessageType,
                              ApplicationUserMessageFormat userMessageFormat, Long entityId, String creatorIdAndTitle,
                              Date dateToSchedule) {
        notifyUser(from, new AelUser[]{to}, subjectArgs, messageBodyArgs, userMessageType, userMessageFormat,
                NotificationType.MESSAGE_AND_MAIL, entityId, creatorIdAndTitle, dateToSchedule);
    }

    private void notifyUser(final AelUser from, final AelUser[] to, final Object[] subjectArgs,
                            final Object[] messageBodyArgs, final UserMessageType userMessageType,
                            final ApplicationUserMessageFormat userMessageFormat,
                            final NotificationType notificationType,
                            final Long entityId, final String creatorIdAndTitle, Date dateToSchedule) {
        if (from == null || to == null) {
            return;
        }

        for (AelUser userTo : to) {
            if (dateToSchedule != null || getScheduleNotificationForUser(userTo, userMessageType)) {
                NotificationSchedule notificationSchedule = new NotificationSchedule();
                notificationSchedule.setFromUser(from);
                notificationSchedule.setToUser(userTo);
                notificationSchedule.setSubjectArgs(serializeObject(subjectArgs));
                notificationSchedule.setMessageBodyArgs(serializeObject(messageBodyArgs));
                notificationSchedule.setUserMessageType(userMessageType);
                notificationSchedule.setUserMessageFormat(userMessageFormat);
                notificationSchedule.setNotificationType(notificationType);
                notificationSchedule.setStatus(false);
                notificationSchedule.setDate(dateToSchedule != null ? dateToSchedule : new Date());
                notificationSchedule.setEntityId(entityId);
                notificationSchedule.setCreatorIdAndTitle(creatorIdAndTitle);
                notificationScheduleRepository.save(notificationSchedule);
            }
        }
    }

    protected boolean getScheduleNotificationForUser(AelUser user, UserMessageType userMessageType) {
        if (userMessageType.equals(UserMessageType.ATTENDANCE_BELOW_THRESHOLD) ||
                userMessageType.equals(UserMessageType.FAILED_ASSESSMENTS)) {
            return true;
        }
        NotificationPreference notificationPreference = notificationPreferenceRepository
                .getNotificationPreferencesByUserIdAndByType(user.getId(), userMessageType);
        return notificationPreference == null || !notificationPreference.getValue().equals("false");
    }

    protected Date getDateToScheduleDeadlineNotification(PlannedCourse plannedCourse) {
        NotificationPreference notificationPreference = notificationPreferenceRepository
                .getNotificationPreferencesByUserIdAndByType(plannedCourse.getTeacher().getId(),
                        UserMessageType.DEADLINE_NOTIFICATION);

        if (notificationPreference == null) {
            return null;
        }

        //serMessageType.DEADLINE_NOTIFICATION - notificationPreference values: 1 - 1 day / 2 - 1 week / 3 - 1 month

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(plannedCourse.getEndDate());
        calendar.add(Calendar.DATE, notificationPreference.getValue().equals("1") ? -1 :
                notificationPreference.getValue().equals("2") ? -7 : 0);
        calendar.add(Calendar.MONTH, notificationPreference.getValue().equals("3") ? -1 : 0);

        return calendar.getTime();
    }

    private byte[] serializeObject(Object[] objectArray) {
        if (objectArray != null) {
            try {
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                ObjectOutputStream out = new ObjectOutputStream(bos);
                out.writeObject(objectArray);
                out.close();
                return bos.toByteArray();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public void setNotificationScheduleRepository(NotificationScheduleRepository notificationScheduleRepository) {
        this.notificationScheduleRepository = notificationScheduleRepository;
    }

    public void setNotificationPreferenceRepository(NotificationPreferenceRepository notificationPreferenceRepository) {
        this.notificationPreferenceRepository = notificationPreferenceRepository;
    }

    public void notifyUser(AelUser from, AelUser to, Object[] subjectArgs, Object[] messageBodyArgs,
                           UserMessageType userMessageType,
                           ApplicationUserMessageFormat userMessageFormat, NotificationType notificationType,
                           Long entityId, String creatorIdAndTitle, Date dateToSchedule,
                           PlannedNotification plannedNotification) {

        if (from == null || to == null) {
            return;
        }

        if (dateToSchedule != null || getScheduleNotificationForUser(to, userMessageType)) {
            NotificationSchedule notificationSchedule = new NotificationSchedule();
            notificationSchedule.setFromUser(from);
            notificationSchedule.setToUser(to);
            notificationSchedule.setSubjectArgs(serializeObject(subjectArgs));
            notificationSchedule.setMessageBodyArgs(serializeObject(messageBodyArgs));
            notificationSchedule.setUserMessageType(userMessageType);
            notificationSchedule.setUserMessageFormat(userMessageFormat);
            notificationSchedule.setNotificationType(notificationType);
            notificationSchedule.setStatus(false);
            notificationSchedule.setDate(dateToSchedule != null ? dateToSchedule : new Date());
            notificationSchedule.setEntityId(entityId);
            notificationSchedule.setCreatorIdAndTitle(creatorIdAndTitle);
            notificationSchedule.setPlannedNotification(plannedNotification);
            notificationScheduleRepository.save(notificationSchedule);
        }
    }

    public void sendPastReviewEmails() {
        List<PlannedCourse> plannedCourses = plannedCourseRepository.getPlannedCoursesForPastReview(datesService.getYesterday());
        if (plannedCourses !=null) {
            for(PlannedCourse plannedCourse : plannedCourses) {
                List<PlannedCourseStudent> attendees = plannedCourseStudentRepository.getAttendingPCStudentsForCourse(plannedCourse.getCourse().getId());
                if (attendees != null) {
                    for(PlannedCourseStudent attendee : attendees) {
                        String attendeeLink = getUrlForNotification(WebPath.PAST_REVIEW_ATTENDEE, plannedCourse.getId().toString(),"userId", attendee.getId().toString());
                        notifyUser(plannedCourse.getCourse().getOwner(), attendee.getUser(), null,
                                new Object[]{plannedCourse.getCourse().getName(), attendeeLink},
                                UserMessageType.COURSE_RELATED_MESSAGE,
                                ApplicationUserMessageFormat.COURSE_STUDENT_PAST_REVIEW);

                        attendee.setReviewEmailSent(true);
                        plannedCourseStudentService.save(attendee);
                    }
                }

                String ownerLink = getUrlForNotification(WebPath.PAST_REVIEW_OWNER, plannedCourse.getCourse().getId().toString(),"courseId", plannedCourse.getId().toString());
                notifyUser(plannedCourse.getCourse().getOwner(), plannedCourse.getCourse().getOwner(), null,
                        new Object[]{plannedCourse.getCourse().getName(), ownerLink},
                        UserMessageType.COURSE_RELATED_MESSAGE,
                        ApplicationUserMessageFormat.COURSE_OWNER_PAST_REVIEW);

                plannedCourse.setReviewEmailSent(true);
                plannedCourseService.save(plannedCourse);
            }
        }
    }

    private String getUrlForNotification(String path, String pathId, String type, String param) {
        Preference hostPreference = preferencesService.getByName(Preferences.GLOBAL_AEL_SERVER_HOST.get());
        Preference schemePreference = preferencesService.getByName(Preferences.GLOBAL_HTTP_SCHEME_TYPE.get());
        if (hostPreference != null && hostPreference.getValue() != null && schemePreference != null && schemePreference.getValue() != null) {
            String courseUrl =  schemePreference.getValue() + hostPreference.getValue() + path;
            if (param != null) {
                courseUrl = courseUrl + "/" + pathId + "?" + type + "=" + param;
            }
            return courseUrl;
        }
        return null;
    }
}
