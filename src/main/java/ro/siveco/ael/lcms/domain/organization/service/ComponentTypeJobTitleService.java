package ro.siveco.ael.lcms.domain.organization.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.siveco.ael.lcms.domain.organization.model.ComponentType;
import ro.siveco.ael.lcms.domain.organization.model.ComponentTypeJobTitle;
import ro.siveco.ael.lcms.domain.organization.model.JobTitle;
import ro.siveco.ael.lcms.repository.ComponentTypeJobTitleRepository;
import ro.siveco.ram.starter.service.BaseEntityService;

import java.util.List;

/**
 * Created by AndradaC on 5/29/2017.
 */
@Service
public class ComponentTypeJobTitleService extends BaseEntityService<ComponentTypeJobTitle>
{
    @Autowired
    public ComponentTypeJobTitleService(ComponentTypeJobTitleRepository componentTypeJobTitleRepository) {
        super(ComponentTypeJobTitle.class, componentTypeJobTitleRepository);
    }

    public List<JobTitle> getJobTitlesByComponentTypeId( Long componentTypeId )
    {
        return ((ComponentTypeJobTitleRepository)getResourceRepository()).getJobTitlesByComponentTypeId(componentTypeId);
    }

    public ComponentTypeJobTitle saveOrUpdate(ComponentTypeJobTitle componentTypeJobTitle)
    {
        if(componentTypeJobTitle.getId() == null)
        {
            return save(componentTypeJobTitle);
        }
        else
        {
            return update(componentTypeJobTitle);
        }
    }

    public ComponentTypeJobTitle createComponentTypeJobTitle(ComponentType componentType, JobTitle jboTitle){
        ComponentTypeJobTitle newComponentTypeJobTitle = new ComponentTypeJobTitle();
        newComponentTypeJobTitle.setComponentType(componentType);
        newComponentTypeJobTitle.setJobTitle(jboTitle);
        newComponentTypeJobTitle = saveOrUpdate(newComponentTypeJobTitle);
        return newComponentTypeJobTitle;
    }
}
