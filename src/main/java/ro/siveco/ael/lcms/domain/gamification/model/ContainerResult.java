package ro.siveco.ael.lcms.domain.gamification.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ContainerResult {

    private List<Container> list;

    public List<Container> getList() {

        return list;
    }

    public void setList(List<Container> list) {

        this.list = list;
    }
}
