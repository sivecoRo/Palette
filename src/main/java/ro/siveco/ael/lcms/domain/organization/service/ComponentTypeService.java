package ro.siveco.ael.lcms.domain.organization.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.siveco.ael.lcms.domain.organization.model.ComponentType;
import ro.siveco.ael.lcms.repository.ComponentTypeRepository;
import ro.siveco.ram.starter.service.BaseEntityService;

import java.util.List;

/**
 * Created by AndradaC on 5/29/2017.
 */
@Service
public class ComponentTypeService extends BaseEntityService<ComponentType>
{
    @Autowired
    public ComponentTypeService(ComponentTypeRepository componentTypeRepository) {
        super(ComponentType.class, componentTypeRepository);
    }

    public ComponentType saveOrUpdate(ComponentType componentType)
    {
        if(componentType.getId() == null)
        {
            return save(componentType);
        }
        else
        {
            return update(componentType);
        }
    }

    public List<ComponentType> getAllForRoot() {

        return ((ComponentTypeRepository) getResourceRepository()).findAllByTenant(null);
    }
}
