package ro.siveco.ael.lcms.domain.course.service;

import com.querydsl.core.types.dsl.BooleanExpression;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import ro.siveco.ael.service.utils.FilterCategories;

import java.util.List;
import java.util.Map;
import java.util.Optional;


@Service
public class CourseFilterService {

    @Autowired
    private ItemCommonFilterService itemCommonFilterService;

    @Autowired
    PlannedCourseService plannedCourseService;

    public Optional<BooleanExpression> createFilterForParameters(MultiValueMap<String, String> parameters, BooleanExpression filterExpression)
    {

        for (Map.Entry<String, List<String>> entry : parameters.entrySet())
        {
            String filterCategory = entry.getKey();
            List<String> filterValues = entry.getValue();

            switch (filterCategory) {
                case FilterCategories.FILTER_MY_SEARCH: {
                    BooleanExpression searchExpression = itemCommonFilterService.getMySearchFilterExpression(filterValues);
                    filterExpression = filterExpression.and(searchExpression);
                    break;
                }
                case FilterCategories.FILTER_TYPE: {
                    BooleanExpression typeExpression = itemCommonFilterService.getTypeFilterExpression(filterValues, true);
                    filterExpression = filterExpression.and(typeExpression);
                    break;
                }
                case FilterCategories.FILTER_DATE:  {
                    BooleanExpression dateExpression = itemCommonFilterService.getDateFilterExpression(filterValues, true);
                    BooleanExpression calendarExpression = null;
                    if(filterValues.contains("CALENDAR"))
                    {
                        List<String> dateValues = parameters.get(FilterCategories.FILTER_CALENDAR);
                        calendarExpression = itemCommonFilterService.getCalendarFilterExpression(dateValues, true);
                    }
                    if(dateExpression != null) {
                        if(calendarExpression != null)
                        {
                            dateExpression = dateExpression.or(calendarExpression);
                        }
                        filterExpression = filterExpression.and(dateExpression);
                    }
                    else
                    {
                        if(calendarExpression != null)
                        {
                            filterExpression = filterExpression.and(calendarExpression);
                        }
                    }
                    break;
                }
                case FilterCategories.FILTER_DISTANCE:  {
                    BooleanExpression distanceExpression = itemCommonFilterService.getDistanceFilterExpression(filterValues, true);
                    if(distanceExpression != null) {
                        filterExpression = filterExpression.and(distanceExpression);
                    }
                    break;
                }
                case FilterCategories.FILTER_TAG: {
                    BooleanExpression tagExpression = itemCommonFilterService.getTagFilterExpression(filterValues, true);
                    filterExpression = filterExpression.and(tagExpression);
                    break;
                }
            }
        }


        return Optional.of(filterExpression);
    }

    public BooleanExpression getFilterForNotExpired() {
        return itemCommonFilterService.getNotExpiredExpression();
    }

}
