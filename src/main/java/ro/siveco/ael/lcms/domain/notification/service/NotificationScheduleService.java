package ro.siveco.ael.lcms.domain.notification.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.siveco.ael.lcms.domain.notification.model.NotificationSchedule;
import ro.siveco.ael.lcms.domain.notification.model.PlannedNotification;
import ro.siveco.ael.lcms.domain.notification.model.UserMessageType;
import ro.siveco.ael.lcms.repository.NotificationScheduleRepository;
import ro.siveco.ram.starter.service.BaseEntityService;

import java.util.Date;
import java.util.List;

/**
 * Created by IuliaP on 29.09.2017.
 */
@Service
public class NotificationScheduleService extends BaseEntityService<NotificationSchedule> {

    @Autowired
    NotificationScheduleService(NotificationScheduleRepository notificationScheduleRepository) {

        super(NotificationSchedule.class, notificationScheduleRepository);
    }

    public List<Long> getAllNotificationScheduleToSend(Date date) {

        return ((NotificationScheduleRepository) getResourceRepository()).getAllNotificationScheduleToSend(date);
    }

    public void deleteUnsentNotificationScheduleByTypeAndUserAndEntityId(UserMessageType userMessageType, Long userId,
                                                                         Long entityId) {

        ((NotificationScheduleRepository) getResourceRepository())
                .deleteUnsentNotificationScheduleByTypeAndUserAndEntityId(userMessageType, userId, entityId);
    }

    public List<NotificationSchedule> findAllByPlannedNotification(PlannedNotification plannedNotification) {

        return ((NotificationScheduleRepository) getResourceRepository())
                .findAllByPlannedNotification(plannedNotification);
    }

    public void deleteAllByPlannedNotification(Long plannedNotificationId) {
        ((NotificationScheduleRepository)getResourceRepository()).deleteNotificationScheduleByPlannedNotification( plannedNotificationId );
    }

    public void createNotificationSchedule(PlannedNotification plannedNotification) {

        if (plannedNotification != null) {

        }
    }

    public Long countAllSentByPlannedNotification(Long plannedNotificationId) {
        return ((NotificationScheduleRepository)getResourceRepository()).countAllSentByPlannedNotification( plannedNotificationId );
    }
}
