package ro.siveco.ael.lcms.domain.materials.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.hibernate.annotations.Cascade;
import ro.siveco.ael.lcms.domain.auth.model.AelUser;
import ro.siveco.ael.lcms.domain.auth.model.User;
import ro.siveco.ram.starter.ui.config.PropertyViewDescriptor;
import ro.siveco.ram.starter.ui.config.ViewDescriptor;
import ro.siveco.ram.starter.ui.model.FormInputType;
import ro.siveco.ram.starter.ui.model.GridSize;
import ro.siveco.ram.starter.ui.model.IndexViewType;
import ro.siveco.ram.starter.ui.model.ViewRendererType;

import javax.persistence.*;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: razvanni
 * Date: 03.06.2010
 * Time: 20:55:57
 * To change this template use File | Settings | File Templates.
 */

@SuppressWarnings("JpaDataSourceORMInspection")
@Entity
@Table( name = "survey" )
@SequenceGenerator( name = "SURVEY_SEQ", sequenceName = "survey_seq" )
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
@ViewDescriptor(pageSize = 10, searchEnabled = true,
		defaultSearchProperty="definition", cardSizeLarge = GridSize.THREE_PER_ROW,
        cardSizeMedium = GridSize.THREE_PER_ROW,
        viewTypes = {IndexViewType.LIST, IndexViewType.CARD})
public class Survey extends ro.siveco.ram.starter.model.base.BaseEntity
{
	private String definition;

	private List<SurveyItem> surveyItems;

	private Boolean isPublic;

    private Boolean allowMultipleAnswers;

    private Boolean anonymous;

	private Long order = 1L;

    private User creator;

	private String learningMaterialNodeId;

	@Id
	@GeneratedValue( generator = "SURVEY_SEQ", strategy = GenerationType.AUTO )
    @PropertyViewDescriptor(hiddenOnCard = true, hiddenOnList = true, hiddenOnForm = true, hiddenOnPresentation = true)
    public Long getId() {

        return super.getId();
    }

	@Column( name = "DEFINITION" )
	@PropertyViewDescriptor(inputType = FormInputType.TEXT, rendererType = ViewRendererType.STRING,
            editInline = false, order = 1, required = true, displayLabelInPresentation = false)  //hiddenOnList = true, hiddenOnCard = true
    public String getDefinition() {

        return definition;
    }

	public void setDefinition( String definition )
	{
		this.definition = definition;
	}

	@Column( name = "ITEM_ORDER" )
	@PropertyViewDescriptor(inputType = FormInputType.NUMBER, rendererType = ViewRendererType.INTEGER,
			editInline = false, hidden = true, required = false, hiddenOnForm = true)
	public Long getOrder()
	{
		return order;
	}

	public void setOrder( Long order )
	{
		this.order = order;
	}

	@Column( name = "IS_PUBLIC" )
    @PropertyViewDescriptor(inputType = FormInputType.SWITCH, rendererType = ViewRendererType.CHIP,
            displayLabelInPresentation = false,
            editInline = false, order = 4, required = false)
    public Boolean getIsPublic() {

        return isPublic;
    }

	public void setIsPublic( Boolean aPublic )
	{
		isPublic = aPublic;
	}

    @Column( name = "ALLOW_MULTIPLE_ANSWERS" )
    @PropertyViewDescriptor(inputType = FormInputType.SWITCH, rendererType = ViewRendererType.CHIP,
            displayLabelInPresentation = false,
            editInline = false, order = 5, required = false)
    public Boolean getAllowMultipleAnswers() {
        return allowMultipleAnswers;
    }

    public void setAllowMultipleAnswers(Boolean allowMultipleAnswers)
    {
        this.allowMultipleAnswers = allowMultipleAnswers;
    }

    @Column( name = "anonymous" )
    @PropertyViewDescriptor(inputType = FormInputType.SWITCH, rendererType = ViewRendererType.CHIP,
            displayLabelInPresentation = false,
            editInline = false, order = 6, required = false)
    public Boolean getAnonymous()
    {
        return anonymous;
    }

    public void setAnonymous(Boolean anonymous)
    {
        this.anonymous = anonymous;
    }

	@ManyToOne(targetEntity = AelUser.class)
    @PropertyViewDescriptor(inputType = FormInputType.SELECT, rendererType = ViewRendererType.MODEL,
            displayLabelInPresentation = false, hiddenOnForm = true,
            editInline = false, order = 3, required = false)
    public User getCreator() {

        return creator;
    }

	public void setCreator( User creator )
	{
		this.creator = creator;
	}

	@OneToMany( mappedBy = "survey", cascade = {CascadeType.ALL} )
	@Cascade( {org.hibernate.annotations.CascadeType.DELETE_ORPHAN} )
    @OrderBy("order ASC")
    @PropertyViewDescriptor(order = 7, rendererType = ViewRendererType.VALUE_OBJECT_MODEL_COLLECTION,
            hiddenOnGrid = true, hiddenOnCard = true, hiddenOnList = true)
    public List<SurveyItem> getSurveyItems() {

        return surveyItems;
    }

	public void setSurveyItems( List<SurveyItem> surveyItems )
	{
		this.surveyItems = surveyItems;
	}

	@Column( name = "learning_material_node_id", length = 500 )
	@PropertyViewDescriptor(inputType = FormInputType.TEXT, rendererType = ViewRendererType.STRING,
			editInline = false, hidden = true, required = false, hiddenOnForm = true)
	public String getLearningMaterialNodeId() {
		return learningMaterialNodeId;
	}

	public void setLearningMaterialNodeId(String learningMaterialNodeId) {
		this.learningMaterialNodeId = learningMaterialNodeId;
	}

    @Override
    @Transient
    @PropertyViewDescriptor(
           hidden = true)
    public String getObjectLabel() {

        return getDefinition();
    }
}
