package ro.siveco.ael.lcms.domain.tenant.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.auth.model.User;
import ro.siveco.ael.lcms.domain.tenant.model.Ip;
import ro.siveco.ael.lcms.domain.tenant.model.Tenant;
import ro.siveco.ael.lcms.domain.tenant.service.IpService;
import ro.siveco.ael.web.controllers.WithUserBaseEntityController;
import ro.siveco.ram.event.listener.type.single.BaseBeforeCreateListener;
import ro.siveco.ram.event.type.single.BeforeCreateEvent;

/**
 * Created by AndradaC on 8/31/2017.
 */
@Component
public class SetTenantForIpBeforeCreateListener extends BaseBeforeCreateListener<Ip>{

    @Autowired
    private IpService ipService;

    @Override
    public void onApplicationEvent(BeforeCreateEvent<Ip> event) {
        Ip ip = event.getDomainModel();
        User loggedUser = WithUserBaseEntityController.getUser();
        Tenant tenant = loggedUser.getTenant();

        ipService.checkIpValidation(ip);
        ipService.checkIfIpExists(ip.getValue(), tenant);
        ip.setTenant(tenant);
    }
}
