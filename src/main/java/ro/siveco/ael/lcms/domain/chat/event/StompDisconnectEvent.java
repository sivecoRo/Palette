package ro.siveco.ael.lcms.domain.chat.event;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;
import ro.siveco.ael.lcms.domain.chat.web.ChatController;


@Component
public class StompDisconnectEvent implements ApplicationListener<SessionDisconnectEvent> {
    
	private ChatController chatController;
    
    
    @Autowired
	public StompDisconnectEvent(ChatController chatController) {
		this.chatController = chatController;
    }
    
    
	@Override
	public void onApplicationEvent(SessionDisconnectEvent event) {
		StompHeaderAccessor sha = StompHeaderAccessor.wrap(event.getMessage());
        
        if ( sha.getUser()==null ) {
        	return;
        }
        else {
        	chatController.sendUsersList(sha.getUser().getName(), true);
        }
	}

}
