package ro.siveco.ael.lcms.domain.profile.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.auth.model.AelUser;
import ro.siveco.ael.lcms.domain.profile.model.UserDetails;
import ro.siveco.ael.lcms.domain.profile.service.UserDetailsService;
import ro.siveco.ael.service.ram.SecurityService;
import ro.siveco.ram.app.exception.AppException;
import ro.siveco.ram.event.listener.type.single.BaseBeforeUpdateListener;
import ro.siveco.ram.event.type.single.BeforeUpdateEvent;

@Component
public class UserDetailsBeforeUpdateListener extends BaseBeforeUpdateListener<UserDetails> {

    private final UserDetailsService userDetailsService;

    private final SecurityService securityService;

    @Override
    public void onApplicationEvent(BeforeUpdateEvent<UserDetails> event) {
        AelUser currentUser = securityService.getCurrentAuthenticatedUser();

        if (event.getDomainModel().getUser().getId() == null || !event.getDomainModel().getUser().getId().equals(currentUser.getId())) {
            throw new AppException("error.userDetail.accees.denied");
        }
    }

    @Autowired
    public UserDetailsBeforeUpdateListener(UserDetailsService userDetailsService, SecurityService securityService) {
        this.userDetailsService = userDetailsService;
        this.securityService = securityService;
    }
}
