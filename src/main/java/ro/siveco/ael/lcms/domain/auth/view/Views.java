package ro.siveco.ael.lcms.domain.auth.view;

public class Views {

    public static class ImportUserView extends ro.siveco.ram.starter.ui.model.Views.View {

    }

    public static class ForgotPasswordView extends ro.siveco.ram.starter.ui.model.Views.View {

    }

    public static class AssociateFunctionsToRoleView extends ro.siveco.ram.starter.ui.model.Views.View {

    }

    public static class AssociateRolesToGroupView extends ro.siveco.ram.starter.ui.model.Views.View {

    }

    public static class AssociateGroupsToUserView extends ro.siveco.ram.starter.ui.model.Views.View {

    }

}
