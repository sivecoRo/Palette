package ro.siveco.ael.lcms.domain.metadata.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import ro.siveco.ael.lcms.domain.metadata.model.Comment;
import ro.siveco.ael.lcms.domain.metadata.service.CommentService;
import ro.siveco.ael.web.controllers.WithUserBaseEntityController;

/**
 * Created by albertb on 2/23/2018.
 */

@Controller
@RequestMapping("/sessions/{sessionId}")
public class CommentController extends WithUserBaseEntityController<Comment> {
    @Autowired
    public CommentController(CommentService restService) {
        super(restService);
    }
}
