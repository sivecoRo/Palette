package ro.siveco.ael.lcms.domain.auth.listener;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.auth.model.AelUser;
import ro.siveco.ael.lcms.domain.auth.model.UserCompetence;
import ro.siveco.ael.web.controllers.WithUserBaseEntityController;
import ro.siveco.ram.event.listener.type.single.BaseBeforeCreateListener;
import ro.siveco.ram.event.type.single.BeforeCreateEvent;

/**
 * Created by AndradaC on 9/3/2017.
 */
@Order(value = 1)
@Component
public class SetUserForUserCompetenceBeforeCreateListener extends BaseBeforeCreateListener<UserCompetence>{

    @Override
    public void onApplicationEvent(BeforeCreateEvent<UserCompetence> event) {
        UserCompetence userCompetence = event.getDomainModel();
        userCompetence.setUser((AelUser) WithUserBaseEntityController.getUser());
    }
}
