package ro.siveco.ael.lcms.domain.profile.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.siveco.ael.lcms.domain.auth.model.AelUser;
import ro.siveco.ael.lcms.domain.auth.service.AelUserService;
import ro.siveco.ael.lcms.domain.auth.service.ProfileVerificationService;
import ro.siveco.ael.lcms.domain.course.service.CourseService;
import ro.siveco.ael.lcms.domain.course.service.PlannedCourseService;
import ro.siveco.ram.starter.service.BaseService;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Service for deleting profiles/accounts created by users.
 * <p>
 * In fact, it consists of 1 method, where 4 methods from different services are called.
 * As there is possibility that any of the four operations may fail, they are all encapsulated with try-catch
 * statements. For either of the failures, there is an appropriate error logged.
 */
@Service
public class DeleteProfileService extends BaseService {

    private AelUserService aelUserService;
    private ProfileVerificationService profileVerificationService;
    private CourseService courseService;
    private PlannedCourseService plannedCourseService;
    private UserDetailsService userDetailsService;


    @Autowired
    public DeleteProfileService(AelUserService aelUserService, ProfileVerificationService profileVerificationService,
                                CourseService courseService, PlannedCourseService plannedCourseService,
                                UserDetailsService userDetailsService) {

        this.aelUserService = aelUserService;
        this.profileVerificationService = profileVerificationService;
        this.courseService = courseService;
        this.plannedCourseService = plannedCourseService;
        this.userDetailsService = userDetailsService;
    }


    @Transactional
    public void deleteProfile(AelUser aelUser) {

        // Perform profile anonymization (personal information is deleted/overwritten).
        userDetailsService.anonymizeProfile(aelUser);

        // First subpart of deleting items (courses) created by the user (as there are two tables where information
        // regarding items is stored).
        // aelUser has to be passed to plannedCourses in order to delete all courses created by this user and
        // comments that this user posted elsewhere (under other people's items).
        plannedCourseService.removePlannedCoursesAndComments(aelUser);

        // Now, as there are no constraints, delete Courses from the second table (named "courses").
        courseService.removeCoursesByUser(aelUser);

        // Delete profile verification as it is no longer required.
        profileVerificationService.deleteProfileVerification(aelUser);
    }
}
