package ro.siveco.ael.lcms.domain.notification.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.notification.model.PlannedNotification;
import ro.siveco.ael.lcms.domain.notification.service.PlannedNotificationService;
import ro.siveco.ram.event.listener.type.single.BaseAfterUpdateListener;
import ro.siveco.ram.event.type.single.AfterUpdateEvent;

/**
 * Created by IuliaP on 29.09.2017.
 */
@Order(value = 1)
@Component
public class SetPropertiesForPlannedNotificationAfterUpdateListener
        extends BaseAfterUpdateListener<PlannedNotification> {

    @Autowired
    PlannedNotificationService plannedNotificationService;

    @Override
    public void onApplicationEvent(AfterUpdateEvent<PlannedNotification> event) {
        PlannedNotification plannedNotification = event.getDomainModel();
        plannedNotificationService.deleteNotificationSchedule( plannedNotification.getId() );
        plannedNotificationService.createNotificationSchedule( plannedNotification );
    }
}
