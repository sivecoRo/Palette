package ro.siveco.ael.lcms.domain.gamification.model;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by LucianR on 30.10.2017.
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Point {

    private String name;
    private Integer score;

    public String getName() {

        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

    public Integer getScore() {

        return score;
    }

    public void setScore(Integer score) {

        this.score = score;
    }
}
