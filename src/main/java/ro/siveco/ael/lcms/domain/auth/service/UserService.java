package ro.siveco.ael.lcms.domain.auth.service;

import org.springframework.social.connect.UserProfile;
import ro.siveco.ael.lcms.domain.auth.model.AelUser;
import ro.siveco.ael.lcms.domain.auth.model.User;

import java.util.Optional;

/**
 * User: AlexandruVi
 * Date: 2017-02-20
 */
public interface UserService {

    Optional<User> getUserByUsername(String username);

    User getByRelatedUid(String relatedUid);

    User findOneByEmail(String email);

    AelUser saveOrUpdate(AelUser user);

    User findOneByUserSocialId(String userSocialId);

    User createSocialUser(String userSocialId, UserProfile profile);

    AelUser createComponentUserJobForUser(AelUser user);

}
