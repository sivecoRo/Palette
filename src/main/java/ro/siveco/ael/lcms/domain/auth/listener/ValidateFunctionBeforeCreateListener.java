package ro.siveco.ael.lcms.domain.auth.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.auth.model.AelFunction;
import ro.siveco.ael.lcms.domain.auth.service.AelFunctionService;
import ro.siveco.ram.app.exception.AppException;
import ro.siveco.ram.event.listener.type.single.BaseBeforeCreateListener;
import ro.siveco.ram.event.type.single.BeforeCreateEvent;

/**
 * Created by AndradaC on 9/1/2017.
 */
@Component
public class ValidateFunctionBeforeCreateListener extends BaseBeforeCreateListener<AelFunction>{

    @Autowired
    private AelFunctionService aelFunctionService;

    @Override
    public void onApplicationEvent(BeforeCreateEvent<AelFunction> event) {
        AelFunction aelFunction = event.getDomainModel();
        if (!aelFunctionService.validateAelFunction(aelFunction)) {
            throw new AppException("error.aelFunction.create.duplicateEntry");
        }

    }
}
