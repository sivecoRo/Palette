package ro.siveco.ael.lcms.domain.notification.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.social.security.SocialAuthenticationToken;
import org.springframework.stereotype.Service;
import ro.siveco.ael.facebook.model.SocialAuthenticatedUser;
import ro.siveco.ael.lcms.domain.auth.model.AelUser;
import ro.siveco.ael.lcms.domain.auth.model.AuthenticatedUser;
import ro.siveco.ael.lcms.domain.auth.model.User;
import ro.siveco.ael.lcms.domain.course.model.PlannedCourse;
import ro.siveco.ael.lcms.domain.course.model.PlannedCourseStudent;
import ro.siveco.ael.lcms.domain.course.service.PlannedCourseService;
import ro.siveco.ael.lcms.domain.course.service.PlannedCourseStudentService;
import ro.siveco.ael.lcms.domain.notification.model.*;
import ro.siveco.ael.lcms.repository.PlannedNotificationRepository;
import ro.siveco.ram.app.exception.AppException;
import ro.siveco.ram.starter.service.BaseEntityService;

import java.security.Principal;
import java.util.Date;
import java.util.List;

/**
 * Created by IuliaP on 29.09.2017.
 */
@Service
public class PlannedNotificationService extends BaseEntityService<PlannedNotification> {

    @Autowired
    NotificationScheduleService notificationScheduleService;

    @Autowired
    PlannedCourseStudentService plannedCourseStudentService;

    @Autowired
    NotificationManagerService notificationManagerService;

    @Autowired
    UserMessageFormatService userMessageFormatService;

    @Autowired
    PlannedCourseService plannedCourseService;

    @Autowired
    public PlannedNotificationService(PlannedNotificationRepository plannedNotificationRepository) {

        super(PlannedNotification.class, plannedNotificationRepository);
    }

    public void createNotificationSchedule(PlannedNotification plannedNotification) {

        if (plannedNotification != null) {
            if (NotificationEntityType.PLANNED_COURSE.equals(plannedNotification.getNotificationEntityType())) {
                List<PlannedCourseStudent> plannedCourseStudentList =
                        plannedCourseStudentService.findByPlannedCourseId(plannedNotification.getEntityId());
                if (plannedCourseStudentList != null) {
                    for (PlannedCourseStudent plannedCourseStudent : plannedCourseStudentList) {
                        //TODO - adauga validator sa vezi daca trebuie trimisa notificarea sau nu
                            Date sendDate = plannedNotification.getSendDate();
                        notificationManagerService
                                .notifyUser((AelUser) getCurrentUser(), plannedCourseStudent.getUser(), null,
                                        new Object[]{getCurrentUser().getDisplayName(),
                                                plannedCourseStudent.getPlannedCourse().getCourse().getName(),
                                                sendDate},
                                        UserMessageType.COURSE_RELATED_MESSAGE,
                                        plannedNotification.getUserMessageFormat(),
                                        NotificationType.MESSAGE_AND_MAIL, null, null, sendDate, plannedNotification);
                    }
                }
            }
        }
    }

    public void deleteNotificationSchedule(Long plannedNotificationId) {
        notificationScheduleService.deleteAllByPlannedNotification( plannedNotificationId );
    }

    public void validateBeforeCurrentDate(PlannedNotification plannedNotification, String errorMessage) {
        if( plannedNotification != null && plannedNotification.getSendDate() != null && plannedNotification.getSendDate().before( new Date() )) {
            throw new AppException(errorMessage);
        }
    }

    public void validateNotNullDate(PlannedNotification plannedNotification, String errorMessage) {
        if( plannedNotification != null && !plannedNotification.getSendByEvent() && plannedNotification.getSendDate() == null ) {
            throw new AppException(errorMessage);
        }
    }

    public void validateNotificationSchedule(PlannedNotification plannedNotification, String errorMessage) {
        if( plannedNotification != null ) {
            Long count = notificationScheduleService.countAllSentByPlannedNotification(plannedNotification.getId());
            if( count > 0 ) {
                throw new AppException(errorMessage);
            }
        }
    }

    public void validateForm(PlannedNotification plannedNotification) {
        if( plannedNotification != null ) {
            if( plannedNotification.getUserMessageFormat() == null ) {
                throw new AppException("error.plannedNotification.userMessageFormat.null");
            }
            if( plannedNotification.getNotificationEntityType().equals( NotificationEntityType.PLANNED_COURSE ) && plannedNotification.getPlannedCourse() == null ) {
                throw new AppException("error.plannedNotification.plannedCourse.null");
            }
            if( !plannedNotification.getSendByEvent() && plannedNotification.getSendDate() == null ) {
                throw new AppException("error.plannedNotification.date.null");
            }
        }
    }
    public void setDate(PlannedNotification plannedNotification) {
        if( plannedNotification != null && plannedNotification.getSendByEvent() ) {
            if( plannedNotification.getNotificationEventType() == null ) {
                throw new AppException("error.plannedNotification.event.null");
            }
            plannedNotification.setSendDate(getDateForEvent(plannedNotification));
        }
    }

    private User getCurrentUser() {

        Principal principal = SecurityContextHolder.getContext().getAuthentication();

        if (principal instanceof SocialAuthenticationToken) {
            return ((SocialAuthenticatedUser) ((SocialAuthenticationToken) principal).getPrincipal()).getUser();
        }

        return ((AuthenticatedUser) ((AbstractAuthenticationToken) principal).getPrincipal()).getUser();
    }

    public Date getDateForEvent(PlannedNotification plannedNotification) {
        Date computedDate = null;
        Integer minutes = 0;
        if( plannedNotification != null ) {
            if( plannedNotification.getSendByEvent() ) {
                PlannedCourse plannedCourse = plannedCourseService.findOne( plannedNotification.getPlannedCourse().getId() );
                Integer daysNo = plannedNotification.getDaysNo();
                Integer hoursNo = plannedNotification.getHoursNo();
                Integer minutesNo = plannedNotification.getMinutesNo();
                if( daysNo != null ) {
                    minutes += daysNo * 1440;
                }
                if( hoursNo != null ) {
                    minutes += hoursNo * 60;
                }
                if( minutesNo != null ) {
                    minutes += minutesNo;
                }
                long millis = minutes * 60 * 1000;
                if( plannedNotification.getNotificationEventType().equals( NotificationEventType.BEFORE_PLANNED_COURSE ) ) {
                    Date plannedCourseStartDate = plannedCourse.getStartDate();
                    return new Date( plannedCourseStartDate.getTime() - millis);
                } else if( plannedNotification.getNotificationEventType().equals( NotificationEventType.ON_PLANNED_COURSE ) ) {
                    Date plannedCourseStartDate = plannedCourse.getStartDate();
                    return new Date( plannedCourseStartDate.getTime() + millis );
                } else if( plannedNotification.getNotificationEventType().equals( NotificationEventType.AFTER_PLANNED_COURSE ) ) {
                    Date plannedCourseEndDate = plannedCourse.getEndDate() == null ? new Date() : plannedCourse.getEndDate();
                    return new Date( plannedCourseEndDate.getTime() + millis);
                }
            }
        }
        return computedDate;
    }

    public void updateNotifications(PlannedCourse plannedCourse) {
        if( plannedCourse != null ) {
            List<PlannedNotification> plannedNotifications = ((PlannedNotificationRepository)getResourceRepository())
                    .findAllByEntityIdAndSendByEvent( plannedCourse.getId(), Boolean.TRUE);
            for( PlannedNotification plannedNotification : plannedNotifications ) {
                List<NotificationSchedule> notificationSchedules = notificationScheduleService.findAllByPlannedNotification( plannedNotification );
                int count = 0;
                for( NotificationSchedule notificationSchedule : notificationSchedules ) {
                    if( notificationSchedule.getStatus() ) {
                        count++;
                        break;
                    }
                }
                if( count == 0 ) {
                    plannedNotification.setSendDate( getDateForEvent( plannedNotification ));
                }
            }
        }
    }
}

