package ro.siveco.ael.lcms.domain.taxonomies.model;

import ro.siveco.ael.lcms.domain.tenant.model.Tenant;
import ro.siveco.ael.lcms.domain.tenant.model.TenantHolder;
import ro.siveco.ram.starter.ui.config.PropertyViewDescriptor;
import ro.siveco.ram.starter.ui.config.ViewDescriptor;
import ro.siveco.ram.starter.ui.model.FormInputType;
import ro.siveco.ram.starter.ui.model.IndexViewType;
import ro.siveco.ram.starter.ui.model.ViewRendererType;

import javax.persistence.*;

/**
 * Created by JohnB on 22/06/2017.
 */
@Entity
@Table( name = "taxonomy_levels" )
@SequenceGenerator( name = "public.taxonomy_levels_seq", sequenceName = "taxonomy_levels_seq", allocationSize = 1)
@ViewDescriptor(paginationEnabled = false, viewTypes = {IndexViewType.LIST}, defaultViewType = IndexViewType.LIST,
        searchEnabled = true, defaultSearchProperty = "name")
public class TaxonomyLevel extends ro.siveco.ram.starter.model.base.BaseEntity implements TenantHolder{
    private String name;
    private String description;
    private Integer orderNumber;
    private Tenant tenant;

    @Override
    @Id
    @GeneratedValue(generator = "taxonomy_levels_seq", strategy = GenerationType.SEQUENCE)
    public Long getId() {
        return super.getId();
    }

    @Column(name = "name")
    @PropertyViewDescriptor(inputType = FormInputType.TEXT, rendererType = ViewRendererType.STRING, required=true)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "description")
    @PropertyViewDescriptor(inputType = FormInputType.TEXT, rendererType = ViewRendererType.STRING, required=true, hiddenOnList = true)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "order_number")
    @PropertyViewDescriptor(inputType = FormInputType.NUMBER, rendererType = ViewRendererType.INTEGER, required=true, hiddenOnList = true)
    public Integer getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(Integer orderNumber) {
        this.orderNumber = orderNumber;
    }

    @ManyToOne
    @JoinColumn(name = "tenant_id", nullable = true )
    @PropertyViewDescriptor(inputType = FormInputType.SELECT, rendererType = ViewRendererType.MODEL,
            editInline = false, hidden = true, required = false, hiddenOnForm = true)
    public Tenant getTenant() {
        return tenant;
    }

    public void setTenant(Tenant tenant) {
        this.tenant = tenant;
    }

    @Override
    public String toString() {
        return getName();
    }

    @Override
    @Transient
    @PropertyViewDescriptor(hiddenOnList = true)
    public String getObjectLabel() {
        return getName();
    }
}