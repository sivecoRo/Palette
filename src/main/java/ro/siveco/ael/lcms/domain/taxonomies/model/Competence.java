package ro.siveco.ael.lcms.domain.taxonomies.model;

import ro.siveco.ael.lcms.domain.tenant.model.Tenant;
import ro.siveco.ael.lcms.domain.tenant.model.TenantHolder;
import ro.siveco.ram.starter.model.base.BaseEntity;
import ro.siveco.ram.starter.ui.config.ActionDescriptor;
import ro.siveco.ram.starter.ui.config.PropertyViewDescriptor;
import ro.siveco.ram.starter.ui.config.ViewDescriptor;
import ro.siveco.ram.starter.ui.model.*;

import javax.persistence.*;

/**
 * Created by JohnB on 16/06/2017.
 */
@Entity
@Table( name = "competences" )
@SequenceGenerator( name = "public.competences_seq", sequenceName = "competences_seq", allocationSize = 1)
@ViewDescriptor(paginationEnabled = false, viewTypes = {IndexViewType.TREE}, defaultViewType = IndexViewType.TREE,
        searchEnabled = true, defaultSearchProperty = "label",
        defaultActions = {
                @ActionDescriptor(
                        type = ActionType.DELETE,
                        requiresConfirmation = true,
                        inputType = FormInputType.ANCHOR
                ),
                @ActionDescriptor(type = ActionType.OPEN_EDIT_FORM,
                        propertyName = "parent",
                        openInModal = true
                ),
                @ActionDescriptor(type = ActionType.OPEN_CREATE_FORM,
                        propertyName = "parent",
                        openInModal = true
                ),
                @ActionDescriptor(type = ActionType.CREATE, forwardFor = "{modelName}-OPEN_CREATE_FORM", availableOnlyInForm = true),
                @ActionDescriptor(type = ActionType.UPDATE, forwardFor = "{modelName}-OPEN_EDIT_FORM", availableOnlyInForm = true),
                @ActionDescriptor(type = ActionType.LIST, availableInForm = true, backToFor = {
                        "{modelName}-OPEN_CREATE_FORM",
                        "{modelName}-CREATE",
                        "{modelName}-OPEN_PRESENTATION",
                        "{modelName}-OPEN_EDIT_FORM",
                        "{modelName}-UPDATE",
                        "{modelName}-DELETE"
                })
        },
        otherActions = {
                @ActionDescriptor(actionId = "openCreateFormForCompetenceActionId", type = ActionType.CUSTOM, fixedAction = true,
                        uri = "/competences/create", openInModal = true, ajax = false)
        }
)
public class Competence extends BaseEntity implements TenantHolder, Taxonomy<Competence>{

    private Competence parent;
    private Tenant tenant;
    private String label;
    private String type;

    @Override
    @Id
    @GeneratedValue(generator = "competences_seq", strategy = GenerationType.SEQUENCE)
    public Long getId() {
        return super.getId();
    }

    @Override
    @ManyToOne
    @JoinColumn(name = "parent_id", nullable = true)
    @PropertyViewDescriptor(hidden = true)
    public Competence getParent() {
        return parent;
    }

    public void setParent(Competence parent) {
        this.parent = parent;
    }

    @ManyToOne
    @JoinColumn(name = "tenant_id", nullable = true )
    @PropertyViewDescriptor(inputType = FormInputType.SELECT, rendererType = ViewRendererType.MODEL,
            editInline = false, hidden = true, required = false, hiddenOnForm = true)
    public Tenant getTenant() {
        return tenant;
    }

    public void setTenant(Tenant tenant) {
        this.tenant = tenant;
    }

    @Column(name = "label")
    @PropertyViewDescriptor(inputType = FormInputType.TEXT,
            rendererType = ViewRendererType.STRING,
            displayLabelInPresentation = false,
            cssClass = CommonConstants.GENERIC_PROPERTY_CLASS + " collapsible-header"
    )
    public String getLabel() { return label; }

    public void setLabel(String label) { this.label = label; }

    @Column(name = "type")
    @PropertyViewDescriptor(inputType = FormInputType.TEXT,
            rendererType = ViewRendererType.STRING,
            hiddenOnList = true,
            displayLabelInPresentation = false
    )
    public String getType() { return type; }

    public void setType(String type) { this.type = type; }

    @Override
    public String toString() {
        return getLabel();
    }

    @Override
    @Transient
    @PropertyViewDescriptor( hidden = true )
    public String getObjectLabel() {
        return getLabel();
    }
}