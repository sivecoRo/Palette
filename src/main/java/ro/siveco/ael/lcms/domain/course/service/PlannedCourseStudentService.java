package ro.siveco.ael.lcms.domain.course.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.siveco.ael.lcms.domain.auth.model.AelUser;
import ro.siveco.ael.lcms.domain.auth.model.User;
import ro.siveco.ael.lcms.domain.course.model.*;
import ro.siveco.ael.lcms.repository.PlannedCourseStudentRepository;
import ro.siveco.ram.starter.service.BaseEntityService;

import java.util.Date;
import java.util.List;

@Service
public class PlannedCourseStudentService extends BaseEntityService<PlannedCourseStudent> {

    @Autowired
    public PlannedCourseStudentService(PlannedCourseStudentRepository resourceRepository) {
        super(PlannedCourseStudent.class, resourceRepository);
    }

    public PlannedCourseStudent saveOrUpdate(PlannedCourseStudent plannedCourseStudent)
    {
        if(plannedCourseStudent.getId() == null)
        {
            return save(plannedCourseStudent);
        }
        else
        {
            return update(plannedCourseStudent);
        }
    }

    public PlannedCourseStudent saveNewRunningPeriod(PlannedCourseStudent plannedCourseStudent, Date periodStartDate, Date periodEndDate)
    {
        plannedCourseStudent.setStartDate(periodStartDate);
        plannedCourseStudent.setEndDate(periodEndDate);
        update(plannedCourseStudent);

        return plannedCourseStudent;
    }

    public Boolean saveNewRunningPeriodForAllUsers(Long plannedCourseId, Date periodStartDate, Date periodEndDate)
    {
        PlannedCourseStudentRepository plannedCourseStudentRepository = (PlannedCourseStudentRepository)getResourceRepository();
        List<PlannedCourseStudent> plannedCourseStudents = plannedCourseStudentRepository.findByPlannedCourseId(plannedCourseId);
        for(PlannedCourseStudent pcs: plannedCourseStudents)
        {
            pcs.setStartDate(periodStartDate);
            pcs.setEndDate(periodEndDate);
        }
        update(plannedCourseStudents);

        return true;
    }

    public PlannedCourseStudent createPlannedCourseStudent(User user, PlannedCourse plannedCourse,
                                                           Boolean  addItemToFavorite, Boolean removeItem,
                                                           Boolean likeItem, Boolean dislikeItem, Boolean attending)
    {
        PlannedCourseStudent newStudent = new PlannedCourseStudent();
        newStudent.setPlannedCourse(plannedCourse);
        newStudent.setUser((AelUser) user);
        newStudent.setStartDate(plannedCourse.getStartDate());
        newStudent.setEndDate(plannedCourse.getEndDate());
        newStudent.setAddedToFavorites(addItemToFavorite);
        newStudent.setRemoveItem(removeItem);
        newStudent.setLikeItem(likeItem);
        newStudent.setDislikeItem(dislikeItem);
        newStudent.setAttending(attending);

        if (plannedCourse.getRegistrationApprovalUser() != null)
        {
            newStudent.setApproved(Boolean.FALSE);
        } else {
            newStudent.setApproved(Boolean.TRUE);
        }
        newStudent.setAllowedByTeacher(Boolean.FALSE);
        
        if (PlannedCourseStatus.RUNNING.equals(plannedCourse.getStatus()))
        {
            newStudent.setApprovedAfterCourseStarted(true);
        } else {
            newStudent.setApprovedAfterCourseStarted(false);
        }
        newStudent = saveOrUpdate(newStudent);

        return newStudent;
    }

    public Long getTotalStudentCoursesNumber(User user)
    {
        PlannedCourseStudentRepository plannedCourseStudentRepository = (PlannedCourseStudentRepository)getResourceRepository();
        return plannedCourseStudentRepository.getTotalCourseNumberForStudent(user.getId());
    }

    public Long getEndedStudentCoursesNumber(User user, Date currentDate)
    {
        PlannedCourseStudentRepository plannedCourseStudentRepository = (PlannedCourseStudentRepository)getResourceRepository();
        return plannedCourseStudentRepository.getEndedCoursesNumberForStudent(user.getId(), currentDate);
    }

    public Long getFutureStudentCoursesNumber(User user, Date currentDate)
    {
        PlannedCourseStudentRepository plannedCourseStudentRepository = (PlannedCourseStudentRepository)getResourceRepository();
        return plannedCourseStudentRepository.getFutureCoursesNumberForStudent(user.getId(), currentDate);
    }

    public Long getOpenStudentCoursesNumber(User user, Date currentDate)
    {
        PlannedCourseStudentRepository plannedCourseStudentRepository = (PlannedCourseStudentRepository)getResourceRepository();
        return plannedCourseStudentRepository.getOpenCoursesNumberForStudent(user.getId(), currentDate);
    }

    public PlannedCourseStudent findOneById(Long plannedCourseStudentId)
    {
       return ((PlannedCourseStudentRepository)getResourceRepository()).findOneById(plannedCourseStudentId);
    }

    public List<PlannedCourseStudent> findByPlannedCourseId(Long plannedCourseId)
    {
        return ((PlannedCourseStudentRepository)getResourceRepository()).findByPlannedCourseId(plannedCourseId);
    }

    public PlannedCourseStudent findByPlanificationAndStundent(Long plannedCourseId, Long studentId)
    {
        return ((PlannedCourseStudentRepository)getResourceRepository()).findByPlanificationAndStundent(plannedCourseId,
                studentId);
    }

    public void removePlannedCourseStudent(Long plannedCourseStudentId){
        remove(plannedCourseStudentId);
    }

    public Long getTotalCourseStudentsNumber(PlannedCourse session) {
        return ((PlannedCourseStudentRepository)getResourceRepository()).getTotalCourseStudentsNumber(session.getId());
	}

	public Integer getStudentsNumberForCourse(Course course) {

        return ((PlannedCourseStudentRepository) getResourceRepository()).getStudentsNumberForCourse(course.getId());
    }

    public Integer getFavoritesStudentsNumberForCourse(Course course) {

        return ((PlannedCourseStudentRepository) getResourceRepository()).getFavoritesStudentsNumberForCourse(course.getId());
    }

    public Integer getItemLikesNumber(Course course)
    {
        return ((PlannedCourseStudentRepository) getResourceRepository()).getItemLikesNumber(course.getId());
    }

    public Integer getItemDislikesNumber(Course course)
    {
        return ((PlannedCourseStudentRepository) getResourceRepository()).getItemDislikesNumber(course.getId());
    }

    public PlannedCourseStudent findPlannedCourseStudentByPlannedCourseAndUserAndRemoveItem(PlannedCourse plannedCourse, AelUser user, Boolean removeItem) {
        return ((PlannedCourseStudentRepository)getResourceRepository()).findPlannedCourseStudentByPlannedCourseAndUserAndRemoveItem(plannedCourse, user, removeItem);
    }

    public Integer getAttendingStudentsNumberForCourse(Course course) {

        return ((PlannedCourseStudentRepository) getResourceRepository()).getAttendingStudentsNumberForCourse(course.getId());
    }

    public List<AelUser> getAttendingStudentsForCourse(Long courseId) {
        return ((PlannedCourseStudentRepository)getResourceRepository()).getAttendingStudentsForCourse(courseId);
    }

    public List<PlannedCourseStudent> getAttendingPCStudentsForCourse(Long courseId) {
        return ((PlannedCourseStudentRepository)getResourceRepository()).getAttendingPCStudentsForCourse(courseId);
    }
}
