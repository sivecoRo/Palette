package ro.siveco.ael.lcms.domain.course.service;

import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.DateTimePath;
import com.querydsl.core.types.dsl.EnumPath;
import com.querydsl.core.types.dsl.NumberPath;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.siveco.ael.lcms.domain.course.model.CourseType;
import ro.siveco.ael.lcms.domain.course.model.QCourse;
import ro.siveco.ael.lcms.domain.course.model.QPlannedCourse;
import ro.siveco.ael.lcms.domain.metadata.model.EntityType;
import ro.siveco.ael.lcms.domain.metadata.model.RelationType;
import ro.siveco.ael.lcms.domain.metadata.service.MetadataService;
import ro.siveco.ael.service.utils.DatesService;

import java.time.*;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class ItemCommonFilterService {

    @Autowired
    private CourseService courseService;

    @Autowired
    private MetadataService metadataService;

    @Autowired
    private PlannedCourseService plannedCourseService;

    @Autowired
    DatesService datesService;

    public BooleanExpression getTypeFilterExpression(List<String> filterValues, Boolean isCourseFilter) {
        BooleanExpression typeExpression = null;
        EnumPath<CourseType> courseTypePath = null;
        if (!isCourseFilter) {
            courseTypePath = QPlannedCourse.plannedCourse.course.type;
        } else {
            courseTypePath = QCourse.course.type;
        }
        for (String type : filterValues) {
            if (typeExpression == null) {
                typeExpression = (courseTypePath.eq(getCourseTypeFromFilter(type)));
            } else {
                typeExpression = typeExpression.or(courseTypePath.eq(getCourseTypeFromFilter(type)));
            }
        }

        return typeExpression;
    }

    public BooleanExpression getMySearchFilterExpression(List<String> filterValues) {
        BooleanExpression searchExpression = null;
        com.querydsl.core.types.dsl.StringPath courseNamePath = QCourse.course.name;
        com.querydsl.core.types.dsl.StringPath courseDescriptionPath = QCourse.course.description;
        for (String value : filterValues) {
            String[] strArray = value.split("\\s+");
            /* title */
            for(int i = 0; i < strArray.length; i++) {
                if(i == 0) { searchExpression = (courseNamePath.upper().like("%" + strArray[i].toUpperCase() + "%")); }
                else { searchExpression = searchExpression.or((courseNamePath.upper().like("%" + strArray[i].toUpperCase() + "%"))); }
            }
            /* description */
            for(int i = 0; i < strArray.length; i++) {
                if (i == 0) { searchExpression = searchExpression.or((courseDescriptionPath.upper().like("%" + strArray[i].toUpperCase() + "%"))); }
                else { searchExpression = searchExpression.or((courseDescriptionPath.upper().like("%" + strArray[i].toUpperCase() + "%"))); }
            }
            break;
        }
        return searchExpression;
    }

    public CourseType getCourseTypeFromFilter(String type) {
        if (type.equals(CourseType.COURSE.toString())) {
            return CourseType.COURSE;
        } else if (type.equals(CourseType.ACTIVITY.toString())) {
            return CourseType.ACTIVITY;
        } else if (type.equals(CourseType.SERVICE.toString())) {
            return CourseType.SERVICE;
        } else {
            return CourseType.MEETING;
        }
    }

    public BooleanExpression getDateFilterExpression(List<String> filterValues, Boolean isCourseFilter) {
        BooleanExpression dateExpression = null;
        LocalDateTime periodStartDate = null;
        LocalDateTime periodEndDate = null;

        LocalDateTime now = LocalDateTime.now(); // current date and time

        for (String filter : filterValues) {
            if (!"CALENDAR".equals(filter)) {
                if ("TODAY".equals(filter)) {
                    periodStartDate = now.toLocalDate().atStartOfDay();
                    LocalDateTime tomorrowMidnight = periodStartDate.plusDays(1);
                    periodEndDate = tomorrowMidnight.minusSeconds(1);
                } else if ("THIS_WEEK".equals(filter)) {
                    LocalDateTime firstDayOfWeek = now.with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY));
                    periodStartDate = firstDayOfWeek.toLocalDate().atStartOfDay();
                    LocalDateTime firstDayOfNextWeek = periodStartDate.plusWeeks(1);
                    periodEndDate = firstDayOfNextWeek.minusSeconds(1);
                } else if ("THIS_MONTH".equals(filter)) {
                    LocalDateTime firstDayOfMonth = now.with(TemporalAdjusters.firstDayOfMonth());
                    periodStartDate = firstDayOfMonth.toLocalDate().atStartOfDay();
                    LocalDateTime firstDayOfNextMonth = periodStartDate.plusMonths(1);
                    periodEndDate = firstDayOfNextMonth.minusSeconds(1);
                }

                dateExpression = getDateExpressionForPeriod(periodStartDate, periodEndDate, isCourseFilter);
            }
        }
        return dateExpression;
    }

    public BooleanExpression getCalendarFilterExpression(List<String> filterValues, Boolean isCourseFilter) {
        BooleanExpression calendarExpression = null;
        LocalDateTime periodStartDate = null;
        LocalDateTime periodEndDate = null;

        if (filterValues != null && filterValues.size() == 2) {

            Date calendarStart = new Date(Long.parseLong(filterValues.get(0)));
            periodStartDate = calendarStart.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();

            Date calendarEnd = new Date(Long.parseLong(filterValues.get(1)));
            periodEndDate = calendarEnd.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();

            calendarExpression = getDateExpressionForPeriod(periodStartDate, periodEndDate, isCourseFilter);
        }

        return calendarExpression;
    }

    public BooleanExpression getDateExpressionForPeriod(LocalDateTime periodStart, LocalDateTime periodEnd, Boolean isCourseFilter) {
        BooleanExpression dateExpression = null;
        Date periodStartDate = Date.from(periodStart.atZone(ZoneId.of(datesService.getCurrentUserTimezoneId())).toInstant());
        Date periodEndDate = Date.from(periodEnd.atZone(ZoneId.of(datesService.getCurrentUserTimezoneId())).toInstant());

        if (!isCourseFilter) {
            DateTimePath<Date> startDatePath = QPlannedCourse.plannedCourse.startDate;
            DateTimePath<Date> endDatePath = QPlannedCourse.plannedCourse.endDate;
            dateExpression = ((startDatePath.loe(periodStartDate)).and(endDatePath.goe(periodStartDate))).or((startDatePath.loe(periodEndDate)).and(endDatePath.goe(periodEndDate))).
                    or((startDatePath.goe(periodStartDate)).and(startDatePath.loe(periodEndDate)).and(endDatePath.goe(periodStartDate)).and(endDatePath.loe(periodEndDate))).or((startDatePath.goe(periodEndDate)).and(endDatePath.isNull()));
        } else {
            NumberPath<Long> courseIdPath = QCourse.course.id;
            List<Long> courseIds = courseService.getCourseIdsForPeriod(periodStartDate, periodEndDate);
            dateExpression = courseIdPath.in(courseIds);
        }

        return dateExpression;
    }

    public BooleanExpression getNotExpiredExpression() {
        Date endDate = datesService.getYesterday();
        NumberPath<Long> courseIdPath = QCourse.course.id;
        List<Long> courseIds = courseService.getCourseIdsExpiresAfterDate(endDate);
        BooleanExpression dateExpression = courseIdPath.in(courseIds);

        return dateExpression;
    }

    public BooleanExpression getTagFilterExpression(List<String> filterValues, Boolean isCourseFilter) {
        BooleanExpression tagExpression = null;
        List<Long> tagIds = new ArrayList<>();
        for(String value : filterValues)
        {
            tagIds.add(Long.parseLong(value));
        }
        List<Long> courseIds = metadataService.getEntityIdsForTags(EntityType.COURSE, RelationType.INCLUDED, tagIds);
        NumberPath<Long> courseIdPath;
        if (!isCourseFilter) {
            courseIdPath = QPlannedCourse.plannedCourse.course.id;
        } else {
            courseIdPath = QCourse.course.id;
        }
        tagExpression = courseIdPath.in(courseIds);

        return tagExpression;
    }

    public Long getMaxDistanceFromFilter(List<String> filterValues) {
        Long distance = 0L;
        for (String filter : filterValues) {
            if ("DISTANCE_1".equals(filter)) {
                distance = 1L;
            }else if ("DISTANCE_2".equals(filter)) {
                distance = 2L;
            }else if ("DISTANCE_5".equals(filter)) {
                distance = 5L;
            }else if ("DISTANCE_10".equals(filter)) {
                distance = 10L;
            }else if ("DISTANCE_15".equals(filter)) {
                distance = 15L;
            }else if ("DISTANCE_25".equals(filter)) {
                distance = 25L;
            }else if ("DISTANCE_35".equals(filter)) {
                distance = 35L;
            }else if ("DISTANCE_50".equals(filter)) {
                distance = 50L;
            }else if ("DISTANCE_100".equals(filter)) {
                distance = 100L;
            }else if ("DISTANCE_150".equals(filter)) {
                distance = 150L;
            }else if ("DISTANCE_200".equals(filter)) {
                distance = 200L;
            }
        }
        return distance;
    }

    public BooleanExpression getDistanceFilterExpression(List<String> filterValues, Boolean isCourseFilter) {

        Long maxDistance = getMaxDistanceFromFilter(filterValues);
        BooleanExpression distanceExpression = null;
        NumberPath<Long> courseIdPath;

        List<Long> courseIds = plannedCourseService.getCoursesFilteredByDistance(maxDistance);
        if (!isCourseFilter) {
            courseIdPath = QPlannedCourse.plannedCourse.course.id;
        } else {
            courseIdPath = QCourse.course.id;
        }
        distanceExpression = courseIdPath.in(courseIds);

        return distanceExpression;
    }

}
