package ro.siveco.ael.lcms.domain.tenant.model;

import ro.siveco.ram.model.Entity;

/**
 * User: AlexandruVi
 * Date: 2017-04-26
 */
public interface TenantHolder extends Entity<Long> {

    Tenant getTenant();

    void setTenant(Tenant tenant);
}
