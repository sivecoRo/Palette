package ro.siveco.ael.lcms.domain.organization.model;

public class ComponentUserJobPathDTO {
	private Long userId;
	private Long componentId;
	private Long jobTitleId;
	private Boolean setAsActive;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getComponentId() {
		return componentId;
	}

	public void setComponentId(Long componentId) {
		this.componentId = componentId;
	}

	public Long getJobTitleId() {
		return jobTitleId;
	}

	public void setJobTitleId(Long jobTitleId) {
		this.jobTitleId = jobTitleId;
	}

	public Boolean getSetAsActive() {
		return setAsActive;
	}

	public void setSetAsActive(Boolean setAsActive) {
		this.setAsActive = setAsActive;
	}
}