package ro.siveco.ael.lcms.domain.profile.model;

import ro.siveco.ael.domain.model.core.SerialValue;

/**
 * User: AlexandruVi
 * Date: 2017-01-09
 */
public interface ProfileProperty extends SerialValue {

    /**
     * @return The property
     */
    public ProfilePropertyName getPropertyName();
}
