package ro.siveco.ael.lcms.domain.tenant.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.siveco.ael.lcms.domain.tenant.model.Ip;
import ro.siveco.ael.lcms.domain.tenant.model.Tenant;
import ro.siveco.ael.lcms.repository.IpRepository;
import ro.siveco.ram.service.exception.ResourceCreateFailedException;
import ro.siveco.ram.starter.service.BaseEntityService;

import java.util.List;

@Service
public class IpService extends BaseEntityService<Ip> {
	
	@Autowired
	public IpService(IpRepository resourceRepository) {
		super(Ip.class, resourceRepository);
	}

	public void checkIpValidation(Ip ip) throws ResourceCreateFailedException {
		String value = ip.getValue();
		
		if ( value==null || value.equals("") ) {
			throw new ResourceCreateFailedException("emptyIp");
		}
		
		String[] nrs = value.split("[.]");

		if ( nrs.length!=4 ) {
			throw new ResourceCreateFailedException("invalidIp");
		}
			
		for(int i=0; i<nrs.length; i++) {
			try {
				int num = Integer.parseInt(nrs[i]);
				
				if ( num<0 || num>255 ) {
					throw new ResourceCreateFailedException("invalidIp");
				}
			} catch (NumberFormatException e) {
				throw new ResourceCreateFailedException("invalidIp");
			}
		}
	}

    public void checkIfIpExists(String value, Tenant tenant) {

        Ip ip1 = ((IpRepository) getResourceRepository()).findOneByValueAndTenant(value, tenant);
        if (ip1 != null) {
            throw new ResourceCreateFailedException("model.ip.already.exists");
        }
    }

	public List<String> getByTenant(Tenant tenant)
	{
		if(tenant != null)
		{
			return ((IpRepository) getResourceRepository()).getByTenant(tenant);
		}
		else
		{
			return ((IpRepository) getResourceRepository()).getByTenantNull();
		}
	}

}
