package ro.siveco.ael.lcms.domain.profile.model;

import ro.siveco.ram.model.Entity;
import ro.siveco.ram.model.Model;
import ro.siveco.ram.starter.ui.config.PropertyViewDescriptor;
import ro.siveco.ram.starter.ui.config.ViewDescriptor;
import ro.siveco.ram.starter.ui.model.ViewRendererType;

import javax.persistence.Transient;
import java.io.Serializable;

/**
 * Created by albertb on 1/30/2018.
 */
@ViewDescriptor
public class InterestDTO implements Model, Serializable, Entity<Long> {
        private String id;
        private String tag_id;
        private String image;
        private String image_selected;

        public Long getId() {
            return Long.valueOf(id);
        }

        @Override
        public boolean isNew() {
            return false;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTag_id() {
            return tag_id;
        }

        public void setTag_id(String tag_id) {
            this.tag_id = tag_id;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getImage_selected() {
            return image_selected;
        }

        public void setImage_selected(String image_selected) {
            this.image_selected = image_selected;
        }

        @Override
        public String getModelName() {
            return null;
        }

        @Override
        public String getObjectUID() {

            return String.valueOf(getId());
        }

        @Override
        @Transient
        @PropertyViewDescriptor(
                hiddenOnGrid = true,
                rendererType = ViewRendererType.OBJECT_LABEL)
        public String getObjectLabel() {

            return getImage();
        }

    }

