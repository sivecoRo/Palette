package ro.siveco.ael.lcms.domain.auth.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.siveco.ael.lcms.domain.auth.model.UserCompetence;
import ro.siveco.ael.lcms.domain.taxonomies.model.Competence;
import ro.siveco.ael.lcms.domain.taxonomies.model.TaxonomyLevel;
import ro.siveco.ael.lcms.repository.UserCompetenceRepository;
import ro.siveco.ram.starter.service.BaseEntityService;

import java.util.List;

/**
 * Created by RoxanneS on 29/06/2017.
 */

@Service
public class UserCompetenceService extends BaseEntityService<UserCompetence> {

    @Autowired
	public UserCompetenceService(UserCompetenceRepository resourceRepository) {
		super(UserCompetence.class, resourceRepository);
	}

	public Boolean previousConnexionExists(UserCompetence userCompetence){
		return ((UserCompetenceRepository)getResourceRepository()).
				findDuplicateConnexion(
						userCompetence.getUser().getId(),
						userCompetence.getCompetence().getId(),
						userCompetence.getId() != null ? userCompetence.getId() : 0L
				) != null;
	}

	public List<UserCompetence> findUserCompetencesByUser(Long userId) {
		return ((UserCompetenceRepository) getResourceRepository()).findUserCompetencesByUser(userId);
	}

	public Boolean competenceExists(Competence competence) {

		Boolean value = ((UserCompetenceRepository) getResourceRepository()).findByCompetence(competence).isEmpty();
		return !value;
	}

	public Boolean taxonomyLevelExists(TaxonomyLevel taxonomyLevel) {

		Boolean value = ((UserCompetenceRepository) getResourceRepository()).findByTaxonomyLevel(taxonomyLevel).isEmpty();
		return !value;
	}
}