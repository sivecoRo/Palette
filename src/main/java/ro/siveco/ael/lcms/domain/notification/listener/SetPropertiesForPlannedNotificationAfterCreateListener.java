package ro.siveco.ael.lcms.domain.notification.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.notification.model.PlannedNotification;
import ro.siveco.ael.lcms.domain.notification.service.PlannedNotificationService;
import ro.siveco.ram.event.listener.type.single.BaseAfterCreateListener;
import ro.siveco.ram.event.type.single.AfterCreateEvent;

/**
 * Created by IuliaP on 29.09.2017.
 */
@Order(value = 1)
@Component
public class SetPropertiesForPlannedNotificationAfterCreateListener
        extends BaseAfterCreateListener<PlannedNotification> {

    @Autowired
    PlannedNotificationService plannedNotificationService;

    @Override
    public void onApplicationEvent(AfterCreateEvent<PlannedNotification> event) {

        plannedNotificationService.createNotificationSchedule(event.getDomainModel());
    }
}
