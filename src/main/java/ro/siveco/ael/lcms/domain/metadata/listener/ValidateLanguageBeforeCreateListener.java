package ro.siveco.ael.lcms.domain.metadata.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.metadata.model.Language;
import ro.siveco.ael.lcms.domain.metadata.service.LanguageService;
import ro.siveco.ram.event.listener.type.single.BaseBeforeCreateListener;
import ro.siveco.ram.event.type.single.BeforeCreateEvent;

/**
 * Created by IuliaP on 13.10.2017.
 */
@Component
@Order(value = 1)
public class ValidateLanguageBeforeCreateListener extends BaseBeforeCreateListener<Language> {

    @Autowired
    LanguageService languageService;

    @Override
    public void onApplicationEvent(BeforeCreateEvent<Language> event) {
        languageService.validateLanguage( event.getDomainModel() );
    }
}
