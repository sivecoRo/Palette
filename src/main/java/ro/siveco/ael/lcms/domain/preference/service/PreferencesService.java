package ro.siveco.ael.lcms.domain.preference.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.siveco.ael.lcms.domain.auth.model.AelUser;
import ro.siveco.ael.lcms.domain.preference.model.Preference;
import ro.siveco.ael.lcms.domain.tenant.model.Tenant;
import ro.siveco.ael.lcms.repository.PreferencesRepository;
import ro.siveco.ram.starter.service.BaseEntityService;

import java.util.*;

@Service
public class PreferencesService extends BaseEntityService<Preference>{

	@Autowired
	public PreferencesService(PreferencesRepository preferencesRepository) {
		super(Preference.class, preferencesRepository);
	}

	
    public List<Preference> getPreferences( Long userId )
	{
		PreferencesRepository preferencesRepository = ((PreferencesRepository)getResourceRepository());
		List<Preference> defaultPreferences = preferencesRepository.getAllDefault();

		if ( userId == null )
		{
			return defaultPreferences;
		}

		List<Preference> userPreferences = preferencesRepository.getAllByUserId(userId);

        Map<String, Preference> mergedPreferences = new HashMap<String, Preference>();

        for(Preference defaultPreference : defaultPreferences) {
            mergedPreferences.put(defaultPreference.getName(), defaultPreference);
        }
        for(Preference userPreference : userPreferences) {
            mergedPreferences.put(userPreference.getName(), userPreference);
        }

        List<Preference> mergedPreferencesList = new ArrayList<Preference>();
        mergedPreferencesList.addAll(mergedPreferences.values());
        return mergedPreferencesList;
	}

	public Map<String, String> getPreferencesMap( Long userId )
	{
		List<Preference> preferences = getPreferences( userId );
		Map<String, String> defaultPreferencesMap = new HashMap<String, String>();

		for ( Iterator it = preferences.iterator(); it.hasNext(); )
		{
			Preference p = ( Preference ) it.next();

			defaultPreferencesMap.put( p.getName(), p.getValue() );

		}
		return defaultPreferencesMap;
	}

	public Preference getByName( String prefName, Tenant tenant )
	{
		List<Preference> preferences = ((PreferencesRepository)getResourceRepository()).getByName(prefName, tenant.getId());
		return preferences != null && preferences.size() > 0 ? preferences.get(0) : null;
	}

	public Preference getByName( String prefName)
	{
		List<Preference> preferences = ((PreferencesRepository)getResourceRepository()).getByName(prefName);
		return preferences != null && preferences.size() > 0 ? preferences.get(0) : null;
	}

	public Preference saveOrUpdate(Preference preference)
	{
		if(preference.getId() == null)
		{
			return save(preference);
		}
		else
		{
			return update(preference);
		}
	}

	public Preference getByNameAndUser( String prefName, AelUser user)
	{
		List<Preference> preferences = ((PreferencesRepository)getResourceRepository()).getByNameAndUser(prefName, user.getId());
		return preferences != null && preferences.size() > 0 ? preferences.get(0) : null;
	}

	public void save(List<Preference> preferences) {
		getResourceRepository().save(preferences);
	}

	public Preference getGlobalPreferenceByName(String name)
	{
		List<Preference> preferences = ((PreferencesRepository)getResourceRepository()).getGlobalPreferenceByName(name);
		return preferences != null && preferences.size() > 0 ? preferences.get(0) : null;
	}

}
