package ro.siveco.ael.lcms.domain.notification.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.notification.model.PlannedNotification;
import ro.siveco.ael.lcms.domain.notification.service.PlannedNotificationService;
import ro.siveco.ram.event.listener.type.single.BaseBeforeCreateListener;
import ro.siveco.ram.event.type.single.BeforeCreateEvent;

/**
 * Created by IuliaP on 29.09.2017.
 */
@Order(value = 3)
@Component
public class SetPropertiesForPlannedNotificationBeforeCreateListener
        extends BaseBeforeCreateListener<PlannedNotification> {

    @Autowired
    PlannedNotificationService plannedNotificationService;

    @Override
    public void onApplicationEvent(BeforeCreateEvent<PlannedNotification> event) {

        PlannedNotification plannedNotification = event.getDomainModel();
        plannedNotification.setEntityId(plannedNotification.getPlannedCourse().getId());
    }
}
