package ro.siveco.ael.lcms.domain.auth.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.auth.model.AelGroup;
import ro.siveco.ael.lcms.domain.auth.service.AelGroupService;
import ro.siveco.ram.app.exception.AppException;
import ro.siveco.ram.event.listener.type.single.BaseBeforeUpdateListener;
import ro.siveco.ram.event.type.single.BeforeUpdateEvent;

/**
 * Created by AndradaC on 9/1/2017.
 */
@Component
public class SetDatabaseInformationForGroupBeforeUpdateListener extends BaseBeforeUpdateListener<AelGroup>{

    @Autowired
    private AelGroupService aelGroupService;

    @Override
    public void onApplicationEvent(BeforeUpdateEvent<AelGroup> event) {
        AelGroup aelGroup = event.getDomainModel();
        AelGroup dbAelGroup = event.getStoredDomainModel();

        aelGroupService.validateGroup(aelGroup);

        aelGroup.setReadOnly(dbAelGroup.getReadOnly());
        aelGroup.setImported(dbAelGroup.getImported());
        aelGroup.setRelatedUid(aelGroup.getName());
        aelGroup.setTenant(dbAelGroup.getTenant());
        dbAelGroup.setRoles(aelGroup.getRoles());
        aelGroup.setRoles(dbAelGroup.getRoles());
        //aelGroup.setUsers(dbAelGroup.getUsers());
    }
}
