package ro.siveco.ael.lcms.domain.profile.model;

import ro.siveco.ael.lcms.domain.auth.model.User;
import ro.siveco.ram.model.Entity;

import java.util.Collection;

/**
 * User: AlexandruVi
 * Date: 2016-12-07
 */
public interface Profile extends Entity<Long> {

    /**
     * @return The user associated with this profile
     */
    public User getUser();

    /**
     * @return the Associated profile properties for this user
     */
    public Collection<ProfileProperty> getProperties();
}
