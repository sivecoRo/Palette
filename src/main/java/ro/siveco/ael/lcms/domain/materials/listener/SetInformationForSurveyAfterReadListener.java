package ro.siveco.ael.lcms.domain.materials.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.materials.model.Survey;
import ro.siveco.ael.lcms.domain.materials.service.SurveyService;
import ro.siveco.ram.event.listener.type.single.BaseAfterReadListener;
import ro.siveco.ram.event.type.single.AfterReadEvent;

@Component
public class SetInformationForSurveyAfterReadListener extends BaseAfterReadListener<Survey> {

    @Autowired
    private SurveyService surveyService;

    @Override
    public void onApplicationEvent(AfterReadEvent<Survey> event) {
        Survey survey = event.getDomainModel();
        if(!survey.isNew())
        {
            survey = surveyService.buildSurveyToDisplay(survey);
        }
    }
}
