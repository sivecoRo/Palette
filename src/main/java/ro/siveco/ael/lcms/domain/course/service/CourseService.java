package ro.siveco.ael.lcms.domain.course.service;

import org.apache.batik.transcoder.TranscoderException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ro.siveco.ael.lcms.domain.auth.model.AelUser;
import ro.siveco.ael.lcms.domain.auth.model.User;
import ro.siveco.ael.lcms.domain.auth.service.AelUserService;
import ro.siveco.ael.lcms.domain.course.model.Course;
import ro.siveco.ael.lcms.domain.course.model.CourseType;
import ro.siveco.ael.lcms.domain.course.model.PlannedCourse;
import ro.siveco.ael.lcms.repository.CourseRepository;
import ro.siveco.ael.lcms.repository.PlannedCourseRepository;
import ro.siveco.ael.lcms.repository.PlannedCourseStudentRepository;
import ro.siveco.ael.service.MultimediaService;
import ro.siveco.ael.service.utils.DatesService;
import ro.siveco.ael.web.controllers.WithUserBaseEntityController;
import ro.siveco.ram.app.exception.AppException;
import ro.siveco.ram.starter.service.BaseEntityService;

import java.io.ByteArrayOutputStream;
import java.util.*;

/**
 * -- DEMO --
 * User: AlexandruVi
 * Date: 2017-04-06
 */
@Service
public class CourseService extends BaseEntityService<Course> {
    private final MultimediaService multimediaService;

    private final Integer COURSE_NAME_LENGTH = 400;
    private final Integer COURSE_DESCRIPTION_LENGTH = 2000;
    private final Integer COURSE_OBJECTIVE_LENGTH = 2000;

    @Autowired
    private DatesService datesService;

    @Autowired
    private PlannedCourseRepository plannedCourseRepository;

    @Autowired
    private PlannedCourseStudentRepository plannedCourseStudentRepository;

    @Autowired
    AelUserService aelUserService;

    @Autowired
    MessageSource messageSource;

    @Autowired
    public CourseService(CourseRepository resourceRepository,
                         MultimediaService multimediaService) {
        super(Course.class, resourceRepository);
        this.multimediaService = multimediaService;
    }

    public Course findById(Long courseId){
        return this.getResourceRepository().findOne(courseId);
    }

    public List<Course> getAsocCoursesForUser(Long userId) {
        return ((CourseRepository) getResourceRepository()).getAsocCoursesForUser(userId);
    }

    public Course addImage(Course course, MultipartFile file) throws Exception {

        if ( file!=null ) {
            ByteArrayOutputStream baos = multimediaService.processThumbnail(file);
            byte[] binaryData = baos.toByteArray();

            course.setImage(binaryData);
            course = update(course);
        }
        course = ((CourseRepository) getResourceRepository()).saveAndFlush(course);

        return course;
    }


    public Course setDefaultCreationInformation(Course course, String viewName)
    {
        User user = WithUserBaseEntityController.getUser();
        course.setCreationDate(new Date());
        course.setCreator((AelUser)user);
        course.setOwner((AelUser)user);
        if(viewName.endsWith("ActivityView"))
        {
            course.setType(CourseType.ACTIVITY);
        }
        else if(viewName.endsWith("ServiceView")) {
            course.setType(CourseType.SERVICE);
        }
        else if(viewName.endsWith("MeetingView")) {
            course.setType(CourseType.MEETING);
        } else {
            course.setType(CourseType.COURSE);
        }


        if (course.getImageMultipart() != null && course.getImageMultipart().length > 0) {
            try {
                ByteArrayOutputStream baos = multimediaService.processThumbnail(course.getImageMultipart()[0]);
                course.setImage(baos.toByteArray());
            } catch (TranscoderException e) {
                getLogger().warning(e.getMessage());
            }
        }

        return course;
    }

    public void validateCourse(Course course, Course dbCourse)
    {
        Calendar calendar = new GregorianCalendar();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        Date currentDate = calendar.getTime();

        currentDate = datesService.formatDate(currentDate, datesService.getCurrentUserTimezoneId());

        if(course.getName() == null || "".equals(course.getName()))
        {
            Locale locale = aelUserService.getLocaleForUser((AelUser) WithUserBaseEntityController.getUser());
            throw new AppException("error.course.name.invalid", new Object[]{messageSource.getMessage("model.course.type." + course.getType().name().toLowerCase(), null, locale)});
        }

        if (course.getName().length() > COURSE_NAME_LENGTH ) {
            throw new AppException("error.course.name.length");
        }

        if (course.getDescription() != null && course.getDescription().length() > COURSE_DESCRIPTION_LENGTH ) {
            throw new AppException("error.course.description.length");
        }

        if (course.getObjective() != null && course.getObjective().length() > COURSE_OBJECTIVE_LENGTH ) {
            throw new AppException("error.course.objective.length");
        }

        if(course.getOwner() == null) {
            throw new AppException("error.course.owner.null");
        }

        if (course.getCreator() != null) {
            Long courseId = 0L;
            if (course.getId() != null) {
                courseId = course.getId();
            }
            List<Course> coursesForName = ((CourseRepository) getResourceRepository()).findCoursesByNameAndCreator(course.getName(), course.getCreator(), courseId);
            if (coursesForName != null && coursesForName.size() > 0) {
                throw new AppException("error.course.name.duplicate");
            }
        }
        if(course.getType() != null && !(course.getType().equals(CourseType.SERVICE) || course.getType().equals(CourseType.MEETING)) && course.getPlannedCourseEndDate() == null)
        {
            throw new AppException("model.plannedCourse.error.endDate.null");
        }
        if(course.getPlannedCourseStartDate() == null)
        {
            throw new AppException("model.plannedCourse.error.startDate.null");
        }
        if(course.getPlannedCourseStartTime() == null)
        {
            throw new AppException("model.plannedCourse.error.startTime.null");
        }

        Integer hoursOffset = 0;
        if (dbCourse == null ||
                (dbCourse.getPlannedCourseStartTime() != null && !dbCourse.getPlannedCourseStartTime().equals(course.getPlannedCourseStartTime()))) {
            hoursOffset = datesService.getCurrentOffset();
        }

        if(datesService.isInThePast(course.getPlannedCourseStartDate(), course.getPlannedCourseStartTime(), hoursOffset))
        {
            throw new AppException("model.plannedCourse.error.startDate.inThePast");
        }

        if(course.getType() != null && !(course.getType().equals(CourseType.SERVICE) || course.getType().equals(CourseType.MEETING)) && course.getPlannedCourseEndTime() == null)
        {
            throw new AppException("model.plannedCourse.error.endTime.null");
        }

        if(course.getPlannedCourseEndDate() != null && course.getPlannedCourseEndDate().before(course.getPlannedCourseStartDate()))
        {
            throw new AppException("model.plannedCourse.error.endDate.before.startDate");
        }

        if (course.getPlannedCourseEndDate() != null && course.getPlannedCourseEndDate().before(datesService.getYesterdayNight()))
        {
            throw new AppException("model.plannedCourse.error.endDate.before.currentDate");
        }

        if (course.getPlannedCourseStartDate() != null && course.getPlannedCourseStartTime() != null
                && course.getPlannedCourseEndDate() != null && course.getPlannedCourseEndTime() != null
                && course.getPlannedCourseStartDate().equals(course.getPlannedCourseEndDate())
                && course.getPlannedCourseEndTime().before(course.getPlannedCourseStartTime())) {

            throw new AppException("model.plannedCourse.error.endTime.before.startTime");
        }

        if (course.getIncludedTags() == null || course.getIncludedTags().size() == 0) {
            throw new AppException("tenant.admin.interest.required");
        }

        if (course.getItemLocation() == null || course.getItemLocation().equals("") || course.getItemLocation().equals("-") || course.getItemLocationCoordinates() == null) {
            throw new AppException("model.plannedCourse.error.location.null");
        }

        if (course.getMinUserNumber() != null && course.getMinUserNumber() < 1) {
            throw new AppException("model.plannedCourse.error.minUsers.less.than.one");
        }

        if (course.getMaxUserNumber() != null && course.getMaxUserNumber() < 1) {
            throw new AppException("model.plannedCourse.error.maxUsers.less.than.one");
        }

        if(course.getMinUserNumber() != null && course.getMaxUserNumber() != null && course.getMinUserNumber() > course.getMaxUserNumber())
        {
            throw new AppException("model.plannedCourse.error.maxUsers.less.than.minUsers");
        }
    }

    public List<Long> getCourseIdsForPeriod(Date startDatePeriod, Date endDatePeriod)
    {
        return ((CourseRepository)getResourceRepository()).getCourseIdsForPeriod(startDatePeriod,endDatePeriod);
    }

    public List<Long> getCourseIdsExpiresAfterDate(Date endDate) {
        return ((CourseRepository)getResourceRepository()).getCourseIdsExpiresAfterDate(endDate);
    }

    public Integer getCourseNumberForOwner(AelUser owner)
    {
        return ((CourseRepository)getResourceRepository()).getCourseNumberForOwner(owner.getId());
    }

    public Course saveOrUpdate( Course course )
    {
        if(course.getId() != null)
        {
            this.update(course);
        }
        else
        {
            this.save(course);
        }
        return course;
    }

    public void removeCoursesByUser(AelUser aelUser) {

        // Get list of courses created by specified user.
        List<Course> courses = ((CourseRepository)getResourceRepository()).findByCreatorId(aelUser.getId());

        for(Course course : courses) {
            // Remove item (row/record from course table).
            super.remove(course.getId());
        }
    }

    public List<Course> getPastCourses(AelUser user) {
        List<Course> courses = new ArrayList<>();
        if (user != null) {
            courses = ((CourseRepository) getResourceRepository()).getCoursesExpiresBeforeDate(user.getId(), datesService.getYesterday());
            for(Course course : courses) {
                populateCourse(course, user);
            }
        }
        return courses;
    }

    private void populateCourse(Course course, AelUser user) {
        Locale locale = aelUserService.getLocaleForUser((AelUser) user);
        if (course != null) {
            List<PlannedCourse> plannedCoursesList = plannedCourseRepository.findByCourseId(course.getId());
            course.setPlannedCoursesNumber(plannedCourseRepository.getPlannedCourseNumberForCourse(course.getId()));
            if (plannedCoursesList != null && plannedCoursesList.size() > 0) {
                PlannedCourse firstPlannedCourse = plannedCoursesList.get(0);
                course.setPlannedCourseStartDate(datesService.formatDate(firstPlannedCourse.getStartDate(), datesService.getCurrentUserTimezoneId()));
                course.setPlannedCourseEndDate(datesService.formatDate(firstPlannedCourse.getEndDate(), datesService.getCurrentUserTimezoneId()));
                if (firstPlannedCourse.getStartDate() != null) {
                    course.setDisplayPlannedCourseStartDate(datesService.formatLongDate(firstPlannedCourse.getStartDate()));
                }
                if (firstPlannedCourse.getEndDate() != null) {
                    course.setDisplayPlannedCourseEndDate(datesService.formatLongDate(firstPlannedCourse.getEndDate()));
                }
                Integer studentsNumber = plannedCourseStudentRepository.getFavoritesStudentsNumberForCourse(course.getId());
                course.setStudentsNumber(studentsNumber);
                course.setDisplayStudentsNumber(studentsNumber == null ? null : studentsNumber.toString() + " " + messageSource.getMessage("displayStudentsNumber.label", null, locale));
                setDefaultImage(course);
            }
        }
    }

    public void setDefaultImage(Course course) {
        if (course != null && course.getType() != null) {
            String defaultImageSrc = getDefaultImageSrc(course.getType());
            course.setImageSrc(defaultImageSrc);
            course.setAddImageSrc(defaultImageSrc);
        }
    }

    public String getDefaultImageSrc(CourseType courseType) {
        if(courseType != null) {
            return "/img/generic/generic-" + courseType.toString().toLowerCase() + "-light.png";
        }
        return null;
    }

    public Boolean canEditCourse(Long courseId) {
        if (courseId != null) {
            Course course = findById(courseId);
            AelUser currentUser = (AelUser) WithUserBaseEntityController.getUser();
            return course != null && course.getCreator() != null && currentUser.getId().equals(course.getCreator().getId());
        }
        return false;
    }
}
