package ro.siveco.ael.lcms.domain.tenant.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import ro.siveco.ram.starter.ui.config.PropertyViewDescriptor;
import ro.siveco.ram.starter.ui.config.ViewDescriptor;
import ro.siveco.ram.starter.ui.model.FormInputType;
import ro.siveco.ram.starter.ui.model.IndexViewType;
import ro.siveco.ram.starter.ui.model.ViewRendererType;

import javax.persistence.*;

@Entity
@Table( name = "ips" )
@SequenceGenerator( name = "ips_seq", sequenceName = "ips_seq" )
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
@ViewDescriptor(pageSize = 10, viewTypes = {IndexViewType.LIST}, searchEnabled = true, defaultSearchProperty = "value")
public class Ip extends ro.siveco.ram.starter.model.base.BaseEntity 
{
	private String value;

    private Tenant tenant;


    @Id
    @Column
	@GeneratedValue( generator = "ips_seq", strategy = GenerationType.AUTO )
	public Long getId()
	{
		return super.getId();
	}

    public void setId( Long id )
    {
    	super.setId(id);
    }

    @Column(name = "value", nullable = false, length = 15)
	@PropertyViewDescriptor(inputType = FormInputType.TEXT, rendererType = ViewRendererType.STRING,
            editInline = false, order = 1, required = true)
    public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

    @ManyToOne
    @JoinColumn(name = "tenant_id", nullable = true)
	@PropertyViewDescriptor(inputType = FormInputType.LIST_OF_VALUES, rendererType = ViewRendererType.MODEL,
            editInline = false, order = 2, hidden = true, hiddenOnForm = true)
    public Tenant getTenant() {
		return tenant;
	}

	public void setTenant(Tenant tenant) {
		this.tenant = tenant;
	}

    @Override
    @Transient
    public String getObjectLabel() {
        return getValue();
    }
	
}
