package ro.siveco.ael.lcms.domain.notification.model;

/**
 * Created by IuliaP on 29.09.2017.
 */
public enum NotificationEventType {
    BEFORE_PLANNED_COURSE(NotificationEntityType.PLANNED_COURSE),
    ON_PLANNED_COURSE(NotificationEntityType.PLANNED_COURSE),
    AFTER_PLANNED_COURSE(NotificationEntityType.PLANNED_COURSE);

    private NotificationEntityType notificationEntityType;

    NotificationEventType(NotificationEntityType notificationEntityType) {

        this.notificationEntityType = notificationEntityType;
    }
}
