package ro.siveco.ael.lcms.domain.materials.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.siveco.ael.lcms.domain.materials.model.SurveyItem;
import ro.siveco.ael.lcms.repository.SurveyItemRepository;
import ro.siveco.ram.repository.BaseEntityRepository;
import ro.siveco.ram.starter.service.BaseEntityService;

import java.util.List;

/**
 * Created by AndradaC on 5/2/2017.
 */
@Service
public class SurveyItemService extends BaseEntityService<SurveyItem> {
    @Autowired
    public SurveyItemService(BaseEntityRepository<SurveyItem> resourceRepository) {
        super(SurveyItem.class, resourceRepository);
    }

    public SurveyItem findOneById(Long id) {
        return ((SurveyItemRepository) getResourceRepository()).findOneById(id);
    }

    public List<SurveyItem> findBySurveyId(Long id) {
        return ((SurveyItemRepository) getResourceRepository()).findBySurveyId(id);
    }

}
