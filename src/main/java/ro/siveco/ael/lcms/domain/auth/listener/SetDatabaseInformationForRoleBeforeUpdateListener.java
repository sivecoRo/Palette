package ro.siveco.ael.lcms.domain.auth.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.auth.model.AelRole;
import ro.siveco.ael.lcms.domain.auth.service.AelRoleService;
import ro.siveco.ram.event.listener.type.single.BaseBeforeUpdateListener;
import ro.siveco.ram.event.type.single.BeforeUpdateEvent;

/**
 * Created by AndradaC on 9/1/2017.
 */
@Component
public class SetDatabaseInformationForRoleBeforeUpdateListener extends BaseBeforeUpdateListener<AelRole>{

    @Autowired
    private AelRoleService aelRoleService;

    @Override
    public void onApplicationEvent(BeforeUpdateEvent<AelRole> event) {
        AelRole aelRole = event.getDomainModel();
        AelRole dbAelRole = event.getStoredDomainModel();

        aelRoleService.validateRole(aelRole);

        aelRole.setReadOnly(dbAelRole.getReadOnly());
        aelRole.setImported(dbAelRole.getImported());
//        aelRole.setGroups(dbAelRole.getGroups());
        aelRole.setTenant(dbAelRole.getTenant());

    }
}
