package ro.siveco.ael.lcms.domain.metadata.model;

/**
 * Created by IuliaP on 13.06.2017.
 */
public enum EntityType {

    LEARNING_MATERIAL, COURSE, USER;
}
