package ro.siveco.ael.lcms.domain.profile.model;

import javax.persistence.*;


/**
 * Created by CatalinS on 19.06.2017.
 */
@Entity
@Table(name = "user_characteristic_tabs")
@SequenceGenerator(name = "user_characteristic_tabs_seq", sequenceName = "user_characteristic_tabs_seq")
public class UserCharacteristicTab extends ro.siveco.ram.starter.model.base.BaseEntity {

	private Characteristic tab;
    private Boolean active = Boolean.TRUE;
    

    public UserCharacteristicTab() { }

    public UserCharacteristicTab(Characteristic characteristic) {
    	this.tab = characteristic;
	}

	@Id
    @Column
    @GeneratedValue(generator = "user_characteristic_tabs_seq", strategy = GenerationType.AUTO)
    public Long getId()
    {
		return super.getId();
    }

    public void setId( Long id )
    {
    	super.setId(id);
    }

    @ManyToOne
    @JoinColumn(name = "tab_id", nullable = false)
    public Characteristic getTab() {
		return tab;
	}

	public void setTab(Characteristic tab) {
		this.tab = tab;
	}

	@Column(name = "active", nullable = false )
	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

}
