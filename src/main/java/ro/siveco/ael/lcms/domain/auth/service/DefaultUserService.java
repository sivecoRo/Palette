package ro.siveco.ael.lcms.domain.auth.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.social.connect.UserProfile;
import org.springframework.stereotype.Service;
import ro.siveco.ael.lcms.domain.auth.model.AelGroup;
import ro.siveco.ael.lcms.domain.auth.model.AelUser;
import ro.siveco.ael.lcms.domain.auth.model.User;
import ro.siveco.ael.lcms.domain.notification.model.ApplicationUserMessageFormat;
import ro.siveco.ael.lcms.domain.notification.model.UserMessageType;
import ro.siveco.ael.lcms.domain.notification.service.NotificationManagerService;
import ro.siveco.ael.lcms.domain.notification.service.UserMessageFormatService;
import ro.siveco.ael.lcms.domain.notification.service.UserMessageService;
import ro.siveco.ael.lcms.domain.organization.model.Component;
import ro.siveco.ael.lcms.domain.organization.model.ComponentUserJob;
import ro.siveco.ael.lcms.domain.organization.model.JobTitle;
import ro.siveco.ael.lcms.domain.organization.service.ComponentService;
import ro.siveco.ael.lcms.domain.organization.service.ComponentUserJobService;
import ro.siveco.ael.lcms.domain.organization.service.JobTitleService;
import ro.siveco.ael.lcms.domain.preference.model.Preference;
import ro.siveco.ael.lcms.domain.preference.model.Preferences;
import ro.siveco.ael.lcms.domain.preference.service.PreferencesService;
import ro.siveco.ael.lcms.domain.profile.model.UserDetails;
import ro.siveco.ael.lcms.repository.AelUserRepository;
import ro.siveco.ael.lcms.repository.UserDetailsRepository;
import ro.siveco.ael.service.BaseEmailService;
import ro.siveco.ael.service.mail.EmailService;
import ro.siveco.ael.service.security.annotation.Groups;
import ro.siveco.ael.service.utils.GuidUtils;
import ro.siveco.ael.service.utils.PasswordGeneratorHelper;
import ro.siveco.ram.starter.service.BaseEntityService;

import java.util.*;
import java.util.logging.Level;

/**
 * User: AlexandruVi
 * Date: 2017-02-20
 */
@SuppressWarnings("deprecation")
@Service
public class DefaultUserService extends BaseEntityService<AelUser> implements UserService {
    private PasswordEncoder passwordEncoder = new ShaPasswordEncoder();

    private AelGroupService aelGroupService;
    private ComponentService componentService;
    private JobTitleService jobTitleService;
    private ComponentUserJobService componentUserJobService;
    private NotificationManagerService notificationManagerService;
    private EmailService emailService;
    private AelUserRepository aelUserRepository;
    private MessageSource messageSource;
    private PreferencesService preferencesService;
    private UserDetailsRepository userDetailsRepository;

    private UserMessageService userMessageService;

    private UserMessageFormatService userMessageFormatService;
    private AelUserService aelUserService;
    private BaseEmailService baseEmailService;


    @Autowired
    public DefaultUserService(AelUserRepository resourceRepository, MessageSource messageSource,
                              AelGroupService aelGroupService, ComponentService componentService,
                              JobTitleService jobTitleService,
                              ComponentUserJobService componentUserJobService,
                              NotificationManagerService notificationManagerService,
                              EmailService emailService, AelUserRepository aelUserRepository,
                              PreferencesService preferencesService,
                              UserDetailsRepository userDetailsRepository, UserMessageService userMessageService,
                              UserMessageFormatService userMessageFormatService,
                              AelUserService aelUserService, BaseEmailService baseEmailService) {
        super(AelUser.class, resourceRepository);
        this.aelGroupService = aelGroupService;
        this.componentService = componentService;
        this.jobTitleService = jobTitleService;
        this.componentUserJobService = componentUserJobService;
        this.notificationManagerService = notificationManagerService;
        this.emailService = emailService;
        this.aelUserRepository = aelUserRepository;
        this.messageSource = messageSource;
        this.preferencesService = preferencesService;
        this.userDetailsRepository = userDetailsRepository;
        this.userMessageService = userMessageService;
        this.userMessageFormatService = userMessageFormatService;
        this.aelUserService = aelUserService;
        this.baseEmailService = baseEmailService;
    }

    public Optional<User> getUserByUsername(String username) {
        return ((AelUserRepository) getResourceRepository()).findOneByUsername(username);
    }

    public User getByRelatedUid(String relatedUid) {
        return ((AelUserRepository) getResourceRepository()).findOneByRelatedUid(relatedUid);
    }

    public User findOneByUserSocialId(String userSocialId) {
        return ((AelUserRepository) getResourceRepository()).findOneByUserSocialId(userSocialId);
    }

    public User findOneByEmail(String email) {
        return ((AelUserRepository) getResourceRepository()).findOneByEmail(email);
    }

    public List<AelUser> findByGroupName(String groupName) {
        return ((AelUserRepository) getResourceRepository()).getUsersByGroup(groupName);
    }

    public AelGroup getGroupByName(String name) {
        return aelGroupService.getByName(name);
    }

    public AelUser saveOrUpdate(AelUser user) {
        if (user.getId() == null) {
            return save(user);
        } else {
            return update(user);
        }
    }

    @Override
    public User createSocialUser(String userSocialId, UserProfile profile) {

        String email = profile.getEmail();
        AelUser user = (AelUser) findOneByEmail(email);
        if (user == null) {
            user = buildUser(profile);
        }
        if (user.getUserSocialId() == null) {
            user.setUserSocialId(userSocialId);
            saveOrUpdate(user);
        }

        return user;
    }

    public AelUser createComponentUserJobForUser(AelUser user) {
        return createComponentUserJobForUser(user,"Student");
    }

    public AelUser createComponentUserJobForUser(AelUser user, String jobTitleName) {
        Component defaultComponent = componentService.getRootComponent();
        JobTitle defaultUserJobTitle = jobTitleService.getByName(jobTitleName);

        ComponentUserJob componentUserJob = new ComponentUserJob();
        componentUserJob.setComponent(defaultComponent);
        componentUserJob.setUser(user);
        componentUserJob.setJobTitle(defaultUserJobTitle);
        componentUserJob.setActive(Boolean.TRUE);
        componentUserJob.setImported(Boolean.FALSE);
        componentUserJob = componentUserJobService.saveOrUpdate(componentUserJob);
        return user;
    }


    private AelUser buildUser(UserProfile profile) {
        AelUser user = new AelUser();
        user.setFirstName(profile.getFirstName());
        user.setLastName(profile.getLastName());
        user.setDisplayName(profile.getFirstName() + " " + ( (profile.getLastName() == null || profile.getLastName().isEmpty() || profile.getLastName().trim().isEmpty()) ? "" : profile.getLastName()));
        user.setEmail(profile.getEmail());

        GuidUtils guid = new GuidUtils();
        user.setRelatedUid(guid.generateGuid());

        user.setUsername(profile.getEmail());
        user.setPassword("-");
        user.setDisabled(false);
        user.setReadOnly(false);
        user.setImported(false);
        user.setCreateDate(new Date());

        saveOrUpdate(user);

        AelGroup everybodyGroup = getGroupByName(Groups.Default.Everybody.get());
        AelGroup studentsGroup = getGroupByName(Groups.Default.Students.get());
        user.setGroups(Arrays.asList(everybodyGroup, studentsGroup));
        saveOrUpdate(user);

        user = createComponentUserJobForUser(user);
        return user;
    }

    public void createUserAccount(AelUser user) {
        AelGroup everybodyGroup = getGroupByName(Groups.Default.Everybody.get());
        AelGroup studentsGroup = getGroupByName(Groups.Default.Students.get());
        user.setGroups(Arrays.asList(everybodyGroup, studentsGroup));
        user.setDisabled(Boolean.TRUE);

        String registrationId = PasswordGeneratorHelper.generatePassword(20);
        user.setRegistrationId(passwordEncoder.encodePassword(registrationId, null));
        user.setUsername(user.getEmail());
        user.setDisplayName(aelUserService.getUserDisplayName(user));

        Component defaultComponent = componentService.getRootComponent();
        JobTitle defaultUserJobTitle = jobTitleService.getByName("Student");

        UserDetails userDetails = new UserDetails();
        userDetails.setUser(user);
        userDetailsRepository.saveAndFlush(userDetails);


        ComponentUserJob componentUserJob = new ComponentUserJob();
        componentUserJob.setComponent(defaultComponent);
        componentUserJob.setUser(user);
        componentUserJob.setJobTitle(defaultUserJobTitle);
        componentUserJob.setActive(Boolean.TRUE);
        componentUserJob.setImported(Boolean.FALSE);
        componentUserJob = componentUserJobService.saveOrUpdate(componentUserJob);

        List<AelUser> adminGroup = aelUserRepository.getUsersByGroup(Groups.Default.Administrators.get());
        List<AelUser> admins = new ArrayList<AelUser>();

        for (AelUser adm : adminGroup) {
            admins.add(aelUserRepository.findOne(adm.getId()));
        }

        notificationManagerService.notifyUser(aelUserRepository.findOne(user.getId()), admins, null,
                new Object[]{user.getDisplayName(), "Student"},
                UserMessageType.GENERIC_MESSAGE, ApplicationUserMessageFormat.CREATED_NEW_USER_MESSAGE);


        String entityId = "" + user.getUsername() + "/" + registrationId;
        sendMailToUser(user, entityId, ApplicationUserMessageFormat.ACTIVATE_ACCOUNT_NOTIFICATION,
                UserMessageType.GENERIC_MESSAGE);
    }

    private void sendMailToUser(AelUser user, String entityId, ApplicationUserMessageFormat userMessageFormat,
                                UserMessageType userMessageType) {

        Locale locale = baseEmailService.getUserPreferredLocale(user);

        String subject = messageSource.getMessage(userMessageFormat.toString() + ".subject", null, locale);

        String localizedBodyText = getMessageSource().getMessage(userMessageFormat.toString() + ".body",
                                                                 new Object[]{entityId}, null, locale);

        try {
            // Wrap the message into layout before sending.
            String body = baseEmailService.putMessageIntoLayout(user, localizedBodyText);

            emailService.sendEmail(user, subject, body);
        } catch (Throwable e) {
            getLogger().log(Level.SEVERE, "Error sending email to the user: " + e.getMessage(), e);
        }
    }

    private AelUser saveUser(AelUser user) {
        user.setConfirmPassword(null);

        GuidUtils guid = new GuidUtils();
        user.setRelatedUid(guid.generateGuid());
        user.setPassword(passwordEncoder.encodePassword(user.getPassword(), null));

        AelGroup everybodyGroup = getGroupByName(Groups.Default.Everybody.get());

        if (user.getGroups().size() != 0) {
            List<AelGroup> userGroups = user.getGroups();
            if (everybodyGroup != null) {
                userGroups.add(everybodyGroup);
            }
            user.setGroups(userGroups);
        } else {
            user.setGroups(Arrays.asList(everybodyGroup));
        }

        return saveOrUpdate(user);
    }

    public boolean existsEmail(String email) {

        User user = aelUserRepository.getByMailAddress(email);

        if (user != null) {
            return true;
        }

        return false;
    }

    public void changePassword(AelUser aelUser, String password) {

        aelUser.setConfirmPassword(null);
        aelUser.setPassword(passwordEncoder.encodePassword(password, null));

        saveOrUpdate(aelUser);
    }

    public void resetPassword(String email) {

        User user = aelUserRepository.getByMailAddress(email);
        AelUser aelUser = aelUserRepository.findOne(user.getId());

        String token = passwordEncoder.encodePassword(aelUser.getRelatedUid(), null);
        String entityId = "" + aelUser.getEmail() + "/" + token;

        sendMailToUser(aelUser, entityId, ApplicationUserMessageFormat.RESET_PASSWORD_NOTIFICATION,
                UserMessageType.GENERIC_MESSAGE);
    }

    public void resetPasswordForMailAndToken(String email, String resetToken) {

        User user = aelUserRepository.getByMailAddress(email);

        if (user != null) {
            AelUser aelUser = aelUserRepository.findOne(user.getId());
            String hash = passwordEncoder.encodePassword(aelUser.getRelatedUid(), null);

            if (hash.equals(resetToken))
            {
                String newPassword = PasswordGeneratorHelper.generatePassword(8);
                aelUser.setPassword(passwordEncoder.encodePassword(newPassword, null));
                saveOrUpdate(aelUser);

                String entityId = "" + newPassword;

                sendMailToUser(aelUser, entityId, ApplicationUserMessageFormat.NEW_PASSWORD_NOTIFICATION,
                        UserMessageType.GENERIC_MESSAGE);
            }
        }
    }

    public void activateAccount(String username, String registrationId) {

        Optional<User> user = aelUserRepository.findOneByUsername(username);

        if (user != null && !user.equals(Optional.empty())) {
            AelUser aelUser = aelUserRepository.findOne(user.get().getId());
            String hashedRegistrationId = passwordEncoder.encodePassword(registrationId, null);

            if (aelUser != null && aelUser.getRegistrationId() != null && aelUser.getDisabled() &&
                    aelUser.getRegistrationId().equals(hashedRegistrationId)) {
                aelUser.setDisabled(Boolean.FALSE);
                //    setChangePasswordOnFirstUse(aelUser);

                saveOrUpdate(aelUser);
            }
        }
    }

    public Boolean checkMailAndTokenBeforeResetPassword(String email, String resetToken) {

        User user = aelUserRepository.getByMailAddress(email);

        if (user != null) {
            AelUser aelUser = aelUserRepository.findOne(user.getId());
            String hash = passwordEncoder.encodePassword(aelUser.getRelatedUid(), null);

            if (hash.equals(resetToken))
            {
                return true;
            }
        }

        return false;
    }

    public void reset(String email, String password) {

        User user = aelUserRepository.getByMailAddress(email);

        if (user != null) {
            AelUser aelUser = aelUserRepository.findOne(user.getId());
            changePassword(aelUser, password);
        }
    }

    public void resetAndSendPassword(String email) {

        User user = aelUserRepository.getByMailAddress(email);
        AelUser aelUser = aelUserRepository.findOne(user.getId());

        String newPassword = PasswordGeneratorHelper.generatePassword(8);
        newPassword = newPassword + "1Aa";
        aelUser.setPassword(passwordEncoder.encodePassword(newPassword, null));
        saveOrUpdate(aelUser);

        String entityId = "" + newPassword;

        sendMailToUser(aelUser, entityId, ApplicationUserMessageFormat.NEW_PASSWORD_NOTIFICATION,
                UserMessageType.GENERIC_MESSAGE);
    }
}
