package ro.siveco.ael.lcms.domain.auth.model;

import org.springframework.security.core.CredentialsContainer;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

/**
 * User: AlexandruVi
 * Date: 2017-02-21
 */
public interface AuthenticatedUser extends UserDetails, CredentialsContainer {

    User getUser();

    Collection<? extends GrantedAuthority> getPermissions();

    boolean hasRole(String roleName);

    boolean hasRole(Role role);
}
