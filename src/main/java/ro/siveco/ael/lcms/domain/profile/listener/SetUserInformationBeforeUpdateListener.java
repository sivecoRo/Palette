package ro.siveco.ael.lcms.domain.profile.listener;

import org.apache.batik.transcoder.TranscoderException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.auth.model.AelUser;
import ro.siveco.ael.lcms.domain.auth.service.AelUserService;
import ro.siveco.ael.lcms.domain.metadata.service.MetadataService;
import ro.siveco.ael.lcms.domain.preference.model.Preference;
import ro.siveco.ael.lcms.domain.preference.model.Preferences;
import ro.siveco.ael.lcms.domain.preference.service.PreferencesService;
import ro.siveco.ael.lcms.domain.profile.model.CountryDTO;
import ro.siveco.ael.lcms.domain.profile.model.UserDetails;
import ro.siveco.ael.lcms.domain.profile.service.UserDetailsService;
import ro.siveco.ael.service.MultimediaService;
import ro.siveco.ram.event.listener.type.single.BaseBeforeUpdateListener;
import ro.siveco.ram.event.type.single.BeforeUpdateEvent;

import java.io.ByteArrayOutputStream;

/**
 * User: LucianR
 * Date: 2017-10-25
 */
@Order(value = 1)
@Component
public class SetUserInformationBeforeUpdateListener extends BaseBeforeUpdateListener<UserDetails> {

    private final UserDetailsService userDetailsService;

    private final AelUserService aelUserService;

    private final MetadataService metadataService;

    private final MultimediaService multimediaService;

    private final PreferencesService preferencesService;

    @Autowired
    public SetUserInformationBeforeUpdateListener(
            UserDetailsService userDetailsService,
            AelUserService aelUserService,
            MetadataService metadataService,
            MultimediaService multimediaService,
            PreferencesService preferencesService) {

        this.userDetailsService = userDetailsService;
        this.aelUserService = aelUserService;
        this.metadataService = metadataService;
        this.multimediaService = multimediaService;
        this.preferencesService = preferencesService;
    }

    @Override
    public void onApplicationEvent(BeforeUpdateEvent<UserDetails> event) {

        UserDetails userDetails = event.getDomainModel();
        userDetailsService.validateUserDetails(userDetails);
        AelUser aelUser = userDetailsService.getUserById(userDetails.getUser().getId());
        aelUser.setLastName(userDetails.getLastName());
        aelUser.setFirstName(userDetails.getFirstName());
        aelUser.setDisplayName(aelUserService.getUserDisplayName(aelUser));
        aelUser.setUsername(userDetails.getUsername());
        aelUser.setEmail(userDetails.getEmail());
        aelUser.setPhoneNo(userDetails.getPhoneNo());
        CountryDTO countryDTO = new CountryDTO();
        countryDTO.setName(userDetails.getCountry() == null ? null : userDetails.getCountry().getCountryName());
        countryDTO.setCode(userDetails.getCountry() == null ? null : userDetails.getCountry().getCode());
        aelUser.setCountry(userDetails.getCountry() == null ? null : countryDTO);
        aelUser.setCity(userDetails.getCity());
        aelUser.setGender(userDetails.getGender());

        String newPassword = userDetailsService.validateNewPassword(userDetails);
        if (newPassword != null) {
            aelUser.setConfirmPassword(null);
            aelUser.setPassword(newPassword);
        }

        if (userDetails.getImageMultipart() != null && userDetails.getImageMultipart().length > 0) {
            try {
                ByteArrayOutputStream baos = multimediaService.processThumbnail(userDetails.getImageMultipart()[0]);
                userDetails.setImage(baos.toByteArray());
            } catch (TranscoderException e) {
                e.printStackTrace();
            }
        } else {
            userDetails.setImage(event.getStoredDomainModel().getImage());
        }
        aelUserService.save(aelUser);

        metadataService.updateMetadata(userDetails);

        saveNotificationsPreferences(userDetails);

    }

    private void saveNotificationsPreferences(UserDetails userDetails) {
        Preference preference = preferencesService.getByNameAndUser(Preferences.SEND_EMAIL.get(), userDetails.getUser());
        String preferenceValue = userDetails.getSendEmail() == null ? Boolean.FALSE.toString() : userDetails.getSendEmail().toString();
        if (preference != null) {
            preference.setValue(preferenceValue);
        }
        else {
            preference = new Preference();
            preference.setUser(userDetails.getUser());
            preference.setName(Preferences.SEND_EMAIL.get());
            preference.setValue(Boolean.TRUE.toString());
            preference.setGlobal(false);
        }
        preferencesService.save(preference);
    }

}
