package ro.siveco.ael.lcms.domain.auth.model;

import org.springframework.data.domain.Sort;
import ro.siveco.ram.starter.ui.config.PropertyViewDescriptor;
import ro.siveco.ram.starter.ui.config.Sorter;
import ro.siveco.ram.starter.ui.config.ViewDescriptor;
import ro.siveco.ram.starter.ui.model.IndexViewType;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Stores information about user profile verification tokens.
 * @author maciejb
 */
@Entity
@Table(name="profile_verifications")
@SequenceGenerator(name = "profile_verifications_seq", sequenceName = "profile_verifications_seq")
@ViewDescriptor(pageSize = 10, viewTypes = {IndexViewType.GRID}, searchEnabled = true, defaultSearchProperty = "id",
        sort = {@Sorter(propertyName = "id", direction = Sort.Direction.ASC)})
public class ProfileVerification extends ro.siveco.ram.starter.model.base.BaseEntity{

    private AelUser user;

    private Long id;

    private String token;
    
    @OneToOne
    @JoinColumn(name = "user_id", nullable = false)
    @NotNull
    @PropertyViewDescriptor(hidden = true)
    public AelUser getUser() {
        return user;
    }

    public void setUser(AelUser user) {
        this.user = user;
    }

    @Id
    @GeneratedValue(generator = "profile_verifications_seq", strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "token", nullable = false)
    @NotNull
    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

}
