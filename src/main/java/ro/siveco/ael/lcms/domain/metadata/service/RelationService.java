package ro.siveco.ael.lcms.domain.metadata.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.siveco.ael.lcms.domain.metadata.model.Relation;
import ro.siveco.ael.lcms.repository.RelationRepository;
import ro.siveco.ram.app.exception.AppException;
import ro.siveco.ram.starter.service.BaseEntityService;

import java.util.List;

/**
 * Created by IuliaP on 15.06.2017.
 */
@Service
public class RelationService extends BaseEntityService<Relation> {

        @Autowired
        public RelationService(RelationRepository relationRepository) {
                super(Relation.class, relationRepository);
        }

        public void validateRelation(Relation relation) {
                if( relation != null ) {
                        List<Relation> relations = ((RelationRepository)getResourceRepository()).findAllByName( relation.getName() );
                        if( relations != null && relations.size() > 0 ) {
                                throw new AppException("error.relation.duplicateName");
                        }
                }
        }

        public List<Relation> findAllByName(String name) {
                return ((RelationRepository)getResourceRepository()).findAllByName(name);
        }
}