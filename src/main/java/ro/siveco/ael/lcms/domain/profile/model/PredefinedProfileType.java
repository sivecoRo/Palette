package ro.siveco.ael.lcms.domain.profile.model;

/**
 * User: AlexandruVi
 * Date: 2017-01-09
 */
public enum PredefinedProfileType implements ProfileType {
    BASIC,
    DISABILITIES,
    PEDAGOGICAL,
    MULTISENSORIAL_AND_AFFECTIVE,
    OPERATIONAL;

    /**
     * @return The name
     */
    @Override
    public String getName() {
        return this.name();
    }

    @Override
    public void setName(String name) {

    }
}
