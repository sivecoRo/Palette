package ro.siveco.ael.lcms.domain.notification.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.notification.model.PlannedNotification;
import ro.siveco.ael.lcms.domain.notification.service.PlannedNotificationService;
import ro.siveco.ram.event.listener.type.single.BaseBeforeDeleteListener;
import ro.siveco.ram.event.type.single.BeforeDeleteEvent;

/**
 * Created by IuliaP on 29.09.2017.
 */
@Component
public class ValidatePlannedNotificationBeforeDeleteListener extends BaseBeforeDeleteListener<PlannedNotification> {

    @Autowired
    PlannedNotificationService plannedNotificationService;

    @Override
    public void onApplicationEvent(BeforeDeleteEvent<PlannedNotification> event) {

        PlannedNotification plannedNotification = event.getDomainModel();
        plannedNotificationService
                .validateBeforeCurrentDate(plannedNotification, "error.plannedNotification.date.beforeCurrentDate");
        plannedNotificationService.validateNotificationSchedule(plannedNotification,
                "error.plannedNotification.hasSentNotificationSchedule");
        plannedNotificationService.deleteNotificationSchedule( plannedNotification.getId() );
    }
}
