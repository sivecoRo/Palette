package ro.siveco.ael.lcms.domain.course.model;

/**
 * Copyright: Copyright (c) 2006
 * Company: SIVECO Romania SA
 *
 * @author RobertR
 * @version : 1.0 / Dec 8, 2006
 */
public enum PlannedCourseStatus
{
	SCHEDULED("scheduled"),
    RUNNING("running"),
	FINISHED("finished");


	PlannedCourseStatus(String name)
	{
		this.name = name;
	}

	
	public String getName()
	{
		return name;
	}


	private String name;
}
