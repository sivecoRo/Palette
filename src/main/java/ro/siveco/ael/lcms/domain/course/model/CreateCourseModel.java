package ro.siveco.ael.lcms.domain.course.model;

import java.math.BigDecimal;
import java.util.List;


public class CreateCourseModel {

	private Long id;
	private String name;
	private String description;
    private Long gradingTypeId;
	private BigDecimal passingGrade;
    private Long programId;
    private Boolean enforceOrderedLearning;
	private Boolean publishToStore;
	private List<Long> materials;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Long getGradingTypeId() {
		return gradingTypeId;
	}
	public void setGradingTypeId(Long gradingTypeId) {
		this.gradingTypeId = gradingTypeId;
	}
	public BigDecimal getPassingGrade() {
		return passingGrade;
	}
	public void setPassingGrade(BigDecimal passingGrade) {
		this.passingGrade = passingGrade;
	}
	public Long getProgramId() {
		return programId;
	}
	public void setProgramId(Long programId) {
		this.programId = programId;
	}
	public Boolean getEnforceOrderedLearning() {
		return enforceOrderedLearning;
	}
	public void setEnforceOrderedLearning(Boolean enforceOrderedLearning) {
		this.enforceOrderedLearning = enforceOrderedLearning;
	}
	public Boolean getPublishToStore() {
		return publishToStore;
	}
	public void setPublishToStore(Boolean publishToStore) {
		this.publishToStore = publishToStore;
	}
	@Override
	public String toString() {
		return "CreateCourseModel [id=" + id + ", name=" + name + ", description=" + description + ", gradingTypeId="
				+ gradingTypeId + ", passingGrade=" + passingGrade + ", programId=" + programId
				+ ", enforceOrderedLearning=" + enforceOrderedLearning + ", publishToStore=" + publishToStore + "]";
	}
	public List<Long> getMaterials() {
		return materials;
	}
	public void setMaterials(List<Long> materials) {
		this.materials = materials;
	}
}
