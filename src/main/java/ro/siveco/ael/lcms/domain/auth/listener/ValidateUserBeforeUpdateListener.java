package ro.siveco.ael.lcms.domain.auth.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.auth.model.AelUser;
import ro.siveco.ael.lcms.domain.auth.service.AelUserService;
import ro.siveco.ram.event.listener.type.single.BaseBeforeUpdateListener;
import ro.siveco.ram.event.type.single.BeforeUpdateEvent;

/**
 * Created by AndradaC on 9/1/2017.
 */
@Order(value = 1)
@Component
public class ValidateUserBeforeUpdateListener extends BaseBeforeUpdateListener<AelUser>{

    @Autowired
    private AelUserService aelUserService;

    @Override
    public void onApplicationEvent(BeforeUpdateEvent<AelUser> event) {
        AelUser aelUser = event.getDomainModel();
        aelUserService.validateUserForUpdate(aelUser);
    }
}
