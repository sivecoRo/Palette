package ro.siveco.ael.lcms.domain.notification.model;


import ro.siveco.ael.domain.annotations.ColumnComment;
import ro.siveco.ael.lcms.domain.auth.model.AelUser;

import javax.persistence.*;

/**
 * Created with IntelliJ IDEA.
 * User: RazvanS
 * Date: 8/6/14
 * Time: 11:25 AM
 */
@Entity
@Table( name = "notification_preference" )
@SequenceGenerator( name = "notification_preference_seq", sequenceName = "notification_preference_seq" )
public class NotificationPreference extends ro.siveco.ram.starter.model.base.BaseEntity
{
	private Long id;

	@ColumnComment( comment = "The user id" )
	private AelUser user;

	@ColumnComment( comment = "The user message type" )
	private UserMessageType userMessageType;

	@ColumnComment( comment = "The value of the preference" )
	private String value;

	@Id
	@GeneratedValue( generator = "notification_preference_seq", strategy = GenerationType.AUTO )
	public Long getId()
	{
		return id;
	}

	public void setId( Long id )
	{
		this.id = id;
	}

	@ManyToOne
	@JoinColumn( name = "user_id", nullable = false )
	public AelUser getUser()
	{
		return user;
	}

	public void setUser( AelUser user )
	{
		this.user = user;
	}

	@Column( name = "user_message_type", nullable = false )
	public UserMessageType getUserMessageType()
	{
		return userMessageType;
	}

	public void setUserMessageType( UserMessageType userMessageType )
	{
		this.userMessageType = userMessageType;
	}

	@Column( name = "value", nullable = false )
	public String getValue()
	{
		return value;
	}

	public void setValue( String value )
	{
		this.value = value;
	}
}
