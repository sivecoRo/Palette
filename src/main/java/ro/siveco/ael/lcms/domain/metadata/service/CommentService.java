package ro.siveco.ael.lcms.domain.metadata.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.siveco.ael.lcms.domain.auth.model.AelUser;
import ro.siveco.ael.lcms.domain.course.model.PlannedCourse;
import ro.siveco.ael.lcms.domain.metadata.model.Comment;
import ro.siveco.ael.lcms.domain.profile.model.UserDetails;
import ro.siveco.ael.lcms.domain.profile.service.UserDetailsService;
import ro.siveco.ael.lcms.repository.CommentRepository;
import ro.siveco.ael.service.ImageService;
import ro.siveco.ael.service.utils.DatesService;
import ro.siveco.ael.web.controllers.WithUserBaseEntityController;
import ro.siveco.ram.app.exception.AppException;
import ro.siveco.ram.starter.service.BaseEntityService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by albertb on 2/23/2018.
 */

@Service
public class CommentService extends BaseEntityService<Comment> {
    @Autowired
    public CommentService(CommentRepository commentRepository) {
        super(Comment.class, commentRepository);
    }

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private DatesService datesService;

    @Autowired
    private ImageService imageService;

    public List<Comment> getAllComments()
    {
        return ((CommentRepository)getResourceRepository()).findAll();
    }

    public List<Comment> findByPlannedCourseId(Long plannedCourseId)
    {
        return ((CommentRepository)getResourceRepository()).findByPlannedCourseId(plannedCourseId);
    }

    public List<Comment> findByCourseId(Long courseId)
    {
        return ((CommentRepository)getResourceRepository()).findByCourseId(courseId);
    }

    public List<Comment> findByUserId(Long userId)
    {
        return ((CommentRepository)getResourceRepository()).findByUserId(userId);
    }


    public List<Comment> getCourseCommentDetails(Long courseId) {
        List<Comment> comments = new ArrayList<>();
        if (courseId != null) {
            comments = findByCourseId(courseId);
        }
        return getCommentDetails(comments);
    }

    public List<Comment> getCommentDetails(Long plannedCourseId)
    {
        List<Comment> cList=findByPlannedCourseId(plannedCourseId);
        return getCommentDetails(cList);
    }

    private List<Comment> getCommentDetails(List<Comment> cList) {
        AelUser currentUser = (AelUser)WithUserBaseEntityController.getUser();
        if (cList != null) {
            for (Comment comment : cList) {
                AelUser commentOwner = comment.getAelUser();
                comment.setUserDisplayName(commentOwner.getDisplayName());
                if (comment.getCommentDate() != null) {
                    comment.setFormattedDate(datesService.formatCommentDateTime(comment.getCommentDate()));
                }
                UserDetails itemOwnerDetails = userDetailsService.getBasicData(commentOwner.getId());
                if (itemOwnerDetails.getImage() != null && itemOwnerDetails.getImage().length > 0) {
                    comment.setUserImage(imageService.imageSrcFromBytes(itemOwnerDetails.getImage()));
                }
                comment.setRemovable(currentUser.getId().equals(commentOwner.getId()));
            }
        }
        return cList;
    }

    public void saveComment(PlannedCourse plannedCourse, String commentText){
        AelUser currentUser=(AelUser) WithUserBaseEntityController.getUser();
        if(currentUser==null)
        {
            throw new AppException("error.comment.user.null");
        }
        else if(plannedCourse==null)
        {
            throw new AppException("error.comment.item.null");
        }
        else{
                Comment comment= new Comment();
                comment.setCommentDate(new Date());
                comment.setPlannedCourse(plannedCourse);
                comment.setAelUser(currentUser);
                comment.setCommentText(commentText);
                save(comment);
        }
    }

    public void removeByPlannedCourseId(Long plannedCourseId) {

        List<Comment> commentsByPlannedCourseId = findByPlannedCourseId(plannedCourseId);

        for(Comment comment : commentsByPlannedCourseId) {
            super.remove(comment);
        }
    }

    public void removeByUserId(Long userId) {

        List<Comment> commentsByUserId = findByUserId(userId);

        for(Comment comment : commentsByUserId) {
            super.remove(comment);
        }
    }

}
