package ro.siveco.ael.lcms.domain.organization.listener;


import org.springframework.core.annotation.Order;
import ro.siveco.ael.lcms.domain.organization.model.Component;
import ro.siveco.ael.lcms.domain.tenant.model.Tenant;
import ro.siveco.ael.web.controllers.WithUserBaseEntityController;
import ro.siveco.ram.event.listener.type.single.BaseBeforeCreateListener;
import ro.siveco.ram.event.type.single.BeforeCreateEvent;

import java.util.UUID;

/**
 * Created by AndradaC on 9/1/2017.
 */
@Order(value = 1)
@org.springframework.stereotype.Component
public class SetDefaultInformationOnComponentBeforeCreateListener extends BaseBeforeCreateListener<Component>{

    @Override
    public void onApplicationEvent(BeforeCreateEvent<Component> event) {
        Component component = event.getDomainModel();
        Tenant tenant = WithUserBaseEntityController.getUser().getTenant();
        component.setRelatedEntityId(UUID.randomUUID().toString());
        component.setCompleteName(component.getName());
        component.setTenant(tenant);
    }
}
