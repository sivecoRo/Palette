package ro.siveco.ael.lcms.domain.profile.model;

/**
 * User: AlexandruVi
 * Date: 2017-01-09
 */
public enum PredefinedProfileCategory implements ProfileCategory {
    DEMOGRAPHIC_DATA(PredefinedProfileType.BASIC);

    private ProfileType type;

    PredefinedProfileCategory(ProfileType type) {
        this.type = type;
    }

    /**
     * @return The name
     */
    @Override
    public String getName() {
        return this.name();
    }

    @Override
    public void setName(String name) {

    }

    /**
     * @return The type of this category
     */
    public ProfileType getType() {
        return this.type;
    }
}
