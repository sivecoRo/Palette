package ro.siveco.ael.lcms.domain.mail.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

/**
 * Created by IntelliJ IDEA.
 * User: laurentium
 * Date: 1/8/13
 * Time: 11:11 AM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "mail_cfg_attributes")
@SequenceGenerator(name = "mail_cfg_attributes_seq", sequenceName = "mail_cfg_attributes_seq")
public class MailConfigurationAttributes {
    private Long id;
    private String cheie;
    private String value;
    @JsonIgnore
    private MailConfiguration parentMailConfiguration;

    @Id
    @GeneratedValue(generator = "mail_cfg_attributes_seq", strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

	@Column(name = "cheie", length = 250, nullable = false)
	public String getCheie()
	{
		return cheie;
	}

	public void setCheie( String cheie )
	{
		this.cheie = cheie;
	}

    @Column(name = "value", length = 250, nullable = false)
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @ManyToOne
    @JoinColumn(name = "parent_mail_cfg_id", nullable = false)
    public MailConfiguration getParentMailConfiguration() {
        return parentMailConfiguration;
    }

    public void setParentMailConfiguration(MailConfiguration parentMailConfiguration) {
        this.parentMailConfiguration = parentMailConfiguration;
    }
}
