package ro.siveco.ael.lcms.domain.metadata.model;

/**
 * Created by IuliaP on 19.06.2017.
 */
public interface Taggable {
    public EntityType getEntityType();
}
