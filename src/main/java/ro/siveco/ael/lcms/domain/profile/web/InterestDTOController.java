package ro.siveco.ael.lcms.domain.profile.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import ro.siveco.ael.lcms.domain.profile.model.InterestDTO;
import ro.siveco.ael.lcms.domain.profile.service.InterestDTOService;
import ro.siveco.ram.controller.annotation.AjaxStructuralMapping;
import ro.siveco.ram.controller.util.MappingUri;
import ro.siveco.ram.starter.controller.BaseDummyController;

/**
 * Created by albertb on 1/30/2018.
 */

@Controller
@RequestMapping("/interest")
public class InterestDTOController extends BaseDummyController<InterestDTO, String> {
    @Autowired
    public InterestDTOController(InterestDTOService service) {

        super(service);
    }

    @Override
    @AjaxStructuralMapping(path = MappingUri.INDEX_URI)
    public ResponseEntity<Page<?>> paginate(ModelMap model, Pageable pageable) {

        return super.paginate(model, pageable);
    }
}

