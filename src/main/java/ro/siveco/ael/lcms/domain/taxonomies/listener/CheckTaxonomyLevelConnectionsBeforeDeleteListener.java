package ro.siveco.ael.lcms.domain.taxonomies.listener;

import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.taxonomies.model.TaxonomyLevel;
import ro.siveco.ael.lcms.domain.taxonomies.service.TaxonomyLevelService;
import ro.siveco.ram.app.exception.AppException;
import ro.siveco.ram.event.listener.type.single.BaseBeforeDeleteListener;
import ro.siveco.ram.event.type.single.BeforeDeleteEvent;

/**
 * Created by AndradaC on 9/1/2017.
 */
@Component
public class CheckTaxonomyLevelConnectionsBeforeDeleteListener extends BaseBeforeDeleteListener<TaxonomyLevel>{

    @Override
    public void onApplicationEvent(BeforeDeleteEvent<TaxonomyLevel> event) {
        TaxonomyLevel taxonomyLevel = event.getDomainModel();
        TaxonomyLevelService taxonomyLevelService = (TaxonomyLevelService)event.getSource();
        String existentConnexionError = taxonomyLevelService.connexionExists(taxonomyLevel.getId());
        if(existentConnexionError != null) {
            throw new AppException(existentConnexionError);
        }
    }
}
