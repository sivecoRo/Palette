package ro.siveco.ael.lcms.domain.course.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.auth.model.AelUser;
import ro.siveco.ael.lcms.domain.course.model.Course;
import ro.siveco.ael.lcms.domain.course.model.PlannedCourse;
import ro.siveco.ael.lcms.domain.course.model.PlannedCourseStudent;
import ro.siveco.ael.lcms.domain.course.service.PlannedCourseService;
import ro.siveco.ael.lcms.domain.course.service.PlannedCourseStudentService;
import ro.siveco.ael.lcms.domain.metadata.model.Comment;
import ro.siveco.ael.lcms.domain.metadata.service.CommentService;
import ro.siveco.ram.event.listener.type.single.BaseBeforeDeleteListener;
import ro.siveco.ram.event.type.single.BeforeDeleteEvent;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class CourseBeforeDeleteListener extends BaseBeforeDeleteListener<Course>
{
    @Autowired
    private PlannedCourseService plannedCourseService;

    @Autowired
    private PlannedCourseStudentService plannedCourseStudentService;

    @Autowired
    private CommentService commentService;

    @Override
    @Transactional
    public void onApplicationEvent(BeforeDeleteEvent<Course> event) {
        Course course = event.getDomainModel();

        List<PlannedCourse> plannedCourses = plannedCourseService.findByCourseId(course.getId());
        if (plannedCourses != null && plannedCourses.size()>0)
        {
            PlannedCourse firstPlannedCourse = plannedCourses.get(0);
            List<PlannedCourseStudent> association = firstPlannedCourse.getPlannedCourseStudents();
            List<AelUser> attendees = new ArrayList<>();
            Date startDate = firstPlannedCourse.getStartDate();
            Date endDate = firstPlannedCourse.getEndDate();
            if(association != null && association.size() > 0)
            {
                attendees = association.stream()
                                        .filter(PlannedCourseStudent::getAttending)
                                        .map(PlannedCourseStudent::getUser).collect(Collectors.toList());
                plannedCourseStudentService.remove(association);
            }

            List<Comment> comments = commentService.findByPlannedCourseId(firstPlannedCourse.getId());
            if (comments != null && comments.size() > 0) {
                commentService.remove(comments);
            }
            plannedCourseService.remove(firstPlannedCourse.getId());

            plannedCourseService.notifyAttendeesOnDelete(attendees, course, startDate, endDate);
        }
    }
}
