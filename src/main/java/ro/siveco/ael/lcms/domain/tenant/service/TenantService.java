package ro.siveco.ael.lcms.domain.tenant.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ro.siveco.ael.lcms.domain.auth.model.AelGroup;
import ro.siveco.ael.lcms.domain.auth.model.AelUser;
import ro.siveco.ael.lcms.domain.auth.service.AelGroupService;
import ro.siveco.ael.lcms.domain.auth.service.UserService;
import ro.siveco.ael.lcms.domain.mail.model.MailConfiguration;
import ro.siveco.ael.lcms.domain.mail.model.MailConfigurationAttributes;
import ro.siveco.ael.lcms.domain.mail.service.MailConfigurationService;
import ro.siveco.ael.lcms.domain.organization.model.Component;
import ro.siveco.ael.lcms.domain.organization.model.ComponentType;
import ro.siveco.ael.lcms.domain.organization.model.ComponentUserJob;
import ro.siveco.ael.lcms.domain.organization.model.JobTitle;
import ro.siveco.ael.lcms.domain.organization.service.*;
import ro.siveco.ael.lcms.domain.preference.model.Preference;
import ro.siveco.ael.lcms.domain.preference.model.Preferences;
import ro.siveco.ael.lcms.domain.preference.service.PreferencesService;
import ro.siveco.ael.lcms.domain.tenant.model.Tenant;
import ro.siveco.ael.lcms.repository.TenantRepository;
import ro.siveco.ael.service.MultimediaService;
import ro.siveco.ael.service.utils.GuidUtils;
import ro.siveco.ael.service.utils.PasswordGeneratorHelper;
import ro.siveco.ram.starter.service.BaseEntityService;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;

/**
 * Created by andradac on 4/13/2017.
 */
@Service
public class TenantService extends BaseEntityService<Tenant> {

    private final PreferencesService preferencesService;

    private final MailConfigurationService mailConfigurationService;

    private final ComponentTypeService componentTypeService;

    private final ComponentTypeJobTitleService componentTypeJobTitleService;

    private final JobTitleService jobTitleService;

    private final ComponentService componentService;

    private final UserService userService;

    private final ComponentUserJobService componentUserJobService;

    private final AelGroupService aelGroupService;

    private final MultimediaService multimediaService;

    @Autowired
    private ShaPasswordEncoder passwordEncoder;

    @Autowired
    public TenantService(TenantRepository resourceRepository, PreferencesService preferencesService,
                         MailConfigurationService mailConfigurationService,
                         ComponentTypeService componentTypeService,
                         ComponentTypeJobTitleService componentTypeJobTitleService, JobTitleService jobTitleService,
                         ComponentService componentService, UserService userService,
                         ComponentUserJobService componentUserJobService, AelGroupService aelGroupService,
                         MultimediaService multimediaService) {

        super(Tenant.class, resourceRepository);
        this.preferencesService = preferencesService;
        this.mailConfigurationService = mailConfigurationService;
        this.componentTypeService = componentTypeService;
        this.componentTypeJobTitleService = componentTypeJobTitleService;
        this.jobTitleService = jobTitleService;
        this.componentService = componentService;
        this.userService = userService;
        this.componentUserJobService = componentUserJobService;
        this.aelGroupService = aelGroupService;
        this.multimediaService = multimediaService;
    }

    public Tenant findOneById(Long id) {
        return ((TenantRepository) getResourceRepository()).findOneById(id);
    }

    public Tenant saveTenant(Tenant tenant) {
        // 1. save base tenant entity
        tenant = save(tenant);

        // 2. save USER LOCALE and
        saveTenantLocalePreference(tenant);

        // 3. save MAIL CONFIGURATION
        saveTenantMailConfiguration(tenant);

        // 4. Create tenant component structure
        Component tenantComponent = createTenantComponentStructure(tenant);

        // 5. Create owner
        createTenantOwner(tenantComponent);

        return tenant;
    }

    public Tenant saveTenantInformation(Tenant tenant) {
        // 1. Save USER LOCALE and
        saveTenantLocalePreference(tenant);

        // 2. Create tenant component structure
        Component tenantComponent = createTenantComponentStructure(tenant);

        return tenant;
    }

    public AelUser createTenantAdmin(Tenant tenant, AelUser owner) {

        Component tenantComponent = componentService.findOneByTenantId(tenant.getId());

        return setTenantOwnerInformation(tenant, owner, tenantComponent);
    }

    private AelUser setTenantOwnerInformation(Tenant tenant, AelUser owner, Component tenantComponent) {

        String password = PasswordGeneratorHelper.generatePassword();
        if (owner != null) {
            //5.1. Create user
            if (owner.getId() == null) {
                owner.setCreateDate(new Date());
                owner.setRelatedUid(owner.getUsername());
                owner.setDisabled(false);
                owner.setImported(false);
                owner.setReadOnly(false);
                owner.setPassword(passwordEncoder.encodePassword(password, null));
                owner.setTenant(tenant);
            } else {
                owner.setTenant(tenant);
            }
            userService.saveOrUpdate(owner);
            tenant.setPlainOwnerPassword(password);
            setChangePasswordOnFirstUse(owner);

            //5.2. Associate groups
            assignGroupsToOwner(owner);

            //5.3. Associate user to tenant component
            createOwnerComponentUserJob(owner, tenantComponent);
        }

        return owner;
    }

    public void saveTenantLocalePreference(Tenant tenant) {
        // salvare preferinta USER LOCALE
        Preference preference = preferencesService.getByName(Preferences.USER_LOCALE.toString(), tenant);
        if (preference == null) {
            preference = new Preference();
            preference.setGlobal(false);
            preference.setName(Preferences.USER_LOCALE.toString());
            preference.setTenant(tenant);
        }
        preference.setValue(tenant.getLanguage());
        if (preference.getId() == null) {
            preferencesService.save(preference);
        } else {
            preferencesService.update(preference);
        }
    }

    public MailConfiguration saveTenantMailConfiguration(Tenant tenant) {
        // salvare preferinta MAIL CONFIGURATION
        MailConfiguration mailConfigurationDB = mailConfigurationService.findOneByTenantId(tenant.getId());
        MailConfiguration mailConfiguration = tenant.getMailConfiguration();
        if (mailConfiguration == null) {
            mailConfiguration = new MailConfiguration();
            mailConfiguration.setHost("");
        }
        if (mailConfigurationDB != null) {
            mailConfiguration.setId(mailConfigurationDB.getId());
        }
        mailConfiguration.setTenant(tenant);
        for (MailConfigurationAttributes mailCfgAttr : mailConfiguration.getMailCfgAttributesList()) {
            mailCfgAttr.setParentMailConfiguration(mailConfiguration);
        }
        return mailConfigurationService.saveConfiguration(mailConfiguration);
    }

    /********************************************************************************
     * 4. Create tenant component structure
     ********************************************************************************/
    private Component createTenantComponentStructure(Tenant tenant) {
        /*       4.2. Create tenant ComponentTypes and associated entities */
        /*          4.2.1. Create tenant ComponentTypes */
        /*          4.2.2. Create tenant JobTitles and associate them to ComponentTypes */
        ComponentType tenantRootCT = createTenantRootComponentType(tenant);
        /*       4.3. Create tenant component child to AeL root component */
        Component tenantComponent = createRootTenantComponent(tenantRootCT);

        return tenantComponent;
    }

    /********************************************************************************
     * 4.2. This method returns the tenant root ComponentType ( it will be used when creating a tenant component)
     * First of all we make some maps to help us duplicate the default AeL ComponentType tree
     * Second, after we saved the tenant ComponentType in "createComponentType" we set the parents
     * helped by the aelTenantCTMapping map created from AeL ComponentType tree
     * Third, we return the root tenant ComponentType from tenant default period ( 4.2 )
     ********************************************************************************/
    private ComponentType createTenantRootComponentType(Tenant tenant) {

        List<ComponentType> aelComponentTypes = componentTypeService.getAllForRoot();
        Map<ComponentType, ComponentType> aelTenantCTMapping = new HashMap<ComponentType, ComponentType>();
        ComponentType tenantRootComponent = null;
        // we map each ael default CT to a new tenant CT
        for (ComponentType aelCT : aelComponentTypes) {
            ComponentType tct = createComponentType(tenant, aelCT);
            aelTenantCTMapping.put(aelCT, tct);
        }

        // with the help of the map we set each tenant CT the rightful tenant CT parent guided by the AeL CTs structure
        for (ComponentType ct : aelComponentTypes) {
            ComponentType tct = aelTenantCTMapping.get(ct);
            tct.setParent(aelTenantCTMapping.get(ct.getParent()));
            if (tct.getParent() == null) {
                tenantRootComponent = tct;
            }
            componentTypeService.saveOrUpdate(tct);
        }

        // we send the aelTenantCTMapping because it will help building the CT-JT association in tenant
        createTenantComponentTypeJobTitles(aelTenantCTMapping, tenant);

        return tenantRootComponent;
    }

    /********************************************************************************
     * 4.2.1 Create tenant ComponentTypes by duplicating AeL default component types
     * Also we modify the name and add the tenant into these new ComponentTypes
     ********************************************************************************/
    private ComponentType createComponentType(Tenant tenant, ComponentType sibling) {

        Date today = new Date();
        LocalDate localDate = today.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        Date oneYearFromNow =
                Date.from(localDate.plusYears(1).atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());

        ComponentType componentType = new ComponentType();
        componentType.setName(tenant.getName() + "-" + sibling.getName());
        componentType.setStartDate(today);
        componentType.setEndDate(oneYearFromNow);
        componentType.setTenant(tenant);
        componentTypeService.saveOrUpdate(componentType);
        return componentType;
    }

    /********************************************************************************
     * 4.2.2. Create tenant JobTitles and CT-JT association from AeL default CT-JT association
     * First, we make a map with each tenant JT name and the AeL coresponding JT entity
     * Second, we make a second map, mapping save each tenant JT name to a new tenant JT object
     * Third, we save the tenant JTs
     * Forth, we create CT-JT associations using tenant CT and JT.
     ********************************************************************************/
    private void createTenantComponentTypeJobTitles(Map<ComponentType, ComponentType> aelTenantCTMapping,
                                                    Tenant tenant) {

        if (tenant != null) {
            Map<String, JobTitle> jtNameMap = new HashMap<String, JobTitle>();
            Map<ComponentType, String> tenantCTJobMapping = new HashMap<ComponentType, String>();

            // create mappings:
            for (ComponentType ct : aelTenantCTMapping.keySet()) {
                List<JobTitle> ctjts = componentTypeJobTitleService.getJobTitlesByComponentTypeId(ct.getId());
                for (JobTitle ctjt : ctjts) {
                    String jtName = tenant.getName() + "-" + ctjt.getName();
                    if (!jtNameMap.containsKey(jtName)) {
                        jtNameMap.put(jtName, new JobTitle(jtName, jtName, tenant));
                    }
                    tenantCTJobMapping.put(aelTenantCTMapping.get(ct), jtName);
                }
            }

            // save all job titles
            jobTitleService.save(jtNameMap.values());

            //create tenant CT-JT associations by duplicating AeL default CT-JT structure
            for( ComponentType tct: tenantCTJobMapping.keySet()){
                componentTypeJobTitleService.createComponentTypeJobTitle(tct, jtNameMap.get(tenantCTJobMapping.get(tct)));
            }
        } else {
            //  logger.error("Cannot create job titles. Tenant is null ");
        }
    }

    /********************************************************************************
     * 4.3. Create tenant root Component
     ********************************************************************************/
    private Component createRootTenantComponent(ComponentType componentType) {

        if (componentType != null && componentType.getTenant() != null) {
            Tenant tenant = componentType.getTenant();

            Component tenantComponent = new Component();
            tenantComponent.setName(tenant.getName());
            tenantComponent.setCompleteName(tenant.getName());
            tenantComponent.setParent(componentService.getRootComponent());
//            tenantComponent.setType(componentType);
            tenantComponent.setRelatedEntityId(GuidUtils.generateGuid());
            tenantComponent.setTenant(tenant);
            componentService.saveOrUpdate(tenantComponent);
            return tenantComponent;
        } else {
            //  logger.error("Could not create tenant root component; Period or tenant is null");
        }
        return null;
    }

    /********************************************************************************
     * 5.1. Create user
     * 5.2. Associate groups
     * 5.3. Associate user to tenant component ( create component-user-job)
     * 5.4. Add rights to approve content
     ******************************************************************************/
    private AelUser createTenantOwner(Component tenantComponent) {
        // salvare user
        Tenant tenant = tenantComponent.getTenant();
        if (tenant == null) {
            return null;
        }

        AelUser owner = (AelUser) tenant.getOwner();
        return setTenantOwnerInformation(tenant, owner, tenantComponent);
    }

    private void setChangePasswordOnFirstUse(AelUser user) {

        Preference preference = new Preference();
        preference.setName(Preferences.CHANGE_PASSWORD_ON_FIRST_USE.get());
        preference.setValue("true");
        preference.setUser(user);
        preference.setGlobal(false);
        preference.setTenant(user.getTenant());
        preferencesService.saveOrUpdate(preference);

        preference = new Preference();
        preference.setName(Preferences.CHANGE_PASSWORD_START_TIME.get());
        preference.setValue((new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date()));
        preference.setUser(user);
        preference.setGlobal(false);
        preference.setTenant(user.getTenant());
        preferencesService.saveOrUpdate(preference);
    }

    /********************************************************************************
     * 5.2. Associate groups
     * By default owner is asociated to Everybody and TenantAdministrators
     ********************************************************************************/
    private void assignGroupsToOwner(AelUser owner) {

        AelGroup everybodyGroup = aelGroupService.getByName("Everybody");
        AelGroup administratorsGroup = aelGroupService.getByName("TenantAdministrators");
        AelGroup contentApproverGroup = aelGroupService.getByName("ContentApprover");

        owner.setGroups(Arrays.asList(everybodyGroup, administratorsGroup, contentApproverGroup));
    }

    /********************************************************************************
     * 5.3. Associate user to tenant component
     * Create owner CUJ association to tenantComponent and try to put it a jobTitle
     ********************************************************************************/
    private void createOwnerComponentUserJob(AelUser owner, Component tenantComp) {
        // asociere user la componenta
        if (owner != null && tenantComp != null) {
            ComponentUserJob componentUserJob = new ComponentUserJob();
            componentUserJob.setUser(owner);
            componentUserJob.setComponent(tenantComp);
            componentUserJob.setActive(Boolean.TRUE);
            List<JobTitle> tenantJobTitles = jobTitleService.findByTenant(owner.getTenant());
            componentUserJob.setJobTitle(tenantJobTitles.get(0));
            componentUserJobService.saveOrUpdate(componentUserJob);
        } else {
            // logger.error("Tenant owner or tenant component is null");
        }
    }

    public Tenant addImage(Tenant tenant, MultipartFile file) throws Exception {

        ByteArrayOutputStream baos = multimediaService.processThumbnail(file);
        byte[] binaryData = baos.toByteArray();

        tenant.setImage(binaryData);
        tenant = saveOrUpdate(tenant);

        return tenant;
    }

    public Tenant saveOrUpdate(Tenant tenant) {

        if (tenant.getId() == null) {
            return save(tenant);
        } else {
            return update(tenant);
        }
    }

    public List<Tenant> findAllActiveTenants() {

        return ((TenantRepository) getResourceRepository()).findAllActiveTenants();
    }

    public Tenant findOneByName(String name) {

        return ((TenantRepository) getResourceRepository()).findOneByName(name);
    }

}
