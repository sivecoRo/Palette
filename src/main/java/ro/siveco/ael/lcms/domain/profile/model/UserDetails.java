package ro.siveco.ael.lcms.domain.profile.model;

import org.hibernate.annotations.Type;
import org.springframework.web.multipart.MultipartFile;
import ro.siveco.ael.domain.annotations.ColumnComment;
import ro.siveco.ael.lcms.domain.auth.model.AelUser;
import ro.siveco.ael.lcms.domain.metadata.model.Country;
import ro.siveco.ael.lcms.domain.metadata.model.Interest;
import ro.siveco.ael.lcms.domain.metadata.model.Tag;
import ro.siveco.ram.starter.ui.config.ActionDescriptor;
import ro.siveco.ram.starter.ui.config.PropertyViewDescriptor;
import ro.siveco.ram.starter.ui.config.ViewDescriptor;
import ro.siveco.ram.starter.ui.model.ActionType;
import ro.siveco.ram.starter.ui.model.FormInputType;
import ro.siveco.ram.starter.ui.model.GridSize;
import ro.siveco.ram.starter.ui.model.ViewRendererType;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

/**
 * Copyright: Copyright (c) 2006 Company: SIVECO Romania SA
 *
 * @author robertr
 * @version : 1.0 / Dec 19, 2006
 */

@Entity
@Table(name = "user_details")
@SequenceGenerator(name = "user_details_seq", sequenceName = "user_details_seq")
@ViewDescriptor(defaultSearchProperty = "birthDate", defaultActions = {
        /* --- [actions] --- */
        @ActionDescriptor(type = ActionType.OPEN_CREATE_FORM, fixedAction = true),
        @ActionDescriptor(type = ActionType.OPEN_EDIT_FORM, mainAction = true),
        @ActionDescriptor(type = ActionType.DELETE, mainAction = true, requiresConfirmation = true),
                                                        /* --- [flows] --- */
        @ActionDescriptor(type = ActionType.CREATE, forwardFor = "{modelName}-OPEN_CREATE_FORM",
                availableOnlyInForm = true),
        @ActionDescriptor(type = ActionType.UPDATE, forwardFor = "{modelName}-OPEN_EDIT_FORM",
                availableOnlyInForm = true,
                cssClass = "generic-action action-update action-userDetails-update btn meta-controller"),
        @ActionDescriptor(type = ActionType.LIST, availableInForm = true, backToFor = {
                "{modelName}-OPEN_CREATE_FORM",
                "{modelName}-CREATE",
                "{modelName}-OPEN_PRESENTATION",
//                "{modelName}-OPEN_EDIT_FORM",
                "{modelName}-DELETE"}),
        @ActionDescriptor(type = ActionType.CUSTOM_GET,
                uri = "/", backToFor = "{modelName}-UPDATE", availableInForm = true)})
public class UserDetails extends ro.siveco.ram.starter.model.base.BaseEntity {

    private Long id;

    @ColumnComment(comment = "The user's id")
    private AelUser user;

    @ColumnComment(comment = "The user's school")
    private String school;

    @ColumnComment(comment = "The user's class")
    private String userClass;

    @ColumnComment(comment = "The user's school_type")
    private String schoolType;

    private Date birthDate;

    private String displayBirthDate;

    private byte[] image;

    private String imageSrc = "/img/generic/generic-user.png";

    @ColumnComment(comment = "The user's first name")
    private String firstName;

    @ColumnComment(comment = "The user's last name")
    private String lastName;

    private Gender gender;

    @ColumnComment(comment = "The user's address in the city")
    private String address;

    @ColumnComment(comment = "The user's phone number")
    private String phoneNo;

    @ColumnComment(comment = "The user's age")
    private Integer age;

    private Boolean hasImage;

    private String oldPassword;

    private String password;

    private String confirmPassword;

    private LanguageDTO language;

    private String username;

    private String email;

    private Country country;

    private String city;

    private List<Tag> includedTags;

    private MultipartFile[] imageMultipart;

    private String city_name;

    /* visibility */
    private Boolean show_city;
    private Boolean show_country;
    private Boolean show_phone;
    private Boolean show_gender;
    private Boolean show_email;
    private Boolean show_birth_date;
    private Boolean show_interests;

    private List<Interest> allInterests;

    private String displayName;

    private Country presentationCountry;
    private String presentationPhoneNo;
    private String presentationEmail;
    private Long createdItemsNo;
    private Long favoredItemsNo;
    //private List<PlannedCourse> plannedCourses;
    private Long currentUserId;

    private Boolean sendEmail = true;
    private Boolean showAttendingItems = true;

    private String defaultImage = "/img/generic/generic-user.png";

    @PropertyViewDescriptor(hiddenOnGrid = true, hiddenOnPresentation = true, hiddenOnList = true, hiddenOnCard = true,
            inputType = FormInputType.CHECKBOX, displayLabelInForm = false,
            inputFieldSize = GridSize.ONE_PER_ROW,
            orderInForm = 14, groupName = "showCountry")
    public Boolean getShow_country() {
        return show_country;
    }

    @PropertyViewDescriptor(hiddenOnGrid = true, hiddenOnPresentation = true, hiddenOnList = true, hiddenOnCard = true,
            inputType = FormInputType.CHECKBOX, displayLabelInForm = false,
            inputFieldSize = GridSize.ONE_PER_ROW,
            orderInForm = 15, groupName = "showCity",
            cssClass = "property-presentation model-userDetails-property model-property-value model-property-show_city switch-input")
    public Boolean getShow_city() { return this.show_city; }

    @PropertyViewDescriptor(hiddenOnGrid = true, hiddenOnPresentation = true, hiddenOnList = true, hiddenOnCard = true,
            inputType = FormInputType.CHECKBOX, displayLabelInForm = false,
            inputFieldSize = GridSize.ONE_PER_ROW,
            orderInForm = 19, groupName = "showPhone")
    public Boolean getShow_phone() {
        return show_phone;
    }

    @PropertyViewDescriptor(hiddenOnGrid = true, hiddenOnPresentation = true, hiddenOnList = true, hiddenOnCard = true,
            inputType = FormInputType.CHECKBOX, displayLabelInForm = false,
            inputFieldSize = GridSize.ONE_PER_ROW,
            orderInForm = 17, groupName = "showGender")
    public Boolean getShow_gender() {
        return show_gender;
    }

    @PropertyViewDescriptor(hiddenOnGrid = true, hiddenOnPresentation = true, hiddenOnList = true, hiddenOnCard = true,
            inputType = FormInputType.CHECKBOX, displayLabelInForm = false,
            inputFieldSize = GridSize.ONE_PER_ROW,
            orderInForm = 18, groupName = "showEmail")
    public Boolean getShow_email() {
        return show_email;
    }

    @PropertyViewDescriptor(hiddenOnGrid = true, hiddenOnPresentation = true, hiddenOnList = true, hiddenOnCard = true, hiddenOnForm = true,
            inputType = FormInputType.CHECKBOX, displayLabelInForm = false,
            inputFieldSize = GridSize.ONE_PER_ROW,
            orderInForm = 16, groupName = "showBirthDate")
    public Boolean getShow_birth_date() {
        return show_birth_date;
    }

    @PropertyViewDescriptor(hiddenOnGrid = true, hiddenOnPresentation = true, hiddenOnList = true, hiddenOnCard = true,
            inputType = FormInputType.CHECKBOX, displayLabelInForm = false,
            inputFieldSize = GridSize.ONE_PER_ROW,
            orderInForm = 21, groupName = "showInterests")
    public Boolean getShow_interests() {
        return show_interests;
    }

    public Boolean setShow_country() { return this.show_country; }
    public Boolean setShow_phone() { return this.show_phone; }
    public Boolean setShow_gender() { return this.show_gender; }
    public Boolean setShow_email() { return this.show_email; }
    public Boolean setShow_birth_date() { return this.show_birth_date; }
    public Boolean setShow_interests() { return this.show_interests; }

    public void setShow_city(Boolean flag) { this.show_city = flag; }
    public void setShow_country(Boolean flag) { this.show_country = flag; }
    public void setShow_phone(Boolean flag) { this.show_phone = flag; }
    public void setShow_gender(Boolean flag) { this.show_gender = flag; }
    public void setShow_email(Boolean flag) { this.show_email = flag; }
    public void setShow_birth_date(Boolean flag) { this.show_birth_date = flag; }
    public void setShow_interests(Boolean flag) { this.show_interests = flag; }

    @PropertyViewDescriptor(orderInForm = 10, hiddenOnCard = true, hiddenOnGrid = true, hiddenOnList = true,
            groupName = "cityName", displayLabelInForm = true)
    public String getCity_name() { return this.city_name; }
    public void setCity_name(String txt) { this.city_name = txt; }


    @Id
    @GeneratedValue(generator = "user_details_seq", strategy = GenerationType.AUTO)
    public Long getId() {

        return id;
    }

    public void setId(Long id) {

        this.id = id;
    }

    @OneToOne()
    @NotNull
    @JoinColumn(name = "user_id", nullable = false, unique = true)
    @PropertyViewDescriptor(ignore = true)
    public AelUser getUser() {

        return user;
    }

    public void setUser(AelUser _user) {

        user = _user;
    }

    @Column(name = "school", length = 500)
    @PropertyViewDescriptor(ignore = true)
    public String getSchool() {

        return school;
    }

    public void setSchool(String school) {

        this.school = school;
    }

    @Column(name = "class", length = 500)
    @PropertyViewDescriptor(ignore = true)
    public String getUserClass() {

        return userClass;
    }

    public void setUserClass(String userClass) {

        this.userClass = userClass;
    }

    @Column(name = "school_type", length = 500)
    @PropertyViewDescriptor(ignore = true)
    public String getSchoolType() {

        return schoolType;
    }

    public void setSchoolType(String schoolType) {

        this.schoolType = schoolType;
    }

    @Column(name = "birth_date")
    @PropertyViewDescriptor(inputType = FormInputType.DATE, rendererType = ViewRendererType.DATE, orderInForm = 12, required = false,
                            groupName = "birthDate", displayLabelInForm = false, hiddenOnPresentation = true, hiddenOnForm = true)
    public Date getBirthDate() {

        return birthDate;
    }

    public void setBirthDate(Date birthDate) {

        this.birthDate = birthDate;
    }

    @Lob
    @Column(name = "image", columnDefinition = "bytea")
    @Type(type = "org.hibernate.type.BinaryType")
    @PropertyViewDescriptor(ignore = true)
    public byte[] getImage() {

        return image;
    }

    public void setImage(byte[] image) {

        this.image = image;
    }

    @Transient
    @PropertyViewDescriptor(inputType = FormInputType.TEXT, rendererType = ViewRendererType.STRING, orderInForm = 2,
                            groupName = "firstName", displayLabelInForm = true, hiddenOnPresentation = true)
    public String getFirstName() {

        return firstName;
    }

    public void setFirstName(String firstName) {

        this.firstName = firstName;
    }

    @Transient
    @PropertyViewDescriptor(inputType = FormInputType.TEXT, rendererType = ViewRendererType.STRING, orderInForm = 3,
                            groupName = "lastName", displayLabelInForm = true, inputFieldValidation = false, hiddenOnPresentation = true)
    public String getLastName() {

        return lastName;
    }

    public void setLastName(String lastName) {

        this.lastName = lastName;
    }

    @Transient
    @PropertyViewDescriptor(orderInForm = 12, hiddenOnCard = true, hiddenOnGrid = true, hiddenOnList = true, hiddenOnPresentation = true, required = false,
                            groupName = "gender", displayLabelInForm = true)
    public Gender getGender() {

        return gender;
    }

    public void setGender(Gender gender) {

        this.gender = gender;
    }

    @Transient
    @PropertyViewDescriptor(ignore = true)
    public String getAddress() {

        return address;
    }

    public void setAddress(String address) {

        this.address = address;
    }

    @Transient
    @PropertyViewDescriptor(orderInForm = 8, hiddenOnCard = true, hiddenOnGrid = true, hiddenOnList = true, hiddenOnPresentation = true, required = false,
                            groupName = "phoneNo", displayLabelInForm = true)
    public String getPhoneNo() {

        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {

        this.phoneNo = phoneNo;
    }

    @Transient
    @PropertyViewDescriptor(inputType = FormInputType.NUMBER, rendererType = ViewRendererType.INTEGER,
            hiddenOnForm = true, hiddenOnPresentation = true)
    public Integer getAge() {

        return age;
    }

    public void setAge(Integer age) {

        this.age = age;
    }

    @Override
    public String toString() {

        return "UserDetails [id=" + id + ", user=" + user + ", age=" + age + ", school=" + school + ", userClass="
                + userClass + ", schoolType=" + schoolType + ", firstName=" + firstName + ", lastName=" + lastName
                + ", gender=" + gender + ", address=" + address + ", phoneNo=" + phoneNo + "]";
    }

    @Transient
    @PropertyViewDescriptor(ignore = true)
    public Boolean getHasImage() {

        return hasImage;
    }

    public void setHasImage(Boolean hasImage) {

        this.hasImage = hasImage;
    }

    @Transient
    @PropertyViewDescriptor(
            hiddenOnList = true, hiddenOnGrid = true, hiddenOnCard = true, hiddenOnPresentation = true,
            rendererType = ViewRendererType.STRING, required = false,
            orderInForm = 5, inputType = FormInputType.PASSWORD, minSize = 8,
            groupName = "oldPassword", displayLabelInForm = true)
    public String getOldPassword() {

        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {

        this.oldPassword = oldPassword;
    }

    @Transient
    @PropertyViewDescriptor(
            hiddenOnList = true, hiddenOnGrid = true, hiddenOnCard = true, hiddenOnPresentation = true,
            rendererType = ViewRendererType.STRING, required = false,
            orderInForm = 6, inputType = FormInputType.PASSWORD,
            groupName = "password", displayLabelInForm = true)
    public String getPassword() {

        return password;
    }

    public void setPassword(String password) {

        this.password = password;
    }

    @Transient
    @PropertyViewDescriptor(
            hiddenOnList = true, hiddenOnGrid = true, hiddenOnCard = true, hiddenOnPresentation = true,
            rendererType = ViewRendererType.STRING, required = false,
            orderInForm = 7, inputType = FormInputType.PASSWORD, minSize = 8,
            groupName = "confirmPassword", displayLabelInForm = true)
    public String getConfirmPassword() {

        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {

        this.confirmPassword = confirmPassword;
    }

    @Transient
    @PropertyViewDescriptor(inputType = FormInputType.LIST_OF_VALUES, rendererType = ViewRendererType.MODEL,
            orderInForm = 16, hiddenOnPresentation = true,
            groupName = "language", displayLabelInForm = true)
    public LanguageDTO getLanguage() {

        return language;
    }

    public void setLanguage(LanguageDTO language) {

        this.language = language;
    }

    @Transient
    @PropertyViewDescriptor(orderInForm = -10, hidden = true, hiddenOnCard = true, hiddenOnGrid = true, hiddenOnList = true, hiddenOnPresentation = true, displayLabelInForm = true)
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Transient
    @PropertyViewDescriptor(orderInForm = 1, disabled = true, hiddenOnCard = true, hiddenOnGrid = true, hiddenOnList = true, hiddenOnPresentation = true,
                            groupName = "email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Transient
    @PropertyViewDescriptor(
            hiddenOnList = true, hiddenOnGrid = true, hiddenOnCard = true,
            rendererType = ViewRendererType.MODEL, hiddenOnPresentation = true,
            orderInForm = 9, inputType = FormInputType.LIST_OF_VALUES,
            groupName = "country", displayLabelInForm = true
    )
    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    @Transient
    @PropertyViewDescriptor(hiddenOnCard = true, hiddenOnGrid = true, hiddenOnList = true,
            displayLabelInPresentation = false, inputType = FormInputType.HIDDEN, hiddenOnPresentation = true,
            rendererType = ViewRendererType.MODEL_COLLECTION, required = false,
            displayLabelInForm = false, groupName = "city")
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Transient
    @PropertyViewDescriptor(
            hiddenOnCard = true, hiddenOnGrid = true, hiddenOnList = true, hiddenOnPresentation = true,
            displayLabelInPresentation = false, inputType = FormInputType.LIST_OF_VALUES_MULTI_SELECT,
            rendererType = ViewRendererType.MODEL_COLLECTION, groupName = "includedTags", required = false,
            orderInForm = 14, displayLabelInForm = false)
    public List<Tag> getIncludedTags() {

        return includedTags;
    }

    public void setIncludedTags(List<Tag> includedTags) {

        this.includedTags = includedTags;
    }

    @Transient
    @PropertyViewDescriptor(
            order = -3, displayLabelInPresentation = false,
            rendererType = ViewRendererType.IMAGE, groupName = "userImage",
            hiddenOnForm = true,
            clickAction = {
                    @ActionDescriptor(type = ActionType.OPEN_PRESENTATION, ajax = false),
                    @ActionDescriptor(type = ActionType.LIST, backToFor = "{modelName}-OPEN_PRESENTATION")})
    public String getImageSrc() {

        return imageSrc;
    }

    public void setImageSrc(String imageSrc) {

        this.imageSrc = imageSrc;
    }

    @Transient
    @PropertyViewDescriptor(hiddenOnGrid = true, hiddenOnPresentation = true, hiddenOnList = true, hiddenOnCard = true,
            inputType = FormInputType.IMAGE_UPLOAD, displayLabelInForm = false,
            inputFieldSize = GridSize.TWO_PER_ROW,
            orderInForm = 13, groupName = "imageMultipart")
    public MultipartFile[] getImageMultipart() {

        return imageMultipart;
    }

    public void setImageMultipart(MultipartFile[] imageMultipart) {

        this.imageMultipart = imageMultipart;
    }

    @Transient
    @PropertyViewDescriptor(hidden = true)
    public List<Interest> getAllInterests() {
        return allInterests;
    }

    public void setAllInterests(List<Interest> allInterests) {
        this.allInterests = allInterests;
    }

    @Transient
    @PropertyViewDescriptor(hiddenOnForm = true, hiddenOnUpdate = true, hiddenOnCreate = true, hiddenOnCard = true, hiddenOnList = true, hiddenOnGrid = true,
            groupName = "displayName", displayLabelInPresentation = false)
    public String getDisplayName() {
        return (getFirstName() == null ? "" : getFirstName()) + " " + (getLastName() == null ? "" : getLastName());
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }


    @Transient
    @PropertyViewDescriptor(
            order = -2,
            hiddenOnForm = true,
            hiddenOnGrid = true,
            hiddenOnPresentation = true,
            displayLabelInPresentation = false,
            rendererType = ViewRendererType.OBJECT_LABEL,
            clickAction = {@ActionDescriptor(
                    type = ActionType.OPEN_PRESENTATION
            ), @ActionDescriptor(
                    type = ActionType.LIST,
                    backToFor = {"{modelName}-OPEN_PRESENTATION"}
            )}
    )
    public String getObjectLabel() {
        return getDisplayName();
    }

    @Transient
    @PropertyViewDescriptor(
            hiddenOnList = true, hiddenOnGrid = true, hiddenOnCard = true, hiddenOnCreate = true, hiddenOnUpdate = true, hiddenOnForm = true,
            rendererType = ViewRendererType.MODEL,
            groupName = "presentationCountry"
    )
    public Country getPresentationCountry() {
        return getCountry();
    }

    public void setPresentationCountry(Country presentationCountry) {
        this.presentationCountry = presentationCountry;
    }

    @Transient
    @PropertyViewDescriptor(
            hiddenOnList = true, hiddenOnGrid = true, hiddenOnCard = true, hiddenOnCreate = true, hiddenOnUpdate = true, hiddenOnForm = true,
            rendererType = ViewRendererType.STRING,
            groupName = "presentationPhoneNo"
    )
    public String getPresentationPhoneNo() {
        return getPhoneNo();
    }

    public void setPresentationPhoneNo(String presentationPhoneNo) {
        this.presentationPhoneNo = presentationPhoneNo;
    }

    @Transient
    @PropertyViewDescriptor(
            hiddenOnList = true, hiddenOnGrid = true, hiddenOnCard = true, hiddenOnCreate = true, hiddenOnUpdate = true, hiddenOnForm = true,
            rendererType = ViewRendererType.STRING,
            groupName = "presentationEmail"
    )
    public String getPresentationEmail() {
        return getEmail();
    }

    public void setPresentationEmail(String presentationEmail) {
        this.presentationEmail = presentationEmail;
    }

    @Transient
    @PropertyViewDescriptor(hidden = true)
    public Long getCreatedItemsNo() {
        return createdItemsNo;
    }

    public void setCreatedItemsNo(Long createdItemsNo) {
        this.createdItemsNo = createdItemsNo;
    }

    @Transient
    @PropertyViewDescriptor(hidden = true)
    public Long getFavoredItemsNo() {
        return favoredItemsNo;
    }

    public void setFavoredItemsNo(Long favoredItemsNo) {
        this.favoredItemsNo = favoredItemsNo;
    }

    @Transient
    @PropertyViewDescriptor(hidden = true)
    public Long getCurrentUserId() {
        return currentUserId;
    }

    public void setCurrentUserId(Long currentUserId) {
        this.currentUserId = currentUserId;
    }

    @Transient
    @PropertyViewDescriptor(hidden = true)
    public String getDisplayBirthDate() {
        return displayBirthDate;
    }

    public void setDisplayBirthDate(String displayBirthDate) {
        this.displayBirthDate = displayBirthDate;
    }

    @Transient
    @PropertyViewDescriptor(hiddenOnGrid = true, hiddenOnPresentation = true, hiddenOnList = true, hiddenOnCard = true,
            inputType = FormInputType.CHECKBOX, displayLabelInForm = true,
            inputFieldSize = GridSize.ONE_PER_ROW,
            orderInForm = 21, groupName = "sendEmail")
    public Boolean getSendEmail() {
        return sendEmail;
    }

    public void setSendEmail(Boolean sendEmail) {
        this.sendEmail = sendEmail;
    }

    @PropertyViewDescriptor(hiddenOnGrid = true, hiddenOnPresentation = true, hiddenOnList = true, hiddenOnCard = true,
            inputType = FormInputType.CHECKBOX, displayLabelInForm = true,
            inputFieldSize = GridSize.ONE_PER_ROW,
            orderInForm = 20, groupName = "showAttendingItems")
    @Column(name="show_attending_items")
    public Boolean getShowAttendingItems() {
        return showAttendingItems;
    }

    public void setShowAttendingItems(Boolean showAttendingItems) {
        this.showAttendingItems = showAttendingItems;
    }

    @Transient
    @PropertyViewDescriptor(hidden = true)
    public String getDefaultImage() {
        return defaultImage;
    }

    public void setDefaultImage(String defaultImage) {
        this.defaultImage = defaultImage;
    }
}
