package ro.siveco.ael.lcms.domain.organization.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.siveco.ael.lcms.domain.auth.service.AelUserService;
import ro.siveco.ael.lcms.domain.organization.model.ComponentUserJob;
import ro.siveco.ael.lcms.domain.organization.model.JobTitle;
import ro.siveco.ael.lcms.repository.ComponentUserJobRepository;
import ro.siveco.ram.starter.service.BaseEntityService;

import java.util.List;

/**
 * Created by LiviuI on 4/10/2017.
 */
@Service
public class ComponentUserJobService extends BaseEntityService<ComponentUserJob> {

    private AelUserService aeluserService;

    private ComponentService componentService;

    private JobTitleService jobTitleService;

    @Autowired
    public ComponentUserJobService(ComponentUserJobRepository resourceRepository, AelUserService aeluserService, ComponentService componentService, JobTitleService jobTitleService) {
        super(ComponentUserJob.class, resourceRepository);
        this.aeluserService = aeluserService;
        this.componentService = componentService;
        this.jobTitleService = jobTitleService;
    }

    public void smth() {
        //getResourceRepository().getComponentChildTreeNodes(asda)
    }

    @Override
    protected ComponentUserJobRepository getResourceRepository() {
        return (ComponentUserJobRepository) super.getResourceRepository();
    }


    public ComponentUserJob saveOrUpdate(ComponentUserJob componentUserJob)
    {
        if(componentUserJob.getId() == null)
        {
            return save(componentUserJob);
        }
        else
        {
            return update(componentUserJob);
        }
    }
    public ComponentUserJob findById(Long id)
    {
        return ((ComponentUserJobRepository)getResourceRepository()).findById(id);
    }

    public ComponentUserJob findByUserIdAndActive(Long userId, Boolean active)
    {
        return ((ComponentUserJobRepository)getResourceRepository()).findByUserIdAndActive(userId, active);
    }

    public ComponentUserJob getComponentUserJobForUserComponentAndJobTitle(Long userId, Long componentId, Long jobTitleId){
        ComponentUserJob componentUserJob = this.getResourceRepository().getComponentUserJobForUserComponentAndJobTitle(userId, componentId, jobTitleId);
        if(componentUserJob == null){
            componentUserJob = new ComponentUserJob();
            componentUserJob.setActive(false);
            componentUserJob.setUser(aeluserService.findById(userId));
            if(componentUserJob.getUser() == null){
                throw new RuntimeException("error.componentUserJob.create.invalidData.user");
            }
            componentUserJob.setComponent(componentService.findById(componentId));
            if(componentUserJob.getComponent() == null){
                throw new RuntimeException("error.componentUserJob.create.invalidData.component");
            }
            componentUserJob.setJobTitle(jobTitleService.findById(jobTitleId));
            if(componentUserJob.getJobTitle() == null){
                throw new RuntimeException("error.componentUserJob.create.invalidData.jobTitle");
            }
            componentUserJob.setImported(false);
            return save(componentUserJob);
        }else{
            return componentUserJob;
        }
    }

    public ComponentUserJob changeActiveStatus(ComponentUserJob componentUserJob, Boolean newActiveStatus){
        componentUserJob.setActive(newActiveStatus);
        return update(componentUserJob);
    }

    public ComponentUserJob getActiveComponentUserJobForUser(Long userId){
        List<ComponentUserJob> cujList =  ((ComponentUserJobRepository) getResourceRepository()).getActiveComponentUserJobForUser(
                userId);
        if(cujList != null && cujList.size()>0)
        {
            return cujList.get(0);
        }
        return null;
    }

    public List<ComponentUserJob> findByJobTitle(JobTitle jobTitle) {
        return ((ComponentUserJobRepository) getResourceRepository()).findByJobTitle(jobTitle);
    }
}
