package ro.siveco.ael.lcms.domain.notification.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.notification.model.PlannedNotification;
import ro.siveco.ael.lcms.domain.notification.service.PlannedNotificationService;
import ro.siveco.ram.event.listener.type.single.BaseBeforeCreateListener;
import ro.siveco.ram.event.type.single.BeforeCreateEvent;

/**
 * Created by IuliaP on 12.10.2017.
 */
@Order(value = 1)
@Component
public class SetDateForPlannedNotificationBeforeCreateListener extends BaseBeforeCreateListener<PlannedNotification> {

    @Autowired
    PlannedNotificationService plannedNotificationService;

    @Override
    public void onApplicationEvent(BeforeCreateEvent<PlannedNotification> event) {
        PlannedNotification plannedNotification = event.getDomainModel();
        plannedNotificationService.validateForm( plannedNotification );
        plannedNotificationService.setDate( plannedNotification );
    }
}
