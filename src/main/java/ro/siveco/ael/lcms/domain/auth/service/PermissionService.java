package ro.siveco.ael.lcms.domain.auth.service;

import org.springframework.security.core.GrantedAuthority;
import ro.siveco.ael.lcms.domain.auth.model.User;

import java.util.List;

/**
 * User: AlexandruVi
 * Date: 2017-02-20
 */
public interface PermissionService {
    List<GrantedAuthority> loadUserPermissions(User user);
}
