package ro.siveco.ael.lcms.domain.preference.model;

/**
 * Created by IntelliJ IDEA.
 * User: AlexandruM
 * Date: 04.04.2007
 * Time: 14:06:57
 */
public enum Preferences
{
	//Populare preferinte BIBLIOTECA
	LIBRARY_TREE_UNFOLDED( "library.tree.show" ),
	LIBRARY_FILESSIZE_VIEW( "library.filesSize.view" ),
	LIBRARY_USEADVANCED_NAVIGATION_MODE( "library.useAdvancedNavigationMode" ),

	//Populare preferinte WIZARDS
	WIZARDS_LIBRARY_LESSON_WIZARD_SHOWINFO( "wizards.library.lessonWizard.showInfo" ),
	WIZARDS_LIBRARY_RESOURCES_WIZARD_SHOWINFO( "wizards.library.resourcesWizard.showInfo" ),
	WIZARDS_LIBRARY_LESSONMOMENTS_WIZARD_SHOWINFO( "wizards.library.lessonMomentsWizard.showInfo" ),
	WIZARDS_LIBRARY_COPYMOVE_WIZARD_SHOWINFO( "wizards.library.copyMoveWizard.showInfo" ),
	WIZARDS_LIBRARY_EDITRIGHTS_WIZARD_SHOWINFO( "wizards.library.editRightsWizard.showInfo" ),

	//Populare preferinte  OTHERS
	SKIN_COLOR( "skin.color" ),
	SEARCH_RES_PERPAGE( "search.resPerPage" ),
	SELECTOR_LANGUAGE( "selector.language" ),
	USER_FULLNAME_FORMAT( "user.fullName.format" ),
    USE_USER_DEFINED_CSS("css.userDefined"),
    USE_PRINTER_FRIENDLY_CSS("css.printerFriendly"),
	USER_COMPONENT("userComponent"),
	SHOW_WELCOME_MESSAGE("show.welcome.message"),
	SHOW_HELP_PAGES("show.help.pages"),
    SHOW_BOTTOM_MENU("show.bottom.menu"),
    PERSONALIZED_EMAIL_ADDRESS("personalized.email.address"),
    SEND_EMAIL("send.email"),
	SHOW_COMPONENT_SELECTOR("show.component.selector"),

	USER_LOCALE("userLocale"),

	DATE_FORMAT("date.format"),
	TIME_FORMAT("time.format"),
	DATE_TIME_FORMAT("date.time.format"),

	CHANGE_PASSWORD_ON_FIRST_USE( "change.password.on.first.use" ),
	CHANGE_PASSWORD_START_TIME( "change.password.start.time" ),

	//Populare preferinte GLOBALE
	GLOBAL_AELCLIENT_MAX_NR_OF_TRIES( "global.AelClient.MAX_NR_OF_TRIES" ),
	GLOBAL_AELCLIENT_TIMEOUT_MILLIS( "global.AelClient.TIMEOUT_MILLIS" ),
	GLOBAL_AELCLIENT_UPDATE_ENTITY_ON_SYNCHRONIZE( "global.AelClient.UPDATE_ENTITY_ON_SYNCHRONIZE" ),
	GLOBAL_AELCLIENT_DEFAULT_APPLICATION_LOCALE("global.AelClient.DEFAULT_APPLICATION_LOCALE"),
	GLOBAL_AELCLIENT_DEFAULT_CRON_EXPRESSION( "global.AelClient.DEFAULT_CRON_EXPRESSION" ),
	GLOBAL_AELCLIENT_SHOW_STUDENTS_RESULTS( "global.AelClient.SHOW_STUDENTS_RESULTS" ),
	GLOBAL_AELCLIENT_FIELD_TO_USE_ON_SYNCHRONIZE( "global.AelClient.FIELD_TO_USE_ON_SYNCHRONIZE" ),
	GLOBAL_SHOW_SIVECO_LINK( "global.SHOW_SIVECO_LINK" ),
    GLOBAL_AELCLIENT_PLAYERS_IN_FULL_SCREEN_MODE("global.AelClient.PLAYERS_IN_FULL_SCREEN_MODE"),
    GLOBAL_AELCLIENT_MAX_SIZE_TO_UPLOAD("global.AelClient.MAX_SIZE_TO_UPLOAD"),
    GLOBAL_CREATE_DESKTOP_LINKS("global.CREATE_DESKTOP_LINKS"),
    GLOBAL_AELCLIENT_DISPLAY_NAME( "global.AelClient.DISPLAY_NAME" ),
    GLOBAL_AEL_SERVER_HOST("global.AEL_SERVER_HOST"),
	GLOBAL_HTTP_SCHEME_TYPE("global.HTTP_SCHEME_TYPE"),
	GLOBAL_DISPLAY_RLO_SCORE("global.Display Rlo Score");

    private String name;

    Preferences( String _name )
	{
		this.name = _name;
	}

	public String get()
	{
		return name;
	}

	public String toString()
	{
		return name;
	}
}
