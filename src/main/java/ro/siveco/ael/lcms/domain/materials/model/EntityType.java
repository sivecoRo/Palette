package ro.siveco.ael.lcms.domain.materials.model;


import java.io.Serializable;


public enum EntityType implements Serializable {
    UNITATE("tip.unitate"),
    UNIVERSITATE("tip.universitate"),
    ACTIVITATE_ELEV_ELEV("tip.activitate.elev1"),
    ACTIVITATE_STUDENT_STUDENT("tip.activitate.student1"),
    ACTIVITATE_ELEV_UNITATE("tip.activitate.elev2"),
    ACTIVITATE_STUDENT_UNIVERSITATE("tip.activitate.student2"),
    ACTIVITATE_ELEV_AGENT("tip.activitate.elev3"),
    ACTIVITATE_STUDENT_AGENT("tip.activitate.student3");

    private String key;

    EntityType(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }

}

