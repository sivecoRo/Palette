package ro.siveco.ael.lcms.domain.organization.model;

//import org.apache.commons.lang.builder.ToStringBuilder;

import org.hibernate.annotations.*;
import ro.siveco.ael.lcms.domain.tenant.model.Tenant;
import ro.siveco.ram.starter.ui.config.PropertyViewDescriptor;
import ro.siveco.ram.starter.ui.config.ViewDescriptor;
import ro.siveco.ram.starter.ui.model.IndexViewType;

import javax.persistence.CascadeType;
import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

//import ro.siveco.ael.model.annotations.ColumnComment;
//import ro.siveco.ael.model.annotations.TableComment;

/**
 * Copyright: Copyright (c) 2006 Company: SIVECO Romania SA
 * @author RobertR
 * @version : 1.0 / Jan 22, 2007
 */

@Entity
@Table(name = "component_type", uniqueConstraints = {@UniqueConstraint(columnNames = {"name", "tenant_id"})})
@SequenceGenerator( name="component_type_seq", sequenceName = "component_type_seq")
//@TableComment(comment = "Stores the organizational component types")
@FilterDefs({
		@FilterDef(name="tenantComponentTypeFilter", parameters=@ParamDef( name="tenantId", type="long" ) ),
		@FilterDef(name="componentTypeFilter" )
})
@Filters( {
        @Filter(name = "tenantComponentTypeFilter", condition = "tenant_id=:tenantId "),
        @Filter(name = "componentTypeFilter", condition = "tenant_id is null")
} )
@ViewDescriptor(paginationEnabled = true, viewTypes = {IndexViewType.LIST}, defaultViewType = IndexViewType.LIST,
		searchEnabled = true, defaultSearchProperty = "name")
public class ComponentType extends ro.siveco.ram.starter.model.base.BaseEntity implements Serializable
{
	private Long id;

	//@ColumnComment(comment = "The component type's name")
	private String name;

	//@ColumnComment(comment = "The parent's id")
	private ComponentType parent;

	private List<ComponentType> children = new ArrayList<ComponentType>();

	//@ColumnComment(comment = "Start date")
	private Date startDate;

	//@ColumnComment(comment = "End date")
	private Date endDate;

    private Tenant tenant;

	@Id
	@GeneratedValue( generator = "component_type_seq", strategy = GenerationType.AUTO )
	public Long getId()
	{
		return id;
	}

	public void setId( Long _id )
	{
		id = _id;
	}

	@Column(unique = false, nullable = false, length = 500, name="name")
	public String getName()
	{
		return name;
	}

	public void setName( String _name )
	{
		name = _name;
	}

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = "parent_id", nullable = true)
	@PropertyViewDescriptor(hidden = true, required = false, hiddenOnForm = true)
	public ComponentType getParent()
	{
		return parent;
	}

	public void setParent( ComponentType _parent )
	{
		parent = _parent;
	}

	@OneToMany(mappedBy = "parent")
	@OrderBy("name")
	@PropertyViewDescriptor(hidden = true, required = false, hiddenOnForm = true)
	public List<ComponentType> getChildren()
	{
		return children;
	}

	public void setChildren( List<ComponentType> _children )
	{
		children = _children;
	}

	@ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = "tenant_id", nullable = false)
    @PropertyViewDescriptor(hidden = true, required = false, hiddenOnForm = true)
    public Tenant getTenant() {

        return tenant;
    }

    public void setTenant(Tenant tenant) {

        this.tenant = tenant;
    }

	@Column(nullable = true, name = "start_date")
	@PropertyViewDescriptor(hidden = true, required = false, hiddenOnForm = true)
	public Date getStartDate()
	{
		return startDate;
	}

	public void setStartDate( Date startDate )
	{
		this.startDate = startDate;
	}

	@Column(nullable = true, name = "end_date")
	@PropertyViewDescriptor(hidden = true, required = false, hiddenOnForm = true)
	public Date getEndDate()
	{
		return endDate;
	}

	public void setEndDate( Date endDate )
	{
		this.endDate = endDate;
	}

	public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ComponentType that = (ComponentType) o;

        if (!id.equals(that.id)) return false;

        return true;
    }

    public int hashCode() {
        return id.hashCode();
    }

//    public String toString()
//	{
//		return new ToStringBuilder( this )
//			.append( "id", getId() )
//			.append( "name", getName() )
//			.append( "parent", getParent() == null ? "null" : getParent().getId() )
//			.toString();
//	}
}
