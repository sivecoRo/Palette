package ro.siveco.ael.lcms.domain.organization.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.organization.model.ComponentUserJob;
import ro.siveco.ael.lcms.domain.organization.model.JobTitle;
import ro.siveco.ael.lcms.domain.organization.service.ComponentUserJobService;
import ro.siveco.ram.app.exception.AppException;
import ro.siveco.ram.event.listener.type.single.BaseBeforeDeleteListener;
import ro.siveco.ram.event.type.single.BeforeDeleteEvent;

import java.util.List;

/**
 * Created by AndradaC on 9/3/2017.
 */
@Component
public class CheckJobTitleConnectionsBeforeDeleteListener extends BaseBeforeDeleteListener<JobTitle>{

    @Autowired
    private ComponentUserJobService componentUserJobService;

    @Override
    public void onApplicationEvent(BeforeDeleteEvent<JobTitle> event) {
        JobTitle jobTitle = event.getDomainModel();
        List<ComponentUserJob> componentUserJobs = componentUserJobService.findByJobTitle(jobTitle);
        if(componentUserJobs != null && componentUserJobs.size() > 0) {
            throw new AppException("error.jobTitle.Delete.alreadyExistsOnCUJ");
        }
    }
}
