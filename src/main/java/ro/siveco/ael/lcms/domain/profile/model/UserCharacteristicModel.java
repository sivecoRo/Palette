package ro.siveco.ael.lcms.domain.profile.model;

import ro.siveco.ael.lcms.domain.metadata.model.Metadata;

import java.util.ArrayList;
import java.util.List;

public class UserCharacteristicModel {

	private Long id;
	private String name;
	private String description;
	private String value;
    private UserCharacteristicType type;
	private List<UserCharacteristicModel> list;
	private List<Metadata> metadataList;
	private String valueS;
	private Boolean valueB;
	private Long valueL;
	private Boolean active;
	
	
	
	public UserCharacteristicModel () {	}
	
	public UserCharacteristicModel (String name) {
		this.name = name;
		this.valueB = false;
		this.list = new ArrayList<UserCharacteristicModel>();
		this.metadataList = new ArrayList<Metadata>();
	}
	
	public UserCharacteristicModel (String name, String description) {
		this.name = name;
		this.description = description;
		this.valueB = false;
		this.list = new ArrayList<UserCharacteristicModel>();
		this.metadataList = new ArrayList<Metadata>();
	}
	
	public UserCharacteristicModel (Long id, String name, String description) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.valueB = false;
		this.list = new ArrayList<UserCharacteristicModel>();
		this.metadataList = new ArrayList<Metadata>();
	}
	
	public UserCharacteristicModel (Long id, String name, String description, Boolean active) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.active = active;
		this.valueB = false;
		this.list = new ArrayList<UserCharacteristicModel>();
		this.metadataList = new ArrayList<Metadata>();
	}
	
	public UserCharacteristicModel (String name, String description, String value) {
		this.name = name;
		this.description = description;
		this.value = value;
		this.valueB = false;
	}
	
	public UserCharacteristicModel (String name, String value, List<UserCharacteristicModel> list) {
		this.name = name;
		this.value = value;
		this.list = list;
		this.valueB = false;
	}
	
	public UserCharacteristicModel (Long id, String name, UserCharacteristicType type) {
		this.id = id;
		this.name = name;
		this.type = type;
		this.valueB = false;
		this.list = new ArrayList<UserCharacteristicModel>();
		this.metadataList = new ArrayList<Metadata>();
	}
	
	public UserCharacteristicModel (Long id, String name, String description, UserCharacteristicType type) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.type = type;
		this.valueB = false;
		this.list = new ArrayList<UserCharacteristicModel>();
		this.metadataList = new ArrayList<Metadata>();
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public UserCharacteristicType getType() {
		return type;
	}
	public void setType(UserCharacteristicType type) {
		this.type = type;
	}
	public List<UserCharacteristicModel> getList() {
		return list;
	}
	public void setList(List<UserCharacteristicModel> list) {
		this.list = list;
	}


	@Override
	public String toString() {
		return "UserCharacteristicModel [name=" + name + ", value=" + value + "]";
	}

	public List<Metadata> getMetadataList() {
		return metadataList;
	}

	public void setMetadataList(List<Metadata> metadataList) {
		this.metadataList = metadataList;
	}

	public String getValueS() {
		return valueS;
	}

	public void setValueS(String valueS) {
		this.valueS = valueS;
	}

	public Boolean getValueB() {
		return valueB;
	}

	public void setValueB(Boolean valueB) {
		this.valueB = valueB;
	}

	public Long getValueL() {
		return valueL;
	}

	public void setValueL(Long valueL) {
		this.valueL = valueL;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}
}
