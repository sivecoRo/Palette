package ro.siveco.ael.lcms.domain.profile.model;

public class UserCharacteristicManagementModel {

	private Long id;
	private Long categoryId;
	private String category;
	private Long characteristicId;
	private String characteristic;
	private Long specificId;
	private String specific;
	private Long tabId;
	private String tab;
	private UserCharacteristicType typeCode;
	private String type;
	private Boolean active;
	
	
	public UserCharacteristicManagementModel() {}
	
	public UserCharacteristicManagementModel(Long id, Long categoryId, Long characteristicId, Long specificId, Long tabId, 
			UserCharacteristicType type, Boolean active) {
		this.id = id;
		this.categoryId = categoryId;
		this.characteristicId = characteristicId;
		this.specificId = specificId;
		this.tabId = tabId;
		this.setTypeCode(type);
		this.type = type.getName();
		this.active = active;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public Long getCharacteristicId() {
		return characteristicId;
	}
	public void setCharacteristicId(Long characteristicId) {
		this.characteristicId = characteristicId;
	}
	public String getCharacteristic() {
		return characteristic;
	}
	public void setCharacteristic(String characteristic) {
		this.characteristic = characteristic;
	}
	public Long getSpecificId() {
		return specificId;
	}
	public void setSpecificId(Long specificId) {
		this.specificId = specificId;
	}
	public String getSpecific() {
		return specific;
	}
	public void setSpecific(String specific) {
		this.specific = specific;
	}
	public Long getTabId() {
		return tabId;
	}
	public void setTabId(Long tabId) {
		this.tabId = tabId;
	}
	public String getTab() {
		return tab;
	}
	public void setTab(String tab) {
		this.tab = tab;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Boolean getActive() {
		return active;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}

	public UserCharacteristicType getTypeCode() {
		return typeCode;
	}

	public void setTypeCode(UserCharacteristicType typeCode) {
		this.typeCode = typeCode;
	}

}
