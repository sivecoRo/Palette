package ro.siveco.ael.lcms.domain.course.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.course.model.Course;
import ro.siveco.ael.lcms.domain.course.service.CourseService;
import ro.siveco.ael.lcms.domain.course.web.CourseController;
import ro.siveco.ram.event.listener.type.single.BaseBeforeCreateListener;
import ro.siveco.ram.event.type.single.BeforeCreateEvent;
import ro.siveco.ram.starter.ui.model.Views;

/**
 * Created by AndradaC on 9/1/2017.
 */
@Component
public class SetCourseDefaultInformationBeforeCreateListener extends BaseBeforeCreateListener<Course>{

    @Autowired
    private CourseService courseService;

    @Override
    public void onApplicationEvent(BeforeCreateEvent<Course> event) {
        Course course = event.getDomainModel();

        CourseController controller = (CourseController) event.getSource();
        Class<? extends Views.View> view = controller.getRequestedView();
        String viewName = view.getName();

        courseService.setDefaultCreationInformation(course, viewName);
        courseService.validateCourse(course, null);
    }
}
