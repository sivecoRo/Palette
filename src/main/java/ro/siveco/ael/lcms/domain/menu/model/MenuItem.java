package ro.siveco.ael.lcms.domain.menu.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import ro.siveco.ram.starter.ui.config.ViewDescriptor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "menu_items")
@SequenceGenerator(name = "menu_items_seq", sequenceName = "menu_items_seq")
@ViewDescriptor
public class MenuItem extends ro.siveco.ram.starter.model.base.BaseEntity  implements Serializable {

    private Long id;

    private String menuKey;

    private String modelName;

    private Integer order = 0;

    private MenuRenderType type;

    private MenuItem parent;

    private List<MenuItem> children = new ArrayList<>();

    @Id
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "menu_key")
    public String getMenuKey() {
        return menuKey;
    }

    public void setMenuKey(String menuKey) {
        this.menuKey = menuKey;
    }

    @Column(name = "model_name")
    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    @Column(name = "order")
    public Integer getOrder() {
        return order;
    }

    /* setters */

    public void setOrder(Integer order) {
        this.order = order;
    }

    @Column(name = "type")
    public MenuRenderType getType() {
        return type;
    }

    public void setType(MenuRenderType type) {
        this.type = type;
    }

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = "parent_id", nullable = true)
    @JsonIgnore
    public MenuItem getParent() {
        return parent;
    }

    public void setParent(MenuItem parent) {
        this.parent = parent;
    }

    @OneToMany(mappedBy = "parent", fetch = FetchType.EAGER)
    public List<MenuItem> getChildren() {
        return children;
    }

    public void setChildren(List<MenuItem> children) {
        this.children = children;
    }
}
