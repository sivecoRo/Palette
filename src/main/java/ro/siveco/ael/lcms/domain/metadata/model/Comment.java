package ro.siveco.ael.lcms.domain.metadata.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.domain.Sort;
import ro.siveco.ael.lcms.domain.auth.model.AelUser;
import ro.siveco.ael.lcms.domain.course.model.PlannedCourse;
import ro.siveco.ram.starter.ui.config.PropertyViewDescriptor;
import ro.siveco.ram.starter.ui.config.Sorter;
import ro.siveco.ram.starter.ui.config.ViewDescriptor;
import ro.siveco.ram.starter.ui.model.IndexViewType;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created by albertb on 2/20/2018.
 */
@Entity
@Table(name="comments")
@SequenceGenerator(name = "comments_seq", sequenceName = "comments_seq")
@ViewDescriptor(pageSize = 10, viewTypes = {IndexViewType.GRID}, searchEnabled = true, defaultSearchProperty = "id",
        sort = {@Sorter(propertyName = "id", direction = Sort.Direction.ASC)})
public class Comment extends ro.siveco.ram.starter.model.base.BaseEntity{
    private AelUser aelUser;
    private PlannedCourse plannedCourse;
    private Date commentDate;
    private String commentText;
    private String userDisplayName;
    private String userImage;
    private String formattedDate;
    private Boolean removable = false;

    @Id
    @GeneratedValue(generator = "comments_seq", strategy =GenerationType.AUTO)
    public Long getId(){return super.getId();}


    @ManyToOne(fetch=FetchType.EAGER)
    @JsonIgnore
    @JoinColumn(name = "user_id", nullable = false)
    @NotNull
    @PropertyViewDescriptor(hidden = true)
    public AelUser getAelUser() {return aelUser;}

    public void setAelUser(AelUser aelUser) {
        this.aelUser = aelUser;
    }

    @ManyToOne(fetch=FetchType.EAGER)
    @JsonIgnore
    @JoinColumn(name = "planned_course_id", nullable = false)
    @NotNull
    @PropertyViewDescriptor(hidden = true)
    public PlannedCourse getPlannedCourse() {
        return plannedCourse;
    }

    public void setPlannedCourse(PlannedCourse plannedCourse) {
        this.plannedCourse = plannedCourse;
    }

    @Column(name = "date", nullable = false)
    @NotNull
    @PropertyViewDescriptor(hidden = true)
    public Date getCommentDate() {return commentDate;}

    public void setCommentDate(Date commentDate) {
        this.commentDate = commentDate;
    }

    @Column(name = "text", nullable = false)
    @NotNull
    @PropertyViewDescriptor(hidden = true)
    public String getCommentText() {return commentText;}

    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    @Transient
    @PropertyViewDescriptor(hidden=true)
    public String getUserDisplayName() {
        return userDisplayName;
    }

    public void setUserDisplayName(String userDisplayName) {
        this.userDisplayName = userDisplayName;
    }

    @Transient
    @PropertyViewDescriptor(hidden=true)
    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    @Transient
    public String getFormattedDate() {
        return formattedDate;
    }

    public void setFormattedDate(String formattedDate) {
        this.formattedDate = formattedDate;
    }

    @Transient
    @PropertyViewDescriptor(hidden=true)
    public Boolean getRemovable() {
        return removable;
    }

    public void setRemovable(Boolean removable) {
        this.removable = removable;
    }
}
