package ro.siveco.ael.lcms.domain.profile.model;

import org.joda.time.DateTime;
import java.util.Date;


public class UserDTO {
    public long id;
    public String username;
    public String password;
    public String first_name;
    public String last_name;
    public String mail;
    public DateTime create_date;
    public String display_name;
    public String phone;
    public String city;
    public String address;
    public String country;
    //data for user_details
    public Integer gender;
    public Date birth_date;
    public String image;
    //interests
    public Long[] selectedInterests;
    //preferences
    public String preferences_city;
    public String preferences_country;
    public String preferences_gender;
    public String preferences_birth_date;
    public String preferences_phone;
    public String preferences_mail;
    public String preferences_interests;
    public Boolean acceptTerms;

    /* visibility */
    public Boolean show_city;
    public Boolean show_country;
    public Boolean show_gender;
    public Boolean show_birth_date;
    public Boolean show_phone;
    public Boolean show_email;
    public Boolean show_interests;

    public String city_name;

    public String getCity_name() { return city_name; }
    public void setCity_name(String str) { this.city_name = str; }

    public String getPreferences_city() {
        return preferences_city;
    }

    public void setPreferences_city(String preferences_city) {
        this.preferences_city = preferences_city;
    }

    public String getPreferences_country() {
        return preferences_country;
    }

    public void setPreferences_country(String preferences_country) {
        this.preferences_country = preferences_country;
    }

    public String getPreferences_gender() {
        return preferences_gender;
    }

    public void setPreferences_gender(String preferences_gender) {
        this.preferences_gender = preferences_gender;
    }

    public String getPreferences_birth_date() {
        return preferences_birth_date;
    }

    public void setPreferences_birth_date(String preferences_birth_date) {
        this.preferences_birth_date = preferences_birth_date;
    }

    public String getPreferences_phone() {
        return preferences_phone;
    }

    public void setPreferences_phone(String preferences_phone) {
        this.preferences_phone = preferences_phone;
    }

    public String getPreferences_mail() {
        return preferences_mail;
    }

    public void setPreferences_mail(String preferences_mail) {
        this.preferences_mail = preferences_mail;
    }

    public String getPreferences_interests() {
        return preferences_interests;
    }

    public void setPreferences_interests(String preferences_interests) {
        this.preferences_interests = preferences_interests;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Long[] getSelectedInterests() {
        return selectedInterests;
    }

    public void setSelectedInterests(Long[] selectedInterests) {
        this.selectedInterests = selectedInterests;
    }

    public Date getBirth_date() {
        return birth_date;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setBirth_date(Date birth_date) {
        this.birth_date = birth_date;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public DateTime getCreate_date() {
        return create_date;
    }

    public void setCreate_date(DateTime create_date) {
        this.create_date = create_date;
    }

    public String getDisplay_name() {
        return display_name;
    }

    public void setDisplay_name(String display_name) {
        this.display_name = display_name;
    }

    public String getPhone() {
        return phone;
    }

    public UserDTO() {
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Boolean getAcceptTerms() {
        return acceptTerms;
    }

    public void setAcceptTerms(Boolean acceptTerms) {
        this.acceptTerms = acceptTerms;
    }

}
