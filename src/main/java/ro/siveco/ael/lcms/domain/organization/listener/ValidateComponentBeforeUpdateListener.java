package ro.siveco.ael.lcms.domain.organization.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import ro.siveco.ael.lcms.domain.organization.model.Component;
import ro.siveco.ael.lcms.domain.organization.service.ComponentService;
import ro.siveco.ram.app.exception.AppException;
import ro.siveco.ram.event.listener.type.single.BaseBeforeUpdateListener;
import ro.siveco.ram.event.type.single.BeforeUpdateEvent;

/**
 * Created by AndradaC on 9/1/2017.
 */
@Order(value = 2)
@org.springframework.stereotype.Component
public class ValidateComponentBeforeUpdateListener extends BaseBeforeUpdateListener<Component>{

    @Autowired
    private ComponentService componentService;

    @Override
    public void onApplicationEvent(BeforeUpdateEvent<Component> event) {
        Component component = event.getDomainModel();

        if (!componentService.validateComponent(component)) {
            throw new AppException("error.component.update.duplicateEntry");
        }
    }
}
