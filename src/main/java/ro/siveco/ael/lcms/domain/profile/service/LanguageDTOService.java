package ro.siveco.ael.lcms.domain.profile.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ro.siveco.ael.lcms.domain.profile.model.LanguageDTO;
import ro.siveco.ael.lcms.repository.LanguageDTORepository;
import ro.siveco.ram.starter.service.BaseDummyService;

/**
 * User: LucianR
 * Date: 2017-10-25
 */

@Service
public class LanguageDTOService extends BaseDummyService<LanguageDTO, String> {

    public LanguageDTOService(LanguageDTORepository languageDTORepository) {

        super(languageDTORepository, LanguageDTO.class);
    }

    @Override
    public Page<LanguageDTO> paginate(Pageable pageable) {

        Page<LanguageDTO> page = getResourceRepository().findAll(pageable);
        page.forEach(this::triggerAfterRead);
        return page;
    }
}
