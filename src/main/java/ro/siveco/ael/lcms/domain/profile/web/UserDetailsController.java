package ro.siveco.ael.lcms.domain.profile.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import ro.siveco.ael.lcms.domain.auth.model.AelUser;
import ro.siveco.ael.lcms.domain.course.service.PlannedCourseService;
import ro.siveco.ael.lcms.domain.profile.model.UserDetails;
import ro.siveco.ael.lcms.domain.profile.service.UserDetailsService;
import ro.siveco.ael.web.components.Template;
import ro.siveco.ael.web.components.WebPath;
import ro.siveco.ael.web.controllers.WithUserBaseEntityController;
import ro.siveco.ram.controller.annotation.HtmlMapping;
import ro.siveco.ram.controller.util.MappingUri;
import ro.siveco.ram.controller.util.ModelAttr;
import ro.siveco.ram.starter.controller.BaseEntityController;

import java.util.Optional;

/**
 * User: LucianR
 * Date: 2017-10-25
 */

@Controller
@RequestMapping("/profile")
public class UserDetailsController extends BaseEntityController<UserDetails> {

    private PlannedCourseService plannedCourseService;

    @Autowired
    public UserDetailsController(UserDetailsService userDetailsService, PlannedCourseService plannedCourseService) {

        super(userDetailsService);
        this.plannedCourseService = plannedCourseService;
    }

    @Override
    @HtmlMapping(path = MappingUri.RESOURCE_URI)
    public String view(@PathVariable(ModelAttr.ID_VARIABLE_NAME) Long id, ModelMap model) {

        AelUser aelUser = ((UserDetailsService)getRestService()).getUserById(id);
        model.put("plannedCourses",plannedCourseService.findAllByUserOrderByStartDate(aelUser.getId()) );
        model.put("attendedCourses", plannedCourseService.getAttendedPlannedCourses(aelUser));
        model.put("attendingCourses", plannedCourseService.getAttendingPlannedCourses(aelUser));
        return super.view(id, model);
    }

    @HtmlMapping(
            path = {"/create", "/edit/{id}"}
    )
    public String form(@PathVariable("id") Optional<Long> id, ModelMap model) {
        AelUser currentUser = (AelUser) WithUserBaseEntityController.getUser();
        if (id.isPresent() && !id.get().equals(currentUser.getId())) {
            return Template.ACCESS_DENIED;
        }
        return super.form(id, model);
    }

    @RequestMapping(value = WebPath.DELETE_PROFILE_PICTURE, method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity deletePicture(@RequestParam("userId") final long userId){
        ((UserDetailsService)getRestService()).deleteProfilePicture(userId);
        return new ResponseEntity(HttpStatus.CREATED);
    }
}
