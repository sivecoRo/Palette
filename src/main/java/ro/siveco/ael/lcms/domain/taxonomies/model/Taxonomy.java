package ro.siveco.ael.lcms.domain.taxonomies.model;

import ro.siveco.ram.model.Entity;

public interface Taxonomy<T> extends Entity<Long> {

    T getParent();

    void setParent(T parent);

//    List<T> getChildren();

//    void setChildren(List<T> children);
}