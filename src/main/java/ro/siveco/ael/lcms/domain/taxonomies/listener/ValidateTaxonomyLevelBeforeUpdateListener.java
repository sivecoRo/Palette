package ro.siveco.ael.lcms.domain.taxonomies.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.taxonomies.model.TaxonomyLevel;
import ro.siveco.ael.lcms.domain.taxonomies.service.TaxonomyLevelService;
import ro.siveco.ram.event.listener.type.single.BaseBeforeUpdateListener;
import ro.siveco.ram.event.type.single.BeforeUpdateEvent;

/**
 * Created by AndradaC on 9/1/2017.
 */
@Component
public class ValidateTaxonomyLevelBeforeUpdateListener extends BaseBeforeUpdateListener<TaxonomyLevel>{
    @Autowired
    private TaxonomyLevelService taxonomyLevelService;

    @Override
    public void onApplicationEvent(BeforeUpdateEvent<TaxonomyLevel> event) {
        TaxonomyLevel taxonomyLevel = event.getDomainModel();
        String validationResult = taxonomyLevelService.validateTaxonomyLevel(taxonomyLevel);
        if(!validationResult.isEmpty()){
            throw new RuntimeException(validationResult);
        }
    }
}
