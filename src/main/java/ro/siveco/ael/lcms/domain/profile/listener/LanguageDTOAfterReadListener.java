package ro.siveco.ael.lcms.domain.profile.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.auth.model.AelUser;
import ro.siveco.ael.lcms.domain.auth.service.AelUserService;
import ro.siveco.ael.lcms.domain.profile.model.LanguageDTO;
import ro.siveco.ael.lcms.domain.profile.service.UserDetailsService;
import ro.siveco.ael.web.controllers.WithUserBaseEntityController;
import ro.siveco.ram.event.listener.type.single.BaseAfterReadListener;
import ro.siveco.ram.event.type.single.AfterReadEvent;

@Component
public class LanguageDTOAfterReadListener extends BaseAfterReadListener<LanguageDTO> {

    private final UserDetailsService userDetailsService;
    private final AelUserService aelUserService;

    @Autowired
    public LanguageDTOAfterReadListener(UserDetailsService userDetailsService, AelUserService aelUserService) {
        this.userDetailsService = userDetailsService;
        this.aelUserService = aelUserService;
    }

    @Override
    public void onApplicationEvent(AfterReadEvent<LanguageDTO> event) {
        LanguageDTO languageDTO = event.getDomainModel();
        AelUser currentUser = (AelUser) WithUserBaseEntityController.getUser();
        languageDTO.setName(userDetailsService.getLocalizedLanguageName(languageDTO.getId(),aelUserService.getLocaleForUser(currentUser)));
    }
}
