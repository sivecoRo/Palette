package ro.siveco.ael.lcms.domain.chat.event;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionConnectEvent;
import ro.siveco.ael.lcms.domain.chat.web.ChatController;


@Component
public class StompConnectEvent implements ApplicationListener<SessionConnectEvent> {

    private ChatController chatController;
    
    
    @Autowired
	public StompConnectEvent(ChatController chatController) {
		this.chatController = chatController;
    }
 
    @Override
    public void onApplicationEvent(SessionConnectEvent event) {
        StompHeaderAccessor sha = StompHeaderAccessor.wrap(event.getMessage());

        if ( sha.getUser()==null ) {
        	return;
        }
        else {
        	chatController.sendUsersList(sha.getUser().getName(), true);
        }
        
    }
}
