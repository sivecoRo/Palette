package ro.siveco.ael.lcms.domain.notification.model;

import ro.siveco.ael.lcms.domain.auth.model.AelUser;

import java.util.Date;


public class UserMessageModel {

	private Long id;
	private String from;
	private String to;
	private String fromUsername;
	private AelUser fromUser;
	private String toUsername;
	private String subject;
	private String messageBody;
	private Date receivedDate;
	private Date sentDate;
	private Date readDate;
	private Boolean isReply;
	private String formattedSentDate;
	
	
	public UserMessageModel () {
		this.isReply = false;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getMessageBody() {
		return messageBody;
	}
	public void setMessageBody(String messageBody) {
		this.messageBody = messageBody;
	}
	public Date getReceivedDate() {
		return receivedDate;
	}
	public void setReceivedDate(Date receivedDate) {
		this.receivedDate = receivedDate;
	}
	public Date getReadDate() {
		return readDate;
	}
	public void setReadDate(Date readDate) {
		this.readDate = readDate;
	}

	public String getFormattedSentDate() {
		return formattedSentDate;
	}

	public void setFormattedSentDate(String formattedSentDate) {
		this.formattedSentDate = formattedSentDate;
	}

	@Override
	public String toString() {
		return "UserMessageModel [id=" + id + ", from=" + from + ", to=" + to + ", subject=" + subject
				+ ", messageBody=" + messageBody + ", receivedDate=" + receivedDate + "]";
	}
	public Boolean getIsReply() {
		return isReply;
	}
	public void setIsReply(Boolean isReply) {
		this.isReply = isReply;
	}

	public Date getSentDate() {
		return sentDate;
	}

	public void setSentDate(Date sentDate) {
		this.sentDate = sentDate;
	}

	public String getFromUsername() {
		return fromUsername;
	}

	public void setFromUsername(String fromUsername) {
		this.fromUsername = fromUsername;
	}

	public String getToUsername() {
		return toUsername;
	}

	public void setToUsername(String toUsername) {
		this.toUsername = toUsername;
	}

    public AelUser getFromUser() {
        return fromUser;
    }

    public void setFromUser(AelUser fromUser) {
        this.fromUser = fromUser;
    }
}
