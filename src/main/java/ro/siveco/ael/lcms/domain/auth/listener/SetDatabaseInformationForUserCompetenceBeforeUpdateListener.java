package ro.siveco.ael.lcms.domain.auth.listener;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.auth.model.UserCompetence;
import ro.siveco.ram.event.listener.type.single.BaseBeforeUpdateListener;
import ro.siveco.ram.event.type.single.BeforeUpdateEvent;

/**
 * Created by AndradaC on 9/3/2017.
 */
@Order(value = 1)
@Component
public class SetDatabaseInformationForUserCompetenceBeforeUpdateListener extends BaseBeforeUpdateListener<UserCompetence>{

    @Override
    public void onApplicationEvent(BeforeUpdateEvent<UserCompetence> event) {
        UserCompetence userCompetence = event.getDomainModel();
        UserCompetence dbUserCompetence = event.getStoredDomainModel();

        userCompetence.setUser(dbUserCompetence.getUser());
    }
}
