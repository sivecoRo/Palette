package ro.siveco.ael.lcms.domain.metadata.web;

import com.querydsl.core.types.Predicate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ro.siveco.ael.lcms.domain.metadata.model.Interest;
import ro.siveco.ael.lcms.domain.metadata.service.InterestService;
import ro.siveco.ael.web.components.Template;
import ro.siveco.ael.web.controllers.WithUserBaseEntityController;
import ro.siveco.ram.controller.annotation.HtmlMapping;

import java.util.List;
import java.util.Optional;

/**
 * Created by albertb on 2/2/2018.
 */

@Controller
@RequestMapping("/interests")
public class InterestController extends WithUserBaseEntityController<Interest> {

    @Autowired
    public InterestController(InterestService restService) {
        super(restService);
    }
}
