package ro.siveco.ael.lcms.domain.organization.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.siveco.ael.lcms.domain.organization.model.JobTitleCompetence;
import ro.siveco.ael.lcms.domain.taxonomies.model.Competence;
import ro.siveco.ael.lcms.domain.taxonomies.model.TaxonomyLevel;
import ro.siveco.ael.lcms.repository.JobTitleCompetenceRepository;
import ro.siveco.ram.starter.service.BaseEntityService;

import java.util.List;

/**
 * Created by JohnB on 21/06/2017.
 */

@Service
public class JobTitleCompetenceService extends BaseEntityService<JobTitleCompetence> {
    
    @Autowired
	public JobTitleCompetenceService(JobTitleCompetenceRepository resourceRepository) {
		super(JobTitleCompetence.class, resourceRepository);
	}

	public Boolean previousConnexionExists(JobTitleCompetence jobTitleCompetence){
        return ((JobTitleCompetenceRepository)getResourceRepository()).
                    findDuplicateConnexion(
                        jobTitleCompetence.getJobTitle().getId(),
                        jobTitleCompetence.getCompetence().getId(),
                        jobTitleCompetence.getId() != null ? jobTitleCompetence.getId() : 0L
                    ) != null;
    }

    public List<JobTitleCompetence> findJobTitleCompetencesByTenant(Long tenantId) {
        return ((JobTitleCompetenceRepository) getResourceRepository()).findJobTitleCompetencesByTenant(tenantId);
    }

    public Boolean competenceExists(Competence competence) {

        Boolean value = ((JobTitleCompetenceRepository) getResourceRepository()).findByCompetence(competence).isEmpty();
        return !value;
    }

    public Boolean taxonomyLevelExists(TaxonomyLevel taxonomyLevel) {

        Boolean value = ((JobTitleCompetenceRepository) getResourceRepository()).findByTaxonomyLevel(taxonomyLevel).isEmpty();
        return !value;
    }
}