package ro.siveco.ael.lcms.domain.auth.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.auth.model.AelUser;
import ro.siveco.ael.lcms.domain.auth.service.AelUserService;
import ro.siveco.ael.lcms.domain.auth.web.UserController;
import ro.siveco.ram.event.listener.type.single.BaseBeforeCreateListener;
import ro.siveco.ram.event.type.single.BeforeCreateEvent;
import ro.siveco.ram.starter.ui.model.Views;

/**
 * Created by AndradaC on 9/1/2017.
 */
@Order(value = 1)
@Component
public class ValidateUserBeforeCreateListener extends BaseBeforeCreateListener<AelUser> {

    @Autowired
    private AelUserService aelUserService;

    @Override
    public void onApplicationEvent(BeforeCreateEvent<AelUser> event) {

        AelUser aelUser = event.getDomainModel();

        UserController controller = (UserController) event.getSource();
        Class<? extends Views.View> view = controller.getRequestedView();
        if (ro.siveco.ael.lcms.domain.auth.view.Views.ImportUserView.class.getName().equalsIgnoreCase(view.getName())) {
            
        } else {
            aelUserService.validateUserForCreation(aelUser);
        }
    }
}
