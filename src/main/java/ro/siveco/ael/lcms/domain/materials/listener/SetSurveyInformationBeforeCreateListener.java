package ro.siveco.ael.lcms.domain.materials.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.auth.model.User;
import ro.siveco.ael.lcms.domain.materials.model.Survey;
import ro.siveco.ael.lcms.domain.materials.service.SurveyService;
import ro.siveco.ael.web.controllers.WithUserBaseEntityController;
import ro.siveco.ram.event.listener.type.single.BaseBeforeCreateListener;
import ro.siveco.ram.event.type.single.BeforeCreateEvent;

import java.util.UUID;

/**
 * Created by AndradaC on 8/31/2017.
 */
@Component
public class SetSurveyInformationBeforeCreateListener extends BaseBeforeCreateListener<Survey>{

    @Autowired
    private SurveyService surveyService;

    @Override
    public void onApplicationEvent(BeforeCreateEvent<Survey> event) {
        Survey survey = event.getDomainModel();

        User creator = WithUserBaseEntityController.getUser();
        if (survey.getCreator() == null) {
            survey.setCreator(creator);
        }

        //1. create learning material associated with the survey
        String surveyNodeId = UUID.randomUUID().toString();
        String name = survey.getDefinition();
        survey.setLearningMaterialNodeId(surveyNodeId);

        //2. update survey questions and answers with generic information
        survey = surveyService.updateSurveyInformation(survey);
    }
}
