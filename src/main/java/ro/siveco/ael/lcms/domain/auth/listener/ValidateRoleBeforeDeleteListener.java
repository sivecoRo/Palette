package ro.siveco.ael.lcms.domain.auth.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.auth.model.AelFunction;
import ro.siveco.ael.lcms.domain.auth.model.AelRole;
import ro.siveco.ael.lcms.domain.auth.model.Group;
import ro.siveco.ael.lcms.domain.auth.service.AelRoleService;
import ro.siveco.ram.app.exception.AppException;
import ro.siveco.ram.event.listener.type.single.BaseBeforeDeleteListener;
import ro.siveco.ram.event.type.single.BeforeDeleteEvent;

import java.util.List;

/**
 * Created by georgianaa on 1/9/2018.
 */
@Component
public class ValidateRoleBeforeDeleteListener extends BaseBeforeDeleteListener<AelRole> {

    @Autowired
    private AelRoleService aelRoleService;

    @Override
    public void onApplicationEvent(BeforeDeleteEvent<AelRole> event) {
        AelRole aelRole = event.getDomainModel();
        AelRole dbRole = aelRoleService.findById(aelRole.getId());

        if(dbRole == null) {
            throw new AppException("error.role.not.found");
        } else {
            List<AelFunction> aelFunctions = dbRole.getFunctions();
            if (aelFunctions != null && aelFunctions.size() > 0) {
                throw new AppException("error.role.cannot.be.deleted.functions");
            }
        }
    }
}
