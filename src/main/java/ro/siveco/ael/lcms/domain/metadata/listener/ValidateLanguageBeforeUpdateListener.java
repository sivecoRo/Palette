package ro.siveco.ael.lcms.domain.metadata.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.metadata.model.Language;
import ro.siveco.ael.lcms.domain.metadata.service.LanguageService;
import ro.siveco.ram.event.listener.type.single.BaseBeforeUpdateListener;
import ro.siveco.ram.event.type.single.BeforeUpdateEvent;

/**
 * Created by IuliaP on 13.10.2017.
 */
@Component
@Order(value = 1)
public class ValidateLanguageBeforeUpdateListener extends BaseBeforeUpdateListener<Language> {

    @Autowired
    LanguageService languageService;

    @Override
    public void onApplicationEvent(BeforeUpdateEvent<Language> event) {
        languageService.validateLanguage( event.getDomainModel() );
    }
}
