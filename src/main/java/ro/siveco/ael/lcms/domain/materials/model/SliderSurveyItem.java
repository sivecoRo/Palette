package ro.siveco.ael.lcms.domain.materials.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: Siveco Romania SA</p>
 * User: LiviuI
 * Date: 12/16/13
 * Time: 10:25 AM
 */

/**
 * Because there can be too many values between min and max we add the SurveyItemValues after
 * the survey is submitted.
 *
 * Therefore all <b>used</b> values are stored within surveyItem's values and the min and max are stored into SliderSurveyItem.
 */
@Entity
@Table( name = "slider_survey_item" )
@SequenceGenerator( name = "slider_survey_item_seq", sequenceName = "slider_survey_item_seq" )
public class SliderSurveyItem extends ro.siveco.ram.starter.model.base.BaseEntity implements Serializable
{
	/**
	 * The survey item that contains all the values used in this slider.
	 * The values include min and max which are added by default.
	 */
	SurveyItem surveyItem;

	/**
	 * The minimum value the slider can take
	 */
	SurveyItemValue min;

	/**
	 * The maximum value the slider can take
	 */
	SurveyItemValue max;


	@Id
	@GeneratedValue( generator = "slider_survey_item_seq", strategy = GenerationType.AUTO )
	public Long getId()
	{
		return super.getId();
	}

	@OneToOne
	@JoinColumn( name = "survey_item_id")
	public SurveyItem getSurveyItem()
	{
		return surveyItem;
	}

	public void setSurveyItem( SurveyItem surveyItem )
	{
		this.surveyItem = surveyItem;
	}

	@OneToOne
	@JoinColumn( name = "minimum_value_id")
	public SurveyItemValue getMin()
	{
		return min;
	}

	public void setMin( SurveyItemValue min )
	{
		this.min = min;
	}

	@OneToOne
	@JoinColumn( name = "maximum_value_id")
	public SurveyItemValue getMax()
	{
		return max;
	}

	public void setMax( SurveyItemValue max )
	{
		this.max = max;
	}
}
