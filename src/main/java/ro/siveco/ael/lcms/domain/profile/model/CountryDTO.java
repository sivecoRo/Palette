package ro.siveco.ael.lcms.domain.profile.model;

import ro.siveco.ram.model.Model;
import ro.siveco.ram.starter.ui.config.ActionDescriptor;
import ro.siveco.ram.starter.ui.config.PropertyViewDescriptor;
import ro.siveco.ram.starter.ui.config.ViewDescriptor;
import ro.siveco.ram.starter.ui.model.ActionType;
import ro.siveco.ram.starter.ui.model.ViewRendererType;

import javax.persistence.Transient;
import java.io.Serializable;

@ViewDescriptor
public class CountryDTO implements Model, Serializable{

    private String name;
    private String code;


    public String getName() {

        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getId() {

        return getName();
    }

    public void setId(String id) {

        this.name = id;
    }

    @Override
    public String getObjectUID() {

        return getName();
    }

    @Override
    @Transient
    @PropertyViewDescriptor(
            order = -2,/* First property */
            hiddenOnForm = true,
            hiddenOnGrid = true,
            displayLabelInPresentation = false,
            rendererType = ViewRendererType.OBJECT_LABEL,
            clickAction = {
                    @ActionDescriptor(type = ActionType.OPEN_PRESENTATION),
                    @ActionDescriptor(type = ActionType.LIST, backToFor = "{modelName}-OPEN_PRESENTATION")})
    public String getObjectLabel() {

        return getName();
    }
}
