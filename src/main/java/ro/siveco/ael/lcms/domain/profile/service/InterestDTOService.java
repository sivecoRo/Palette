package ro.siveco.ael.lcms.domain.profile.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ro.siveco.ael.lcms.domain.profile.model.InterestDTO;
import ro.siveco.ael.lcms.repository.InterestDTORepository;
import ro.siveco.ram.starter.service.BaseDummyService;

/**
 * Created by albertb on 1/30/2018.
 */

@Service
public class InterestDTOService extends BaseDummyService<InterestDTO, String> {
        public InterestDTOService(InterestDTORepository interestDTORepository)
        {
            super(interestDTORepository, InterestDTO.class);
        }

    @Override
    public Page<InterestDTO> paginate(Pageable pageable) {

        Page<InterestDTO> page = getResourceRepository().findAll(pageable);
        page.forEach(this::triggerAfterRead);
        return page;
    }

    }
