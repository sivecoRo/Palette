package ro.siveco.ael.lcms.domain.chat.model;

public class ChatUserModel {

	private String username;
	private String name;
	private Boolean active;
	private Boolean hasImage;
	
	
	public ChatUserModel() {}
	
	public ChatUserModel(String username, String name) {
		this.username = username;
		this.name = name;
		this.active = false;
		this.hasImage = false;
	}
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Boolean getActive() {
		return active;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}

	public Boolean getHasImage() {
		return hasImage;
	}

	public void setHasImage(Boolean hasImage) {
		this.hasImage = hasImage;
	}
	
}
