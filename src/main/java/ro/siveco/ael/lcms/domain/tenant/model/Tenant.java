package ro.siveco.ael.lcms.domain.tenant.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Type;
import org.hibernate.type.BinaryType;
import ro.siveco.ael.domain.annotations.ColumnComment;
import ro.siveco.ael.lcms.domain.auth.model.AelUser;
import ro.siveco.ael.lcms.domain.mail.model.MailConfiguration;
import ro.siveco.ram.model.TargetImplementation;
import ro.siveco.ram.starter.model.base.BaseEntity;
import ro.siveco.ram.starter.ui.config.PropertyViewDescriptor;
import ro.siveco.ram.starter.ui.config.ViewDescriptor;
import ro.siveco.ram.starter.ui.model.FormInputType;
import ro.siveco.ram.starter.ui.model.GridSize;
import ro.siveco.ram.starter.ui.model.IndexViewType;
import ro.siveco.ram.starter.ui.model.ViewRendererType;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by IulianB on 1/23/2015.
 */

@Entity
@Table( name = "tenant", uniqueConstraints = {@UniqueConstraint( columnNames = {"name"} )})
@SequenceGenerator( name = "TENANT_SEQ", sequenceName = "tenant_seq" )
@ViewDescriptor(pageSize = 10, viewTypes = {IndexViewType.GRID}, cardSizeLarge = GridSize.THREE_PER_ROW,
        defaultSearchProperty = "name")
public class Tenant extends BaseEntity implements Serializable {

    private Long id;

    private String name;

    private String phone;

    private String fax;

    private String email;

    private String address;

    @JsonIgnore
    private AelUser owner;

    private String language;

    private MailConfiguration mailConfiguration;

    private Boolean showCollaboration;

    private String plainOwnerPassword;

    private Boolean showForum;

    private Boolean active = Boolean.TRUE;

    private Boolean showPublicLibrary = Boolean.FALSE;

    private Boolean allowPublishToStore = Boolean.FALSE;

    private Boolean checkIPOnLogin = Boolean.FALSE;

    @ColumnComment(comment = "users will receive learning material recommendations")
    private Boolean predictiveAtContentLevel=Boolean.FALSE;

    @ColumnComment(comment = "users will receive course recommendations")
    private Boolean predictiveAtCourseLevel=Boolean.FALSE;

    private byte[] image;

    @Id
    @GeneratedValue( generator = "TENANT_SEQ", strategy = GenerationType.AUTO )
    @Column( name = "ID" )
    public Long getId()
    {
        return id;
    }

    public void setId( Long id )
    {
        this.id = id;
    }

    @Column( name = "NAME" )
    @PropertyViewDescriptor(inputType = FormInputType.TEXT, rendererType = ViewRendererType.STRING,
            editInline = false, order = 2, required = true)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column( name = "PHONE" )
    @PropertyViewDescriptor(inputType = FormInputType.TEXT, rendererType = ViewRendererType.STRING,
            editInline = false, order = 3, required = true)
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Column( name = "FAX" )
    @PropertyViewDescriptor(inputType = FormInputType.TEXT, rendererType = ViewRendererType.STRING,
            editInline = false, order = 4, required = true, hiddenOnGrid = true)
    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    @Column( name = "EMAIL" )
    @PropertyViewDescriptor(inputType = FormInputType.TEXT, rendererType = ViewRendererType.STRING,
            editInline = false, order = 5, required = true)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Column( name = "ADDRESS" )
    @PropertyViewDescriptor(inputType = FormInputType.TEXTAREA, rendererType = ViewRendererType.STRING,
            editInline = false, order = 6, required = true, hiddenOnGrid = true)
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Transient
    @PropertyViewDescriptor(inputType = FormInputType.SELECT, rendererType = ViewRendererType.MODEL,
            editInline = false, order = 8, required = true, hidden = true, hiddenOnForm = false)
    public AelUser getOwner() {
        return owner;
    }

    public void setOwner(AelUser owner) {
        this.owner = owner;
    }

    @Transient
    @PropertyViewDescriptor(inputType = FormInputType.LIST_OF_VALUES, rendererType = ViewRendererType.STRING,
            editInline = false, order = 9, required = true, hidden = true)
    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    @Transient
    @PropertyViewDescriptor(inputType = FormInputType.SELECT, rendererType = ViewRendererType.MODEL,
            editInline = false, order = 10, required = true, hidden = true, hiddenOnForm = false)
    public MailConfiguration getMailConfiguration() {
        return mailConfiguration;
    }

    public void setMailConfiguration(MailConfiguration mailConfiguration) {
        this.mailConfiguration = mailConfiguration;
    }

    @Column( name = "SHOW_COLLABORATION" )
    @PropertyViewDescriptor(inputType = FormInputType.CHECKBOX, rendererType = ViewRendererType.BOOLEAN,
            editInline = false, order = 11, required = false)
    public Boolean getShowCollaboration() {
        return showCollaboration;
    }

    public void setShowCollaboration(Boolean showCollaboration) {
        this.showCollaboration = showCollaboration;
    }

    @Column(name="SHOW_FORUM")
    @PropertyViewDescriptor(inputType = FormInputType.CHECKBOX, rendererType = ViewRendererType.BOOLEAN,
            editInline = false, order = 12, required = true, hidden = true)
    public Boolean getShowForum() {
        return showForum;
    }

    public void setShowForum(Boolean showForum) {
        this.showForum = showForum;
    }

    @Column(name="ACTIVE")
    @PropertyViewDescriptor(inputType = FormInputType.CHECKBOX, rendererType = ViewRendererType.BOOLEAN,
            editInline = false, order = 7, required = true)
    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    @Column(name="show_public_library")
    @PropertyViewDescriptor(inputType = FormInputType.CHECKBOX, rendererType = ViewRendererType.BOOLEAN,
            editInline = false, order = 13, required = true, hidden = true)
    public Boolean getShowPublicLibrary() {
        return showPublicLibrary;
    }

    public void setShowPublicLibrary(Boolean showPublicLibrary) {
        this.showPublicLibrary = showPublicLibrary;
    }

    @Column(name="allow_publish_to_store")
    @PropertyViewDescriptor(inputType = FormInputType.CHECKBOX, rendererType = ViewRendererType.BOOLEAN,
            editInline = false, order = 14, required = true, hidden = true)
    public Boolean getAllowPublishToStore() {
        return allowPublishToStore;
    }

    public void setAllowPublishToStore(Boolean allowPublishToStore) {
        this.allowPublishToStore = allowPublishToStore;
    }

    @Column(name="check_ip_on_login")
    @PropertyViewDescriptor(inputType = FormInputType.CHECKBOX, rendererType = ViewRendererType.BOOLEAN,
            editInline = false, order = 16, required = false)
    public Boolean getCheckIPOnLogin() {
        return checkIPOnLogin;
    }

    public void setCheckIPOnLogin(Boolean checkIPOnLogin) {
        this.checkIPOnLogin = checkIPOnLogin;
    }

    @Transient
    @PropertyViewDescriptor(inputType = FormInputType.TEXT, rendererType = ViewRendererType.STRING,
            editInline = false, order = 15, required = true, hidden = true)
    public String getPlainOwnerPassword() {
        return plainOwnerPassword;
    }

    public void setPlainOwnerPassword(String plainOwnerPassword) {
        this.plainOwnerPassword = plainOwnerPassword;
    }

    @Lob
    @Column(name = "image", columnDefinition = "bytea")
    @Type(type = "org.hibernate.type.BinaryType")
    @TargetImplementation(targetEntity = BinaryType.class)
    @PropertyViewDescriptor(inputType = FormInputType.IMAGE, rendererType = ViewRendererType.IMAGE,
            editInline = false, order = 17, required = true)
    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    @Column(name="predictive_content_level")
    @PropertyViewDescriptor(inputType = FormInputType.CHECKBOX, rendererType = ViewRendererType.BOOLEAN,
            editInline = false, order = 18, hidden = true)
    public Boolean getPredictiveAtContentLevel() {
        return predictiveAtContentLevel;
    }

    public void setPredictiveAtContentLevel(Boolean predictiveAtContentLevel) {
        this.predictiveAtContentLevel = predictiveAtContentLevel;
    }

    @Column(name="predictive_course_level")
    @PropertyViewDescriptor(inputType = FormInputType.CHECKBOX, rendererType = ViewRendererType.BOOLEAN,
            editInline = false, order = 19, hidden = true)
    public Boolean getPredictiveAtCourseLevel() {
        return predictiveAtCourseLevel;
    }

    public void setPredictiveAtCourseLevel(Boolean predictiveAtCourseLevel) {
        this.predictiveAtCourseLevel = predictiveAtCourseLevel;
    }

    @Override
    @Transient
    @PropertyViewDescriptor(hiddenOnGrid = true, hiddenOnForm = true)
    public String getObjectLabel() {
        return getName();
    }
}
