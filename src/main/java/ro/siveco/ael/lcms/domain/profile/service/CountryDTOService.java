package ro.siveco.ael.lcms.domain.profile.service;

import org.springframework.stereotype.Service;
import ro.siveco.ael.lcms.domain.profile.model.CountryDTO;
import ro.siveco.ael.lcms.repository.CountryDTORepository;
import ro.siveco.ram.starter.service.BaseDummyService;

@Service
public class CountryDTOService extends BaseDummyService<CountryDTO, String> {

    public CountryDTOService(CountryDTORepository countryDTORepository) {

        super(countryDTORepository, CountryDTO.class);
    }
}
