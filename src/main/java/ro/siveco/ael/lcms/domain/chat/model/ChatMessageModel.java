package ro.siveco.ael.lcms.domain.chat.model;

import java.util.Date;

public class ChatMessageModel {
  
	private String receiver;
	private String sender;
	private String message;
	
	private Date creationDate;

	
	public ChatMessageModel() {}

	public ChatMessageModel(String receiver, String sender, String message) {
		this.receiver = receiver;
		this.sender = sender;
		this.message = message;
	}

	public ChatMessageModel(String receiver, String sender, String message, Date creationDate) {
		this.receiver = receiver;
		this.sender = sender;
		this.message = message;
		this.creationDate = creationDate;
	}
	
	public String getReceiver() {
		return receiver;
	}
	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}
	public String getSender() {
		return sender;
	}
	public void setSender(String sender) {
		this.sender = sender;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
  
}
