package ro.siveco.ael.lcms.domain.menu.service;

import org.springframework.security.access.prepost.PostFilter;
import org.springframework.stereotype.Service;
import ro.siveco.ael.lcms.domain.menu.model.MenuItem;
import ro.siveco.ael.lcms.repository.MenuItemRepository;
import ro.siveco.ram.starter.service.BaseEntityService;

import java.util.List;

@Service
public class MenuItemService extends BaseEntityService<MenuItem> {

    private final static String MENU_FUNCTION_BASE = "type:entityManagement:menuItem:";

    public MenuItemService(MenuItemRepository resourceRepository) {
        super(MenuItem.class, resourceRepository);
    }

    @PostFilter("hasAuthority('" + MENU_FUNCTION_BASE + "' + filterObject.id)")
    public List<MenuItem> findAllByRights() {
        return getResourceRepository().findAll();
    }
}
