package ro.siveco.ael.lcms.domain.profile.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.auth.model.AelUser;
import ro.siveco.ael.lcms.domain.auth.service.AelUserService;
import ro.siveco.ael.lcms.domain.metadata.model.Country;
import ro.siveco.ael.lcms.domain.metadata.service.CountryService;
import ro.siveco.ael.lcms.domain.metadata.service.MetadataService;
import ro.siveco.ael.lcms.domain.profile.model.CountryDTO;
import ro.siveco.ael.lcms.domain.profile.model.UserDetails;
import ro.siveco.ael.lcms.domain.profile.service.UserDetailsService;
import ro.siveco.ael.service.ImageService;
import ro.siveco.ael.service.mail.utils.MailUtils;
import ro.siveco.ael.service.ram.SecurityService;
import ro.siveco.ram.app.exception.AppException;
import ro.siveco.ram.event.listener.type.single.BaseBeforeReadListener;
import ro.siveco.ram.event.type.single.BeforeReadEvent;

import java.util.List;
import java.util.Optional;

/**
 * User: LucianR
 * Date: 2017-10-25
 */
@Component
public class UserDetailsBeforeReadListener extends BaseBeforeReadListener<UserDetails> {

    /**
     * Handle an application event.
     *
     * @param event the event to respond to
     */

    private final UserDetailsService userDetailsService;

    private final AelUserService aelUserService;

    private final SecurityService securityService;

    private final MetadataService metadataService;

    private final CountryService countryService;

    private final ImageService imageService;

    private final MailUtils mailUtils;

    @Autowired
    public UserDetailsBeforeReadListener(UserDetailsService userDetailsService,
                                         AelUserService aelUserService,
                                         SecurityService securityService,
                                         MetadataService metadataService,
                                         CountryService countryService,
                                         ImageService imageService,
                                         MailUtils mailUtils) {

        this.userDetailsService = userDetailsService;
        this.aelUserService = aelUserService;
        this.securityService = securityService;
        this.metadataService = metadataService;
        this.countryService = countryService;
        this.imageService = imageService;
        this.mailUtils = mailUtils;
    }

    @Override
    public void onApplicationEvent(BeforeReadEvent<UserDetails> event) {

        AelUser aelUser = userDetailsService.getUserById(Long.valueOf(event.getObjectUID().toString()));

        Optional<UserDetails> userDetails = userDetailsService.getByAelUser(aelUser);
        if(!userDetails.isPresent()){
            throw new AppException("error.userDetail.update.profile");
        }
        userDetailsService.populateUserDetails(userDetails.get(), aelUser);

        if (userDetails.get().getImage() != null && userDetails.get().getImage().length > 0) {
            userDetails.get().setImageSrc(imageService.imageSrcFromBytes(userDetails.get().getImage()));
        }
        userDetails.get().setSendEmail(mailUtils.isMailEnabled(aelUser));
        event.setDomainModel(userDetails.get());
    }

    private Country getCountryByCountryDTO(CountryDTO countryDTO) {
        if (countryDTO == null) {
            return null;
        }
        List<Country> countries = countryService.findAllByCountryName(countryDTO.getName());
        if (countries != null && countries.size() >0) {
            return countries.get(0);
        }
        return null;

    }
}
