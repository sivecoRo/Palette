package ro.siveco.ael.lcms.domain.auth.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.springframework.data.domain.Sort;
import ro.siveco.ael.lcms.domain.auth.view.Views;
import ro.siveco.ael.lcms.domain.tenant.model.Tenant;
import ro.siveco.ael.lcms.domain.tenant.model.TenantHolder;
import ro.siveco.ram.starter.ui.config.ActionDescriptor;
import ro.siveco.ram.starter.ui.config.PropertyViewDescriptor;
import ro.siveco.ram.starter.ui.config.Sorter;
import ro.siveco.ram.starter.ui.config.ViewDescriptor;
import ro.siveco.ram.starter.ui.model.ActionType;
import ro.siveco.ram.starter.ui.model.FormInputType;
import ro.siveco.ram.starter.ui.model.IndexViewType;
import ro.siveco.ram.starter.ui.model.ViewRendererType;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "groups")
@SequenceGenerator(name = "SEQ_GEN", sequenceName = "groups_seq")
@ViewDescriptor(pageSize = 10, viewTypes = {IndexViewType.LIST},
        defaultViewType = IndexViewType.LIST,
        searchEnabled = true, defaultSearchProperty = "name",
        sort = {@Sorter(propertyName = "name", direction = Sort.Direction.ASC)},
        defaultActions = {
                @ActionDescriptor(
                        type = ActionType.OPEN_CREATE_FORM,
                        fixedAction = true
                ),         @ActionDescriptor(
                type = ActionType.OPEN_EDIT_FORM,
                mainAction = true
        ),         @ActionDescriptor(
                type = ActionType.DELETE,
                mainAction = true,
                requiresConfirmation = true
        ),         @ActionDescriptor(
                type = ActionType.CREATE,
                forwardFor = {"{modelName}-OPEN_CREATE_FORM"},
                availableOnlyInForm = true
        ),         @ActionDescriptor(
                type = ActionType.UPDATE,
                forwardFor = {"{modelName}-OPEN_EDIT_FORM"},
                availableOnlyInForm = true
        ),         @ActionDescriptor(
                type = ActionType.LIST,
                availableInForm = true,
                availableInPresentation = true,
                backToFor = {
                        "{modelName}-OPEN_CREATE_FORM",
                        "{modelName}-CREATE",
                        "{modelName}-OPEN_PRESENTATION",
                        "{modelName}-OPEN_EDIT_FORM",
                        "{modelName}-UPDATE",
                        "{modelName}-DELETE"}
        )},
        otherActions = {
                @ActionDescriptor(actionId = "associateRolesToGroupId", type = ActionType.OPEN_EDIT_FORM,
                        view = Views.AssociateRolesToGroupView.class)
        })
@ViewDescriptor(
        view = Views.AssociateRolesToGroupView.class,
        otherActions = {
                @ActionDescriptor(actionId = "associateRolesToGroupId", type = ActionType.OPEN_EDIT_FORM,
                        view = Views.AssociateRolesToGroupView.class),
                @ActionDescriptor(type = ActionType.UPDATE, forwardFor = "associateGroupsToUserId",
                        availableOnlyInForm = true),
                @ActionDescriptor(type = ActionType.LIST, availableInForm = true, inheritViewFromWrapper = false,
                        view = ro.siveco.ram.starter.ui.model.Views.__default.class, backToFor = {
                        "{modelName}-OPEN_EDIT_FORM",
                        "{modelName}-UPDATE"
                })}
)

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class AelGroup extends ro.siveco.ram.starter.model.base.BaseEntity implements Group, TenantHolder {
    private String name;

    private Tenant tenant;

    private String description;

    private String relatedUid;

    private Boolean readOnly = Boolean.FALSE;

    private Boolean imported = Boolean.FALSE;

    private List<User> users = new ArrayList<>();

    private List<AelRole> roles = new ArrayList<>();

    //set transient fields for form
    List<MultiSelectDTO> listAllRoles = new ArrayList<>();

    @Id
    @PropertyViewDescriptor(hidden = true)
    @GeneratedValue(generator = "groups_seq", strategy = GenerationType.SEQUENCE)
    public Long getId() {
        return super.getId();
    }

    @PropertyViewDescriptor(
            hiddenOnList = true,
            order = 1,
            inputType = FormInputType.TEXT, rendererType = ViewRendererType.STRING,
            orderInForm = 1)

    @Column(name = "imported", nullable = false)
    @PropertyViewDescriptor(hidden = true)
    public Boolean getImported() {
        return imported;
    }

    public void setImported(Boolean imported) {
        this.imported = imported;
    }

    @Column(name = "readonly", nullable = false)
    @PropertyViewDescriptor(hidden = true)
    public Boolean getReadOnly() {
        return readOnly;
    }

    public void setReadOnly(Boolean readOnly) {
        this.readOnly = readOnly;
    }

    @Column(name = "name", unique = true, nullable = false, length = 500)
    @PropertyViewDescriptor(
            hiddenOnList = true,
            order = 1, inputType = FormInputType.TEXT, rendererType = ViewRendererType.STRING,
            orderInForm = 2
    )
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "description", nullable = true, length = 2000)
    @PropertyViewDescriptor(
            hiddenOnList = true,
            order = 2,
            inputType = FormInputType.TEXT, rendererType = ViewRendererType.STRING, required = false,
            orderInForm = 2
    )
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "related_uid", unique = true, nullable = true, length = 500)
    @PropertyViewDescriptor(hidden = true)
    public String getRelatedUid() {
        return relatedUid;
    }

    public void setRelatedUid(String _relatedUid) {
        relatedUid = _relatedUid;
    }


    @ManyToMany(
            targetEntity = AelUser.class,
            cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.REFRESH}
    )
    @PropertyViewDescriptor(hidden = true)
    @JoinTable(
            name = "group_user",
            joinColumns = {@JoinColumn(name = "group_id")},
            inverseJoinColumns = {@JoinColumn(name = "user_id")},
            uniqueConstraints = {@UniqueConstraint(columnNames = {"group_id", "user_id"})}
    )
    @OrderBy("username")
    @JsonIgnore
    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    @ManyToMany(
            targetEntity = AelRole.class,
            cascade = {CascadeType.PERSIST, CascadeType.MERGE}
    )
    @PropertyViewDescriptor(hidden = true)
    @JoinTable(
            name = "group_role",
            joinColumns = {@JoinColumn(name = "group_id")},
            inverseJoinColumns = {@JoinColumn(name = "role_id")},
            uniqueConstraints = {@UniqueConstraint(columnNames = {"group_id", "role_id"})}
    )
    @JsonView(Views.AssociateRolesToGroupView.class)
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = AelRole.class)
    @PropertyViewDescriptor( view = ro.siveco.ram.starter.ui.model.Views.__default.class, hidden = true)
    @PropertyViewDescriptor(
            hiddenOnCard = true, hiddenOnGrid = true,
            view = Views.AssociateRolesToGroupView.class,
            rendererType = ViewRendererType.MODEL, inputType = FormInputType.LIST_OF_VALUES_MULTI_SELECT, orderInForm = 1)

    public List<AelRole> getRoles() {
        return roles;
    }

    public void setRoles(List<AelRole> roles) {
        this.roles = roles;
    }

    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "tenant_id", nullable = true)
    @PropertyViewDescriptor(hidden = true)
    public Tenant getTenant() {
        return tenant;
    }

    public void setTenant(Tenant tenant) {
        this.tenant = tenant;
    }

    @Transient
    @PropertyViewDescriptor(hiddenOnList = true, hiddenOnPresentation = true, hiddenOnForm = true)
    public List<MultiSelectDTO> getListAllRoles() {
        return listAllRoles;
    }

    public void setListAllRoles(List<MultiSelectDTO> listAllRoles) {
        this.listAllRoles = listAllRoles;
    }

    @Override
    public String toString() {
        return this.getName();
    }

    @Transient
    @PropertyViewDescriptor(
            order = -2,
            hiddenOnForm = true,
            hiddenOnGrid = true,
            displayLabelInPresentation = false,
            rendererType = ViewRendererType.OBJECT_LABEL,
            clickAction = {}
    )
    public String getObjectLabel() {

        return getName();
    }

}