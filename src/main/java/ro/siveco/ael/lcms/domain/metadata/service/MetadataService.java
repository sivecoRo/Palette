package ro.siveco.ael.lcms.domain.metadata.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.siveco.ael.lcms.domain.course.model.Course;
import ro.siveco.ael.lcms.domain.metadata.model.EntityType;
import ro.siveco.ael.lcms.domain.metadata.model.Metadata;
import ro.siveco.ael.lcms.domain.metadata.model.RelationType;
import ro.siveco.ael.lcms.domain.metadata.model.Tag;
import ro.siveco.ael.lcms.domain.profile.model.UserDetails;
import ro.siveco.ael.lcms.repository.MetadataRepository;
import ro.siveco.ram.starter.service.BaseEntityService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IuliaP on 12.06.2017.
 */
@Service
public class MetadataService extends BaseEntityService<Metadata> {

    private final TagService tagService;

    @Autowired
    public MetadataService(MetadataRepository metadataRepository,
                           TagService tagService) {
        super(Metadata.class, metadataRepository);
        this.tagService = tagService;
    }

    public List<Metadata> findByEntityIdAndEntityTypeAndRelationType(Long entityId, EntityType entityType, RelationType relationType) {
        return ((MetadataRepository)getResourceRepository()).findByEntityIdAndEntityTypeAndRelationType(entityId, entityType, relationType);
    }

    public List<Metadata> findByTag(Tag tag) {
        return ((MetadataRepository)getResourceRepository()).findByTag( tag );
    }
    public void delete(List<Metadata> metadata) {

        getResourceRepository().delete(metadata);
    }

    public void save(List<Metadata> metadata) {

        getResourceRepository().save(metadata);
    }

     public void updateMetadata(Course course)
     {
         List dbIncludedMetadata = findByEntityIdAndEntityTypeAndRelationType(course.getId(), EntityType.COURSE, RelationType.INCLUDED);
         List<Tag> includedTags = course.getIncludedTags();
         if(includedTags == null)
         {
             includedTags = new ArrayList<>();
         }

         delete(retain(dbIncludedMetadata,
                 buildMetadataFromTag(course.getId(), includedTags, EntityType.COURSE, RelationType.INCLUDED)));
         save(retain(
                 buildMetadataFromTag(course.getId(), includedTags, EntityType.COURSE, RelationType.INCLUDED),
                 dbIncludedMetadata));
     }

    public void updateMetadata(UserDetails userDetails)
    {
        List dbIncludedMetadata = findByEntityIdAndEntityTypeAndRelationType(userDetails.getUser().getId(), EntityType.USER, RelationType.INCLUDED);
        List<Tag> includedTags = userDetails.getIncludedTags();
        if(includedTags == null)
        {
            includedTags = new ArrayList<>();
        }

        delete(retain(dbIncludedMetadata,
                buildMetadataFromTag(userDetails.getUser().getId(), includedTags, EntityType.USER, RelationType.INCLUDED)));
        save(retain(
                buildMetadataFromTag(userDetails.getUser().getId(), includedTags, EntityType.USER, RelationType.INCLUDED),
                dbIncludedMetadata));
    }

    private List<Metadata> retain(List<Metadata> list1, List<Metadata> list2) {

        List<Metadata> temp = new ArrayList<>(list1);
        temp.removeAll(list2);
        return temp;
    }

    private List<Metadata> buildMetadataFromTag(Long entityId, List<Tag> tags, EntityType entityType,
                                                RelationType relationType) {

        List<Metadata> metadata = new ArrayList<>();
        if (entityId != null && tags != null) {
            for (Tag tag : tags) {
                Metadata m = new Metadata();
                m.setEntityId(entityId);
                m.setEntityType(entityType);
                m.setRelationType(relationType);
                m.setTag(tag);
                metadata.add(m);
            }
        }
        return metadata;
    }

    public List<Long> getEntityIdsForTags(EntityType entityType, RelationType relationType, List<Long> tagIds)
    {
        return ((MetadataRepository)getResourceRepository()).getEntityIdsForTags(entityType, relationType, tagIds);
    }

    public void populateUserTags(UserDetails userDetails) {
        if (userDetails != null) {
            userDetails.setIncludedTags(tagService.getTags(
                    findByEntityIdAndEntityTypeAndRelationType(userDetails.getUser().getId(), EntityType.USER,
                            RelationType.INCLUDED)));
        }
    }
}
