package ro.siveco.ael.lcms.domain.auth.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.auth.model.AelUser;
import ro.siveco.ael.lcms.domain.auth.service.AelUserService;
import ro.siveco.ram.event.listener.type.single.BaseBeforeUpdateListener;
import ro.siveco.ram.event.type.single.BeforeUpdateEvent;

/**
 * Created by AndradaC on 9/1/2017.
 */
@Order(value = 2)
@Component
public class SetDatabaseInformationForUserBeforeUpdateListener extends BaseBeforeUpdateListener<AelUser>{

    @Autowired
    AelUserService aelUserService;

    @Override
    public void onApplicationEvent(BeforeUpdateEvent<AelUser> event) {
        AelUser aelUser = event.getDomainModel();
        AelUser dbUser = event.getStoredDomainModel();

        aelUser.setGender(dbUser.getGender());
        aelUser.setTenant(dbUser.getTenant());
        aelUser.setPassword(dbUser.getPassword());
        aelUser.setRelatedUid(dbUser.getRelatedUid());
        aelUser.setDisplayName(aelUserService.getUserDisplayName(aelUser));
    }
}
