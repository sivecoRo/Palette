package ro.siveco.ael.lcms.domain.materials.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.materials.model.*;
import ro.siveco.ael.lcms.domain.materials.service.SliderSurveyItemService;
import ro.siveco.ael.lcms.domain.materials.service.SurveyItemValueService;
import ro.siveco.ael.lcms.domain.materials.service.SurveyService;
import ro.siveco.ram.event.listener.type.single.BaseBeforeDeleteListener;
import ro.siveco.ram.event.type.single.BeforeDeleteEvent;

import java.util.List;

/**
 * Created by AndradaC on 8/31/2017.
 */
@Component
public class DeleteAssociationsForSurveyBeforeDeleteListener extends BaseBeforeDeleteListener<Survey>{

    @Autowired
    private SurveyService surveyService;

    @Autowired
    private SurveyItemValueService surveyItemValueService;

    @Autowired
    private SliderSurveyItemService sliderSurveyItemService;

    @Override
    public void onApplicationEvent(BeforeDeleteEvent<Survey> event) {
        Survey survey = event.getDomainModel();

            List<SurveyItem> surveyItems = survey.getSurveyItems();
            for (SurveyItem surveyItem : surveyItems) {
                if (SurveyItemType.SLIDE.equals(surveyItem.getItemType())) {
                    try {
                        List<SurveyItemValue> surveyItemValues =
                                surveyItemValueService.findBySurveyItemOrderByDefinitionAsc(surveyItem);
                        SliderSurveyItem sliderSurveyItem = sliderSurveyItemService.findOneBySurveyItem(surveyItem);
                        sliderSurveyItemService.remove(sliderSurveyItem);
                        surveyItemValueService.remove(surveyItemValues);
                    } catch (Exception e) {
                        // throw new RuntimeException(getText("attached.survey"));
                    }

                }
            }

        }
}
