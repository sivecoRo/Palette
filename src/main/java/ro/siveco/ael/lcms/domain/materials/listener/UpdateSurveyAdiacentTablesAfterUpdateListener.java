package ro.siveco.ael.lcms.domain.materials.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.materials.model.Survey;
import ro.siveco.ael.lcms.domain.materials.service.SurveyService;
import ro.siveco.ram.event.listener.type.single.BaseAfterUpdateListener;
import ro.siveco.ram.event.type.single.AfterUpdateEvent;

/**
 * Created by AndradaC on 8/31/2017.
 */
@Component
public class UpdateSurveyAdiacentTablesAfterUpdateListener extends BaseAfterUpdateListener<Survey>{
    @Autowired
    private SurveyService surveyService;

    @Override
    public void onApplicationEvent(AfterUpdateEvent<Survey> event) {
        Survey updatedSurvey = event.getDomainModel();
        surveyService.addSurveySlider(updatedSurvey);
    }
}
