package ro.siveco.ael.lcms.domain.metadata.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.metadata.model.Relation;
import ro.siveco.ael.lcms.domain.metadata.service.RelationService;
import ro.siveco.ram.event.listener.type.single.BaseBeforeUpdateListener;
import ro.siveco.ram.event.type.single.BeforeUpdateEvent;

/**
 * Created by IuliaP on 13.10.2017.
 */
@Component
@Order(value = 1)
public class ValidateRelationBeforeUpdateListener extends BaseBeforeUpdateListener<Relation> {

    @Autowired
    RelationService relationService;

    @Override
    public void onApplicationEvent(BeforeUpdateEvent<Relation> event) {
        relationService.validateRelation( event.getDomainModel() );
    }
}
