package ro.siveco.ael.lcms.domain.organization.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ro.siveco.ael.lcms.domain.organization.model.Component;
import ro.siveco.ael.lcms.domain.tenant.model.Tenant;
import ro.siveco.ael.lcms.repository.ComponentRepository;
import ro.siveco.ram.security.annotation.CanListEntity;
import ro.siveco.ram.starter.service.BaseEntityService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by AndradaC on 5/29/2017.
 */
@Service
public class ComponentService extends BaseEntityService<Component>
{
    @Autowired
    public ComponentService(ComponentRepository componentRepository) {
        super(Component.class, componentRepository);
    }

    public Component getRootComponent()
    {
        return ((ComponentRepository)getResourceRepository()).getRootComponent();
    }
    public Component findByNameAndTenant(String name, Tenant tenant)
    {
        return (Component) ((ComponentRepository) getResourceRepository()).findByNameAndTenant(name, tenant);
    }

    public Component findById(Long componentId) {
        return (Component) ((ComponentRepository) getResourceRepository()).findOne(componentId);
    }

    public Component findOneByTenantId(Long tenantId)
    {
        return (Component) ((ComponentRepository) getResourceRepository()).getByTenantId(tenantId);
    }

    public Boolean validateComponent(Component component) {
        Boolean isNewComponent = component.getId() == null;
        Component componentWithSameName = findByNameAndTenant(component.getName(), component.getTenant());
        if(componentWithSameName != null) {
            if(isNewComponent || component.getId().compareTo(componentWithSameName.getId()) != 0) {
                return Boolean.FALSE;
            }
        }
        return Boolean.TRUE;
    }

    @CanListEntity
    final public Page<Component> paginate(Pageable pageable, Component parent) {
        List<Component> childrenList = new ArrayList<>();
        for(Component component : parent.getChildren()){
            childrenList.add(component);
        }
        return new PageImpl<>(childrenList, pageable, parent.getChildren().size());
    }
    public Component saveOrUpdate(Component component)
    {
        if(component.getId() == null)
        {
            return save(component);
        }
        else
        {
            return update(component);
        }
    }

    public String connexionsExist(Long componentId) {

        Component component = ((ComponentRepository) getResourceRepository()).findOne(componentId);
        // TODO: 7/31/2017  
        return null;
    }


}
