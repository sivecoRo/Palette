package ro.siveco.ael.lcms.domain.auth.service;

import org.springframework.stereotype.Service;
import ro.siveco.ael.lcms.domain.auth.model.AelUser;
import ro.siveco.ael.lcms.domain.auth.model.UserGroup;
import ro.siveco.ael.lcms.repository.AelUserRepository;
import ro.siveco.ael.lcms.repository.UserGroupRepository;
import ro.siveco.ram.service.exception.ResourceNotFoundException;
import ro.siveco.ram.service.exception.ResourceUpdateFailedException;
import ro.siveco.ram.starter.service.BaseEntityService;

/**
 * User: AlexandruVi
 * Date: 2018-01-22
 */
@Service
public class UserGroupService extends BaseEntityService<UserGroup> {

    private final AelUserRepository userRepository;

    public UserGroupService(UserGroupRepository resourceRepository,
                            AelUserRepository userRepository) {

        super(UserGroup.class, resourceRepository);
        this.userRepository = userRepository;
    }

    @Override
    public UserGroup update(UserGroup resource) throws ResourceNotFoundException, ResourceUpdateFailedException {

        AelUser user=userRepository.get(resource.getId());
        user.setGroups(resource.getGroups());
        userRepository.save(user);
        return resource;
    }
}
