package ro.siveco.ael.lcms.domain.materials.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import ro.siveco.ael.lcms.domain.auth.model.AelGroup;
import ro.siveco.ael.lcms.domain.auth.model.AelUser;
import ro.siveco.ael.lcms.domain.auth.model.User;
import ro.siveco.ael.lcms.domain.auth.service.AelGroupService;
import ro.siveco.ael.lcms.domain.auth.service.AelUserService;
import ro.siveco.ael.lcms.domain.course.model.*;
import ro.siveco.ael.lcms.domain.course.service.*;
import ro.siveco.ael.lcms.domain.materials.model.*;
import ro.siveco.ael.lcms.repository.SurveyRepository;
import ro.siveco.ael.qti.domain.model.InteractionType;
import ro.siveco.ael.service.security.annotation.Groups;
import ro.siveco.ram.repository.BaseEntityRepository;
import ro.siveco.ram.starter.service.BaseEntityService;

import java.util.*;

/**
 * Created by AndradaC on 4/27/2017.
 */
@Service
public class SurveyService extends BaseEntityService<Survey> {
    private final SliderSurveyItemService sliderSurveyItemService;
    private final AelUserService aelUserService;
    private final AelGroupService aelGroupService;
    private final PlannedCourseService plannedCourseService;
    private final CourseService courseService;
    private final PlannedCourseStudentService plannedCourseStudentService;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    public SurveyService(BaseEntityRepository<Survey> resourceRepository,
                         SliderSurveyItemService sliderSurveyItemService,
                         AelUserService aelUserService,
                         AelGroupService aelGroupService, PlannedCourseService plannedCourseService,
                         CourseService courseService,
                         PlannedCourseStudentService plannedCourseStudentService) {
        super(Survey.class, resourceRepository);
        this.sliderSurveyItemService = sliderSurveyItemService;
        this.aelUserService = aelUserService;
        this.aelGroupService = aelGroupService;
        this.plannedCourseService = plannedCourseService;
        this.courseService = courseService;
        this.plannedCourseStudentService = plannedCourseStudentService;
    }

    public Survey findOneByDefinitionAndCreator(String name, User creator) {
        return ((SurveyRepository) getResourceRepository()).findOneByDefinitionAndCreator(name, creator);
    }

    public Survey findOneById(Long id) {
        return ((SurveyRepository) getResourceRepository()).findOneById(id);
    }

    public void addSliderItem(SurveyItem surveyItem) {
        SliderSurveyItem sliderItem = null;
        if (surveyItem.getId() != null) {
            sliderItem = sliderSurveyItemService.findOneBySurveyItem(surveyItem);
        }

        if (sliderItem == null) {
            sliderItem = new SliderSurveyItem();
        }

        if (!surveyItem.equals(sliderItem.getSurveyItem())) {
            sliderItem.setSurveyItem(surveyItem);
        }

        List<SurveyItemValue> surveyValues = surveyItem.getSurveyItemValues();

        Collections.sort(surveyValues, SliderValueUtils.VALUE_ASC);

        if (surveyValues.size() >= 2) {
            sliderItem.setMin(surveyValues.get(0));

            sliderItem.setMax(surveyValues.get(surveyValues.size() - 1));
        }

        if (sliderItem.getId() != null) {
            sliderSurveyItemService.update(sliderItem);
        } else {
            sliderSurveyItemService.save(sliderItem);
        }
    }

    public Survey buildSurveyToDisplay(Survey survey)
    {
        List<SurveyItem> dbSurveyItems = survey.getSurveyItems();
        List<SurveyItem> editorSurveyItems = survey.getSurveyItems();
        for (SurveyItem surveyItem : dbSurveyItems) {
            surveyItem.setSurvey(survey);
            if(SurveyItemType.LIST_OF_VALUES.equals(surveyItem.getItemType())) {
                if(SelectionMethod.SINGLE.equals(surveyItem.getSelectionMethod())) {
                    surveyItem.setInteractionType(InteractionType.SINGLE_CHOICE);
                }
                else
                {
                    surveyItem.setInteractionType(InteractionType.MULTIPLE_CHOICE);
                }
            }
            else if(SurveyItemType.FREE_TEXT.equals(surveyItem.getItemType()))
            {
                surveyItem.setInteractionType(InteractionType.ESSAY);
            }
            else if(SurveyItemType.SLIDE.equals(surveyItem.getItemType())  )
            {
                surveyItem.setInteractionType(InteractionType.SLIDER);
                SliderSurveyItem sliderSurveyItem = sliderSurveyItemService.findOneBySurveyItem(surveyItem);

                SurveyItemValue sivMin = sliderSurveyItem.getMin();
                surveyItem.setSliderMinVal(Integer.parseInt(sivMin.getDefinition()));

                SurveyItemValue sivMax = sliderSurveyItem.getMax();
                surveyItem.setSliderMaxVal(Integer.parseInt(sivMax.getDefinition()));
            }

         //   editorSurveyItems.add(surveyItem);
        }
       // survey.setSurveyItems(editorSurveyItems);

        return survey;
    }

    public Survey updateSurveyInformation(Survey survey)
    {
        List<SurveyItem> editorSurveyItems = survey.getSurveyItems();
        long questionNo =1;
        for (SurveyItem surveyItem : editorSurveyItems) {
            surveyItem.setSurvey(survey);
            Locale locale = aelUserService.getLocaleForUser((AelUser)survey.getCreator());
            surveyItem.setCode(messageSource
                    .getMessage("label.survey.question", null, "label.survey.question", locale) + " " + questionNo);
            surveyItem.setOrder(questionNo);
            questionNo++;

            switch (surveyItem.getInteractionType())
            {
                case SINGLE_CHOICE:
                    surveyItem.setSelectionMethod(SelectionMethod.SINGLE);
                    surveyItem.setItemType(SurveyItemType.LIST_OF_VALUES);
                    break;
                case MULTIPLE_CHOICE:
                    surveyItem.setSelectionMethod(SelectionMethod.MULTIPLE);
                    surveyItem.setItemType(SurveyItemType.LIST_OF_VALUES);
                    break;
                case SLIDER:
                    surveyItem.setSelectionMethod(SelectionMethod.SINGLE);
                    surveyItem.setItemType(SurveyItemType.SLIDE);
                    SurveyItemValue sivMin = new SurveyItemValue();
                    sivMin.setDefinition(surveyItem.getSliderMinVal().toString());
                    SurveyItemValue sivMax = new SurveyItemValue();
                    sivMax.setDefinition(surveyItem.getSliderMaxVal().toString());
                    surveyItem.setSurveyItemValues(Arrays.asList(sivMin,sivMax));
                    break;
                case ESSAY:
                    surveyItem.setSelectionMethod(SelectionMethod.SINGLE);
                    surveyItem.setItemType(SurveyItemType.FREE_TEXT);
            }

            List<SurveyItemValue> editorSurveyItemValues = surveyItem.getSurveyItemValues();
            if(editorSurveyItemValues != null && editorSurveyItemValues.size() > 0) {
                long answerNo = 0;
                for (SurveyItemValue siv : editorSurveyItemValues) {
                    siv.setSurveyItem(surveyItem);
                    siv.setCode("OPTION_" + (answerNo + 1));
                    siv.setOrder(answerNo);
                    answerNo++;
                    siv.setHasText(false);
                }
            }
        }

       return survey;
    }



    private List<AelUser> getAllStudentsForTenant(Long tenantId)
    {
        AelGroup studentsGroup = aelGroupService.getByName(Groups.Default.Students.get());
        List<AelUser> studentsForSurvey = aelUserService.getUsersByGroupAndTenant(studentsGroup.getId(), tenantId);

        return studentsForSurvey;
    }

    private Map<Long, Long> getDbAlreadyAssignedStudents(PlannedCourse plannedCourse)
    {
        Map<Long, Long> dbStudentIdsMap = new HashMap<>();

        List<PlannedCourseStudent> dbAlreadyAssignedStudents = plannedCourse.getPlannedCourseStudents();
        for(PlannedCourseStudent student: dbAlreadyAssignedStudents){
            dbStudentIdsMap.put(student.getUser().getId(), student.getId());
        }

        return dbStudentIdsMap;
    }

    private PlannedCourse findPlannedCourseForSurveyCourse(Course surveyCourse){
        PlannedCourse plannedCourseForSurvey = null;
        List<PlannedCourse> plannedCourseListForSurveyCourse = plannedCourseService.findByCourseIdAndStatus(
                surveyCourse.getId(),
                PlannedCourseStatus.RUNNING);
        if(plannedCourseListForSurveyCourse != null && plannedCourseListForSurveyCourse.size()>1){
            throw new RuntimeException("error.surveyUsersAssociation.addStudentsForSurvey.moreThanOnePlannedCourse");
        }else if(plannedCourseListForSurveyCourse != null && plannedCourseListForSurveyCourse.size() == 1){
            plannedCourseForSurvey = plannedCourseListForSurveyCourse.get(0);
        }
        return plannedCourseForSurvey;
    }

    public void addSurveySlider(Survey survey) {

        if (survey != null) {
            for (SurveyItem si : survey.getSurveyItems()) {
                if (si.getItemType().equals(SurveyItemType.SLIDE)) {
                    addSliderItem(si);
                }
            }
        }
    }

}
