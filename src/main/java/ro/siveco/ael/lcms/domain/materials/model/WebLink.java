package ro.siveco.ael.lcms.domain.materials.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import ro.siveco.ael.lcms.domain.auth.model.AelUser;
import ro.siveco.ael.lcms.domain.auth.model.User;
import ro.siveco.ram.starter.ui.config.PropertyViewDescriptor;
import ro.siveco.ram.starter.ui.config.ViewDescriptor;
import ro.siveco.ram.starter.ui.model.FormInputType;
import ro.siveco.ram.starter.ui.model.IndexViewType;
import ro.siveco.ram.starter.ui.model.ViewRendererType;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by AndradaC on 5/4/2017.
 */
@Entity
@Table(name = "web_links")
@SequenceGenerator(name = "web_links_seq", sequenceName = "web_links_seq")
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@ViewDescriptor(pageSize = 10, viewTypes = {IndexViewType.CARD}, searchEnabled = true, defaultSearchProperty = "name")
public class WebLink extends ro.siveco.ram.starter.model.base.BaseEntity implements Serializable {
    private String name;

    private String address;

    private User creator;

    private String learningMaterialNodeId;


    @Id
    @GeneratedValue(generator = "web_links_seq", strategy = GenerationType.AUTO)
    @PropertyViewDescriptor(hiddenOnCard = true)
    public Long getId() {
        return super.getId();
    }

    @Column(name = "name")
    @PropertyViewDescriptor(inputType = FormInputType.TEXT, rendererType = ViewRendererType.STRING,
            editInline = false, order = 2, required = true, hidden = true)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "address")
    @PropertyViewDescriptor(inputType = FormInputType.TEXT, rendererType = ViewRendererType.STRING,
            editInline = false, order = 3, required = true,
            inputFieldValidationPattern = "/^(https?|ftp):\\/\\/(((([a-z]|\\d|-|\\.|_|~|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])|(%[\\da-f]{2})|[!\\$&'\\(\\)\\*\\+,;=]|:)*@)?(((\\d|[1-9]\\d|1\\d\\d|2[0-4]\\d|25[0-5])\\.(\\d|[1-9]\\d|1\\d\\d|2[0-4]\\d|25[0-5])\\.(\\d|[1-9]\\d|1\\d\\d|2[0-4]\\d|25[0-5])\\.(\\d|[1-9]\\d|1\\d\\d|2[0-4]\\d|25[0-5]))|((([a-z]|\\d|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])|(([a-z]|\\d|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])([a-z]|\\d|-|\\.|_|~|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])*([a-z]|\\d|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])))\\.)+(([a-z]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])|(([a-z]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])([a-z]|\\d|-|\\.|_|~|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])*([a-z]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])))\\.?)(:\\d*)?)(\\/((([a-z]|\\d|-|\\.|_|~|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])|(%[\\da-f]{2})|[!\\$&'\\(\\)\\*\\+,;=]|:|@)+(\\/(([a-z]|\\d|-|\\.|_|~|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])|(%[\\da-f]{2})|[!\\$&'\\(\\)\\*\\+,;=]|:|@)*)*)?)?(\\?((([a-z]|\\d|-|\\.|_|~|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])|(%[\\da-f]{2})|[!\\$&'\\(\\)\\*\\+,;=]|:|@)|[\\uE000-\\uF8FF]|\\/|\\?)*)?(\\#((([a-z]|\\d|-|\\.|_|~|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])|(%[\\da-f]{2})|[!\\$&'\\(\\)\\*\\+,;=]|:|@)|\\/|\\?)*)?$/i")
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @ManyToOne(targetEntity = AelUser.class)
    @JoinColumn(name = "creator_id")
    @PropertyViewDescriptor(inputType = FormInputType.SELECT, rendererType = ViewRendererType.MODEL,
            editInline = false, order = 6, required = false, hidden = true, hiddenOnForm = true)
    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    @Column(name = "learning_material_node_id", length = 500)
    @PropertyViewDescriptor(inputType = FormInputType.TEXT, rendererType = ViewRendererType.STRING,
            editInline = false, order = 7, hidden = true, required = false, hiddenOnForm = true)
    public String getLearningMaterialNodeId() {
        return learningMaterialNodeId;
    }

    public void setLearningMaterialNodeId(String learningMaterialNodeId) {
        this.learningMaterialNodeId = learningMaterialNodeId;
    }

    @Override
    @Transient
    public String getObjectLabel() {

        return getName();
    }
}
