package ro.siveco.ael.lcms.domain.course.web;

import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.NumberPath;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;
import ro.siveco.ael.lcms.domain.auth.model.AelGroup;
import ro.siveco.ael.lcms.domain.auth.model.AelUser;
import ro.siveco.ael.lcms.domain.auth.model.User;
import ro.siveco.ael.lcms.domain.auth.service.AelGroupService;
import ro.siveco.ael.lcms.domain.auth.service.AelUserService;
import ro.siveco.ael.lcms.domain.course.model.*;
import ro.siveco.ael.lcms.domain.course.service.CourseFilterService;
import ro.siveco.ael.lcms.domain.course.service.CourseService;
import ro.siveco.ael.lcms.domain.course.service.PlannedCourseService;
import ro.siveco.ael.lcms.domain.course.service.PlannedCourseStudentService;
import ro.siveco.ael.lcms.domain.metadata.model.Interest;
import ro.siveco.ael.lcms.domain.metadata.model.Tag;
import ro.siveco.ael.lcms.domain.metadata.service.InterestService;
import ro.siveco.ael.lcms.domain.metadata.service.TagService;
import ro.siveco.ael.lcms.domain.notification.service.NotificationManagerService;
import ro.siveco.ael.service.security.annotation.Groups;
import ro.siveco.ael.web.components.Template;
import ro.siveco.ael.web.components.WebPath;
import ro.siveco.ael.web.controllers.WithUserBaseEntityController;
import ro.siveco.ram.controller.annotation.AjaxHtmlMapping;
import ro.siveco.ram.controller.annotation.HtmlMapping;
import ro.siveco.ram.controller.util.MappingUri;
import ro.siveco.ram.controller.util.ModelAttr;
import ro.siveco.ram.service.exception.ResourceConflictException;
import ro.siveco.ram.service.exception.ResourceCreateFailedException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.security.Principal;
import java.util.*;

/**
 * Created by LiviuI on 4/19/2017.
 */
@Controller
@RequestMapping(WebPath.COURSES)
public class CourseController extends WithUserBaseEntityController<Course> {
	private final NotificationManagerService notificationManagerService;
	private final PlannedCourseService plannedCourseService;
	private final PlannedCourseStudentService plannedCourseStudentService;
	private final AelUserService aelUserService;
	private final AelGroupService aelGroupService;
	private Integer materialListPageSize = 3;
	private final CourseFilterService courseFilterService;
	private final TagService tagService;
	private final InterestService interestService;

	@Autowired
	public CourseController(CourseService restService, NotificationManagerService notificationManagerService,
							PlannedCourseService plannedCourseService,
							PlannedCourseStudentService plannedCourseStudentService,
							AelUserService aelUserService, AelGroupService aelGroupService,
							CourseFilterService courseFilterService, TagService tagService,
							InterestService interestService) {
		super(restService);
		this.notificationManagerService = notificationManagerService;
		this.plannedCourseService = plannedCourseService;
		this.plannedCourseStudentService = plannedCourseStudentService;
		this.aelUserService = aelUserService;
		this.aelGroupService = aelGroupService;
		this.courseFilterService = courseFilterService;
		this.tagService = tagService;
		this.interestService = interestService;
	}

	protected Optional<BooleanExpression> buildFilter(MultiValueMap<String, String> parameters) {

		NumberPath<Long> userIdPath = QCourse.course.creator.id;
		BooleanExpression filterExpression = userIdPath.eq(WithUserBaseEntityController.getUser().getId());

		filterExpression = filterExpression.and(courseFilterService.getFilterForNotExpired());

		if(parameters != null && parameters.size() > 0)
		{
			return courseFilterService.createFilterForParameters(parameters, filterExpression);
		}
		else {
			return Optional.of(filterExpression);
		}
	}

	@HtmlMapping(
			path = {"/"}
	)
	public String index(ModelMap model, @QuerydslPredicate Optional<Predicate> predicate, Pageable pageable, @RequestParam MultiValueMap<String, String> parameters) {

		Principal principal = SecurityContextHolder.getContext().getAuthentication();
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes())
				.getRequest();
		AelUser currentUser = (AelUser) WithUserBaseEntityController.getUser();
		List<Interest> userInterestList = interestService.getI18NSelectedInterests(principal, request);
		if(userInterestList != null && userInterestList.size() > 0)
		{
			model.put("userTags", userInterestList);
		}
		else
		{
			model.put("userTags", new ArrayList<Tag>());
		}

		List<Interest> notSelectedInterests = interestService.getI18NNotSelectedInterests(principal,request);
		if(notSelectedInterests != null && notSelectedInterests.size() > 0)
		{
			model.put("otherTags", notSelectedInterests);
		}
		else
		{
			model.put("otherTags",new ArrayList<Tag>());
		}

		model.put("notSelectedInterests", notSelectedInterests);

		model.put("pastCourses", ((CourseService)getRestService()).getPastCourses(currentUser));
		model.put("attendedCourses", plannedCourseService.getAttendedPlannedCourses(currentUser));
		model.put("attendingCourses", plannedCourseService.getAttendingPlannedCourses(currentUser));

		return super.index(model, predicate, pageable, parameters);
	}

	@Override
	@HtmlMapping(path = MappingUri.RESOURCE_URI)
	public String view(@PathVariable(ModelAttr.ID_VARIABLE_NAME) Long id, ModelMap model) {

		setCourseAttributesToDisplay(id, model);

		return super.view(id, model);
	}

	private void setCourseAttributesToDisplay(@PathVariable(ModelAttr.ID_VARIABLE_NAME) Long id, ModelMap model) {

		List<PlannedCourse> course_sessions = plannedCourseService.findByCourseId(id);
		model.addAttribute("course_sessions", course_sessions);

		if (course_sessions.size() > 0 && course_sessions.get(0).getTeacher() != null) {
			model.addAttribute("has_teacher", true);
		} else {
			model.addAttribute("has_teacher", false);
		}

		if (course_sessions.size() > 0) {
			PlannedCourse session = course_sessions.get(0);
			Date currentDate = new Date();
		}

		User user = getUser();
		Long tenantId = aelUserService.getTenantIdByUsername(user.getUsername());
		AelGroup studentsGroup = aelGroupService.getByName(Groups.Default.Students.get());
		List<AelUser> studentsFromTenant = aelUserService.getUsersByGroupAndTenant(studentsGroup.getId(), tenantId);
		model.addAttribute("studentsList", studentsFromTenant);
	}

	@Override
	@AjaxHtmlMapping(path = MappingUri.RESOURCE_URI)
	public String viewFragment(@PathVariable(ModelAttr.ID_VARIABLE_NAME) Long id, ModelMap model) {

		setCourseAttributesToDisplay(id, model);

		return super.viewFragment(id, model);
	}



	@RequestMapping(value = "updateCourse", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> updateTenant(@ModelAttribute("course") CreateCourseModel model, @RequestParam(name = "imageToUpload", required = false) MultipartFile imageToUpload) {
		Map<String, Object> modelMap = new HashMap<>();
		try {
			CourseService courseService = (CourseService) getRestService();
			Course dbCourse;

			if (model.getId() == null) {
				dbCourse = new Course();
			} else {
				dbCourse = courseService.findById(model.getId());
			}

			dbCourse.setName(model.getName());
			dbCourse.setDescription(model.getDescription());
			dbCourse.setUpdateDate(new Date());

			if (model.getId() == null) {

				dbCourse = courseService.create(courseService.setDefaultCreationInformation(dbCourse, null));

				if (dbCourse != null) {
					dbCourse = courseService.addImage(dbCourse, imageToUpload);
				}
			} else {
				dbCourse = courseService.update(dbCourse);
				dbCourse = courseService.addImage(dbCourse, imageToUpload);
			}


			modelMap.put("success", Boolean.TRUE);


		} catch (ResourceConflictException e) {
			modelMap.put("success", Boolean.FALSE);
		} catch (ResourceCreateFailedException e) {
			modelMap.put("success", Boolean.FALSE);
		} catch (Exception e) {
			modelMap.put("success", Boolean.FALSE);
		}

		return modelMap;
	}


	@RequestMapping(value = WebPath.TENANT_GET_IMAGE, method = RequestMethod.GET)
	public void getImage(@PathVariable(name = "id") Long id, HttpServletResponse response) throws Exception {
		CourseService courseService = (CourseService) getRestService();
		Course course = courseService.findById(id);

		byte[] dataBinary = course.getImage();

		if (dataBinary != null) {
			response.setContentType("application/octet-stream");

			OutputStream os = response.getOutputStream();
			os.write(dataBinary);

			os.close();
		}
	}


	@RequestMapping(value = "/removeUserFromSession/{sessionId}/{userId}", method = RequestMethod.GET)
	public ResponseEntity<String> removeUserFromSession(@PathVariable(name = "sessionId") Long sessionId, @PathVariable(name = "userId") Long userId, Model model) {
		String response = "";

		PlannedCourseStudent plannedCourseStudent = plannedCourseStudentService.findByPlanificationAndStundent(sessionId, userId);
		if (plannedCourseStudent != null) {
			plannedCourseStudentService.remove(plannedCourseStudent.getId());
			response = "{\"error\": \"0\"}";
		} else {
			response = "{\"error\": \"1\"}";
		}

		return new ResponseEntity<String>(response, HttpStatus.ACCEPTED);
	}


	@RequestMapping(value = "/addUserToCourseSession/{sessionId}/{userId}", method = RequestMethod.GET)
	public ResponseEntity<String> addUserToCourseSession(@PathVariable(name = "sessionId") Long sessionId, @PathVariable(name = "userId") Long userId, Model model) {
		String response = "";


		if (plannedCourseStudentService.findByPlanificationAndStundent(sessionId, userId) != null) {
			response = "{\"error\": \"1\"}";
		} else {
			PlannedCourse plannedCourse = plannedCourseService.findOneById(sessionId);
			AelUser user = aelUserService.findById(userId);
			PlannedCourseStudent pcs = plannedCourseStudentService.createPlannedCourseStudent(user, plannedCourse, false, false, false, false, false);

			response = "{\"error\": \"0\", \"id\": \"" + pcs.getUser().getId() + "\", \"name\": \"" + (pcs.getUser().getFirstName() + " " + pcs.getUser().getLastName()) + "\"}";
		}

		return new ResponseEntity<String>(response, HttpStatus.ACCEPTED);
	}


	@RequestMapping(value = "/getSession/{sessionId}", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> getSession(@PathVariable(name = "sessionId") Long sessionId, Model model) {
		Map<String, Object> modelMap = new HashMap<>();
		PlannedCourse plannedCourse = plannedCourseService.findOneById(sessionId);

		if (plannedCourse == null) {
			modelMap.put("succes", false);
		} else {
			modelMap.put("session", plannedCourse);

			if (plannedCourse.getTeacher() != null) {
				modelMap.put("has_teacher", true);
				modelMap.put("teacher", plannedCourse.getTeacher());
			} else {
				modelMap.put("has_teacher", false);
			}

			modelMap.put("succes", true);
		}

		return modelMap;
	}


	@GetMapping("/session/getRating/{sessionId}")
	public String getRating(@PathVariable(name = "sessionId") Long sessionId, ModelMap model) {
		PlannedCourse plannedCourse = plannedCourseService.findOneById(sessionId);

		if (plannedCourse != null) {
			if (plannedCourse.getRating() != null) {
				model.put("value", plannedCourse.getRating());
			} else {
				model.put("value", 0);
			}

			model.put("_starSize", "small");
		}

		return "app/components/rating/widget :: view";
	}

	@HtmlMapping(
			path = {"/create", "/edit/{id}"}
	)
	public String form(@PathVariable("id") Optional<Long> id, ModelMap model) {
		if (id.isPresent() && !((CourseService)getRestService()).canEditCourse(id.get())) {
			return Template.ACCESS_DENIED;
		}
		return super.form(id, model);
	}

	@RequestMapping(value ="/getComment", method = RequestMethod.POST)
	@ResponseBody
	public Map getCommentForCommentsTable( @RequestParam(name = "comment") String comment, @RequestParam(name = "courseId") String courseId) {
		Map<String, Object> modelMap = new HashMap<>();
		modelMap.put("success", plannedCourseService.addCourseComment(comment, courseId));
		return modelMap;
	}

	@PostMapping(value = "/attendItem/{id}")
	@ResponseBody
	public ResponseEntity<PlannedCourse> attendItem(@PathVariable(name = "id") Long id)
	{
		List<PlannedCourse> plannedCourses = plannedCourseService.findByCourseId(id);
		return new ResponseEntity<>(plannedCourseService.attendItem(plannedCourses.get(0).getId()), HttpStatus.OK);
	}
}
