package ro.siveco.ael.lcms.domain.auth.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.siveco.ael.lcms.domain.auth.model.AelRole;
import ro.siveco.ael.lcms.domain.auth.model.Group;
import ro.siveco.ael.lcms.domain.auth.model.MultiSelectDTO;
import ro.siveco.ael.lcms.domain.tenant.model.Tenant;
import ro.siveco.ael.lcms.repository.AelRoleRepository;
import ro.siveco.ram.app.exception.AppException;
import ro.siveco.ram.starter.service.BaseEntityService;

import java.util.ArrayList;
import java.util.List;

@Service
public class AelRoleService extends BaseEntityService<AelRole> {

    @Autowired
    public AelRoleService(AelRoleRepository aelRoleRepository) {
        super(AelRole.class, aelRoleRepository);
    }

    public AelRole findByNameAndTenant(String name, Tenant tenant) {
        return ((AelRoleRepository) getResourceRepository()).findByNameAndTenant(name, tenant);
    }

    public List<AelRole> findByidRoles(List<Long> idRoles) {
        return ((AelRoleRepository) getResourceRepository()).findByidRoles(idRoles);
    }
    public List<AelRole> findAll() {
        return ((AelRoleRepository) getResourceRepository()).findAll();
    }

    public List<AelRole> findAllByTenant(Tenant tenant) {
        return ((AelRoleRepository) getResourceRepository()).findAllByTenant(tenant);
    }

    public AelRole findById(Long id) {
        return ((AelRoleRepository) getResourceRepository()).findById(id);
    }

    public void validateRole(AelRole role){
        if(role.getName() == null || "".equals(role.getName())){
            throw new AppException("error.role.null");
        }
        List<AelRole> existingRoles = ((AelRoleRepository) getResourceRepository()).findByName(role.getName());
        if (existingRoles != null && existingRoles.size() > 0) {
            AelRole existingRole = existingRoles.get(0);
            if (role.isNew() || role.getId().compareTo(existingRole.getId()) != 0) {
                throw new AppException("error.role.exists");
            }

        }
    }

    public List<MultiSelectDTO> getMultiSelectDTOS(List<AelRole> aelRoleList) {

        List<MultiSelectDTO> multiSelectDTOs = new ArrayList<MultiSelectDTO>();
        for (AelRole aelRole : aelRoleList) {
            MultiSelectDTO multiSelectDTO = new MultiSelectDTO();
            multiSelectDTO.setId(aelRole.getId());
            multiSelectDTO.setName(aelRole.getName());
            multiSelectDTO.setStatus(0L);
            multiSelectDTOs.add(multiSelectDTO);
        }
        return multiSelectDTOs;
    }

    public List<AelRole> getAelRoles(Tenant tenant) {

        List<AelRole> listRoles = new ArrayList<AelRole>();
        if (tenant == null) {
            listRoles = findAll();
        } else {
            listRoles = findAllByTenant(tenant);
        }
        return listRoles;
    }
}
