package ro.siveco.ael.lcms.domain.auth.web;

import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ro.siveco.ael.lcms.domain.auth.service.ProfileVerificationResult;
import ro.siveco.ael.lcms.domain.auth.service.ProfileVerificationService;
import ro.siveco.ael.web.components.Template;
import ro.siveco.ael.web.components.WebPath;
import ro.siveco.ael.web.controllers.BaseController;
import ro.siveco.ael.web.controllers.ProfileController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * <p>Services user profile verification requests. It uses the {@link ProfileVerificationService} to perform
 * the actual verifications.</p>
 *
 * <p>The profile ({@link ro.siveco.ael.lcms.domain.auth.model.AelUser} needs to be verified after it's created if
 * the option <code>features.profile-verification-enabled</code> is set to <code>true</code>. Until the profile
 * is verified, the user is unable to sign in.</p>
 *
 * @author maciejb
 */
@Controller
public class ProfileVerificationController extends BaseController {


    /*
       Model data names
     */
    private static final String EMAIL = "email";
    private static final String VERIFICATION_RESULT = "verificationResult";
    private static final String TOKEN = "token";
    private static final String VERIFICATION_ERROR = "verificationError";
    private static final String BEFORE_SUBMIT = "beforeSubmit";
    private static final String IS_SUCCESS = "isSuccess";


    private ProfileVerificationService profileVerificationService;
    private MessageSource messageSource;

    @Autowired
    public ProfileVerificationController(ProfileVerificationService profileVerificationService, MessageSource messageSource) {

        Assert.notNull(profileVerificationService, "Supplied ProfileVerificationService is null.");
        Assert.notNull(messageSource, "Supplied MessageSource is null.");

        this.profileVerificationService = profileVerificationService;
        this.messageSource = messageSource;
    }

    /**
     * Verify a profile using a <code>token</code> request parameter
     *
     * @param request HTTP request
     * @param modelMap view model map
     * @return Template.RESEND_PROFILE_VERIFICATION;
     */
    @GetMapping(WebPath.PROFILE_VERIFICATION)
    public String verifyProfile(HttpServletRequest request, ModelMap modelMap){


        String token = request.getParameter(TOKEN);
        ProfileVerificationResult result = profileVerificationService.verifyProfile(token);

        modelMap.put(VERIFICATION_RESULT, result);

        return Template.PROFILE_VERIFICATION;
    }


    /**
     * Show the resend profile verification email page.
     * @param request HTTP request
     * @param modelMap view model map
     * @return Template.RESEND_PROFILE_VERIFICATION;
     */
    @GetMapping(WebPath.RESEND_PROFILE_VERIFICATION)
    public String showResendProfileVerificationPage(HttpServletRequest request, ModelMap modelMap){

        modelMap.put(BEFORE_SUBMIT, true);
        modelMap.put(EMAIL, "");

        String emailFromSession = getEmailFromSession(request.getSession());

        if (emailFromSession != null){
            modelMap.put(EMAIL, emailFromSession);
        }


        return Template.RESEND_PROFILE_VERIFICATION;
    }


    /**
     * Resend the profile verification email to the email address supplied in the request.
     * @param email email address from the HTTP request
     * @param modelMap view model map
     * @return Template.RESEND_PROFILE_VERIFICATION
     */
    @PostMapping(WebPath.RESEND_PROFILE_VERIFICATION)
    public String resendProfileVerification(@RequestParam String email, ModelMap modelMap){

        ProfileVerificationResult verificationResult;

        if (EmailValidator.getInstance().isValid(email)){
            verificationResult = profileVerificationService.resendVerificationEmail(email);
        } else {
            verificationResult
                    = ProfileVerificationResult.badEmail(
                    messageSource.getMessage(
                            ProfileVerificationResult.BAD_EMAIL_ERR,
                            null,
                            LocaleContextHolder.getLocale()));
        }

        modelMap.put(EMAIL, email);
        modelMap.put(VERIFICATION_RESULT, verificationResult);
        modelMap.put(VERIFICATION_ERROR, verificationResult.getErrorMessage());
        modelMap.put(IS_SUCCESS, verificationResult.isSuccess());
        modelMap.put(BEFORE_SUBMIT, false);

        return Template.RESEND_PROFILE_VERIFICATION;
    }
}
