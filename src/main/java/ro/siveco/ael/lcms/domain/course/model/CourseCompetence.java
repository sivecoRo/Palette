package ro.siveco.ael.lcms.domain.course.model;

import org.springframework.data.domain.Sort;
import ro.siveco.ael.lcms.domain.taxonomies.model.Competence;
import ro.siveco.ael.lcms.domain.taxonomies.model.TaxonomyLevel;
import ro.siveco.ram.starter.ui.config.PropertyViewDescriptor;
import ro.siveco.ram.starter.ui.config.Sorter;
import ro.siveco.ram.starter.ui.config.ViewDescriptor;
import ro.siveco.ram.starter.ui.model.FormInputType;
import ro.siveco.ram.starter.ui.model.IndexViewType;
import ro.siveco.ram.starter.ui.model.ViewRendererType;

import javax.persistence.*;

@Entity
@Table( name = "course_competences" )
@SequenceGenerator( name = "course_competences_seq", sequenceName = "course_competences_seq" )
//@TableComment(comment = "Stores the system's associations between courses and competences")

@ViewDescriptor(paginationEnabled = true, viewTypes = {IndexViewType.GRID}, defaultViewType = IndexViewType.GRID,
        searchEnabled = true, defaultSearchProperty = "competence.label",
        sort = {@Sorter(propertyName = "competence.label", direction = Sort.Direction.ASC),
                @Sorter(propertyName = "taxonomyLevel.name", direction = Sort.Direction.ASC)})
public class CourseCompetence extends ro.siveco.ram.starter.model.base.BaseEntity
{
	private Course course;

	private Competence competence;

	private TaxonomyLevel taxonomyLevel;

	@Id
	@GeneratedValue( generator = "course_competences_seq", strategy = GenerationType.AUTO )
	public Long getId()
	{
		return super.getId();
	}

    @ManyToOne
    @JoinColumn(name = "course_id", nullable = false )
    @PropertyViewDescriptor(inputType = FormInputType.LIST_OF_VALUES, rendererType = ViewRendererType.MODEL,
            editInline = false, hidden = true, required = true, hiddenOnForm = true)
    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

	@ManyToOne
	@JoinColumn(name = "competence_id", nullable = false )
	@PropertyViewDescriptor(inputType = FormInputType.LIST_OF_VALUES, rendererType = ViewRendererType.MODEL,
            editInline = false, hidden = false, required = true, hiddenOnForm = false)
    public Competence getCompetence() {
		return competence;
	}

	public void setCompetence(Competence competence) {
		this.competence = competence;
	}

	@ManyToOne
	@JoinColumn(name = "taxonomy_level_id", nullable = true )
	@PropertyViewDescriptor(inputType = FormInputType.LIST_OF_VALUES, rendererType = ViewRendererType.MODEL,
            editInline = false, hidden = false, required = true, hiddenOnForm = false)
    public TaxonomyLevel getTaxonomyLevel() {
		return taxonomyLevel;
	}

	public void setTaxonomyLevel(TaxonomyLevel taxonomyLevel) {
		this.taxonomyLevel = taxonomyLevel;
	}

    @Override
    public String toString() {
        return "Competence " + (this.getCompetence() != null ? this.getCompetence().getLabel() : "") + " for course " +  (this.getCourse()!=null ? this.getCourse().getName() : "");
    }
}