package ro.siveco.ael.lcms.domain.taxonomies.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.taxonomies.model.Competence;
import ro.siveco.ael.lcms.domain.taxonomies.service.CompetenceService;
import ro.siveco.ram.app.exception.AppException;
import ro.siveco.ram.event.listener.type.single.BaseBeforeUpdateListener;
import ro.siveco.ram.event.type.single.BeforeUpdateEvent;

/**
 * Created by AndradaC on 9/1/2017.
 */
@Component
public class ValidateCompetenceBeforeUpdateListener extends BaseBeforeUpdateListener<Competence>{

    @Autowired
    private CompetenceService competenceService;

    @Override
    public void onApplicationEvent(BeforeUpdateEvent<Competence> event) {
        Competence competence = event.getDomainModel();
        String validationResult = competenceService.validateCompetence(competence);
        if(validationResult != null)
        {
            throw new AppException(validationResult);
        }
    }
}
