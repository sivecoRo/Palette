package ro.siveco.ael.lcms.domain.materials.model;

/**
 * Created by IntelliJ IDEA.
 * User: razvanni
 * To change this template use File | Settings | File Templates.
 */
public interface SurveyAnswer
{

	Long getId();
	void setId(Long id);


	Survey getSurvey();
	void setSurvey(Survey survey);


	SurveyItemValue getSurveyItemValue();
	void setSurveyItemValue(SurveyItemValue surveyItemValue);


	Boolean getBooleanValue();
	void setBooleanValue(Boolean booleanValue);


	String getTextValue();
	void setTextValue(String textValue);


	Long getLongValue();
	void setLongValue(Long longValue);


	Long getRowID();
	void setRowID(Long rowID);

	
}
