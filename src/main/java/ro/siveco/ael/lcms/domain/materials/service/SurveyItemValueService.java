package ro.siveco.ael.lcms.domain.materials.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.siveco.ael.lcms.domain.materials.model.SurveyItem;
import ro.siveco.ael.lcms.domain.materials.model.SurveyItemValue;
import ro.siveco.ael.lcms.repository.SurveyItemValueRepository;
import ro.siveco.ram.repository.BaseEntityRepository;
import ro.siveco.ram.starter.service.BaseEntityService;

import java.util.List;

/**
 * Created by AndradaC on 5/2/2017.
 */
@Service
public class SurveyItemValueService extends BaseEntityService<SurveyItemValue> {
    @Autowired
    public SurveyItemValueService(BaseEntityRepository<SurveyItemValue> resourceRepository) {
        super(SurveyItemValue.class, resourceRepository);
    }

    public SurveyItemValue findOneById(Long id) {
        return ((SurveyItemValueRepository) getResourceRepository()).findOneById(id);
    }

    public List<SurveyItemValue> findBySurveyItemOrderByDefinitionAsc(SurveyItem surveyItem) {
        return ((SurveyItemValueRepository) getResourceRepository()).findBySurveyItemOrderByDefinitionAsc(surveyItem);
    }
}
