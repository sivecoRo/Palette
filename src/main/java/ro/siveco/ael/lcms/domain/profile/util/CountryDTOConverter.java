package ro.siveco.ael.lcms.domain.profile.util;

import ro.siveco.ael.lcms.domain.profile.model.CountryDTO;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class CountryDTOConverter implements AttributeConverter<CountryDTO, String> {

    @Override
    public String convertToDatabaseColumn(CountryDTO attribute) {

        if(attribute != null) {
            return attribute.getName();
        }
        return "";
    }

    @Override
    public CountryDTO convertToEntityAttribute(String dbData) {

        CountryDTO countryDTO = new CountryDTO();
        countryDTO.setName(dbData);
        return countryDTO;
    }
}
