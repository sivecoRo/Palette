package ro.siveco.ael.lcms.domain.metadata.model;

/**
 * Created by IuliaP on 27.06.2017.
 */
public enum RelationType {
    INCLUDED, EXCLUDED, OPTIONAL
}
