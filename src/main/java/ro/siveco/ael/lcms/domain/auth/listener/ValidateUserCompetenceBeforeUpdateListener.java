package ro.siveco.ael.lcms.domain.auth.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.auth.model.UserCompetence;
import ro.siveco.ael.lcms.domain.auth.service.UserCompetenceService;
import ro.siveco.ram.app.exception.AppException;
import ro.siveco.ram.event.listener.type.single.BaseBeforeUpdateListener;
import ro.siveco.ram.event.type.single.BeforeUpdateEvent;

/**
 * Created by AndradaC on 9/3/2017.
 */
@Order(value = 2)
@Component
public class ValidateUserCompetenceBeforeUpdateListener extends BaseBeforeUpdateListener<UserCompetence>{

    @Autowired
    private UserCompetenceService userCompetenceService;

    @Override
    public void onApplicationEvent(BeforeUpdateEvent<UserCompetence> event) {
        UserCompetence userCompetence = event.getDomainModel();

        if(userCompetenceService.previousConnexionExists(userCompetence)){
            throw new AppException("error.userCompetence.update.duplicateEntry");
        }
    }
}
