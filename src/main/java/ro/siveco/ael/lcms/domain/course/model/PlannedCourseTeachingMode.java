package ro.siveco.ael.lcms.domain.course.model;

/**
 * Created by AndradaC on 8/18/2017.
 */
public enum PlannedCourseTeachingMode {
    ONLINE_COURSE,
    CLASS_COURSE
}
