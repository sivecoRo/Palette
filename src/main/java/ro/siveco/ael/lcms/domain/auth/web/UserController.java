package ro.siveco.ael.lcms.domain.auth.web;

import com.querydsl.core.types.EntityPath;
import com.querydsl.core.types.Predicate;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.data.domain.Pageable;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriComponentsBuilder;
import ro.siveco.ael.lcms.domain.auth.model.*;
import ro.siveco.ael.lcms.domain.auth.service.AelGroupService;
import ro.siveco.ael.lcms.domain.auth.service.AelUserService;
import ro.siveco.ael.lcms.domain.auth.service.DefaultUserService;
import ro.siveco.ael.lcms.domain.auth.service.ProfileVerificationService;
import ro.siveco.ael.lcms.domain.course.service.CourseService;
import ro.siveco.ael.lcms.domain.course.service.PlannedCourseService;
import ro.siveco.ael.lcms.domain.metadata.model.Metadata;
import ro.siveco.ael.lcms.domain.metadata.service.LanguageService;
import ro.siveco.ael.lcms.domain.profile.model.*;
import ro.siveco.ael.lcms.domain.profile.service.DeleteProfileService;
import ro.siveco.ael.lcms.domain.profile.service.UserDetailsService;
import ro.siveco.ael.lcms.domain.tenant.model.QTenant;
import ro.siveco.ael.web.components.Template;
import ro.siveco.ael.web.components.WebPath;
import ro.siveco.ael.web.controllers.TenantAwareBaseEntityController;
import ro.siveco.ram.controller.annotation.AjaxHtmlMapping;
import ro.siveco.ram.controller.annotation.AjaxStructuralMapping;
import ro.siveco.ram.controller.annotation.HtmlMapping;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.Principal;
import java.util.*;


/**
 * Created by LiviuI on 4/27/2017.
 */
@Controller
@RequestMapping("/user")
public class UserController extends TenantAwareBaseEntityController<AelUser> {

    @Autowired
    private AelUserService aelUserService;

    private final UserDetailsService userDetailsService;
    private final LanguageService languageService;
    private final CourseService courseService;
    private final DeleteProfileService deleteProfileService;

    @Autowired
    private AelGroupService aelGroupService;

    @Autowired
    private ResourceLoader resourceLoader;

    @Autowired
    private ShaPasswordEncoder passwordEncoder;

    @Autowired
    private ProfileVerificationService profileVerificationService;

    @Override
    protected QTenant getTenantPath() {
        return QAelUser.aelUser.tenant;
    }

    public UserController(DefaultUserService restService, UserDetailsService userDetailsService,
                          LanguageService languageService, CourseService courseService,
                          DeleteProfileService deleteProfileService) {
        super(restService);
        this.userDetailsService = userDetailsService;
        this.languageService = languageService;
        this.courseService = courseService;
        this.deleteProfileService = deleteProfileService;
    }

    //    /**
    //     * Fetch a specific page of entities (json/xml)
    //     *
    //     * @param predicate  Predicate (query dsl)
    //     * @param pageable   Pagination info
    //     * @param parameters Parameters
    //     * @return A page of entities
    //     */
    //    @Override
    //    @AjaxStructuralMapping(path = MappingUri.INDEX_URI)
    //    public ResponseEntity<Page<?>> paginate(@QuerydslPredicate(bindings = UserQuerydslBinderCustomizer.class)
    //                                                    Optional<com.querydsl.core.types.Predicate> predicate,
    //                                            Pageable pageable,
    //                                            @RequestParam MultiValueMap<String, String> parameters) {
    //        return super.paginate(predicate, revisePageable(pageable, parameters), parameters);
    //    }

    @RequestMapping(value = "/changePassword", method = RequestMethod.POST)
    public ResponseEntity<String> changePassword(@RequestBody Object model) {
        Boolean success = true;
        String msg = "", resp;

        AelUser aelUser = aelUserService.findById(getUser().getId());
        String oldPassword =
                passwordEncoder.encodePassword(((LinkedHashMap) model).get("oldPassword").toString(), null);

        if (!oldPassword.equals(aelUser.getPassword())) {
            success = false;
            msg = "old_pass";
        } else {
            String password = ((LinkedHashMap) model).get("password").toString();
            String confirmPassword = ((LinkedHashMap) model).get("confirmPassword").toString();

            if (!password.equals(confirmPassword)) {
                success = false;
                msg = "duplicate_pass";
            } else {
                ((DefaultUserService) getRestService()).changePassword(aelUser, password);
            }
        }

        resp = "{\"success\": \"" + success + "\", \"msg\": \"" + msg + "\"}";

        return new ResponseEntity<String>(resp, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/resetPassword", method = RequestMethod.POST)
    @ResponseBody
    public AelUser resetPassword(
            @RequestBody Object model) throws RuntimeException {
        AelUser aelUser = new AelUser();

        aelUser.setPassword(((LinkedHashMap) model).get("password").toString());
        aelUser.setConfirmPassword(((LinkedHashMap) model).get("changePassword").toString());
        if (!aelUser.getPassword().equals(aelUser.getConfirmPassword())) {
            throw new RuntimeException("error.aelUser.create.passwordError");
        } else if (aelUser.getPassword().length() == 0 || aelUser.getConfirmPassword().length() == 0) {
            throw new RuntimeException("error.aelUser.create.passwordChange");
        } else {
            AelUser aelUserCurent =
                    aelUserService.findById(Long.valueOf(String.valueOf(((LinkedHashMap) model).get("id"))));
            aelUserCurent.setPassword(null);
            String password = passwordEncoder.encodePassword(aelUser.getPassword(), null);
            aelUserCurent.setPassword(password);
            update(aelUserCurent.getId(), aelUserCurent);
        }
        return aelUser;
    }

    @GetMapping("/profile")
    public String register(Principal principal, HttpServletRequest request, HttpServletResponse response,
                           HttpSession session, Model model) {

        model.addAttribute("basic", userDetailsService.getBasicData(getUser().getId()));
        model.addAttribute("userCourses", courseService.getAsocCoursesForUser(getUser().getId()));
        List<GenderSelectModel> genders = new ArrayList<GenderSelectModel>();

        for (Gender gen : Gender.values()) {
            genders.add(new GenderSelectModel(gen, gen.getName()));
        }
        model.addAttribute("genders", genders);

        List<UserCharacteristicModel> tabList = userDetailsService.getActiveTabs(getUser().getId());
        model.addAttribute("tabList", tabList);

        for (int i = 0; i < tabList.size(); i++) {
            List<UserCharacteristicModel> list =
                    userDetailsService.getTabCharacteristics(tabList.get(i).getId(), getUser().getId());
            model.addAttribute("list_" + tabList.get(i).getId(), list);
        }

        return "/content/userProfile/index";
    }

    @RequestMapping(value = "/profile/saveBasic", method = RequestMethod.POST)
    public ResponseEntity<String> saveBasic(@RequestBody UserDetails user) {

        userDetailsService.saveBasic(user, getUser().getId());

        String resp = "{\"success\": \"true\"}";

        return new ResponseEntity<String>(resp, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/profile/saveCharacteristics", method = RequestMethod.POST)
    public ResponseEntity<String> saveCharacteristics(@RequestBody SaveCharacteristicsModel characteritics) {
        userDetailsService.saveCharacteristics(characteritics.getList(), getUser().getId());

        String resp = "{\"success\": \"true\"}";

        return new ResponseEntity<String>(resp, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/profile/add_image", method = RequestMethod.POST)
    public ResponseEntity<String> addImage(@RequestParam("image") MultipartFile file) throws Exception {
        userDetailsService.addImage(getUser().getId(), file);

        String resp = "{\"success\": \"true\"}";

        return new ResponseEntity<String>(resp, HttpStatus.CREATED);
    }
    
    @RequestMapping(value = "/profile/get_image", method = RequestMethod.GET)
    public void getImage(HttpServletResponse response) throws Exception {

        byte[] dataBinary = userDetailsService.getImage(getUser().getId());

        writeImageData(response, dataBinary);
    }

    private void writeImageData(HttpServletResponse response, byte[] dataBinary) throws IOException {

        if (dataBinary != null) {
            response.setContentType("application/octet-stream");

            OutputStream os = response.getOutputStream();
            os.write(dataBinary);

            os.close();
        } else {
            ServletContext servletContext =
                    ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest()
                            .getServletContext();
            InputStream in = servletContext.getResourceAsStream("/img/generic/generic-user.png");
            response.setContentType("application/octet-stream");

            OutputStream os = response.getOutputStream();
            os.write(IOUtils.toByteArray(in));

            os.close();
        }
    }

    @RequestMapping(value = "/profile/get_image_by_user_id/{id}", method = RequestMethod.GET)
    public void getUserImageById(@PathVariable(name = "id") Long id, HttpServletResponse response) throws
            Exception {
        AelUser user = (AelUser)aelUserService.findOneById(id);

        if (user != null ) {
            byte[] dataBinary = userDetailsService.getImage(user.getId());

            writeImageData(response, dataBinary);
        }
    }

    @RequestMapping(value = "/profile/get_user_image/{username}", method = RequestMethod.GET)
    public void getUserImage(@PathVariable(name = "username") String username, HttpServletResponse response) throws
            Exception {
        Optional<User> user = aelUserService.findOneByUsername(username);

        if (user != null && !user.equals(Optional.empty())) {
            byte[] dataBinary = userDetailsService.getImage(user.get().getId());

            writeImageData(response, dataBinary);
        }
    }


    @GetMapping("/profile/management")
    public String managementProfile(HttpServletRequest request, HttpServletResponse response,
                                    HttpSession session, Model model) {

        model.addAttribute("data", userDetailsService.getAllCharacteristics(getUser().getId()));
        //        model.addAttribute("languages", languageService.list());

        List<UserCharacteristicTypeSelectModel> characteristic_types =
                new ArrayList<UserCharacteristicTypeSelectModel>();

        for (UserCharacteristicType uct : UserCharacteristicType.values()) {
            characteristic_types.add(new UserCharacteristicTypeSelectModel(uct, uct.getName()));
        }
        model.addAttribute("characteristic_types", characteristic_types);
        model.addAttribute("tabs_list", userDetailsService.getAllTabs(getUser().getId()));

        return "/content/userProfile/management/index";
    }


    @RequestMapping(value = "/profile/getCaracteristics/{type}", method = RequestMethod.GET)
    public
    @ResponseBody
    List<Characteristic> getCaracteristics(@PathVariable(name = "type") String type) {
        if (type.equals("tab")) {
            return userDetailsService.getCharacteristicTabs(getUser().getId());
        }

        return userDetailsService.getCharacteristicsByType(getUser().getId(), CharacteristicType.CHARACTERISTIC);
    }

    @RequestMapping(value = "/addCharacteristic", method = RequestMethod.POST)
    public ResponseEntity<String> addCharacteristic(@RequestBody SaveCharacteristicsModel characteritic) {
        UserCharacteristicModel cv = userDetailsService
                .saveCharacteristic(characteritic.getList(), characteritic.getType(), getUser().getId());

        String resp = "{\"success\": \"true\", \"entityId\": \"" + cv.getId() + "\", \"entityValue\": \"" +
                cv.getValue() + "\"}";

        return new ResponseEntity<String>(resp, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/addUserCharacteristic", method = RequestMethod.POST)
    public ResponseEntity<String> addUserCharacteristic(@RequestBody UserCharacteristicManagementModel ucmm) {
        userDetailsService.addUserCharacteristic(ucmm);

        String resp = "{\"success\": \"true\"}";

        return new ResponseEntity<String>(resp, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/activateCharactetistic/{ucID}", method = RequestMethod.GET)
    public ResponseEntity<String> activateCharactetistic(@PathVariable(name = "ucID") Long ucID) {
        userDetailsService.changeCharactetisticState(ucID, true);

        String resp = "{\"success\": \"true\"}";

        return new ResponseEntity<String>(resp, HttpStatus.ACCEPTED);
    }

    @RequestMapping(value = "/deactivateCharactetistic/{ucID}", method = RequestMethod.GET)
    public ResponseEntity<String> deactivateCharactetistic(@PathVariable(name = "ucID") Long ucID) {
        userDetailsService.changeCharactetisticState(ucID, false);

        String resp = "{\"success\": \"true\"}";

        return new ResponseEntity<String>(resp, HttpStatus.ACCEPTED);
    }

    @RequestMapping(value = "/getCaracteristicTagList/{ucID}", method = RequestMethod.GET)
    public
    @ResponseBody
    List<Metadata> getCaracteristicTagList(@PathVariable(name = "ucID") Long ucID) {
        return userDetailsService.getCaracteristicTagList(ucID, getUser().getId());
    }

    @RequestMapping(value = "/addTagsToCharacteristic", method = RequestMethod.POST)
    public ResponseEntity<String> addTagsToCharacteristic(@RequestBody SaveCharacteristicsModel characteritic) {
        CharacteristicValue cv = userDetailsService
                .addTagsToCharacteristic(characteritic.getList(), characteritic.getEntityId(), getUser().getId());

        String resp = "{\"success\": \"true\", \"entityId\": \"" + cv.getId() + "\", \"entityValue\": \"" +
                cv.getValue() + "\"}";

        return new ResponseEntity<String>(resp, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/activateTab/{tabID}", method = RequestMethod.GET)
    public ResponseEntity<String> activateTab(@PathVariable(name = "tabID") Long tabID) {
        userDetailsService.changeTabState(tabID, true);

        String resp = "{\"success\": \"true\"}";

        return new ResponseEntity<String>(resp, HttpStatus.ACCEPTED);
    }

    @RequestMapping(value = "/deactivateTab/{tabID}", method = RequestMethod.GET)
    public ResponseEntity<String> deactivateTab(@PathVariable(name = "tabID") Long tabID) {
        userDetailsService.changeTabState(tabID, false);

        String resp = "{\"success\": \"true\"}";

        return new ResponseEntity<String>(resp, HttpStatus.ACCEPTED);
    }

    @RequestMapping(value = "/saveGroupsAssociationAtUser", method = RequestMethod.POST)
    public void saveAssociation(
            @RequestBody Object objectToSave) {

        Long idUser = Long.valueOf(((LinkedHashMap) objectToSave).get("id").toString());
        List<String> listString = (List<String>) ((LinkedHashMap) objectToSave).get("selectedValues");
        List<Long> listLong = new ArrayList<>();
        if (listString != null) {
            for (String s : listString) {
                listLong.add(Long.valueOf(s));
            }
        }
        List<AelGroup> aelGroupList = new ArrayList<>();
        AelUser aelUser = aelUserService.findById(idUser);
        if (listString != null && listString.size() != 0) {
            aelGroupList = aelGroupService.findByidGroups(listLong);
        }
        aelUser.setGroups(aelGroupList);
        update(aelUser.getId(), aelUser);
    }


    @RequestMapping(value = "/getGroupsAssociationsForUser", method = RequestMethod.POST)
    @ResponseBody
    public Map getGroupsAssociationsForUser(@RequestBody Object modelObj) {
        Map<Long, Object> groupsMap = new HashMap<>();

        if (!((LinkedHashMap) modelObj).get("id").toString().equals("")) {
            AelUser user = aelUserService.findById(Long.valueOf(((LinkedHashMap) modelObj).get("id").toString()));
            List<Group> aelGroups = new ArrayList<>();
            if (getUser().getTenant() == null) {
                aelGroups = aelGroupService.findAllGroup();
            } else {
                aelGroups = aelGroupService.findTenant(getUser().getTenant());
            }

            for (Group group : aelGroups) {
                if (user.getGroups().contains((Group) group)) {
                    groupsMap.put(group.getId(), 1);
                } else {
                    groupsMap.put(group.getId(), 0);
                }
            }
        }
        return groupsMap;
    }

    @RequestMapping(value = "/download/template", method = RequestMethod.GET)
    @ResponseBody
    public String export(HttpServletResponse response, Principal principal) throws IOException {

        AelUser aelUser = (AelUser) getUser();
        String xlsFileName;
        if (aelUser.getLocale().getLanguage().equals("ro")) {
            xlsFileName = "template.xls";
        } else {
            xlsFileName = "template_en.xls";
        }
        Resource resource = resourceLoader.getResource("classpath:user/" + xlsFileName);
        String headerKey = "Content-Disposition";
        String headerValue = String.format("attachment; filename=\"%s\"", xlsFileName);
        response.setHeader(headerKey, headerValue);

        InputStream is = resource.getInputStream();
        org.apache.commons.io.IOUtils.copy(is, response.getOutputStream());
        response.flushBuffer();
        is.close();

        return "";
    }

    @RequestMapping(value = "/download/template/csv", method = RequestMethod.GET)
    @ResponseBody
    public String exportCSV(HttpServletResponse response, Principal principal) throws IOException {
        AelUser aelUser = (AelUser) getUser();
        String csvFileName;

        if (aelUser.getLocale().getLanguage().equals("ro")) {
            csvFileName = "templateCsv.csv";
        } else {
            csvFileName = "templateCsv_en.csv";
        }
        Resource resource = resourceLoader.getResource("classpath:user/" + csvFileName);
        String headerKey = "Content-Disposition";
        String headerValue = String.format("attachment; filename=\"%s\"", csvFileName);
        response.setHeader(headerKey, headerValue);

        InputStream is = resource.getInputStream();
        org.apache.commons.io.IOUtils.copy(is, response.getOutputStream());
        response.flushBuffer();
        is.close();

        return "";
    }

    /**
     * Delete user profile
     *
     * Get password provided by the user, (re)authenticate, anonymize user profile, remove all user offers (items/
     * courses) and comments (regarding these, and other courses the user commented on).
     *
     * When deleting (removing) items/offers (stored in "courses" and "planned_courses" tables/entities), there is
     * a cascade from Courses to PlannedCourses concerning user associations: "likes/favourites", * "attending",
     * "removed" from suggested offers". As there is no cascade for Comments from PlannedCourses (nor from User to
     * her/his Comments), these Comments has to be found and deleted with "removeByPlannedCourseId" * and
     * "removeByUserId".
     */
    @RequestMapping(value = "/deleteProfile", method = RequestMethod.POST)
    public String deleteProfile(@RequestParam(value = "password", required = false) String password) throws IOException {

        // Get logged user object.
        AelUser aelUser = (AelUser) getUser();

        // Check if provided password is correct.
        Boolean passwordCorrect = userDetailsService.checkPassword(password, aelUser);

        if (passwordCorrect) {

            // Delete user profile. A separate service had to be created in order to handle @Transactional and prevent
            // incoherent states of the system (in case something went wrong during removing/anonymizing the profile).
            deleteProfileService.deleteProfile(aelUser);

            // Special case of logout that redirects to view after profile deletion (user is already logged out when
            // reaching this view).
            return WebPath.redirect(WebPath.LOGOUT + "?profileDeleted=true");
        } else {
            // In case of failure (incorrect password provided), redirect to delete-profile view and display
            // appropriate error message.
            return WebPath.redirect(WebPath.DELETE_PROFILE + "?passwordCorrect=" + passwordCorrect.toString());
        }
    }

    @AjaxStructuralMapping(
            path = {"/"},
            method = {RequestMethod.POST}
    )
    public ResponseEntity<?> create(@RequestPart(name = "object") AelUser entity, @RequestPart(name = "files") Optional<MultipartFile[]> files, @RequestPart(name = "fileField") Optional<String> fileField, UriComponentsBuilder ucBuilder, ModelMap model) {
        return null;
    }

    @AjaxStructuralMapping(
            path = {"/{id}"},
            method = {RequestMethod.PUT}
    )
    public ResponseEntity<?> update(@PathVariable("id") Long id, @RequestPart(name = "object") AelUser entity, @RequestPart(name = "files") Optional<MultipartFile[]> files, @RequestPart(name = "fileField") Optional<String> fileField) {
        return null;
    }

    @AjaxStructuralMapping(
            path = {"/{id}"},
            method = {RequestMethod.DELETE}
    )
    public ResponseEntity<?> delete(@PathVariable("id") Long id) {
        return null;
    }

    @AjaxHtmlMapping(
            path = {"/create", "/edit/{id}"}
    )
    public String formFragment(@PathVariable("id") Optional<Long> id, ModelMap model) {
        return Template.ACCESS_DENIED;
    }

    @HtmlMapping(
            path = {"/create", "/edit/{id}"}
    )
    public String form(@PathVariable("id") Optional<Long> id, ModelMap model) {
        return Template.ACCESS_DENIED;
    }

    public String view(@PathVariable("id") Long id, ModelMap model) {
        return Template.ACCESS_DENIED;
    }

    @HtmlMapping(
            path = {"/"}
    )
    public String index(ModelMap model, @QuerydslPredicate Optional<Predicate> predicate, Pageable pageable, @RequestParam MultiValueMap<String, String> parameters) {
        return Template.ACCESS_DENIED;
    }
}

class UserQuerydslBinderCustomizer implements QuerydslBinderCustomizer {

    @Override
    public void customize(QuerydslBindings bindings, EntityPath root) {
        bindings
                .bind(QAelUser.aelUser.displayName)
                .first((path, value) -> path.toLowerCase().like("%" + value.toString() + "%"));
    }
}
