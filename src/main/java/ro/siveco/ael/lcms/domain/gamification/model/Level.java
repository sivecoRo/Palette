package ro.siveco.ael.lcms.domain.gamification.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

/**
 * Created by LucianR on 30.10.2017.
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Level {

    private String name;
    private String icon;
    private String points;

    public String getName() {

        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

    public String getIcon() {

        return icon;
    }

    public void setIcon(String icon) {

        this.icon = icon;
    }

    public String getPoints() {

        return points;
    }

    public void setPoints(String points) {

        this.points = points;
    }
}
