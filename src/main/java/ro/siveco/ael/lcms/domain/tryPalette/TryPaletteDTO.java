package ro.siveco.ael.lcms.domain.tryPalette;

import java.util.List;

public class TryPaletteDTO {

    private String country;
    private String city;
    private String coordinates;
    private List<Long> interests;
}
