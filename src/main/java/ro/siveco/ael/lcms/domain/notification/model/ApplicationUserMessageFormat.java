package ro.siveco.ael.lcms.domain.notification.model;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * Created by IntelliJ IDEA.
 * User: AlexandruM
 * Date: 18.07.2007
 * Time: 16:53:58
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum ApplicationUserMessageFormat {
    GENERIC_MESSAGE("generic.message"),
    GRADE_MESSAGE("grade.message"),
    GRADE_DELETED_MESSAGE("grade.deleted.message"),
    GRADE_UPDATED_MESSAGE("grade.updated.message"),

    COURSE_GRADE_MESSAGE("course.grade.message"),
    COURSE_GRADE_DELETED_MESSAGE("course.grade.deleted.message"),
    COURSE_GRADE_UPDATED_MESSAGE("course.grade.updated.message"),

    ASSIGNMENT_GRADE_MESSAGE("assignment.grade.message"),
    ASSIGNMENT_GRADE_DELETED_MESSAGE("assignment.grade.deleted.message"),
    ASSIGNMENT_GRADE_UPDATED_MESSAGE("assignment.grade.updated.message"),

    SUBJECT_GRADE_MESSAGE("subject.grade.message"),
    SUBJECT_GRADE_DELETED_MESSAGE("subject.grade.deleted.message"),
    SUBJECT_GRADE_UPDATED_MESSAGE("subject.grade.updated.message"),

    CREATED_NEW_USER_MESSAGE("created.user.message"),

    ABSENCE_MESSAGE("absence.message"),
    ABSENCE_UPDATE_MESSAGE("absence.updated.message"),
    ABSENCE_DELETED_MESSAGE("absence.deleted.message"),

    COURSE_SUBMISSION_MESSAGE("course.submision.message"),

    COURSE_SUBMISSION_MESSAGE_EXTERN("course.submision.message.extern"),

    GRADE_MESSAGE_PARENT("grade.message.parent"),
    GRADE_DELETED_MESSAGE_PARENT("grade.deleted.message.parent"),
    GRADE_UPDATED_MESSAGE_PARENT("grade.updated.message.parent"),

    COURSE_GRADE_MESSAGE_PARENT("course.grade.message.parent"),
    COURSE_GRADE_DELETED_MESSAGE_PARENT("course.grade.deleted.message.parent"),
    COURSE_GRADE_UPDATED_MESSAGE_PARENT("course.grade.updated.message.parent"),

    ASSIGNMENT_GRADE_MESSAGE_PARENT("assignment.grade.message.parent"),
    ASSIGNMENT_GRADE_DELETED_MESSAGE_PARENT("assignment.grade.deleted.message.parent"),
    ASSIGNMENT_GRADE_UPDATED_MESSAGE_PARENT("assignment.grade.updated.message.parent"),

    SUBJECT_GRADE_MESSAGE_PARENT("subject.grade.message.parent"),
    SUBJECT_GRADE_DELETED_MESSAGE_PARENT("subject.grade.deleted.message.parent"),
    SUBJECT_GRADE_UPDATED_MESSAGE_PARENT("subject.grade.updated.message.parent"),

    ABSENCE_MESSAGE_PARENT("absence.message.parent"),
    ABSENCE_UPDATE_MESSAGE_PARENT("absence.updated.message.parent"),
    ABSENCE_DELETED_MESSAGE_PARENT("absence.deleted.message.parent"),

    COURSE_SUBMISSION_MESSAGE_PARENT("course.submision.message.parent"),
    COURSE_START_MESSAGE("course.start.message"),
    COURSE_START_MESSAGE_PARENT("course.start.message.parent"),
    COURSE_FINISH_MESSAGE("course.finish.message"),
    COURSE_FINISH_MESSAGE_PARENT("course.finish.message.parent"),
    COURSE_REMOVAL_MESSAGE("course.removal.message"),
    COURSE_REMOVAL_MESSAGE_PARENT("course.removal.message.parent"),
    COURSE_EDIT_MESSAGE("course.edit.message"),
    COURSE_EDIT_PERIOD_MESSAGE("course.edit.period.message"),
    COURSE_EDIT_MESSAGE_TEACHING("teacher.course.edit.message"),
    COURSE_DATE_EDIT_MESSAGE("course.date.message"),
    COURSE_TEACHER_ASSIGNED_MESSAGE("course.teacher.assigned.message"),
    COURSE_TEACHER_REPLACED_MESSAGE("course.teacher.replaced.message"),
    COURSE_REGISTRATION_USER_ASSIGNED_MESSAGE("course.registration-user.assigned.message"),
    COURSE_REGISTRATION_USER_REPLACED_MESSAGE("course.registration-user.replaced.message"),
    COURSE_REGISTRATION_APPROVAL_USER_ASSIGNED_MESSAGE("course.registration-approval-user.assigned.message"),
    COURSE_REGISTRATION_APPROVAL_USER_REPLACED_MESSAGE("course.registration-approval-user.replaced.message"),
    COURSE_APPROVAL_MESSAGE("course.approval.message"),
    COURSE_APPROVAL_MESSAGE_PARENT("course.approval.message.parent"),
    COURSE_MARKED_PUBLIC_IN_STORE_MESSAGE("course.marked.public.in.store.message"),
    COURSE_NEW_STUDENT_ENROLLMENT("course.new.student.enrolled"),
    COURSE_STUDENT_ENROLLED("course.student.successfully.enrolled"),
    COURSE_STUDENT_ADDED_TO_FAVORITES("course.student.added.to.favorites"),

    CONTENT_APPROVE_MESSAGE("content.approve.message"),
    CONTENT_REJECT_MESSAGE("content.reject.message"),
    CONTENT_ASSIGN_MESSAGE("content.assign.message"),

    CONTENT_SHARED_MESSAGE("content.shared.message"),
    CONTENT_UNSHARED_MESSAGE("content.unshared.message"),

    ASSIGNMENT_MESSAGE("assignment.message"),
    ASSIGNMENT_DELETED_MESSAGE("assignment.deleted.message"),
    ASSIGNMENT_UPDATE_MESSAGE("assignment.updated.message"),
    ASSIGNMENT_REPLY_MESSAGE("assignment.reply.message"),
    ASSIGNMENT_REPLY_UPDATE_MESSAGE("assignment.reply.update.message"),

    ASSIGNMENT_MESSAGE_PARENT("assignment.message.parent"),
    ASSIGNMENT_DELETED_MESSAGE_PARENT("assignment.deleted.message.parent"),
    ASSIGNMENT_UPDATE_MESSAGE_PARENT("assignment.updated.message.parent"),

    LEARNING_PERIOD_EXPIRED("learning.period.expired"),
    SIGNUP_PERIOD_EXPIRED("signup.period.expired"),
    SIGNUP_PERIOD_STARTED("signup.period.started"),
    QUOTA_UPDATED("quota.updated"),

    PLANNED_COURSE_TEST_RESULT_NOTIFICATION("planned.course.test.result.notification"),

    PLANNED_COURSE_FAILED_ASSIGNMENT_FAILED("planned.course.failed.assignment.failed"),
    PLANNED_COURSE_FAILED_ASSIGNMENT_FAILED_TEACHER("planned.course.failed.assignment.failed.teacher"),
    PLANNED_COURSE_FAILED_TEST_FAILED("planned.course.failed.test.failed"),
    PLANNED_COURSE_FAILED_TEST_FAILED_TEACHER("planned.course.failed.test.failed.teacher"),
    PLANNED_COURSE_FAILED_GRADE_FAILED("planned.course.failed.grade.failed"),
    PLANNED_COURSE_FAILED_GRADE_FAILED_TEACHER("planned.course.failed.grade.failed.teacher"),
    PLANNED_COURSE_FAILED_ACTIVITIES_NOT_COMPLETED("planned.course.failed.activities.not.completed"),
    PLANNED_COURSE_FAILED_ACTIVITIES_NOT_COMPLETED_TEACHER("planned.course.failed.activities.not.completed.teacher"),

    ATTENDANCE_BELOW_THRESHOLD("attendance.below.threshold"),
    FAILED_ASSESSMENTS("failed.assessments"),
    DEADLINE_PLANNED_COURSE("deadline.planned.course"),

    USER_PRIVATE_MESSAGE("user.message"),

    PROMOTE_TO_PUBLIC_NOTIFICATION("promote.to.public.notification"),
    ACTIVATE_ACCOUNT_NOTIFICATION("activate.account.notification"),
    RESET_PASSWORD_NOTIFICATION("reset.password.notification"),
    NEW_PASSWORD_NOTIFICATION("new.password.notification"),

    COURSE_STUDENT_REMOVED_FROM_FAVORITES("course.student.removed.from.favorites"),
    COURSE_STUDENT_ATTENDING("course.student.attending"),
    COURSE_STUDENT_NOT_ATTENDING("course.student.not.attending"),
    COURSE_STUDENT_ADD_COMMENT("course.student.add.comment"),
    COURSE_STUDENT_EMAIL_SENT("course.student.email.sent"),
    COURSE_STUDENT_DELETE_ITEM("course.student.delete.item"),
    COURSE_STUDENT_UPDATE_ITEM("course.student.update.item"),
    COURSE_STUDENT_UPDATE_ITEM_LOCATION("course.student.update.item.location"),
    COURSE_STUDENT_EMAIL_RECEIVED("course.student.email.received"),
    COURSE_STUDENT_PAST_REVIEW("course.student.past.review"),
    COURSE_OWNER_PAST_REVIEW("course.owner.past.review");

    private String name;

    private Boolean application = Boolean.TRUE;

    ApplicationUserMessageFormat(String name) {

        this.name = name;
    }

    ApplicationUserMessageFormat(String name, Boolean application) {

        this.name = name;
        this.application = application;
    }

    public static String getLinkWithEntity(String message) {

        if (message.equals(COURSE_SUBMISSION_MESSAGE.toString()) || message.equals(COURSE_FINISH_MESSAGE.toString()) ||
                message.equals(COURSE_START_MESSAGE.toString()) || message.equals(COURSE_EDIT_MESSAGE.toString()) ||
                message.equals(COURSE_APPROVAL_MESSAGE.toString()) ||
                message.equals(DEADLINE_PLANNED_COURSE.toString())
                ) {
            return UserMessageLinks.LEARNING_COURSE_SUMMARY_LINK;
        } else if (message.equals(COURSE_TEACHER_ASSIGNED_MESSAGE.toString()) ||
                message.equals(COURSE_TEACHER_REPLACED_MESSAGE.toString()) ||
                message.equals(COURSE_REGISTRATION_USER_ASSIGNED_MESSAGE.toString()) ||
                message.equals(COURSE_REGISTRATION_USER_REPLACED_MESSAGE.toString()) ||
                message.equals(COURSE_REGISTRATION_APPROVAL_USER_ASSIGNED_MESSAGE.toString()) ||
                message.equals(COURSE_REGISTRATION_APPROVAL_USER_REPLACED_MESSAGE.toString()) ||
               message.equals(ATTENDANCE_BELOW_THRESHOLD.toString()) ||
                message.equals(COURSE_EDIT_MESSAGE_TEACHING.toString())
            // || message.equals( ORANGE_PLANNED_COURSE_NOTIFY_T_FOR_OPEN_QUESTION.toString() )
                ) {
            return UserMessageLinks.TEACHING_COURSE_SUMMARY_LINK;
        } else if (message.equals(ASSIGNMENT_MESSAGE.toString()) ||
                message.equals(ASSIGNMENT_UPDATE_MESSAGE.toString()) || message.equals(FAILED_ASSESSMENTS.toString())) {
            return UserMessageLinks.LEARNING_SHOW_ASSIGNMENT;
        } else if (message.equals(ASSIGNMENT_REPLY_MESSAGE.toString()) ||
                message.equals(ASSIGNMENT_REPLY_UPDATE_MESSAGE.toString())) {
            return UserMessageLinks.TEACHING_ASSIGNMENT_REPLY_LINK;
        } else if (message.equals(PLANNED_COURSE_TEST_RESULT_NOTIFICATION.toString())) {
            return UserMessageLinks.TEACHING_TEST_REPORT_LINK;
        }
        return "";
    }

    public static String getAttachmentLinkWithEntity(String message) {

        if (message.equals(PLANNED_COURSE_TEST_RESULT_NOTIFICATION.toString())) {
            return UserMessageLinks.TEACHING_TEST_RESULT_EXPORT_LINK;
        }

        return "";
    }

    public static String getLinkWithoutEntity(String message) {

        if (message.equals(COURSE_SUBMISSION_MESSAGE_PARENT.toString()) ||
                message.equals(COURSE_START_MESSAGE_PARENT.toString()) ||
                message.equals(COURSE_FINISH_MESSAGE_PARENT.toString()) ||
                message.equals(COURSE_APPROVAL_MESSAGE_PARENT.toString()) ||
                message.equals(GRADE_MESSAGE_PARENT.toString()) ||
                message.equals(GRADE_DELETED_MESSAGE_PARENT.toString()) ||
                message.equals(GRADE_UPDATED_MESSAGE_PARENT.toString()) ||
                message.equals(ABSENCE_MESSAGE_PARENT.toString()) ||
                message.equals(ABSENCE_UPDATE_MESSAGE_PARENT.toString()) ||
                message.equals(ABSENCE_DELETED_MESSAGE_PARENT.toString()) ||
                message.equals(COURSE_GRADE_MESSAGE_PARENT.toString()) ||
                message.equals(COURSE_GRADE_DELETED_MESSAGE_PARENT.toString()) ||
                message.equals(COURSE_GRADE_UPDATED_MESSAGE_PARENT.toString()) ||
                message.equals(SUBJECT_GRADE_MESSAGE_PARENT.toString()) ||
                message.equals(SUBJECT_GRADE_DELETED_MESSAGE_PARENT.toString()) ||
                message.equals(SUBJECT_GRADE_UPDATED_MESSAGE_PARENT.toString()) ||
                message.equals(ASSIGNMENT_GRADE_MESSAGE_PARENT.toString()) ||
                message.equals(ASSIGNMENT_GRADE_DELETED_MESSAGE_PARENT.toString()) ||
                message.equals(ASSIGNMENT_GRADE_UPDATED_MESSAGE_PARENT.toString())
                ) {
            return UserMessageLinks.PARENTS_ACTION_LINK;
        } else if (message.equals(GRADE_MESSAGE.toString()) || message.equals(GRADE_DELETED_MESSAGE.toString()) ||
                message.equals(GRADE_UPDATED_MESSAGE.toString())) {
            return UserMessageLinks.LEARNING_GRADES_LINK;
        } else if (message.equals(ABSENCE_MESSAGE.toString()) || message.equals(ABSENCE_UPDATE_MESSAGE.toString()) ||
                message.equals(ABSENCE_DELETED_MESSAGE.toString())) {
            return UserMessageLinks.LEARNING_ABSENCES_LINK;
        } else if (message.equals(CONTENT_ASSIGN_MESSAGE.toString())) {
            return UserMessageLinks.CONTENT_APPROVAL_LINK;
        } else if (message.equals(CONTENT_SHARED_MESSAGE.toString())) {
            return UserMessageLinks.CONTENT_SHARED_LINK;
        } else if (message.equals(QUOTA_UPDATED.toString())) {
            return UserMessageLinks.QUOTA_UPDATED_LINK;
        } else if (message.equals(COURSE_GRADE_MESSAGE.toString()) ||
                message.equals(COURSE_GRADE_DELETED_MESSAGE.toString()) ||
                message.equals(COURSE_GRADE_UPDATED_MESSAGE.toString())) {
            return UserMessageLinks.LEARNING_COURSE_GRADES_LINK;
        } else if (message.equals(SUBJECT_GRADE_MESSAGE.toString()) ||
                message.equals(SUBJECT_GRADE_DELETED_MESSAGE.toString()) ||
                message.equals(SUBJECT_GRADE_UPDATED_MESSAGE.toString())) {
            return UserMessageLinks.LEARNING_SUBJECTS_GRADES_LINK;
        } else if (message.equals(ASSIGNMENT_GRADE_MESSAGE.toString()) ||
                message.equals(ASSIGNMENT_GRADE_DELETED_MESSAGE.toString()) ||
                message.equals(ASSIGNMENT_GRADE_UPDATED_MESSAGE.toString())) {
            return UserMessageLinks.LEARNING_ASSIGNMENTS_GRADES_LINK;
        } else if (message.equals(PROMOTE_TO_PUBLIC_NOTIFICATION.toString())) {
            return UserMessageLinks.PROMOTE_TO_PUBLIC_LINK;
        } else if (message.equals(ACTIVATE_ACCOUNT_NOTIFICATION.toString())) {
            return UserMessageLinks.ACTIVATE_ACCOUNT_LINK;
        } else if (message.equals(RESET_PASSWORD_NOTIFICATION.toString())) {
            return UserMessageLinks.RESET_PASSWORD_LINK;
        }

        return "";
    }

    public String get() {

        return name;
    }

    public String toString() {

        return name;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

    public Boolean getApplication() {

        return application;
    }

    public void setApplication(Boolean application) {

        this.application = application;
    }
}
