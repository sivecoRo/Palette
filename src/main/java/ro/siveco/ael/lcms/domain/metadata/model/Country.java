package ro.siveco.ael.lcms.domain.metadata.model;

import org.springframework.data.domain.Sort;
import ro.siveco.ram.starter.ui.config.ActionDescriptor;
import ro.siveco.ram.starter.ui.config.PropertyViewDescriptor;
import ro.siveco.ram.starter.ui.config.Sorter;
import ro.siveco.ram.starter.ui.config.ViewDescriptor;
import ro.siveco.ram.starter.ui.model.ActionType;
import ro.siveco.ram.starter.ui.model.IndexViewType;
import ro.siveco.ram.starter.ui.model.ViewRendererType;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by albertb on 2/8/2018.
 */

@Entity
@Table(name="countries")
@SequenceGenerator(name = "countries_seq", sequenceName = "countries_seq")
@ViewDescriptor(pageSize = 10, viewTypes = {IndexViewType.GRID}, searchEnabled = true, defaultSearchProperty = "countryName",
        sort = {@Sorter(propertyName = "id", direction = Sort.Direction.ASC)})
public class Country extends ro.siveco.ram.starter.model.base.BaseEntity{
    private String countryName;
    private Language language;
    private String code;
    private String nameEn;
    private String nameRo;
    private String namePl;
    private String nameFr;
    private String nameNl;
    private String nameSl;
    private String displayName;

    @Id
    @GeneratedValue(generator = "countries_seq", strategy =GenerationType.AUTO)
    public Long getId(){return super.getId();}

    @Column(name = "country_name", nullable = false)
    @NotNull
    public String getCountryName() {return countryName;}

    @Column(name = "code")
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @ManyToOne
    @JoinColumn(name = "language_id", nullable = false)
    @NotNull
    @PropertyViewDescriptor(hidden = true)
    public Language getLanguage() {return language;}

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    @Override
    public String toString() {
        return this.getCountryName();
    }

    @Column(name = "name_en")
    public String getNameEn() {
        return nameEn;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    @Column(name = "name_ro")
    public String getNameRo() {
        return nameRo;
    }

    public void setNameRo(String nameRo) {
        this.nameRo = nameRo;
    }

    @Column(name = "name_pl")
    public String getNamePl() {
        return namePl;
    }

    public void setNamePl(String namePl) {
        this.namePl = namePl;
    }

    @Column(name = "name_fr")
    public String getNameFr() {
        return nameFr;
    }

    public void setNameFr(String nameFr) {
        this.nameFr = nameFr;
    }

    @Column(name = "name_nl")
    public String getNameNl() {
        return nameNl;
    }

    public void setNameNl(String nameNl) {
        this.nameNl = nameNl;
    }

    @Column(name = "name_sl")
    public String getNameSl() {
        return nameSl;
    }

    public void setNameSl(String nameSl) {
        this.nameSl = nameSl;
    }

    @Transient
    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    @Transient
    @PropertyViewDescriptor(
            order = -2,
            hiddenOnForm = true,
            hiddenOnGrid = true,
            hiddenOnPresentation = true,
            displayLabelInPresentation = false,
            rendererType = ViewRendererType.OBJECT_LABEL,
            clickAction = {@ActionDescriptor(
                    type = ActionType.OPEN_PRESENTATION
            ), @ActionDescriptor(
                    type = ActionType.LIST,
                    backToFor = {"country-OPEN_PRESENTATION"}
            )}
    )
    public String getObjectLabel() {
        return getDisplayName();
    }
}
