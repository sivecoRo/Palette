package ro.siveco.ael.lcms.domain.tenant.listener;

import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.auth.model.User;
import ro.siveco.ael.lcms.domain.tenant.model.Tenant;
import ro.siveco.ael.lcms.domain.tenant.model.TenantCreatorUser;
import ro.siveco.ael.lcms.domain.tenant.model.TenantHolder;
import ro.siveco.ael.web.controllers.WithUserBaseEntityController;
import ro.siveco.ram.event.listener.type.single.BaseBeforeUpdateListener;
import ro.siveco.ram.event.type.single.BeforeUpdateEvent;

@Component
public class SetInformationForTenantHolderBeforeUpdateListener extends BaseBeforeUpdateListener<TenantHolder> {

    @Override
    public void onApplicationEvent(BeforeUpdateEvent<TenantHolder> event) {

        TenantHolder tenantHolder = event.getDomainModel();
        User creator = WithUserBaseEntityController.getUser();
        Tenant tenant = creator.getTenant();
        tenantHolder.setTenant(tenant);
        if (tenantHolder instanceof TenantCreatorUser) {
            ((TenantCreatorUser) tenantHolder).setCreator(creator);
        }
    }
}
