package ro.siveco.ael.lcms.domain.tenant.listener;

import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.auth.model.User;
import ro.siveco.ael.lcms.domain.tenant.model.Tenant;
import ro.siveco.ael.lcms.domain.tenant.model.TenantCreatorUser;
import ro.siveco.ael.lcms.domain.tenant.model.TenantHolder;
import ro.siveco.ael.web.controllers.WithUserBaseEntityController;
import ro.siveco.ram.event.listener.type.single.BaseBeforeCreateListener;
import ro.siveco.ram.event.type.single.BeforeCreateEvent;

@Component
public class SetInformationForTenantHolderBeforeCreateListener extends BaseBeforeCreateListener<TenantHolder> {

    @Override
    public void onApplicationEvent(BeforeCreateEvent<TenantHolder> event) {

        TenantHolder tenantHolder = event.getDomainModel();
        User creator = WithUserBaseEntityController.getUser();
        Tenant tenant = creator.getTenant();
        tenantHolder.setTenant(tenant);
        if (tenantHolder instanceof TenantCreatorUser) {
            ((TenantCreatorUser) tenantHolder).setCreator(creator);
        }
    }
}
