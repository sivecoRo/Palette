package ro.siveco.ael.lcms.domain.metadata.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.metadata.model.Interest;
import ro.siveco.ael.lcms.domain.metadata.service.InterestService;
import ro.siveco.ram.event.listener.type.single.BaseBeforeCreateListener;
import ro.siveco.ram.event.type.single.BeforeCreateEvent;

/**
 * Created by albertb on 2/2/2018.
 */

@Component
@Order(value = 1)
public class ValidateInterestBeforeCreateListener extends BaseBeforeCreateListener<Interest> {

    @Autowired
    InterestService interestService;

    @Override
    public void onApplicationEvent(BeforeCreateEvent<Interest> event) {
        interestService.validateInterest( event.getDomainModel() );
    }
}
