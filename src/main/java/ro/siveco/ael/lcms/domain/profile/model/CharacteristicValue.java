package ro.siveco.ael.lcms.domain.profile.model;

import ro.siveco.ael.lcms.domain.metadata.model.Language;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table( name = "characteristic_values" )
@SequenceGenerator(name = "characteristic_values_seq", sequenceName = "characteristic_values_seq")
public class CharacteristicValue extends ro.siveco.ram.starter.model.base.BaseEntity
{
	private String value;
	private String description;
    private Language language;
    private Characteristic characteristic;

	@Id
	@GeneratedValue( generator = "characteristic_values_seq", strategy = GenerationType.AUTO )
	public Long getId()
	{
		return super.getId();
	}

	public void setId( Long id )
	{
		super.setId(id);
	}
	
	@Column(name = "value", nullable = false)
    @NotNull
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	@Column(name = "description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	@ManyToOne
    @JoinColumn(name = "language_id", nullable = false)
	public Language getLanguage() {
		return language;
	}

	public void setLanguage(Language language) {
		this.language = language;
	}

	@ManyToOne
    @JoinColumn(name = "characteristic_id", nullable = false)
	public Characteristic getCharacteristic() {
		return characteristic;
	}

	public void setCharacteristic(Characteristic characteristic) {
		this.characteristic = characteristic;
	}

	@Override
	public String toString() {
		return "CharacteristicValue [value=" + value + ", description=" + description + ", language=" + language
				+ ", characteristic=" + characteristic + "]";
	}
}
