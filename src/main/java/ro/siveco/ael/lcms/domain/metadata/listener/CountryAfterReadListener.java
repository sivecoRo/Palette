package ro.siveco.ael.lcms.domain.metadata.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.auth.model.AelUser;
import ro.siveco.ael.lcms.domain.auth.service.AelUserService;
import ro.siveco.ael.lcms.domain.metadata.model.Country;
import ro.siveco.ael.lcms.domain.metadata.service.CountryService;
import ro.siveco.ael.web.controllers.WithUserBaseEntityController;
import ro.siveco.ram.event.listener.type.single.BaseAfterReadListener;
import ro.siveco.ram.event.type.single.AfterReadEvent;

@Component
public class CountryAfterReadListener extends BaseAfterReadListener<Country> {


    private final CountryService countryService;
    private final AelUserService aelUserService;

    @Autowired
    public CountryAfterReadListener(CountryService countryService, AelUserService aelUserService) {
        this.countryService = countryService;
        this.aelUserService = aelUserService;
    }

    @Override
    public void onApplicationEvent(AfterReadEvent<Country> event) {
        Country country = event.getDomainModel();
        AelUser currentUser = (AelUser) WithUserBaseEntityController.getUser();
        countryService.setDisplayName(country,aelUserService.getLocaleForUser(currentUser).getLanguage());
    }
}
