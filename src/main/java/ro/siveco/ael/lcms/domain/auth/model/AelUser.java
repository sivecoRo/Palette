package ro.siveco.ael.lcms.domain.auth.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.CallbackException;
import org.hibernate.Session;
import org.springframework.data.domain.Sort;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import ro.siveco.ael.lcms.domain.auth.view.Views;
import ro.siveco.ael.lcms.domain.profile.model.CountryDTO;
import ro.siveco.ael.lcms.domain.profile.model.Gender;
import ro.siveco.ael.lcms.domain.profile.util.CountryDTOConverter;
import ro.siveco.ael.lcms.domain.tenant.model.Tenant;
import ro.siveco.ram.model.AvatarModel;
import ro.siveco.ram.model.TargetImplementation;
import ro.siveco.ram.starter.ui.config.ActionDescriptor;
import ro.siveco.ram.starter.ui.config.PropertyViewDescriptor;
import ro.siveco.ram.starter.ui.config.Sorter;
import ro.siveco.ram.starter.ui.config.ViewDescriptor;
import ro.siveco.ram.starter.ui.model.ActionType;
import ro.siveco.ram.starter.ui.model.FormInputType;
import ro.siveco.ram.starter.ui.model.IndexViewType;
import ro.siveco.ram.starter.ui.model.ViewRendererType;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "users")
@SequenceGenerator(name = "SEQ_GEN", sequenceName = "users_seq")
@ViewDescriptor(pageSize = 10, viewTypes = {IndexViewType.GRID, IndexViewType.CARD},
        searchEnabled = true, defaultSearchProperty = "email",
        sort = {@Sorter(propertyName = "displayName", direction = Sort.Direction.ASC)},
        defaultActions = {
                                                        /* --- [actions] --- */
                /*@ActionDescriptor(type = ActionType.OPEN_CREATE_FORM, fixedAction = true),
                @ActionDescriptor(type = ActionType.OPEN_CREATE_FORM, fixedAction = true,
                        actionId = "importUserActionId", view = Views.ImportUserView.class),
                @ActionDescriptor(type = ActionType.OPEN_EDIT_FORM, mainAction = true),
                                                        *//* --- [flows] --- *//*
                @ActionDescriptor(type = ActionType.CREATE, forwardFor = "{modelName}-OPEN_CREATE_FORM",
                        availableOnlyInForm = true),
                @ActionDescriptor(type = ActionType.UPDATE, forwardFor = "{modelName}-OPEN_EDIT_FORM",
                        availableOnlyInForm = true),
                @ActionDescriptor(type = ActionType.LIST, availableInForm = true, backToFor = {
                        "{modelName}-OPEN_CREATE_FORM",
                        "{modelName}-CREATE",
                        "{modelName}-OPEN_PRESENTATION",
                        "{modelName}-OPEN_EDIT_FORM",
                        "{modelName}-UPDATE",
                        "{modelName}-DELETE"}
                )*/},
        otherActions = {
                //@ActionDescriptor(type = ActionType.OPEN_EDIT_FORM,  modelName = "userGroup")
        })
@ViewDescriptor(
        view = Views.AssociateGroupsToUserView.class,
        otherActions = {
                @ActionDescriptor(actionId = "associateGroupsToUserId", type = ActionType.OPEN_EDIT_FORM,
                        view = Views.AssociateGroupsToUserView.class),
                @ActionDescriptor(type = ActionType.UPDATE, forwardFor = "associateGroupsToUserId",
                        availableOnlyInForm = true),
                @ActionDescriptor(type = ActionType.LIST, availableInForm = true, inheritViewFromWrapper = false,
                        view = ro.siveco.ram.starter.ui.model.Views.__default.class, backToFor = {
                        "{modelName}-OPEN_EDIT_FORM",
                        "{modelName}-UPDATE"
                })}
)

@ViewDescriptor(
        view = Views.ImportUserView.class,
        searchEnabled = true, defaultSearchProperty = "importUserTemplate",
        defaultActions = {
                @ActionDescriptor(type = ActionType.OPEN_CREATE_FORM, fixedAction = true,
                        actionId = "importUserActionId"),
                @ActionDescriptor(type = ActionType.CREATE,
                        forwardFor = "importUserActionId", availableOnlyInForm = true),

                @ActionDescriptor(type = ActionType.CUSTOM,
                        forwardFor = "importUserActionId",
                        uri = "/user/download/template",
                        ajax = false,
                        label = "modal.aelUser.download.file.xls",
                        availableOnlyInForm = true,
                        actionId = "downloadXLSTemplateId"),
                @ActionDescriptor(type = ActionType.CUSTOM,
                        forwardFor = "importUserActionId",
                        uri = "/user/download/template/csv",
                        ajax = false,
                        label = "modal.aelUser.download.file.csv",
                        availableOnlyInForm = true,
                        actionId = "downloadCSVTemplateId"),
                @ActionDescriptor(type = ActionType.LIST, availableInForm = true,
                        inheritViewFromWrapper = false,
                        view = ro.siveco.ram.starter.ui.model.Views.__default.class,
                        backToFor = {
                                "importUserActionId",
                                "aelUser-CREATE-ImportUserView"
                        })
        }
)
@ViewDescriptor(
        view = Views.ForgotPasswordView.class,
        searchEnabled = true, defaultSearchProperty = "email",
        defaultActions = {
                @ActionDescriptor(type = ActionType.CREATE,
                        availableOnlyInForm = true)
        }
)
public class AelUser extends ro.siveco.ram.starter.model.base.BaseEntity implements User, AvatarModel {

    /**
     * The username used to log into the system
     */
    private String username;

    /**
     * The user's password
     */
    private String password;

    /**
     * The user's confirm password
     */
    private String confirmPassword;

    /**
     * The user's first name
     */
    private String firstName;

    /**
     * The user's last name
     */
    private String lastName;

    /**
     * The create date of the user
     */
    private Date createDate;


    /**
     * Date when the user account has been verified
     */
    private Date verifiedDate;

    /**
     * Marca utilizatorului
     */
    private String marca;

    /**
     * The user's unique id
     */
    private String relatedUid;

    /**
     * The user's email address
     */
    private String email;

    /**
     * Whether this is a system user and can not be modified
     */
    private Boolean readOnly = Boolean.FALSE;

    /**
     * Whether this user was imported after a synchronization with the Active Directory
     */
    private Boolean imported = Boolean.FALSE;

    private Boolean disabled = Boolean.FALSE;

    /**
     * The number of login attempts until the account will be blocked - max 5
     */

    private Integer noOfAttempts;

    /**
     * The expire date of the password
     */
    private Date expireDate;

    private Long externalId;

    private User manager;

    private Gender gender;

    private String displayName;

    private Tenant tenant;

    /**
     * The users's telephone number
     */
    private String phoneNo;

    /**
     * The users's resident city
     */
    private String city;

    /**
     * The user's address in the city
     */
    private String address;

    /**
     * The user's self registration identifier
     */
    private String registrationId;

    private List<AelGroup> groups = new ArrayList<>();

    private String alwaysNonEmptyDisplayName;

    private String userSocialId;

    private String avatarSrc;

    private String initials;

    private List<MultiSelectDTO> listAllGroups = new ArrayList<>();

    private CountryDTO country;

    private MultipartFile[] importUserTemplate;

    private Float lat;

    private Float lon;

    @Id
    @PropertyViewDescriptor(hidden = true, hiddenOnForm = true, required = false)
    @GeneratedValue(generator = "users_seq", strategy = GenerationType.SEQUENCE)
    public Long getId() {

        return super.getId();
    }

    @PropertyViewDescriptor(
            order = 1, rendererType = ViewRendererType.STRING,
            hiddenOnForm = true)
    @Column(name = "display_name", nullable = false, length = 1000)
    public String getDisplayName() {

        return displayName;
    }

    public void setDisplayName(String displayName) {

        this.displayName = displayName;
    }

    @PropertyViewDescriptor(
            hidden = true
    )
    @Column(name = "imported", nullable = false)
    public Boolean getImported() {

        return imported;
    }

    public void setImported(Boolean imported) {

        this.imported = imported;
    }

    @PropertyViewDescriptor(
            hidden = true
    )
    @Column(name = "readonly", nullable = false)
    public Boolean getReadOnly() {

        return readOnly;
    }

    public void setReadOnly(Boolean readOnly) {

        this.readOnly = readOnly;
    }

    @PropertyViewDescriptor(
            hiddenOnList = true, hiddenOnCard = true,
            rendererType = ViewRendererType.BOOLEAN,
            orderInForm = 14, inputType = FormInputType.SELECT, required = false)
    @Column(name = "disabled", nullable = false)
    public Boolean getDisabled() {

        return disabled;
    }

    public void setDisabled(Boolean disabled) {

        this.disabled = disabled;
    }

    @PropertyViewDescriptor(
            hidden = true
    )
    @JsonIgnore
    @Column(name = "noofattempts")
    public Integer getNoOfAttempts() {

        return noOfAttempts;
    }

    public void setNoOfAttempts(Integer noOfAttempts) {

        this.noOfAttempts = noOfAttempts;
    }

    @PropertyViewDescriptor(
            hiddenOnList = true, hiddenOnCard = true,
            rendererType = ViewRendererType.DATE,
            orderInForm = 13, inputType = FormInputType.DATE, required = false
    )
    @JsonIgnore
    @Column(name = "expiredate")
    public Date getExpireDate() {

        return expireDate;
    }

    public void setExpireDate(Date expireDate) {

        this.expireDate = expireDate;
    }

    @PropertyViewDescriptor(
            order = 2, rendererType = ViewRendererType.HTML,
            orderInForm = 2, inputType = FormInputType.TEXT
    )
    @Column(name = "username", unique = true, nullable = false, length = 500)
    public String getUsername() {

        return username;
    }

    public void setUsername(String username) {

        this.username = username;
    }

    @PropertyViewDescriptor(
            hidden = true
    )
    @Override
    @Transient
    public String getAlwaysNonEmptyDisplayName() {

        if (alwaysNonEmptyDisplayName == null) {
            if (!StringUtils.isEmpty(displayName)) {
                alwaysNonEmptyDisplayName = displayName;
            } else if (!StringUtils.isEmpty(username)) {
                alwaysNonEmptyDisplayName = username;
            } else {
                alwaysNonEmptyDisplayName = "User #" + getId();
            }
        }
        return alwaysNonEmptyDisplayName;
    }

    @PropertyViewDescriptor(
            hidden = true
    )
    @JsonIgnore
    @Column(name = "related_uid", unique = true, nullable = true, length = 500)
    public String getRelatedUid() {

        return relatedUid;
    }

    public void setRelatedUid(String _relatedUid) {

        relatedUid = _relatedUid;
    }

    @PropertyViewDescriptor(hidden = true)
    @ManyToMany(
            cascade = {CascadeType.PERSIST, CascadeType.MERGE},
            targetEntity = AelGroup.class
    )
    @JoinTable(
            name = "group_user",
            joinColumns = {@JoinColumn(name = "user_id")},
            inverseJoinColumns = {@JoinColumn(name = "group_id")}
    )
    @OrderBy("name")
    @JsonIgnore
    public List<AelGroup> getGroups() {

        return groups;
    }

    public void setGroups(List<AelGroup> groups) {

        this.groups = groups;
    }

    @PropertyViewDescriptor(
            hiddenOnList = true,
            order = 3,
            inputType = FormInputType.TEXT, rendererType = ViewRendererType.STRING,
            orderInForm = 15)

    @Override
    @Transient
    @PropertyViewDescriptor(
            hidden = true
    )
    @JsonIgnore
    @TargetImplementation(targetEntity = AelRole.class)
    public List<Role> getRoles() {

        List<Role> roles = new ArrayList<>();
        for (Group group :
                getGroups()) {
            roles.addAll(group.getRoles());
        }
        return roles;
    }

    @PropertyViewDescriptor(
            hiddenOnList = true, hiddenOnGrid = true, hiddenOnCard = true, hiddenOnPresentation = true,
            rendererType = ViewRendererType.AUTO_GENERATED,
            orderInForm = 6, inputType = FormInputType.PASSWORD
    )
    @Column(name = "password", length = 500)
    public String getPassword() {

        return password;
    }

    public void setPassword(String _password) {

        password = _password;
    }

    @Transient
    @PropertyViewDescriptor(
            hiddenOnList = true, hiddenOnGrid = true, hiddenOnCard = true, hiddenOnPresentation = true,
            rendererType = ViewRendererType.AUTO_GENERATED,
            orderInForm = 7, inputType = FormInputType.PASSWORD
    )
    public String getConfirmPassword() {

        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {

        this.confirmPassword = confirmPassword;
    }

    /* Used in ael-ro */
    @PropertyViewDescriptor(
            hidden = true
    )
    @JsonIgnore
    @Column(name = "external_id", nullable = true)
    public Long getExternalId() {

        return externalId;
    }

    public void setExternalId(Long externalId) {

        this.externalId = externalId;
    }

    @PropertyViewDescriptor(
            hiddenOnList = true, hiddenOnGrid = true, hiddenOnCard = true,
            orderInForm = 3, inputType = FormInputType.TEXT
    )
    @Column(name = "first_name", nullable = false, length = 500)
    public String getFirstName() {

        return firstName;
    }

    public void setFirstName(String _firstName) {

        firstName = _firstName;
    }

    @PropertyViewDescriptor(
            hiddenOnList = true, hiddenOnGrid = true, hiddenOnCard = true,
            orderInForm = 4, inputType = FormInputType.TEXT, inputFieldValidation = false
    )
    @Column(name = "last_name", nullable = true, length = 500)
    public String getLastName() {

        return lastName;
    }

    public void setLastName(String _lastName) {

        lastName = _lastName;
    }

    @PropertyViewDescriptor(
            hidden = true
    )
    @Column(name = "create_date")
    public Date getCreateDate() {

        return createDate;
    }

    public void setCreateDate(Date createDate) {

        this.createDate = createDate;
    }

    @PropertyViewDescriptor(
            hidden = true
    )
    @Column(name = "verified_date")
    public Date getVerifiedDate() {
        return verifiedDate;
    }

    public void setVerifiedDate(Date verifiedDate) {
        this.verifiedDate = verifiedDate;
    }

    @PropertyViewDescriptor(
            view = ro.siveco.ram.starter.ui.model.Views.__default.class,
            rendererType = ViewRendererType.HTML,
            orderInForm = 5, inputType = FormInputType.TEXT)
    @PropertyViewDescriptor( view = Views.ForgotPasswordView.class,
            rendererType = ViewRendererType.HTML,
            orderInForm = 1, inputType = FormInputType.TEXT)
    @Column(name = "mail", nullable = true, length = 100)
    public String getEmail() {

        return email;
    }

    public void setEmail(String _mail) {

        this.email = _mail;
    }

    @PropertyViewDescriptor(
            hidden = true
    )
    @Column(name = "marca", nullable = true, length = 100)
    public String getMarca() {

        return marca;
    }

    public void setMarca(String marca) {

        this.marca = marca;
    }

    //@JsonIgnore
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, targetEntity = AelUser.class)
    @JoinColumn(name = "manager_id", nullable = true)
    @PropertyViewDescriptor(
            hidden = true
    )
    public User getManager() {

        return manager;
    }

    public void setManager(User manager) {

        this.manager = manager;
    }

    @PropertyViewDescriptor(
            rendererType = ViewRendererType.ENUM,
            orderInForm = 8, inputType = FormInputType.SELECT, required = false
    )
    @Column(name = "gender")
    public Gender getGender() {

        return gender;
    }

    public void setGender(Gender gender) {

        this.gender = gender;
    }

    @ManyToOne
    @PropertyViewDescriptor(
            hidden = true
    )
    @JoinColumn(name = "tenant_id", nullable = true)
    public Tenant getTenant() {

        return tenant;
    }

    public void setTenant(Tenant tenant) {

        this.tenant = tenant;
    }

    @PropertyViewDescriptor(
            hiddenOnList = true, hiddenOnGrid = true, hiddenOnCard = true,
            rendererType = ViewRendererType.HTML,
            orderInForm = 12, inputType = FormInputType.TEXT, required = false
    )
    @Column(name = "phone", nullable = true, length = 20)
    public String getPhoneNo() {

        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {

        this.phoneNo = phoneNo;
    }

    @PropertyViewDescriptor(
            hiddenOnList = true, hiddenOnGrid = true, hiddenOnCard = true,
            rendererType = ViewRendererType.HTML,
            orderInForm = 10, inputType = FormInputType.TEXT, required = false
    )
    @Column(name = "city", nullable = true, length = 500)
    public String getCity() {

        return city;
    }

    public void setCity(String city) {

        this.city = city;
        if (city != null) {
            String[] arr = city.split(";");
            if (arr.length == 2) {
                this.lat = Float.parseFloat(arr[0]);
                this.lon = Float.parseFloat(arr[1]);
            }
        }
    }

    @PropertyViewDescriptor(
            hiddenOnList = true, hiddenOnGrid = true, hiddenOnCard = true,
            rendererType = ViewRendererType.HTML,
            orderInForm = 11, inputType = FormInputType.TEXT, required = false
    )
    @Column(name = "address", nullable = true, length = 500)
    public String getAddress() {

        return address;
    }

    public void setAddress(String address) {

        this.address = address;
    }

    @PropertyViewDescriptor(
            hiddenOnList = true, hiddenOnGrid = true, hiddenOnCard = true, hiddenOnPresentation = true,
            rendererType = ViewRendererType.MODEL,
            orderInForm = 9, inputType = FormInputType.LIST_OF_VALUES, required = false
    )
    @Column(name = "country", nullable = true)
    @Convert(converter = CountryDTOConverter.class)
    public CountryDTO getCountry() {

        return country;
    }

    public void setCountry(CountryDTO country) {

        this.country = country;
    }

    @Transient
    @PropertyViewDescriptor(
            view = ro.siveco.ram.starter.ui.model.Views.__default.class,
            ignore = true)
    @PropertyViewDescriptor(
            view = Views.ImportUserView.class,
            inputType = FormInputType.FILE, orderInForm = 1)
    public MultipartFile[] getImportUserTemplate() {

        return importUserTemplate;
    }

    public void setImportUserTemplate(MultipartFile[] importUserTemplate) {

        this.importUserTemplate = importUserTemplate;
    }

//    @Transient
//    @PropertyViewDescriptor(
//            view = ro.siveco.ram.starter.ui.model.Views.__default.class,
//            hidden = true)
//    @PropertyViewDescriptor(
//            view = Views.ImportUserView.class,
//            inputFieldSize = GridSize.TWO_PER_ROW,
//            readonly = true,
//            displayLabelInForm = false,
//            orderInForm = 2,
//            clickAction = {@ActionDescriptor(
//                    type = ActionType.CUSTOM,
//                    uri = "/user/download/template",
//                    label = "modal.aelUser.download.file.xls",
//                    ajax = false,
//                    availableOnlyInForm = true,
//                    actionId = "downloadXLSTemplateId")}
//    )
//    public String getDownloadXLSTemplate() {
//
//        return "modal.aelUser.download.file.xls";
//    }
//
//    @Transient
//    @PropertyViewDescriptor(
//            view = ro.siveco.ram.starter.ui.model.Views.__default.class,
//            hidden = true)
//    @PropertyViewDescriptor(
//            view = Views.ImportUserView.class,
//            inputFieldSize = GridSize.TWO_PER_ROW,
//            readonly = true,
//            displayLabelInForm = false,
//            orderInForm = 3,
//            clickAction = {@ActionDescriptor(type = ActionType.CUSTOM,
//                    uri = "/user/download/template/csv",
//                    label = "modal.aelUser.download.file.csv",
//                    ajax = false,
//                    availableOnlyInForm = true,
//                    actionId = "downloadCSVTemplateId")}
//    )
//    public String getDownloadCSVTemplate() {
//
//        return "modal.aelUser.download.file.csv";
//    }

    @PropertyViewDescriptor(
            hidden = true
    )
    @Column(name = "registration_id", nullable = true, length = 20)
    public String getRegistrationId() {

        return registrationId;
    }

    public void setRegistrationId(String registrationId) {

        this.registrationId = registrationId;
    }

    @PropertyViewDescriptor(
            hidden = true
    )
    @Column(name = "user_social_id")
    public String getUserSocialId() {

        return userSocialId;
    }

    public void setUserSocialId(String userSocialId) {

        this.userSocialId = userSocialId;
    }

    @Transient
    @PropertyViewDescriptor(
            hidden = true
    )
    public List<MultiSelectDTO> getListAllGroups() {

        return listAllGroups;
    }

    public void setListAllGroups(List<MultiSelectDTO> listAllGroups) {

        this.listAllGroups = listAllGroups;
    }

    public Boolean hasRole(Role role) {

        List<AelGroup> groups = getGroups();

        for (Group group : groups) {
            List roles = group.getRoles();
            if (roles.contains(role)) {
                return true;
            }
        }
        return false;
    }

    public boolean onSave(Session s) throws CallbackException {
//
//        Query query=s.createQuery("Select p.value from Preference p where p.name = 'global.AelClient.DISPLAY_NAME'");
//        List<String> preference=query.list() ;
//         if(preference!=null){
//        if("firstName".equals(preference.get(0)))
//            displayName=firstName+" "+lastName;
//        else
//            displayName=lastName+" "+firstName;
//         }
        return false;
    }

    public boolean onUpdate(Session s) throws CallbackException {

        return false;
    }

    public boolean onDelete(Session s) throws CallbackException {

        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public void onLoad(Session s, Serializable id) {

    }

    @Override
    @PropertyViewDescriptor(
            hidden = true
    )
    @Transient
    public String getObjectLabel() {

        return getDisplayName();
    }

    @Override
    public String toString() {

        return this.getFirstName();
    }

    @Override
    @Transient
    @PropertyViewDescriptor(
            hiddenOnList = true, hiddenOnGrid = true, hiddenOnCard = true,
            rendererType = ViewRendererType.IMAGE,
            orderInForm = 1, inputType = FormInputType.IMAGE_UPLOAD, required = false)
    public String getAvatarSrc() {

        return avatarSrc;
    }

    public void setAvatarSrc(String avatarSrc) {

        this.avatarSrc = avatarSrc;
    }

    @Override
    @Transient
    @PropertyViewDescriptor(
            hiddenOnGrid = true, hiddenOnCard = true,
            hiddenOnForm = true
    )
    public String getInitials() {

        if (initials == null) {
            initials = "";
            String[] names = getAlwaysNonEmptyDisplayName().split("\\s");
            for (String name : names) {
                if (name != null && name.length() > 0) {
                    initials += name.charAt(0);
                }
            }
            initials = initials.toUpperCase();
        }
        return initials;
    }

    public void setInitials(String initials) {

        this.initials = initials;
    }

    @Column(name = "lat", nullable = true)
    public Float getLat() {
        return lat;
    }
    public void setLat(Float lat) {
        this.lat = lat;
    }

    @Column(name = "lon", nullable = true)
    public Float getLon() {
        return lon;
    }
    public void setLon(Float lon) {
        this.lon = lon;
    }
}
