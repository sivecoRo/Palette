package ro.siveco.ael.lcms.domain.notification.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.siveco.ael.lcms.domain.notification.model.UserMessageFormat;
import ro.siveco.ael.lcms.repository.NotificationRepository;
import ro.siveco.ram.starter.service.BaseEntityService;

/**
 * Created by IuliaP on 29.09.2017.
 */
@Service
public class NotificationService extends BaseEntityService<UserMessageFormat> {

    @Autowired
    public NotificationService(NotificationRepository notificationRepository) {

        super(UserMessageFormat.class, notificationRepository);
    }
}
