package ro.siveco.ael.lcms.domain.chat.web;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.messaging.Message;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.user.SimpUser;
import org.springframework.messaging.simp.user.SimpUserRegistry;
import org.springframework.social.security.SocialAuthenticationToken;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ro.siveco.ael.facebook.model.SocialAuthenticatedUser;
import ro.siveco.ael.lcms.domain.auth.model.AelUser;
import ro.siveco.ael.lcms.domain.auth.model.User;
import ro.siveco.ael.lcms.domain.auth.service.AelUserService;
import ro.siveco.ael.lcms.domain.chat.model.ChatMessage;
import ro.siveco.ael.lcms.domain.chat.model.ChatMessageModel;
import ro.siveco.ael.lcms.domain.chat.model.ChatUserModel;
import ro.siveco.ael.lcms.domain.notification.model.UserMessageModel;
import ro.siveco.ael.lcms.domain.notification.service.UserMessageService;
import ro.siveco.ael.lcms.domain.profile.model.UserDetails;
import ro.siveco.ael.lcms.repository.ChatMessageRepository;
import ro.siveco.ael.lcms.repository.UserDetailsRepository;
import ro.siveco.ael.service.utils.DatesService;
import ro.siveco.ael.web.components.Template;
import ro.siveco.ael.web.components.WebPath;

import javax.inject.Inject;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;


@Controller
public class ChatController {

    private final AelUserService aelUserService;
    private final UserDetailsRepository userDetailsRepository;
    private final ChatMessageRepository chatMessageRepository;
    private final DatesService datesService;
    private SimpMessagingTemplate template;
    private SimpUserRegistry userRegistry;
    private UserMessageService userMessageService;


    @Inject
    public ChatController(SimpUserRegistry userRegistry, SimpMessagingTemplate template,
                          AelUserService aelUserService, ChatMessageRepository chatMessageRepository,
                          UserMessageService userMessageService, UserDetailsRepository userDetailsRepository,
                          DatesService datesService) {
        this.userRegistry = userRegistry;
        this.template = template;
        this.aelUserService = aelUserService;
        this.chatMessageRepository = chatMessageRepository;
        this.userMessageService = userMessageService;
        this.userDetailsRepository = userDetailsRepository;
        this.datesService = datesService;
    }


    @MessageMapping("/sendMessage")
    public void getMessage(Message<Object> message, @Payload ChatMessageModel chatMessage) throws Exception {
        Principal principal = message.getHeaders().get(SimpMessageHeaderAccessor.USER_HEADER, Principal.class);
        String sender = principal.getName();
        String receiver = chatMessage.getReceiver();

        if (receiver != null) {
            ChatMessage msg = new ChatMessage();
            msg.setReceiver(receiver);
            msg.setSender(sender);
            msg.setMessage(chatMessage.getMessage());
            msg.setCreationDate(new Date());
            chatMessageRepository.save(msg);


            chatMessage.setSender(sender);
            template.convertAndSendToUser(receiver, "/queue/messages", chatMessage);

            if (!sender.equals(receiver)) {
                template.convertAndSendToUser(sender, "/queue/messages", chatMessage);
            }
        }
    }


    @GetMapping(WebPath.CHAT_PATH)
    public String chatPage(Principal principal, Model model) {
        return Template.CHAT;
    }


    public void sendUsersList(String username, Boolean sendToAll) {
        List<ChatUserModel> usersList = new ArrayList<ChatUserModel>();
        Long tenantId = aelUserService.getTenantIdByUsername(username);
        List<AelUser> usersFromTenant = aelUserService.getUsersByTenant(tenantId);

        List<String> usersOnline = new ArrayList<String>();
        for (SimpUser user : this.userRegistry.getUsers()) {
            usersOnline.add(user.getName());
        }

        for (AelUser user : usersFromTenant) {
            ChatUserModel newCum =
                    new ChatUserModel(user.getUsername(), user.getFirstName() + " " + user.getLastName());
            List<UserDetails> uds = userDetailsRepository.getByUserId(user.getId());

            if (uds != null && !uds.isEmpty()) {
                newCum.setHasImage(uds.get(0).getImage() != null);
            }

            if (usersOnline.contains(user.getUsername())) {
                newCum.setActive(true);
            }

            usersList.add(newCum);
        }

        if (sendToAll) {
            for (String user : usersOnline) {
                template.convertAndSendToUser(user, "/queue/active_users", usersList);
            }
        } else {
            template.convertAndSendToUser(username, "/queue/active_users", usersList);
        }
    }


    @MessageMapping("/activeUsers")
    public void activeUsers(Message<Object> message, Principal principal) {
        sendUsersList(principal.getName(), false);
    }


    @MessageMapping("/getMessages")
    public void getMessages(@Payload ChatMessageModel chatMessage, Principal principal) {
        List<ChatMessage> messages =
                chatMessageRepository.getMessagesBetweenTwoUsers(chatMessage.getSender(), chatMessage.getReceiver());

        for (ChatMessage message : messages) {
            template.convertAndSendToUser(principal.getName(), "/queue/messages",
                    new ChatMessageModel(message.getReceiver(), message.getSender(), message.getMessage(),
                            message.getCreationDate()));
        }
    }


    @MessageMapping("/getNavbarNumbers")
    public void getNavbarNumbers(Principal principal) {
        if (principal instanceof SocialAuthenticationToken) {
            User user = ((SocialAuthenticatedUser) ((SocialAuthenticationToken) principal).getPrincipal()).getUser();
            sendNavbarNumbers(user.getUsername());
        } else {
            sendNavbarNumbers(principal.getName());
        }
    }


    public void sendNavbarNumbers(String username) {
        Optional<User> user = aelUserService.findByUsername(username);

        if (user == null || user.equals(Optional.empty())) {
            return;
        }

        Integer navbarMessagesNr = 10;
        Integer maxTextSize = 20;

        Long unreadMsgNr = userMessageService.getAllUnreadMessagesByUserId(user.get().getId());
        Long unreadNotifNr = userMessageService.getAllUnreadNotificationsByUserId(user.get().getId());
        List<UserMessageModel> lastMsg =
                userMessageService.getLastNrUnreadMessagesByUserId(user.get().getId(), navbarMessagesNr);
        List<UserMessageModel> lastNotif =
                userMessageService.getLastNotificationsByUserId(user.get().getId(), navbarMessagesNr);


        String response = "{\"unreadMsgNr\" : \"" + unreadMsgNr +
                "\", \"unreadNotifNr\" : \"" + unreadNotifNr + "\", \"lastMsg\" : [ ";

        for (UserMessageModel um : lastMsg) {
//			if ( um.getFrom().length()>maxTextSize ) {
//				um.setFrom(um.getFrom().substring(0,maxTextSize) + "...");
//			}
//			if ( um.getSubject().length()>maxTextSize ) {
//				um.setSubject(um.getSubject().substring(0,maxTextSize) + "...");
//			}

            response += " { \"from\": \"" + um.getFrom() + "\", \"subject\": \"" + um.getSubject() + "\"},";
        }
        response = response.substring(0, response.length() - 1);
        response += " ] , \"lastNotif\" : [ ";

        for (UserMessageModel um : lastNotif) {
//			if ( um.getFrom().length()>maxTextSize ) {
//				um.setFrom(um.getFrom().substring(0,maxTextSize) + "...");
//			}
//			if ( um.getSubject().length()>maxTextSize ) {
//				um.setSubject(um.getSubject().substring(0,maxTextSize) + "...");
//			}

            String sendDate = "\""+datesService.formatMachineDateTime(um.getSentDate())+"\"";
            String readDate = um.getReadDate() != null ? "\""+datesService.formatMachineDateTime(um.getReadDate())+"\"" : null;
            String sender = null;
            try {
                sender = aelUserService.resourceToJsonString(um.getFromUser());
            } catch (JsonProcessingException e) {
                sender = "{\"alwaysNonEmptyDisplayName\":\" + um.getFrom() + \"}";
            }
            response += " { \"from\": \"" + um.getFrom() + "\", \"sender\":" + sender + ", \"subject\": \"" +
                    um.getSubject() + "\", \"sendDate\":" +
                    sendDate + ", \"readDate\":" + readDate + "},";
        }
        response = response.substring(0, response.length() - 1);
        response += " ] }";

        template.convertAndSendToUser(username, "/queue/navbarNumbers", response);
    }

}