package ro.siveco.ael.lcms.domain.course.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.course.model.PlannedCourseStudent;
import ro.siveco.ram.event.listener.type.single.BaseAfterReadListener;
import ro.siveco.ram.event.type.single.AfterReadEvent;

/**
 * User: AlexandruVi
 * Date: 2017-10-18
 */
@Component
public class PlannedCourseAfterReadListener extends BaseAfterReadListener<PlannedCourseStudent> {

    /**
     * Handle an application event.
     *
     * @param event the event to respond to
     */
    @Override
    public void onApplicationEvent(AfterReadEvent<PlannedCourseStudent> event) {

        if (event.getOptionalDomainModel().isPresent()) {
            PlannedCourseStudent learnerCourseAssociation = event.getOptionalDomainModel().get();
        }
    }
}
