package ro.siveco.ael.lcms.domain.metadata.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.metadata.model.Metadata;
import ro.siveco.ael.lcms.domain.metadata.model.Tag;
import ro.siveco.ael.lcms.domain.metadata.service.MetadataService;
import ro.siveco.ram.app.exception.AppException;
import ro.siveco.ram.event.listener.type.single.BaseBeforeDeleteListener;
import ro.siveco.ram.event.type.single.BeforeDeleteEvent;

import java.util.List;

/**
 * Created by IuliaP on 24.10.2017.
 */
@Component
public class ValidateTagBeforeDeleteListener extends BaseBeforeDeleteListener<Tag> {

    @Autowired
    MetadataService metadataService;

    @Override
    public void onApplicationEvent(BeforeDeleteEvent<Tag> event) {
        Tag tag = event.getDomainModel();
        List<Metadata> metadataList = metadataService.findByTag( tag );
        if( metadataList != null && metadataList.size() > 0 ) {
            throw new AppException("error.tag.metadata.exists");
        }
    }
}
