package ro.siveco.ael.lcms.domain.auth.listener;

import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.auth.model.AelFunction;
import ro.siveco.ram.event.listener.type.single.BaseBeforeUpdateListener;
import ro.siveco.ram.event.type.single.BeforeUpdateEvent;

/**
 * Created by AndradaC on 9/1/2017.
 */
@Component
public class SetDatabaseInformationForFunctionBeforeUpdateListener extends BaseBeforeUpdateListener<AelFunction>{

    @Override
    public void onApplicationEvent(BeforeUpdateEvent<AelFunction> event) {
        AelFunction aelFunction = event.getDomainModel();
        AelFunction dbAelFunction = event.getStoredDomainModel();

        //aelFunction.setRoles(dbAelFunction.getRoles());
    }
}
