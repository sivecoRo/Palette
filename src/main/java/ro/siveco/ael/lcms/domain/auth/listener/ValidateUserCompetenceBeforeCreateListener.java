package ro.siveco.ael.lcms.domain.auth.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.auth.model.UserCompetence;
import ro.siveco.ael.lcms.domain.auth.service.UserCompetenceService;
import ro.siveco.ram.app.exception.AppException;
import ro.siveco.ram.event.listener.type.single.BaseBeforeCreateListener;
import ro.siveco.ram.event.type.single.BeforeCreateEvent;

/**
 * Created by AndradaC on 9/3/2017.
 */
@Order(value = 2)
@Component
public class ValidateUserCompetenceBeforeCreateListener extends BaseBeforeCreateListener<UserCompetence>{

    @Autowired
    private UserCompetenceService userCompetenceService;

    @Override
    public void onApplicationEvent(BeforeCreateEvent<UserCompetence> event) {
        UserCompetence userCompetence = event.getDomainModel();
        if(userCompetenceService.previousConnexionExists(userCompetence)){
            throw new AppException("error.userCompetence.create.duplicateEntry");
        }
    }
}
