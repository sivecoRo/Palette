package ro.siveco.ael.lcms.domain.organization.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.siveco.ael.lcms.domain.organization.model.JobTitle;
import ro.siveco.ael.lcms.domain.tenant.model.Tenant;
import ro.siveco.ael.lcms.repository.JobTitleRepository;
import ro.siveco.ram.starter.service.BaseEntityService;

import java.util.List;

/**
 * Created by AndradaC on 5/29/2017.
 */
@Service
public class JobTitleService extends BaseEntityService<JobTitle>
{
    @Autowired
    public JobTitleService(JobTitleRepository jobTitleRepository) {
        super(JobTitle.class, jobTitleRepository);
    }

    public JobTitle getByName(String name)
    {
        List<JobTitle> jobTitles = ((JobTitleRepository)getResourceRepository()).getByName(name);
		if( jobTitles != null && jobTitles.size() > 0 )
		{
			return ( JobTitle ) jobTitles.get( 0 );
		}
		else
		{
			return null;
		}
    }

    public JobTitle findByNameAndTenant(String name, Tenant tenant) {
        return ((JobTitleRepository) getResourceRepository()).findByNameAndTenant(name, tenant);
    }

    public List<JobTitle> findByTenant(Tenant tenant) {
        return ((JobTitleRepository) getResourceRepository()).findJobTitlesByTenant(tenant.getId());
    }

    public JobTitle findById(Long jobTitleId) {
        return (JobTitle) ((JobTitleRepository) getResourceRepository()).findOne(jobTitleId);
    }

    public List<JobTitle> findJobTitlesByTenant(Long tenantId) {
        return ((JobTitleRepository) getResourceRepository()).findJobTitlesByTenant(tenantId);
    }

    public Boolean validateJobTitle(JobTitle jobTitle) {
        Boolean isNewJobTitle = jobTitle.getId() == null;
        JobTitle jobTitleWithSameLabel = findByNameAndTenant(jobTitle.getName(), jobTitle.getTenant());
        if(jobTitleWithSameLabel != null) {
            if(isNewJobTitle || jobTitle.getId().compareTo(jobTitleWithSameLabel.getId()) != 0) {
                return Boolean.FALSE;
            }
        }
        return Boolean.TRUE;
    }
}
