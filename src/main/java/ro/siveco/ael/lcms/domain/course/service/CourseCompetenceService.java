package ro.siveco.ael.lcms.domain.course.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.siveco.ael.lcms.domain.course.model.CourseCompetence;
import ro.siveco.ael.lcms.domain.taxonomies.model.Competence;
import ro.siveco.ael.lcms.domain.taxonomies.model.TaxonomyLevel;
import ro.siveco.ael.lcms.repository.CourseCompetenceRepository;
import ro.siveco.ram.starter.service.BaseEntityService;

@Service
public class CourseCompetenceService extends BaseEntityService<CourseCompetence> {

    @Autowired
	public CourseCompetenceService(CourseCompetenceRepository resourceRepository) {
		super(CourseCompetence.class, resourceRepository);
	}

	public Boolean previousConnexionExists(CourseCompetence courseCompetence){
		return ((CourseCompetenceRepository)getResourceRepository()).
				findDuplicateConnexion(
						courseCompetence.getCourse().getId(),
						courseCompetence.getCompetence().getId(),
						courseCompetence.getId() != null ? courseCompetence.getId() : 0L
				) != null;
	}

	public Boolean competenceExists(Competence competence) {

		Boolean value = ((CourseCompetenceRepository) getResourceRepository()).findByCompetence(competence).isEmpty();
		return !value;
	}

	public Boolean taxonomyLevelExists(TaxonomyLevel taxonomyLevel) {

		Boolean value = ((CourseCompetenceRepository) getResourceRepository()).findByTaxonomyLevel(taxonomyLevel).isEmpty();
		return !value;
	}
}