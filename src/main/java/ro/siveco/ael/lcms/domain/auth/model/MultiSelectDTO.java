package ro.siveco.ael.lcms.domain.auth.model;

import java.io.Serializable;

public class MultiSelectDTO implements Serializable{

    Long id;

    String name;

    Long status;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }
}
