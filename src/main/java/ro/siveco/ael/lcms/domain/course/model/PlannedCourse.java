package ro.siveco.ael.lcms.domain.course.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.hibernate.annotations.*;
import ro.siveco.ael.lcms.domain.auth.model.AelUser;
import ro.siveco.ael.lcms.domain.metadata.model.Comment;
import ro.siveco.ram.starter.ui.config.ActionDescriptor;
import ro.siveco.ram.starter.ui.config.PropertyViewDescriptor;
import ro.siveco.ram.starter.ui.config.ViewDescriptor;
import ro.siveco.ram.starter.ui.model.*;

import javax.persistence.CascadeType;
import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: Siveco Romania SA</p>
 *
 * @author RobertR
 * @version : 1.0 / Dec 4, 2006
 */

@Entity
@Table(name = "planned_courses")
@SequenceGenerator(name = "planned_courses_seq", sequenceName = "planned_courses_seq")
// Numele tabelei asociate trebuie declarat ca plural ral numelui entitatii
@FilterDef(name = "tenantPlannedCourseFilter", parameters = @ParamDef(name = "tenantId", type = "long"))
@Filters({
        @Filter(name = "tenantPlannedCourseFilter",
                condition = "exists (select 1 from users u where u.id=creator_id and u.tenant_id=:tenantId)")
})
@ViewDescriptor(pageSize = 9, viewTypes = {IndexViewType.CARD},
        cardSizeLarge = GridSize.THREE_PER_ROW,
        defaultActions = {
             /* --- [actions] --- */
                //  @ActionDescriptor(type = ActionType.DELETE, mainAction = true, requiresConfirmation = true),
                @ActionDescriptor(type = ActionType.OPEN_PRESENTATION, availableOnlyInPresentation = true),
                @ActionDescriptor(type = ActionType.CUSTOM, actionId = "addToFavoritesId",
                        uri = "/sessions/addItemToFavorites/{id}", mainAction = true),
                @ActionDescriptor(type = ActionType.CUSTOM_GET, actionId = "removeItemId",
                        uri = "/sessions/removeItem/{id}", mainAction = true, requiresConfirmation = true),
                                                        /* --- [flows] --- */
                @ActionDescriptor(type = ActionType.LIST, availableInForm = true, availableInPresentation = true,
                        cssClass = "generic-action action-list action-plannedCourse-list btn secondary meta-controller left back-action",
                        backToFor = {
                                "{modelName}-OPEN_PRESENTATION"
                                //"{modelName}-DELETE"
                        })
        }
)
public class PlannedCourse extends ro.siveco.ram.starter.model.base.BaseEntity implements Cloneable {

    protected List<PlannedCourseStudent> plannedCourseStudents = new ArrayList<PlannedCourseStudent>();

    private Course course;

    private String scheduleName;

    private Date signupStartDate;

    private Date signupEndDate;

    private Date startDate;

    private Date endDate;

    private Time startTime;

    private Time endTime;

    private Integer minNrOfStudents;

    private Integer maxNrOfStudents;

    private String location;

    private String locationCoordinates;

    private AelUser registrationUser;

    private AelUser registrationApprovalUser;

    private AelUser creator;

    private AelUser teacher;

    private Boolean selfRegistration = Boolean.FALSE;

    private PlannedCourseStatus status;

    private BigDecimal rating;

    private PlannedCourseTeachingMode teachingMode = PlannedCourseTeachingMode.ONLINE_COURSE;

    private String courseDescription;

    private String courseObjective;

    private String objective;

    private Boolean advancedOptions = Boolean.FALSE;

    private String imageSrc = "/img/generic/generic-course-light.png";

    private Integer usersAddedToFavorites;

    private String displayUsersAddedToFavorites;

    private CourseType courseType;

    // Palette offer details information
    private String ownerImageSrc = "/img/generic/generic-user.png";

    private String ownerName;

    private String ownerEmail;

    private String ownerPhone;

    private Integer ownerItemsNumber;

    private String itemSchedulePeriod;

    private String itemScheduleTimePeriod;

    private String addToFavoritesBtn= "";

    private String itemLikeBtn= "";

    private String itemDislikeBtn= "";

    private Integer itemLikeNumber;

    private Integer itemDislikeNumber;

    private Long matchMetadataNo;

    private byte[] image;

    private String readMoreLink;

    private String country;

    private String countryCode;

    private List<Comment> comments= new ArrayList<>();

    private Boolean addedToFavorite;

    private Boolean removed;

    private String displayStartDate;

    private Boolean attending;

    private String attendingBtn= "";

    private Integer usersAttending;

    private String displayUsersAttending;

    private String allAttendingBtn= "";

    private Boolean reviewEmailSent;

    private Boolean reviewed;

    private List<AelUser> allAttendingStudents;

    private List<AelUser>  attendingStudentsForDisplay;

    private Boolean showOwnerPhone;

    private Boolean showOwnerEmail;

    private Float distance;

    private Float lat;

    private Float lon;

    public PlannedCourse() {

    }

    public PlannedCourse(Long id, String scheduleName, Date startDate) {

        this.scheduleName = scheduleName;
        this.startDate = startDate;
    }

    public PlannedCourse(Long id, Course course, Date startDate) {

        this.course = course;
        this.startDate = startDate;
    }

    public PlannedCourse(Long id, Course course, byte[] image, Date startDate, String scheduleName, String location, AelUser creator, AelUser teacher, PlannedCourseStatus status,
                         BigDecimal rating, PlannedCourseTeachingMode teachingMode, long matchMetadataNo) {
        this.setId(id);
        this.course = course;
        this.image = image;
        this.startDate = startDate;
        this.scheduleName = scheduleName;
        this.location = location;
        this.creator = creator;
        this.teacher = teacher;
        this.status = status;
        this.rating = rating;
        this.teachingMode = teachingMode;
        this.matchMetadataNo = matchMetadataNo;
    }

    public PlannedCourse(Long id, Course course, byte[] image, Date startDate) {
        this.setId(id);
        this.course = course;
        this.image = image;
        this.startDate = startDate;
    }

    public PlannedCourse(Long id, Course course, byte[] image, Date startDate, Float distance) {
        this.setId(id);
        this.course = course;
        this.image = image;
        this.startDate = startDate;
        this.distance = distance;
    }

    @Override
    @Id
    @GeneratedValue(generator = "planned_courses_seq", strategy = GenerationType.AUTO)
    public Long getId() {

        return super.getId();
    }

    @ManyToOne
    @NotNull
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    @PropertyViewDescriptor(
            hidden = true, order = 1, displayLabelInPresentation = false)
    public Course getCourse() {

        return course;
    }

    public void setCourse(Course _course) {

        course = _course;
    }

    @Transient
    @PropertyViewDescriptor(order = 5, hiddenOnPresentation = true, rendererType = ViewRendererType.HTML, displayLabelInPresentation = false)
    public String getName() {
        return getCourse().getName();
    }

    @Column(name = "schedule_name")
    @PropertyViewDescriptor(hidden = true)
    public String getScheduleName() {

        return scheduleName;
    }

    public void setScheduleName(String _scheduleName) {

        scheduleName = _scheduleName;
    }

    @Column(name = "signup_start_date")
    @PropertyViewDescriptor(
            hidden = true)
    public Date getSignupStartDate() {

        return signupStartDate;
    }

    public void setSignupStartDate(Date _signupStartDate) {

        signupStartDate = _signupStartDate;
    }

    @Column(name = "signup_end_date")
    @PropertyViewDescriptor(
            hidden = true)
    public Date getSignupEndDate() {

        return signupEndDate;
    }

    public void setSignupEndDate(Date _signupEndDate) {

        signupEndDate = _signupEndDate;
    }

    @Column(name = "start_date", nullable = false)
    @PropertyViewDescriptor(hiddenOnForm = true, hiddenOnPresentation = true, hiddenOnCard = true,
            order = 2, rendererType = ViewRendererType.DATE, displayLabelInPresentation = false)
    public Date getStartDate() {

        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    @Transient
    @PropertyViewDescriptor(hiddenOnForm = true, hiddenOnPresentation = true,
            order = 2, rendererType = ViewRendererType.STRING, displayLabelInPresentation = false)
    public String getDisplayStartDate() {
        return displayStartDate;
    }

    public void setDisplayStartDate(String displayStartDate) {
        this.displayStartDate = displayStartDate;
    }


    @Column(name = "end_date", nullable = false)
    @PropertyViewDescriptor(
            hiddenOnCard = true, hiddenOnPresentation = true, displayLabelInPresentation = false,
            hiddenOnForm = true, displayLabelInForm = false,
            order = 3, rendererType = ViewRendererType.DATE)
    public Date getEndDate() {

        return endDate;
    }

//	@OneToMany( mappedBy = "plannedCourse", cascade = {CascadeType.ALL} )
//	@Cascade( {org.hibernate.annotations.CascadeType.DELETE_ORPHAN} )
//	@OrderBy( "id" )
//	public List<ParticipationFilter> getParticipationFilters()
//	{
//		return participationFilters;
//	}

    public void setEndDate(Date _endDate) {

        endDate = _endDate;
    }

    @Column(name = "start_time")
    @PropertyViewDescriptor( hiddenOnCard = true, hiddenOnList = true,
            hiddenOnGrid = true, hiddenOnForm = true, hiddenOnPresentation = true)
    public Time getStartTime() {
        return startTime;
    }

    public void setStartTime(Time startTime) {
        this.startTime = startTime;
    }

    @Column(name = "end_time")
    @PropertyViewDescriptor( hiddenOnCard = true, hiddenOnList = true,
            hiddenOnGrid = true, hiddenOnForm = true, hiddenOnPresentation = true)
    public Time getEndTime() {
        return endTime;
    }

    public void setEndTime(Time endTime) {
        this.endTime = endTime;
    }

    @Column(name = "min_nr_of_students")
    @PropertyViewDescriptor( hidden = true)
    public Integer getMinNrOfStudents() {
        return minNrOfStudents;
    }

    public void setMinNrOfStudents(Integer minNrOfStudents) {
        this.minNrOfStudents = minNrOfStudents;
    }

    @Column(name = "max_nr_of_students")
    @PropertyViewDescriptor(
            hiddenOnList = true, hiddenOnGrid = true, hiddenOnCard = true, hiddenOnPresentation = true,
            hiddenOnForm = true,
            rendererType = ViewRendererType.INTEGER,
            inputType = FormInputType.NUMBER, required = false, orderInForm = 17, groupName = "numberFields")
    public Integer getMaxNrOfStudents() {

        return maxNrOfStudents;
    }

    public void setMaxNrOfStudents(Integer _maxNrOfStudents) {

        maxNrOfStudents = _maxNrOfStudents;
    }

    @ManyToOne
    @JoinColumn(name = "registration_user_id")
    @PropertyViewDescriptor(
            hiddenOnList = true, hiddenOnGrid = true, hiddenOnCard = true, hiddenOnPresentation = true,
            hiddenOnForm = true,
            rendererType = ViewRendererType.MODEL,
            inputType = FormInputType.LIST_OF_VALUES, required = false, orderInForm = 10,
            groupName = "managedRegistrationInfo")
    public AelUser getRegistrationUser() {

        return registrationUser;
    }

    public void setRegistrationUser(AelUser registrationUser) {

        this.registrationUser = registrationUser;
    }

    @ManyToOne
    @JoinColumn(name = "registration_approval_user_id")
    @PropertyViewDescriptor(
            hiddenOnList = true, hiddenOnGrid = true, hiddenOnCard = true, hiddenOnPresentation = true,
            hiddenOnForm = true,
            rendererType = ViewRendererType.MODEL,
            inputType = FormInputType.LIST_OF_VALUES, required = false, orderInForm = 11,
            groupName = "managedRegistrationInfo")
    public AelUser getRegistrationApprovalUser() {

        return registrationApprovalUser;
    }

    public void setRegistrationApprovalUser(AelUser _registrationApprovalUser) {

        registrationApprovalUser = _registrationApprovalUser;
    }

    @ManyToOne(targetEntity = AelUser.class)
    @JoinColumn(name = "creator_id")
    @PropertyViewDescriptor(hidden = true)
    public AelUser getCreator() {

        return creator;
    }

    public void setCreator(AelUser _creator) {

        creator = _creator;
    }

    @ManyToOne
    @JoinColumn(name = "teacher_id")
    @PropertyViewDescriptor(
            hiddenOnList = true, hiddenOnCard = true, hiddenOnGrid = true, hiddenOnPresentation = true,
            hiddenOnForm = true,
            order = 2, rendererType = ViewRendererType.MODEL_AVATAR,
            inputType = FormInputType.LIST_OF_VALUES, orderInForm = 3, groupName = "mandatoryFields")
    public AelUser getTeacher() {

        return teacher;
    }

    public void setTeacher(AelUser _teacher) {

        teacher = _teacher;
    }

    @OneToMany(mappedBy = "plannedCourse", cascade = {CascadeType.ALL})
    @Cascade({org.hibernate.annotations.CascadeType.DELETE_ORPHAN})
    @OrderBy("approved")
    @PropertyViewDescriptor(hidden = true)
    public List<PlannedCourseStudent> getPlannedCourseStudents() {

        return plannedCourseStudents;
    }

    public void setPlannedCourseStudents(List<PlannedCourseStudent> _plannedCourseStudents) {

        plannedCourseStudents = _plannedCourseStudents;
    }

    @Column(name = "self_registration", nullable = false)
    @PropertyViewDescriptor(
            hiddenOnList = true, hiddenOnGrid = true, hiddenOnCard = true, hiddenOnPresentation = true,
            hiddenOnForm = true,
            rendererType = ViewRendererType.BOOLEAN,
            inputType = FormInputType.SELECT, orderInForm = 9, groupName = "advancedInfo")
    public Boolean getSelfRegistration() {

        return selfRegistration;
    }

//	public void setParticipationFilters( List<ParticipationFilter> _participationFilters )
//	{
//		participationFilters = _participationFilters;
//	}

    public void setSelfRegistration(Boolean _selfRegistration) {

        selfRegistration = _selfRegistration;
    }

    @Column(nullable = false)
    @PropertyViewDescriptor(
            hidden = true,
            displayLabelInPresentation = false, groupName = "status")
    public PlannedCourseStatus getStatus() {

        return status;
    }

    public void setStatus(PlannedCourseStatus _status) {

        status = _status;
    }
    @Formula(
            value = "(select coalesce(sum(pcs.rating)/count(pcs.id), 0) from planned_course_students pcs where pcs.rating is not null and pcs.planned_course_id=id)")
    @PropertyViewDescriptor(
            displayLabelInPresentation = false, order = 2, editInline = false,
            rendererType = ViewRendererType.RATING_STARS,
            hidden = true)
    public BigDecimal getRating() {

        return rating;
    }

    public void setRating(BigDecimal rating) {

        this.rating = rating;
    }

    @Column(name = "teaching_mode", nullable = false)
    @PropertyViewDescriptor(
            hiddenOnList = true, hiddenOnGrid = true, hiddenOnCard = true, hiddenOnPresentation = true,
            hiddenOnForm = true,
            displayLabelInPresentation = false, order = 6, rendererType = ViewRendererType.ENUM,
            inputType = FormInputType.SELECT, required = false, orderInForm = 8, groupName = "advancedInfo")
    public PlannedCourseTeachingMode getTeachingMode() {

        return teachingMode;
    }

    public void setTeachingMode(PlannedCourseTeachingMode teachingMode) {

        this.teachingMode = teachingMode;
    }

    @Column(name = "objective", length = 5000)
    @PropertyViewDescriptor(
            hiddenOnList = true, hiddenOnGrid = true, hiddenOnCard = true, hiddenOnPresentation = true,
            hiddenOnForm = true,
            rendererType = ViewRendererType.HTML,
            inputType = FormInputType.TEXTAREA, required = false, orderInForm = 7, groupName = "advancedInfo")
    public String getObjective() {

        return objective;
    }

    public void setObjective(String objective) {

        this.objective = objective;
    }

    @Column(name = "location", length = 500)
    @PropertyViewDescriptor(
            order = 10, hiddenOnGrid = true, hiddenOnList = true, hiddenOnCard = true,
            hiddenOnForm = true, groupName = "scheduleLocation" )
    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Column(name = "location_coordinates")
    @PropertyViewDescriptor(hidden = true)
    public String getLocationCoordinates() {
        return locationCoordinates;
    }

    public void setLocationCoordinates(String locationCoordinates) {
        this.locationCoordinates = locationCoordinates;

        if (locationCoordinates != null) {
            String[] arr = locationCoordinates.split(";");
            if (arr.length == 2) {
                this.lat = Float.parseFloat(arr[0]);
                this.lon = Float.parseFloat(arr[1]);
            }
        }
    }

    /**
     * @return the type of a planned course
     */
    @PropertyViewDescriptor(hidden = true)
    public PlannedCourseType obtainType() {

        if (getSelfRegistration() == null) {
            setSelfRegistration(false);
        }
        if (!getSelfRegistration()) {
            return PlannedCourseType.ASYNCHRONOUS_SRF;
        }
        return PlannedCourseType.ASYNCHRONOUS_SRT;
    }

    @Transient
    @PropertyViewDescriptor(order = 7, hiddenOnList = true,
            hiddenOnCard = true, hiddenOnForm = true, hiddenOnGrid = true, groupName = "itemDescription")
    public String getCourseDescription() {

        if (getCourse() != null) {
            return getCourse().getDescription();
        }
        return "";
    }

    @Transient
    @PropertyViewDescriptor(order = 8, hiddenOnList = true,
            hiddenOnCard = true, hiddenOnForm = true, hiddenOnGrid = true, groupName = "itemObjective")
    public String getCourseObjective() {

        if (getCourse() != null) {
            return getCourse().getObjective();
        }
        return "";
    }

    @Transient
    @PropertyViewDescriptor(
            hiddenOnForm = true, displayLabelInForm = true, displayLabelInPresentation = false,
            order = -3, rendererType = ViewRendererType.IMAGE, groupName = "itemImage")
    public String getImageSrc() {
        return imageSrc;
    }

    public void setImageSrc(String imageSrc) {
        this.imageSrc = imageSrc;
    }

    @Override
    @Transient
    @PropertyViewDescriptor(
            order = -2,/* First property */
            hiddenOnCard = true,
            hiddenOnForm = true,
            hiddenOnGrid = true,
            displayLabelInPresentation = false,
            rendererType = ViewRendererType.OBJECT_LABEL,
            groupName = "itemNameAndFavorites",
            clickAction = {
                    @ActionDescriptor(type = ActionType.OPEN_PRESENTATION),
                    @ActionDescriptor(type = ActionType.LIST, backToFor = "{modelName}-OPEN_PRESENTATION")})
    public String getObjectLabel() {

        if (getCourse() != null) {
            return getCourse().getName();
        }
        return "";
    }

    @Override
    public PlannedCourse clone() throws CloneNotSupportedException {

        PlannedCourse model = (PlannedCourse) super.clone();
        model.setId(null);
        model.setPlannedCourseStudents(null);
        //model.setParticipationFilters(null);
        return model;
    }

    @Transient
    @PropertyViewDescriptor(
            hiddenOnList = true, hiddenOnGrid = true, hiddenOnCard = true, hiddenOnPresentation = true,
            rendererType = ViewRendererType.BUTTON,
            hiddenOnForm = true,
            clickAction = @ActionDescriptor(actionId = "enrollToCourseId", type = ActionType.CUSTOM,
                    uri = "/sessions/enrollToCourse"))
    public String getEnrollButton() {

        return "course.enroll";
    }

    @Transient
    @PropertyViewDescriptor(hidden = true)
    public List<AelUser> getSessionStudents() {

        List<AelUser> sessionStudents = new ArrayList<>();
        getPlannedCourseStudents().forEach(plannedCourseStudent -> sessionStudents.add(plannedCourseStudent.getUser()));
        return sessionStudents;
    }

    @Transient
    @PropertyViewDescriptor(
            hiddenOnList = true, hiddenOnGrid = true, hiddenOnCard = true, hiddenOnPresentation = true,
            hiddenOnForm = true,
            displayLabelInPresentation = true,
            inputType = FormInputType.SWITCH, required = false, orderInForm = 6, groupName = "advancedInfo")
    public Boolean getAdvancedOptions() {

        return advancedOptions;
    }

    public void setAdvancedOptions(Boolean advancedOptions) {

        this.advancedOptions = advancedOptions;
    }

    @Transient
    @PropertyViewDescriptor(
            order = 4, hiddenOnGrid = true, hiddenOnList = true, hiddenOnCard = true, hiddenOnPresentation = true,
            hiddenOnForm = true, groupName = "itemNameAndFavorites")
    public Integer getUsersAddedToFavorites() {
        return usersAddedToFavorites;
    }

    public void setUsersAddedToFavorites(Integer usersAddedToFavorites) {
        this.usersAddedToFavorites = usersAddedToFavorites;
    }

    @Transient
    @PropertyViewDescriptor(order = 4, hiddenOnGrid = true, hiddenOnList = true, hiddenOnCreate = true, hiddenOnUpdate = true, hiddenOnForm = true,
            groupName = "itemNameAndFavorites")
    public String getDisplayUsersAddedToFavorites() {
        return displayUsersAddedToFavorites;
    }

    public void setDisplayUsersAddedToFavorites(String displayUsersAddedToFavorites) {
        this.displayUsersAddedToFavorites = displayUsersAddedToFavorites;
    }

    @Transient
    @PropertyViewDescriptor(
            order = 1, hiddenOnList = true, hiddenOnGrid = true,
            hiddenOnForm = true, displayLabelInForm = false, displayLabelInPresentation = false,
            rendererType = ViewRendererType.ENUM, groupName = "itemType")
    public CourseType getCourseType() {
        return courseType;
    }

    public void setCourseType(CourseType courseType) {
        this.courseType = courseType;
    }

    @Transient
    @PropertyViewDescriptor(
            order = 4, hiddenOnGrid = true, hiddenOnList = true, hiddenOnCard = true,
            hiddenOnForm = true, groupName = "itemOwner")
    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    @Transient
    @PropertyViewDescriptor(
            order = 5, hiddenOnGrid = true, hiddenOnList = true, hiddenOnCard = true,
            hiddenOnForm = true, groupName = "itemOwner")
    public Integer getOwnerItemsNumber() {
        return ownerItemsNumber;
    }

    public void setOwnerItemsNumber(Integer ownerItemsNumber) {
        this.ownerItemsNumber = ownerItemsNumber;
    }

    @Transient
    @PropertyViewDescriptor(
            order = 6, hiddenOnGrid = true, hiddenOnList = true, hiddenOnCard = true,
            hiddenOnForm = true, groupName = "itemOwnerDetails")
    public String getOwnerEmail() {
        return ownerEmail;
    }

    public void setOwnerEmail(String ownerEmail) {
        this.ownerEmail = ownerEmail;
    }

    @Transient
    @PropertyViewDescriptor(
            order = 7, hiddenOnGrid = true, hiddenOnList = true, hiddenOnCard = true,
            hiddenOnForm = true, groupName = "itemOwnerDetails")
    public String getOwnerPhone() {
        return ownerPhone;
    }

    public void setOwnerPhone(String ownerPhone) {
        this.ownerPhone = ownerPhone;
    }

    @Transient
    @PropertyViewDescriptor(
            order = 8, hiddenOnGrid = true, hiddenOnList = true, hiddenOnCard = true,
            hiddenOnForm = true, groupName = "schedulePeriod")
    public String getItemSchedulePeriod() {
        return itemSchedulePeriod;
    }

    public void setItemSchedulePeriod(String itemSchedulePeriod) {
        this.itemSchedulePeriod = itemSchedulePeriod;
    }

    @Transient
    @PropertyViewDescriptor(
            order = 9, hiddenOnGrid = true, hiddenOnList = true, hiddenOnCard = true,
            hiddenOnForm = true, groupName = "scheduleTimePeriod")
    public String getItemScheduleTimePeriod() {
        return itemScheduleTimePeriod;
    }

    public void setItemScheduleTimePeriod(String itemScheduleTimePeriod) {
        this.itemScheduleTimePeriod = itemScheduleTimePeriod;
    }

    @Transient
    @PropertyViewDescriptor(
            hiddenOnList = true, hiddenOnGrid = true, hiddenOnCard = true,
            rendererType = ViewRendererType.BUTTON,
            hiddenOnForm = true,
            cssClass = "property-presentation model-plannedCourse-property model-property-value model-property-addToFavoritesBtn meta-controller property-input-wrapper btn",
            order = 3,
            groupName = "itemNameAndFavorites",
            clickAction = @ActionDescriptor(type = ActionType.CUSTOM, uri="/sessions/addItemToFavorites/{id}", availableInPresentation=true))
    public String getAddToFavoritesBtn() {
        return addToFavoritesBtn;
    }

    @Transient
    @PropertyViewDescriptor(
            hiddenOnList = true, hiddenOnGrid = true, hiddenOnCard = true,
            rendererType = ViewRendererType.BUTTON,
            hiddenOnForm = true,
            cssClass = "property-presentation model-plannedCourse-property model-property-value model-property-itemLikeBtn meta-controller property-input-wrapper btn",
            order = 10,
            groupName = "itemRatingLike",
            clickAction = @ActionDescriptor(type = ActionType.CUSTOM, uri="/sessions/likeItem/{id}", availableInPresentation=true))
    public String getItemLikeBtn() {
        return itemLikeBtn;
    }

    @Transient
    @PropertyViewDescriptor(
            hiddenOnList = true, hiddenOnGrid = true, hiddenOnCard = true,
            rendererType = ViewRendererType.BUTTON,
            hiddenOnForm = true,
            cssClass = "property-presentation model-plannedCourse-property model-property-value model-property-itemDislikeBtn meta-controller property-input-wrapper btn",
            order = 12,
            groupName = "itemRatingDislike",
            clickAction = @ActionDescriptor(type = ActionType.CUSTOM, uri="/sessions/dislikeItem/{id}", availableInPresentation=true))
    public String getItemDislikeBtn() {
        return itemDislikeBtn;
    }

    @Transient
    @PropertyViewDescriptor(
            hiddenOnList = true, hiddenOnGrid = true, hiddenOnCard = true,
            rendererType = ViewRendererType.STRING,
            hiddenOnForm = true, displayLabelInPresentation = false,
            order = 11, groupName = "itemRatingLike")
    public Integer getItemLikeNumber() {
        return itemLikeNumber;
    }

    public void setItemLikeNumber(Integer itemLikeNumber) {
        this.itemLikeNumber = itemLikeNumber;
    }

    @Transient
    @PropertyViewDescriptor(
            hiddenOnList = true, hiddenOnGrid = true, hiddenOnCard = true,
            rendererType = ViewRendererType.STRING,
            hiddenOnForm = true,  displayLabelInPresentation = false,
            order = 13, groupName = "itemRatingDislike")
    public Integer getItemDislikeNumber() {
        return itemDislikeNumber;
    }

    public void setItemDislikeNumber(Integer itemDislikeNumber) {
        this.itemDislikeNumber = itemDislikeNumber;
    }

    @Transient
    @PropertyViewDescriptor(ignore = true)
    public Long getMatchMetadataNo() {
        return matchMetadataNo;
    }

    public void setMatchMetadataNo(Long matchMetadataNo) {
        this.matchMetadataNo = matchMetadataNo;
    }

    @Transient
    @PropertyViewDescriptor(ignore = true)
    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    @Transient
    @PropertyViewDescriptor(order = 14, rendererType = ViewRendererType.BUTTON,
            hiddenOnForm = true, hiddenOnPresentation = true,
            cssClass = "property-presentation model-plannedCourse-property model-property-value model-property-readMoreLink " +
                    "generic-action action-open-presentation action-plannedCourse-open-presentation btn meta-controller " +
                    "property-input-wrapper plannedCourse-read-more-class",
            clickAction = {
                    @ActionDescriptor( type = ActionType.OPEN_PRESENTATION)
            })
    public String getReadMoreLink() {
        return readMoreLink;
    }

    public void setReadMoreLink(String readMoreLink) {
        this.readMoreLink = readMoreLink;
    }

    @Transient
    @PropertyViewDescriptor(
            hiddenOnList = true, hiddenOnGrid = true, hiddenOnCard = true,hiddenOnPresentation = true,
            displayLabelInForm = false,rendererType = ViewRendererType.STRING,
            hiddenOnForm = true,  displayLabelInPresentation = false,
            order = 14, groupName = "plannedCourseComments")
    public List<Comment> getComments(){return comments;}

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    @Transient
    @PropertyViewDescriptor(
            hiddenOnCard = true, hiddenOnForm=true, displayLabelInForm = false,
            order = 15, displayLabelInPresentation = false,
            rendererType = ViewRendererType.IMAGE, groupName = "itemOwnerPicture")
    public String getOwnerImageSrc() {
        return ownerImageSrc;
    }

    public void setOwnerImageSrc(String ownerImageSrc) {
        this.ownerImageSrc = ownerImageSrc;
    }


    @Column(name = "country")
    @PropertyViewDescriptor(hidden = true)
    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Column(name = "country_code")
    @PropertyViewDescriptor(hidden = true)
    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    @Transient
    @PropertyViewDescriptor(inputType = FormInputType.HIDDEN, hiddenOnCard = true, hiddenOnGrid = true, hiddenOnList = true, hiddenOnPresentation = true, displayLabelInForm = false)
    public Boolean getAddedToFavorite() {
        return addedToFavorite;
    }

    public void setAddedToFavorite(Boolean addedToFavorite) {
        this.addedToFavorite = addedToFavorite;
    }

    @Transient
    @PropertyViewDescriptor(inputType = FormInputType.HIDDEN, hiddenOnCard = true, hiddenOnGrid = true, hiddenOnList = true, hiddenOnPresentation = true, displayLabelInForm = false)
    public Boolean getRemoved() {
        return removed;
    }

    public void setRemoved(Boolean removed) {
        this.removed = removed;
    }

    @Transient
    @PropertyViewDescriptor(hidden = true)
    public Boolean getAttending() {
        return attending;
    }

    public void setAttending(Boolean attending) {
        this.attending = attending;
    }

    @Transient
    @PropertyViewDescriptor(
            hiddenOnList = true, hiddenOnGrid = true, hiddenOnCard = true,
            rendererType = ViewRendererType.BUTTON,
            hiddenOnForm = true,
            cssClass = "property-presentation model-plannedCourse-property model-property-value model-property-attendingBtn meta-controller property-input-wrapper btn no-preloader",
            order = 5,
            groupName = "itemNameAndFavorites",
            clickAction = @ActionDescriptor(actionId = "attendItemActionId", type = ActionType.CUSTOM, uri="/sessions/attendItem/{id}", availableInPresentation=true))
    public String getAttendingBtn() {
        return attendingBtn;
    }

    @Transient
    @PropertyViewDescriptor(
            order = 6, hiddenOnGrid = true, hiddenOnList = true, hiddenOnCard = true, hiddenOnPresentation = true, displayLabelInPresentation = false,
            hiddenOnForm = true, groupName = "itemNameAndFavorites")
    public Integer getUsersAttending() {
        return usersAttending;
    }

    public void setUsersAttending(Integer usersAttending) {
        this.usersAttending = usersAttending;
    }

    @Transient
    @PropertyViewDescriptor(order = 7, hiddenOnGrid = true, hiddenOnList = true, hiddenOnCreate = true, hiddenOnUpdate = true, hiddenOnForm = true, hiddenOnCard = true,
            displayLabelInPresentation = false, groupName = "itemNameAndFavorites")
    public String getDisplayUsersAttending() {
        return displayUsersAttending;
    }

    public void setDisplayUsersAttending(String displayUsersAttending) {
        this.displayUsersAttending = displayUsersAttending;
    }

    @Transient
    @PropertyViewDescriptor(
            hiddenOnList = true, hiddenOnGrid = true, hiddenOnCard = true,
            rendererType = ViewRendererType.BUTTON,
            hiddenOnForm = true,
            cssClass = "property-presentation model-plannedCourse-property model-property-value model-property-allAttendingBtn meta-controller property-input-wrapper btn no-preloader",
            order = 10,
            groupName = "itemNameAndFavorites")
    public String getAllAttendingBtn() {
        return allAttendingBtn;
    }

    @Transient
    @PropertyViewDescriptor(
            hiddenOnList = true, hiddenOnGrid = true, hiddenOnCard = true, order = 8,
            displayLabelInPresentation = false, hiddenOnForm = true, groupName = "itemNameAndFavorites")
    public List<AelUser> getAttendingStudentsForDisplay() {
        return attendingStudentsForDisplay;
    }

    public void setAttendingStudentsForDisplay(List<AelUser> attendingStudentsForDisplay) {
        this.attendingStudentsForDisplay = attendingStudentsForDisplay;
    }

    @Transient
    @PropertyViewDescriptor(
            hiddenOnList = true, hiddenOnGrid = true, hiddenOnCard = true, order = 9,
            displayLabelInPresentation = false, hiddenOnForm = true, groupName = "itemNameAndFavorites")
    public List<AelUser> getAllAttendingStudents() {
        return allAttendingStudents;
    }

    public void setAllAttendingStudents(List<AelUser> allAttendingStudents) {
        this.allAttendingStudents = allAttendingStudents;
    }

    @PropertyViewDescriptor(hidden = true)
    @Column(name = "review_email_sent")
    public Boolean getReviewEmailSent() {
        return reviewEmailSent;
    }

    public void setReviewEmailSent(Boolean reviewEmailSent) {
        this.reviewEmailSent = reviewEmailSent;
    }

    @PropertyViewDescriptor(hidden = true)
    @Column(name = "reviewed")
    public Boolean getReviewed() {
        return reviewed;
    }

    public void setReviewed(Boolean reviewed) {
        this.reviewed = reviewed;
    }

    @Transient
    @PropertyViewDescriptor(hidden = true)
    public Boolean getShowOwnerPhone() {
        return showOwnerPhone;
    }

    public void setShowOwnerPhone(Boolean showOwnerPhone) {
        this.showOwnerPhone = showOwnerPhone;
    }

    @Transient
    @PropertyViewDescriptor(hidden = true)
    public Boolean getShowOwnerEmail() {
        return showOwnerEmail;
    }

    public void setShowOwnerEmail(Boolean showOwnerEmail) {
        this.showOwnerEmail = showOwnerEmail;
    }

    @Transient
    @PropertyViewDescriptor(hidden = true)
    public Float getDistance() {
        return distance;
    }
    public void setDistance(Float distance) {
        this.distance = distance;
    }

    @Column(name = "lat")
    @PropertyViewDescriptor(hidden = true)
    public Float getLat() {
        return lat;
    }
    public void setLat(Float lat) {
        this.lat = lat;
    }

    @Column(name = "lon")
    @PropertyViewDescriptor(hidden = true)
    public Float getLon() {
        return lon;
    }
    public void setLon(Float lon) {
        this.lon = lon;
    }

}
