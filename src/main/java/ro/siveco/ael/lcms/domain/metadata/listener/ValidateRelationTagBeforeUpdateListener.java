package ro.siveco.ael.lcms.domain.metadata.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.metadata.model.RelationTag;
import ro.siveco.ael.lcms.domain.metadata.service.RelationTagService;
import ro.siveco.ram.event.listener.type.single.BaseBeforeUpdateListener;
import ro.siveco.ram.event.type.single.BeforeUpdateEvent;

/**
 * Created by IuliaP on 13.10.2017.
 */
@Component
@Order(value = 1)
public class ValidateRelationTagBeforeUpdateListener extends BaseBeforeUpdateListener<RelationTag> {

    @Autowired
    RelationTagService relationTagService;

    @Override
    public void onApplicationEvent(BeforeUpdateEvent<RelationTag> event) {
        relationTagService.validateRelationTag( event.getDomainModel() );
    }
}
