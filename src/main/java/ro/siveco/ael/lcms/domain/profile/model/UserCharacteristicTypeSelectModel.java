package ro.siveco.ael.lcms.domain.profile.model;


public class UserCharacteristicTypeSelectModel {
	public UserCharacteristicType code;
	public String value;
	
	public UserCharacteristicTypeSelectModel(UserCharacteristicType code, String value) {
		this.code = code;
		this.value = value;
	}
}
