package ro.siveco.ael.lcms.domain.course.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.auth.model.AelUser;
import ro.siveco.ael.lcms.domain.auth.service.AelUserService;
import ro.siveco.ael.lcms.domain.course.model.PlannedCourse;
import ro.siveco.ael.lcms.domain.course.model.PlannedCourseStatus;
import ro.siveco.ael.lcms.domain.course.service.PlannedCourseService;
import ro.siveco.ael.web.controllers.WithUserBaseEntityController;
import ro.siveco.ram.event.listener.type.single.BaseBeforeCreateListener;
import ro.siveco.ram.event.type.single.BeforeCreateEvent;

import java.util.ArrayList;

/**
 * Created by AndradaC on 8/30/2017.
 */
@Component
public class SetPlannedCourseDefaultInformationBeforeCreateListener extends BaseBeforeCreateListener<PlannedCourse>
{
    @Autowired
    private AelUserService aelUserService;

    @Autowired
    private PlannedCourseService plannedCourseService;

    @Override
    public void onApplicationEvent(BeforeCreateEvent<PlannedCourse> event) {
        PlannedCourse plannedCourse = event.getDomainModel();
        AelUser teacher = null;

        Boolean isValid = plannedCourseService.validatePlannedCourseDates(plannedCourse);

        if (isValid) {
            if (plannedCourse.getTeacher() != null && plannedCourse.getTeacher().getId() != null) {
                teacher = aelUserService.findById(plannedCourse.getTeacher().getId());
            }

            if (!plannedCourse.getSelfRegistration()) {
                if (plannedCourse.getRegistrationUser() == null) {
                    plannedCourse.setRegistrationUser(teacher);
                } else {
                    AelUser registrationUser = aelUserService.findById(plannedCourse.getRegistrationUser().getId());
                    plannedCourse.setRegistrationUser(registrationUser);
                }

                if (plannedCourse.getRegistrationApprovalUser() == null) {
                    plannedCourse.setRegistrationApprovalUser(teacher);
                } else {
                    AelUser registrationApprovalUser =
                            aelUserService.findById(plannedCourse.getRegistrationApprovalUser().getId());
                    plannedCourse.setRegistrationApprovalUser(registrationApprovalUser);
                }
            }

            plannedCourse.setCreator((AelUser) WithUserBaseEntityController.getUser());
            plannedCourse.setStatus(PlannedCourseStatus.RUNNING);
            plannedCourse.setPlannedCourseStudents(new ArrayList<>());
        }
    }
}
