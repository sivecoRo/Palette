package ro.siveco.ael.lcms.domain.metadata.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.siveco.ael.lcms.domain.metadata.model.RelationTag;
import ro.siveco.ael.lcms.repository.RelationTagRepository;
import ro.siveco.ram.app.exception.AppException;
import ro.siveco.ram.starter.service.BaseEntityService;

import java.util.List;

/**
 * Created by IuliaP on 15.06.2017.
 */
@Service
public class RelationTagService extends BaseEntityService<RelationTag> {

    @Autowired
    public RelationTagService(RelationTagRepository relationTagRepository) {
        super(RelationTag.class, relationTagRepository);
    }

    public void validateRelationTag(RelationTag relationTag) {
        if( relationTag != null ) {
            List<RelationTag> relationTags = ((RelationTagRepository)getResourceRepository())
                    .findAllBySourceTagAndAndDestinationTagAndRelation(relationTag.getSourceTag(), relationTag.getDestinationTag(), relationTag.getRelation());
            if( relationTags != null && relationTags.size() > 0 ) {
                throw new AppException("error.relationTag.duplicate");
            }
        }
    }

}
