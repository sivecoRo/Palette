package ro.siveco.ael.lcms.domain.organization.model;

public class CUJPathRetrievalDTO {
	private Long userId;
	private Boolean retrieveFuture;
	private Boolean onlyCurrentDataSet;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Boolean getRetrieveFuture() {
		return retrieveFuture;
	}

	public void setRetrieveFuture(Boolean retrieveFuture) {
		this.retrieveFuture = retrieveFuture;
	}

	public Boolean getOnlyCurrentDataSet() {
		return onlyCurrentDataSet;
	}

	public void setOnlyCurrentDataSet(Boolean onlyCurrentDataSet) {
		this.onlyCurrentDataSet = onlyCurrentDataSet;
	}
}