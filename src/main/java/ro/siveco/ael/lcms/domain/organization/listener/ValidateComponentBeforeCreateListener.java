package ro.siveco.ael.lcms.domain.organization.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import ro.siveco.ael.lcms.domain.organization.model.Component;
import ro.siveco.ael.lcms.domain.organization.service.ComponentService;
import ro.siveco.ram.app.exception.AppException;
import ro.siveco.ram.event.listener.type.single.BaseBeforeCreateListener;
import ro.siveco.ram.event.type.single.BeforeCreateEvent;

/**
 * Created by AndradaC on 9/1/2017.
 */
@Order(value = 2)
@org.springframework.stereotype.Component
public class ValidateComponentBeforeCreateListener extends BaseBeforeCreateListener<Component>{

    @Autowired
    private ComponentService componentService;

    @Override
    public void onApplicationEvent(BeforeCreateEvent<Component> event) {
        Component component = event.getDomainModel();

        if (!componentService.validateComponent(component)) {
            throw new AppException("error.component.create.duplicateEntry");
        }
    }
}
