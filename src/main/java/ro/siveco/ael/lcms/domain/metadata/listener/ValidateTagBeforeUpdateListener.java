package ro.siveco.ael.lcms.domain.metadata.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.metadata.model.Tag;
import ro.siveco.ael.lcms.domain.metadata.service.TagService;
import ro.siveco.ram.event.listener.type.single.BaseBeforeUpdateListener;
import ro.siveco.ram.event.type.single.BeforeUpdateEvent;

/**
 * Created by IuliaP on 13.10.2017.
 */
@Component
@Order(value = 1)
public class ValidateTagBeforeUpdateListener extends BaseBeforeUpdateListener<Tag> {

    @Autowired
    TagService tagService;

    @Override
    public void onApplicationEvent(BeforeUpdateEvent<Tag> event) {
        tagService.validateTag( event.getDomainModel() );
    }
}
