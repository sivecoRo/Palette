package ro.siveco.ael.lcms.domain.mail.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Service;
import ro.siveco.ael.lcms.domain.auth.model.User;
import ro.siveco.ael.lcms.domain.auth.service.UserService;
import ro.siveco.ael.lcms.domain.mail.model.MailConfiguration;
import ro.siveco.ael.lcms.domain.mail.model.MailConfigurationAttributes;
import ro.siveco.ael.lcms.domain.tenant.model.Tenant;
import ro.siveco.ael.lcms.repository.MailConfigurationRepository;
import ro.siveco.ael.web.controllers.WithUserBaseEntityController;
import ro.siveco.ram.starter.service.BaseEntityService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * Created by AndradaC on 5/29/2017.
 */

@Service
public class MailConfigurationService extends BaseEntityService<MailConfiguration>
{
    private Map<Long,JavaMailSender> javaMailSenderHash = new HashMap<Long,JavaMailSender>();
    private final UserService userService;

    @Autowired
    public MailConfigurationService(MailConfigurationRepository mailConfigurationRepository, UserService userService) {
        super(MailConfiguration.class, mailConfigurationRepository);
        this.userService = userService;
    }

    public MailConfiguration findOneByTenantId(Long tenantId)
    {
        return ((MailConfigurationRepository)getResourceRepository()).findOneByTenantId(tenantId);
    }

    public MailConfiguration saveConfiguration(MailConfiguration mailConfiguration)
    {
        save(mailConfiguration);
        JavaMailSender mailSender = getMailSender(null);
        setMailSenderProperties(mailSender, mailConfiguration);

        return mailConfiguration;
    }

    public JavaMailSender getMailSender(String sendFromEMail) {

        Long tenantId = null;
        List<MailConfiguration> mailConfigurations =
                ((MailConfigurationRepository) getResourceRepository()).getMailConfiguration();
        MailConfiguration mailConfiguration =
                (mailConfigurations != null && mailConfigurations.size() > 0) ? mailConfigurations.get(0) : null;
        JavaMailSender mailSender = null;
        if (mailConfiguration != null)
        {
            try {
                User user = WithUserBaseEntityController.getUser();
                if (user != null) {
                    Tenant tenant = user.getTenant();
                    if (tenant != null) {
                        tenantId = tenant.getId();
                        mailConfiguration = findOneByTenantId(tenantId);
                    }
                }
            } catch (Exception e) { // se apleaza si dn NotificationJob si s-ar putea sa nu existe un user logat cand ruleaza job
                if (sendFromEMail != null) {
                    User user = userService.findOneByEmail(sendFromEMail);
                    if (user != null) {
                        Tenant tenant = user.getTenant();
                        if (tenant != null) {
                            tenantId = tenant.getId();
                            mailConfiguration = findOneByTenantId(tenantId);
                        }
                    }
                }
            }
            mailSender = javaMailSenderHash.get(tenantId);

            if (mailSender == null) {
                mailSender = new JavaMailSenderImpl();
                setMailSenderProperties(mailSender, mailConfiguration);
                javaMailSenderHash.put(tenantId, mailSender);
            }
        }
        return mailSender;
    }

    public void setMailSenderProperties(JavaMailSender mailSender, MailConfiguration mailConfiguration){
        if(mailConfiguration!=null){
            if(mailConfiguration.getHost()!=null)
                ((JavaMailSenderImpl)mailSender).setHost(mailConfiguration.getHost());
            if(mailConfiguration.getPort()!=null)
                ((JavaMailSenderImpl)mailSender).setPort(mailConfiguration.getPort());
            if(mailConfiguration.getDefaultEncoding()!=null)
                ((JavaMailSenderImpl)mailSender).setDefaultEncoding(mailConfiguration.getDefaultEncoding());
            if(mailConfiguration.getPassword()!=null)
                ((JavaMailSenderImpl)mailSender).setPassword(mailConfiguration.getPassword());
            if(mailConfiguration.getUsername()!=null)
                ((JavaMailSenderImpl)mailSender).setUsername(mailConfiguration.getUsername());

            List<MailConfigurationAttributes> attrList = mailConfiguration.getMailCfgAttributesList();
            Properties properties = new Properties();
            for(MailConfigurationAttributes attributes : attrList){
                properties.setProperty(attributes.getCheie(), attributes.getValue());
            }
            if(attrList!=null && attrList.size() > 0)
                ((JavaMailSenderImpl)mailSender).setJavaMailProperties(properties);
        }
    }

}
