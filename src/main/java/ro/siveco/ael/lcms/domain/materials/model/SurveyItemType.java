package ro.siveco.ael.lcms.domain.materials.model;


import java.io.Serializable;

/**
 * Created by IntelliJ IDEA.
 * User: razvanni
 * Date: 31.05.2010
 * Time: 16:56:03
 * To change this template use File | Settings | File Templates.
 */
public enum SurveyItemType implements Serializable, MessageResolvableEnum
{
	SLIDE("slide.values"),
	LIST_OF_VALUES("list.of.values"),
	FREE_TEXT("free.text"),
	YES_NO("yes.no"),
	MASTER_DETAIL("master.detail"),
	MASTER_DETAIL_COMP("master.detail.comp"),
    NUMBER("number.values"),
	DROP_DOWN("drop");


	private String key;

	SurveyItemType( String key )
	{
		this.key = key;
	}

	public String getKey()
	{
		return key;
	}

}
