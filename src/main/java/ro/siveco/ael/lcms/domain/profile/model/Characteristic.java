package ro.siveco.ael.lcms.domain.profile.model;

import javax.persistence.*;

@Entity
@Table( name = "characteristics" )
@SequenceGenerator(name = "characteristics_seq", sequenceName = "characteristics_seq")
public class Characteristic extends ro.siveco.ram.starter.model.base.BaseEntity
{
    private CharacteristicType type;
    private String value;
    private String description;
    

    public Characteristic() { }
    
    public Characteristic(CharacteristicType type) {
    	this.type = type;
    }

	@Id
	@GeneratedValue( generator = "characteristics_seq", strategy = GenerationType.AUTO )
	public Long getId()
	{
		return super.getId();
	}

	public void setId( Long id )
	{
		super.setId(id);
	}

	@Column( name = "type", nullable = false )
	public CharacteristicType getType() {
		return type;
	}

	public void setType(CharacteristicType type) {
		this.type = type;
	}

	@Transient
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Transient
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
