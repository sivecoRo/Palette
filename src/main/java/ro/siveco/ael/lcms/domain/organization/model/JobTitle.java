package ro.siveco.ael.lcms.domain.organization.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.*;
import ro.siveco.ael.lcms.domain.tenant.model.Tenant;
import ro.siveco.ael.lcms.domain.tenant.model.TenantHolder;
import ro.siveco.ram.starter.ui.config.PropertyViewDescriptor;
import ro.siveco.ram.starter.ui.config.ViewDescriptor;
import ro.siveco.ram.starter.ui.model.FormInputType;
import ro.siveco.ram.starter.ui.model.IndexViewType;
import ro.siveco.ram.starter.ui.model.ViewRendererType;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

/**
 * Copyright: Copyright (c) 2006 Company: SIVECO Romania SA
 *
 * @author RobertR
 * @version : 1.0 / Jan 24, 2007
 */

@Entity
@Table( name = "job_title" )
@SequenceGenerator( name = "job_title_seq", sequenceName = "job_title_seq" )
//@TableComment(comment = "Stores the system's job titles")
//@Tenantable
@FilterDefs({
		@FilterDef(name="tenantJobTitleFilter", parameters=@ParamDef( name="tenantId", type="long" ) ),
		@FilterDef(name="jobTitleFilter" )
})
@Filters( {
		@Filter(name="tenantJobTitleFilter", condition="tenant_id=:tenantId"),
		@Filter(name="jobTitleFilter", condition="tenant_id is null")
})

@ViewDescriptor(paginationEnabled = true, viewTypes = {IndexViewType.LIST}, defaultViewType = IndexViewType.LIST,
        searchEnabled = true, defaultSearchProperty = "name")
public class JobTitle extends ro.siveco.ram.starter.model.base.BaseEntity implements TenantHolder
{
    //	@ColumnComment(comment = "The job title's name")
	private String name;

    //	@ColumnComment(comment = "The job title's description")
	private String description;

	private Tenant tenant;

	private Long externalId;

	private List<JobTitleCompetence> jobTitleCompetenceList = new ArrayList<>();

	public JobTitle(){}

	public JobTitle(String name, String description, Tenant tenant) {
		this.name = name;
		this.description = description;
		this.tenant = tenant;
	}

	@Id
	@GeneratedValue( generator = "job_title_seq", strategy = GenerationType.AUTO )
	public Long getId()
	{
		return super.getId();
	}

	@Column(name = "name",  nullable = false, length = 500 )
	@PropertyViewDescriptor(inputType = FormInputType.TEXT, rendererType = ViewRendererType.STRING, required=true)
	public String getName()
	{
		return name;
	}

	public void setName( String _name )
	{
		name = _name;
	}

	@Column(name = "description", unique = false, nullable = true, length = 2000 )
	@PropertyViewDescriptor(inputType = FormInputType.TEXT, rendererType = ViewRendererType.STRING, required=true, hiddenOnList = true)
	public String getDescription()
	{
		return description;
	}

	public void setDescription( String _description )
	{
		description = _description;
	}

    @Column(name = "external_id", nullable = true)
	@PropertyViewDescriptor(inputType = FormInputType.NUMBER, rendererType = ViewRendererType.INTEGER, required=false, hiddenOnForm = true, hidden = true)
    public Long getExternalId()
    {
        return externalId;
    }

    public void setExternalId( Long _exExternalId )
    {
        externalId = _exExternalId;
    }

    @ManyToOne
    @JoinColumn(name = "tenant_id", nullable = true )
    @PropertyViewDescriptor(inputType = FormInputType.SELECT, rendererType = ViewRendererType.MODEL,
            editInline = false, hidden = true, required = false, hiddenOnForm = true)
    public Tenant getTenant() {
        return tenant;
    }

    public void setTenant(Tenant tenant) {
        this.tenant = tenant;
    }

	@OneToMany(mappedBy = "jobTitle", fetch = FetchType.EAGER)
	@JsonIgnore
	@PropertyViewDescriptor( hidden = true, hiddenOnForm = true)
	public List<JobTitleCompetence> getJobTitleCompetenceList() {
		return jobTitleCompetenceList;
	}

	public void setJobTitleCompetenceList(List<JobTitleCompetence> jobTitleCompetenceList) {
		this.jobTitleCompetenceList = jobTitleCompetenceList;
	}

	@Override
	@Transient
	@PropertyViewDescriptor( hidden = true, hiddenOnForm = true)
	public String getObjectLabel() {
		return getName();
	}

	@Override
	public String toString() {
		return this.getName();
	}
}