package ro.siveco.ael.lcms.domain.profile.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import ro.siveco.ael.lcms.domain.profile.model.LanguageDTO;
import ro.siveco.ael.lcms.domain.profile.service.LanguageDTOService;
import ro.siveco.ram.controller.annotation.AjaxStructuralMapping;
import ro.siveco.ram.controller.util.MappingUri;
import ro.siveco.ram.starter.controller.BaseDummyController;

/**
 * User: LucianR
 * Date: 2017-10-25
 */

@Controller
@RequestMapping("/language")
public class LanguageDTOController extends BaseDummyController<LanguageDTO, String> {

    @Autowired
    public LanguageDTOController(LanguageDTOService service) {

        super(service);
    }

    @Override
    @AjaxStructuralMapping(path = MappingUri.INDEX_URI)
    public ResponseEntity<Page<?>> paginate(ModelMap model, Pageable pageable) {

        return super.paginate(model, pageable);
    }
}
