package ro.siveco.ael.lcms.domain.course.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import ro.siveco.ael.lcms.domain.auth.model.AelGroup;
import ro.siveco.ael.lcms.domain.auth.model.AelUser;
import ro.siveco.ael.lcms.domain.auth.model.User;
import ro.siveco.ael.lcms.domain.auth.service.AelGroupService;
import ro.siveco.ael.lcms.domain.auth.service.AelUserService;
import ro.siveco.ael.lcms.domain.course.model.Course;
import ro.siveco.ael.lcms.domain.course.model.PlannedCourse;
import ro.siveco.ael.lcms.domain.course.model.PlannedCourseStudent;
import ro.siveco.ael.lcms.domain.metadata.model.*;
import ro.siveco.ael.lcms.domain.metadata.service.CommentService;
import ro.siveco.ael.lcms.domain.metadata.service.InterestService;
import ro.siveco.ael.lcms.domain.metadata.service.MetadataService;
import ro.siveco.ael.lcms.domain.metadata.service.TagService;
import ro.siveco.ael.lcms.domain.profile.model.UserDetails;
import ro.siveco.ael.lcms.domain.profile.service.UserDetailsService;
import ro.siveco.ael.service.ImageService;
import ro.siveco.ael.service.security.annotation.Groups;
import ro.siveco.ael.service.utils.DatesService;
import ro.siveco.ael.web.controllers.WithUserBaseEntityController;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

/**
 * Created by AndradaC on 9/15/2017.
 */
@Service
public class CourseDecoratorService {

    private Integer materialListPageSize = 3;

    @Autowired
    private PlannedCourseService plannedCourseService;

    @Autowired
    private PlannedCourseStudentService plannedCourseStudentService;

    @Autowired
    private MetadataService metadataService;

    @Autowired
    private AelUserService aelUserService;

    @Autowired
    private AelGroupService aelGroupService;

    @Autowired
    private ImageService imageService;

    @Autowired
    private CourseService courseService;

    @Autowired
    private DatesService datesService;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private TagService tagService;

    @Autowired
    private InterestService interestService;

    @Autowired
    MessageSource messageSource;

    @Autowired
    CommentService commentService;

    public void populateCourse(Course course) {

        User user = WithUserBaseEntityController.getUser();
        AelUser currentUser = (AelUser)user;
        Locale userLocale = aelUserService.getLocaleForUser(currentUser);
        String language = userLocale.getLanguage();

        if (!course.isNew()) {
            populateCourseSessionsInformation(course);
            populateCourseStudentsInformation(course, user);
            populateCourseTags(course, language);
            courseService.setDefaultImage(course);
            if (course.getImage() != null && course.getImage().length > 0) {
                course.setImageSrc(imageService.imageSrcFromBytes(course.getImage()));
            }
            AelUser itemOwner = course.getOwner();
            UserDetails itemOwnerDetails = userDetailsService.getBasicData(itemOwner.getId());
            if(itemOwnerDetails.getImage() != null && itemOwnerDetails.getImage().length > 0)
            {
                course.setOwnerImageSrc(imageService.imageSrcFromBytes(itemOwnerDetails.getImage()));
            }

            course.setComments(commentService.getCourseCommentDetails(course.getId()));

            List<PlannedCourse> plannedCoursesList = plannedCourseService.findByCourseId(course.getId());
            Integer usersAttending = plannedCourseStudentService.getAttendingStudentsNumberForCourse(course);
            course.setUsersAttending(usersAttending);
            course.setDisplayUsersAttending(usersAttending == null ? null : usersAttending.toString() + " " + messageSource.getMessage(plannedCourseService.getDisplayUsersAttendingLabel(usersAttending), null, LocaleContextHolder.getLocale()));
            if (plannedCoursesList != null && plannedCoursesList.size() > 0) {
                PlannedCourse plannedCourse = plannedCoursesList.get(0);
                PlannedCourseStudent plannedCourseStudent = plannedCourseStudentService.findByPlanificationAndStundent(plannedCourse.getId(), currentUser.getId());
                course.setAttending(plannedCourseStudent == null ? null : plannedCourseStudent.getAttending());
                List<AelUser> attendingStudents = plannedCourseService.getAllAttendingStudents(plannedCourse.getPlannedCourseStudents());
                course.setAllAttendingStudents(attendingStudents);
                course.setAttendingStudentsForDisplay(plannedCourseService.getAttendingStudentsForDisplay(attendingStudents));
            }
            
        } else {
            setDefaultInformationForNewCourse(course);
        }
        populateCourseAllInterests(course);
    }

    private void populateCourseAllInterests(Course course) {
        if (course != null) {
            HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
            Principal principal = SecurityContextHolder.getContext().getAuthentication();

            List<Interest> interests = interestService.getAllI18NInterests(principal, request);
            setSelectedInterests(interests, course);
            course.setAllInterests(interests);
        }
    }

    private void setSelectedInterests(List<Interest> interests, Course course) {
        if (interests != null ) {
            interests.forEach(i -> i.setSelected(isIncludedInterest(i, course)));
        }
    }

    private Boolean isIncludedInterest(Interest interest, Course course) {
        if (course != null && interest != null && interest.getTag() != null && course.getIncludedTags() != null) {
            for(Tag tag : course.getIncludedTags()) {
                if (interest.getTag().getId().equals(tag.getId())) {
                    return true;
                }
            }
        }
        return false;
    }

    private void populateCourseSessionsInformation(Course course) {
        AelUser currentUser = (AelUser) WithUserBaseEntityController.getUser();
        List<PlannedCourse> plannedCoursesList = plannedCourseService.findByCourseId(course.getId());
      //  course.setPlannedCourses(plannedCoursesList);
        course.setPlannedCoursesNumber(plannedCourseService.getPlannedCourseNumberForCourse(course));
        if (plannedCoursesList != null && plannedCoursesList.size() > 0) {
            //course.getPlannedCourses().forEach(plannedCourseService::decorateCourseSession);
            PlannedCourse firstPlannedCourse = plannedCoursesList.get(0);
            course.setPlannedCourseStartDate(datesService.formatDate(firstPlannedCourse.getStartDate(), datesService.getCurrentUserTimezoneId()));
            course.setPlannedCourseEndDate(datesService.formatDate(firstPlannedCourse.getEndDate(), datesService.getCurrentUserTimezoneId()));
            if (firstPlannedCourse.getStartDate() != null) {
                course.setDisplayPlannedCourseStartDate(datesService.formatLongDate(firstPlannedCourse.getStartDate()));
            }
            if (firstPlannedCourse.getEndDate() != null) {
                course.setDisplayPlannedCourseEndDate(datesService.formatLongDate(firstPlannedCourse.getEndDate()));
            }
            course.setPlannedCourseStartTime(firstPlannedCourse.getStartTime());
            course.setPlannedCourseEndTime(firstPlannedCourse.getEndTime());
            course.setMinUserNumber(firstPlannedCourse.getMinNrOfStudents());
            course.setMaxUserNumber(firstPlannedCourse.getMaxNrOfStudents());
            if(firstPlannedCourse.getLocation() == null)
            {
                course.setItemLocation("-");
            }
            else {
                course.setItemLocation(firstPlannedCourse.getLocation());
            }
            course.setItemLocationCoordinates(firstPlannedCourse.getLocationCoordinates());
            AelUser owner =course.getOwner();
            course.setOwnerName(owner.getDisplayName());
            course.setOwnerEmail(owner.getEmail());
            if(owner.getPhoneNo() == null || "".equals(owner.getPhoneNo())) {
                course.setOwnerPhone("-");
            }
            else {
                course.setOwnerPhone(owner.getPhoneNo());
            }
            Optional<UserDetails> optionalUserDetails = userDetailsService.getByAelUser(currentUser);
            if (optionalUserDetails.isPresent()) {
                UserDetails userDetails = optionalUserDetails.get();
                course.setShowOwnerEmail(userDetails.getShow_email());
                course.setShowOwnerPhone(userDetails.getShow_phone());
            }
            course.setOwnerItemsNumber(courseService.getCourseNumberForOwner(owner));

            String startDateFormat =  firstPlannedCourse.getStartDate() == null ? "" : datesService.formatLongDate(firstPlannedCourse.getStartDate());
            String endDateFormat = firstPlannedCourse.getEndDate() == null ? "" : datesService.formatLongDate(firstPlannedCourse.getEndDate());
            course.setItemSchedulePeriod(startDateFormat + "-" + endDateFormat);

            String startTimeFormat = firstPlannedCourse.getStartTime() == null ? "" : datesService.formatShortTime(firstPlannedCourse.getStartTime());
            String endTimeFormat = firstPlannedCourse.getEndTime() == null ? "" : datesService.formatShortTime(firstPlannedCourse.getEndTime());
            course.setItemScheduleTimePeriod(startTimeFormat + "-" + endTimeFormat);

            Integer noLikes = plannedCourseStudentService.getItemLikesNumber(course);
            Integer noDislikes = plannedCourseStudentService.getItemDislikesNumber(course);
            course.setItemLikeNumber(noLikes);
            course.setItemDislikeNumber(noDislikes);
        }
    }

    private void populateCourseStudentsInformation(Course course, User user) {

        Long tenantId = user.getTenant() != null ? user.getTenant().getId() : null;
        AelGroup studentsGroup = aelGroupService.getByName(Groups.Default.Students.get());
        Locale locale = aelUserService.getLocaleForUser((AelUser) user);
        Integer studentsNumber = plannedCourseStudentService.getFavoritesStudentsNumberForCourse(course);
        course.setStudentsNumber(studentsNumber);
        course.setDisplayStudentsNumber(studentsNumber == null ? null : studentsNumber.toString() + " " + messageSource.getMessage("displayStudentsNumber.label", null, locale));
    }

    private void populateCourseTags(Course course, String language) {

        course.setIncludedTags(getTags(metadataService
                .findByEntityIdAndEntityTypeAndRelationType(course.getId(), EntityType.COURSE,
                        RelationType.INCLUDED), language));
        course.setExcludedTags(getTags(metadataService
                .findByEntityIdAndEntityTypeAndRelationType(course.getId(), EntityType.COURSE, RelationType.EXCLUDED), language));
        course.setOptionalTags(getTags(metadataService
                .findByEntityIdAndEntityTypeAndRelationType(course.getId(), EntityType.COURSE, RelationType.OPTIONAL), language));
    }

    public List<Tag> getTags(List<Metadata> metadataList, String language) {

        List<Tag> tags = new ArrayList<>();
        for (Metadata metadata : metadataList) {
            Tag tag = metadata.getTag();
            tag.setDisplayName(tagService.getI18NTagName(tag, language));
            tags.add(tag);
        }
        return tags;
    }

    private void setDefaultInformationForNewCourse(Course course) {

        course.setPlannedCourses(new ArrayList<>());
        course.setPlannedCoursesNumber(0);
        course.setStudentsList(new ArrayList<>());
        course.setStudentsNumber(0);
        courseService.setDefaultImage(course);
    }
}
