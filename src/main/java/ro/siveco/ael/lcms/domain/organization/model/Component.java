package ro.siveco.ael.lcms.domain.organization.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.*;
import ro.siveco.ael.lcms.domain.taxonomies.model.Taxonomy;
import ro.siveco.ael.lcms.domain.tenant.model.Tenant;
import ro.siveco.ael.lcms.domain.tenant.model.TenantHolder;
import ro.siveco.ram.starter.model.base.BaseEntity;
import ro.siveco.ram.starter.ui.config.PropertyViewDescriptor;
import ro.siveco.ram.starter.ui.config.ViewDescriptor;
import ro.siveco.ram.starter.ui.model.FormInputType;
import ro.siveco.ram.starter.ui.model.IndexViewType;
import ro.siveco.ram.starter.ui.model.ViewRendererType;

import javax.persistence.CascadeType;
import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table( name = "component" )
//        uniqueConstraints = {@UniqueConstraint(columnNames = {"related_entity_id", "component_type_id"})})
@SequenceGenerator(name = "component_seq", sequenceName = "component_seq")
@FilterDefs({
        @FilterDef(name = "tenantComponentFilter", parameters = @ParamDef(name = "tenantId", type = "long")),
        @FilterDef(name = "componentFilter")
})
@Filters({
        @Filter(name = "tenantComponentFilter", condition = "tenant_id=:tenantId "), // or component_type_id=1
        @Filter(name = "componentFilter", condition = "tenant_id is null")
})
@ViewDescriptor(paginationEnabled = true, viewTypes = {IndexViewType.LIST}, defaultViewType = IndexViewType.LIST,
        searchEnabled = true, defaultSearchProperty = "name")
public class Component extends BaseEntity implements TenantHolder, Taxonomy<Component> {
    private Long id;

    private String name;

//    private ComponentType type;

    private Component parent;

    private String relatedEntityId;

    private List<Component> children = new ArrayList<Component>();
    private List<ComponentUserJob> userJobs = new ArrayList<ComponentUserJob>();

    private String completeName;

    /* Used in ael-ro */
    //@ColumnComment(comment = "This is the identifier from ASM")
    private Long externalId;

    private Tenant tenant;

    @Id
    @PropertyViewDescriptor(hidden = true, required = false, hiddenOnForm = true)
    @GeneratedValue(generator = "component_seq", strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(Long _id) {
        id = _id;
    }

    @Column(nullable = false, length = 500)
    @PropertyViewDescriptor(inputType = FormInputType.TEXT, rendererType = ViewRendererType.STRING, required = true)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "external_id", nullable = true)
    @PropertyViewDescriptor(hidden = true, required = false, hiddenOnForm = true)
    public Long getExternalId() {
        return externalId;
    }

    public void setExternalId(Long externalId) {
        this.externalId = externalId;
    }


    @Override
    @ManyToOne
    @JoinColumn(name = "parent_id", nullable = true)
    @PropertyViewDescriptor(hidden = true)
    public Component getParent() {
        return parent;
    }

    public void setParent(Component parent) {
        this.parent = parent;
    }

    @OneToMany(mappedBy = "parent", cascade = {CascadeType.ALL})
    @JsonIgnore
    @PropertyViewDescriptor(hidden = true)
    public List<Component> getChildren() {
        return children;
    }

    public void setChildren(List<Component> children) {
        this.children = children;
    }


    @OneToMany(mappedBy = "component")
    @JsonIgnore
    @PropertyViewDescriptor(hidden = true, required = false, hiddenOnForm = true)
    @OrderBy("id")
    public List<ComponentUserJob> getUserJobs() {
        return userJobs;
    }

    public void setUserJobs(List<ComponentUserJob> _userJobs) {
        userJobs = _userJobs;
    }

    @Column(nullable = false, length = 100, name = "related_entity_id")
    @PropertyViewDescriptor(hidden = true, required = false, hiddenOnForm = true)
    public String getRelatedEntityId() {
        return relatedEntityId;
    }

    public void setRelatedEntityId(String relatedEntityId) {
        this.relatedEntityId = relatedEntityId;
    }

    @PropertyViewDescriptor(hidden = true, required = false, hiddenOnForm = true)
    @Column(name = "complete_name", nullable = false, length = 2000)
    public String getCompleteName() {
        return completeName;
    }

    public void setCompleteName(String completeName) {
        this.completeName = completeName;
    }

    @ManyToOne
    @JoinColumn(name = "tenant_id", nullable = true)
    @PropertyViewDescriptor(inputType = FormInputType.SELECT, rendererType = ViewRendererType.MODEL,
            editInline = false, hidden = true, required = false, hiddenOnForm = true)
    public Tenant getTenant() {
        return tenant;
    }

    public void setTenant(Tenant tenant) {
        this.tenant = tenant;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final Component component = (Component) o;

        if (!id.equals(component.id)) {
            return false;
        }

        return true;
    }

    @Override
    @Transient
    public String getObjectLabel() {
        return getName();
    }

    @Override
    public String toString() {
        return this.getName();
    }
}
