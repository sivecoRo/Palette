package ro.siveco.ael.lcms.domain.auth.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import ro.siveco.ael.lcms.domain.auth.model.AelUser;
import ro.siveco.ael.lcms.domain.auth.model.ProfileVerification;

import ro.siveco.ael.lcms.repository.ProfileVerificationRepository;
import ro.siveco.ael.service.utils.GuidUtils;
import ro.siveco.ram.starter.service.BaseEntityService;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * <p>Service for verifying profiles/accounts created by users. For this feature to work,
 * the <code>features.profile-verification-enabled</code> must be set to <code>true</code>.</p>
 *
 * <p>During profile creation, the user ({@link AelUser}) has no <code>verifiedDate</code> set.
 * At the same time, an email with a verification link is sent to user's email using {@link ProfileVerificationEmailService}.
 * When the user clicks on the verification link in the email, the profile ({@link AelUser} is activated
 * by setting the verifiedDate to the current date.</p>
 *
 * @author maciejb
 */
@Service
public class ProfileVerificationService extends BaseEntityService<ProfileVerification> {


    private AelUserService aelUserService;
    private ProfileVerificationEmailService profileVerificationEmailService;


    @Autowired
    public ProfileVerificationService(ProfileVerificationRepository profileVerificationRepository,
                                      ProfileVerificationEmailService profileVerificationEmailService,
                                      AelUserService aelUserService) {
        super(ProfileVerification.class, profileVerificationRepository);

        Assert.notNull(profileVerificationEmailService, "Supplied ProfileVerificationEmailService is null.");
        Assert.notNull(profileVerificationEmailService, "Supplied AelUserService is null.");

        this.profileVerificationEmailService = profileVerificationEmailService;
        this.aelUserService = aelUserService;


    }


    /**
     * Create a profile verification entry for specified user, disable the user
     * by setting the verifiedDate as null and send an email to the user with the profile verification link.
     *
     * @param user user profile to be verified
     */
    @Transactional
    public void createProfileVerification(AelUser user){

        Assert.notNull(user, "Can't create a profile verification for null user.");

        String token = saveProfileVerification(user);
        setUserAsUnverified(user);
        sendProfileVerificationEmail(user, token);

    }


    /**
     * Delete profile verification belonging to specific user
     * @param user user
     * @return id of removed profile verification
     */
    @Transactional
    public Long deleteProfileVerification(AelUser user){
        Assert.notNull(user, "Can't delete profile verification for null user");
        return getRepository().deleteByUser(user);
    }



    /**
     * Verify user profile. If the token is valid, the user profile gets verified by setting the verifiedDate
     * on {@link AelUser}.
     *
     * @param token verification token
     */
    @Transactional
    public ProfileVerificationResult verifyProfile(String token){

        Optional<ProfileVerification> profileVerificationOptional = getProfileVerificationByToken(token);

        if (profileVerificationOptional.isPresent()) {
            setUserAsVerified(profileVerificationOptional.get().getUser());
            return ProfileVerificationResult.SUCCESS;
        } else {
            return ProfileVerificationResult.wrongToken(getMessage(ProfileVerificationResult.WRONG_TOKEN_ERR));
        }

//      getRepository().delete(profileVerification.getId());
    }


    /**
     * <p>Resend the verification email. The verification email can be sen† again if for some reason the user didn't
     * receive it upon profile creation or missed it etc.</p>
     *
     * <p>In order for the email to be re-sent, the provided email address must be valid, belonging to an existing user
     * and the user must be currently unverified.</p>
     *
     * @param email email address to which the verification email will be sent
     * @return operation result
     */
    @Transactional
    public ProfileVerificationResult resendVerificationEmail(String email){

        Assert.notNull(email, "Can't resend profile verification for null email.");

        AelUser user = aelUserService.getAelUserByUsername(email);

        if (isUserNotFound(user)){
            return ProfileVerificationResult.emailNotFound(
                            getMessage(ProfileVerificationResult.EMAIL_NOT_FOUND_ERR,
                            email));
        } else if (isUserAlreadyVerified(user)){
            return ProfileVerificationResult.alreadyVerified(
                            getMessage(ProfileVerificationResult.ALREADY_VERIFIED_ERR));
        }


        ProfileVerification profileVerification = getRepository().findByUser(user);

        if (profileVerification == null){
            //This shouldn't really happen
            createProfileVerification(user);
        } else {
            sendProfileVerificationEmail(user, profileVerification.getToken());
        }

        return ProfileVerificationResult.SUCCESS;
    }

    /**
     * Send profile verification email to specified user, using supplied token.
     * @param user user to send the email to
     * @param token profile validation token
     */
    private void sendProfileVerificationEmail(AelUser user, String token) {
        try {
            profileVerificationEmailService.sendProfileVerificationEmail(user, token);
        } catch (Throwable e) {
            getLogger().log(Level.SEVERE, "Error sending email: " + e.getMessage(), e);
        }
    }

    /**
     * Check if user is "not found"
     * @param user user to be checked
     * @return <code>true</code> if user is <code>null</code>, <code>false</code> otherwise
     */
    private boolean isUserNotFound(AelUser user) {
        return user == null;
    }

    /**
     * Check if user is already verified.
     * @param user user to be checked
     * @return <code>true</code> if user's <code>getVerifiedDate()</code> is not <code>null</code>,
     * <code>false</code> otherwise
     */
    private boolean isUserAlreadyVerified(AelUser user) {
        return user.getVerifiedDate() != null;
    }


    /**
     * Save {@link ProfileVerification} for specified user
     * @param user user
     * @return profile verification token
     */
    private String saveProfileVerification(AelUser user) {
        ProfileVerification profileVerification = new ProfileVerification();
        profileVerification.setUser(user);

        String token = getToken();
        profileVerification.setToken(token);

        getRepository().save(profileVerification);
        return token;
    }


    /**
     * Mark user as verified by setting their <code>verfiedDate</code> to current date.
     * @param user user
     */
    private void setUserAsVerified(AelUser user) {
        user.setVerifiedDate(new Date());
        saveUser(user);
    }

    /**
     * Mark user as unverified by setting their <code>verfiedDate</code> to <code>null</code>
     * @param user user
     */
    private void setUserAsUnverified(AelUser user) {
        user.setVerifiedDate(null);
        saveUser(user);
    }

    /**
     * Save user in the repository
     * @param user user to be saved
     */
    private void saveUser(AelUser user) {
        aelUserService.saveOrUpdate(user);
    }


    /**
     * Get the {@link ProfileVerification} for specified token
     * @param token verification token
     * @return <code>Optional</code> of {@link ProfileVerification}
     */
    private Optional<ProfileVerification> getProfileVerificationByToken(String token){

        ProfileVerification profileVerification = getRepository().findByToken(token);

        return Optional.ofNullable(profileVerification);
    }

    /**
     * Get profile verification token - use a GUID as the token value.
     * @return verification token
     */
    private String getToken() {
        return GuidUtils.generateGuid();
    }


    /**
     * Get the {@link ProfileVerificationRepository}
     * @return the repository
     */
    private ProfileVerificationRepository getRepository(){
        return (ProfileVerificationRepository) getResourceRepository();
    }


}
