package ro.siveco.ael.lcms.domain.profile.model;

import java.util.List;

public class SaveCharacteristicsModel {
	private List<UserCharacteristicModel> list;
	private String type;
	private Long entityId;

	public List<UserCharacteristicModel> getList() {
		return list;
	}

	public void setList(List<UserCharacteristicModel> list) {
		this.list = list;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Long getEntityId() {
		return entityId;
	}

	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}
}
