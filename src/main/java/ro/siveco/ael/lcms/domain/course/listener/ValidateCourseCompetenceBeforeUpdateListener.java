package ro.siveco.ael.lcms.domain.course.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.course.model.CourseCompetence;
import ro.siveco.ael.lcms.domain.course.service.CourseCompetenceService;
import ro.siveco.ram.app.exception.AppException;
import ro.siveco.ram.event.listener.type.single.BaseBeforeUpdateListener;
import ro.siveco.ram.event.type.single.BeforeUpdateEvent;

/**
 * Created by AndradaC on 9/3/2017.
 */
@Component
public class ValidateCourseCompetenceBeforeUpdateListener extends BaseBeforeUpdateListener<CourseCompetence>{

    @Autowired
    private CourseCompetenceService courseCompetenceService;

    @Override
    public void onApplicationEvent(BeforeUpdateEvent<CourseCompetence> event) {
        CourseCompetence courseCompetence = event.getDomainModel();
        if(courseCompetenceService.previousConnexionExists(courseCompetence)){
            throw new AppException("error.courseCompetence.update.duplicateEntry");
        }
    }
}
