package ro.siveco.ael.lcms.domain.metadata.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.metadata.model.Interest;
import ro.siveco.ael.lcms.domain.metadata.service.InterestService;
import ro.siveco.ram.event.listener.type.single.BaseBeforeUpdateListener;
import ro.siveco.ram.event.type.single.BeforeUpdateEvent;

/**
 * Created by albertb on 2/2/2018.
 */
@Component
@Order(value = 1)
public class ValidateInterestBeforeUpdateListener extends BaseBeforeUpdateListener<Interest> {

    @Autowired
    InterestService interestService;

    @Override
    public void onApplicationEvent(BeforeUpdateEvent<Interest> event) {
        interestService.validateInterest( event.getDomainModel() );
    }
}
