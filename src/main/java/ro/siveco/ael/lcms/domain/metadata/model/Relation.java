package ro.siveco.ael.lcms.domain.metadata.model;

import ro.siveco.ram.starter.ui.config.PropertyViewDescriptor;
import ro.siveco.ram.starter.ui.config.ViewDescriptor;
import ro.siveco.ram.starter.ui.model.IndexViewType;

import javax.persistence.*;

/**
 * Created by IuliaP on 15.06.2017.
 */
@Entity
@Table(name = "relations")
@SequenceGenerator(name = "relations_seq", sequenceName = "relations_seq")
@ViewDescriptor(pageSize = 10, viewTypes = {IndexViewType.GRID}, searchEnabled = true, defaultSearchProperty = "name")
public class Relation extends ro.siveco.ram.starter.model.base.BaseEntity {

    private String name;

    @Id
    @GeneratedValue(generator = "relations_seq", strategy = GenerationType.AUTO)
    public Long getId() {
        return super.getId();
    }

    @Column(name = "name", nullable = false)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    @PropertyViewDescriptor(hidden = true)
    @Transient
    public String getObjectLabel() {
        return getName();
    }
}
