package ro.siveco.ael.lcms.domain.auth.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.siveco.ael.lcms.domain.auth.model.AelFunction;
import ro.siveco.ael.lcms.repository.AelFunctionRepository;
import ro.siveco.ram.starter.service.BaseEntityService;

import java.util.List;

@Service
public class AelFunctionService extends BaseEntityService<AelFunction> {

    @Autowired
    public AelFunctionService(AelFunctionRepository aelFunctionRepository) {
        super(AelFunction.class, aelFunctionRepository);
    }

    public List<AelFunction> findByIdFunctions(List<Long> idFunctions) {
        return ((AelFunctionRepository) getResourceRepository()).findByIdFunctions(idFunctions);
    }

    public List<AelFunction> findAll() {
        return ((AelFunctionRepository) getResourceRepository()).findAll();
    }

    public AelFunction findById(Long idFunction) {
        return ((AelFunctionRepository) getResourceRepository()).findById(idFunction);
    }

    public AelFunction findByName(String name) {

        return ((AelFunctionRepository) getResourceRepository()).findByName(name);
    }

    public Boolean validateAelFunction(AelFunction aelFunction) {
        Boolean isNewAelFunction = aelFunction.getId() == null;
        AelFunction aelFunctionWithSameName = findByName(aelFunction.getName());
        if (aelFunctionWithSameName != null) {
            if (isNewAelFunction || aelFunction.getId().compareTo(aelFunctionWithSameName.getId()) != 0) {
                return Boolean.FALSE;
            }
        }
        return Boolean.TRUE;
    }

    public List<AelFunction> getAelFunctions() {

        return findAll();
    }
}
