package ro.siveco.ael.lcms.domain.profile.model;

import javax.persistence.*;

/**
 * Created by CatalinS on 16.06.2017.
 */
@Entity
@Table(name = "user_characteristics")
@SequenceGenerator(name = "user_characteristics_seq", sequenceName = "user_characteristics_seq")
public class UserCharacteristic extends ro.siveco.ram.starter.model.base.BaseEntity {

    private Characteristic category;
    private Characteristic characteristic;
    private Characteristic specific;
    private UserCharacteristicType type;
    private UserCharacteristicTab tab;
    private Boolean active = Boolean.TRUE;

    @Id
    @Column
    @GeneratedValue(generator = "user_characteristics_seq", strategy = GenerationType.AUTO)
    public Long getId()
    {
		return super.getId();
    }

    public void setId( Long id )
    {
    	super.setId(id);
    }

    @ManyToOne
    @JoinColumn(name = "category_id", nullable = false)
	public Characteristic getCategory() {
		return category;
	}

	public void setCategory(Characteristic category) {
		this.category = category;
	}

	@ManyToOne
    @JoinColumn(name = "characteristic_id")
	public Characteristic getCharacteristic() {
		return characteristic;
	}

	public void setCharacteristic(Characteristic characteristic) {
		this.characteristic = characteristic;
	}

	@ManyToOne
    @JoinColumn(name = "specific_id")
	public Characteristic getSpecific() {
		return specific;
	}

	public void setSpecific(Characteristic specific) {
		this.specific = specific;
	}

	@Column( name = "type", nullable = false )
	public UserCharacteristicType getType() {
		return type;
	}

	public void setType(UserCharacteristicType type) {
		this.type = type;
	}
	
	@ManyToOne
	@JoinColumn( name = "tab", nullable = false )
	public UserCharacteristicTab getTab() {
		return tab;
	}

	public void setTab(UserCharacteristicTab tab) {
		this.tab = tab;
	}

	@Column(name = "active", nullable = false )
	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

}
