package ro.siveco.ael.lcms.domain.notification.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import ro.siveco.ael.domain.annotations.ColumnComment;
import ro.siveco.ael.domain.annotations.TableComment;
import ro.siveco.ael.lcms.domain.auth.model.AelUser;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table( name = "user_messages" )
@TableComment(comment = "Stores sytem notifications and user messages")
public class UserMessage extends ro.siveco.ram.starter.model.base.BaseEntity
{
	private Long id;

	@ColumnComment(comment = "The sender's user id")
	private AelUser from;

	@ColumnComment(comment = "The receiver's user id")
	private AelUser to;

	@ColumnComment(comment = "The plain text list with all receivers of the message")
	private String toList;

	@ColumnComment(comment = "The message subject")
	private String subject;

	@ColumnComment(comment = "The message content")
	private String messageBody;

	@ColumnComment(comment = "Sent date")
	private Date sentDate;

	@ColumnComment(comment = "The date when the message arrived in the receiver's inbox")
	private Date receivedDate;

	@ColumnComment(comment = "Whether the message was sent or the composer saved it as a draft")
	private Boolean sent;

	@ColumnComment(comment = "The date when the receiver opened the message")
	private Date readDate;

	@ColumnComment(comment = "The message type")
	private UserMessageType userMessageType;

	@ColumnComment(comment = "XML encoded JAVA objects needed to internationalize system notifications")
	private String subjectArgs;

	@ColumnComment(comment = "XML encoded JAVA objects needed to internationalize system notifications")
	private String messageBodyArgs;

	@ColumnComment(comment = "The id of the object this notification refers to")
	private Long entityId;

	@ColumnComment(comment = "The id of the sender's saved copy of this message")
	private UserMessage source;

	@ColumnComment(comment = "Whether this messages is a saved copy of the sender's message")
	private Boolean ownerCopy = false;

	@ColumnComment(comment = "Course creator id + _ + course title. It is used to delete messages associated with a course")
	private String creatorIdAndTitle;

	@ColumnComment(comment = "Priority of the notification: 0 for all, 1 for quota exceed")
	@Column(name = "priority", nullable = false, columnDefinition = "default 0")
	private int priority;

    private List<UserMessageModel> inboxMessages = new ArrayList<>();

    private List<UserMessageModel> sentMessages = new ArrayList<>();

    private List<UserMessageModel> draftMessages = new ArrayList<>();

    private int unreadMessagesNr;

	public UserMessage()
	{

	}

	public UserMessage( UserMessage userMessage )
	{
		this.id = userMessage.getId();
		this.from = userMessage.getFrom();
		this.to = userMessage.getTo();
		this.subject = userMessage.getSubject();
		this.messageBody = userMessage.getMessageBody();
		this.sentDate = userMessage.getSentDate();
		this.receivedDate = userMessage.getReceivedDate();
		this.sent = userMessage.getSent();
		this.readDate = userMessage.getReadDate();
		this.userMessageType = userMessage.getUserMessageType();
		this.subjectArgs = userMessage.getSubjectArgs();
		this.messageBodyArgs = userMessage.getMessageBodyArgs();
		this.entityId = userMessage.getEntityId();
		this.source = userMessage.getSource();
		this.creatorIdAndTitle = userMessage.getCreatorIdAndTitle();
    }

	@Id
	@GeneratedValue( strategy = GenerationType.AUTO )
	public Long getId()
	{
		return id;
	}

	public void setId( Long id )
	{
		this.id = id;
	}


	@ManyToOne( cascade = CascadeType.MERGE )
	@JoinColumn( name = "from_user_id" )
	public AelUser getFrom()
	{
		return from;
	}

	public void setFrom( AelUser from )
	{
		this.from = from;
	}


	@ManyToOne( cascade = CascadeType.MERGE )
	@JoinColumn( name = "to_user_id" )
	public AelUser getTo()
	{
		return to;
	}

	public void setTo( AelUser to )
	{
		this.to = to;
	}


    @Column( name = "to_list", length = 500 )
    public String getToList()
    {
        return toList;
    }

    public void setToList(String toList)
    {
        this.toList = toList;
    }

    @Column( name = "subject", length = 500 )
	public String getSubject()
	{
		return subject;
	}

	public void setSubject( String subject )
	{
		this.subject = subject;
	}


	@Column( name = "message_body", length = 4000 )
	public String getMessageBody()
	{
		return messageBody;
	}

	public void setMessageBody( String messageBody )
	{
		this.messageBody = messageBody;
	}


	@Column( name = "sent_date" )
	public Date getSentDate()
	{
		return sentDate;
	}

	public void setSentDate( Date sentDate )
	{
		this.sentDate = sentDate;
	}


	@Column( name = "received_date" )
	public Date getReceivedDate()
	{
		return receivedDate;
	}

	public void setReceivedDate( Date receivedDate )
	{
		this.receivedDate = receivedDate;
	}


	@Column( name = "sent", nullable = false )
	public Boolean getSent()
	{
		return sent;
	}

	public void setSent( Boolean sent )
	{
		this.sent = sent;
	}


	@Column( name = "read_date" )
	public Date getReadDate()
	{
		return readDate;
	}

	public void setReadDate( Date readDate )
	{
		this.readDate = readDate;
	}


	@Column( name = "type", nullable = false )
	public UserMessageType getUserMessageType()
	{
		return userMessageType;
	}

	public void setUserMessageType( UserMessageType userMessageType )
	{
		this.userMessageType = userMessageType;
	}

	@Column( name = "message_body_args", length = 4000 )
	public String getMessageBodyArgs()
	{
		return messageBodyArgs;
	}

	public void setMessageBodyArgs( String messageBodyArgs )
	{
		this.messageBodyArgs = messageBodyArgs;
	}

	@Column( name = "subject_args", length = 500 )
	public String getSubjectArgs()
	{
		return subjectArgs;
	}

	public void setSubjectArgs( String subjectArgs )
	{
		this.subjectArgs = subjectArgs;
	}

	@Column( name = "entity_id" )
	public Long getEntityId()
	{
		return entityId;
	}

	public void setEntityId( Long entityId )
	{
		this.entityId = entityId;
	}

	@ManyToOne( cascade = {CascadeType.PERSIST, CascadeType.MERGE} )
    @JoinColumn( name = "source_id", nullable = true )
    public UserMessage getSource()
    {
        return source;
    }

    public void setSource(UserMessage source)
    {
        this.source = source;
    }


    @Column( name = "is_owner_copy", nullable = false )
    public Boolean getOwnerCopy()
    {
        return ownerCopy;
    }

    public void setOwnerCopy(Boolean ownerCopy)
    {
        this.ownerCopy = ownerCopy;
    }

	@Column( name = "creator_id_and_title", nullable = true )
	public String getCreatorIdAndTitle()
	{
		return creatorIdAndTitle;
	}

	public void setCreatorIdAndTitle( String creatorIdAndTitle )
	{
		this.creatorIdAndTitle = creatorIdAndTitle;
	}

    @Transient
    public List<UserMessageModel> getInboxMessages() {

        return inboxMessages;
    }

    public void setInboxMessages(
            List<UserMessageModel> inboxMessages) {

        this.inboxMessages = inboxMessages;
    }

    @Transient
    public List<UserMessageModel> getSentMessages() {

        return sentMessages;
    }

    public void setSentMessages(
            List<UserMessageModel> sentMessages) {

        this.sentMessages = sentMessages;
    }

    @Transient
    public List<UserMessageModel> getDraftMessages() {

        return draftMessages;
    }

    public void setDraftMessages(
            List<UserMessageModel> draftMessages) {

        this.draftMessages = draftMessages;
    }

    @Transient
    public int getUnreadMessagesNr() {

        return unreadMessagesNr;
    }

    public void setUnreadMessagesNr(int unreadMessagesNr) {

        this.unreadMessagesNr = unreadMessagesNr;
    }

	public String toString()
	{
		return new ToStringBuilder( this,
									ToStringStyle.MULTI_LINE_STYLE )
				.append( this.getId() )
				.append( this.getFrom() )
				.append( this.getTo() )
				.append( this.getSubject() )
				.append( this.getMessageBody() )
				.append( this.getSentDate() )
				.append( this.getReceivedDate() )
				.append( this.getUserMessageType() )
				.append( this.getSent() )
				.append( this.getReadDate() )
				.append( this.getMessageBodyArgs() )
				.append( this.getSubjectArgs() )
                .append( this.getSource())
                .append( this.getCreatorIdAndTitle())
                .toString();
	}

	public boolean equals( Object other )
	{
		if( ( this == other ) )
		{
			return true;
		}
		if( !( other instanceof UserMessage ) )
		{
			return false;
		}
		UserMessage castOther = ( UserMessage ) other;
		return new EqualsBuilder()
				.append( this.getId(), castOther.getId() )
				.append( this.getFrom(), castOther.getFrom() )
				.append( this.getTo(), castOther.getTo() )
				.append( this.getSubject(), castOther.getSubject() )
				.append( this.getMessageBody(), castOther.getMessageBody() )
				.append( this.getSentDate(), castOther.getSentDate() )
				.append( this.getReceivedDate(), castOther.getReceivedDate() )
				.append( this.getUserMessageType(), castOther.getUserMessageType() )
				.append( this.getSent(), castOther.getSent() )
				.append( this.getReadDate(), castOther.getReadDate() )
				.append( this.getMessageBodyArgs(), castOther.getMessageBodyArgs() )
				.append( this.getSubjectArgs(), castOther.getSubjectArgs() )
                .append( this.getSource(), castOther.getSource())
				.append( this.getCreatorIdAndTitle(), castOther.getCreatorIdAndTitle())
                .isEquals();
	}

	public int hashCode()
	{
		return new HashCodeBuilder()
				.append( getId() )
				.append( getFrom() )
				.append( getTo() )
				.append( getSubject() )
				.append( getMessageBody() )
				.append( getSentDate() )
				.append( getReceivedDate() )
				.append( getUserMessageType() )
				.append( getSent() )
				.append( getReadDate() )
				.append( getMessageBodyArgs() )
				.append( getSubjectArgs() )
                .append( getSource())
                .append( getCreatorIdAndTitle())
                .toHashCode();
	}
}

