package ro.siveco.ael.lcms.domain.notification.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.notification.model.PlannedNotification;
import ro.siveco.ael.lcms.domain.notification.service.PlannedNotificationService;
import ro.siveco.ram.event.listener.type.single.BaseBeforeUpdateListener;
import ro.siveco.ram.event.type.single.BeforeUpdateEvent;

/**
 * Created by IuliaP on 29.09.2017.
 */
@Order(value = 2)
@Component
public class ValidatePlannedNotificationBeforeUpdateListener extends BaseBeforeUpdateListener<PlannedNotification> {

    @Autowired
    PlannedNotificationService plannedNotificationService;

    @Override
    public void onApplicationEvent(BeforeUpdateEvent<PlannedNotification> event) {

        PlannedNotification dbPlannedNotification = event.getStoredDomainModel();
        PlannedNotification plannedNotification = event.getDomainModel();
        plannedNotificationService.validateNotNullDate(plannedNotification, "error.plannedNotification.date.null");
        plannedNotificationService
                .validateBeforeCurrentDate(dbPlannedNotification, "error.plannedNotification.date.beforeCurrentDate");
        plannedNotificationService.validateNotificationSchedule(dbPlannedNotification,
                "error.plannedNotification.hasSentNotificationSchedule");
    }

}
