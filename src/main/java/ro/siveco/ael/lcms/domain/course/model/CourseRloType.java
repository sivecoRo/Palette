package ro.siveco.ael.lcms.domain.course.model;

/**
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: Siveco Romania SA</p>
 *
 * @author RobertR
 * @version : 1.0 / Dec 6, 2006
 */
public enum CourseRloType
{
	LESSON,
	TEST,
	TEST_PATTERN,
    SURVEY,
	FILE,
	LINK,
	WEB_STORE
}
