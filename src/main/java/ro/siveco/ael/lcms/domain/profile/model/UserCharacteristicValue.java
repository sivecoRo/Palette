package ro.siveco.ael.lcms.domain.profile.model;

import ro.siveco.ael.lcms.domain.auth.model.AelUser;
import ro.siveco.ael.lcms.domain.metadata.model.Metadata;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by CatalinS on 16.06.2017.
 */
@Entity
@Table(name = "user_characteristic_values")
@SequenceGenerator(name = "user_characteristic_values_seq", sequenceName = "user_characteristic_values_seq")
public class UserCharacteristicValue extends ro.siveco.ram.starter.model.base.BaseEntity {

    private UserCharacteristic characteristic;
    private Metadata metadata;
    private String valueString;
    private Boolean valueBoolean;
    private AelUser user;

    @Id
    @GeneratedValue(generator = "user_characteristic_values_seq", strategy = GenerationType.AUTO)
    public Long getId() {
        return super.getId();
    }
    
    @ManyToOne
	@JoinColumn( name = "characteristic_id", nullable = false )
	public UserCharacteristic getCharacteristic() {
		return characteristic;
	}

	public void setCharacteristic(UserCharacteristic characteristic) {
		this.characteristic = characteristic;
	}

    
    @ManyToOne
	@JoinColumn( name = "metadata_id" )
	public Metadata getMetadata() {
		return metadata;
	}

	public void setMetadata(Metadata metadata) {
		this.metadata = metadata;
	}


    @Column(name = "value_string", length = 200 )
	public String getValueString() {
		return valueString;
	}

	public void setValueString(String valueString) {
		this.valueString = valueString;
	}


    @Column(name = "value_boolean" )
	public Boolean getValueBoolean() {
		return valueBoolean;
	}

	public void setValueBoolean(Boolean valueBoolean) {
		this.valueBoolean = valueBoolean;
	}

	@ManyToOne
	@NotNull
	@JoinColumn( name = "user_id", nullable = false )
	public AelUser getUser()
	{
		return user;
	}

	public void setUser( AelUser _user )
	{
		user = _user;
	}
}
