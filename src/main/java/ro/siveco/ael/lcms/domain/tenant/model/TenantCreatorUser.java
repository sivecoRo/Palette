package ro.siveco.ael.lcms.domain.tenant.model;

import ro.siveco.ael.lcms.domain.auth.model.User;
import ro.siveco.ram.model.Entity;

/**
 * User: AlexandruVi
 * Date: 2017-04-26
 */
public interface TenantCreatorUser extends Entity<Long> {

    User getCreator();

    void setCreator(User creator);
}
