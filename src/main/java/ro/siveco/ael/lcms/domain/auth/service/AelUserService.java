package ro.siveco.ael.lcms.domain.auth.service;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.stereotype.Service;
import ro.siveco.ael.lcms.domain.auth.model.AelGroup;
import ro.siveco.ael.lcms.domain.auth.model.AelUser;
import ro.siveco.ael.lcms.domain.auth.model.User;
import ro.siveco.ael.lcms.domain.preference.model.Preference;
import ro.siveco.ael.lcms.domain.preference.model.Preferences;
import ro.siveco.ael.lcms.domain.preference.service.PreferencesService;
import ro.siveco.ael.lcms.domain.tenant.model.Tenant;
import ro.siveco.ael.lcms.repository.AelUserRepository;
import ro.siveco.ael.service.security.annotation.Groups;
import ro.siveco.ael.web.controllers.WithUserBaseEntityController;
import ro.siveco.ael.web.mvc.MvcProperties;
import ro.siveco.ram.app.exception.AppException;
import ro.siveco.ram.starter.service.BaseEntityService;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.Principal;
import java.util.*;

@Service
public class AelUserService extends BaseEntityService<AelUser> {

    @Autowired
    AelGroupService aelGroupService;

    @Autowired
    PreferencesService preferencesService;

    @Autowired
    private ShaPasswordEncoder passwordEncoder;

    @Autowired
    private MvcProperties mvcProperties;

    public User findOneById(Long id){
        return ((AelUserRepository)getResourceRepository()).findOneById(id);
    }

    public Optional<User> findOneByUsername(String username)
    {
        return ((AelUserRepository)getResourceRepository()).findOneByUsername(username);
    }

    public List<AelUser> getUsersByTenantId(Long tenantId) {
        return ((AelUserRepository)getResourceRepository()).getUsersByTenantId(tenantId);
    }

    public List<AelUser> getUsersByTenantNull() {
        return ((AelUserRepository)getResourceRepository()).getUsersByTenantNull();
    }

    @Autowired
    public AelUserService(AelUserRepository aelUserRepository) {
        super(AelUser.class, aelUserRepository);
    }

    public AelUser findByEmail(String email) {
        return (AelUser) ((AelUserRepository) getResourceRepository()).findOneByEmail(email);
    }

    public List<AelUser> findByIdUsers(List<Long> idFunctions) {
        return ((AelUserRepository) getResourceRepository()).findByIdUsers(idFunctions);
    }

    public List<AelUser> findAll() {
        return ((AelUserRepository) getResourceRepository()).findAll();
    }

    public Long findUserCountForLocation(String location) {
        return ((AelUserRepository) getResourceRepository()).findUserCountForLocation(location);
    }
    public Long findUserCountForLocationCoordinates(String coordinates) {
        return ((AelUserRepository) getResourceRepository()).findUserCountForLocationCoordinates(coordinates);
    }

    public AelUser findByUsername(String username, Tenant tenant) {
        return (AelUser) ((AelUserRepository) getResourceRepository()).findByUsernameAndTenant(username, tenant);
    }

    public AelUser findById(Long idUser) {
        return (AelUser) ((AelUserRepository) getResourceRepository()).findOneById(idUser);
    }

    public AelUser findByMarca(String marca) {
        return ((AelUserRepository) getResourceRepository()).findByMarca(marca);
    }

    public Optional<User> findByUsername(String username) {
        return ((AelUserRepository) getResourceRepository()).findOneByUsername(username);
    }

    public void validateUserForCreation(AelUser aelUser)
    {
        if (!validateEmail(aelUser)) {
            throw new AppException("error.aelUser.create.duplicateEmail");
        } else if (!validatePassword(aelUser)) {
            throw new AppException("error.aelUser.create.passwordError");
        } else if (!validateUsername(aelUser)) {
            throw new AppException("error.aelUser.create.duplicateUsername");
        }
    }

    public void validateUserForUpdate(AelUser aelUser)
    {
        if (!validateEmail(aelUser)) {
            throw new RuntimeException("error.aelUser.create.duplicateEmail");
        } else if (!validateUsername(aelUser)) {
            throw new RuntimeException("error.aelUser.create.duplicateUsername");
        }
    }

    public Boolean validateEmail(AelUser aelUser) {

        Boolean isNewAelUser = aelUser.getId() == null;
        AelUser aelUserWithSameEmail = findByEmail(aelUser.getEmail());
        return checkIfUserExists(aelUser, isNewAelUser, aelUserWithSameEmail);
    }

    public Boolean checkIfUserExists(AelUser aelUser, Boolean isNewAelUser, AelUser aelUserWithSameEmail) {

        if (aelUserWithSameEmail != null) {
            if (isNewAelUser || aelUser.getId().compareTo(aelUserWithSameEmail.getId()) != 0) {
                return Boolean.FALSE;
            }
        }
        return Boolean.TRUE;
    }

    public Boolean validatePassword(AelUser aelUser) {
        if (!aelUser.getPassword().equals(aelUser.getConfirmPassword()) || aelUser.getPassword().equals("") ||
                aelUser.getConfirmPassword().equals("")) {
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }

    public Boolean validateUsername(AelUser aelUser) {
        Boolean isNewAelUser = aelUser.getId() == null;
        AelUser aelUserWithSameUsername = findByUsername(aelUser.getUsername(), aelUser.getTenant());
        return checkIfUserExists(aelUser, isNewAelUser, aelUserWithSameUsername);
    }

    public Boolean validateMarca(AelUser aelUser) {
        Boolean isNewAelUser = aelUser.getId() == null;
        AelUser aelUserWithSameMarca = findByMarca(aelUser.getMarca());
        return checkIfUserExists(aelUser, isNewAelUser, aelUserWithSameMarca);
    }

    public Long getTenantIdByUsername(String username) {
        return ((AelUserRepository) getResourceRepository()).getTenantIdByUsername(username);
    }

    public AelUser getAelUserByUsername(String username) {
        return ((AelUserRepository) getResourceRepository()).getAelUserByUsername(username);
    }

    public List<AelUser> getUsersByTenant(Long tenantId) {
        if (tenantId == null) {
            return ((AelUserRepository) getResourceRepository()).getUsersByTenantNull();
        }

        return ((AelUserRepository) getResourceRepository()).getUsersByTenantId(tenantId);
    }

    public List<AelUser> getStudentsByTenant(Long tenantId) {
        if (tenantId == null) {
            return ((AelUserRepository) getResourceRepository()).getUsersByTenantNull();
        }

        return ((AelUserRepository) getResourceRepository()).getUsersByTenantId(tenantId);
    }

    public List<AelUser> getUsersByGroupAndTenant(Long groupId, Long tenantId) {
        if (tenantId == null) {
            return ((AelUserRepository) getResourceRepository()).getUsersByGroupIdAndTenantNull(groupId);
        }

        return ((AelUserRepository) getResourceRepository()).getUsersByGroupIdAndTenantId(groupId, tenantId);
    }

    public List<AelUser> getUsersByGroup(String groupName){
        return ((AelUserRepository) getResourceRepository()).getUsersByGroup(groupName);
    }

    public boolean existsEmail(String email) {

        User user = ((AelUserRepository) getResourceRepository()).getByMailAddress(email);

        if (user != null) {
            return true;
        }

        return false;
    }

    public AelUser getTenantAdministrator(Long tenantId) {
        AelUser admin = null;
        List<AelUser> users = new ArrayList<>();

        if (tenantId == null) {
            users = ((AelUserRepository) getResourceRepository()).getUsersByTenantNull();
        } else {
            users = ((AelUserRepository) getResourceRepository()).getUsersByTenantId(tenantId);
        }
        if (users != null && users.size() > 0) {
            admin = users.get(0);
        }

        return admin;
    }

    public List<AelUser> importUsersFromXLS(InputStream content, Tenant tenant) throws IOException {
        List<StringBuilder> stringBuilderList = new ArrayList<>();
        StringBuilder usernameNull = new StringBuilder();
        StringBuilder lastNameNull = new StringBuilder();
        StringBuilder firstNameNull = new StringBuilder();
        StringBuilder duplicateEmailUsers = new StringBuilder();
        List<AelUser> aelUserList = new ArrayList<>();
        List<String> email = new ArrayList<>();
        try {
            POIFSFileSystem fs = new POIFSFileSystem(content);
            HSSFWorkbook wb = new HSSFWorkbook(fs);
            HSSFSheet sheet = wb.getSheetAt(0);
            HSSFRow row;
            int rows; // No of rows
            rows = sheet.getPhysicalNumberOfRows();
            int cols = 0; // No of columns
            int tmp = 0;
            for (int i = 0; i < 10 || i < rows; i++) {
                row = sheet.getRow(i);
                if (row != null) {
                    tmp = sheet.getRow(i).getPhysicalNumberOfCells();
                    if (tmp > cols) {
                        cols = tmp;
                    }
                }
            }
            for (int r = 1; r < rows; r++) {
                row = sheet.getRow(r);
                if (setNullIfBlank(row.getCell(4)) == null) {
                    throw new AppException("error.aelUser.create.missing.email");
                }
                else if (setNullIfBlank(row.getCell(0)) == null) {
                    throw new AppException("error.aelUser.create.missing.username");
                }
                else if (setNullIfBlank(row.getCell(1)) == null) {
                    throw new AppException("error.aelUser.create.missing.lastName");
                }
                else if (setNullIfBlank(row.getCell(2)) == null) {
                    throw new AppException("error.aelUser.create.missing.firstName");
                }
                else
                {
                    AelUser aelUser = new AelUser();
                    aelUser.setUsername(setNullIfBlank(row.getCell(0)));
                    aelUser.setFirstName(setNullIfBlank(row.getCell(1)));
                    aelUser.setLastName(setNullIfBlank(row.getCell(2)));
                    aelUser.setPassword(passwordEncoder.encodePassword((setNullIfBlank(row.getCell(3))), null));
                    aelUser.setEmail(setNullIfBlank(row.getCell(4)));
                    aelUser.setGroups(saveGroups(row.getCell(6)));
                    aelUser.setDisabled(setNullIfBlank((row.getCell(6))) == null ? false :
                            (setNullIfBlank((row.getCell(6))).equalsIgnoreCase("activ") ? false : true));
                    aelUser.setMarca(setNullIfBlank(row.getCell(9)));
                    aelUser.setPhoneNo(setNullIfBlank(row.getCell(10)));
                    aelUser.setImported(true);
                    aelUser.setTenant(tenant);
                    aelUser.setRelatedUid(aelUser.getUsername());
                    email.add(row.getCell(4).toString());
                    aelUser.setCreateDate(new Date());
                    aelUser.setDisplayName(getUserDisplayName(aelUser));
                    aelUserList.add(aelUser);
                }

            }
        } catch (Exception ioe) {
            ioe.printStackTrace();
        }

      //  usersListValidation(email);

        return aelUserList;
    }

    private List<AelGroup> saveGroups(HSSFCell groups) {
        List<AelGroup> aelGroupList = new ArrayList<>();
        aelGroupList.addAll(aelGroupService.findByidGroups(Groups.Default.Everybody.get()));
        if (groups != null && StringUtils.isNotBlank(groups.toString())) {

            String[] groupsList = groups.toString().split(",");
            if(groupsList != null && groupsList.length > 0) {
                for (String group : groupsList) {
                    aelGroupList.addAll(aelGroupService.findByidGroups(group));
                }
            }
        }

        return aelGroupList;
    }

    private List<AelGroup> saveGroups(String groups) {
        List<AelGroup> aelGroupList = new ArrayList<>();
        aelGroupList.addAll(aelGroupService.findByidGroups(Groups.Default.Everybody.get()));
        if (groups != null && StringUtils.isNotBlank(groups.toString())) {
            String groupsList =  groups.toString();
            aelGroupList.addAll(aelGroupService.findByidGroups(groupsList));
        }
        return aelGroupList;
    }

    public static String setNullIfBlank(HSSFCell arg) {
        if (arg != null && StringUtils.isNotBlank(arg.toString())) {
            return arg.toString();
        }
        return null;
    }

    public static String setNullIfBlank(String arg) {
        if (arg != null && StringUtils.isNotBlank(arg)) {
            return arg;
        }
        return null;
    }

    private void usersListValidation(List<String> email) throws RuntimeException {

        List<AelUser> aelUsers = ((AelUserRepository) getResourceRepository()).findAllByEmail(email);
        if (email.size() != 0) {
            if (aelUsers.size() > 0) {
                throw new AppException("error.aelUser.create.duplicateEmail");
            }
        }
    }

    public List<AelUser> importUsersFromCSV(InputStream inputStream, Tenant tenant) {
        List<StringBuilder> stringBuilderList = new ArrayList<>();
        StringBuilder usernameNull = new StringBuilder();
        StringBuilder lastNameNull = new StringBuilder();
        StringBuilder firstNameNull = new StringBuilder();
        StringBuilder duplicateEmailUsers = new StringBuilder();
        List<AelUser> aelUserList = new ArrayList<>();
        List<String> email = new ArrayList<>();
        String line = "";
        String cvsSplitBy = ";";
        Long i = 0L;

        try (BufferedReader br = new BufferedReader(new InputStreamReader(inputStream))) {
            while ((line = br.readLine()) != null) {
                i = i + 1L;
                if (!i.equals(1L)) {
                    String[] country = line.split(cvsSplitBy);
                    if (setNullIfBlank(country[4]) == null) {
                        throw new AppException("error.aelUser.create.missing.email");
                    }
                    else if (setNullIfBlank(country[0]) == null) {
                        throw new AppException("error.aelUser.create.missing.username");
                    }
                    else if (setNullIfBlank(country[1]) == null) {
                        throw new AppException("error.aelUser.create.missing.lastName");
                    }
                    else if (setNullIfBlank(country[2]) == null) {
                        throw new AppException("error.aelUser.create.missing.firstName");
                    }
                    else {
                        AelUser aelUser = new AelUser();
                        aelUser.setUsername(setNullIfBlank(country[0]));
                        aelUser.setFirstName(setNullIfBlank(country[1]));
                        aelUser.setLastName(setNullIfBlank(country[2]));
                        aelUser.setPassword(passwordEncoder.encodePassword((setNullIfBlank(country[3])), null));
                        aelUser.setEmail(setNullIfBlank(country[4]));
                        aelUser.setGroups(saveGroups(country[6]));
                        aelUser.setDisabled(setNullIfBlank((country[6])) == null ? false :
                                (setNullIfBlank((country[6])).equalsIgnoreCase("activ") ? false : true));
                        aelUser.setMarca(setNullIfBlank(country[9]));
                        aelUser.setPhoneNo(setNullIfBlank(country[10]));
                        aelUser.setImported(true);
                        aelUser.setTenant(tenant);
                        aelUser.setRelatedUid(UUID.randomUUID().toString());
                        email.add(country[4].toString());
                        aelUser.setCreateDate(new Date());
                        aelUser.setDisplayName(getUserDisplayName(aelUser));
                        aelUserList.add(aelUser);
                    }

                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

      //  usersListValidation(email);

        return aelUserList;
    }

    public Locale getLocaleForUser(AelUser user)
    {
        Preference preference = preferencesService.getByNameAndUser(Preferences.USER_LOCALE.toString(), user);
        Locale locale =
                Locale.forLanguageTag(preference != null ? preference.getValue().replaceAll("_", "-") : "en-US");

        return locale;
    }

    public void setLocaleForUser(AelUser user, Locale locale) {

        Preference preference = preferencesService.getByNameAndUser(Preferences.USER_LOCALE.toString(), user);
        if (preference == null) {
            preference = new Preference();
            preference.setUser(user);
            preference.setName(Preferences.USER_LOCALE.toString());
            preference.setValue(locale.toLanguageTag());
            preference.setGlobal(false);
            preference.setTenant(user.getTenant());
        } else {
            preference.setValue(locale.toLanguageTag());
        }
        preferencesService.saveOrUpdate(preference);
    }

    public AelUser saveOrUpdate(AelUser aelUser)
    {
        if(aelUser.getId() == null)
        {
            return save(aelUser);
        }
        else
        {
            return update(aelUser);
        }
    }

    public String getLanguage(Principal principal, HttpServletRequest request) {
        String language = "en";
        if(principal == null)
        {
            String languageString = (String) request.getHeader("accept-language");

            /* since IE is using a different ISO Country Code ('sl-SI') as Chrome ('sl'),
             * we will use a substring of what we get from browser */
            languageString = languageString.substring(0,2);

            final List<Locale.LanguageRange> languageRanges = Locale.LanguageRange.parse(languageString);
            if(languageRanges != null && languageRanges.size() > 0)
            {
                Locale.LanguageRange languageRange = languageRanges.get(0);
                Locale browserLocale = Locale.forLanguageTag(languageRange.getRange());

                List<Locale> allowedLocales = mvcProperties.getAvailableLocales();
                if(allowedLocales.contains(browserLocale))
                {
                    language = browserLocale.getLanguage();

                }
                else
                {
                    Preference globalLanguage =  preferencesService.getGlobalPreferenceByName(Preferences.GLOBAL_AELCLIENT_DEFAULT_APPLICATION_LOCALE.toString());
                    language = globalLanguage.getValue();
                }
            }
            else
            {
                Preference globalLanguage =  preferencesService.getGlobalPreferenceByName(Preferences.GLOBAL_AELCLIENT_DEFAULT_APPLICATION_LOCALE.toString());
                language = globalLanguage.getValue();
            }
        }
        else
        {
            AelUser currentUser = (AelUser) WithUserBaseEntityController.getUser();
            Locale userLocale = getLocaleForUser(currentUser);
            language = userLocale.getLanguage();
        }
        return language;
    }

    public String getLanguageForLogin(Optional<User> user, HttpServletRequest request) {
        String language = "en";
        if(user.isPresent()) {
            Preference preference = preferencesService.getByNameAndUser(Preferences.USER_LOCALE.toString(), (AelUser) user.get());

            if (preference != null) {
                language = preference.getValue().replaceAll("_", "-");
            }
            else {
                String languageString = request.getHeader("accept-language");
                languageString = languageString.substring(0, 2);

                final List<Locale.LanguageRange> languageRanges = Locale.LanguageRange.parse(languageString);
                if (languageRanges != null && languageRanges.size() > 0) {
                    Locale.LanguageRange languageRange = languageRanges.get(0);
                    Locale browserLocale = Locale.forLanguageTag(languageRange.getRange());

                    List<Locale> allowedLocales = mvcProperties.getAvailableLocales();
                    if (allowedLocales.contains(browserLocale)) {
                        language = browserLocale.getLanguage();

                    }
                    else {
                        Preference globalLanguage = preferencesService.getGlobalPreferenceByName(Preferences.GLOBAL_AELCLIENT_DEFAULT_APPLICATION_LOCALE.toString());
                        language = globalLanguage.getValue();
                    }
                }
            }
        }
        else {
            Preference globalLanguage = preferencesService.getGlobalPreferenceByName(Preferences.GLOBAL_AELCLIENT_DEFAULT_APPLICATION_LOCALE.toString());
            language = globalLanguage.getValue();
        }
        return language;
    }

    public String getUserDisplayName(AelUser user) {
        if (user != null) {
            return ( (user.getFirstName() == null || user.getFirstName().isEmpty() || user.getFirstName().trim().isEmpty()) ? "" : user.getFirstName())
                    + ( (user.getLastName() == null || user.getLastName().isEmpty() || user.getLastName().trim().isEmpty()) ? "" : (" " + user.getLastName()));
        }
        return null;
    }
}
