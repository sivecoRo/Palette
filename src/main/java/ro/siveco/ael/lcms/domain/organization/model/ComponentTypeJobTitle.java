package ro.siveco.ael.lcms.domain.organization.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by IntelliJ IDEA.
 * User: RazvanNi
 * Date: 24.07.2007
 */
@Entity
@Table( name = "component_type_job_title" )
@SequenceGenerator( name="comp_type_job_tytle_seq", sequenceName = "comp_type_job_tytle_seq")
//@TableComment(comment = "Stores the associations between the system's component types and job titles")
public class ComponentTypeJobTitle extends ro.siveco.ram.starter.model.base.BaseEntity implements Serializable {
    private Long id;

//	@ColumnComment(comment = "The component type's id")
	private ComponentType componentType;

//	@ColumnComment(comment = "The job title's id")
	private JobTitle jobTitle;

    @Id
    @GeneratedValue( generator = "comp_type_job_tytle_seq", strategy = GenerationType.AUTO )
	public Long getId()
	{
		return super.getId();
	}

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	@JoinColumn(name = "component_type_id", nullable = false)
    public ComponentType getComponentType() {
        return componentType;
    }

    public void setComponentType(ComponentType componentType) {
        this.componentType = componentType;
    }

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	@JoinColumn(name = "job_title_id", nullable = false)
    public JobTitle getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(JobTitle jobTitle) {
        this.jobTitle = jobTitle;
    }
}
