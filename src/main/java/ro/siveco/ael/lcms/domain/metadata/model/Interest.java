package ro.siveco.ael.lcms.domain.metadata.model;

import org.springframework.data.domain.Sort;
import ro.siveco.ram.starter.ui.config.PropertyViewDescriptor;
import ro.siveco.ram.starter.ui.config.Sorter;
import ro.siveco.ram.starter.ui.config.ViewDescriptor;
import ro.siveco.ram.starter.ui.model.IndexViewType;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by albertb on 1/30/2018.
 */
@Entity
@Table(name="interests")
@SequenceGenerator(name = "interests_seq", sequenceName = "interests_seq")
@ViewDescriptor(pageSize = 10, viewTypes = {IndexViewType.GRID}, searchEnabled = true, defaultSearchProperty = "id",
        sort = {@Sorter(propertyName = "id", direction = Sort.Direction.ASC)})
public class Interest extends ro.siveco.ram.starter.model.base.BaseEntity{
    private Tag tag;
    private String image;
    private String imageSelected;
    private String displayName;
    private Boolean selected;

    @Id
    @GeneratedValue(generator = "interests_seq", strategy =GenerationType.AUTO)
    public Long getId(){return super.getId();}

    @OneToOne
    @JoinColumn(name = "tag_id", nullable = false)
    @NotNull
    @PropertyViewDescriptor(hidden = true)
    public Tag getTag() {
        return tag;
    }

    @Column(name = "image", nullable = false)
    @NotNull
    public String getImage(){return image;}

    @Column(name = "image_selected", nullable = false)
    @NotNull
    public String getImageSelected(){return imageSelected;}

    public void setTag(Tag tag) {
        this.tag = tag;
    }

    public void setImageSelected(String imageSelected) {
        this.imageSelected = imageSelected;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Transient
    @PropertyViewDescriptor(hidden = true)
    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    @Transient
    @PropertyViewDescriptor(hidden = true)
    public Boolean getSelected() {
        return selected;
    }

    public void setSelected(Boolean selected) {
        this.selected = selected;
    }
}
