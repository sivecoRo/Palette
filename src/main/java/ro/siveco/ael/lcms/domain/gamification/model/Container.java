package ro.siveco.ael.lcms.domain.gamification.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

/**
 * Created by LucianR on 23.10.2017.
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Container {

    private String id;
    private String name;
    private String titleUI;
    private String description;
    private String owner;
    private String createdBy;
    private String tenant;
    private boolean terminated;
    private List<java.lang.Object> classificationTask;
    private List<java.lang.Object> actions;
    private List<java.lang.Object> badges;
    private List<java.lang.Object> levels;
    private List<java.lang.Object> rules;
    private List<java.lang.Object> points;
    private List<java.lang.Object> leaderboards;
    private boolean sendNotification;
    private long lastModify;
    private boolean publicContainer;

    public String getId() {

        return id;
    }

    public void setId(String id) {

        this.id = id;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

    public String getTitleUI() {

        return titleUI;
    }

    public void setTitleUI(String titleUI) {

        this.titleUI = titleUI;
    }

    public String getOwner() {

        return owner;
    }

    public void setOwner(String owner) {

        this.owner = owner;
    }

    public String getCreatedBy() {

        return createdBy;
    }

    public void setCreatedBy(String createdBy) {

        this.createdBy = createdBy;
    }

    public String getTenant() {

        return tenant;
    }

    public void setTenant(String tenant) {

        this.tenant = tenant;
    }

    public boolean isTerminated() {

        return terminated;
    }

    public void setTerminated(boolean terminated) {

        this.terminated = terminated;
    }

    public List<Object> getClassificationTask() {

        return classificationTask;
    }

    public void setClassificationTask(List<Object> classificationTask) {

        this.classificationTask = classificationTask;
    }

    public List<Object> getPoints() {

        return points;
    }

    public void setPoints(List<Object> points) {

        this.points = points;
    }

    public List<Object> getLeaderboards() {

        return leaderboards;
    }

    public void setLeaderboards(List<Object> leaderboards) {

        this.leaderboards = leaderboards;
    }

    public boolean isSendNotification() {

        return sendNotification;
    }

    public void setSendNotification(boolean sendNotification) {

        this.sendNotification = sendNotification;
    }

    public long getLastModify() {

        return lastModify;
    }

    public void setLastModify(long lastModify) {

        this.lastModify = lastModify;
    }

    public boolean isPublicContainer() {

        return publicContainer;
    }

    public void setPublicContainer(boolean publicContainer) {

        this.publicContainer = publicContainer;
    }

    public String getDescription() {

        return description;
    }

    public void setDescription(String description) {

        this.description = description;
    }

    public List<Object> getActions() {

        return actions;
    }

    public void setActions(List<Object> actions) {

        this.actions = actions;
    }

    public List<Object> getBadges() {

        return badges;
    }

    public void setBadges(List<Object> badges) {

        this.badges = badges;
    }

    public List<Object> getLevels() {

        return levels;
    }

    public void setLevels(List<Object> levels) {

        this.levels = levels;
    }

    public List<Object> getRules() {

        return rules;
    }

    public void setRules(List<Object> rules) {

        this.rules = rules;
    }
}
