package ro.siveco.ael.lcms.domain.course.model;
import com.fasterxml.jackson.annotation.JsonIgnore;
import ro.siveco.ael.lcms.domain.auth.model.AelUser;
import ro.siveco.ram.starter.ui.config.ActionDescriptor;
import ro.siveco.ram.starter.ui.config.PropertyViewDescriptor;
import ro.siveco.ram.starter.ui.config.ViewDescriptor;
import ro.siveco.ram.starter.ui.model.ActionType;
import ro.siveco.ram.starter.ui.model.GridSize;
import ro.siveco.ram.starter.ui.model.IndexViewType;
import ro.siveco.ram.starter.ui.model.ViewRendererType;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Copyright: Copyright (c) 2006
 * Company: Siveco Romania SA
 *
 * @author RobertR
 * @version : 1.0 / Nov 30, 2006
 */

@Entity
@Table(name = "planned_course_students")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(
        name = "discriminator",
        discriminatorType = DiscriminatorType.STRING
)
@DiscriminatorValue(value = "C")
@SequenceGenerator(name = "planned_course_students_seq", sequenceName = "planned_course_students_seq")
// Numele tabelei asociate trebuie declarat ca plural ral numelui entitatii
//@TableComment(comment = "Stores the associations between courses and students")
@ViewDescriptor(pageSize = 10, viewTypes = {IndexViewType.CARD},
        cardSizeLarge = GridSize.THREE_PER_ROW,
        cardSizeMedium = GridSize.TWO_PER_ROW,
        searchEnabled = false, defaultSearchProperty = "plannedCourse.course.name",
        defaultActions = {
                                                        /* --- [actions] --- */

                                                        /* --- [flows] --- */
                @ActionDescriptor(type = ActionType.CREATE, forwardFor = "{modelName}-OPEN_CREATE_FORM",
                        availableOnlyInForm = true),
                @ActionDescriptor(type = ActionType.UPDATE, forwardFor = "{modelName}-OPEN_EDIT_FORM",
                        availableOnlyInForm = true),
                @ActionDescriptor(type = ActionType.LIST, availableInForm = true, backToFor = {
                        "{modelName}-OPEN_CREATE_FORM",
                        "{modelName}-CREATE",
                        "{modelName}-OPEN_PRESENTATION",
                        "{modelName}-OPEN_EDIT_FORM",
                        "{modelName}-UPDATE",
                        "{modelName}-DELETE"
                })
        })
public class PlannedCourseStudent extends ro.siveco.ram.starter.model.base.BaseEntity implements Serializable {

    //@ColumnComment(comment = "The planned course's id")
    private PlannedCourse plannedCourse;

    //@ColumnComment(comment = "Whether the student's participation to the planned course is approved or not")
    private Boolean approved;

    //@ColumnComment(comment = "Whether the student's participation was approved when the course was already started")
    private Boolean approvedAfterCourseStarted = Boolean.FALSE;

    //@ColumnComment(comment = "Whether the student's participation was approved by the teacher")
    private Boolean allowedByTeacher;

    private String comment;

    private Boolean showComment;

    private Double rating;

    private Date startDate;

    private Date endDate;

    private AelUser user;

    private Boolean addedToFavorites;

    private Boolean removeItem;

    private Boolean likeItem;

    private Boolean dislikeItem;

    private Boolean attending;

    private Boolean reviewEmailSent;

    private Boolean reviewed;

    @Id
    @GeneratedValue(generator = "planned_course_students_seq", strategy = GenerationType.AUTO)
    public Long getId() {

        return super.getId();
    }

    @ManyToOne
    @JoinColumn(name = "planned_course_id", nullable = false)
    @JsonIgnore
    @PropertyViewDescriptor(order = 1, hiddenOnList = true, hiddenOnCard = true, hiddenOnGrid = true,
            hiddenOnPresentation = true)
    public PlannedCourse getPlannedCourse() {

        return plannedCourse;
    }

    public void setPlannedCourse(PlannedCourse _plannedCourse) {

        plannedCourse = _plannedCourse;
    }

    @NotNull
    @PropertyViewDescriptor(hidden = true)
    public Boolean getApproved() {

        return approved;
    }

    public void setApproved(Boolean _approved) {

        approved = _approved;
    }

    @Column(name = "approved_after_course_started")
    @PropertyViewDescriptor(hidden = true)
    public Boolean getApprovedAfterCourseStarted() {

        return approvedAfterCourseStarted;
    }

    public void setApprovedAfterCourseStarted(Boolean approvedAfterCourseStarted) {

        this.approvedAfterCourseStarted = approvedAfterCourseStarted;
    }

    @Column(name = "allowed_by_teacher")
    @PropertyViewDescriptor(hidden = true)
    public Boolean getAllowedByTeacher() {

        return allowedByTeacher;
    }

    public void setAllowedByTeacher(Boolean allowedByTeacher) {

        this.allowedByTeacher = allowedByTeacher;
    }

    @Column(name = "rating")
    @PropertyViewDescriptor(rendererType = ViewRendererType.RATING_STARS,
            displayLabelInPresentation = false,
            order = -2,
            editInline = true, hiddenOnCard = true, hiddenOnGrid = true, hiddenOnList = true,
            clickAction = @ActionDescriptor(type = ActionType.CUSTOM, uri = "/my-courses/rateCourse/"),
            groupName = "courseDetails")
    public Double getRating() {

        return rating;
    }

    public void setRating(Double rating) {

        this.rating = rating;
    }

    @Column(name = "student_start_date")
    @PropertyViewDescriptor(order = 3, rendererType = ViewRendererType.DATE, groupName = "courseDetails",
            hiddenOnCard = true)
    public Date getStartDate() {

        return startDate;
    }

    public void setStartDate(Date startDate) {

        this.startDate = startDate;
    }

    @Column(name = "student_end_date")
    @PropertyViewDescriptor(order = 4, rendererType = ViewRendererType.DATE, hiddenOnPresentation = true,
            hiddenOnCard = true)
    public Date getEndDate() {

        return endDate;
    }

    public void setEndDate(Date endDate) {

        this.endDate = endDate;
    }

    @ManyToOne
    @NotNull
    @JoinColumn(name = "user_id")
    @PropertyViewDescriptor(hidden = true)
    public AelUser getUser() {

        return user;
    }

    public void setUser(AelUser user) {

        this.user = user;
    }

    @Column(name = "comment")
    @PropertyViewDescriptor(hidden = true)
    public String getComment() {

        return comment;
    }

    public void setComment(String comment) {

        this.comment = comment;
    }

    @Column(name = "show_comment")
    @PropertyViewDescriptor(hidden = true)
    public Boolean getShowComment() {

        return showComment;
    }

    public void setShowComment(Boolean showComment) {

        this.showComment = showComment;
    }

    @Column(name="added_to_favorites")
    public Boolean getAddedToFavorites() {
        return addedToFavorites;
    }

    public void setAddedToFavorites(Boolean addedToFavorites) {
        this.addedToFavorites = addedToFavorites;
    }

    @Column(name = "remove_item")
    public Boolean getRemoveItem() {
        return removeItem;
    }

    public void setRemoveItem(Boolean removeItem) {
        this.removeItem = removeItem;
    }

    @Column(name="like_item")
    public Boolean getLikeItem() {
        return likeItem;
    }

    public void setLikeItem(Boolean likeItem) {
        this.likeItem = likeItem;
    }

    @Column(name="dislike_item")
    public Boolean getDislikeItem() {
        return dislikeItem;
    }

    public void setDislikeItem(Boolean dislikeItem) {
        this.dislikeItem = dislikeItem;
    }

    @Transient
    @PropertyViewDescriptor(secondaryContent = false, displayLabelInPresentation = false, order = 3,
            hiddenOnCard = true,
            groupName = "courseDetails", rendererType = ViewRendererType.HTML)
    public String getCourseDescription() {

        if (getPlannedCourse() != null && getPlannedCourse().getCourse() != null) {
            return getPlannedCourse().getCourse().getDescription();
        }

        return "";
    }

    @Transient
    @PropertyViewDescriptor(order = -3, displayLabelInPresentation = false,
            groupName = "image",
            rendererType = ViewRendererType.IMAGE)
    public String getImageSrc() {

        String imageSrc = "/img/generic/generic-course-light.png";
        if (getPlannedCourse() != null && getPlannedCourse().getCourse() != null &&
                getPlannedCourse().getCourse().getImageSrc() != null) {
            return getPlannedCourse().getCourse().getImageSrc();
        }
        return imageSrc;
    }

    @Transient
    @PropertyViewDescriptor(order = 2, rendererType = ViewRendererType.MODEL_AVATAR,
            secondaryContent = true, displayLabelInPresentation = false, groupName = "instructors", hiddenOnCard = true)
    public AelUser getTeacher() {

        if (getPlannedCourse() != null) {
            return getPlannedCourse().getTeacher();
        }
        return null;
    }

    @Transient
    @PropertyViewDescriptor(order = 2, displayLabelInPresentation = false, rendererType = ViewRendererType.RATING_STARS,
            hiddenOnPresentation = true, hiddenOnCard = true)
    public BigDecimal getPlannedCourseRating() {

        if (getPlannedCourse() != null && getPlannedCourse().getRating() != null) {
            return getPlannedCourse().getRating();
        }

        return BigDecimal.ZERO;
    }

    @Override
    @Transient
    @PropertyViewDescriptor(
            order = 0,
            hiddenOnForm = true,
            hiddenOnGrid = true,
            displayLabelInPresentation = false,
            rendererType = ViewRendererType.OBJECT_LABEL,
            groupName = "courseDetails",
            clickAction = {
                    @ActionDescriptor(type = ActionType.OPEN_PRESENTATION),
                    @ActionDescriptor(type = ActionType.LIST, backToFor = "{modelName}-OPEN_PRESENTATION")})
    public String getObjectLabel() {

        if (getPlannedCourse() != null && getPlannedCourse().getCourse() != null) {
            return getPlannedCourse().getCourse().getName();
        }
        return "";
    }

    /*
        My Courses - Course details page
     */
    @Transient
    @PropertyViewDescriptor(order = 1,
            hiddenOnCard = true, hiddenOnGrid = true, hiddenOnList = true, groupName = "courseContent",
            rendererType = ViewRendererType.HTML)
    public String getCourseGeneralObjectives() {

        PlannedCourse pc = getPlannedCourse();
        if (pc != null && pc.getCourse() != null) {
            String obj = pc.getCourse().getObjective();
            if (obj != null && !"".equals(obj.trim())) {
                return obj;
            }
        }
        return "";
    }

    @Transient
    @PropertyViewDescriptor(order = 2, hiddenOnCard = true, hiddenOnGrid = true, hiddenOnList = true,
            groupName = "courseContent",
            hiddenOnPresentation = true,
            rendererType = ViewRendererType.HTML)
    public String getCourseObjectives() {

        PlannedCourse pc = getPlannedCourse();
        if (pc != null) {
            String obj = pc.getObjective();
            if (obj != null && !"".equals(obj.trim())) {
                return obj;
            }
        }
        return "";
    }

    @Column(name="attending")
    public Boolean getAttending() {
        return attending;
    }

    public void setAttending(Boolean attending) {
        this.attending = attending;
    }

    @PropertyViewDescriptor(hidden = true)
    @Column(name = "review_email_sent")
    public Boolean getReviewEmailSent() {
        return reviewEmailSent;
    }

    public void setReviewEmailSent(Boolean reviewEmailSent) {
        this.reviewEmailSent = reviewEmailSent;
    }

    @PropertyViewDescriptor(hidden = true)
    @Column(name = "reviewed")
    public Boolean getReviewed() {
        return reviewed;
    }

    public void setReviewed(Boolean reviewed) {
        this.reviewed = reviewed;
    }
}
