package ro.siveco.ael.lcms.domain.profile.listener;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import ro.siveco.ael.lcms.domain.auth.service.AelUserService;
import ro.siveco.ael.lcms.domain.profile.model.UserDetails;
import ro.siveco.ram.event.listener.type.single.BaseBeforeUpdateListener;
import ro.siveco.ram.event.type.single.BeforeUpdateEvent;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

import static org.springframework.web.servlet.i18n.SessionLocaleResolver.LOCALE_SESSION_ATTRIBUTE_NAME;

/**
 * User: LucianR
 * Date: 2017-10-26
 */
@Component
@Order(value = 2)
public class SetUserLanguageBeforeUpdateListener extends BaseBeforeUpdateListener<UserDetails> {

    private final AelUserService aelUserService;

    public SetUserLanguageBeforeUpdateListener(AelUserService aelUserService) {

        this.aelUserService = aelUserService;
    }

    @Override
    public void onApplicationEvent(BeforeUpdateEvent<UserDetails> event) {

        UserDetails userDetails = event.getDomainModel();
        if (userDetails.getLanguage() != null) {

            String languageCode=userDetails.getLanguage().getId();

            aelUserService
                    .setLocaleForUser(userDetails.getUser(), Locale.forLanguageTag(languageCode));


           HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes())
                    .getRequest();

            request.getSession().setAttribute(LOCALE_SESSION_ATTRIBUTE_NAME, Locale.forLanguageTag(languageCode));
        }
    }
}
