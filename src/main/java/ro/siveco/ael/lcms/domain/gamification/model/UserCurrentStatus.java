package ro.siveco.ael.lcms.domain.gamification.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

/**
 * Created by LucianR on 30.10.2017.
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserCurrentStatus {

    private String playerId;
    private String gameId;
    private Level level;
    private List<java.lang.Object> levels;
    private List<Point> points;

    public String getPlayerId() {

        return playerId;
    }

    public void setPlayerId(String playerId) {

        this.playerId = playerId;
    }

    public String getGameId() {

        return gameId;
    }

    public void setGameId(String gameId) {

        this.gameId = gameId;
    }

    public Level getLevel() {

        return level;
    }

    public void setLevel(Level level) {

        this.level = level;
    }

    public List<Object> getLevels() {

        return levels;
    }

    public void setLevels(List<Object> levels) {

        this.levels = levels;
    }

    public List<Point> getPoints() {

        return points;
    }

    public void setPoints(List<Point> points) {

        this.points = points;
    }
}
