package ro.siveco.ael.lcms.domain.notification.service;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.SessionHolder;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import ro.siveco.ael.lcms.domain.chat.web.ChatController;
import ro.siveco.ael.lcms.domain.notification.model.NotificationSchedule;
import ro.siveco.ael.lcms.domain.notification.model.NotificationType;
import ro.siveco.ael.lcms.domain.notification.model.UserMessage;
import ro.siveco.ael.lcms.repository.NotificationScheduleRepository;
import ro.siveco.ael.service.BaseService;
import ro.siveco.ael.service.BaseEmailService;
import ro.siveco.ael.service.mail.EmailService;
import ro.siveco.ael.service.mail.utils.MailUtils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;


@Service
public class NotificationsJobService extends BaseService {

	protected SessionFactory hibernateSessionFactory;
	private NotificationScheduleRepository notificationScheduleRepository;
	private UserMessageService userMessageService;
	private EmailService emailService;
	private MailUtils mailUtils;
    private ChatController chatController;
	private NotificationManagerService notificationManagerService;
    private BaseEmailService baseEmailService;

	@Autowired
    public NotificationsJobService(SessionFactory hibernateSessionFactory, NotificationScheduleRepository notificationScheduleRepository,
    								UserMessageService userMessageService, EmailService emailService, MailUtils mailUtils, 
    					    		ChatController chatController, NotificationManagerService notificationManagerService,
									BaseEmailService baseEmailService) {
        this.hibernateSessionFactory = hibernateSessionFactory;
        this.notificationScheduleRepository = notificationScheduleRepository;
        this.userMessageService = userMessageService;
        this.emailService = emailService;
        this.mailUtils = mailUtils;
        this.chatController = chatController;
        this.notificationManagerService = notificationManagerService;
        this.baseEmailService = baseEmailService;
    }
    
    public NotificationsJobService() {}
    
        
	@Scheduled(cron = "0 0/1 * * * ?")
	public void sendNotifications()
	{
		try
		{
			openSession();

			List<Long> notificationIds = notificationScheduleRepository.getAllNotificationScheduleToSend(new Date());

			for( Long id : notificationIds ) {
				NotificationSchedule notificationSchedule = notificationScheduleRepository.findOne(id);

				if ( NotificationType.MESSAGE_AND_MAIL == notificationSchedule.getNotificationType() || NotificationType.MAIL == notificationSchedule.getNotificationType() ) {
					if( mailUtils.isMailEnabled( notificationSchedule.getToUser() ) )
					{
						UserMessage userMessage = userMessageService.createMessageFromNotificationSchedule(notificationSchedule,
								notificationSchedule.getSubjectArgs() != null ? deserializeObject(notificationSchedule.getSubjectArgs()) : null,
								notificationSchedule.getMessageBodyArgs() != null ? deserializeObject(notificationSchedule.getMessageBodyArgs()) : null);

						// Send an e-mail with a notification. Body will be formatted according to the layout (with
						// message put into its place).

						try {
							String messageBodyInTemplate = baseEmailService.putMessageIntoLayout(userMessage.getTo(),
                                                                                                 userMessage.getMessageBody());

							emailService.sendEmail(userMessage.getFrom(), userMessage.getTo(),
                                                   userMessage.getSubject(), messageBodyInTemplate);
						} catch (Throwable e) {
							getLogger().log(Level.SEVERE, "Error sending notification email: " + e.getMessage(), e);
						}

					}

					userMessageService.translateAndSendUserMessage( notificationSchedule.getFromUser(),
					                                           notificationSchedule.getToUser(),
					                                           notificationSchedule.getSubjectArgs() != null ? deserializeObject( notificationSchedule.getSubjectArgs() ) : null,
					                                           notificationSchedule.getMessageBodyArgs() != null ? deserializeObject( notificationSchedule.getMessageBodyArgs() ) : null,
					                                           notificationSchedule.getUserMessageType(),
					                                           notificationSchedule.getUserMessageFormat(),
					                                           notificationSchedule.getEntityId(),
					                                           notificationSchedule.getCreatorIdAndTitle() );

					chatController.sendNavbarNumbers(notificationSchedule.getToUser().getUsername());
				}
				
				notificationSchedule.setStatus( Boolean.TRUE );
				notificationScheduleRepository.save( notificationSchedule );
			}
		}
		finally
		{
			closeSession();
		}
	}

	private void closeSession() {
		SessionHolder holder = ( SessionHolder ) TransactionSynchronizationManager.getResource( hibernateSessionFactory );
		Session s = holder.getSession();
		if( s.isOpen() )
        {
            s.flush();
        }

		TransactionSynchronizationManager.unbindResource( hibernateSessionFactory );
		s.close();
	}

	private void openSession() {
		Session s = hibernateSessionFactory.openSession();

		TransactionSynchronizationManager.bindResource( hibernateSessionFactory, new SessionHolder( s ) );
	}

	//@Scheduled(cron = "0 0 1 * * *")
	@Scheduled(cron = "0 0/5 * * * ?")
	public void sendDailyNotifications() {
		try {
			openSession();
			notificationManagerService.sendPastReviewEmails();
		}
		finally
		{
			closeSession();
		}
	}

	private Object[] deserializeObject( byte[] bytes )
	{
		try
		{
			ByteArrayInputStream bis = new ByteArrayInputStream( bytes );
			ObjectInput in = new ObjectInputStream( bis );
			return ( Object[] ) in.readObject();
		}
		catch( IOException e )
		{
			e.printStackTrace();
		}
		catch( ClassNotFoundException e )
		{
			e.printStackTrace();
		}
		return null;
	}

}
