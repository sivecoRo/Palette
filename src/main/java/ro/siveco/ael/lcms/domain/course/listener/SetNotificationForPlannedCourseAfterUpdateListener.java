package ro.siveco.ael.lcms.domain.course.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.course.model.PlannedCourse;
import ro.siveco.ael.lcms.domain.notification.service.PlannedNotificationService;
import ro.siveco.ram.event.listener.type.single.BaseAfterUpdateListener;
import ro.siveco.ram.event.type.single.AfterUpdateEvent;

/**
 * Created by IuliaP on 12.10.2017.
 */
@Component
public class SetNotificationForPlannedCourseAfterUpdateListener extends BaseAfterUpdateListener<PlannedCourse> {

    @Autowired
    PlannedNotificationService plannedNotificationService;

    @Override
    public void onApplicationEvent(AfterUpdateEvent<PlannedCourse> event) {
        plannedNotificationService.updateNotifications( event.getDomainModel() );
    }
}
