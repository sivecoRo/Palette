package ro.siveco.ael.lcms.domain.auth.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.auth.model.AelGroup;
import ro.siveco.ael.lcms.domain.auth.model.AelUser;
import ro.siveco.ael.lcms.domain.auth.model.MultiSelectDTO;
import ro.siveco.ael.lcms.domain.auth.service.AelGroupService;
import ro.siveco.ael.lcms.domain.profile.service.UserDetailsService;
import ro.siveco.ael.lcms.domain.tenant.model.Tenant;
import ro.siveco.ael.service.ImageService;
import ro.siveco.ael.web.controllers.WithUserBaseEntityController;
import ro.siveco.ram.event.listener.type.single.BaseAfterReadListener;
import ro.siveco.ram.event.type.single.AfterReadEvent;
import ro.siveco.ael.lcms.domain.profile.model.UserDetails;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by AndradaC on 9/1/2017.
 */
@Component
public class SetMultiselectGroupListForUserAfterReadListener extends BaseAfterReadListener<AelUser>{

    @Autowired
    private AelGroupService aelGroupService;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private ImageService imageService;

    @Override
    public void onApplicationEvent(AfterReadEvent<AelUser> event) {
        AelUser aelUser = event.getDomainModel();

        Tenant tenant = WithUserBaseEntityController.getUser().getTenant();
        List<AelGroup> aelGroups = new ArrayList<>();
        if (tenant == null) {
            aelGroups = aelGroupService.findAll();
        } else {
            aelGroups = aelGroupService.findAllByTenant(tenant);
        }
        List<MultiSelectDTO> multiSelectDTOs = new ArrayList<>();
        for (AelGroup aelGroup : aelGroups) {
            MultiSelectDTO multiSelectDTO = new MultiSelectDTO();
            multiSelectDTO.setId(aelGroup.getId());
            multiSelectDTO.setName(aelGroup.getName());
            multiSelectDTO.setStatus(0L);
            multiSelectDTOs.add(multiSelectDTO);
        }

        aelUser.setListAllGroups(multiSelectDTOs);

        Optional<UserDetails> userDetailsOptional = userDetailsService.getByAelUser(aelUser);
        if (userDetailsOptional.isPresent() && userDetailsOptional.get().getImage() != null) {
            aelUser.setAvatarSrc(imageService.imageSrcFromBytes(userDetailsOptional.get().getImage()));
        }
    }
}
