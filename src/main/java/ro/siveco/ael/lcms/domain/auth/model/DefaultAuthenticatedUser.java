package ro.siveco.ael.lcms.domain.auth.model;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Collection;
import java.util.Date;

/**
 * User: AlexandruVi
 * Date: 2017-02-21
 */
public class DefaultAuthenticatedUser extends org.springframework.security.core.userdetails.User implements
        AuthenticatedUser {

    private User user;
    private Collection<? extends GrantedAuthority> permissions;

    public DefaultAuthenticatedUser(User user, Collection<? extends GrantedAuthority> permissions) {
        super(user.getUsername(), user.getPassword(), permissions);
        this.user = user;
        this.permissions = permissions;
    }

    @Override
    public User getUser() {
        return user;
    }

    @Override
    public Collection<? extends GrantedAuthority> getPermissions() {
        return permissions;
    }

    @Override
    public boolean hasRole(String roleName) {
        if (permissions != null && !permissions.isEmpty()) {
            return permissions.contains(new SimpleGrantedAuthority(roleName));
        }
        return false;
    }

    @Override
    public boolean hasRole(Role role) {
        return hasRole(role.getName());
    }

    public boolean isEnabled() {
        return !getUser().getDisabled();
    }

    public boolean isAccountNonExpired() {
        Date expireDate = getUser().getExpireDate();
        if(expireDate !=null && expireDate.before(new Date()))
        {
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }

}
