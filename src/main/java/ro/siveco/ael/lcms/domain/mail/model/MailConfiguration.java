package ro.siveco.ael.lcms.domain.mail.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.*;
import ro.siveco.ael.domain.annotations.Tenantable;
import ro.siveco.ael.lcms.domain.tenant.model.Tenant;

import javax.persistence.CascadeType;
import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

//import ro.siveco.ael.model.annotations.Tenantable;
//import ro.siveco.ael.tenancy.Tenant;
//import ro.siveco.ams.domain.BaseEntity;
//import ro.siveco.ams.utils.annotation.EntityName;

/**
 * Created by IntelliJ IDEA.
 * User: laurentium
 * Date: 1/8/13
 * Time: 11:10 AM
 */
@Entity
@Table(name = "mail_cfg")
@SequenceGenerator(name = "mail_cfg_seq", sequenceName = "mail_cfg_seq")
@Tenantable
@FilterDefs({
        @FilterDef(name="tenantMailCfgFilter", parameters=@ParamDef( name="tenantId", type="long" ) ),
        @FilterDef(name="mailCfgFilter" )
})
@Filters( {
        @Filter(name="tenantMailCfgFilter", condition="tenant_id=:tenantId"),
        @Filter(name="mailCfgFilter", condition="tenant_id is null")
} )
//@EntityName("mailConfigurations")
public class MailConfiguration extends ro.siveco.ram.starter.model.base.BaseEntity {

    private Long id;
    private String host;
    private Integer port;
    private String username;
    private String password;
    private String defaultEncoding;
    private List<MailConfigurationAttributes> mailCfgAttributesList = new ArrayList<MailConfigurationAttributes>();

    @JsonIgnore
    private Tenant tenant;

    @Id
    @GeneratedValue(generator = "mail_cfg_seq", strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "host", length = 50, nullable = false)
    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    @Column(name = "port", nullable = true)
    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    @Column(name = "usename", length = 20, nullable = true)
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Column(name = "password", length = 20, nullable = true)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Column(name = "default_encoding", length = 10, nullable = true)
    public String getDefaultEncoding() {
        return defaultEncoding;
    }

    public void setDefaultEncoding(String defaultEncoding) {
        this.defaultEncoding = defaultEncoding;
    }

    @OneToMany(mappedBy = "parentMailConfiguration", cascade = { CascadeType.ALL }, fetch = FetchType.EAGER)
    @Cascade( {org.hibernate.annotations.CascadeType.DELETE_ORPHAN} )
    public List<MailConfigurationAttributes> getMailCfgAttributesList() {
        return mailCfgAttributesList;
    }

    public void setMailCfgAttributesList(List<MailConfigurationAttributes> mailCfgAttributesList) {
        this.mailCfgAttributesList = mailCfgAttributesList;
    }

    @ManyToOne
    @JoinColumn( name = "tenant_id", nullable = true )
    public Tenant getTenant() {
        return tenant;
    }

    public void setTenant(Tenant tenant) {
        this.tenant = tenant;
    }
}
