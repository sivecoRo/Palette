package ro.siveco.ael.lcms.domain.course.web;

import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.StringPath;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ro.siveco.ael.lcms.domain.auth.model.AelUser;
import ro.siveco.ael.lcms.domain.auth.model.QAelUser;
import ro.siveco.ael.lcms.domain.auth.model.User;
import ro.siveco.ael.lcms.domain.auth.service.AelGroupService;
import ro.siveco.ael.lcms.domain.auth.service.AelUserService;
import ro.siveco.ael.lcms.domain.course.model.PlannedCourse;
import ro.siveco.ael.lcms.domain.course.model.PlannedCourseStudent;
import ro.siveco.ael.lcms.domain.course.model.QPlannedCourse;
import ro.siveco.ael.lcms.domain.course.service.CourseService;
import ro.siveco.ael.lcms.domain.course.service.PlannedCourseFilterService;
import ro.siveco.ael.lcms.domain.course.service.PlannedCourseService;
import ro.siveco.ael.lcms.domain.course.service.PlannedCourseStudentService;
import ro.siveco.ael.lcms.domain.metadata.model.Interest;
import ro.siveco.ael.lcms.domain.metadata.model.Tag;
import ro.siveco.ael.lcms.domain.metadata.service.CommentService;
import ro.siveco.ael.lcms.domain.metadata.service.CountryService;
import ro.siveco.ael.lcms.domain.metadata.service.InterestService;
import ro.siveco.ael.lcms.domain.metadata.service.TagService;
import ro.siveco.ael.lcms.domain.notification.model.ApplicationUserMessageFormat;
import ro.siveco.ael.lcms.domain.notification.model.UserMessageType;
import ro.siveco.ael.lcms.domain.notification.service.NotificationManagerService;
import ro.siveco.ael.lcms.domain.organization.service.ComponentUserJobService;
import ro.siveco.ael.service.utils.DatesService;
import ro.siveco.ael.web.components.WebPath;
import ro.siveco.ael.web.controllers.WithUserBaseEntityController;
import ro.siveco.ram.controller.annotation.HtmlMapping;
import ro.siveco.ram.starter.controller.BaseEntityController;
import ro.siveco.ram.starter.service.BaseEntityService;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.*;

/**
 * Created by LiviuI on 4/19/2017.
 */
@Controller
@RequestMapping(WebPath.SESSION)
public class PlannedCourseController extends BaseEntityController<PlannedCourse> {

    private final CourseService courseService;

    @Autowired
    AelUserService aelUserService;

    @Autowired
    PlannedCourseService plannedCourseService;

    @Autowired
    ComponentUserJobService componentUserJobService;

    @Autowired
    private PlannedCourseFilterService plannedCourseFilterService;

    @Autowired
    private TagService tagService;

    @Autowired
    private CommentService commentService;

    private final AelGroupService aelGroupService;

    private final PlannedCourseStudentService plannedCourseStudentService;

    private final NotificationManagerService notificationManagerService;

    private final CountryService countryService;

    private final InterestService interestService;

    private final DatesService datesService;

    @Autowired
    public PlannedCourseController(BaseEntityService<PlannedCourse> restService, CourseService courseService,
                                   AelGroupService aelGroupService,
                                   PlannedCourseStudentService plannedCourseStudentService,
                                   NotificationManagerService notificationManagerService,
                                   CountryService countryService,
                                   InterestService interestService,
                                   DatesService datesService) {

        super(restService);
        this.courseService = courseService;
        this.aelGroupService = aelGroupService;
        this.plannedCourseStudentService = plannedCourseStudentService;
        this.notificationManagerService = notificationManagerService;
        this.countryService = countryService;
        this.interestService = interestService;
        this.datesService = datesService;
    }

    @Override
    protected Optional<BooleanExpression> buildFilter(MultiValueMap<String, String> parameters) {

        AelUser currentUser = aelUserService.findById(WithUserBaseEntityController.getUser().getId());
        QAelUser creatorPath = QPlannedCourse.plannedCourse.creator;
        QAelUser ownerPath = QPlannedCourse.plannedCourse.course.owner;
        StringPath countryCodePath = QPlannedCourse.plannedCourse.countryCode;
        String currentUserCountryCode = plannedCourseService.getUserCountryCode(currentUser);

        BooleanExpression filterExpression = QPlannedCourse.plannedCourse.id.isNotNull();
        BooleanExpression notExpiredFilterExpression = QPlannedCourse.plannedCourse.endDate.after(datesService.getYesterdayNight());
        BooleanExpression nullEndDateExpression = QPlannedCourse.plannedCourse.endDate.isNull();
        BooleanExpression validDateExpression = notExpiredFilterExpression.or(nullEndDateExpression);
        filterExpression = filterExpression.and(validDateExpression);
        //filterExpression = filterExpression.and(creatorPath.ne(currentUser));
        //filterExpression = filterExpression.and(ownerPath.ne(currentUser));
        if(currentUserCountryCode != null)
        {
            filterExpression = filterExpression.and(countryCodePath.isNotNull()).and(countryCodePath.eq(currentUserCountryCode));
        }

        BooleanExpression tagsExpression = plannedCourseService.getTagsFilterExpression(currentUser.getId(), parameters.get("tagFilter"));
        if (tagsExpression != null) {
            filterExpression = filterExpression.and(tagsExpression);
        }

        if(parameters != null && parameters.size() > 0)
        {
            return plannedCourseService.createFilterForParameters(parameters, filterExpression);
        }
        else {
            return Optional.of(filterExpression);
        }
    }

    @HtmlMapping(
            path = {"/"}
    )
    public String index(ModelMap model, @QuerydslPredicate Optional<Predicate> predicate, Pageable pageable, @RequestParam MultiValueMap<String, String> parameters) {

        Principal principal = SecurityContextHolder.getContext().getAuthentication();
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes())
                .getRequest();
        User currentUser = WithUserBaseEntityController.getUser();
        List<Interest> userInterestList = interestService.getI18NSelectedInterests(principal, request);
        if(userInterestList != null && userInterestList.size() > 0)
        {
            model.put("userTags", userInterestList);
        }
        else
        {
            model.put("userTags", new ArrayList<Tag>());
        }

        List<Interest> notSelectedInterests = interestService.getI18NNotSelectedInterests(principal,request);
        if(notSelectedInterests != null && notSelectedInterests.size() > 0)
        {
            model.put("otherTags", notSelectedInterests);
        }
        else
        {
            model.put("otherTags",new ArrayList<Tag>());
        }

        model.put("notSelectedInterests", notSelectedInterests);

        model.put("recommendedPlannedCourses", plannedCourseService.getRecommendedPlannedCourses((AelUser) WithUserBaseEntityController.getUser()));

        model.put("pastPlannedCourses", plannedCourseService.getPastPlannedCourses((AelUser) WithUserBaseEntityController.getUser()));

        model.put("relevantItemsNo", plannedCourseService.getRelevantPlannedCourses((AelUser) currentUser).size());

        return super.index(model, predicate, pageable, parameters);
    }


    @GetMapping("/{sessionId}/participants")
    public String addParticipants(Principal principal, @PathVariable(name = "sessionId") Long sessionId, Model model) {

        PlannedCourse plannedCourse = plannedCourseService.findOne(sessionId);
        model.addAttribute("sessionData", plannedCourse);
        return "/teacher/manage-participants";
    }

    @ModelAttribute("courseId")
    private String courseId(@RequestParam(value = "courseId",required = false) String courseId){
        return courseId;
    }


    @RequestMapping(value = "enrollToCourse", method = RequestMethod.POST)
    public String enrollToCourse(@RequestParam(name = "plannedCourseId") Long plannedCourseId,
                                 RedirectAttributes redirectAttributes) {

        Map<String, Object> modelMap = new HashMap<>();
        PlannedCourseService plannedCourseService = (PlannedCourseService) getRestService();

        PlannedCourse plannedCourse = plannedCourseService.findOneById(plannedCourseId);
        if (plannedCourse != null) {
            if (plannedCourse.getMaxNrOfStudents() != null) {
                List<PlannedCourseStudent> plannedCourseStudentList =
                        plannedCourseStudentService.findByPlannedCourseId(plannedCourseId);
                if (plannedCourseStudentList.size() >= plannedCourse.getMaxNrOfStudents()) {
                    throw new RuntimeException("plannedCourse.students.number.exceded.error");
                }
            }

            User currentUser = WithUserBaseEntityController.getUser();
            PlannedCourseStudent newStudent =
                    plannedCourseStudentService.createPlannedCourseStudent(currentUser, plannedCourse, false, false, false, false, false);
            if (newStudent == null) {
                throw new RuntimeException("student.could.not.be.enrolled.error");
            } else {

                notificationManagerService
                        .notifyUser((AelUser) currentUser, plannedCourse.getCourse().getCreator(), null,
                                new Object[]{currentUser.getDisplayName(), plannedCourse.getCourse().getName(),
                                        new Date()},
                                UserMessageType.COURSE_RELATED_MESSAGE,
                                ApplicationUserMessageFormat.COURSE_NEW_STUDENT_ENROLLMENT);

                notificationManagerService.notifyUser((AelUser) currentUser, (AelUser) currentUser, null,
                        new Object[]{plannedCourse.getCourse().getName(), new Date()},
                        UserMessageType.COURSE_RELATED_MESSAGE, ApplicationUserMessageFormat.COURSE_STUDENT_ENROLLED);
            }
            redirectAttributes.addFlashAttribute("enrollMsg", "student.enrolled.successfully");
            return WebPath.redirect("/my-courses/" + newStudent.getId());
        }

        return WebPath.redirect("/sessions/");
    }

    @PostMapping(value = "/addItemToFavorites/{id}")
    @ResponseBody
    public ResponseEntity<PlannedCourse> addItemToFavorites(@PathVariable(name = "id") Long id)
    {
        PlannedCourse plannedCourse = plannedCourseService.findOne(id);
        User currentUser = WithUserBaseEntityController.getUser();
        plannedCourseService.addCourseToFavorites(id, plannedCourse, currentUser);

        return new ResponseEntity<>(plannedCourse, HttpStatus.OK);
    }

    @PostMapping(value = "/addItemToFavorites/{id}/{userId}")
    @ResponseBody
    public ResponseEntity<PlannedCourse> addItemToFavorites(@PathVariable(name = "id") Long id, @PathVariable(name = "userId") Long userId)
    {
        PlannedCourse plannedCourse = plannedCourseService.findOne(id);
        User user = aelUserService.findById(userId);
        plannedCourseService.addCourseToFavorites(id, plannedCourse, user);

        return new ResponseEntity<>(plannedCourse, HttpStatus.OK);
    }



    @PostMapping(value = "/attendItem/{id}")
    @ResponseBody
    public ResponseEntity<PlannedCourse> attendItem(@PathVariable(name = "id") Long id)
    {
        return new ResponseEntity<>(plannedCourseService.attendItem(id), HttpStatus.OK);
    }

    @GetMapping(value = "/removeItem/{id}")
    public String removeItemFromList(@PathVariable(name = "id") Long id)
    {
        PlannedCourse plannedCourse = plannedCourseService.findOne(id);
        User currentUser = WithUserBaseEntityController.getUser();
        PlannedCourseStudent plannedCourseStudent = plannedCourseStudentService.findByPlanificationAndStundent(id, currentUser.getId());

        if(plannedCourseStudent == null)
        {
            plannedCourseStudent = plannedCourseStudentService.createPlannedCourseStudent(currentUser, plannedCourse, Boolean.FALSE, Boolean.TRUE, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE);
        }
        else
        {
            plannedCourseStudent.setAddedToFavorites(Boolean.FALSE);
            plannedCourseStudent.setRemoveItem(Boolean.TRUE);
            plannedCourseStudentService.update(plannedCourseStudent);
        }

        return WebPath.redirect("/sessions/");
    }

    @PostMapping(value = "/likeItem/{id}")
    @ResponseBody
    public ResponseEntity<PlannedCourse> likeItem(@PathVariable(name = "id") Long id, Model model)
    {
        PlannedCourse plannedCourse = plannedCourseService.findOne(id);
        User currentUser = WithUserBaseEntityController.getUser();
        PlannedCourseStudent plannedCourseStudent = plannedCourseStudentService.findByPlanificationAndStundent(id, currentUser.getId());

        if(plannedCourseStudent == null) {
            plannedCourseStudent = plannedCourseStudentService.createPlannedCourseStudent(currentUser, plannedCourse, Boolean.FALSE, Boolean.FALSE, Boolean.TRUE, Boolean.FALSE, Boolean.FALSE);
        }
        else
        {
            plannedCourseStudent.setLikeItem(Boolean.TRUE);
            plannedCourseStudent.setDislikeItem(Boolean.FALSE);
            plannedCourseStudentService.update(plannedCourseStudent);
        }

        Integer noLikes = plannedCourseStudentService.getItemLikesNumber(plannedCourse.getCourse());
        Integer noDislikes = plannedCourseStudentService.getItemDislikesNumber(plannedCourse.getCourse());
        plannedCourse.setItemLikeNumber(noLikes);
        plannedCourse.setItemDislikeNumber(noDislikes);

        return new ResponseEntity<PlannedCourse>(plannedCourse, HttpStatus.OK);
    }

    @PostMapping(value = "/dislikeItem/{id}")
    @ResponseBody
    public ResponseEntity<PlannedCourse> dislikeItem(@PathVariable(name = "id") Long id, Model model)
    {
        PlannedCourse plannedCourse = plannedCourseService.findOne(id);
        User currentUser = WithUserBaseEntityController.getUser();
        PlannedCourseStudent plannedCourseStudent = plannedCourseStudentService.findByPlanificationAndStundent(id, currentUser.getId());

        if(plannedCourseStudent == null)
        {
            plannedCourseStudent = plannedCourseStudentService.createPlannedCourseStudent(currentUser, plannedCourse, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE, Boolean.TRUE, Boolean.FALSE);
        }
        else
        {
            plannedCourseStudent.setDislikeItem(Boolean.TRUE);
            plannedCourseStudent.setLikeItem(Boolean.FALSE);
            plannedCourseStudentService.update(plannedCourseStudent);
        }

        Integer noLikes = plannedCourseStudentService.getItemLikesNumber(plannedCourse.getCourse());
        Integer noDislikes = plannedCourseStudentService.getItemDislikesNumber(plannedCourse.getCourse());
        plannedCourse.setItemLikeNumber(noLikes);
        plannedCourse.setItemDislikeNumber(noDislikes);

        return new ResponseEntity<PlannedCourse>(plannedCourse, HttpStatus.OK);
    }
    @RequestMapping(value = "/mail", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity sendMailToTeacher(
            @RequestParam("message") final String message,
            @RequestParam("plannedCourseId") final long plannedCourseId){
        plannedCourseService.sendMailToTeacher(plannedCourseId, message);
        return new ResponseEntity(HttpStatus.CREATED);
    }

    @RequestMapping(value ="/getComment", method = RequestMethod.POST)
    @ResponseBody
    public Map getCommentForCommentsTable( @RequestParam(name = "comment") String comment, @RequestParam(name = "pcId") String pcId) {

        Map<String, Object> modelMap = new HashMap<>();
        modelMap.put("success", plannedCourseService.addPlannedCourseComment(comment, pcId));

        return modelMap;
    }

    @RequestMapping(value = WebPath.DELETE_COMMENT, method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity deleteComment(@RequestParam("commentId") final long commentId){
        commentService.remove(commentId);
        return new ResponseEntity(HttpStatus.CREATED);
    }
}
