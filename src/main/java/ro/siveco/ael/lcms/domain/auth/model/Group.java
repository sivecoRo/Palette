package ro.siveco.ael.lcms.domain.auth.model;

import ro.siveco.ael.domain.model.core.Record;

import java.util.List;

/**
 * User: AlexandruVi
 * Date: 2017-02-20
 */
public interface Group extends Record<Long> {

    String getName();

    List<AelRole> getRoles();
}
