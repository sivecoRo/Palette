package ro.siveco.ael.lcms.domain.taxonomies.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.taxonomies.model.TaxonomyLevel;
import ro.siveco.ael.lcms.domain.taxonomies.service.TaxonomyLevelService;
import ro.siveco.ram.app.exception.AppException;
import ro.siveco.ram.event.listener.type.single.BaseBeforeCreateListener;
import ro.siveco.ram.event.type.single.BeforeCreateEvent;

/**
 * Created by AndradaC on 9/1/2017.
 */
@Component
public class ValidateTaxonomyLevelBeforeCreateListener extends BaseBeforeCreateListener<TaxonomyLevel>{

    @Autowired
    private TaxonomyLevelService taxonomyLevelService;

    @Override
    public void onApplicationEvent(BeforeCreateEvent<TaxonomyLevel> event) {
        TaxonomyLevel taxonomyLevel = event.getDomainModel();
        String validationResult = taxonomyLevelService.validateTaxonomyLevel(taxonomyLevel);
        if(!validationResult.isEmpty()){
            throw new AppException(validationResult);
        }
    }
}
