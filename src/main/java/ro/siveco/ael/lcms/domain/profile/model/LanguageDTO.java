package ro.siveco.ael.lcms.domain.profile.model;

import ro.siveco.ram.model.Model;
import ro.siveco.ram.starter.ui.config.PropertyViewDescriptor;
import ro.siveco.ram.starter.ui.config.ViewDescriptor;
import ro.siveco.ram.starter.ui.model.ViewRendererType;

import javax.persistence.Transient;
import java.io.Serializable;

@ViewDescriptor
public class LanguageDTO implements Model, Serializable {

    private String name;

    private String id;

    public String getName() {

        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

    public String getId() {

        return id;
    }

    public void setId(String id) {

        this.id = id;
    }

    @Override
    public String getObjectUID() {

        return getId();
    }

    @Override
    @Transient
    @PropertyViewDescriptor(
            hiddenOnGrid = true,
            rendererType = ViewRendererType.OBJECT_LABEL)
    public String getObjectLabel() {

        return getName();
    }
}
