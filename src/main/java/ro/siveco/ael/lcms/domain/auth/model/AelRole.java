package ro.siveco.ael.lcms.domain.auth.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.springframework.data.domain.Sort;
import ro.siveco.ael.lcms.domain.auth.view.Views;
import ro.siveco.ael.lcms.domain.tenant.model.Tenant;
import ro.siveco.ael.lcms.domain.tenant.model.TenantHolder;
import ro.siveco.ram.starter.ui.config.*;
import ro.siveco.ram.starter.ui.model.ActionType;
import ro.siveco.ram.starter.ui.model.FormInputType;
import ro.siveco.ram.starter.ui.model.IndexViewType;
import ro.siveco.ram.starter.ui.model.ViewRendererType;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "roles")
@SequenceGenerator(name = "SEQ_GEN", sequenceName = "roles_seq")
//@TableComment(comment = "Stores the system's security roles")
@ViewDescriptor(pageSize = 10, viewTypes = {IndexViewType.LIST},
        defaultViewType = IndexViewType.LIST,
        searchEnabled = true, defaultSearchProperty = "name",
        sort = {@Sorter(propertyName = "name", direction = Sort.Direction.ASC)},
        defaultActions = {
                @ActionDescriptor(
                        type = ActionType.OPEN_CREATE_FORM,
                        fixedAction = true
                ),         @ActionDescriptor(
                type = ActionType.OPEN_EDIT_FORM,
                mainAction = true
        ),         @ActionDescriptor(
                type = ActionType.DELETE,
                mainAction = true,
                requiresConfirmation = true
        ),         @ActionDescriptor(
                type = ActionType.CREATE,
                forwardFor = {"{modelName}-OPEN_CREATE_FORM"},
                availableOnlyInForm = true
        ),         @ActionDescriptor(
                type = ActionType.UPDATE,
                forwardFor = {"{modelName}-OPEN_EDIT_FORM"},
                availableOnlyInForm = true
        ),         @ActionDescriptor(
                type = ActionType.LIST,
                availableInForm = true,
                availableInPresentation = true,
                backToFor = {"{modelName}-OPEN_CREATE_FORM", "{modelName}-CREATE", "{modelName}-OPEN_PRESENTATION", "{modelName}-OPEN_EDIT_FORM", "{modelName}-UPDATE", "{modelName}-DELETE"}
        )},
        otherActions = {
                @ActionDescriptor(actionId = "associateFunctionsToRoleId", type = ActionType.OPEN_EDIT_FORM,
                        view = Views.AssociateFunctionsToRoleView.class)
        })
@ViewDescriptor(
        view = Views.AssociateFunctionsToRoleView.class,
        otherActions = {
                @ActionDescriptor(actionId = "associateFunctionsToRoleId", type = ActionType.OPEN_EDIT_FORM,
                        view = Views.AssociateFunctionsToRoleView.class),
                @ActionDescriptor(type = ActionType.UPDATE, forwardFor = "associateGroupsToUserId",
                        availableOnlyInForm = true),
                @ActionDescriptor(type = ActionType.LIST, availableInForm = true, inheritViewFromWrapper = false,
                        view = ro.siveco.ram.starter.ui.model.Views.__default.class, backToFor = {
                        "{modelName}-OPEN_EDIT_FORM",
                        "{modelName}-UPDATE"
                })}
)
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class AelRole extends ro.siveco.ram.starter.model.base.BaseEntity implements Role, TenantHolder {

    //@ColumnComment(comment = "The role's display name")
    private String name;

    private Tenant tenant;

    private List<Group> groups = new ArrayList<>();

    private List<AelFunction> functions = new ArrayList<>();

    //@ColumnComment(comment = "The role's description")
    private String description;

    //@ColumnComment(comment = "Whether this role is a system built-in role and can not be modified")
    private Boolean readOnly = Boolean.FALSE;

    //@ColumnComment(comment = "Whether this role was created by a system administrator")
    private Boolean imported = Boolean.FALSE;


    //transient fields for form
    private List<MultiSelectDTO> listAllFunction = new ArrayList<>();

    @Id
    @PropertyViewDescriptor(hidden = true)
    @GeneratedValue(generator = "roles_seq", strategy = GenerationType.SEQUENCE)
    public Long getId() {
        return super.getId();
    }

    @PropertyViewDescriptor(
            hiddenOnList = true,
            order = 1,
            inputType = FormInputType.TEXT, rendererType = ViewRendererType.STRING,
            orderInForm = 1)
    @Column(name = "name", nullable = false, length = 500)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @PropertyViewDescriptor(hidden = true)
    @Column(name = "imported", nullable = false)
    public Boolean getImported() {
        return imported;
    }

    public void setImported(Boolean imported) {
        this.imported = imported;
    }

    @PropertyViewDescriptor(hidden = true)
    @Column(name = "readonly", nullable = false)
    public Boolean getReadOnly() {
        return readOnly;
    }

    public void setReadOnly(Boolean readOnly) {
        this.readOnly = readOnly;
    }

    @PropertyViewDescriptor(
            hiddenOnList = true, order = 2,
            inputType = FormInputType.TEXT, rendererType = ViewRendererType.STRING, required = false,
            orderInForm = 2
    )
    @Column(name = "description", length = 2000)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @ManyToMany(
            targetEntity = AelFunction.class,
            cascade = {CascadeType.ALL}
    )
    @JoinTable(
            name = "role_function",
            joinColumns = {@JoinColumn(name = "role_id")},
            inverseJoinColumns = {@JoinColumn(name = "function_id")},
            uniqueConstraints = {@UniqueConstraint(columnNames = {"function_id", "role_id"})}
    )
    @JsonView(Views.AssociateFunctionsToRoleView.class)
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = AelFunction.class)
    @PropertyViewDescriptor( view = ro.siveco.ram.starter.ui.model.Views.__default.class, hidden = true)
    @PropertyViewDescriptor(
            hiddenOnCard = true, hiddenOnGrid = true,
            view = Views.AssociateFunctionsToRoleView.class,
            rendererType = ViewRendererType.MODEL, inputType = FormInputType.LIST_OF_VALUES_MULTI_SELECT, orderInForm = 1)
    public List<AelFunction> getFunctions() {
        return functions;
    }

    public void setFunctions(List<AelFunction> functions) {
        this.functions = functions;
    }

    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "tenant_id", nullable = true)
    @PropertyViewDescriptor(hidden = true)
    public Tenant getTenant() {
        return tenant;
    }

    public void setTenant(Tenant tenant) {
        this.tenant = tenant;
    }

    @Transient
    @PropertyViewDescriptor(hiddenOnList = true, hiddenOnForm = true, hiddenOnPresentation = true)
    public List<MultiSelectDTO> getListAllFunction() {
        return listAllFunction;
    }

    public void setListAllFunction(List<MultiSelectDTO> listAllFunction) {
        this.listAllFunction = listAllFunction;
    }

    @Override
    public String toString() {
        return this.getName();
    }

    @Transient
    @PropertyViewDescriptor(
            order = -2,
            hiddenOnForm = true,
            hiddenOnGrid = true,
            displayLabelInPresentation = false,
            rendererType = ViewRendererType.OBJECT_LABEL,
            clickAction = {}
    )
    public String getObjectLabel() {

        return getName();
    }
}
