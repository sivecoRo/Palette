package ro.siveco.ael.lcms.domain.course.model;

/**
 * Created by JohnB on 26.07.2017.
 */
public enum CourseType
{
	COURSE,
	ACTIVITY,
	SERVICE,
	MEETING
}
