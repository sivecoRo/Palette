package ro.siveco.ael.lcms.domain.profile.model;

/**
 * Created with IntelliJ IDEA.
 * User: IulianB
 * Date: 6/25/13
 * Time: 8:26 PM
 * To change this template use File | Settings | File Templates.
 */
public enum Gender {

    MALE("male"),
    FEMALE("female"),
    UNDEFINED("undefined");

    private String name;

    Gender(String name) {
        this.name = name;
    }


    public String getName() {
        return name;
    }

}
