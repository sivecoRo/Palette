package ro.siveco.ael.lcms.domain.auth.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.auth.model.AelGroup;
import ro.siveco.ael.lcms.domain.auth.model.AelRole;
import ro.siveco.ael.lcms.domain.auth.model.AelUser;
import ro.siveco.ael.lcms.domain.auth.model.User;
import ro.siveco.ael.lcms.domain.auth.service.AelGroupService;
import ro.siveco.ael.lcms.domain.auth.service.AelUserService;
import ro.siveco.ram.app.exception.AppException;
import ro.siveco.ram.event.listener.type.single.BaseBeforeDeleteListener;
import ro.siveco.ram.event.type.single.BeforeDeleteEvent;

import java.util.List;

/**
 * Created by georgianaa on 1/10/2018.
 */
@Component
public class ValidateGroupBeforeDeleteListener extends BaseBeforeDeleteListener<AelGroup> {

    @Autowired
    private AelUserService aelUserService;

    @Autowired
    private AelGroupService aelGroupService;

    @Override
    public void onApplicationEvent(BeforeDeleteEvent<AelGroup> event) {
        AelGroup aelGroup = event.getDomainModel();
        AelGroup dbGroup= aelGroupService.findById(aelGroup.getId());

        if(dbGroup == null) {
            throw new AppException("error.group.not.found");
        } else {
            List<AelRole> aelRoles = dbGroup.getRoles();
            List<AelUser> aelUsers = aelUserService.getUsersByGroup(dbGroup.getName());
            if (aelRoles != null && aelRoles.size() > 0) {
                throw new AppException("error.group.cannot.be.deleted.roles");
            }
            if(aelUsers != null && aelUsers.size() > 0) {
                throw new AppException("error.group.cannot.be.deleted.users");
            }
        }
    }
}
