package ro.siveco.ael.lcms.domain.taxonomies.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import ro.siveco.ael.lcms.domain.taxonomies.model.Competence;
import ro.siveco.ael.lcms.domain.taxonomies.service.CompetenceService;
import ro.siveco.ram.event.listener.type.single.BaseBeforeCreateListener;
import ro.siveco.ram.event.type.single.BeforeCreateEvent;

/**
 * Created by IuliaP on 26.10.2017.
 */
@Component
@Order(value = 2)
public class SetPropertiesForCompetenceBeforeCreateListener extends BaseBeforeCreateListener<Competence> {

    @Autowired
    private CompetenceService competenceService;

    @Override
    public void onApplicationEvent(BeforeCreateEvent<Competence> event) {
        competenceService.decorateCompetence(event.getDomainModel());
    }
}
