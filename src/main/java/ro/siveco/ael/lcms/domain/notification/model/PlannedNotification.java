package ro.siveco.ael.lcms.domain.notification.model;

import ro.siveco.ael.lcms.domain.course.model.PlannedCourse;
import ro.siveco.ram.starter.model.base.BaseEntity;
import ro.siveco.ram.starter.ui.config.PropertyViewDescriptor;
import ro.siveco.ram.starter.ui.config.ViewDescriptor;
import ro.siveco.ram.starter.ui.model.FormInputType;
import ro.siveco.ram.starter.ui.model.IndexViewType;
import ro.siveco.ram.starter.ui.model.ViewRendererType;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created by IuliaP on 28.09.2017.
 */
@Entity
@Table(name = "planned_notifications")
@SequenceGenerator(name = "planned_notifications_seq", sequenceName = "planned_notifications_seq")
@ViewDescriptor(pageSize = 10, viewTypes = {IndexViewType.GRID}, defaultSearchProperty = "userMessageFormat")
public class PlannedNotification extends BaseEntity {

    private ApplicationUserMessageFormat userMessageFormat;

    private Long entityId;

    private NotificationEntityType notificationEntityType = NotificationEntityType.PLANNED_COURSE;

    private PlannedCourse plannedCourse;

    private Boolean sendByEvent = Boolean.FALSE;

    private NotificationEventType notificationEventType;

    private Integer daysNo;

    private Integer hoursNo;

    private Integer minutesNo;

    private String plannedCourseName;

    private Date sendDate;

    @Id
    @GeneratedValue(generator = "message_templates_seq", strategy = GenerationType.SEQUENCE)
    public Long getId() {

        return super.getId();
    }

    /*@ManyToOne
    @JoinColumn(name = "user_message_format_id")*/
    @Column(name = "user_message_format_id")
    @PropertyViewDescriptor(order = 1,
            rendererType = ViewRendererType.ENUM,
            inputType = FormInputType.SELECT, orderInForm = 1)
    @NotNull
    public ApplicationUserMessageFormat getUserMessageFormat() {

        return userMessageFormat;
    }

    public void setUserMessageFormat(ApplicationUserMessageFormat userMessageFormat) {

        this.userMessageFormat = userMessageFormat;
    }

    @Column(name = "entity_id")
    @NotNull
    @PropertyViewDescriptor(hidden = true)
    public Long getEntityId() {

        return entityId;
    }

    public void setEntityId(Long entityId) {

        this.entityId = entityId;
    }

    @Column(name = "entity_type")
    @NotNull
    @PropertyViewDescriptor(order = 2, orderInForm = 2)
    public NotificationEntityType getNotificationEntityType() {

        return notificationEntityType;
    }

    public void setNotificationEntityType(
            NotificationEntityType notificationEntityType) {

        this.notificationEntityType = notificationEntityType;
    }

    @Transient
    @PropertyViewDescriptor(hiddenOnGrid = true, orderInForm = 3)
    public PlannedCourse getPlannedCourse() {

        return plannedCourse;
    }

    public void setPlannedCourse(PlannedCourse plannedCourse) {

        this.plannedCourse = plannedCourse;
    }

    @Column(name = "send_by_event")
    @NotNull
    @PropertyViewDescriptor(hiddenOnGrid = true, orderInForm = 4)
    public Boolean getSendByEvent() {

        return sendByEvent;
    }

    public void setSendByEvent(Boolean sendByEvent) {

        this.sendByEvent = sendByEvent;
    }

    @Column(name = "notification_event_type")
    @PropertyViewDescriptor(hiddenOnGrid = true, order = 3, orderInForm = 5, required = false)
    public NotificationEventType getNotificationEventType() {

        return notificationEventType;
    }

    public void setNotificationEventType(
            NotificationEventType notificationEventType) {

        this.notificationEventType = notificationEventType;
    }

    @Column(name = "days_no")
    @Min(value = 1)
    @Max(value = 365)
    @PropertyViewDescriptor(hiddenOnGrid = true, orderInForm = 6, minSize = 1, maxSize = 1000)
    public Integer getDaysNo() {

        return daysNo;
    }

    public void setDaysNo(Integer daysNo) {

        this.daysNo = daysNo;
    }

    @Column(name = "hours_no")
    @Min(value = 1)
    @Max(value = 24)
    @PropertyViewDescriptor(hiddenOnGrid = true, orderInForm = 7, required = false, minSize = 1, maxSize = 24)
    public Integer getHoursNo() {

        return hoursNo;
    }

    public void setHoursNo(Integer hoursNo) {

        this.hoursNo = hoursNo;
    }

    @Column(name = "minutes_no")
    @Min(value = 1)
    @Max(value = 59)
    @PropertyViewDescriptor(hiddenOnGrid = true, orderInForm = 8, required = false, minSize = 1, maxSize = 59)
    public Integer getMinutesNo() {

        return minutesNo;
    }

    public void setMinutesNo(Integer minutesNo) {

        this.minutesNo = minutesNo;
    }

    @Transient
    @PropertyViewDescriptor(order = 3, hiddenOnForm = true)
    public String getPlannedCourseName() {
        return plannedCourseName;
    }

    public void setPlannedCourseName(String plannedCourseName) {

        this.plannedCourseName = plannedCourseName;
    }

    @Column(name = "send_date")
    @PropertyViewDescriptor(order = 4, orderInForm = 9, inputType = FormInputType.DATE, required = false)
    public Date getSendDate() {

        return sendDate;
    }

    public void setSendDate(Date sDate) {
        this.sendDate = sDate;
    }
}
