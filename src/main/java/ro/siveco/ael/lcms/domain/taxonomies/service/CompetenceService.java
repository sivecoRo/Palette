package ro.siveco.ael.lcms.domain.taxonomies.service;

import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.BooleanExpression;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ro.siveco.ael.lcms.domain.auth.service.UserCompetenceService;
import ro.siveco.ael.lcms.domain.course.service.CourseCompetenceService;
import ro.siveco.ael.lcms.domain.organization.service.JobTitleCompetenceService;
import ro.siveco.ael.lcms.domain.taxonomies.model.Competence;
import ro.siveco.ael.lcms.domain.taxonomies.model.QCompetence;
import ro.siveco.ael.lcms.domain.tenant.model.Tenant;
import ro.siveco.ael.lcms.repository.CompetenceRepository;
import ro.siveco.ael.web.controllers.WithUserBaseEntityController;
import ro.siveco.ram.security.annotation.CanListEntity;
import ro.siveco.ram.service.TreeEntityService;
import ro.siveco.ram.starter.service.BaseEntityService;

/**
 * Created by JohnB on 16/06/2017.
 */

@Service
public class CompetenceService extends BaseEntityService<Competence> implements TreeEntityService<Competence, Long> {

    @Autowired
    CourseCompetenceService courseCompetenceService;

    @Autowired
    UserCompetenceService userCompetenceService;

    @Autowired
    JobTitleCompetenceService jobTitleCompetenceService;

    @Autowired
    public CompetenceService(CompetenceRepository resourceRepository) {

        super(Competence.class, resourceRepository);
    }

    @CanListEntity
    final public Page<Competence> paginate(Pageable pageable, Competence parent) {

        BooleanExpression parentPredicate = QCompetence.competence.parent.eq(parent);
        return paginate(parentPredicate, pageable);
    }

    @CanListEntity
    final public Page<Competence> paginate(Predicate predicate, Pageable pageable, Competence parent) {

        BooleanExpression parentPredicate = QCompetence.competence.parent.eq(parent);
        return paginate(parentPredicate.and(predicate), pageable);
    }

    public Competence findByLabelAndTenant(String label, Tenant tenant) {

        return ((CompetenceRepository) getResourceRepository()).findByLabelAndTenant(label, tenant);
    }

    public Competence findById(Long id) {

        return ((CompetenceRepository) getResourceRepository()).findById(id);
    }

    public String validateCompetence(Competence competence) {

        Boolean isNewCompetence = competence.getId() == null;
        Competence competenceWithSameLabel = findByLabelAndTenant(competence.getLabel(), competence.getTenant());
        if (competenceWithSameLabel != null) {
            if (isNewCompetence || competence.getId().compareTo(competenceWithSameLabel.getId()) != 0) {
                return "error.competence.update.duplicateEntry";
            }
        }

        return null;
    }

    public String connexionsExist(Long competenceId) {

        Competence competence = ((CompetenceRepository) getResourceRepository()).findOne(competenceId);
        if (courseCompetenceService.competenceExists(competence)) {
            return "error.competence.delete.courseCompetenceAssociation";
        }
        if (userCompetenceService.competenceExists(competence)) {
            return "error.competence.delete.userCompetenceAssociation";
        }
        if (jobTitleCompetenceService.competenceExists(competence)) {
            return "error.competence.delete.jobTitleCompetenceAssociation";
        }
        return null;
    }

    @Override
    public Page<Competence> getChildrenPage(Predicate predicate, Pageable pageable, Competence parent) {

        if (predicate != null) {
            Page<Competence> page = paginate(predicate, pageable, parent);
            page.forEach(this::triggerAfterRead);
            return page;
        }
        Page<Competence> page = paginate(pageable, parent);
        page.forEach(this::triggerAfterRead);
        return page;
    }

    @Override
    public Page<Competence> getChildrenPage(Pageable pageable, Competence parent) {

        return getChildrenPage(null, pageable, parent);
    }

    public void decorateCompetence(Competence competence) {
        if( competence != null ) {
            competence.setTenant(WithUserBaseEntityController.getUser().getTenant());
        }
    }
}