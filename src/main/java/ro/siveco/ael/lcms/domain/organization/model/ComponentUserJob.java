package ro.siveco.ael.lcms.domain.organization.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.Filters;
import org.hibernate.annotations.ParamDef;
import ro.siveco.ael.lcms.domain.auth.model.AelUser;
import ro.siveco.ram.starter.ui.config.PropertyViewDescriptor;
import ro.siveco.ram.starter.ui.config.ViewDescriptor;
import ro.siveco.ram.starter.ui.model.FormInputType;
import ro.siveco.ram.starter.ui.model.IndexViewType;
import ro.siveco.ram.starter.ui.model.ViewRendererType;

import javax.persistence.*;

@Entity
@Table(name = "component_user_job",
        uniqueConstraints = {@UniqueConstraint(columnNames = {"component_id", "user_id", "job_title_id"})})
@SequenceGenerator(name = "component_user_job_seq", sequenceName = "component_user_job_seq")
@ViewDescriptor(pageSize = 10, viewTypes = {IndexViewType.LIST},
        defaultViewType = IndexViewType.LIST)
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(
        name = "discriminator",
        discriminatorType = DiscriminatorType.STRING
)
@DiscriminatorValue(value = "C")
@FilterDef(name = "tenantComponentUserJobFilter", parameters = @ParamDef(name = "tenantId", type = "long"))
@Filters({
        @Filter(name = "tenantComponentUserJobFilter",
                condition = "exists (select 1 from users u where u.id=user_id and u.tenant_id=:tenantId)")
})
public class ComponentUserJob extends ro.siveco.ram.starter.model.base.BaseEntity {
    private Long id;

    private Component component;

    private AelUser user;

    private JobTitle jobTitle;

    private Boolean active = Boolean.TRUE;

    private Boolean imported = Boolean.FALSE;

    @Id
    @PropertyViewDescriptor(hidden = true, required = false, hiddenOnForm = true)
    @GeneratedValue(generator = "component_user_job_seq", strategy = GenerationType.AUTO)

    public Long getId() {
        return id;
    }

    public void setId(Long _id) {
        id = _id;
    }

    @ManyToOne
    @PropertyViewDescriptor(inputType = FormInputType.LIST_OF_VALUES, rendererType = ViewRendererType.MODEL,
            editInline = false, required = true)
    @JoinColumn(name = "component_id", nullable = false)
    public Component getComponent() {
        return component;
    }

    public void setComponent(Component component) {
        this.component = component;
    }

    @ManyToOne
    @JsonIgnore
    @PropertyViewDescriptor(hidden = true, required = false, hiddenOnForm = true)
    @JoinColumn(name = "user_id", nullable = false)
    public AelUser getUser() {
        return user;
    }

    public void setUser(AelUser _user) {
        user = _user;
    }

    @ManyToOne
    @JoinColumn(name = "job_title_id", nullable = false)
    @PropertyViewDescriptor(inputType = FormInputType.LIST_OF_VALUES, rendererType = ViewRendererType.MODEL,
            editInline = false, required = true)
    public JobTitle getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(JobTitle jobTitle) {
        this.jobTitle = jobTitle;
    }


    @Column(nullable = false)
    @PropertyViewDescriptor(hidden = true, required = false, hiddenOnForm = true)
    public Boolean getImported() {
        return imported;
    }

    public void setImported(Boolean imported) {
        this.imported = imported;
    }

    @Column(name = "active", nullable = false)
    @PropertyViewDescriptor(hidden = true, required = false, hiddenOnForm = true)
    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

}
