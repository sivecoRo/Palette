package ro.siveco.ael.lcms.domain.materials.model;


import java.util.Comparator;

/**
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: Siveco Romania SA</p>
 * User: LiviuI
 * Date: 12/12/13
 * Time: 5:57 PM
 */
public class SliderValueUtils
{

	public static Comparator<SurveyItemValue> VALUE_ASC = new Comparator<SurveyItemValue>(){

		public int compare( SurveyItemValue siv1, SurveyItemValue siv2 )
		{
			long v1 = Long.parseLong( siv1.getDefinition() );
			long v2 = Long.parseLong( siv2.getDefinition() );
			return (v1 == v2)? 0:( (v1 < v2)? -1 : 1);
		}
	};

}
