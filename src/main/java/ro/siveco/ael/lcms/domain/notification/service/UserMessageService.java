package ro.siveco.ael.lcms.domain.notification.service;

import com.querydsl.core.types.Predicate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.social.security.SocialAuthenticationToken;
import org.springframework.stereotype.Service;
import ro.siveco.ael.facebook.model.SocialAuthenticatedUser;
import ro.siveco.ael.lcms.domain.auth.model.AelUser;
import ro.siveco.ael.lcms.domain.auth.model.AuthenticatedUser;
import ro.siveco.ael.lcms.domain.auth.model.User;
import ro.siveco.ael.lcms.domain.auth.service.AelUserService;
import ro.siveco.ael.lcms.domain.notification.model.*;
import ro.siveco.ael.lcms.domain.preference.model.Preference;
import ro.siveco.ael.lcms.domain.preference.model.Preferences;
import ro.siveco.ael.lcms.domain.preference.service.PreferencesService;
import ro.siveco.ael.lcms.repository.AelUserRepository;
import ro.siveco.ael.lcms.repository.UserMessageRepository;
import ro.siveco.ael.service.utils.DatesService;
import ro.siveco.ram.app.exception.AppException;
import ro.siveco.ram.starter.service.BaseEntityService;

import java.beans.*;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.security.Principal;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;


@Service
public class UserMessageService extends BaseEntityService<UserMessage> {

    private MessageSource messageSource;
    private AelUserRepository aelUserRepository;

    private MessageTemplateService messageTemplateService;

    private PreferencesService preferencesService;

    private final DatesService datesService;

    private final AelUserService aelUserService;

    @Autowired
    public UserMessageService(UserMessageRepository userMessageRepository, MessageSource messageSource,
                              AelUserRepository aelUserRepository,
                              MessageTemplateService messageTemplateService,
                              PreferencesService preferencesService,
                              DatesService datesService,
                              AelUserService aelUserService
    ) {
        super(UserMessage.class, userMessageRepository);
        this.messageSource = messageSource;
        this.aelUserRepository = aelUserRepository;
        this.messageTemplateService = messageTemplateService;
        this.preferencesService = preferencesService;
        this.datesService = datesService;
        this.aelUserService = aelUserService;
    }

    public List<UserMessageModel> getAllReceivedMessagesByUserId(Long userId, boolean translate) {
        List<UserMessage> messages =
                ((UserMessageRepository) getResourceRepository())
                        .getAllReceivedMessagesByUserIdAndType(userId, UserMessageType.USER_MESSAGE);

        if (translate) {
            for (int i = 0; i < messages.size(); i++) {
                UserMessage message = messages.get(i);
                markMessageAsReceived(message);

                if (message.getUserMessageType() != UserMessageType.USER_MESSAGE) {
                    message = translateMessage(message, aelUserService.getLocaleForUser((AelUser) aelUserRepository.findOneById(userId)),
                            DateFormat.getInstance());
                    messages.set(i, message);
                }
            }
        }

        return extractUsefullData(messages);
    }

    public List<UserMessageModel> getAllSentMessagesByUserId(Long userId) {
        List<UserMessage> messages =
                ((UserMessageRepository) getResourceRepository())
                        .getAllSentMessagesByUserIdAndType(userId, UserMessageType.USER_MESSAGE);

        return extractUsefullData(messages);
    }

    public List<UserMessageModel> getAllDraftMessagesByUserId(Long userId) {
        List<UserMessage> messages =
                ((UserMessageRepository) getResourceRepository())
                        .getAllDraftMessagesByUserIdAndType(userId, UserMessageType.USER_MESSAGE);

        return extractUsefullData(messages);
    }

    public int getUnreadMessages(List<UserMessageModel> messages) {

        int count = 0;
        for (int i = 0; i < messages.size(); i++) {
            if (messages.get(i).getReadDate() == null) {
                count++;
            }
        }
        return count;
    }


    public UserMessageModel showMessage(Long messageId, Locale locale) {
        UserMessage message = ((UserMessageRepository) getResourceRepository()).get(messageId);

        markMessageAsReceived(message);

        if (message.getUserMessageType() != UserMessageType.USER_MESSAGE) {
            message = translateMessage(message, locale, DateFormat.getInstance());
        }

        List<UserMessage> messages = new ArrayList<UserMessage>();
        messages.add(message);
        List<UserMessageModel> models = extractUsefullData(messages);

        return models.get(0);
    }

    public void markMessageAsReceived(UserMessage userMessage) {
        if (userMessage.getReceivedDate() == null) {
            userMessage.setReceivedDate(new Date());

           getResourceRepository().save(userMessage);
        }
    }

    public UserMessage translateMessage(UserMessage userMessage, Locale locale, DateFormat dateFormat) {
        UserMessage tempUserMessage = new UserMessage(userMessage);

        Object[] subjectArgs = null;

        if (userMessage.getSubjectArgs() != null) {
            XMLDecoder subjectArgsXmlDecoder;
            try {
                subjectArgsXmlDecoder =
                        new XMLDecoder(new ByteArrayInputStream(userMessage.getSubjectArgs().getBytes("UTF-8")));
            } catch (UnsupportedEncodingException e) {
                subjectArgsXmlDecoder =
                        new XMLDecoder(new ByteArrayInputStream(userMessage.getSubjectArgs().getBytes()));
            }
            subjectArgs = (Object[]) subjectArgsXmlDecoder.readObject();
        }

        XMLDecoder messageBodyArgsXmlDecoder;
        try {
            messageBodyArgsXmlDecoder =
                    new XMLDecoder(new ByteArrayInputStream(userMessage.getMessageBodyArgs().getBytes("UTF-8")));
        } catch (UnsupportedEncodingException e) {
            messageBodyArgsXmlDecoder =
                    new XMLDecoder(new ByteArrayInputStream(userMessage.getMessageBodyArgs().getBytes()));
        }

        Object[] messageBodyArgs = (Object[]) messageBodyArgsXmlDecoder.readObject();

        if (messageBodyArgs != null) {
            for (int k = 0; k < messageBodyArgs.length; k++) {
                if (messageBodyArgs[k] instanceof java.util.Date) {
                    messageBodyArgs[k] = datesService.formatShortDate((Date)messageBodyArgs[k], datesService.getTimezoneId(userMessage.getTo()), locale );
                }
            }
        }
        tempUserMessage.setSubject(messageSource.getMessage(tempUserMessage.getSubject(), subjectArgs, locale));
        tempUserMessage
                .setMessageBody(messageSource.getMessage(tempUserMessage.getMessageBody(), messageBodyArgs, locale));
        return tempUserMessage;
    }

    public UserMessage translateMessageWithTemplate(UserMessage userMessage,
                                                    ApplicationUserMessageFormat userMessageFormat,
                                                    Object[] subjectArgs,
                                                    Object[] messageBodyArgs,
                                                    String entityId, String serverContextPath, User to,
                                                    Locale locale) {

        UserMessage tempUserMessage = new UserMessage(userMessage);
        if (messageBodyArgs != null) {
            for (int k = 0; k < messageBodyArgs.length; k++) {
                if (messageBodyArgs[k] instanceof java.util.Date) {
                    messageBodyArgs[k] = datesService.formatShortDate((Date)messageBodyArgs[k], datesService.getTimezoneId(userMessage.getTo()), locale );
                }
            }
        }
        String body = messageSource.getMessage(tempUserMessage.getMessageBody(), messageBodyArgs, locale);
        if (existsTemplateForUserMessageFormat(userMessage.getTo(), userMessageFormat, locale)) {
            body = getMailBodyFromTemplateForUserMessageFormat(userMessageFormat, messageBodyArgs, entityId,
                    serverContextPath, to, locale);
        }
        tempUserMessage.setSubject(messageSource.getMessage(tempUserMessage.getSubject(), subjectArgs, locale));
        tempUserMessage.setMessageBody(body);
        return tempUserMessage;
    }

    public List<UserMessageModel> extractUsefullData(List<UserMessage> userMessages) {
        List<UserMessageModel> modelList = new ArrayList<UserMessageModel>();

        if (userMessages != null && !userMessages.isEmpty()) {
            for (int k = 0; k < userMessages.size(); k++) {
                UserMessage um = userMessages.get(k);
                UserMessageModel model = new UserMessageModel();
                Locale locale = aelUserService.getLocaleForUser(um.getTo());
                model.setFromUser(um.getFrom());
                model.setId(um.getId());
                model.setSubject(um.getSubject() != null && !um.getSubject().equals("") ? um.getSubject() : null);
                model.setMessageBody(um.getMessageBody());
                model.setReceivedDate(um.getReceivedDate());
                model.setSentDate(um.getSentDate());
                model.setReadDate(um.getReadDate());
                model.setFormattedSentDate(datesService.formatShortDateTime(um.getSentDate(), datesService.getTimezoneId(um.getTo()), locale));

                if (um.getFrom() != null) {
                    model.setFrom(um.getFrom().getFirstName() + " " +
                            um.getFrom().getLastName());
                    model.setFromUsername(um.getFrom().getUsername());
                }
                if (um.getTo() != null) {
                    model.setTo(um.getTo().getFirstName() + " " +
                            um.getTo().getLastName());
                    model.setToUsername(um.getTo().getUsername());
                }

                modelList.add(model);
            }
        }

        return modelList;
    }

    public Long sendMail(UserMessageModel form, String username) {

        UserMessage userMessage;

        if (form.getId() != null && !form.getIsReply()) {
            userMessage = getResourceRepository().findOne(form.getId());
        } else {
            userMessage = new UserMessage();
        }

        Optional<User> userFrom = aelUserRepository.findOneByUsername(username);
        Optional<User> userTo = aelUserRepository.findOneByUsername(form.getTo());

        if (userTo == null || userTo.equals(Optional.empty())) {
            return 1L;
        }

        userMessage.setFrom(aelUserRepository.findOne(userFrom.get().getId()));
        userMessage.setTo(aelUserRepository.findOne(userTo.get().getId()));
        updateUserMessageInformation(form, userMessage, true);

        return 0L;
    }

    private void updateUserMessageInformation(UserMessageModel form, UserMessage userMessage,
                                              boolean sent) {

        userMessage.setSubject(form.getSubject() != null && !form.getSubject().equals("") ? form.getSubject() : null);
        userMessage.setMessageBody(form.getMessageBody());
        userMessage.setSent(sent);
        userMessage.setUserMessageType(UserMessageType.USER_MESSAGE);
        userMessage.setSentDate(new Date());

        if (form.getIsReply()) {
            userMessage.setSource(getResourceRepository().findOne(form.getId()));
        }

        getResourceRepository().save(userMessage);
    }

    public Long saveDraft(UserMessageModel form, String username) {
        UserMessage userMessage;

        if (form.getId() != null && !form.getIsReply()) {
            userMessage = getResourceRepository().findOne(form.getId());
        } else {
            userMessage = new UserMessage();
        }

        Optional<User> userFrom = aelUserRepository.findOneByUsername(username);


        userMessage.setFrom(aelUserRepository.findOne(userFrom.get().getId()));

        if (form.getTo() != null) {
            Optional<User> userTo = aelUserRepository.findOneByUsername(form.getTo());

            if (userTo == null) {
                return 1L;
            }
            userMessage.setTo(aelUserRepository.findOne(userTo.get().getId()));
        }
        updateUserMessageInformation(form, userMessage, false);

        return 0L;
    }

    public void markAsRead(Long id) {
        UserMessage userMessage = getResourceRepository().findOne(id);

        if (userMessage.getReadDate() == null) {
            userMessage.setReadDate(new Date());

            getResourceRepository().save(userMessage);
        }
    }

    public void markAsUnread(Long id) {
        UserMessage userMessage = getResourceRepository().findOne(id);

        if (userMessage.getReadDate() != null) {
            userMessage.setReadDate(null);

            getResourceRepository().save(userMessage);
        }
    }

    public void markAllNotificationsAsRead(Long userId) {
        List<UserMessage> messages = ((UserMessageRepository) getResourceRepository())
                .getAllNotificationsByUserId(userId, UserMessageType.USER_MESSAGE);

        for (UserMessage um : messages) {
            if (um.getReadDate() == null) {
                um.setReadDate(new Date());

                getResourceRepository().save(um);
            }
        }
    }

    public void deleteMessage(Long id) {
        UserMessage userMessage = getResourceRepository().findOne(id);

        if (userMessage != null) {
            if (userMessage.getOwnerCopy()) {
                List<UserMessage> relatedMessages =
                        ((UserMessageRepository) getResourceRepository()).getAllMessagesRelatedToSource(id);
                if (relatedMessages != null) {
                    for (UserMessage relatedMessage : relatedMessages) {
                        relatedMessage.setSource(null);
                        getResourceRepository().save(relatedMessage);
                    }
                }
            }
            getResourceRepository().delete(id);
        }
    }

    private void sendUserMessage(AelUser from, AelUser to, String subject, String messageBody,
                                 String subjectArgs, String messageBodyArgs, UserMessageType userMessageType,
                                 Long entityId, String creatorIdAndTitle) {
        UserMessage userMessage = new UserMessage();

        userMessage.setFrom(from);
        userMessage.setTo(to);
        userMessage.setSubject(subject);
        userMessage.setMessageBody(messageBody);
        userMessage.setSubjectArgs(subjectArgs);
        userMessage.setMessageBodyArgs(messageBodyArgs);
        userMessage.setSent(true);
        userMessage.setUserMessageType(userMessageType);
        userMessage.setSentDate(new Date());
        userMessage.setEntityId(entityId);
        userMessage.setCreatorIdAndTitle(creatorIdAndTitle);

        getResourceRepository().save(userMessage);
    }

    public void translateAndSendUserMessage(AelUser from, AelUser to, Object[] subjectArgs, Object[] messageBodyArgs,
                                            UserMessageType userMessageType,
                                            ApplicationUserMessageFormat userMessageFormat,
                                            Long entityId, String creatorIdAndTitle) {

        String subject = userMessageFormat.get() + ".subject";
        String body = userMessageFormat.get() + ".body";

        sendUserMessage(from, to, subject, body, getXmlAsStringFromObject(subjectArgs),
                getXmlAsStringFromObject(messageBodyArgs), userMessageType, entityId, creatorIdAndTitle);
    }

    private String getXmlAsStringFromObject(Object o) {
        if (o == null) {
            return null;
        }

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        XMLEncoder xmlEncoder = new XMLEncoder(baos);
        xmlEncoder.setPersistenceDelegate(BigDecimal.class, new DefaultPersistenceDelegate() {
                    protected Expression instantiate(Object oldInstance, Encoder out) {
                        BigDecimal bd = (BigDecimal) oldInstance;
                        return new Expression(oldInstance, oldInstance.getClass(), "new", new Object[]{bd.toString()});
                    }

                    protected boolean mutatesTo(Object oldInstance, Object newInstance) {
                        return oldInstance.equals(newInstance);
                    }
                }
        );

        xmlEncoder.setPersistenceDelegate(Timestamp.class, new DefaultPersistenceDelegate() {
                    protected Expression instantiate(Object oldInstance, java.beans.Encoder out) {
                        Timestamp d = (Timestamp) oldInstance;
                        return new Expression(oldInstance, d.getClass(), "new", new Object[]{d.getTime()});
                    }

                    protected boolean mutatesTo(Object oldInstance, Object newInstance) {
                        Timestamp ts = (Timestamp) oldInstance;
                        return ts.equals(newInstance);
                    }
                }
        );
        xmlEncoder.writeObject(o);
        xmlEncoder.flush();
        xmlEncoder.close();

        try {
            return baos.toString("UTF-8");
        } catch (UnsupportedEncodingException e) {
            return baos.toString();
        }
    }

    public List<UserMessageModel> getAllNotificationsByUserId(Long userId, boolean b) {
        List<UserMessage> messages =
                ((UserMessageRepository) getResourceRepository())
                        .getAllNotificationsByUserId(userId, UserMessageType.USER_MESSAGE);
        return extractUsefullData(processMessages(messages, true, aelUserService.getLocaleForUser((AelUser)getUser(userId))));
    }

    private Predicate unreadMessagesPredicate(Long userId) {
        return QUserMessage.userMessage.to.id.eq(userId)
                .and(QUserMessage.userMessage.sent.isTrue())
                .and(QUserMessage.userMessage.ownerCopy.isFalse())
                .and(QUserMessage.userMessage.userMessageType.eq(UserMessageType.USER_MESSAGE))
                .and(QUserMessage.userMessage.readDate.isNull());
    }

    private Predicate unreadNotificationsPredicate(Long userId) {
        return QUserMessage.userMessage.to.id.eq(userId)
                .and(QUserMessage.userMessage.sent.isTrue())
                .and(QUserMessage.userMessage.ownerCopy.isFalse())
                .and(QUserMessage.userMessage.userMessageType.ne(UserMessageType.USER_MESSAGE))
                .and(QUserMessage.userMessage.readDate.isNull());
    }

    private Predicate notificationsPredicate(Long userId) {
        return QUserMessage.userMessage.to.id.eq(userId)
                .and(QUserMessage.userMessage.sent.isTrue())
                .and(QUserMessage.userMessage.ownerCopy.isFalse())
                .and(QUserMessage.userMessage.userMessageType.ne(UserMessageType.USER_MESSAGE));
    }

    public Long getAllUnreadMessagesByUserId(Long userId) {
        return ((UserMessageRepository) getResourceRepository()).count(unreadMessagesPredicate(userId));
    }

    public List<UserMessageModel> getLastNrUnreadMessagesByUserId(Long userId, Integer size) {
        Pageable pageable = createPageable(size, new Sort(Sort.Direction.DESC, "sentDate"));
        Page<UserMessage> messages = paginate(unreadMessagesPredicate(userId), pageable);

        return extractUsefullData(messages.getContent());
    }

    public List<UserMessageModel> getLastNotificationsByUserId(Long userId, Integer size) {
        Pageable pageable = createPageable(size, new Sort(Sort.Direction.DESC, "sentDate"));
        Page<UserMessage> messages = paginate(notificationsPredicate(userId), pageable);

        return extractUsefullData(messages.getContent());
    }

    public Long getAllUnreadNotificationsByUserId(Long userId) {
        return ((UserMessageRepository) getResourceRepository()).count(unreadNotificationsPredicate(userId));
    }

    public List<UserMessageModel> getLastNrUnreadNotificationsByUserId(Long userId, Integer size) {
        Pageable pageable = createPageable(size, new Sort(Sort.Direction.DESC, "sentDate"));
        Page<UserMessage> messages = paginate(unreadNotificationsPredicate(userId), pageable);

        return extractUsefullData(processMessages(messages.getContent(), false, aelUserService.getLocaleForUser((AelUser)getUser(userId))));
    }

    private List<UserMessage> processMessages(List<UserMessage> messages, boolean markAsReceived, Locale locale) {
        return messages.stream().filter(message -> !message.getUserMessageType().equals(UserMessageType.USER_MESSAGE))
                .map(
                        message -> {
                            if (markAsReceived) {
                                markMessageAsReceived(message);
                            }
                            return translateMessage(message, locale, DateFormat.getInstance());
                        }).collect(Collectors.toList());
    }

    public UserMessage createMessageFromNotificationSchedule(NotificationSchedule notificationSchedule,
                                                             Object[] subjectArgs, Object[] messageBodyArgs) {
        UserMessage userMessage = new UserMessage();

        userMessage.setFrom(notificationSchedule.getFromUser());
        userMessage.setTo(notificationSchedule.getToUser());
        userMessage.setSubject(notificationSchedule.getUserMessageFormat().toString() + ".subject");
        userMessage.setSubjectArgs(getXmlAsStringFromObject(subjectArgs));
        userMessage.setMessageBody(notificationSchedule.getUserMessageFormat().toString() + ".body");
        userMessage.setMessageBodyArgs(getXmlAsStringFromObject(messageBodyArgs));

        String entityId =
                notificationSchedule.getEntityId() == null ? null : notificationSchedule.getEntityId().toString();
        Preference pref = preferencesService.getByName(Preferences.GLOBAL_AEL_SERVER_HOST.get());
        if (pref == null || pref.getValue() == null || pref.getValue().equals("")) {
            return translateMessage(userMessage, aelUserService.getLocaleForUser(notificationSchedule.getToUser()) ,
                    DateFormat.getInstance());
        }
        return translateMessageWithTemplate(userMessage, notificationSchedule.getUserMessageFormat(), subjectArgs,
                messageBodyArgs,
                entityId, pref.getValue(), notificationSchedule.getToUser(),
                aelUserService.getLocaleForUser(notificationSchedule.getToUser()));
    }

    private User getUser(Long userId) {
        if (userId > 0) {
            return aelUserRepository.findOneById(userId);
        }
        Principal principal = SecurityContextHolder.getContext().getAuthentication();

        if (principal instanceof SocialAuthenticationToken) {
            return ((SocialAuthenticatedUser) ((SocialAuthenticationToken) principal).getPrincipal()).getUser();
        }

        return ((AuthenticatedUser) ((AbstractAuthenticationToken) principal).getPrincipal()).getUser();
    }

    public Boolean existsTemplateForUserMessageFormat(User user, ApplicationUserMessageFormat userMessageFormat,
                                                      Locale locale) {

        Long tenantId = null;
        if (user != null && user.getTenant() != null) {
            tenantId = user.getTenant().getId();
        }

        List<MessageTemplate> messageTemplates = messageTemplateService
                .findByNameAndLocaleAndTenantId(userMessageFormat.get(), locale.toString(), tenantId);

        if (messageTemplates == null || messageTemplates.size() == 0) {
            messageTemplates = messageTemplateService
                    .findByNameAndLocaleAndTenantId(userMessageFormat.get(), null, tenantId);
        }

        if (messageTemplates != null && messageTemplates.size() > 0) {
            return Boolean.TRUE;
        }

        return Boolean.FALSE;
    }

    public String getMailBodyFromTemplateForUserMessageFormat(ApplicationUserMessageFormat userMessageFormat,
                                                              Object[] messageBodyArgs,
                                                              String entityId, String serverContextPath, User to,
                                                              Locale locale) {

        String templateContent = getTemplateContent(userMessageFormat, locale);

        Matcher m = Pattern.compile("\\{\\d\\}").matcher(templateContent);
        StringBuffer sb = new StringBuffer();

        int index = 0;
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");
        while (m.find()) {
            index = Integer.valueOf(m.group().substring(1, 2));
            try {
                Object part = messageBodyArgs[index] == null ? "" : messageBodyArgs[index];
                m.appendReplacement(sb, part instanceof Date ? dateFormat.format(part) : part.toString());
            } catch (ArrayIndexOutOfBoundsException e) {
                continue;
            }
        }
        m.appendTail(sb);

        String link = ApplicationUserMessageFormat.getLinkWithEntity(userMessageFormat.get());
        link = "".equals(link) ? ApplicationUserMessageFormat.getLinkWithoutEntity(userMessageFormat.get()) : link;
        String linkForMail = link.contains("{0}") && entityId != null && !"".equals(entityId) ?
                serverContextPath + link.replaceAll("\\{0\\}", entityId) :
                !"".equals(link) ? serverContextPath + link : serverContextPath;

        String attachmentLink = ApplicationUserMessageFormat.getAttachmentLinkWithEntity(userMessageFormat.get());
        attachmentLink =
                "".equals(attachmentLink) ? ApplicationUserMessageFormat.getLinkWithoutEntity(userMessageFormat.get()) :
                        attachmentLink;
        String attachmentLinkForMail = "";

        if ("".equals(attachmentLink)) {
            attachmentLinkForMail = serverContextPath;
        } else {
            if (attachmentLink.contains("{0}")) {
                attachmentLink = attachmentLink.replaceAll("\\{0\\}", messageBodyArgs[0].toString());
            }
            if (attachmentLink.contains("{1}")) {
                attachmentLink = attachmentLink.replaceAll("\\{1\\}", messageBodyArgs[1].toString());
            }
            if (attachmentLink.contains("{2}")) {
                attachmentLink = attachmentLink.replaceAll("\\{2\\}", messageBodyArgs[2].toString());
            }
            attachmentLinkForMail = serverContextPath + attachmentLink;
        }

        String s = sb.toString().replaceAll("\\{LINK_PARAMETER\\}", linkForMail);
        String displayName = to.getDisplayName();
        if (displayName == null) {
            displayName = to.getUsername();
        }
        s = s.replaceAll("\\{NUME_UTILIZATOR_PARAMETER\\}", displayName);
        s = s.replaceAll("\\{ATTACHMENT_PARAMETER\\}", attachmentLinkForMail);

        return s;
    }

    private String getTemplateContent(ApplicationUserMessageFormat userMessageFormat, Locale locale) {
        String templateContent = null;
        List<MessageTemplate> messageTemplates =
                messageTemplateService.findByNameAndLocale(userMessageFormat.get(), locale.toString());
        if (messageTemplates == null || messageTemplates.size() == 0) {
            messageTemplates = messageTemplateService.findByNameAndLocale(userMessageFormat.get(), locale.toString());
        }
        if (messageTemplates != null) {
            if (messageTemplates.size() > 1) {
                //should be only one template
                throw new AppException("error.messageTemplate.notUnique");
            }
            templateContent = messageTemplates.get(0).getContent();
        }
        return templateContent;
    }

}
