package ro.siveco.ael.lcms.domain.metadata.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.siveco.ael.lcms.domain.auth.model.AelUser;
import ro.siveco.ael.lcms.domain.auth.service.AelUserService;
import ro.siveco.ael.lcms.domain.metadata.model.*;
import ro.siveco.ael.lcms.repository.InterestRepository;
import ro.siveco.ael.web.controllers.WithUserBaseEntityController;
import ro.siveco.ram.app.exception.AppException;
import ro.siveco.ram.starter.service.BaseEntityService;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/*Created by albertb on 1/30/2018.*/


@Service
public class InterestService extends BaseEntityService<Interest> {

    private final TagService tagService;
    private final LanguageService languageService;
    private final RelationService relationService;
    private final AelUserService aelUserService;

    private final String TAG_FILTER = "tagFilter";

    @Autowired
    public InterestService(InterestRepository interestRepository, TagService tagService, LanguageService languageService, RelationService relationService, AelUserService aelUserService ) {
        super(Interest.class, interestRepository);
        this.tagService = tagService;
        this.languageService = languageService;
        this.relationService = relationService;
        this.aelUserService = aelUserService;
    }

    public void validateInterest(Interest interest) {
        if( interest != null ) {
            List<Interest> interests= getResourceRepository().findAll();
            if( interests!= null && interests.size() > 0 ) {
                throw new AppException("Interest image error.");
            }
        }
    }

    public List<Interest> getAllInterests()
    {
        return ((InterestRepository)getResourceRepository()).findAll();
    }

    public List<Interest> getAllI18NInterests(Principal principal, HttpServletRequest request)
    {
        List<Interest> allInterests = getResourceRepository().findAll();

        if (allInterests != null) {
            String language = aelUserService.getLanguage(principal, request);
            for (Interest interest : allInterests) {
                interest.setDisplayName(tagService.getI18NTagName(interest.getTag(), language));
            }
        }
        return allInterests;
    }

    public List<Interest> getI18NNotSelectedInterests(Principal principal, HttpServletRequest request)
    {
        List<Interest> allInterests = getResourceRepository().findAll();
        List<Interest> notSelectedInterests = new ArrayList<>();
        AelUser currentUser = (AelUser)WithUserBaseEntityController.getUser();
        List<Tag> otherTags = tagService.findAllWithoutMetadataWithInterest(currentUser.getId(), EntityType.USER);
        List<String> filterIds = new ArrayList<>();
        String[] tagFilterIds = request.getParameterValues(TAG_FILTER);
        if (tagFilterIds != null) {
            filterIds = Arrays.asList(tagFilterIds);
        }
        getMatchingInterests(principal, request, allInterests, notSelectedInterests, otherTags, filterIds);
        return notSelectedInterests;
    }

    public List<Interest> getI18NSelectedInterests(Principal principal, HttpServletRequest request)
    {
        List<Interest> allInterests = getResourceRepository().findAll();
        List<Interest> selectedInterests = new ArrayList<>();
        AelUser currentUser = (AelUser)WithUserBaseEntityController.getUser();
        List<String> filterIds = new ArrayList<>();
        String[] tagFilterIds = request.getParameterValues(TAG_FILTER);
        if (tagFilterIds != null) {
            filterIds = Arrays.asList(tagFilterIds);
        }
        List<Tag> userTags = tagService.getAllTagsByEntityIdTypeAndRelation(currentUser.getId(), EntityType.USER, RelationType.INCLUDED);
        if (allInterests != null) {
            for (Interest interest : allInterests) {
                if (userTags != null && userTags.contains(interest.getTag())) {
                    interest.setDisplayName(tagService.getI18NTagName(interest.getTag(), aelUserService.getLanguage(principal, request)));
                    selectedInterests.add(interest);
                }
                if (filterIds.contains(interest.getTag().getId().toString()) && !selectedInterests.contains(interest)) {
                    interest.setDisplayName(tagService.getI18NTagName(interest.getTag(), aelUserService.getLanguage(principal, request)));
                    selectedInterests.add(interest);
                }
            }
        }
        return selectedInterests;
    }

    private void getMatchingInterests(Principal principal, HttpServletRequest request, List<Interest> allInterests, List<Interest> interests, List<Tag> userTags, List<String> filterIds) {
        if (allInterests != null) {
            for (Interest interest : allInterests) {
                if (userTags != null && userTags.contains(interest.getTag())) {
                    interest.setDisplayName(tagService.getI18NTagName(interest.getTag(), aelUserService.getLanguage(principal, request)));
                    if (!filterIds.contains(interest.getTag().getId().toString())) {
                        interests.add(interest);
                    }
                }
            }
        }
    }


    public List<Tag> getI18NNotSelectedTags()
    {
        List<Interest> allInterests = getResourceRepository().findAll();
        List<Tag> notSelectedTags = new ArrayList<>();
        AelUser currentUser = (AelUser)WithUserBaseEntityController.getUser();
        String language = aelUserService.getLocaleForUser(currentUser).getLanguage();
        List<Tag> otherTags = tagService.findAllWithoutMetadataWithInterest(currentUser.getId(), EntityType.USER);
        if (allInterests != null) {
            for (Interest interest : allInterests) {
                Tag tag = interest.getTag();
                if (otherTags != null && otherTags.contains(tag)) {
                    tag.setDisplayName(tagService.getI18NTagName(tag,language));
                    notSelectedTags.add(tag);
                }
            }
        }
        return notSelectedTags;
    }
}
