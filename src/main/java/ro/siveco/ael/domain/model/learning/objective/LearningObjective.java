package ro.siveco.ael.domain.model.learning.objective;

import ro.siveco.ael.domain.model.core.Record;
import ro.siveco.ael.domain.model.learning.grammer.Verb;

import java.util.Collection;

/**
 * User: AlexandruVi
 * Date: 2016-12-08
 */
public interface LearningObjective extends Record<Long> {

    /**
     * @return The verbs
     */
    public Collection<Verb> getObjectiveVerbs();
}
