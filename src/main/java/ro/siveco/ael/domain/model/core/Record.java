package ro.siveco.ael.domain.model.core;

import ro.siveco.ram.model.Entity;

import java.io.Serializable;

/**
 * <p>The record interface is a marker for every entity that the app's end-user needs to be aware of.</p>
 * <p>E.g.: <code>"In this LRS you have control over the following records[...]. There are other records that you can
 * encounter, but the focus should be on these enumerated."</code> </p>
 * User: AlexandruVi
 * Date: 2016-12-07
 */
public interface Record<ID extends Serializable> extends Entity<ID> {
}
