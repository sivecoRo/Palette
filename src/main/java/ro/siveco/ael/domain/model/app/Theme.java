package ro.siveco.ael.domain.model.app;

/**
 * User: AlexandruVi
 * Date: 2017-02-23
 */
public interface Theme {

    String getThemeName();
}
