package ro.siveco.ael.domain.model.core;

import java.io.Serializable;

/**
 * User: AlexandruVi
 * Date: 2017-01-10
 */
public interface SerialValue {

    /**
     * @return The serializable value
     */
    public Serializable getValue();
}
