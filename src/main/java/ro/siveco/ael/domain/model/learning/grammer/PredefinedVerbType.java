package ro.siveco.ael.domain.model.learning.grammer;

/**
 * User: AlexandruVi
 * Date: 2016-12-15
 */
public enum PredefinedVerbType implements VerbType {
    EXPERIENCE_VERB,;

    /**
     * @return The type name
     */
    @Override
    public String getName() {
        return this.name();
    }

    @Override
    public void setName(String name) {

    }
}
