package ro.siveco.ael.domain.model.learning.level;

import ro.siveco.ael.domain.model.learning.grammer.Verb;

import java.util.*;

/**
 * User: AlexandruVi
 * Date: 2016-12-13
 */
public enum KnowledgePhase implements LearningLevel {
    FACTUAL,
    CONCEPTUAL,
    PROCEDURAL,
    META_COGNITIVE;

    private final Collection<Verb> verbs;

    private final LearningProcessDimension learningProcessDimension =
            PredefinedLearningProcessDimension.KNOWLEDGE_DIMENSION;

    KnowledgePhase(Verb... verbs) {
        this.verbs = new ArrayList<Verb>(Arrays.asList(verbs));

    }

    static public NavigableSet<LearningLevel> getHierarchy() {

        NavigableSet<LearningLevel> hierarchy = new TreeSet<LearningLevel>();
        Collections.addAll(hierarchy, KnowledgePhase.values());
        return hierarchy;
    }

    /**
     * @return The verbs associated with this level
     */
    @Override
    public Collection<Verb> getVerbs() {
        return this.verbs;
    }

    /**
     * @return The level name
     */
    @Override
    public String getName() {
        return this.name();
    }

    @Override
    public void setName(String name) {

    }

    /**
     * @return The level (zero-based of this level in the hierarchy)
     */
    @Override
    public Integer getHierarchicalLevel() {
        return null;
    }

    /**
     * @return The link to other learning level
     */
    @Override
    public LearningLevel getLink() {
        return null;
    }

    /**
     * @return The dimension associated with this level
     */
    @Override
    public LearningProcessDimension getLearningProcessDimension() {
        return this.learningProcessDimension;
    }

    /**
     * @return The type of relation to the other learning level
     */
    @Override
    public LearningLevelRelation getLearningLevelLinkType() {
        return null;
    }
}
