package ro.siveco.ael.domain.model.learning.grammer;

/**
 * User: AlexandruVi
 * Date: 2016-12-12
 */
public enum PredefinedVerb implements Verb {
    list, state, name, recall, label, record, define, repeat, underline, arrange,
    memorize, recognize, reproduce,
    explain, describe, report, translate, express, summarize, identify, classify, restate, locate,
    discuss, illustrate, tell, estimate, reference, reiterate, apply, sketch, perform, use,
    solve, respond, practice,
    role_play, demonstrate, conduct, execute, complete, dramatize, employ, analyze, inspect, test, distinguish,
    categorize, critique, differentiate, catalogue, diagnose, quantify, extrapolate, calculate,
    theorize, experiment, relate, debate, develop, revise, compose, plan, formulate, collect, build, propose, construct,
    create, establish, prepare, design, integrate, devise, organize, modify, manage, review, appraise, choose, justify,
    argue, conclude, assess, rate, compare, defend, score, evaluate,
    report_on, select, interpret, investigate, measure, support;

    /**
     * @return The name of the verb
     */
    @Override
    public String getName() {
        return this.name();
    }

    @Override
    public void setName(String name) {

    }


}
