package ro.siveco.ael.domain.model.learning.resource;

import ro.siveco.ael.domain.model.core.Record;

/**
 * User: AlexandruVi
 * Date: 2016-12-20
 */
public interface LearningObject extends Record<Long> {
}
