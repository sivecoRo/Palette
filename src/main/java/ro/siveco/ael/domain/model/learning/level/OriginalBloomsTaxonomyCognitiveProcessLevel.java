package ro.siveco.ael.domain.model.learning.level;

import ro.siveco.ael.domain.model.learning.grammer.PredefinedVerb;
import ro.siveco.ael.domain.model.learning.grammer.Verb;

import java.util.*;

/**
 * User: AlexandruVi
 * <p>
 * <a href="https://www.clinton.edu/curriculumcommittee/listofmeasurableverbs.cxml">Bloom's Taxonomy of Educational Objectives (1956): Cognitive Skills</a>
 * </p>
 * Date: 2016-12-12
 */
public enum OriginalBloomsTaxonomyCognitiveProcessLevel implements LearningLevel {
    KNOWLEDGE(PredefinedVerb.list, PredefinedVerb.state, PredefinedVerb.name, PredefinedVerb.tell,
            PredefinedVerb.recall, PredefinedVerb.label, PredefinedVerb.record, PredefinedVerb.define,
            PredefinedVerb.relate, PredefinedVerb.repeat, PredefinedVerb.select, PredefinedVerb.underline,
            PredefinedVerb.arrange, PredefinedVerb.describe, PredefinedVerb.memorize,
            PredefinedVerb.recognize, PredefinedVerb.reproduce),
    COMPREHENSION(PredefinedVerb.explain, PredefinedVerb.translate, PredefinedVerb.identify, PredefinedVerb.restate,
            PredefinedVerb.discuss, PredefinedVerb.tell, PredefinedVerb.reference,
            PredefinedVerb.describe, PredefinedVerb.express, PredefinedVerb.classify, PredefinedVerb.locate,
            PredefinedVerb.review, PredefinedVerb.critique, PredefinedVerb.interpret,
            PredefinedVerb.report, PredefinedVerb.summarize, PredefinedVerb.discuss, PredefinedVerb.compare,
            PredefinedVerb.illustrate, PredefinedVerb.estimate, PredefinedVerb.reiterate),
    APPLICATION(PredefinedVerb.apply, PredefinedVerb.use, PredefinedVerb.practice, PredefinedVerb.demonstrate,
            PredefinedVerb.complete, PredefinedVerb.sketch, PredefinedVerb.solve,
            PredefinedVerb.construct, PredefinedVerb.dramatize, PredefinedVerb.conduct, PredefinedVerb.perform,
            PredefinedVerb.respond, PredefinedVerb.role_play, PredefinedVerb.execute,
            PredefinedVerb.employ),
    ANALYSIS(PredefinedVerb.analyze, PredefinedVerb.distinguish, PredefinedVerb.differentiate, PredefinedVerb.appraise,
            PredefinedVerb.calculate, PredefinedVerb.experiment,
            PredefinedVerb.inspect, PredefinedVerb.categorize, PredefinedVerb.catalogue, PredefinedVerb.quantify,
            PredefinedVerb.measure, PredefinedVerb.relate, PredefinedVerb.test,
            PredefinedVerb.critique, PredefinedVerb.diagnose, PredefinedVerb.extrapolate, PredefinedVerb.theorize,
            PredefinedVerb.debate),
    SYNTHESIS(PredefinedVerb.develop, PredefinedVerb.plan, PredefinedVerb.build, PredefinedVerb.create,
            PredefinedVerb.design, PredefinedVerb.organize, PredefinedVerb.revise, PredefinedVerb.formulate,
            PredefinedVerb.propose, PredefinedVerb.establish, PredefinedVerb.integrate, PredefinedVerb.modify,
            PredefinedVerb.compose, PredefinedVerb.collect, PredefinedVerb.construct,
            PredefinedVerb.prepare, PredefinedVerb.devise, PredefinedVerb.manage),
    EVALUATION(PredefinedVerb.review, PredefinedVerb.justify, PredefinedVerb.assess, PredefinedVerb.defend,
            PredefinedVerb.report_on, PredefinedVerb.investigate, PredefinedVerb.appraise,
            PredefinedVerb.argue, PredefinedVerb.rate, PredefinedVerb.score, PredefinedVerb.select,
            PredefinedVerb.measure, PredefinedVerb.choose, PredefinedVerb.conclude, PredefinedVerb.compare,
            PredefinedVerb.evaluate, PredefinedVerb.interpret, PredefinedVerb.support);

    private final LearningProcessDimension learningProcessDimension =
            PredefinedLearningProcessDimension.COGNITIVE_PROCESS_DIMENSION;

    private final Collection<Verb> verbs;

    OriginalBloomsTaxonomyCognitiveProcessLevel(Verb... verbs) {
        this.verbs = new ArrayList<Verb>(Arrays.asList(verbs));

    }

    static public NavigableSet<LearningLevel> getHierarchy() {

        NavigableSet<LearningLevel> hierarchy = new TreeSet<LearningLevel>();
        Collections.addAll(hierarchy, OriginalBloomsTaxonomyCognitiveProcessLevel.values());
        return hierarchy;
    }

    /**
     * @return The verbs associated with this level
     */
    @Override
    public Collection<Verb> getVerbs() {
        return this.verbs;
    }

    /**
     * @return The level name
     */
    @Override
    public String getName() {
        return this.name();
    }

    @Override
    public void setName(String name) {

    }

    /**
     * @return The level (zero-based of this level in the hierarchy)
     */
    @Override
    public Integer getHierarchicalLevel() {
        return this.ordinal();
    }

    /**
     * @return The link to other learning level
     */
    @Override
    public LearningLevel getLink() {
        return null;
    }

    /**
     * @return The dimension associated with this level
     */
    public LearningProcessDimension getLearningProcessDimension() {
        return this.learningProcessDimension;
    }

    /**
     * @return The type of relation to the other learning level
     */
    @Override
    public LearningLevelRelation getLearningLevelLinkType() {
        return null;
    }
}
