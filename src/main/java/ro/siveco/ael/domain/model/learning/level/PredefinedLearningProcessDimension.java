package ro.siveco.ael.domain.model.learning.level;

/**
 * User: AlexandruVi
 * Date: 2016-12-12
 */
public enum PredefinedLearningProcessDimension implements LearningProcessDimension {
    COGNITIVE_PROCESS_DIMENSION, KNOWLEDGE_DIMENSION;
}
