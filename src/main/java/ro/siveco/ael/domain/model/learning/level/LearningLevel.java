package ro.siveco.ael.domain.model.learning.level;

import ro.siveco.ael.domain.model.learning.grammer.Verb;
import ro.siveco.ram.model.Named;

import java.util.Collection;

/**
 * User: AlexandruVi
 * Date: 2016-12-12
 */
public interface LearningLevel extends Named {
    /**
     * @return The verbs associated with this level
     */
    Collection<Verb> getVerbs();

    /**
     * @return The level (zero-based of this level in the hierarchy)
     */
    Integer getHierarchicalLevel();

    /**
     * @return The link to other learning level
     */
    LearningLevel getLink();

    /**
     * @return The dimension associated with this level
     */
    LearningProcessDimension getLearningProcessDimension();

    /**
     * @return The type of relation to the other learning level
     */
    LearningLevelRelation getLearningLevelLinkType();

}
