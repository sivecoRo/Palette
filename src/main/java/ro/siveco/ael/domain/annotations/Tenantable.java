package ro.siveco.ael.domain.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by IulianB on 1/29/2015.
 */
@Target({TYPE})
@Retention(RUNTIME)
public @interface Tenantable {
}
