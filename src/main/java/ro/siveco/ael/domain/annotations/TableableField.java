package ro.siveco.ael.domain.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by IntelliJ IDEA.
 * User: RazvanNi
 * Date: Apr 9, 2008
 * Time: 5:26:49 PM
 * To change this template use File | Settings | File Templates.
 */
@Target( ElementType.FIELD )
@Retention( RUNTIME )
public @interface TableableField
{
	String key() default "";

	String property() default "";

	String reportKey() default "";

	boolean hidden() default false;

	boolean sortable() default true;

	boolean defaultSortColumn() default false;

	String[] groups() default {"*"};

	long[] order() default {0};
}
