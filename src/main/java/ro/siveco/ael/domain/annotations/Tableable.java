package ro.siveco.ael.domain.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by IntelliJ IDEA.
 * User: RazvanNi
 * Date: Mar 25, 2008
 * Time: 5:13:31 PM
 * To change this template use File | Settings | File Templates.
 */

@Target( ElementType.FIELD )
@Retention( RUNTIME )
public @interface Tableable
{
	TableableField[] value() default {};
}
