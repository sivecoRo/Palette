package ro.siveco.ael.domain.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * User: cristianc
 * Date: 29.01.2008
 * Time: 14:53:22
 */

@Target({TYPE})
@Retention(RUNTIME)

public @interface TableComment {
    String comment() default "";
}
