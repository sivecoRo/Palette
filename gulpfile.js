'use strict';

var gulp = require('gulp'),
    sass = require('gulp-sass'),
    sourceMaps = require('gulp-sourcemaps'),

    SASS_SOURCE = [
        './src/main/resources/sass/app.scss'
    ],
    DESTINATION_FOLDER = "./src/main/webapp/css/generated";

gulp.task('build', ["scss_convert_to_css"]);

gulp.task('scss_convert_to_css', function () {
    return gulp.src(SASS_SOURCE)
        .pipe(sass().on('error', sass.logError))
        .pipe(sourceMaps.write())
        .pipe(gulp.dest(DESTINATION_FOLDER));
});

gulp.task('sass:watch', function () {
    gulp.watch(SASS_SOURCE, ['sass']);
});