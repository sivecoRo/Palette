# Palette

**Project Structure**

src/main/java (backend)

src/main/resources (configurations and properties, html templates)

src/main/webapp (web static files: images, javascript, generated css)

**How to start developing the client side**

Html files are located in 

                         src/main/resources/templates/themes
                         
under the theme name (current: materialize).

The web app will resolve these templates with controllers returning the template name relative to the theme folder.

**See** _ro.siveco.ael.web.controllers.AppController_.register
                        
                   @GetMapping(WebPath.REGISTER)
                      public String register(Principal principal, HttpServletRequest request, HttpServletResponse response,
                                             HttpSession session, ModelMap modelMap) {
                  
                          return Template.REGISTER;
                      }

**CSS sources** are generated on build from sass (located in src/main/resources/sass).
If you are not familiar with sass/scss you can create your own .css file an put it under _src/main/webapp/css_ and then reference it in 

                                templates/themes/materialize/layout/stylesAll.html
                                in the "allStyleElements" fragment

**Javascript sources** is located under src/main/webapp/js.
Similar to css, files have to be referenced in the

                                templates/themes/materialize/layout/scriptsAll.html
                                
                                
====================================
                        TO BE CONTINUED                                
